# README #

LAP Debug Tools is a small collection of useful debugging classes. It includes a callstack generator, a callstack enabled assert and a heap debugger
version 1.0

### SCREENSHOTS ###

Assert with callstack:

 ![](img/assert.png) 

Simple callstack:

 ![](img/callstack.png)

Callstack from multiple threads:

 ![](img/callstack_multithreaded.png)

No memory leaks:

 ![](img/leaks_no.png)

Various types of memory leaks:

 ![](img/leaks_present.png)

###PROPERTIES (ALL)###
- c++11
- thread safe
- no cpp exceptions (while more elegant, exceptions are not available/desired in/on some environments/platforms)
- tested on vs2015 cpp and gcc 4.8

###PROPERTIES (DebugToolsCallStack)###
- callstack is stored and printed the correct way (growing top to bottom)
- works on windows, unix, should work on macs (needs testing)
- callstack exports unmangled symbols
- uses addr2line on unix to demangle the symbols (windows has better support for such operations). needs to be tested on mac.
  addr2line has some cases where it can fail and will produce 0s and "not available" names. Unfortunately, given the lack 
  of other portable demangling tools on *nix this remains an open problem.
- the callstack might return slightly wrong line numbers (e.g. 184 instead of 182). This is caused by compiler optimizations 
  (such as inlining) which are sometimes performed even in debug mode on certain platforms. Unfortunately, without more exact 
  platform specific debug tools this problem is unsolvable.
  Common inlining culprits are (compiler dependent):
	- one/two line functions
	- operators (including global operators)
	- some copy ellisions
	- some virtual functions
	- recursive functions that can only be partially inlined
	- branches
	- temporary objects
	- etc\n
	Nevertheless, approximate line numbers ARE helpful in debugging. 
- defines a macro, LAP_FUNC, with the name of the current function (platform independent for __func__, __function__, etc)
- on windows the unimportant entries in the callstack are eliminated (mainCRT, rundll, etc)

###PROPERTIES (LAP_ASSERT)###
- is callstack-enabled, on failed assertions it prints the entire callstack to the screen.
- the macro can be disabled by defining LAP_NO_ASSERT  before including lap_logger, working like NDEBUG for assert. 
- this macro overwrites any previously defined LAP_ASSERT (e.g. lap_logger.hpp)

###PROPERTIES (DebugToolsHeapTracker)###
- overloads the global new, new[], delete, delete[] operators
- does not work with malloc/calloc/realloc
- the heap debuger needs to be included once, anywhere, anyhow (compared to #define based methods which need to be included in each file, in a very specific order)
- it does not use non - standard mechanics such as defining new (undefined behavior as per standard)
- can use the CALLSTACK to properly determine the function, line, filename, etc of the caller. As expected, this is expensive.
- can perform different types of tracking, through the setTracking function
	- TRACKING_NONE      -> tracks nothing
	- TRACKING_NORMAL -> tracks memory leaks (size, address, threadid, array allocation?) but does not track special information (filename, function name, caller line, etc)
	- TRACKING_FULL        -> tracks caller filename, function name, line, threadid, address, size
- a reporting function is included which can log to the stdout or a user specified file
- an atexit function is included which controls the program termination heap debugger behavior (perform a report?, free leaked memory?)
- provides allocate and deallocate functions which can be used manually.
- notifies bad deletes (deletes on non malloc'd memory or deletes on already freed memory)
- notifies unmatching deletes (delete [] on memory allocated with new, delete on memory allocated with new [] )

###TODO LIST###

add monitoring and profiling tools

### COMPILING ###

Visual studio 2015 project&solution files can be found in the **vstudio** folder. Makefiles for gnu/gcc ( **makefile** ) and for visual studio ( **makefilevs.bat** ) are provided in the root folder. All methods will generate objs, pdbs, exes, etc in **bin**.


###EXAMPLES (DebugToolsCallStack)###

		#include "lap_debug_tools.hpp"
		...
		lap::DebugToolsCallStack cs; 
		//do a full stack print
		cs.print();
		//assuming callstack is sufficiently long (guaranteed to have at least one element)..
		std::cout << "At 2 " << cs.getCallStack()[2].function << std::endl;
		std::cout << "At 4 " << cs.getCallStack()[4].line << " " << cs.getCallStack()[4].function << std::endl;
		...
		//print callstack to buffer
		lap::DebugToolsCallStack cs; 
		char buffer[4096];
		cs.print(buffer, 4096);
		...
		//get caller function information, 
		//when calling with a specified number of frames the last in stack are taken, thus here cs[0] is the caller function and cs[1] is this function
		lap::DebugToolsCallStack cs(2);
		const lap::DebugToolsCallStack::Entry& e = cs.getCallStackEntry(0);
		std::cout<<"Called from function "<<e.function<<" from filename "<<e.filename<<" from line"<<e.line<<" (and some dbg info, function address is "<<e.address<<")"<<std::endl;

###EXAMPLES (LAP_ASSERT)###

		//#define LAP_NO_ASSERT			// will disable LAP_ASSERT
		#include "lap_debug_tools.hpp"
		...
		LAP_ASSERT(0 == 1, "this will fail and the callstack will be printed")
		
### EXAMPLES (DebugToolsCallStack) ###

		#include "lap_debug_tools.hpp"
		...
		lap::DebugToolsHeapTracker::getInstance().setTrackingMode(lap::DebugToolsHeapTracker::TRACKING_FULL);
		lap::DebugToolsHeapTracker::getInstance().setReport(true, true, "heapdebugger.log");
		lap::DebugToolsHeapTracker::getInstance().setAtexit(true, true);
		int* p = new int[10];
		delete o;				//good
		int *p2 = nullptr;
		delete [] p2;			//bad, delete[] for new'd memory
		delete p2;				//bad, no new'd memory
		int* p3 = new int[4];
		delete p3;				//bad, delete for new[]'d memory
		delete [] p3;			//good
		lap::DebugToolsHeapTracker::getInstance().report();
		...
		lap::DebugToolsHeapTracker::getInstance().setTrackingMode(lap::DebugToolsHeapTracker::TRACKING_NORMAL);
		std::map<std::string, std::pair<std::pair< std::string, std::string>, std::pair<std::string, std::string>>> lotsofnews;
		for(int i=0; i< 932984; i++){
			std::string istr = std::to_string(i);
			lotsofnews[istr] = {{istr + "a", istr+ "b"},{istr + "c", istr + "d"} };
		}
		lotsofnews.clear();
		lap::DebugToolsHeapTracker::getInstance().report();

### DOCUMENTATION ###

a Doxyfile is provided in **doc**, which can be used with Doxygen to generate  the entire documentation in HTML format. 

### COMPATIBILITY ###
tested with visual studio 2015, Ubuntu 15 + gcc 4.8.4, MacOS El Capitan + clang 3.7.1. The output is limited to the platform exposed entry hooks.
does not work with MSYS2 + MinGW x64 + GCC 6.20

