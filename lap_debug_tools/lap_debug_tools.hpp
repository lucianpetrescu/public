///---------------------------------------------------------------------------------------------------------------------
/// The MIT License (MIT)
/// 
/// Copyright (c) 2016 Lucian Petrescu, lucian (dot) petrescu (dot) 24 (at) gmail (dot) com
/// 
/// Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation 
/// files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, 
/// modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software 
/// is furnished to do so, subject to the following conditions:
/// 
/// The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
/// 
/// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
/// OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE 
/// LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR 
/// IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
///---------------------------------------------------------------------------------------------------------------------

///------------------------------------------------------------------------------------------------
/// @file
/// @mainpage LAP Debug Tools
/// LAP Debug Tools is a small collection of useful debugging classes. It includes a callstack generator, a callstack enabled assert and a heap debugger
///
///	@version 1.00
///
///	@author 
///		Lucian Petrescu( lucian (dot) petrescu (dot) 24 (at) gmail (dot) com )
/// @par All
///		- c++11
///		- thread safe
///		- no cpp exceptions (while more elegant, exceptions are not available/desired in/on some environments/platforms)
///		- tested on vs2015 cpp, gcc 4.8, clang 3.6
///
///	@par DebugToolsCallStack
///		- callstack is stored and printed the correct way (growing top to bottom)
///		- works on windows, unix, should work on macs (needs testing)
///		- callstack exports unmangled symbols
///		- uses addr2line on unix to demangle the symbols (windows has better support for such operations). needs to be tested on mac.
///		  addr2line has some cases where it can fail and will produce 0s and "not available" names. Unfortunately, given the lack 
///		  of other portable demangling tools on *nix this remains an open problem.
///		- the callstack might return slightly wrong line numbers (e.g. 184 instead of 182). This is caused by compiler optimizations 
///		  (such as inlining) which are sometimes performed even in debug mode on certain platforms. Unfortunately, without more exact 
///		  platform specific debug tools this problem is unsolvable.
///		  Common inlining culprits are (compiler dependent of course):
///			- one/two line functions
///			- operators (including global operators)
///			- some copy ellisions
///			- some virtual functions
///			- recursive functions that can only be partially inlined
///			- branches
///			- temporary objects
///			- etc\n
///		  Nevertheless, approximate line numbers ARE helpful in debugging. 
///		- defines a macro, LAP_FUNC, with the name of the current function (platform independent for __func__, __function__, etc)
///		- on windows the unimportant entries in the callstack are eliminated (mainCRT, rundll, etc)
///
///	@par DebugToolsCallStack usage
///		@code
///		#include "lap_debug_tools.hpp"
///		...
///		lap::DebugToolsCallStack cs; 
///		//do a full stack print
///		cs.print();
///		//assuming callstack is sufficiently long..
///		std::cout << "At 2 " << cs.getCallStack()[2].function << std::endl;
///		std::cout << "At 4 " << cs.getCallStack()[4].line << " " << cs.getCallStack()[4].function << std::endl;
///		...
///		//print callstack to buffer
///		lap::DebugToolsCallStack cs; 
///		char buffer[4096];
///		cs.print(buffer, 4096);
///		...
///		//get caller function information, 
///		//when calling with a specified number of frames the last in stack are taken, thus here cs[0] is the caller function and cs[1] is this function
///		lap::DebugToolsCallStack cs(2);
///		const lap::DebugToolsCallStack::Entry& e = cs.getCallStackEntry(0);
///		std::cout<<"Called from function "<<e.function<<" from filename "<<e.filename<<" from line"<<e.line<<" (and some dbg info, function address is "<<e.address<<")"<<std::endl;
///		@endcode
///
///	@par LAP_ASSERT
///		- is callstack-enabled, on failed assertions it prints the entire callstack to the screen.
///		- the macro can be disabled by defining LAP_NO_ASSERT  before including lap_logger, working like NDEBUG for assert. 
///		- this macro overwrites any previously defined LAP_ASSERT (e.g. lap_logger.hpp)
///
///	@par LAP_ASSERT usage
///		@code
///		//#define LAP_NO_ASSERT			// will disable LAP_ASSERT
///		#include "lap_debug_tools.hpp"
///		...
///		LAP_ASSERT(0 == 1, "this will fail and the callstack will be printed")
///		@endcode
///
/// @par DebugToolsHeapTracker
///		- overloads the global new, new[], delete, delete[] operators
///		- does not work with malloc/calloc/realloc
///		- the heap debuger needs to be included once, anywhere, anyhow (compared to #define based methods which need to be included in each file, in a very specific order)
///		- it does not use non - standard mechanics such as defining new (undefined behavior as per standard)
///		- can use the CALLSTACK to properly determine the function, line, filename, etc of the caller. As expected, this is expensive.
///		- can perform different types of tracking, through the setTracking function
///			- TRACKING_NONE   -> tracks nothing
///			- TRACKING_NORMAL -> tracks memory leaks (size, address, threadid, array allocation?) but does not track special information (filename, function name, caller line, etc)
///			- TRACKING_FULL	  -> tracks caller filename, function name, line, threadid, address, size
///		- a reporting function is included which can log to the stdout or a user specified file
///		- an atexit function is included which controls the program termination heap debugger behavior (perform a report?, free leaked memory?)
///		- provides allocate and deallocate functions which can be used manually.
///		- notifies bad deletes (deletes on non malloc'd memory or deletes on already freed memory)
///		- notifies unmatching deletes (delete [] on memory allocated with new, delete on memory allocated with new [] )
///
/// @par DebugToolsCallStack usage
///		@code
///		#include "lap_debug_tools.hpp"
///		...
///		lap::DebugToolsHeapTracker::getInstance().setTrackingMode(lap::DebugToolsHeapTracker::TRACKING_FULL);
///		lap::DebugToolsHeapTracker::getInstance().setReport(true, true, "heapdebugger.log");
///		lap::DebugToolsHeapTracker::getInstance().setAtexit(true, true);
///		int* p = new int[10];
///		delete o;				//good
///		int *p2 = nullptr;
///		delete [] p2;			//bad, delete[] for new'd memory
///		delete p2;				//bad, no new'd memory
///		int* p3 = new int[4];
///		delete p3;				//bad, delete for new[]'d memory
///		delete [] p3;			//good
///		lap::DebugToolsHeapTracker::getInstance().report();
///		...
///		lap::DebugToolsHeapTracker::getInstance().setTrackingMode(lap::DebugToolsHeapTracker::TRACKING_NORMAL);
///		std::map<std::string, std::pair<std::pair< std::string, std::string>, std::pair<std::string, std::string>>> lotsofnews;
///		for(int i=0; i< 932984; i++){
///			std::string istr = std::to_string(i);
///			lotsofnews[istr] = {{istr + "a", istr+ "b"},{istr + "c", istr + "d"} };
///		}
///		lotsofnews.clear();
///		lap::DebugToolsHeapTracker::getInstance().report();
///		@endcode
///
///------------------------------------------------------------------------------------------------


#ifndef LAP_DEBUG_TOOLS_H_
#define LAP_DEBUG_TOOLS_H_
#include <cassert>
#include <cstdio>
#include <cstdarg>
#include <cstdlib>
#include <mutex>
#include <cstring>



///------------------------------------------------------------------------------------------------
//platform specific includes
#if defined(__gcc__) || defined(unix) || defined(__unix) || defined(__unix__) || defined(__linux__) || defined(__APPLE__) || defined(__MACH__)
	#include <execinfo.h>
	#include <cxxabi.h>
	#include <sys/types.h>
	#include <unistd.h>
	#include <pthread.h>
	#include <errno.h>
	//clang defines __GNUC__ but does not implement some extensions..
	#if defined(__GNUC__) && !defined(__clang__)
		#ifndef _GNU_SOURCE
			#define _GNU_SOURCE
		#endif
		extern char *program_invocation_name;	//gnu extension
	#endif
#elif defined(_WIN32) || defined(_WIN64)
	#ifndef WIN32_LEAN_AND_MEAN
		#define WIN32_LEAN_AND_MEAN
	#endif
	#ifndef NOMINMAX
		#define NOMINMAX
	#endif
	#if defined(_MSC_VER)
		#ifndef VC_EXTRALEAN
			#define VC_EXTRALEAN
		#endif
		#ifndef _CRT_SECURE_NO_DEPRECATE 
			#define _CRT_SECURE_NO_DEPRECATE 
		#endif
	#endif
	#include <Windows.h>
	#include <process.h>
	//#if defined(_MSC_VER)
		//MinGW can also access Dbghelp.
		#pragma warning(push)
		#pragma warning(disable : 4091)
		#include <Dbghelp.h>
		#pragma comment(lib, "Dbghelp.lib")
		#pragma warning(pop)
	//#endif
	#undef NOMINMAX
	#undef WIN32_LEAN_AND_MEAN
	#undef VC_EXTRALEAN
#else
	"ERROR: platform not supported!"
#endif

///------------------------------------------------------------------------------------------------
/// function macro, inspired from http://www.boost.org/doc/libs/1_59_0/boost/current_function.hpp 
#ifndef LAP_FUNC
	#if (defined(__cplusplus) && (__cplusplus >= 201103)) || (defined(__STDC_VERSION__) && (__STDC_VERSION__ >= 199901))
		#define LAP_FUNC __func__
	#elif defined(__GNUC__) || (defined(__MWERKS__) && (__MWERKS__ >= 0x3000)) || (defined(__ICC) && (__ICC >= 600)) || defined(__ghs__) || (defined(__DMC__) && (__DMC__ >= 0x810))
		#define LAP_FUNC __PRETTY_FUNCTION__
	#elif defined(__FUNCSIG__) || defined(_MSC_VER)
		#define LAP_FUNC __FUNCSIG__
	#elif (defined(__INTEL_COMPILER) && (__INTEL_COMPILER >= 600)) || (defined(__IBMCPP__) && (__IBMCPP__ >= 500))
		#define LAP_FUNC __FUNCTION__
	#elif defined(__BORLANDC__) && (__BORLANDC__ >= 0x550)
		#define LAP_FUNC __FUNC__
	#else
		"(unknown)"
	#endif
#endif




namespace lap{
	
	///--------------------------------------------------------------------------------------------
	/// a tool for debugging call stacks
	/// using only malloc, in order to 
	class DebugToolsCallStack{
	public:

		/// a stack trace entry
		struct Entry{
			size_t address;				///< the addres of the function
			char* function;				///< the name of the function
			unsigned int line;			///< the line of the function
			char* filename;				///< the name of the file, in which the function resides.
		};

		/// the maximum callstack depth (modify this if necessary)
		const static unsigned int MAX_STACK_DEPTH = 128;

		/// constructor
		/// @param max_depth	the maximum depth for the current callstack. e.g. 1 would just get the caller function information
		DebugToolsCallStack(unsigned int max_depth = MAX_STACK_DEPTH-1, bool demangle=true);
		~DebugToolsCallStack();
		DebugToolsCallStack(const DebugToolsCallStack&) = delete;
		DebugToolsCallStack(DebugToolsCallStack&&) = delete;
		DebugToolsCallStack& operator=(const DebugToolsCallStack&) = delete;
		DebugToolsCallStack& operator=(DebugToolsCallStack&&) = delete;

		/// returns a pointer to the callstack
		/// @return a pointer to the callstack
		const Entry* getCallStack() const;
		/// returns an entry from the callstack
		/// @param index the index of the entry, 0 is bottom of the stack.
		/// @return an entry from the the callstack
		const Entry& getCallStackEntry(unsigned int index) const;
		/// returns the current entries in the callstack
		/// @return the current entries in the callstack
		unsigned int getCallStackNumEntries() const;
		/// returns the id of the thread that generated the callstack
		/// @return the id of the thread that generated the callstack
		static size_t getThreadId();
		/// returns the id of the process that generated the callstack
		/// @return the id of the process that generated the callstack
		static size_t getProcessId();

		/// prints the callstack to pointer
		/// @param dst a pointer to an already allocated destination char array
		/// @param max_len the maximum length of dst
		/// @return true if writing was successful, false if otherwise (dst too small)
		bool print(char*& dst, size_t max_len) const;
		/// prints the callstack to an already opened FILE
		/// @param file the output file 
		void print(FILE* file) const;
		/// prints the callstack to the normal output
		void print() const;
	private:
		char* internalFilename(const char* src, size_t size);
		char* internalFunction(const char* src, size_t size);
		class Internal{
		public:
			Internal();
			~Internal();
		};
		const static char notavailablestr[];
		const static Internal singleton;
		static std::mutex mutex;
		unsigned int callstack_size;
		Entry* callstack;
	};

	


	
	#ifdef LAP_ASSERT
		#undef LAP_ASSERT
	#endif
	#ifdef LAP_NO_ASSERT
		#define LAP_ASSERT(condition , message) {}
	#else
		///--------------------------------------------------------------------------------------------
		/// custom assert, will print callstack
		#define LAP_ASSERT(condition, message)\
			if (!(condition)){\
				std::printf("---------------------------------------------------------------------------------------------------------\n");\
				std::cout << " ***[ASSERTION FAILURE]***"<<std::endl<<message<<std::endl;\
				lap::DebugToolsCallStack cs;\
				cs.print();\
				std::cout << "Press any key to terminate\n";\
				std::cin.get();\
				std::terminate(); \
			}
	#endif



	///--------------------------------------------------------------------------------------------
	/// a singleton heap debugger class
	class DebugToolsHeapTracker{
	private:

		struct Allocation{
			size_t address;
			size_t size;
			char* filename;
			char* function;
			unsigned int line;
			size_t thread_id;
			bool array;
			Allocation *next;
		};

		struct AllocationData {
			const unsigned int HASHTABLE_SIZE = 100000;
			AllocationData();
			~AllocationData();
			Allocation *createAllocation(size_t address, size_t size, const char* filename, const char* function, unsigned int line, size_t threadid, bool array);
			unsigned int hash(size_t address);
			void addAllocation(size_t address, size_t size, const char* filename, const char* function, unsigned int line, size_t threadid, bool array);
			bool removeAllocation(size_t address, const char* filename, const char* function, unsigned int line, size_t threadid, bool array);

			//local
			Allocation** hashtable;
			size_t allocations_size;
			unsigned int allocations_count;

			//bad deallocations (unmatched -> delete nothing, delete freed
			Allocation* bad_deallocations_first;
			Allocation* bad_deallocations_last;
			unsigned int bad_deallocations_count;
			// delete [] on new, delete on now []
			Allocation* bad_array_deallocations_first;
			Allocation* bad_array_deallocations_last;
			unsigned int bad_array_deallocations_count;

			//total (program duration)
			size_t allocations_total_size;
			unsigned int allocations_total_count;
			size_t allocations_total_freed_size;
			unsigned int allocations_total_freed_count;

			//free at exit?
			bool atexit_free;
		};

		DebugToolsHeapTracker();
		~DebugToolsHeapTracker();
		DebugToolsHeapTracker(const DebugToolsHeapTracker&) = delete;
		DebugToolsHeapTracker(DebugToolsHeapTracker&&) = delete;
		DebugToolsHeapTracker& operator=(const DebugToolsHeapTracker&) = delete;
		DebugToolsHeapTracker& operator=(DebugToolsHeapTracker&&) = delete;


	public:
		/// returns the unique instance of the debug tools heap tracker
		/// @return the unique instance of the debug tools heap tracker
		static DebugToolsHeapTracker& getInstance();
		
		const static unsigned int TRACKING_NONE = 1;		///< does not track memory at all
		const static unsigned int TRACKING_NORMAL = 2;		///< tracks leaks and reports basic leak information(address, allocation size) + bad deletes
		const static unsigned int TRACKING_FULL = 3;		///< normal + further leak/double deletion information such as filename, function, line and thread id

		/// sets the tracking mode
		/// mode can be have any of the following values:\n
		/// TRACKING_NONE   does not track memory at all\n
		/// TRACKING_NORMAL tracks leaks and reports basic leak information(address, allocation size) + bad deletes\n
		/// TRACKING_FULL   normal + further leak/double deletion information such as filename, function, line and thread id\n
		/// TRACKING_NORMAL is used by default
		/// @param mode the tracking mode ( TRACKING_NONE / TRACKING_NORMAL / TRACKING_FULL)
		void setTrackingMode(unsigned int mode = TRACKING_NORMAL);


		/// sets the program exit behavior
		/// by default the behavior is to report and to free the leaked pointers
		/// @param report if true a report will be done at program exit
		/// @param free	  if true the leaked pointers will be freed at program axit	
		void setAtexit(bool report = true, bool free = true);

		/// sets the reporting mode
		/// by default the report is full (all info) print on the stdout
		/// @param full true = full report (report all leaked allocations), false = (report only the existence of leaks and their total size)
		/// @param to_stdout report to stdout?, if true report will print to stdout
		/// @param filename report to filename?, if filename is not null than it is created and report writes 
		void setReport(bool full = true, bool to_stdout = true, const char* filename = nullptr);

		/// reports the heap tracking results
		/// the output depends on the tracking mode (set with setTrackingMode) and on the report parameters (set with setReport)
		void report();

		/// allocates memory
		/// this function should usually be called through the overloaded global new operators, albeit it can be called manually.
		/// @param count the amount of memory allocated
		void* allocate(size_t count, bool array);

		/// deallocates memory
		/// this function should usually be called through the overloaded global delete operators, albeit it can be called manually.
		/// @param count the amount of memory allocated
		void deallocate(void* ptr, bool array);

		/// returns the total size of allocations, for the entire duration of the program
		/// @return the total size of allocations, for the entire duration of the program
		size_t getAllocationsTotalSize();
		/// returns the total number of allocations, for the entire duration of the program
		/// @return the total number of allocations, for the entire duration of the program
		unsigned int getAllocationsTotalCount();
		/// returns the total size of freed allocations, for the entire duration of the program
		/// @return the total size of freed allocations, for the entire duration of the program
		size_t getAllocationsTotalFreedSize();
		/// returns the total number of freed allocations, for the entire duration of the program
		/// @return the total number of freed allocations, for the entire duration of the program
		unsigned int getAllocationsTotalFreedCount();

		/// returns the current size of allocations
		/// @return the current size of allocations
		size_t getAllocationsCurrentSize();
		/// returns the current number of allocations
		/// @return the current number of allocations
		unsigned int getAllocationsCurrentCount();

		/// returns the current number of not found/double deallocations
		/// @return the current number of not found/double deallocations
		unsigned int getNotFoundDeallocationsCount();

		/// returns the current number of unmatched array deallocations
		/// @return the current number of unmatched array deallocations
		unsigned int getUnmatchedArrayDeallocationsCount();

	private:
		static DebugToolsHeapTracker instance;
		std::mutex mutex;

		//state
		unsigned int tracking_mode;
		FILE* report_file;
		bool report_full;
		bool report_stdout;

		bool atexit_report;

		//allocation data
		AllocationData data;
	};

}


//include guard
#endif

