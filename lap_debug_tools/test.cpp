///---------------------------------------------------------------------------------------------------------------------
/// The MIT License (MIT)
/// 
/// Copyright (c) 2016 Lucian Petrescu, lucian (dot) petrescu (dot) 24 (at) gmail (dot) com
/// 
/// Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation 
/// files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, 
/// modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software 
/// is furnished to do so, subject to the following conditions:
/// 
/// The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
/// 
/// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
/// OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE 
/// LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR 
/// IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
///---------------------------------------------------------------------------------------------------------------------


#include <iostream>
#include <vector>
#include <string>
#include <map>
#include <set>
#include <list>
#include <thread>

#include "lap_debug_tools.hpp"

#if defined(_MSC_VER)
#pragma warning(push)
#pragma warning(disable : 4996)
#endif


void test_callstack_basic_unique_name(){
	lap::DebugToolsCallStack cs; 
	cs.print();
	std::cout << "At 2 "<<cs.getCallStack()[2].function << std::endl;
	std::cout << "At 4 " << cs.getCallStack()[4].line<<" "<<cs.getCallStack()[4].function << std::endl;
	lap::DebugToolsCallStack cs2(3); 
	cs2.print();
}
void test_callstack_basic_7() { test_callstack_basic_unique_name();}
void test_callstack_basic_6() { test_callstack_basic_7(); }
void test_callstack_basic_5() { test_callstack_basic_6(); }
void test_callstack_basic_4() { test_callstack_basic_5(); }
void test_callstack_basic_3() { test_callstack_basic_4(); }
void test_callstack_basic_2() { test_callstack_basic_3(); }
void test_callstack_basic_1() { test_callstack_basic_2(); }
void test_callstack_basic_0() { test_callstack_basic_1(); }
void test_callstack_basic()	  { test_callstack_basic_0(); }

void test_callstack_complex_7() {
	lap::DebugToolsCallStack cs;
	cs.print();
	std::cout << "At 2 " << cs.getCallStack()[2].function << std::endl;
	std::cout << "At 4 " << cs.getCallStack()[4].line << " " << cs.getCallStack()[4].function << std::endl;
}
void test_callstack_complex_6() {
	std::vector<int> somedata;
	for (int i = 0; i < 23;i++) somedata.push_back(i);
	test_callstack_complex_7(); 

}
void test_callstack_complex_5() { 
	int* val;
	test_callstack_complex_6(); 
	for (int i = 0; i < 24;i++) {
		val = (int*)malloc(sizeof(int) * 5);
		free(val);
	}
}
void test_callstack_complex_4() {
	test_callstack_complex_5();
	std::vector<int> somedata;
	for (int i = 0; i < 23;i++) somedata.push_back(i);
}
void test_callstack_complex_3() { 
	test_callstack_complex_4();
	for (int i = 0; i < 24;i++) {
		int* val = (int*)malloc(sizeof(int) * 5);
		free(val);
	}
}
void test_callstack_complex_2() { 
	test_callstack_complex_3(); 
	for (int i = 0; i < 24; i++) {
		int* val = (int*)malloc(sizeof(int) * 5);
		free(val);
	}
}
void test_callstack_complex_1() { test_callstack_complex_2(); }
void test_callstack_complex_0() { test_callstack_complex_1(); }
void test_callstack_complex() { test_callstack_complex_0(); }


std::mutex mutex;




void test_callstack_mt_0(FILE* f, int x, int y){
	std::this_thread::sleep_for(std::chrono::milliseconds(10));
	lap::DebugToolsCallStack cs(1); 
	std::lock_guard<std::mutex> l(mutex); 

	fprintf(f, "*************THREAD %zd %zd\n", cs.getThreadId(), cs.getProcessId());
	cs.print(f); 
}
void test_callstack_mt_1(FILE* f){
	std::this_thread::sleep_for(std::chrono::milliseconds(10));
	lap::DebugToolsCallStack cs(4);
	std::lock_guard<std::mutex> l(mutex);
	fprintf(f, "*************THREAD %zd %zd\n", cs.getThreadId(), cs.getProcessId());
	cs.print(f);
}
void test_callstack_mt_2(FILE* f){
	std::this_thread::sleep_for(std::chrono::milliseconds(10));
	lap::DebugToolsCallStack cs;
	std::lock_guard<std::mutex> l(mutex);
	fprintf(f, "*************THREAD %zd %zd\n", cs.getThreadId(), cs.getProcessId());
	cs.print(f);
}
void test_callstack_mt_3(FILE* f){
	std::this_thread::sleep_for(std::chrono::milliseconds(10));
	lap::DebugToolsCallStack cs;
	std::lock_guard<std::mutex> l(mutex);
	fprintf(f, "*************THREAD %zd %zd\n", cs.getThreadId(), cs.getProcessId());
	cs.print(f);
}
void test_callstack_mt_4(FILE* f){
	std::this_thread::sleep_for(std::chrono::milliseconds(10));
	lap::DebugToolsCallStack cs;
	std::lock_guard<std::mutex> l(mutex);
	fprintf(f, "*************THREAD %zd %zd\n", cs.getThreadId(), cs.getProcessId());
	cs.print(f);
}
void test_callstack_mt_5(FILE* f){
	std::this_thread::sleep_for(std::chrono::milliseconds(10));
	lap::DebugToolsCallStack cs;
	std::lock_guard<std::mutex> l(mutex);
	fprintf(f, "*************THREAD %zd %zd\n", cs.getThreadId(), cs.getProcessId());
	cs.print(f);
}

void test_callstack_mt_basic(){
	FILE *f = fopen("callstack.log", "w");
	const int COUNT = 6;
	std::thread *t[COUNT];
	for (int i = 0; i < COUNT; i++){
		if(i%6==0) t[i] = new std::thread(test_callstack_mt_0, f, 5, 23);
		if(i%6==1) t[i] = new std::thread(test_callstack_mt_1, f);
		if(i%6==2) t[i] = new std::thread(test_callstack_mt_2, f);
		if(i%6==3) t[i] = new std::thread(test_callstack_mt_3, f);
		if(i%6==4) t[i] = new std::thread(test_callstack_mt_4, f);
		if(i%6==5) t[i] = new std::thread(test_callstack_mt_5, f);
	}
	for (int i = 0; i < COUNT; i++) t[i]->join();
	for (int i = 0; i < COUNT; i++) delete t[i];
	fclose(f);
}


void test_callstack_mt_base(FILE *f, int m, int a){
	test_callstack_mt_0(f, m, a);
	test_callstack_mt_1(f);
	test_callstack_mt_2(f);
	test_callstack_mt_3(f);
	test_callstack_mt_4(f);
	test_callstack_mt_5(f);
}
void test_callstack_mt_start(FILE *f){
	test_callstack_mt_base(f, 2, 9);
}
void test_callstack_mt(){

	FILE *f = fopen("callstack.log", "w");
	const int COUNT = 5000;
	std::thread *t[COUNT];
	for (int i = 0; i < COUNT; i++) t[i] = new std::thread(test_callstack_mt_start, f);
	for (int i = 0; i < COUNT; i++) t[i]->join();
	for (int i = 0; i < COUNT; i++) delete t[i];
	fclose(f);
}
void test_callstack_print_3(){
	lap::DebugToolsCallStack cs; 
	std::cout << "***************************SIMPLE****************************" << std::endl;
	cs.print();
	std::cout << "***************************BUFFER****************************" << std::endl;
	std::vector<char> buffer(5000); char* ptr = &buffer[0];
	cs.print(ptr,buffer.size());
	std::cout << "***************************FILE****************************" << std::endl;
	FILE *f = fopen("log.log", "w");
	cs.print(f);
	fclose(f);
}
void test_callstack_print_2(){
	test_callstack_print_3();
}
void test_callstack_print_1(){
	test_callstack_print_2();
}
void test_callstack_st(){
	test_callstack_print_1();
}


void test_assert_3() { LAP_ASSERT(0 == 1, "the text for the assert, usually some broken assumption info here") }
void test_assert_2() { test_assert_3(); }
void test_assert_1() { test_assert_2(); }
void test_assert_0() { test_assert_1(); }
void test_assert() { test_assert_0(); }



void test_heap_basic_3() {
	int*p = new int[10];
	delete[]p;

	int*p2 = new int[10];
	delete[] p2;

	int*p3 = new int[10];
	delete[] p3;

	int*p4 = new int[10];
	delete[] p4;

	int* p5 = new int();
	delete p5;

	int* p6 = new int();
	delete p6;
}
void test_heap_basic_2() { test_heap_basic_3(); }
void test_heap_basic_1() { test_heap_basic_2(); }
void test_heap_basic_0() { test_heap_basic_1(); }
void test_heap_basic() {
	lap::DebugToolsHeapTracker::getInstance().setTrackingMode(lap::DebugToolsHeapTracker::TRACKING_NORMAL);
	lap::DebugToolsHeapTracker::getInstance().setReport(true, true, "heapdebugger.log");
	test_heap_basic_0(); 
	lap::DebugToolsHeapTracker::getInstance().report();
}

void test_heap_baddelete() {
	lap::DebugToolsHeapTracker::getInstance().setTrackingMode(lap::DebugToolsHeapTracker::TRACKING_FULL);
	lap::DebugToolsHeapTracker::getInstance().setReport(true, true, "heapdebugger.log");
	int*p = new int[10];
	delete p;
	delete p;
	delete[]p;
	delete p;

	lap::DebugToolsHeapTracker::getInstance().report();
}


void test_heap_leaks_free(int* p, int *p2, int *p3, int *p4, int *p5, int* p6, int *p7, int * p8) {
	// delete[] p;
	delete[] p2;		//GOOD
	delete[] p3;		//GOOD
	//delete[] p4;
	delete[] p3;		//BAD
	//delete[] p4;
	delete[] p3;		//BAD
	//delete p5
	delete p5;			//GOOD
	//delete p6
	delete[] p6;		//BAD
	//delete[] p7;
	delete p7;			//BAD
	delete p8;			//BAD empty
	delete[] p8;		//BAD empty
}
void test_heap_leaks_3() {
	int*p = new int[10];
	int*p2 = new int[10];
	int*p3 = new int[10];
	int*p4 = new int[10];
	int* p5 = new int();
	int* p6 = new int();
	int* p7 = new int[10];
	int* p8 = nullptr;
	test_heap_leaks_free(p, p2, p3, p4, p5, p6, p7, p8);
	
}
void test_heap_leaks_2() { test_heap_leaks_3(); }
void test_heap_leaks_1() { test_heap_leaks_2(); }
void test_heap_leaks_0() { test_heap_leaks_1(); }
void test_heap_leaks() {
	lap::DebugToolsHeapTracker::getInstance().setTrackingMode(lap::DebugToolsHeapTracker::TRACKING_FULL);
	lap::DebugToolsHeapTracker::getInstance().setReport(true, true, "heapdebugger.log");
	lap::DebugToolsHeapTracker::getInstance().setAtexit(true, true);
	test_heap_leaks_0();
	lap::DebugToolsHeapTracker::getInstance().report();
}


void test_heap_std2() {
	std::string fff = "42323";
	std::string ff2 = fff;

	std::vector<std::string> fffa;
	for (int i = 0; i < 1000; i++) {
		fffa.push_back(fff);
		fffa.push_back(std::string("some text") + std::to_string(i));
	}

	std::vector<std::string> a;
	for (int i = 0; i < 11225; i++) a.push_back("some text" + std::to_string(i));

	std::vector<float> f;
	f.resize(200);
	for (int i = 0; i < 110000; i++) f.push_back(i*2.0f);

	std::vector<std::string> aa;
	for (int i = 0; i < 11000; i++) aa.push_back("some text" + std::to_string(i));

	
	std::map<std::string, std::pair<std::string, std::string>> b;
	for (int i = 0; i < 15000; i++) b[std::to_string((i * 2334 + i*i) % 150)] = { "a", "b" };

	std::map<int, std::string> c;
	for (int i = 0; i < 11000; i++) c[i] = std::to_string(i);
	
}


void test_heap_std() {
	lap::DebugToolsHeapTracker::getInstance().setTrackingMode(lap::DebugToolsHeapTracker::TRACKING_FULL);
	lap::DebugToolsHeapTracker::getInstance().setReport(true, true, "heapdebugger.log");
	lap::DebugToolsHeapTracker::getInstance().setAtexit(true, true);
	test_heap_std2();
}

void test_heap_volume() {
	lap::DebugToolsHeapTracker::getInstance().setTrackingMode(lap::DebugToolsHeapTracker::TRACKING_FULL);
	lap::DebugToolsHeapTracker::getInstance().setReport(true, true, "heapdebugger.log");
	lap::DebugToolsHeapTracker::getInstance().setAtexit(true, true);
	class C { int num; float f; char c[5];  C* next; C* prev; };

	std::vector<C*> cs;
	for (int i = 0; i < 10000; i++) cs.push_back(new C());
	for (unsigned int i = 0; i < 10000; i++) {
		unsigned int index = ( i*i * 1432 + i * 4532 + 743 + i *2 +9) % 1000;
		if(cs[index]) delete cs[index];
		cs[index] = nullptr;
	}
	for (auto& e : cs) if(e) delete e;
}





int main(int argc, char* argv[]){
	
	//test_callstack_basic();
	//test_callstack_complex(); //shows inlining effects
	//test_callstack_st();
	//test_callstack_mt_basic();
	test_callstack_mt();
	//test_assert();

	//test_heap_basic();
	//test_heap_baddelete();
	//test_heap_leaks();
	//test_heap_std();
	//test_heap_volume();

	
	std::cout << "Finished" << std::endl;
	std::cin.get();
	return 0;
}
