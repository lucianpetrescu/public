///---------------------------------------------------------------------------------------------------------------------
/// The MIT License (MIT)
/// 
/// Copyright (c) 2016 Lucian Petrescu, lucian (dot) petrescu (dot) 24 (at) gmail (dot) com
/// 
/// Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation 
/// files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, 
/// modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software 
/// is furnished to do so, subject to the following conditions:
/// 
/// The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
/// 
/// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
/// OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE 
/// LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR 
/// IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
///---------------------------------------------------------------------------------------------------------------------

#include "lap_debug_tools.hpp"

namespace lap{
	///--------------------------------------------------------------------------------------------
	DebugToolsCallStack::Internal::Internal(){
	#if defined(_WIN32) || defined(_WIN64)
		SymSetOptions(SYMOPT_UNDNAME | SYMOPT_DEFERRED_LOADS | SYMOPT_LOAD_LINES | SYMOPT_INCLUDE_32BIT_MODULES);
		SymInitialize(GetCurrentProcess(), NULL, true);
	#endif
	}
	DebugToolsCallStack::Internal::~Internal(){
	#if defined(_WIN32) || defined(_WIN64)
		SymCleanup(GetCurrentProcess());
	#endif
	}

	//vars
	const DebugToolsCallStack::Internal DebugToolsCallStack::singleton;
	std::mutex DebugToolsCallStack::mutex;
	const char DebugToolsCallStack::notavailablestr[]= "not available\0";


#if defined(_MSC_VER)
#pragma warning(push)
#pragma warning(disable : 4996)
#endif

	char* DebugToolsCallStack::internalFilename(const char* src, size_t size) {
		char *result;
		if (src) {
			if (size == 0) size = std::strlen(src);
			result = (char*)std::malloc(size + 1);
			std::strncpy(result, src, size+1);
		}
		else {
			size = strlen(notavailablestr);
			result = (char*)std::malloc(size + 1);
			std::strncpy(result, notavailablestr, size + 1);
		}
		return result;
	}
	char* DebugToolsCallStack::internalFunction(const char* src, size_t size) {
		char *result;
		if (src) {
			if (size == 0) size = std::strlen(src);
			result = (char*)std::malloc(size + 1);
			std::strncpy(result, src, size + 1);
		}
		else {
			size = strlen(notavailablestr);
			result = (char*)std::malloc(size + 1);
			std::strncpy(result, notavailablestr, size +1);
		}
		return result;
	}

	DebugToolsCallStack::DebugToolsCallStack(unsigned int depth, bool demangle){
		std::lock_guard<std::mutex> lock(mutex);

		// storage array for stack trace address data
		void* backtrace_buffer[MAX_STACK_DEPTH]{};
		if (depth >= MAX_STACK_DEPTH) depth = MAX_STACK_DEPTH - 1;

	#if defined(_WIN32) || defined(_WIN64)
		HANDLE process_handle = GetCurrentProcess();

		//capture backtrace
		USHORT num_frames = CaptureStackBackTrace(1, depth, backtrace_buffer, NULL);	//don't show this (DebugToolsCallstack) function's name (the first in the stack)
		
		//translate the symbols loaded from the callstack frames to human readable information
		//symbols
		SYMBOL_INFO *symbol = (SYMBOL_INFO *)std::malloc(sizeof(SYMBOL_INFO)+(MAX_SYM_NAME+1) * sizeof(TCHAR));
		std::memset(symbol, 0, sizeof(SYMBOL_INFO)+(MAX_SYM_NAME+1)  * sizeof(TCHAR));
		symbol->MaxNameLen = MAX_SYM_NAME;
		symbol->SizeOfStruct = sizeof(SYMBOL_INFO);

		//symbol line
		IMAGEHLP_LINE64 *line = (IMAGEHLP_LINE64 *)std::malloc(sizeof(IMAGEHLP_LINE64));
		std::memset(line, 0, sizeof(IMAGEHLP_LINE64));
		line->SizeOfStruct = sizeof(IMAGEHLP_LINE64);
		DWORD line_displacement;

		// cut the following frames -> -6
		//00:	0x0000000076f2c520 not available											0     RtlUserThreadStart
		//01:	0x0000000076cf6520 not available											0     BaseThreadInitThunk
		//02:	0x000000013fa9dce0 f : \dd\vctools\crt\vcstartup\src\startup\exe_main.cpp	17    mainCRTStartup
		//03:	0x000000013fa9da10 f : \dd\vctools\crt\vcstartup\src\startup\exe_common.inl 309   __scrt_common_main
		//04:	0x000000013fa9da30 f : \dd\vctools\crt\vcstartup\src\startup\exe_common.inl 264   __scrt_common_main_seh
		//05:	0x000000013fa9dc90 f : \dd\vctools\crt\vcstartup\src\startup\exe_common.inl 75    invoke_main
		if(num_frames>6) callstack_size = num_frames-6;	//full?
		else callstack_size = num_frames;
		callstack = (Entry*)std::malloc(sizeof(Entry)*callstack_size);

		//walk the callstack
		for (unsigned int f = 0; f <callstack_size; f++) {
			SymFromAddr(process_handle, (DWORD64)(backtrace_buffer[callstack_size - f - 1]), 0, symbol);
			SymGetLineFromAddr64(process_handle, (DWORD64)(backtrace_buffer[callstack_size - f - 1]), &line_displacement, line);

			//adress
			callstack[f].address = (size_t)symbol->Address;

			//function
			callstack[f].function = internalFunction(symbol->Name, symbol->NameLen);

			//line
			callstack[f].line = (unsigned int)line->LineNumber;

			//filename
			callstack[f].filename = internalFilename(line->FileName, 0);
		}
		std::free(line);
		std::free(symbol);
	#elif defined(__gcc__) || defined(unix) || defined(__unix) || defined(__unix__) || defined(__linux__) || defined(__APPLE__) || defined(__MACH__)

		// retrieve current stack addresses
		int num_frames = backtrace(backtrace_buffer, sizeof(backtrace_buffer) / sizeof(void*));

		// resolve addresses into strings containing "filename(function+address)", this array must be free()-ed
		char** symbols = backtrace_symbols(backtrace_buffer, num_frames);

		// allocate string which will be filled with the demangled function name
		size_t funcnamesize = 256;
		char* funcname = (char*)std::malloc(funcnamesize);

		// iterate over the returned symbol lines. skip the first, it is the address of this function.
		if(num_frames > (int)depth) num_frames = depth+1;
		callstack_size = num_frames-1;
		callstack = (Entry*)std::malloc(sizeof(Entry)*callstack_size);
		//starting from 1 and writing to f-1 <=>jumping over the first index (this function)
		for (int f = 1; f<num_frames; f++) {
			int write_f = num_frames -1 - f;	//invert order (stack grows downwards) and jump over first entry (this func)

			char *begin_name = 0, *end_name =0, *begin_address = 0, *end_address = 0;

			// find parentheses and [address] surrounding the mangled name: ./module(function+0x15c) [0x8048a6d]
			for (char *p = symbols[f]; *p; ++p){
				if (*p == '(') begin_name = p;
				else if (*p == '+') end_name = p;
				else if (*p == '[') begin_address = p;
				else if (*p == ']'){
					end_address = p;
					break;
				}
			}

			if (begin_name && end_name && begin_address && end_address && end_address>begin_address && end_name>begin_name){
				*begin_name++ = '\0';
				*end_name = '\0';
				*begin_address++ = '\0';
				*end_address = '\0';

				// mangled name is now in [begin_name, begin_offset) and caller offset in [begin_offset, end_offset). now apply __cxa_demangle():
				int status;
				char* ret = abi::__cxa_demangle(begin_name,funcname, &funcnamesize, &status);
				//the demangle option is especially relevant for low cost non human readble traces (e.g. for heap debugger)
				if (status == 0 && demangle) {
					funcname = ret; // use possibly realloc()-ed string

					//create addr2line command
					char addr2line_cmd[512] = {0};
					#if defined(__GNUC__) && !defined(__clang__)
						//std::sprintf(addr2line_cmd,"atos -o %s %p", program_invocation_name, begin_address);
						std::sprintf(addr2line_cmd,"addr2line -f -p -e %s %s", program_invocation_name, begin_address);
						//open a pipe over the command
						FILE* fp = popen(addr2line_cmd, "r");					
					#else
						FILE* fp = nullptr;
					#endif
					if(fp!=NULL){
						char addr2line_output[512];
						std::fgets(addr2line_output, sizeof(addr2line_output)-1, fp);
						char* begin_line=0, *end_line=0, *begin_filename=0;
						
						// parse addr2line output ex: _Z21test_callstack_base_3v at /home/user621/Desktop/lap_debug_tools/test.cpp:30
						bool first_slash=true;
						for (char *p = addr2line_output; *p; ++p){
							if (*p == '/' && first_slash){
								begin_filename = p+1;
								first_slash=false;
							}else if (*p == ':'){
								*p='\0';
								begin_line = (p+1);
							}else if(*p == '\n'){
								*p = '\0';
								end_line = p;
							}
						}

						//adress
						callstack[write_f].address = (size_t)std::strtol(begin_address, &end_address, 16);

						//function
						callstack[write_f].function = internalFunction(funcname, 0);

						//line
						callstack[write_f].line = (unsigned int)std::strtol(begin_line, &end_line, 10);

						//filename
						callstack[write_f].filename = internalFilename(begin_filename, 0);

					}else{
						//print without lines and filename
						//adress
						callstack[write_f].address = (size_t)std::strtol(begin_address, &end_address, 16);

						//function
						callstack[write_f].function = internalFunction(funcname, 0);

						//line
						callstack[write_f].line = 0;

						//filename
						callstack[write_f].filename = internalFilename(nullptr, 0);
					}
				}
				else {
					// demangling failed. Output function name as a C function with no arguments.
					//adress
					callstack[write_f].address = (size_t)std::strtol(begin_address, &end_address, 16);

					//function
					callstack[write_f].function = internalFunction(begin_name, 0);

					//line
					callstack[write_f].line = 0;

					//filename
					callstack[write_f].filename = internalFilename(nullptr, 0);
				}
			}else{
				//kernel calls?
				if(begin_address && end_address && end_address>begin_address) callstack[write_f].address = (size_t)std::strtol(begin_address, &end_address, 16);
				else callstack[write_f].address = 0;
				if(begin_name && end_name && end_name>begin_name) callstack[write_f].function = internalFunction(begin_name, 0);
				else callstack[write_f].function = internalFunction(nullptr, 0);
				callstack[write_f].line = 0;
				callstack[write_f].filename = internalFilename(nullptr, 0);
			}
		}

		std::free(funcname);
		std::free(symbols);

		// mingw (no execinfo.h->backtrace and no -rdynamic), symbols must be on -rdynamic, can't run from gdb or valgrind __progname can cause portability problems (although it's well supported)
	#endif
	}
#if defined(_MSC_VER)
#pragma warning(pop)
#endif
	DebugToolsCallStack::~DebugToolsCallStack(){
		for (unsigned int i = 0; i < callstack_size; i++) {
			//can't be null
			std::free(callstack[i].filename);
			std::free(callstack[i].function);
		}
		std::free(callstack);
	}


	const DebugToolsCallStack::Entry* DebugToolsCallStack::getCallStack() const {
		return callstack;
	}
	const DebugToolsCallStack::Entry& DebugToolsCallStack::getCallStackEntry(unsigned int index) const {
		return callstack[index];
	}
	unsigned int DebugToolsCallStack::getCallStackNumEntries() const {
		return callstack_size;
	}
	size_t DebugToolsCallStack::getProcessId() {
	#if defined(_WIN32) || defined(_WIN64)
		return (int64_t)_getpid();
	#else
		return (int64_t)getpid();
	#endif	
	}
	size_t DebugToolsCallStack::getThreadId() {
	#if defined(_WIN32) || defined(_WIN64)
		return (int64_t)GetThreadId(GetCurrentThread());
	#else
		return (int64_t)pthread_self();
	#endif	
	}

	void DebugToolsCallStack::print() const{
		std::printf("--------------------------------CALLSTACK FOR THREAD %zd---------------------------------------\n", getThreadId());
		std::printf("Depth       ADDRESS           FILENAME                                LINE                 FUNCTION\n");
		for (unsigned int i = 0; i < callstack_size;i++) std::printf("%02u:        0x%016zx %-50s %-5u %s\n", i, callstack[i].address, callstack[i].filename, callstack[i].line, callstack[i].function);
	}
	bool DebugToolsCallStack::print(char*& dst, size_t max_len) const{
		int idx = std::snprintf(&dst[0], max_len, "--------------------------------CALLSTACK FOR THREAD %zd---------------------------------------\n", getThreadId());
		if (idx<0 || idx>(int)max_len) return false;
		idx += std::snprintf(&dst[idx], max_len -idx, "Depth       ADDRESS           FILENAME                                LINE                 FUNCTION\n");
		if (idx<0 || idx> (int)(max_len -idx)) return false;
		for (unsigned int i = 0; i < callstack_size; i++){
			idx += std::snprintf(&dst[idx], max_len - idx,"%02u:        0x%016zx %-50s %-5u %s\n", i, callstack[i].address, callstack[i].filename, callstack[i].line, callstack[i].function);
			if (idx<0 || idx> (int)(max_len - idx)) return false;
		}
		return true;
	}
	void DebugToolsCallStack::print(FILE* file) const{
		std::fprintf(file, "--------------------------------CALLSTACK FOR THREAD %zd---------------------------------------\n", getThreadId());
		std::fprintf(file,"Depth       ADDRESS           FILENAME                                LINE                 FUNCTION\n");
		for (unsigned int i = 0; i < callstack_size; i++) std::fprintf(file, "%02u:        0x%016zx %-50s %-5u %s\n", i, callstack[i].address, callstack[i].filename, callstack[i].line, callstack[i].function);
	}






	///--------------------------------------------------------------------------------------------
	DebugToolsHeapTracker::AllocationData::AllocationData() {
		//hashtable
		hashtable = (Allocation**)std::malloc(sizeof(Allocation*) * HASHTABLE_SIZE);
		for (unsigned int i = 0; i < HASHTABLE_SIZE; i++) hashtable[i] = nullptr;

		//local
		allocations_size = 0;
		allocations_count = 0;
		bad_deallocations_first = nullptr;
		bad_deallocations_last = nullptr;
		bad_deallocations_count = 0;
		bad_array_deallocations_first = nullptr;
		bad_array_deallocations_last = nullptr;
		bad_array_deallocations_count = 0;

		//total (program duration)
		allocations_total_size = 0;
		allocations_total_count = 0;
		allocations_total_freed_size = 0;
		allocations_total_freed_count = 0;
	}
	DebugToolsHeapTracker::AllocationData::~AllocationData() {
		//hashtable
		for (unsigned int i = 0; i < HASHTABLE_SIZE; i++) {
			for (Allocation* ptr = hashtable[i]; ptr; ) {
				if (ptr->filename) std::free(ptr->filename);
				if (ptr->function) std::free(ptr->function);

				// free dangling pointers (they are leaked)
				if(atexit_free)	std::free((void*)ptr->address);

				//next and kill self
				Allocation *currentptr = ptr;
				ptr = ptr->next;
				std::free(currentptr);
			}
		}
		std::free(hashtable);

		//bad deallocs
		for (Allocation* ptr = bad_deallocations_first; ptr; ) {
			if (ptr->filename) std::free(ptr->filename);
			if (ptr->function) std::free(ptr->function);

			Allocation* currentptr = ptr;
			ptr = ptr->next;
			std::free(currentptr);
		}

		//bad array deallocs
		for (Allocation* ptr = bad_array_deallocations_first; ptr; ) {
			if (ptr->filename) std::free(ptr->filename);
			if (ptr->function) std::free(ptr->function);

			Allocation* currentptr = ptr;
			ptr = ptr->next;
			std::free(currentptr);
		}

	}
	unsigned int DebugToolsHeapTracker::AllocationData::hash(size_t address) {
		// Thomas Wang's 64 bit uint hash, from http://naml.us/blog/tag/thomas-wang
		uint64_t key = address;
		key = (~key) + (key << 21);
		key = key ^ (key >> 24);
		key = (key + (key << 3)) + (key << 8);
		key = key ^ (key >> 14);
		key = (key + (key << 2)) + (key << 4);
		key = key ^ (key >> 28);
		key = key + (key << 31);
		return key % HASHTABLE_SIZE;
	}
#if defined(_MSC_VER)
#pragma warning(push)
#pragma warning(disable : 4996)
#endif
	DebugToolsHeapTracker::Allocation* DebugToolsHeapTracker::AllocationData::createAllocation(size_t address, size_t size, const char* filename, const char* function, unsigned int line, size_t threadid, bool array) {
		//allocate entry
		Allocation *result = (Allocation*)std::malloc(sizeof(Allocation));
		
		result->address = address;
		result->size = size;
		result->line = line;
		result->thread_id = threadid;
		result->array = array;
		result->next = nullptr;
		
		if (filename) {
			size_t length = strlen(filename) + 1;
			result->filename = (char*)std::malloc(length);
			std::strncpy(result->filename, filename, length);
		}
		else result->filename = nullptr;

		if (function) {
			size_t length = strlen(function) + 1;
			result->function = (char*)std::malloc(length);
			std::strncpy(result->function, function, length);
		}
		else result->function = nullptr;
		
		return result;
	}
#if defined(_MSC_VER)
#pragma warning(pop)
#endif
	void DebugToolsHeapTracker::AllocationData::addAllocation(size_t address, size_t size, const char* filename, const char* function, unsigned int line, size_t threadid, bool array) {

		//allocate entry
		Allocation *allocd = createAllocation(address, size, filename, function, line, threadid, array);

		//state
		this->allocations_size += size;
		this->allocations_count++;
		this->allocations_total_size += size;
		this->allocations_total_count++;

		//find hashtable key
		unsigned int key = hash(address);

		//first?
		if (!hashtable[key]) {
			hashtable[key] = allocd;
		}
		else {
			//last?
			for (Allocation* ptr = hashtable[key]; ptr; ptr = ptr->next) {
				if (!ptr->next) {
					ptr->next = allocd;
					break;
				}
			}
		}
	}
	bool DebugToolsHeapTracker::AllocationData::removeAllocation(size_t address, const char* filename, const char* function, unsigned int line, size_t threadid, bool array) {
		
		//find hashtable key
		unsigned int key = hash(address);

		//matches type of operator?
		bool array_unmatched = false;

		//try to find a matching entry
		if (hashtable[key]) {

			Allocation *cleanptr = nullptr;

			//search
			Allocation *prev = nullptr;
			for (Allocation* ptr = hashtable[key]; ptr; ptr = ptr->next) {
				if (ptr->address == address) {
					//match
					if (ptr->array == array) {
						
						//first?
						if (!prev) {
							cleanptr = ptr;
							hashtable[key] = ptr->next;
						}
						else {
							cleanptr = ptr;
							prev->next = ptr->next;
						}

					} //no match
					else {
						array_unmatched = true;
					}

					//either match or !match search is done.
					break;
				}
				prev = ptr;
			}


			//found a matching entry
			if (cleanptr) {
				//state
				allocations_size -= cleanptr->size;
				allocations_count--;
				allocations_total_freed_size += cleanptr->size;
				allocations_total_freed_count++;

				//free
				if (cleanptr->filename) std::free(cleanptr->filename);
				if (cleanptr->function) std::free(cleanptr->function);
				std::free(cleanptr);

				//done, removed
				return true;
			}
		}


		//couldn't find an exact match
		if (array_unmatched) {
			//not found bad array match, (new [] with delete) OR (new with delete[])
			Allocation *allocd = createAllocation(address, 0, filename, function, line, threadid, array);
			bad_array_deallocations_count++;
			if (!bad_array_deallocations_first) {
				bad_array_deallocations_first = allocd;
				bad_array_deallocations_last = allocd;
			}
			else {
				bad_array_deallocations_last->next = allocd;
				bad_array_deallocations_last = allocd;
			}
		}
		else {		
			//not found = bad delete (either there is nothing there or this is a double delete)
			Allocation *allocd = createAllocation(address, 0, filename, function, line, threadid, array);
			bad_deallocations_count++;
			if (!bad_deallocations_first) {
				bad_deallocations_first = allocd;
				bad_deallocations_last = allocd;
			}
			else {
				bad_deallocations_last->next = allocd;
				bad_deallocations_last = allocd;
			}
		}
		return false;
	}
	

	DebugToolsHeapTracker DebugToolsHeapTracker::instance;
	DebugToolsHeapTracker::DebugToolsHeapTracker() {
		//state
		tracking_mode = TRACKING_NORMAL;
		report_file = nullptr;
		report_full = true;
		report_stdout = true;

		data.atexit_free = true;
		atexit_report = true;
	}
	DebugToolsHeapTracker::~DebugToolsHeapTracker() {
		if(atexit_report) report();

		//close report file
		if (report_file) fclose(report_file);
	}

	void DebugToolsHeapTracker::report(){
		std::lock_guard<std::mutex> lock(mutex);
		const char* notavailable = "not available";

		if (report_stdout) {
			std::printf("---------------------------------------------------------------------------------------------------------\n");
			std::printf("--------------------------------------------- LAP LEAK DETECTOR -----------------------------------------\n");
			std::printf("---------------------------------------------------------------------------------------------------------\n");
			std::printf("------- Cumulative number of allocations   : %-6d , with a cumulative size of %zd bytes\n", data.allocations_total_count, data.allocations_total_size);
			std::printf("------- Cumulative number of deallocations : %-6d , with a cumulative size of %zd bytes\n", data.allocations_total_freed_count, data.allocations_total_freed_size);
			std::printf("-------\n");
			std::printf("------- Number of leaked allocations : %-6d , with a size of %zd bytes\n", data.allocations_count, data.allocations_size);
			std::printf("------- Number of not found/double deallocations : %d\n", data.bad_deallocations_count);
			std::printf("------- Number of unmatched array deallocations  : %d\n", data.bad_array_deallocations_count);
		}
		if (report_file) {
			std::fprintf(report_file, "---------------------------------------------------------------------------------------------------------\n");
			std::fprintf(report_file, "--------------------------------------------- LAP LEAK DETECTOR -----------------------------------------\n");
			std::fprintf(report_file, "---------------------------------------------------------------------------------------------------------\n");
			std::fprintf(report_file, "------- Cumulative number of allocations   : %-6d , with a cumulative size of %zd bytes\n", data.allocations_total_count, data.allocations_total_size);
			std::fprintf(report_file, "------- Cumulative number of deallocations : %-6d , with a cumulative size of %zd bytes\n", data.allocations_total_freed_count, data.allocations_total_freed_size);
			std::fprintf(report_file, "-------\n");
			std::fprintf(report_file, "------- Number of leaked allocations : %-6d , with a size of %zd bytes\n", data.allocations_count, data.allocations_size);
			std::fprintf(report_file, "------- Number of not found/double deallocations : %d\n", data.bad_deallocations_count);
			std::fprintf(report_file, "------- Number of unmatched array deallocations  : %d\n", data.bad_array_deallocations_count);
		}

		if (report_full) {
			//report memory leaks
			if (data.allocations_count > 0) {
				if (report_stdout) std::printf("---------------------- MEMORY LEAKS DETECTED:\n");
				if (report_file) std::fprintf(report_file, "---------------------- MEMORY LEAKS DETECTED:\n");

				bool first = true;
				for (unsigned int i = 0; i < data.HASHTABLE_SIZE; i++) {
					for (Allocation* ptr = data.hashtable[i]; ptr; ptr = ptr->next) {
						const char* filename = notavailable;
						if (ptr->filename) filename = ptr->filename;
						const char* function = notavailable;
						if (ptr->function) function = ptr->function;
						if (report_stdout) {
							if (!first) std::printf("-       ..............\n");
							std::printf("-\tAddress   = %zd\n-\tSize      = %zd\n-\tFilename  = %s\n-\tFunction  = %s\n-\tLine      = %d\n-\tThread id = %zd\n-\tArray     = %s\n", ptr->address, ptr->size, filename, function, ptr->line, ptr->thread_id, ptr->array ? "yes" : "no");
						}
						if (report_file) {
							if (!first) std::fprintf(report_file, "-       ..............\n");
							std::fprintf(report_file, "-\tAddress   = %zd\n-\tSize      = %zd\n-\tFilename  = %s\n-\tFunction  = %s\n-\tLine      = %d\n-\tThread id = %zd\n-\tArray     = %s\n", ptr->address, ptr->size, filename, function, ptr->line, ptr->thread_id, ptr->array ? "yes" : "no");
						}
						first = false;
					}
				}
			}


			//report unmatched / double deletes
			if (data.bad_deallocations_count > 0) {
				if (report_stdout) std::printf("---------------------- NOT FOUND/DOUBLE DEALLOCATIONS DETECTED:\n");
				if (report_file) std::fprintf(report_file, "---------------------- NOT FOUND/DOUBLE DEALLOCATIONS DETECTED:\n");

				bool first = true;
				for (Allocation* ptr = data.bad_deallocations_first; ptr; ptr = ptr->next) {
					const char* filename = notavailable;
					if (ptr->filename) filename = ptr->filename;
					const char* function = notavailable;
					if (ptr->function) function = ptr->function;
					if (report_stdout) {
						if (!first) std::printf("-       ..............\n");
						std::printf("-\tAddress   = %zd\n-\tFilename  = %s\n-\tFunction  = %s\n-\tLine      = %d\n-\tThread id = %zd\n-\tArray     = %s\n", ptr->address, filename, function, ptr->line, ptr->thread_id, ptr->array ? "yes" : "no");
					}
					if (report_file) {
						if (!first) std::fprintf(report_file, "-       ..............\n");
						std::fprintf(report_file, "-\tAddress   = %zd\n-\tFilename  = %s\n-\tFunction  = %s\n-\tLine      = %d\n=\tThread id = %zd\n-\tArray     = %s\n", ptr->address, filename, function, ptr->line, ptr->thread_id, ptr->array ? "yes" : "no");
					}
					first = false;
				}
			}

			//report unmatched array deletes
			if (data.bad_array_deallocations_count > 0) {
				if (report_stdout) std::printf("---------------------- UNMATCHED ARRAY DEALLOCATIONS DETECTED:\n");
				if (report_file) std::fprintf(report_file, "---------------------- UNMATCHED ARRAY DEALLOCATIONS DETECTED:\n");

				bool first = true;
				for (Allocation* ptr = data.bad_array_deallocations_first; ptr; ptr = ptr->next) {
					const char* filename = notavailable;
					if (ptr->filename) filename = ptr->filename;
					const char* function = notavailable;
					if (ptr->function) function = ptr->function;
					if (report_stdout) {
						if (!first) std::printf("-       ..............\n");
						std::printf("-\tAddress   = %zd\n-\tFilename  = %s\n-\tFunction  = %s\n-\tLine      = %d\n-\tThread id = %zd\n-\tArray     = %s\n", ptr->address, filename, function, ptr->line, ptr->thread_id, ptr->array ? "yes" : "no");
					}
					if (report_file) {
						if (!first) std::fprintf(report_file, "-       ..............\n");
						std::fprintf(report_file, "-\tAddress   = %zd\n-\tFilename  = %s\n-\tFunction  = %s\n-\tLine      = %d\n-\tThread id = %zd\n-\tArray     = %s\n", ptr->address, filename, function, ptr->line, ptr->thread_id, ptr->array ? "yes" : "no");
					}
					first = false;
				}
			}
		}

		if (report_stdout) std::printf("---------------------------------------------------------------------------------------------------------\n");
		if (report_file) std::fprintf(report_file, "---------------------------------------------------------------------------------------------------------\n");
	}

	DebugToolsHeapTracker& DebugToolsHeapTracker::getInstance() {
		return instance;
	}
	
	void DebugToolsHeapTracker::setTrackingMode(unsigned int mode) {
		std::lock_guard<std::mutex> lock(mutex);
		tracking_mode = mode;
	}


	void DebugToolsHeapTracker::setAtexit(bool rep, bool free) {
		atexit_report = rep;
		data.atexit_free = free;
	}

	void DebugToolsHeapTracker::setReport(bool full, bool to_stdout, const char* filename) {
		std::lock_guard<std::mutex> lock(mutex);
		report_full = full;
		report_stdout = to_stdout;
		report_file = nullptr;
		if (filename) {
			#if defined(_MSC_VER)
			fopen_s(&report_file, filename, "w");
			#else
			report_file = std::fopen(filename, "w");
			#endif
		}
	}


	void* DebugToolsHeapTracker::allocate(size_t count, bool array) {
		std::lock_guard<std::mutex> lock(mutex);

		//allocate memory
		void* result = std::malloc(count);

		//tracking
		if(tracking_mode == TRACKING_NONE) return result;
		else if (tracking_mode == TRACKING_NORMAL) {
			data.addAllocation(reinterpret_cast<size_t>(result), count, nullptr, nullptr, 0, 0, array);
		}
		else if (tracking_mode == TRACKING_FULL) {
			lap::DebugToolsCallStack cs(3, true);
			const lap::DebugToolsCallStack::Entry& e = cs.getCallStackEntry(0);
			data.addAllocation(reinterpret_cast<size_t>(result), count, e.filename, e.function, e.line, cs.getThreadId(), array);
		}

		return result;
	}

	void DebugToolsHeapTracker::deallocate(void* ptr, bool array) {
		std::lock_guard<std::mutex> lock(mutex);

		//tracking
		if (tracking_mode == TRACKING_NONE) {
			std::free(ptr);
		}
		else if (tracking_mode == TRACKING_NORMAL) {
			bool result = data.removeAllocation(reinterpret_cast<size_t>(ptr), nullptr, nullptr, 0, 0, array);
			//free only if this is a good delete
			if (result) std::free(ptr);
		}
		else if (tracking_mode == TRACKING_FULL) {
			lap::DebugToolsCallStack cs(3, true);
			const lap::DebugToolsCallStack::Entry& e = cs.getCallStackEntry(0);
			bool result = data.removeAllocation(reinterpret_cast<size_t>(ptr), e.filename, e.function, e.line, cs.getThreadId(), array);
			//free only if this is a good delete
			if (result) std::free(ptr);
		}
	}

	size_t DebugToolsHeapTracker::getAllocationsTotalSize() {
		std::lock_guard<std::mutex> lock(mutex);
		return data.allocations_total_size;
	}

	unsigned int DebugToolsHeapTracker::getAllocationsTotalCount() {
		std::lock_guard<std::mutex> lock(mutex);
		return data.allocations_total_count;
	}

	size_t DebugToolsHeapTracker::getAllocationsTotalFreedSize() {
		std::lock_guard<std::mutex> lock(mutex);
		return data.allocations_total_freed_size;
	}

	unsigned int DebugToolsHeapTracker::getAllocationsTotalFreedCount() {
		std::lock_guard<std::mutex> lock(mutex);
		return data.allocations_total_freed_count;
	}

	size_t DebugToolsHeapTracker::getAllocationsCurrentSize() {
		std::lock_guard<std::mutex> lock(mutex);
		return data.allocations_size;
	}

	unsigned int DebugToolsHeapTracker::getAllocationsCurrentCount() {
		std::lock_guard<std::mutex> lock(mutex);
		return data.allocations_count;
	}

	unsigned int DebugToolsHeapTracker::getNotFoundDeallocationsCount() {
		std::lock_guard<std::mutex> lock(mutex);
		return data.bad_deallocations_count;
	}
	unsigned int DebugToolsHeapTracker::getUnmatchedArrayDeallocationsCount() {
		std::lock_guard<std::mutex> lock(mutex);
		return data.bad_array_deallocations_count;
	}
	
}

//-------------------------------------------------------------------------------------------------
//overload new and delete signatures
void* operator new(std::size_t count){
	return lap::DebugToolsHeapTracker::getInstance().allocate(count, false);
}
void* operator new(std::size_t count, const std::nothrow_t& tag) noexcept{
	return lap::DebugToolsHeapTracker::getInstance().allocate(count, false);
}
void* operator new[](std::size_t count) {
	return lap::DebugToolsHeapTracker::getInstance().allocate(count, true);
}
void* operator new[](std::size_t count, const std::nothrow_t& tag) noexcept{
	return lap::DebugToolsHeapTracker::getInstance().allocate(count, true);
}
void operator delete(void* ptr){
	lap::DebugToolsHeapTracker::getInstance().deallocate(ptr, false);
}
void operator delete[](void* ptr) {
	lap::DebugToolsHeapTracker::getInstance().deallocate(ptr, true);
}
void operator delete  (void* ptr, const std::nothrow_t& tag) noexcept {
	lap::DebugToolsHeapTracker::getInstance().deallocate(ptr, false);
}
void operator delete[](void* ptr, const std::nothrow_t& tag) noexcept{
	lap::DebugToolsHeapTracker::getInstance().deallocate(ptr, true);
}

//cpp14, vs2015 uses these functions.
void operator delete  (void* ptr, std::size_t sz) {
	lap::DebugToolsHeapTracker::getInstance().deallocate(ptr, false);
}
void operator delete[](void* ptr, std::size_t sz) {
	lap::DebugToolsHeapTracker::getInstance().deallocate(ptr, true);
}

