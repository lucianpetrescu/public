﻿///---------------------------------------------------------------------------------------------------------------------
/// The MIT License (MIT)
/// 
/// Copyright (c) 2016 Lucian Petrescu, lucian (dot) petrescu (dot) 24 (at) gmail (dot) com
/// 
/// Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation 
/// files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, 
/// modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software 
/// is furnished to do so, subject to the following conditions:
/// 
/// The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
/// 
/// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
/// OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE 
/// LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR 
/// IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
///---------------------------------------------------------------------------------------------------------------------

///------------------------------------------------------------------------------------------------
/// @file
/// @mainpage LAP OpenGL DebugContext
///
/// DebugContext is a debugging tool for OpenGL based programs. It is written for the OpenGL Core 4.5 profile but it also includes 
/// previous compatibility functions. This tool is useful for tracking the OpenGL state (which buffers are created, which sampler is
/// bound to which unit, which shaders are attached to which program, etc), OpenGL errors and OpenGL stats (memory usage, number of
/// commands, state changes, etc) and logging them into a user provided stream.
///
/// While lacking the functionality of established software such as glDebugger, glIntercept and so on, LAP GL DebugContext is friendlier
/// towards programmers, by giving programmable access into the application (callbacks on errors, stats for code sections, etc), minimizing
/// scope creep. This feature is very valuable when debugging large applications where only a tiny portion of the code is of interest. 
/// The OpenGL functions are not intercepted through hooking but through define redefinition (e.g. #undef glClear, #define glClear .... )
/// LAP GL DebugContext is compatible with both macro-based and function-based OpenGL function loaders, and should be included just after
/// the OpenGL function loader. 
///
/// @par What is this software useful for?
///
/// - debugging OpenGL programs
/// - optimization of OpenGL programs
/// - teaching OpenGL in depth
/// - monitoring OpenGL resource usage
/// - logging OpenGL commands
///
/// @par Properties
///
///	- c++11, no exceptions
///	- overrides normal OpenGL calls (a DebugContext object has to be bound and LAP_GL_CONTEXT_OVERRIDE_FUNCTIONS macro has to be defined)
///	- written for 4.5 core (thus nu extra core extensions such as sparse buffers/textures)
///	- intentionally not thread safe (like OpenGL)
///	- does not overwrite the OpenGL function loader, just plug and play
///	- easy debugging and state inspection, full core state except default texture objects (which should never be used..)
///	- offers various parameters which control what work is performed and which state is tracked
///		- safety_checks	use extensive safety checks? (will call glGetError after each GL call and will call the debug callback on GL errors)
///		- keep_state		maintain an internal state machine (almost complete, some things (glsl compiler related) can only be queried through GL)
///		- compute_stats		compute statistics, many memory stats are computed only if keep_state = true, redundancy stats are computed only if redundancy_checks = true
///		- redundancy_checks check redundancies? (will call the debug callback on warnings), if true it automatically sets keep_state=true
///		- leak_checks		check for leaks on context destruction (will call the debug callback on leaks), if true it automatically sets keep_state=true
///		- logger			log all OpenGL commands to this stream
///	- if no options are selected then DebugContext is just a minimal wrapper of the OpenGL functions.
///	- provides command groups (e.g. draw commands, buffer commands, etc)
///		- logging per command group
///	- provides a debug callback even if debug is not supported
///	- stats
///		- state switches
///		- draw calls
///		- query commands
///		- memory usage and stats
///		- synchronization commands
///		- gpu-cpu and gpu-gpu transfers
///		- etc
///	- inside context the spec-style naming is used (e.g. CullFace, Hint, etc)
///	- cleans up defines, can exclude compatibility functions and defines
///	- file / func / line on each OpenGL call (not available on direct class calls)
///	- descriptors are created on generation and creation (the name is reserved, the descriptor is contains no other data or size)
///	- indexed operations intentionally crash if out of bounds. No checks besides internal access checks. OpenGL errors 
///	  are preferrable to cpp asserts, so DebugContext asserts only where it is necessary (object found/not found)
///	- cpu data transfers assume that the gpu will make a local copy (thus multichannel images loaded from the cpu are loaded entirely,
///   not per-channel). Since many drivers do this, a convservative approximation should be a better choice.
///	- large parts of interface and implementation are script generated, but logic and advanced stats are manually implemented
///
/// @par Limitations
///
/// - As there is no OpenGL api for context switching (the api is platform based and EGL is still very recent and not widely supported), 
/// multiple contexts can be supported through manual context switching and assignment, see 
///		- void setDebugContext(DebugContext* context)
///		- DebugContext* getDebugContext();
/// Basically the user creates a DebugContext object for each context and sets the current DebugContext object when the OpenGL context is changed.
/// This behavior is compliant to OpenGL principles, where only one context per thread can be active. 
/// - The DebugContext object has zero thread safety, as the OpenGL driver is singlethreaded and will synchronize commands from concurrent contexts 
/// on different threads. Therefore, manual synchronization from the user is required as it is with OpenGL.
/// - State is kept only for the core functions, therefore there are no state/stats/redundancy checks/etc for deprecated functions.
/// - a very small number of default objects (the default textures) are not maintained. It has to be noted that such objects should never be used
/// in production code and that they are remnants of deprecated OpenGL.
///
/// @par The Anatomy of a Call
///
///		Type0 DebugContext::FunctionName(Type1 arg1, Type2 arg2, Type3 arg3, const char* file, const char* func, unsigned int line) {
///			Type0 result = glFunctionName(arg1, arg2, arg3);
///			if (no options) return result;	// quick return if no options are provided 
///			if (logging || safety checks || redundancy checks) {
///				create result string
///				create arguments string
///			}
///			if (logging ) do logging
///			if (safety checks) do safety checks
///			if (stats) compute non-memory related stats (e.g. num commands)
///			if (state){
///				maintain state
///				if(stats) compute memory and state based stats (e.g. gpu-cpu transfer size)
///			}
///			return result;
///		}
///		#undef glFunctionName
///		#define glFunctionName (...) lap::gl::detail::context->FunctionName(__VA_ARGS__, __FILE__, __func__, __LINE__)
///
/// @version 0.36
/// @author Lucian Petrescu( lucian (dot) petrescu (dot) 24 (at) gmail (dot) com )
///
/// @par Compiling and linking
///
///	- Windows:
///		- no special setup for visual studio projects
///		- -pthread -lgdi32 otherwise
///		- an example visual studio project+solution is provide in the vstudio folder
///	- Linux:
///		- -ldl -lGL -lX11 -lXxf86vm -lXrandr -lXi -lXinerama -lXcursor -lrt -lm
///		- the wayland and mir backends are not used by lapwic (but they can easily be added in the future when they mature, right now they are experimental in glfw)
///		- an example makefile is provided in the root folder
///		- to install the project dependencies get:
///			- sudo apt-get install xorg-dev
///			- sudo apt-get install libgl-dev
///		- also works in a VMWARE virtual machine, but needs a special mesa driver http://mesa3d.org/vmware-guest.html.
///	- OSX
///		- -framework Cocoa -framework OpenGL -framework IOKit -framework CoreVideo -framework CoreFoundation -framework Carbon
///		- an example makefile is provided in the root folder\n
///		
///
/// @par Usage:
///	
///	- basic usage
///		@code
///		... include function loader header ...
///		#define LAP_GL_CONTEXT_OVERRIDE_FUNCTIONS
///		#include "lap_gl_debug_context.hpp"
///		...
///		... create OpenGL context...
///		...
///		lap::gl::DebugContext dc(true, true, true, true, true, &std::cout, true, true, true);	// queries the native OpenGL context and creates a Context object with identical properties and blank state
///		dc.setCurrent(true);															// set the newly created context as the current context
///		auto callback = [](const std::string& function, const std::string& result, const std::string& parameters, GLenum errorcode, const char* file, const char* func, unsigned int line) {
///			std::cout << "           **" << ((errorcode == GL_NONE) ? "ERROR" : "WARNING") << " from function " << function << "(" << parameters << ")";
///			if (result.size() > 0) std::cout << " = " << result << " ";
///			std::cout << " errorcode = " << errorcode << std::endl;
///			if (file) std::cout << "           **call came from FILE=" << file;
///			if (func) std::cout << " FUNC=" << func;
///			if (line) std::cout << " LINE=" << line;
///			std::cout << std::endl;
///		};
///		dc.setCallbackDebug(callback);	///< will get debug messages on this callback (anything capturable by a std::function can be placed here)
///		//usage example #1 : call OpenGL functions directly through the Context object --BUT filename, function name and line will be BLANK--
///		dc.BlendEquation(GL_FUNC_REVERSE_SUBTRACT);			//goes through the DebugContext
///		//usage example #2 : call global OpenGL functions which are overrided with #define LAP_GL_DEBUG_CONTEXT_OVERRIDE_FUNCTIONS . Doesn't work without this define.
///		glBlendEquation(GL_FUNC_ADD);						//goes through the DebugContext
///		@endcode
///
///	- function groups
///		@code
///		... include function loader header ...
///		#define LAP_GL_CONTEXT_OVERRIDE_FUNCTIONS
///		#include "lap_gl_debug_context.hpp"
///		...
///		... create OpenGL context...
///		...
///		lap::gl::DebugContext dc(true, true, true, true, true, &std::cout, true, true, true);	// queries the native OpenGL context and creates a Context object with identical properties and blank state
///		dc.setCurrent(true);															// set the newly created context as the current context
///		auto names = dc.getFunctionGroupNames(lap::gl::DebugContext::GROUP::V30);
///		std::cout << "These functions were introduced by OpenGL v30:" << std::endl;
///		for (const auto& s : names) std::cout << s << std::endl;
///		#ifdef LAP_GL_DEBUG_CONTEXT_COMPATIBILITY
///		names = dc.getFunctionGroupNames(lap::gl::DebugContext::GROUP::V14_DEPRECATED);
///		std::cout << "These functions were introduced by OpenGL v14, but are now deprecated:" << std::endl;
///		for (const auto& s : names) std::cout << s << std::endl;
///		#endif
///		@endcode
///
///	- logging
///		@code
///		... include function loader header ...
///		#define LAP_GL_CONTEXT_OVERRIDE_FUNCTIONS
///		#include "lap_gl_debug_context.hpp"
///		...
///		... create OpenGL context...
///		...
///		lap::gl::DebugContext dc(true, true, true, true, true, &std::cout, true, true, true);	// queries the native OpenGL context and creates a Context object with identical properties and blank state
///		dc.setCurrent(true);															// set the newly created context as the current context
///		glBlendEquationSeparate(GL_FUNC_ADD, GL_FUNC_SUBTRACT);		//not logged (no ostream!)
///		dc.setLogger(&std::cout);
///		glBlendEquationSeparatei(0, GL_FUNC_ADD, GL_FUNC_SUBTRACT);	//logged (by default all groups are logged if a log ostream is provided)
///		dc.setLogGroup(DebugContext::GROUP::BLEND, false);			//disable logging of blend functions
///		glBlendColor(0.5f, 0.5f, 1.0f, 0.0f);						//not logged (disabled)
///		dc.setLogGroup(DebugContext::GROUP::BLEND, true);			//re-enable logging of blend functions
///		glBlendColor(0.9f, 0.9f, 0.9f, 0.9f);						//logged (enabled)
///		dc.setLogger(&std::cout, false, false, false);				//disable __FILE__, __LINE__, __func__
///		glBlendFunc(GL_ZERO, GL_ONE);								//logged
///		glBlendFunci(3, GL_ONE_MINUS_SRC_ALPHA, GL_DST_ALPHA);		//logged
///		glBlendFuncSeparate(GL_ZERO, GL_ONE_MINUS_DST_ALPHA, GL_ONE, GL_ONE_MINUS_SRC_COLOR);	//logged
///		glBlendFuncSeparatei(0, GL_ZERO, GL_ONE_MINUS_DST_ALPHA, GL_ONE, GL_ONE_MINUS_SRC_COLOR);//logged
///		@endcode
///
///	- stats and state
///		@code
///		... include function loader header ...
///		#define LAP_GL_CONTEXT_OVERRIDE_FUNCTIONS
///		#include "lap_gl_debug_context.hpp"
///		...
///		... create OpenGL context...
///		...
///		lap::gl::DebugContext dc(true, true, true, true, true, &std::cout, true, true, true);	// queries the native OpenGL context and creates a Context object with identical properties and blank state
///		dc.setCurrent(true);															// set the newly created context as the current context
///		...
///		... do some OpenGL work ...
///		...
///		auto& state = dc.getState()
///		auto& stats = dc.getStats()
///		std::cout << stats.commands.all << " OpenGL commands, of which:" << std::endl;
///		std::cout << stats.commands.attribute << " attribute commands" << std::endl;
///		std::cout << stats.commands.blend << " blend commands" << std::endl;
///		std::cout << stats.commands.buffer << " buffer commands" << std::endl;
///		...
///		@endcode
///------------------------------------------------------------------------------------------------

#ifndef LAP_GL_DEBUG_CONTEXT_H_
#define LAP_GL_DEBUG_CONTEXT_H_


///------------------------------------------------------------------------------------------------
///								THIS SECTION IS IMPORTANT, PLEASE READ IT
///------------------------------------------------------------------------------------------------
/// using lap_wic ONLY for opengl related tokens and functions
/// this can be changed to fit user needs, the code will work with any OpenGL extension loaders which exposes glXXXXXXXX macros or global functions (e.g. glew, glad, gl3w, glLoadGen, glsdk, glbinding, etc)
#include "dep/lap_wic.hpp"
/// uncomment the next line to enable deprecated OpenGL (otherwise the context works only for core functions)
/// this is included because the deprecated functions generate a large number of functions, burdening the compiler and intelligent code completion.
#define LAP_GL_DEBUG_CONTEXT_COMPATIBILITY
/// uncomment the next line to enable the deprecated ARB_imaging core-extension (many loaders don't load these functions)
/// NOTE: some gl function loaders consider these functions as extensions to OpenGL 2.1 
//#define LAP_GL_DEBUG_CONTEXT_COMPATIBILITY_IMAGING
///------------------------------------------------------------------------------------------------




#include <string>
#include <vector>
#include <ostream>
#include <functional>
#include <sstream>
#undef MemoryBarrier
#undef max
#undef min

namespace lap {
	namespace gl {

		class DebugContext;
		namespace detail {
			extern DebugContext* context;
		}
		///-----------------------------------------------------------------------------------------------------------------------
		/// sets the debug context (can be used to work with multiple contexts)
		/// @param context	the debug context
		void setDebugContext(DebugContext* context);
		///-----------------------------------------------------------------------------------------------------------------------
		/// returns the current debug context
		/// @return the current debug context or nullptr if none is bound
		DebugContext* getDebugContext();

		///---------------------------------------------------------------------------------------------------------------------------
		/// buffer descriptor
		struct DescBuffer {
			GLuint id = 0;						///< buffer id/name
			GLboolean created = GL_FALSE;		///< has the object been created? (OpengGL can generate objects but not create them)
			GLsizeiptr size = 0;				///< size in bytes
			GLenum usage = GL_STATIC_DRAW;		///< usage hint
			GLboolean immutable = GL_FALSE;		///< immutable?
			GLbitfield flags = 0;				///< buffer storage flags (0 if none was set)
		};

		/// framebuffer descriptor (needs an active OpenGL context)
		struct DescFramebuffer {
			DescFramebuffer();
			GLuint id = 0;						///< framebuffer id/name
			GLboolean created = GL_FALSE;		///< has the object been created? (OpengGL can generate objects but not create them)
			/// framebuffer bind
			struct Bind {
				GLint id =0;					///< texture or renderbuffer id
				GLint level =0;					///< mipmap level of texture
				GLint layer = 0;				///< the 2D layer in a 3D texture
				GLenum target = GL_NONE;		///< target or target of texture (e.g. GL_RENDERBUFFER, GL_TEXTURE_2D, GL_TEXTURE_CUBEMAP_POSITIVE_X
				GLint color_encoding = GL_LINEAR;///< the encoding of color buffer
			};
			std::vector<GLenum> draw_buffers;///< which fragment drawbuffers is linked to which color attachment (GL_NONE / GL_COLOR_ATTACHMENTx)?
			std::vector<Bind> color_buffer;		///< color buffer attachments (GL ids/names)
			Bind depth_buffer;					///< depth buffer GL id/name (0 if none)
			Bind stencil_buffer;				///< stencil buffer GL id/name(0 if none)
			/// framebuffer parameters
			struct Parameters{
				GLuint default_width =0;			///< default width
				GLuint default_height =0;			///< default height
				GLuint default_layers =0;			///< default layers
				GLuint default_samples =0;			///< default samples
				GLboolean default_fixed_sample_locations = GL_FALSE;			///< default sample locations
			}parameters;							///< framebuffer parameters
		};

		/// renderbuffer descriptor
		struct DescRenderbuffer {
			GLboolean created = GL_FALSE;		///< has the object been created? (OpengGL can generate objects but not create them)
			GLuint id = 0;						///< renderbuffer id/name
			GLint width = 0;					///< width
			GLint height = 0;					///< height
			GLenum internal_format = GL_NONE;	///< internal format
			GLint samples = 0;;					///< num samples
			GLint red_bits = 0;					///< red size
			GLint green_bits = 0;				///< green size
			GLint blue_bits = 0;				///< blue size
			GLint alpha_bits = 0;				///< alpha size
			GLint depth_bits = 0;				///< depth size
			GLint stencil_bits = 0;				///< stencil size
			GLfloat bps =0;						///< bits per sample
			GLint size =0;						///< size of renderbuffer
		};

		/// shader descriptor
		struct DescShader {
			GLuint id = 0;						///< shader id/name
			GLboolean compile_status = GL_FALSE;///< compile status
			GLenum type = GL_NONE;				///< type of vertex shader (e.g. GL_VERTEX_SHADER)
		};

		/// program descriptor (needs an active OpenGL context)
		struct DescProgram {
			DescProgram();
			GLuint id = 0;									///< program id/name
			GLboolean delete_status = GL_FALSE;				///< status of deletion
			GLboolean link_status = GL_FALSE;				///< linked?	(last updated at linkage)
			GLboolean validate_status = GL_FALSE;			///< validated? (last updated at linkage)
			GLint info_log_length = 0;						///< info log length	(last updated at linkage)
			GLint active_atomic_counters = 0;				///< number of active atomic counters	(last updated at linkage)
			GLint active_attributes = 0;					///< number of active attributes (last updated at linkage)
			GLint active_attributes_max_length = 0;			///< max name length of active attributes (last updated at linkage)
			GLint binary_length = 0;						///< program binary length (last updated at linkage)
			GLenum binary_format = GL_NONE;					///< program binary length (last updated at linkage)
			GLint binary_retrievable_hint = GL_FALSE;		///< is this program binary retrievable? (last updated at linkage)
			GLint compute_work_group_size[3] = { 0 };		///< size of compute work group (last updated at linkage)
			GLint geometry_vertices_out = 0;				///< the max number of geometry vertices that can be outputted by this program (last updated at linkage)
			GLenum geometry_input_type = GL_NONE;			///< type of primitive type accepted as input by the geometry shader	(last updated at linkage)
			GLenum geometry_output_type = GL_NONE;			///< type of primitive type outputed by the geometry shader 	(last updated at linkage)
			GLint separable = GL_FALSE;						///< is this program separable? (last updated at linkage)

			GLboolean has_vertex_shader = GL_FALSE;			///< is this shader stage present in this program?
			GLboolean has_tessellation_control_shader = GL_FALSE;		///< is this shader stage present in this program?
			GLboolean has_tessellation_evaluation_shader = GL_FALSE;		///< is this shader stage present in this program?
			GLboolean has_geometry_shader = GL_FALSE;		///< is this shader stage present in this program?
			GLboolean has_fragment_shader = GL_FALSE;		///< is this shader stage present in this program?
			GLboolean has_compute_shader = GL_FALSE;		///< is this shader stage present in this program?
			std::vector<GLuint> attached_shaders;			///< attached shaders			

			/// explicit locations.
			struct Bindings {
				//Thus you cannot bind one user-defined attribute (string) variable to multiple indices
				//but you can bind multiple user-defined attribute (string) variables to the same index.
				std::vector<std::vector<std::string>> attribute_index_to_variables;	///< varying name, index by attribute
				std::vector<std::vector<std::string>> color_index_to_varying;			///< the varying names outputing on index 0, indexed by the color (draw buffer) number
				std::vector<GLuint> shader_storage_index_to_local_block;///< remapped shader storage from global index TO shader block index
			}bindings;													///< explicit bindings


			/// uniforms
			struct Uniforms {
				GLint active_uniforms = 0;					///< number of active uniforms (last updated at linkage)
				GLint active_uniform_max_length = 0;		///< max name length of active uniforms (last updated at linkage)
				GLint active_uniforms_blocks = 0;			///< number of active uniforms blocks (last updated at linkage)
				GLint active_uniform_block_max_length = 0;	///< max name length of active uniform blocks (last updated at linkage)
				std::vector<GLuint> uniform_block_to_local_block;	///< remapped uniform block from global index TO shader block index
				struct Float16 { GLfloat v[16]; };
				struct Float4 { GLfloat v[4]; };
				std::map<std::string, GLint> locations;		///< uniform locations
			}uniforms;										///< uniforms

			/// active subroutines
			struct UniformSubroutines {
				std::vector<GLuint> vertex;					///< ids of subroutines active in vertex shader
				std::vector<GLuint> tess_control;			///< ids of subroutines active in tessellation control shader
				std::vector<GLuint> tess_eval;				///< ids of subroutines active in tessellation evaluation shader
				std::vector<GLuint> geometry;				///< ids of subroutines active in geometry shader
				std::vector<GLuint> fragment;				///< ids of subroutines active in fragment shader
			}uniform_subroutines;							///< subroutines

			/// transform feedback
			struct TransformFeedback {
				GLenum buffer_mode = GL_NONE;				///< buffer mode (GL_INTERLEAVED_ATTRIBS or GL_SEPARATE_ATTRIBS)
				std::vector<std::string> varyings;			///< names of varyings captured by transform feedback
				GLint varyings_max_length = 0;				///< max name length of varyings capture in transform feedback (last updated at linkage)
			}transform_feedback;							///< transform feedback
		};

		/// program pipeline descriptor
		struct DescProgramPipeline {
			GLuint id = 0;						///< program pipeline id/name
			GLboolean created = GL_FALSE;		///< has the object been created? (OpengGL can generate objects but not create them)
			GLboolean validate_status = GL_FALSE;///< validated?
			GLuint active_program;				///< for uniform binding
			GLuint vertex_shader_program = 0;	///< using vertex shader from this program 
			GLuint tess_control_shader_program = 0;///< using tess control shader from this program 
			GLuint tess_eval_shader_program = 0;///< using tess eval shader from this program 
			GLuint geometry_shader_program = 0;	///< using geometry shader from this program 
			GLuint fragment_shader_program = 0;	///< using fragment shader from this program 
			GLuint compute_shader_program = 0;	///< using compute shader from this program 
		};

		/// query descriptor
		struct DescQuery {
			GLuint id = 0;						///< query id/name
			GLboolean created = GL_FALSE;		///< has the object been created? (OpengGL can generate objects but not create them)
			GLenum target = GL_SAMPLES_PASSED;	///< query target
			GLboolean active = GL_FALSE;		///< is the query active? (between glBeginQuery and glEndQuery)
		};
		/// sync obj descriptor
		struct DescSync {
			GLsync id = 0;						///< sync object id/name
			GLint type = GL_SYNC_FENCE;			///< only possible type
			GLenum condition = GL_SYNC_GPU_COMMANDS_COMPLETE;///< sync condition (only one available as of 4.5)
			GLbitfield flags = 0;				///< flags (only 0 as of 4.5)
		};
		/// sampler obj descriptor
		struct DescSampler {
			GLuint id = 0;						///< sampler id/name
			GLboolean created = GL_FALSE;		///< has the object been created? (OpengGL can generate objects but not create them)
			GLint filter_min = GL_NEAREST_MIPMAP_LINEAR;///< minification filter for texturing
			GLint filter_mag = GL_LINEAR;		///< magnification filtering
			GLfloat lod_min= -1000.0f;			///< minimium LOD
			GLfloat lod_max = 1000.0f;			///< max LOD
			GLfloat lod_bias = 0.0;				///< lod bias?
			GLint wrap_s = GL_REPEAT;			///< texture wrapping on the s axis
			GLint wrap_t = GL_REPEAT;			///< texture wrapping on the s axis
			GLint wrap_r = GL_REPEAT;			///< texture wrapping on the s axis
			GLint compare_mode = GL_NONE;		///< compare mode, valid only for sampling of textures of GL_DEPTH_COMPONENT_* format
			GLint compare_func = GL_LESS;		///< compare function

			/// border color
			struct BorderColor {
				GLfloat r = 0.0f;				///< border color - red
				GLfloat g = 0.0f;				///< border color - green
				GLfloat b = 0.0f;				///< border color - blue
				GLfloat a = 0.0f;				///< border color - alpha
			}bordercolor;						///< border color
		};

		/// texture descriptor
		struct DescTexture {
			GLuint id = 0;						///< texture id/name
			GLboolean created = GL_FALSE;		///< has the object been created? (OpengGL can generate objects but not create them)
			GLuint alias_id = 0;				///< is this texture an alias of another texture? (0 if no)
			GLenum target = GL_NONE;			///< type of texture
			
			/// texture storage
			struct Storage{
				/// see https://www.opengl.org/wiki/Texture_Storage
				struct Mipmap{
					GLint width;				///< support for degenerate/incomplete textures
					GLint height;
					GLint depth;
					GLint size;
				};
				struct Layer{
					std::map<GLuint, Mipmap> mipmap;
				};
				struct Face{
					std::vector<Layer> layer;
				};
				std::vector<Face> faces;		///< texture faces
				GLint base_width = 1;
				GLint base_height = 1;
				GLint base_depth = 1;
				GLint base_size = 0;			///< in bytes, the BASE image = (layer=0, face=0, mipmap=0)
				GLint total_size =0;			/// size of all layers, all faces and all mipmaps
				GLboolean immutable = GL_FALSE;	///< is storage immutable?
			}storage;							///< texture storage
									
			/// data format
			struct PixelFormat{
				GLint samples = 1;				///< number of samples per texel (relevant for multisampled textures)
				GLboolean sampled_at_fixed_locations = GL_TRUE; ///< fixed sample locations?
				GLint format = GL_NONE;			///< internal format
				GLfloat bps = 0;				///< BITS per sample / pixel (inexact for internal=nonsized formats!)
				GLint red_bits = 0;				///< red bits 
				GLint green_bits = 0;			///< green bits
				GLint blue_bits = 0;			///< blue bits
				GLint alpha_bits = 0;			///< alpha bits
				GLint depth_bits = 0;			///< depth bits
				GLint stencil_bits = 0;			///< stencil bits
				GLint shared_bits =0;			///< shared bits
				GLboolean compressed = GL_FALSE;///< compressed format?
				GLboolean srgb = GL_FALSE;	///< format is srgb?
				GLenum data_type = GL_NONE;		///< type of data (GL_NONE, GL_SIGNED_NORMALIZED, GL_UNSIGNED_NORMALIZED, GL_FLOAT, GL_INT, and GL_UNSIGNED_INT)
												///< stencil is ALWAYS integer, so the type of depth_stencil formats describes only the depth channel
			}pixelformat;						///< format

			/// buffer storage
			struct Buffer {
				GLuint id = 0;					///< texture storage comes from a buffer with this id (0 if texture has its own storage)
				GLintptr offset = 0;			///< offset in the buffer from which the texture storage is provided 
				GLsizeiptr size = 0;			///< size of the range of the buffer from which the texture storage is provided
			}buffer_store;						///< buffer storage (texture has storage from a buffer object)

			/// parameters (some overlap with samplers)
			struct Parameters {
				GLint depth_stencil_texture_mode = GL_DEPTH_COMPONENT;	///< reading mode from depth-stencil formats
				GLint mipmap_base_level = 0;	///< index of the lowest defined mipamp
				
				//border color
				struct BorderColor {
					float r = 0.0f;				///< normalized or unnormalized depending on setting command
					float g = 0.0f;				///< normalized or unnormalized depending on setting command
					float b = 0.0f;				///< normalized or unnormalized depending on setting command
					float a = 0.0f;				///< normalized or unnormalized depending on setting command
				}bordercolor;					///< border color

				//swizzle 
				struct SwizzleRGBA {
					GLint r = GL_RED;			///< to which r/g/b/a channel is the r channel swizzled? (this NOT an index, use GL_RED, GL_GREEN, GL_BLUE, GL_ALPHA)
					GLint g = GL_GREEN;			///< to which r/g/b/a channel is the g channel swizzled? (this NOT an index, use GL_RED, GL_GREEN, GL_BLUE, GL_ALPHA)
					GLint b = GL_BLUE;			///< to which r/g/b/a channel is the b channel swizzled? (this NOT an index, use GL_RED, GL_GREEN, GL_BLUE, GL_ALPHA)
					GLint a = GL_ALPHA;			///< to which r/g/b/a channel is the a channel swizzled? (this NOT an index, use GL_RED, GL_GREEN, GL_BLUE, GL_ALPHA)
				}swizzle_rgba;					///< swizzle rgba

				GLint compare_func = GL_LESS;	///< compare func when compare mode is GL_COMPARE_REF_TO_TEXTURE, valid only for sampling of textures of GL_DEPTH_COMPONENT_* format
				GLint compare_mode = GL_NONE;	///< compare mode

				GLint filter_min = GL_NEAREST_MIPMAP_LINEAR;///< minification filter for texturing
				GLint filter_mag = GL_LINEAR;	///< magnification filtering
				GLfloat lod_min = -1000.0f;		///< minimium LOD
				GLfloat lod_max = 1000.0f;		///< max LOD
				GLfloat lod_bias = 0.0f;		///< lod bias
				GLint lod_base = 0;				///< lod base
				GLint max_level = 1000;			///< highest defined mipmap level
				GLint wrap_s = GL_REPEAT;		///< wrapping on the s axis
				GLint wrap_t = GL_REPEAT;		///< wrapping on the s axis
				GLint wrap_r = GL_REPEAT;		///< wrapping on the s axis
				GLint max_anisotropy = 0;		///< maximum anisotropy
			}parameters;						///< filtering parameters
		};

		/// transform feedback descriptor
		struct DescTransformFeedback {
			GLuint id = 0;								///< transform feedback id/name
			GLboolean created = GL_FALSE;				///< has the object been created? (OpengGL can generate objects but not create them)

			// target buffer
			struct TargetBuffer{
				GLuint id = 0;							///< id/name of target buffer
				GLintptr offset = 0;					///< offset into the target buffer (in machine units)
				GLsizeiptr size = 0;					///< The amount of data in basic machine units that can be read from or written to the buffer object while used as an indexed target. 
			};
			std::vector<TargetBuffer> target_buffers;	///< target buffers for each transform feedback index
		};

		/// vertex array object descriptor
		struct DescVertexArrayObject {
			GLuint id = 0;								///< vao id/name
			GLboolean created = GL_FALSE;				///< has the object been created? (OpengGL can generate objects but not create them)

			struct BufferBinding {
				GLuint vertex_buffer = 0;				///< vertex buffer id/name
				GLintptr offset = 0;					///< offset (offset of the first element in the buffer)
				GLintptr stride = 0;					///< stride (distance between elements in the buffer)
				GLuint divisor = 0;						///< divisor (how many elements to jump per instance, 0 = non instanced rendering)
			};
			std::vector<BufferBinding> vertex_buffer_bindings;	///< the buffer bindings of the vao	(e.g. one buffer for vertices, one for normals, etc)
			struct ElementArrayBuffer{
				GLuint id = 0;							///< the id of the buffer
				GLintptr offset = 0;					///< the offset of the buffer
				GLintptr size =0;						///< the maximum size read from the buffer
			}element_array_buffer;						///< the element array buffer
			struct Attribute {
				GLboolean enabled = GL_FALSE;			///< is this vertex attribute enabled
				GLdouble generic[4] = {0,0,0,1};		///< generic vertex arguments, defaults to (0,0,0,1)
				GLuint vertex_buffer_binding = 0;		///< which is the BufferBinding which describes how the attribute is read from memory?
				GLint size = 0;							///< the number of values per vertex that are stored in the attribute (e.g. vec3 = 3, vec4 =4)
				GLenum type = GL_NONE;					///< the type of the data stored in the array (e.g. GL_FLOAT)
				GLboolean normalized = GL_FALSE;		///< should integer data be normalized to [-1,1] or [0,1] depending on type (signed vs unsigned) ?
				GLuint relative_offset = 0;				///< distance in machine units between vertex buffer start and first attrib (in the bound vertex buffer)
			};
			std::vector<Attribute> attributes;			///< the attributes of the vao
		};


		///---------------------------------------------------------------------------------------------------------------------------
		//data state
		struct State {
			//should only be obtained from DebugContext, requires an OpenGL context
			State();

			///context constants (on some platforms and drivers this data is not 100% reliable.
			struct Constants {
				GLboolean core = GL_FALSE;						///< is this context core?
				GLboolean debug = GL_FALSE;						///< is this context debug?
				GLboolean robustness = GL_FALSE;				///< is this context robust?
				GLint MAJOR_VERSION = 0;						///< context major version (some drivers output 4.5 directly!)
				GLint MINOR_VERSION = 0;						///< context minor version (some drivers output 4.5 directly!)
				GLint CONTEXT_FLAGS = 0;						///< context flags
				GLint MAX_COMPUTE_SHADER_STORAGE_BLOCKS = 0;	///< max number of shader storage blocks
				GLint MAX_COMBINED_SHADER_STORAGE_BLOCKS = 0;	///< max number of combined shader storage blocks
				GLint MAX_COMPUTE_UNIFORM_BLOCKS = 0;			///< max compute uniform blocks
				GLint MAX_COMPUTE_TEXTURE_IMAGE_UNITS = 0;		///< max compute texture image units
				GLint MAX_COMPUTE_UNIFORM_COMPONENTS = 0;		///< max compute uniform components
				GLint MAX_COMPUTE_ATOMIC_COUNTERS = 0;			///< max compute atomic counters
				GLint MAX_COMPUTE_ATOMIC_COUNTER_BUFFERS = 0;	///< max compute atomic counter buffers
				GLint MAX_COMBINED_COMPUTE_UNIFORM_COMPONENTS = 0;///< max combined compute uniform components
				GLint MAX_COMPUTE_WORK_GROUP_INVOCATIONS = 0;	///< max compute work group invocations
				GLint MAX_COMPUTE_WORK_GROUP_COUNT[3] = { 0 };	///< max compute work group count
				GLint MAX_COMPUTE_WORK_GROUP_SIZE[3] = { 0 };	///< max compute work group size
				GLint MAX_DEBUG_GROUP_STACK_DEPTH = 0;			///< max debug group stack depth
				GLint MAX_3D_TEXTURE_SIZE = 0;					///< max 3d texture size
				GLint MAX_ARRAY_TEXTURE_LAYERS = 0;				///< max array texture layers
				GLint MAX_CLIP_DISTANCES = 0;					///< max clip distances
				GLint MAX_COLOR_TEXTURE_SAMPLES = 0;			///< max color texture samples
				GLint MAX_COMBINED_ATOMIC_COUNTERS = 0;			///< max combined atomic counters
				GLint MAX_COMBINED_FRAGMENT_UNIFORM_COMPONENTS = 0;///< max combined fragment uniform components
				GLint MAX_COMBINED_GEOMETRY_UNIFORM_COMPONENTS = 0;///< max combined geometry uniform components
				GLint MAX_COMBINED_TEXTURE_IMAGE_UNITS = 0;		///< max combined texture image units
				GLint MAX_COMBINED_UNIFORM_BLOCKS = 0;			///< max combined uniform blocks
				GLint MAX_COMBINED_VERTEX_UNIFORM_COMPONENTS = 0;///< max combined vertex uniform components
				GLint MAX_CUBE_MAP_TEXTURE_SIZE = 0;			///< max cube map texture size
				GLint MAX_DEPTH_TEXTURE_SAMPLES = 0;			///< max depth texture samples
				GLint MAX_DRAW_BUFFERS = 0;						///< max draw buffers 
				GLint MAX_COLOR_ATTACHMENTS = 0;				///< max color attachments 
				GLint MAX_DUAL_SOURCE_DRAW_BUFFERS = 0;			///< max dual source draw buffers
				GLint MAX_ELEMENTS_INDICES = 0;					///< max elements indices
				GLint MAX_ELEMENTS_VERTICES = 0;				///< max elements vertices
				GLint MAX_FRAGMENT_ATOMIC_COUNTERS = 0;			///< max fragment atomic counters
				GLint MAX_FRAGMENT_SHADER_STORAGE_BLOCKS = 0;	///< max fragment shader storage blocks
				GLint MAX_FRAGMENT_INPUT_COMPONENTS = 0;		///< max fragment input component
				GLint MAX_FRAGMENT_UNIFORM_COMPONENTS = 0;		///< max fragment uniform components
				GLint MAX_FRAGMENT_UNIFORM_VECTORS = 0;			///< max fragment uniform vectors
				GLint MAX_FRAGMENT_UNIFORM_BLOCKS = 0;			///< max fragment uniform blocks
				GLint MAX_FRAMEBUFFER_WIDTH = 0;				///< max framebuffer width
				GLint MAX_FRAMEBUFFER_HEIGHT = 0;				///< max framebuffer height
				GLint MAX_FRAMEBUFFER_LAYERS = 0;				///< max framebuffer layers
				GLint MAX_FRAMEBUFFER_SAMPLES = 0;				///< max framebuffer samples
				GLint MAX_GEOMETRY_ATOMIC_COUNTERS = 0;			///< max geometry atomic counters
				GLint MAX_GEOMETRY_SHADER_STORAGE_BLOCKS = 0;	///< max geometry shader storage blocks
				GLint MAX_GEOMETRY_INPUT_COMPONENTS = 0;		///< max geometry input components
				GLint MAX_GEOMETRY_OUTPUT_COMPONENTS = 0;		///< max geometry output components
				GLint MAX_GEOMETRY_TEXTURE_IMAGE_UNITS = 0;		///< max geometry texture image units
				GLint MAX_GEOMETRY_UNIFORM_BLOCKS = 0;			///< max geometry uniform blocks
				GLint MAX_GEOMETRY_UNIFORM_COMPONENTS = 0;		///< max geometry uniform components
				GLint MIN_MAP_BUFFER_ALIGNMENT = 0;				///< min map buffer alignment
				GLint MAX_INTEGER_SAMPLES = 0;					///< max integer samples
				GLint MAX_LABEL_LENGTH = 0;						///< max label length
				GLint MAX_PROGRAM_TEXEL_OFFSET = 0;				///< max program texel offset
				GLint MIN_PROGRAM_TEXEL_OFFSET = 0;				///< min program texel offset
				GLint MAX_RECTANGLE_TEXTURE_SIZE = 0;			///< max rectangle texture size
				GLint MAX_RENDERBUFFER_SIZE = 0;				///< max renderbuffer size
				GLint MAX_SAMPLE_MASK_WORDS = 0;				///< max sample mask words
				GLint MAX_SERVER_WAIT_TIMEOUT = 0;				///< max server wait timeout
				GLint MAX_SHADER_STORAGE_BUFFER_BINDINGS = 0;	///< max shader storage buffer bindings
				GLint MAX_TESS_CONTROL_ATOMIC_COUNTERS = 0;		///< max tess control atomic counters
				GLint MAX_TESS_EVALUATION_ATOMIC_COUNTERS = 0;	///< max tess evaluation atomic counters
				GLint MAX_TESS_CONTROL_SHADER_STORAGE_BLOCKS = 0;///< max tess control shader storage blocks
				GLint MAX_TESS_EVALUATION_SHADER_STORAGE_BLOCKS = 0;///< max tess evaluation shader storage blocks
				GLint MAX_TEXTURE_BUFFER_SIZE = 0;				///< max texture buffer size
				GLint MAX_TEXTURE_IMAGE_UNITS = 0;				///< max texture image units
				GLint MAX_TEXTURE_LOD_BIAS = 0;					///< max texture lod bias
				GLint MAX_TEXTURE_SIZE = 0;						///< max texture size
				GLint MAX_UNIFORM_BUFFER_BINDINGS = 0;			///< max uniform buffer bindings
				GLint MAX_UNIFORM_BLOCK_SIZE = 0;				///< max uniform block size
				GLint MAX_UNIFORM_LOCATIONS = 0;				///< max uniform locations
				GLint MAX_VARYING_COMPONENTS = 0;				///< max varying components
				GLint MAX_VARYING_VECTORS = 0;					///< max varying vectors
				GLint MAX_VARYING_FLOATS = 0;					///< max varying floats
				GLint MAX_VERTEX_ATOMIC_COUNTERS = 0;			///< max vertex atomic counters
				GLint MAX_VERTEX_ATTRIBS = 0;					///< max vertex attribs
				GLint MAX_VERTEX_SHADER_STORAGE_BLOCKS = 0;		///< max vertex shader storage blocks
				GLint MAX_VERTEX_TEXTURE_IMAGE_UNITS = 0;		///< max vertex texture image units
				GLint MAX_VERTEX_UNIFORM_COMPONENTS = 0;		///< max vertex uniform components
				GLint MAX_VERTEX_UNIFORM_VECTORS = 0;			///< max vertex uniform vectors
				GLint MAX_VERTEX_OUTPUT_COMPONENTS = 0;			///< max vertex output components
				GLint MAX_VERTEX_UNIFORM_BLOCKS = 0;			///< max vertex uniform blocks
				GLint MAX_VIEWPORT_DIMS = 0;					///< max viewport dims
				GLint MAX_VIEWPORTS = 0;						///< max viewports
				GLint MAX_VERTEX_ATTRIB_RELATIVE_OFFSET = 0;	///< max vertex attrib relative offset
				GLint MAX_VERTEX_ATTRIB_BINDINGS = 0;			///< max vertex attrib bindings
				GLint MAX_ELEMENT_INDEX = 0;					///< max element index
				
				GLint MAX_ATOMIC_COUNTER_BUFFER_BINDINGS = 0;	///< atomic counter buffer bindings
				GLint MAX_ATOMIC_COUNTER_BUFFER_SIZE = 0;
				GLint MAX_COMBINED_SHADER_OUTPUT_RESOURCES = 0;
				GLint MAX_COMBINED_TESS_CONTROL_UNIFORM_COMPONENTS = 0;
				GLint MAX_COMBINED_TESS_EVALUATION_UNIFORM_COMPONENTS = 0;
				GLint MAX_COMPUTE_IMAGE_UNIFORMS = 0;
				GLint MAX_COMPUTE_SHARED_MEMORY_SIZE = 0;
				GLint MAX_DEBUG_LOGGED_MESSAGES = 0;
				GLint MAX_DEBUG_MESSAGE_LENGTH = 0;
				GLint MAX_FRAGMENT_ATOMIC_COUNTER_BUFFERS = 0;
				GLint MAX_FRAGMENT_IMAGE_UNIFORMS = 0;
				GLint MAX_FRAGMENT_INTERPOLATION_OFFSET = 0;
				GLint MAX_GEOMETRY_ATOMIC_COUNTER_BUFFERS = 0;
				GLint MAX_GEOMETRY_IMAGE_UNIFORMS = 0;
				GLint MAX_GEOMETRY_OUTPUT_VERTICES = 0;
				GLint MAX_GEOMETRY_SHADER_INVOCATIONS = 0;
				GLint MAX_GEOMETRY_TOTAL_OUTPUT_COMPONENTS = 0;
				GLint MAX_IMAGE_SAMPLES = 0;
				GLint MAX_IMAGE_UNITS = 0;
				GLint MAX_PATCH_VERTICES = 0;
				GLint MAX_PROGRAM_TEXTURE_GATHER_OFFSET = 0;
				GLint MAX_SHADER_STORAGE_BLOCK_SIZE = 0;
				GLint MAX_SUBROUTINE_UNIFORM_LOCATIONS = 0;
				GLint MAX_SUBROUTINES = 0;
				GLint MAX_TESS_CONTROL_ATOMIC_COUNTER_BUFFERS = 0;
				GLint MAX_TESS_CONTROL_IMAGE_UNIFORMS = 0;
				GLint MAX_TESS_CONTROL_INPUT_COMPONENTS = 0;
				GLint MAX_TESS_CONTROL_OUTPUT_COMPONENTS = 0;
				GLint MAX_TESS_CONTROL_TEXTURE_IMAGE_UNITS = 0;
				GLint MAX_TESS_CONTROL_TOTAL_OUTPUT_COMPONENTS = 0;
				GLint MAX_TESS_CONTROL_UNIFORM_BLOCKS = 0;
				GLint MAX_TESS_CONTROL_UNIFORM_COMPONENTS = 0;
				GLint MAX_TESS_EVALUATION_ATOMIC_COUNTER_BUFFERS = 0;
				GLint MAX_TESS_EVALUATION_IMAGE_UNIFORMS = 0;
				GLint MAX_TESS_EVALUATION_INPUT_COMPONENTS = 0;
				GLint MAX_TESS_EVALUATION_OUTPUT_COMPONENTS = 0;
				GLint MAX_TESS_EVALUATION_TEXTURE_IMAGE_UNITS = 0;
				GLint MAX_TESS_EVALUATION_UNIFORM_BLOCKS = 0;
				GLint MAX_TESS_EVALUATION_UNIFORM_COMPONENTS = 0;
				GLint MAX_TESS_GEN_LEVEL = 0;
				GLint MAX_TESS_PATCH_COMPONENTS = 0;
				GLint MAX_TRANSFORM_FEEDBACK_BUFFERS = 0;
				GLint MAX_TRANSFORM_FEEDBACK_INTERLEAVED_COMPONENTS = 0;
				GLint MAX_TRANSFORM_FEEDBACK_SEPARATE_ATTRIBS = 0;
				GLint MAX_TRANSFORM_FEEDBACK_SEPARATE_COMPONENTS = 0;
				GLint MAX_VERTEX_ATOMIC_COUNTER_BUFFERS = 0;
				GLint MAX_VERTEX_ATTRIB_STRIDE = 0;
				GLint MAX_VERTEX_IMAGE_UNIFORMS = 0;
				GLint MAX_VERTEX_STREAMS = 0;
				GLint MIN_FRAGMENT_INTERPOLATION_OFFSET = 0;
				GLint MIN_PROGRAM_TEXTURE_GATHER_OFFSET = 0;
				GLint NUM_PROGRAM_BINARY_FORMATS = 0;
				GLint PROGRAM_BINARY_FORMATS = 0;
				GLint SHADER_COMPILER = 0;
				GLint SHADER_STORAGE_BUFFER_OFFSET_ALIGNMENT = 0;
				GLint UNIFORM_BUFFER_OFFSET_ALIGNMENT = 0;

				GLint ALIASED_LINE_WIDTH_RANGE[2] = { 0 };
				GLint SMOOTH_LINE_WIDTH_RANGE[2] = { 0 };
				GLint POINT_SIZE_RANGE[2] = { 0 };
				GLint VIEWPORT_BOUNDS_RANGE[2] = { 0 };

				GLint NUM_COMPRESSED_TEXTURE_FORMATS = 0;
				std::vector<GLint> COMPRESSED_TEXTURE_FORMATS;
				GLint NUM_SHADER_BINARY_FORMATS = 0;
				std::vector<GLint> SHADER_BINARY_FORMATS;
				
				std::string VENDOR = "";						///< vendor
				std::string RENDERER = "";						///< renderer
				std::string VERSION = "";						///< version
				std::string SHADING_LANGUAGE_VERSION = "";		///< sl version
			}constants;											///< constants

			/// attribute state
			struct Attributes {
				std::map<GLuint, DescVertexArrayObject> vertex_array_objects;///< descriptor map for all GL vao objects
				GLuint current_vertex_array = 0;				///< currently bound vao, or 0 if none is bound.

				/// provoking vertex (strips)
				struct ProvokingVertex {
					GLenum mode = GL_LAST_VERTEX_CONVENTION;	///< the vertex to be used as the source of data for flat shaded varyings
					GLuint index = 0;							///< the restart index (default is 0, be sure to change if using this..)
					GLboolean enabled = GL_FALSE;				///< is primitive restart enabled?
					GLboolean enabled_fixed_index = GL_FALSE;	///< is primitive restarting with a fixed index enabled?
				}provoking_vertex;								///< provoking vertex properties

				/// patch state (tessellation)
				struct Patch {
					GLint vertices;								///< number of vertices in a patch
					GLfloat default_outer_level[4] = { 1.0f };	///< patch for outer level (can be overwritten in shader)
					GLfloat default_inner_level[2] = { 1.0f };	///< patch for outer level (can be overwritten in shader)
				}patch;											///< patch state
			}attributes;										///< attribute state


			/// blend state
			struct Blend {
				/// blend constant color
				struct ConstantColor {
					GLclampf r = 0.0f;							///< constant color r
					GLclampf g = 0.0f;							///< constant color g
					GLclampf b = 0.0f;							///< constant color b
					GLclampf a = 0.0f;							///< constant color a
				}constant_color;								///< constant color for blending
				/// draw buffer
				struct DrawBuffer {
					GLboolean enabled = GL_FALSE;				///< is blending enabled on this drawbuffer
					/// blend equation
					struct Equation {
						GLenum mode_rgb = GL_FUNC_ADD;			///< blending equation for rgb
						GLenum mode_alpha = GL_FUNC_ADD;		///< blending equation for alpha
					}equation;									///< equation
					/// blend function
					struct Function {
						GLenum src_rgb_factor = GL_ONE;			///< factor for source		result = SOURCE * sfactor [EQUATION] DESTINATION * dfactor
						GLenum src_alpha_factor = GL_ONE;		///< factor for source		result = SOURCE * sfactor [EQUATION] DESTINATION * dfactor
						GLenum dst_rgb_factor = GL_ZERO;		///< facotr for destination result = SOURCE * sfactor [EQUATION] DESTINATION * dfactor 
						GLenum dst_alpha_factor = GL_ZERO;		///< factor for destination result = SOURCE * sfactor [EQUATION] DESTINATION * dfactor 
					}function;									///< function
				};
				std::vector<DrawBuffer> drawbuffer;				///< draw buffer
			}blend;												///< blending

			/// buffers state
			struct Buffer {
				std::map<GLuint, DescBuffer> buffers;			///< descriptor map for all GL buffer objects
				struct Binding {
					GLuint buffer = 0;							///< which buffer is bound
					GLintptr offset = 0;						///< from which offset is the buffer bound?
					GLsizeiptr size = 0;						///< amount of data that can be read form the bound buffer
				};
				Binding ARRAY_BUFFER;							///< which buffer is bound to GL_ARRAY_BUFFER (0 = none)
				std::vector<Binding> ATOMIC_COUNTER_BUFFER;		///< which buffer is bound to GL_ATOMIC_COUNTER_BUFFER (0 = none)
				Binding COPY_READ_BUFFER;						///< which buffer is bound to GL_COPY_READ_BUFFER (0 = none)
				Binding COPY_WRITE_BUFFER;						///< which buffer is bound to GL_COPY_WRITE_BUFFER (0 = none)
				Binding DISPATCH_INDIRECT_BUFFER;				///< which buffer is bound to GL_DISPATCH_INDIRECT_BUFFER (0 = none)
				Binding DRAW_INDIRECT_BUFFER;					///< which buffer is bound to GL_DRAW_INDIRECT_BUFFER (0 = none)
				Binding ELEMENT_ARRAY_BUFFER;					///< which buffer is bound to GL_ELEMENT_ARRAY_BUFFER (0 = none)
				Binding PIXEL_PACK_BUFFER;						///< which buffer is bound to GL_PIXEL_PACK_BUFFER (0 = none)
				Binding PIXEL_UNPACK_BUFFER;					///< which buffer is bound to GL_PIXEL_UNPACK_BUFFER (0 = none)
				Binding QUERY_BUFFER;							///< which buffer is bound to GL_QUERY_BUFFER (0 = none)
				std::vector<Binding> SHADER_STORAGE_BUFFER;		///< which buffer is bound to GL_SHADER_STORAGE_BUFFER (0 = none)
				Binding TEXTURE_BUFFER;							///< which buffer is bound to GL_TEXTURE_BUFFER (0 = none)
				std::vector<Binding> TRANSFORM_FEEDBACK_BUFFER;	///< which buffer is bound to GL_TRANSFORM_FEEDBACK_BUFFER (0 = none)
				std::vector<Binding> UNIFORM_BUFFER;			///< which buffer is bound to GL_UNIFORM_BUFFER (0 = none)
			}buffer;											///< buffer targets and data

			/// culling and clipping state
			struct Culling {
				GLboolean enabled = false;						///< is culling enabled?
				std::vector<GLboolean> enabled_clip_distance;	///< is clipping plane X enabled (for shader usage) ?
				/// clip control
				struct ClipControl {
					GLenum origin = GL_LOWER_LEFT;				///< origin
					GLenum depth = GL_NEGATIVE_ONE_TO_ONE;		///< depth
				}clip_control;									///< clip control (clip coordinate to window coordinate)
				GLenum front_face = GL_CCW;						///< front face (clockwise or counterclockwise)
				GLenum cull_face = GL_BACK;						///< cull face
			}culling;											///< culling

			//debug information
			struct Debug {
				GLboolean enabled_output = GL_FALSE;			///< is debug output enabled?
				GLboolean enabled_synchronous = GL_FALSE;		///< is the output synchronous?
				//no other information, not debugging the gl debugger.
			}debug;												///< debugging

			/// depth state
			struct Depth {
				GLboolean enabled_test = GL_FALSE;				///< is the depth test enabled?
				GLboolean clamped = GL_FALSE;					///< true = depth is not clamped but mapped to a provided range (see Range struct, see glDepthRange)
				/// depth range
				struct Range {
					GLdouble near_value = 0;					///< near value
					GLdouble far_value = 1;						///< far value
				};
				std::vector<Range> range;						///< clamp ranges
				GLdouble clear_value = 1.0;						///< value used by glClearDepth				
				GLenum func = GL_LESS;							///< depth test function
				GLboolean enabled_write = GL_TRUE;				///< are writes enabled on the depth buffer?
			}depth;												///< depth

			/// framebuffer state
			struct Framebuffer {
				std::map<GLuint, DescFramebuffer> framebuffers;	///< descriptor map for all GL framebuffer objects
				std::map<GLuint, DescRenderbuffer> renderbuffers;///< descriptor map for all GL renderbuffer objects
				GLuint DRAW_FRAMEBUFFER = 0;					///< which framebuffer is bound to GL_DRAW_FRAMEBUFFER (0 = none)
				GLuint READ_FRAMEBUFFER = 0;					///< which framebuffer is bound to GL_READ_FRAMEBUFFER (0 = none)
				GLuint RENDERBUFFER =0;							///< which renderbuffer is bound to GL_RENDERBUFFER (0=none)
				GLboolean enabled_srgb = GL_FALSE;				///< perform SRGB conversions for all buffers with SRGB formats attached to framebuffers?
				
				//readpixels 
				struct ReadPixels{
					GLuint framebuffer =0;								///< source of read pixels (the framebuffer)
					GLenum color_buffer =GL_NONE;				///< source of read pixels (the color buffer in the framebuffer)
					GLboolean enabled_clamp_color = GL_FALSE;		///< is data read with glReadPixels clamped?
				}readpixels;													///< read pixels state
				
				///clear coor
				struct ClearColor {
					GLfloat r = 0.0f;							///< clear red for the framebuffer currently bound on GL_DRAW_FRAMEBUFFER
					GLfloat g = 0.0f;							///< clear green for the framebuffer currently bound on GL_DRAW_FRAMEBUFFER
					GLfloat b = 0.0f;							///< clear blue for the framebuffer currently bound on GL_DRAW_FRAMEBUFFER
					GLfloat a = 0.0f;							///< clear alpha for the framebuffer currently bound on GL_DRAW_FRAMEBUFFER
				}clear_color;									///< clear color for the framebuffer currently bound on GL_DRAW_FRAMEBUFFER
				/// color mask
				struct ColorMask {
					GLboolean r = GL_TRUE;						///< is the red channel written?
					GLboolean g = GL_TRUE;						///< is the green channel written?
					GLboolean b = GL_TRUE;						///< is the blue channel written?
					GLboolean a = GL_TRUE;						///< is the alpha channel written?
				};
				std::vector<ColorMask> colormask;				///< color masks for each draw buffer

				/// default parameters values for a textureless framebuffer
				struct DefaultParams {
					GLint width = 0;							///< width of a new framebuffer (without textures/renderbuffers)
					GLint height = 0;							///< height of a new framebuffer (without textures/renderbuffers)
					GLint layers = 0;							///< the number of layers of a new framebuffer (without textures/renderbuffers)
					GLint samples = 0;							///< the number of samples per pixel (coverage mask size) of a new framebuffer(without textures/renderbuffers)
					GLboolean sampled_at_locations_default = GL_TRUE;///< are sample locations default?
				}default_params;								///< the default framebuffer (number 0)

				/// initial
				struct DefaultFBO {
					GLenum drawbuffer = GL_FRONT;			///< which buffer is drawn into?
					GLboolean doublebuffered = GL_FALSE;		///< is the default framebuffer double buffered?
					GLint color_read_format = GL_NONE;			///< the internal format of the default framebuffer
					GLint color_read_type = GL_NONE;			///< the implementation preferred pixel data type for pixel transfers on the default framebuffer
					GLint samples = 0;							///< the number of samples per pixel (coverage mask size) of the default framebuffer
					GLboolean stereo = GL_FALSE;				///< is the default buffer stereo
					GLint width = 0;							///< width of the default framebuffer
					GLint height = 0;							///< height of the default framebuffer
					GLint red_bits =0;							///< number of red bits
					GLint green_bits =0;							///< number of green bits
					GLint blue_bits =0;							///< number of blue bits
					GLint alpha_bits =0;							///< number of alpha bits
					GLint depth_bits =0;							///< number of depth bits
					GLint stencil_bits =0;							///< number of stencil bits
				}default_fbo;									///< the default framebuffer
			}framebuffer;										///< framebuffer state

			//current program
			struct Program {
				std::map<GLuint, DescProgram> programs;			///< descriptor map for all GL program objects
				std::map<GLuint, DescShader> shaders;			///< descriptor map for all GL shader objects
				std::map<GLuint, DescProgramPipeline> program_pipelines;///< descriptor map for all GL program pipeline objects

				GLenum fragment_shader_derivative_hint = GL_DONT_CARE;	///< fragment shader derivative hint
				GLuint current_program = 0;						///< currently bound program
				GLuint current_program_pipeline = 0;			///< currently bound program pipeline
			}program;											///< program, shader and program pipeline state

			/// query state
			struct Queries {
				std::map<GLuint, DescQuery> queries;			///< descriptor map for all GL query objects
				GLuint SAMPLES_PASSED = 0;						///< the query object currently used in the sample passed target
				GLuint ANY_SAMPLES_PASSED = 0;					///< the query object currently used in the any sample passed target
				GLuint ANY_SAMPLES_PASSED_CONSERVATIVE = 0;		///< the query object currently used in the any sample passed conservative target
				std::vector<GLuint> PRIMITIVES_GENERATED;		///< the query object currently used in the INDEXED primitives generated target
				std::vector<GLuint> TRANSFORM_FEEDBACK_PRIMITIVES_WRITTEN;	///< the query object currently used in the INDEXED primitives generated target
				GLuint TIME_ELAPSED = 0;						///< the query object currently used in the time elapsed target
				GLuint TIMESTAMP = 0;							///< the query object currently used in the timestamp target
				/// conditional rendering
				struct ConditionalRendering {
					GLboolean enabled = GL_FALSE;				///< conditional rendering enabled?
					GLuint query = 0;							///< query id/name used in conditional rendering
					GLenum mode = GL_ANY_SAMPLES_PASSED;		///< the conditional rendering mode
				}conditional_rendering;
			}queries;


			/// general render properties
			struct Rasterization {
				GLboolean dithered = GL_TRUE;					///< are the color components dithered (99.999% does not matter, driver controlled, was useful for 16/24 bit color framebuffers, 32bit is standard today)
				GLboolean framebuffer_srgb = GL_FALSE;			///< if true the fragment shader output is considered linear and the rgb-srgb transformation is performed before writing to the framebuffer
				GLboolean discard = GL_FALSE;					///< are primitives discarded before rasterization? (GL_RASTERIZER_DISCARD)
				GLboolean texture_cubemap_seamless = GL_FALSE;	///< filter the cubemap seamlessly? 

				/// sampling
				struct Sampling {
					GLboolean multisampled = GL_TRUE;			///< use multiple fragment samples in computing the pixel value?
					GLboolean alpha_to_coverage = GL_FALSE;		///< compute temporary coverage with the alpha values of multiple samples
					GLboolean alpha_to_one = GL_FALSE;			///< each sample alpha is set to one
					GLfloat sample_min_shading = 1.0f;			///< the minimum rate of shading samples (after temporary mask). [0,1], where 0 is 0%  1 is 100%.
					GLboolean sample_shading = GL_FALSE;		///< run the fragment shader for each covered sample or as defined by ( GL_MIN_SAMPLE_SHADING_VALUE + coverage + sample mask)
					GLboolean sample_mask = GL_FALSE;			///< should the coverage mask be ANDed with the GL_SAMPLE_MASK_VALUE
					std::vector<GLbitfield> sample_mask_words;	///< mask words (e.g. a 128 sample mask uses 4 32-bit words)
					GLboolean coverage_mask_enabled = GL_FALSE;	///< if true fragment coverage is ANDed with temporary coverage mask (rasterizer generates many samples which are filtered with this mask)
					GLboolean coverage_mask_inverted = GL_FALSE;///< invert the coverage mask?
					GLfloat coverage_mask_percentage = 1.0f;	///< which percentage of all visibility samples should be generated
				}sampling;										///< sample state

				/// logic
				struct Logic {
					GLboolean enabled = GL_FALSE;				///< are logic operations on the framebuffer enabled?
					GLenum operation = GL_COPY;					///< what is the logic operation?
				}logic;											///< logic state

				/// points
				struct Point {
					GLboolean program_point_size = GL_FALSE;	///< clamp the vertex/geometry shader point size to the implementation dependent point size range?
					GLfloat fade_threshold_size = 1.0f;			///< threshold value used for clamping point size, 
					GLenum sprite_coord_origin = GL_UPPER_LEFT;	///< point sprite coordinate origin for point
					GLfloat size = 1.0f;						///< the default size of a point
					GLboolean offset = GL_FALSE;				///< use a depth offset for depth comparisons of fragments generated from geometry rasterized with GL_POINT mode
				}point;											///< point state

				/// line
				struct Line {
					GLenum smooth_hint = GL_DONT_CARE;			///< smoothing hint
					GLboolean smoothed = GL_FALSE;				///< rasterize antialiased lines?
					GLfloat width = 1.0f;						///< the width of the rasterized lines
					GLboolean offset = GL_FALSE;				///< use a depth offset for depth comparisons of fragments generated from geometry rasterized with GL_LINE mode
				}line;											///< line state

				/// polygon related state
				struct Polygons {
					GLenum face = GL_FRONT_AND_BACK;			///< polygons for which mode applies (can only be GL_FRONT_AND_BACK)
					GLenum mode = GL_FILL;						///< rasterization mode (fill / points / line)
					GLboolean offset = GL_FALSE;				///< use a depth offset for depth comparisons of fragments generated from geometry rasterized with GL_FILL mode
					GLboolean smooth = GL_FALSE;				///< rasterize antialiased polygons?
					GLenum smooth_hint = GL_DONT_CARE;			///< smoothing hint
				}polygons;										///< polygon state

				/// depth offset
				struct DepthOffest {
					GLfloat factor = 0.0f;						///< Specifies a scale factor that is used to create a variable depth offset for each polygon		
					GLfloat units = 0.0f;						///< Is multiplied by an implementation-specific value to create a constant depth offset
				}depth_offset;									///< depth offset

				//viewport
				struct Viewport {
					GLint x = 0;								///< start of viewport on axis x
					GLint y = 0;								///< start of viewport on axis y
					GLint width = 0;							///< viewport width
					GLint height = 0;							///< viewport height
				};
				std::vector<Viewport> viewports;				///< viewports
			}rasterization;										///< rasterization state

			/// scissor state
			struct Scissor {
				//viewport
				struct Viewport {
					GLboolean enabled = GL_FALSE;				///< is the scissor test enabled on this viewport?
					GLint x = 0;								///< start of scissor on axis x
					GLint y = 0;								///< start of scissor on axis y
					GLint width = 0;							///< scissor width
					GLint height = 0;							///< scissor height
				};
				std::vector<Viewport> viewports;				///< viewports
			} scissor;											///< scissor state	

			/// stencil state
			struct Stencil {
				GLboolean enabled = GL_FALSE;					///< is the stencil test enabled?
				GLint clear_value = 0;							///< the clear value

				/// polygon facing
				struct PolygonFacing {
					GLuint write_mask = std::numeric_limits<GLuint>::max();	///< mask on writing on stencil (like depth mask)
					GLenum func = GL_ALWAYS;					///< stencil function
					GLint func_ref = 0;							///< stencil reference value
					GLuint func_mask = std::numeric_limits<GLuint>::max();	///< a mask that is ANDed with both the reference value and the stored stencil value when the test is done 
					GLenum sfail = GL_KEEP;						///< action for stencil fail
					GLenum dpfail = GL_KEEP;					///< action for stencil passes but depth fails
					GLenum dppass = GL_KEEP;					///< action for stencil pass and (depth pass or no depth test)
				};
				PolygonFacing front_facing;						///< front facing polygons stencil state
				PolygonFacing back_facing;						///< back facing polygons stencil state
			}stencil;											///< stencil state

			/// sync state
			struct Sync {
				std::map<GLsync, DescSync> sync_objects;		///< descriptor map for all GL sync objects
			}sync;												///< sync state

			/// pixel storage state
			struct PixelStorage {
				GLboolean pack_swap_bytes = GL_FALSE;			///< swap bytes?
				GLboolean pack_lsb_first = GL_FALSE;			///< lsb first?
				GLint pack_row_length = 0;						///< [0, ∞)
				GLint pack_image_height = 0;					///< [0, ∞)
				GLint pack_skip_rows = 0;						///< [0, ∞)
				GLint pack_skip_pixels = 0;						///< [0, ∞)
				GLint pack_skip_images = 0;						///< [0, ∞)
				GLint pack_alignment = 4;						///< 1, 2, 4, or 8
				GLboolean unpack_swap_bytes = GL_FALSE;			///< swap bytes?
				GLboolean unpack_lsb_first = GL_FALSE;			///< lsb first?
				GLint unpack_row_length = 0;					///< [0, ∞)
				GLint unpack_image_height = 0;					///< [0, ∞)
				GLint unpack_skip_rows = 0;						///< [0, ∞)
				GLint unpack_skip_pixels = 0;					///< [0, ∞)
				GLint unpack_skip_images = 0;					///< [0, ∞)
				GLint unpack_alignment = 4;						///< 1, 2, 4, or 8
			}pixel_storage;										///< pixel storage state

			/// image state
			struct Image {
				/// image unit state
				struct Unit {
					GLuint texture = 0;							///< texture bound to the image unit
					GLint level = 0;							///< level of the texture bound on the image unit
					GLboolean layered = GL_TRUE;				///< is the texture binding layered?
					GLint layer = 0;							///< layer of the texture bound on the image unit
					GLenum access = GL_READ_WRITE;				///< type of access
					GLenum format = GL_NONE;					///< internal format of texture
				};
				std::vector<Unit> unit;							///< image units
			}image;												///< image state

			/// texture state
			struct Texture {
				std::map<GLuint, DescSampler> samplers;			///< descriptor map for all GL sampler objects
				std::map<GLuint, DescTexture> textures;			///< descriptor map for all GL texture objects
				GLenum compression_hint = GL_DONT_CARE;			///< texture compression hint
				
				/// texture unit state
				struct Unit {
					GLuint sampler = 0;							///< which sampler is bound to this unit (0 if none)
					GLuint target_1d = 0;						///< texture bound on the GL_TEXTURE_1D target
					GLuint target_2d = 0;						///< texture bound on the GL_TEXTURE_2D target
					GLuint target_3d = 0;						///< texture bound on the GL_TEXTURE_3D target
					GLuint target_1d_array = 0;					///< texture bound on the GL_TEXTURE_1D_ARRAY target
					GLuint target_2d_array = 0;					///< texture bound on the GL_TEXTURE_2D_ARRAY target
					GLuint target_rectangle = 0;				///< texture bound on the GL_TEXTURE_RECTANGLE target
					GLuint target_cubemap = 0;					///< texture bound on the GL_TEXTURE_CUBEMAP target
					GLuint target_cubemap_array = 0;			///< texture bound on the GL_TEXTURE_CUBEMAP_ARRAY target
					GLuint target_buffer = 0;					///< texture bound on the GL_TEXTURE_BUFFER target
					GLuint target_2d_multisample = 0;			///< texture bound on the GL_TEXTURE_2D_MULTISAMPLE target
					GLuint target_2d_multisample_array = 0;		///< texture bound on the GL_TEXTURE_2D_MULTISAMPLE_ARRAY target
				};
				std::vector<Unit> unit;							///< texture units
				GLenum active_unit_gl_name = GL_TEXTURE0;		///< currently active texture unit OpenGL name = GL_TEXTURE0 + active_unit
				GLuint active_unit = 0;							///< currently active texture unit 
			}texture;											///< texture state

			/// transform feedback state
			struct TransformFeedback {
				GLboolean active = GL_FALSE;					///< transform feedback active?
				GLboolean paused = GL_FALSE;					///< is the active transform feedback paused?
				GLenum primitive_mode =  GL_POINTS;				///< transform feedback mode
				std::map<GLuint, DescTransformFeedback> transform_feedbacks;///< descriptor map for all GL transform feedback objects
				GLuint current =0;								///< current transform feedback

			}transform_feedback;								///< transform feedback state
		};


		///---------------------------------------------------------------------------------------------------------------------------
		/// GL context stats
		struct Stats {
			struct {
				unsigned int all = 0;					///< number of all OpenGL commands
				unsigned int attribute = 0;				///< number of attribute binding commands (part of geometry group)
				unsigned int blend = 0;					///< number of blend commands
				unsigned int buffer = 0;				///< number of buffer commands
				unsigned int compute = 0;				///< number of compute commands 
				unsigned int culling = 0;				///< number of culling commands
				unsigned int debug = 0;					///< number of debug commands
				unsigned int depth = 0;					///< number of depth commands
				unsigned int deprecated = 0;			///< number of deprecated commands
				unsigned int draw = 0;					///< number of draw commands (part of geometry group)
				unsigned int enabler = 0;				///< number of enable commands
				unsigned int framebuffer = 0;			///< number of framebuffer commands
				unsigned int get = 0;					///< number of get commands
				unsigned int image = 0;					///< number of image commands
				unsigned int program_shader = 0;		///< number of program and shader commands
				unsigned int pixel_storage = 0;			///< number of pixel storage commands
				unsigned int rasterization = 0;			///< number of rasterization state commands
				unsigned int redundant = 0;				///< number of redundant commands
				unsigned int sampler = 0;				///< number of sampler commands
				unsigned int scissor = 0;				///< number of scissor commands
				unsigned int stencil = 0;				///< number of stencil commands
				unsigned int query = 0;					///< number of query commands 
				unsigned int sync = 0;					///< number of sync commands 
				unsigned int texture = 0;				///< number of texure commands
				unsigned int transform_feedback = 0;	///< number of transform feedback commands
				unsigned int uniform = 0;				///< number of shader uniform commands
			}commands;									///< commands

			unsigned int objects_created = 0;			///< number of created objects
			unsigned int objects_destroyed = 0;			///< number of destroyed objects
			unsigned int program_changes = 0;			///< number of useProgram and useProgramStages commands
			unsigned int pipeline_flushes = 0;			///< number of pipeline flushes (finish, use program, but not window flushes like SwapBuffers)
			unsigned int pipeline_barriers = 0;			///< number of memory barriers
			unsigned int primitives_issued = 0;			///< number of primitives sent to gpu.
			unsigned int vertices_issued = 0;			///< number of vertices sent to gpu.
			unsigned int state_changes = 0;				///< number of state changes, caused by depth, stencil, bindings, everything that changes state.
			unsigned int errors = 0;					///< number of errors

			struct Memory {
				unsigned int allocations = 0;			///< number of memory allocations
				unsigned int deallocations = 0;			///< number of memory deallocations
				unsigned int allocations_buffer = 0;	///< number of memory allocations in buffers
				unsigned int deallocations_buffer = 0;	///< number of memory deallocations in buffers
				unsigned int allocations_texture = 0;	///< number of memory allocations in textures
				unsigned int deallocations_texture = 0;	///< number of memory deallocations in textures
				unsigned int allocations_renderbuffer = 0;	///< number of memory allocations in renderbuffers
				unsigned int deallocations_renderbuffer = 0;///< number of memory deallocations in renderbuffers
				uint64_t allocated = 0;					///< total memory allocated 
				uint64_t allocated_buffer = 0;			///< memory allocated in buffers 
				uint64_t allocated_texture = 0;			///< memory allocated in textures
				uint64_t allocated_renderbuffer = 0;	///< memory allocated in renderbuffers 
				uint64_t deallocated = 0;				///< total memory deallocated 
				uint64_t deallocated_buffer = 0;		///< memory deallocated in buffers 
				uint64_t deallocated_texture = 0;		///< memory deallocated in buffers 
				uint64_t deallocated_renderbuffer = 0;	///< memory deallocated in renderbuffers 
				uint64_t current = 0;					///< current (data only)
				unsigned int transfers_cpu = 0;			///< number cpu data transfers
				uint64_t transfers_cpu_size = 0;		///< size of cpu data transfers
				unsigned int transfers_gpu = 0;			///< number of gpu data transfers
				uint64_t transfers_gpu_size = 0;		///< size of gpu data transfers (orientative! .. internal formats)
			}memory;									///< memory
			
			/// add the stats from on stats object to another
			/// @param src	the source stats object from which data will be added to this object
			void add(const Stats& src);
		};


		///---------------------------------------------------------------------------------------------------------------------------
		/// a class to encapsulate the OpenGL debug context
		/// NOTE: requires manual binding (through global setCurrentContext or through member setCurrent)
		class DebugContext {
		public:
			///-----------------------------------------------------------------------------------------------------------------------
			/// queries the native OpenGL context bound on this thread for properties and then creates a DebugContext object with the same
			/// properties as the native context.
			/// NOTE: A native OpenGL context HAS TO BE bound/current on the caller thread.
			///
			/// The DebugContext object can be used in two modes:
			/// 1. by directly calling the DebugContext functions, like CullFace, FrontFace, Hint, etc (with specification naming)
			/// 2. by directly calling the global glCullFace, glFrontFace, glHint, etc. 
			/// The second mode works ONLY IF LAP_GL_DEBUG_CONTEXT_OVERRIDE_FUNCTIONS is defined before the inclusion of this header file.
			/// Then each invocation of an OpenGL function will invoke the function of the DebugContext object bound on the caller thread. If
			/// no DebugContext is bound on the caller thread then the program will crash. (this behavior is similar to that of working with 
			/// native OpenGL functions without a valid native OpenGL context (undefined behavior).
			/// 
			/// @param safety_checks	use extensive safety checks? (will call glGetError after each GL call and will call the debug callback on GL errors)
			/// @param keep_state		maintain an internal state machine (almost complete, some things (glsl compiler related) can only be queried through GL)
			/// @param compute_stats	compute statistics, many memory stats are computed only if keep_state = true, redundancy stats are computed only if redundancy_checks = true
			/// @param redundancy_checks check redundancies? (will call the debug callback on warnings), if true it automatically sets keep_state=true
			/// @param leak_checks		check for leaks on context destruction (will call the debug callback on leaks), if true it automatically sets keep_state=true
			/// @param logger			log all OpenGL commands to this stream
			/// @param logfile			log the filename from which the OpenGL command was invoked . Not available(="") on direct class calls (instance.XXXXXX), only on global override calls (glXXXXXX)
			/// @param logfunc			log the function name from which the OpenGL command was invoked. Not available(="") on direct class calls (instance.XXXXXX), only on global override calls (glXXXXXX)
			/// @param logline			log the file line from which the OpenGL command was invoked. Not available(=0) on direct class calls (instance.XXXXXX), only on global override calls (glXXXXXX)
			/// NOTE : if no option is enabled then DebugContext as a lean wrapper, only logging the functions
			DebugContext(bool safety_checks = true, bool keep_state = true, bool compute_stats = true, bool redundancy_checks = true, bool leak_checks = true, std::ostream* logger = nullptr, bool logfile = false, bool logfunc = false, bool logline = false);
			DebugContext(const DebugContext&) = default;
			DebugContext(DebugContext&&) = default;
			DebugContext& operator=(const DebugContext&) = default;
			DebugContext& operator=(DebugContext&&) = default;
			~DebugContext();

			///-----------------------------------------------------------------------------------------------------------------------
			/// (un)binds this DebugContext to the caller thread
			/// @param bound		if true then the context is bound, else the context is unbound.
			void setCurrent(bool bound);

			///-----------------------------------------------------------------------------------------------------------------------
			/// returns the safety checks option
			/// @return the safety checks option
			bool getOptionSafetyCheck();
			/// returns the safety checks option
			/// @return the safety checks option
			bool getOptionComputeStats();
			/// returns the keep state option
			/// @return the keep state option
			bool getOptionKeepState();
			/// returns the redundancy checks option
			/// @return the redundancy checks option
			bool getOptionRedundancyChecks();
			/// returns the leak checks option
			/// @return the leak checks option
			bool getOptionLeakChecks();


			///-----------------------------------------------------------------------------------------------------------------------
			/// various function groups which can be used in logging and queries , sometimes groups have small overlaps (e.g. glPixelStore)
			/// e.g. glClearDepth is in both (default)framebuffer and depth groups
			enum class GROUP : int {
				ALL,						///< ALL channels
				ATTRIBUTE,					///< attribute (vertex specification, vao, etc)
				BLEND,						///< blending
				BUFFER,						///< buffer
				COMPUTE,					///< compute
				CULLING,					///< culling and clipping
				DEBUG,						///< debug
				DEPTH,						///< depth
				DRAW,						///< draw commands
				ENABLERS,					///< enable disable state (glEnable / glDisable / etc)
				FRAMEBUFFER,				///< framebuffer
				GET,						///< gets, IS
				IMAGE,						///< image
				PIXEL_STORE,				///< pixel storage
				RASTERIZATION,				///< rasterization properties (viewport, logical op, tessellation, shading rate, hints)
				QUERY,						///< OpenGL queries (occlusion queries, num vertices queries, etc), standard data queries (isBuffer, isTexture, getTexParameter are in GETS group)
				SAMPLER,					///< sampler (texture sampler)
				SCISSOR,					///< scissor
				SHADER,						///< shaders, programs and pipeline objects
				STENCIL,					///< stencil
				SYNC,						///< synchronization
				TEXTURE,					///< texture
				TRANSFORM_FEEDBACK,			///< transform feedback
				UNIFORM,					///< uniform commands
				V10,						///< V10
				V11,						///< V11
				V12,						///< V12
				V13,						///< V13
				V14,						///< V14
				V15,						///< V15
				V20,						///< V20
				V21,						///< V21
				V30,						///< V30
				V31,						///< V31
				V32,						///< V32
				V33,						///< V33
				V40,						///< V40
				V41,						///< V41
				V42,						///< V42
				V43,						///< V43
				V44,						///< V44
				V45,						///< V45
#ifdef LAP_GL_DEBUG_CONTEXT_COMPATIBILITY
				DEPRECATED,					///< deprecated
				V10_DEPRECATED,				///< V10 deprecated
				V11_DEPRECATED,				///< V11 deprecated
				V13_DEPRECATED,				///< V13 deprecated
				V14_DEPRECATED,				///< V14 deprecated
				V20_DEPRECATED,				///< V20 deprecated
				V32_DEPRECATED,				///< V32 deprecated
#endif
			};
			/// sets the logger for this context
			/// @param logger	the new logger stream
			/// @param logfile	log the filename from which the OpenGL command was invoked . Not available(="") on direct class calls (instance.XXXXXX), only on global override calls (glXXXXXX)
			/// @param logfunc	log the function name from which the OpenGL command was invoked. Not available(="") on direct class calls (instance.XXXXXX), only on global override calls (glXXXXXX)
			/// @param logline	log the file line from which the OpenGL command was invoked. Not available(=0) on direct class calls (instance.XXXXXX), only on global override calls (glXXXXXX)
			void setLogger(std::ostream* logger, bool logfile = false, bool logfunc = false, bool logline = false);
			/// sets the logging state of a group
			/// @param group	the function group
			/// @param enabled	enable or disable logging of commands from this group
			void setLogGroup(GROUP group, bool enabled);
			/// resets the logging state of all groups (all are enabled)
			void setLogGroupsDefault();
			/// sets the loggging state of multiple groups
			/// @param groups		a list of groups
			/// @param enabled		enable or disable logging of commands from these groups?
			void setLogGroups(const std::vector<GROUP>& groups, bool enabled);
			
			///-----------------------------------------------------------------------------------------------------------------------
			/// returns a reference to the state of the context
			/// @return a reference to the state of the context
			const State& getState() const;
			
			///-----------------------------------------------------------------------------------------------------------------------
			/// register a debug callback (similar to OpenGL 4.3. debug callback)
			/// Will be invoked when glGetError fails, when the parameters make no sense, when the function is not loaded (e.g calling
			/// glDispatchCompute on a 4.0 context or glVertex3f on a 3.0 core context) or when a performance warning is issued.
			/// NOTE: will not spam with portability/performance or other weird compiler warnings.
			/// @param callback		the registered callback
			/// @param function		argument of callback, contains the name of the function which caused the error
			/// @param resultstr	argument of callback, result(s) separated by spaces as string (e.g. 23) 
			/// @param paramsstr	argument of callback, arguments separated by spaces as string (e.g. GL_POINTS 0 GL_TRIANGLES 4.4322 32 1) 
			/// @param errorcode	argument of callback, the OpenGL error code if the error was signaled by OpenGL, GL_NO_ERROR if only a warning.
			/// @param callfile		file from which this OpenGL function was invoked. Not available(="") on direct class calls (instance.XXXXXX), only on global override calls (glXXXXXX)
			/// @param callfunc		function from which this OpenGL function was invoked. Not available(="") on direct class calls (instance.XXXXXX), only on global override calls (glXXXXXX)
			/// @param callline		line at which this OpenGL function resides (optimization CAN change this!). Not available(="") on direct class calls (instance.XXXXXX), only on global override calls (glXXXXXX)
			void setCallbackDebug(const std::function<void(const std::string& function, const std::string& resultstr, const std::string& paramsstr, GLenum errorcode, const char* callfile, const char* callfunc, unsigned int callline)>& callback);
			
			///-----------------------------------------------------------------------------------------------------------------------
			/// generates and returns the list of names for an OpenGL function group. useful for debugging.
			/// NOTE: the list is generated on each call, thus this function is not intended for common usage.
			/// @param group the OpenGL function group
			/// @return the list of function names within a group. useful for debugging.
			const std::vector<std::string> getFunctionGroupNames(GROUP group);
			
			///-----------------------------------------------------------------------------------------------------------------------
			/// returns several statistics (number of draw commands, memory allocated, etc)
			/// useful for profiling
			/// @return context stats
			const Stats& getStats() const;
			/// reset stats. useful for frame by frame stats.
			/// useful for profiling
			void resetStats();


			///-----------------------------------------------------------------------------------------------------------------------
			/// attribute
			void InternalVertexAttrib(GLuint index, int num, GLdouble v0, GLdouble v1, GLdouble v2, GLdouble v3, const char* file, const char* func, unsigned int line);
			void BindVertexArray(GLuint array, const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);
			void BindVertexBuffer(GLuint bindingindex, GLuint buffer, GLintptr offset, GLsizei stride, const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);
			void BindVertexBuffers(GLuint first, GLsizei count, const GLuint *buffers, const GLintptr *offsets, const GLsizei *strides, const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);
			void CreateVertexArrays(GLsizei n, GLuint *arrays, const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);
			void DeleteVertexArrays(GLsizei n, const GLuint *arrays, const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);
			void GenVertexArrays(GLsizei n, GLuint *arrays, const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);
			void PatchParameterfv(GLenum pname, const GLfloat *values, const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);
			void PatchParameteri(GLenum pname, GLint value, const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);
			void PrimitiveRestartIndex(GLuint index, const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);
			void ProvokingVertex(GLenum provokeMode, const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);
			void VertexArrayAttribBinding(GLuint vaobj, GLuint attribindex, GLuint bindingindex, const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);
			void VertexArrayAttribFormat(GLuint vaobj, GLuint attribindex, GLint size, GLenum type, GLboolean normalized, GLuint relativeoffset, const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);
			void VertexArrayAttribIFormat(GLuint vaobj, GLuint attribindex, GLint size, GLenum type, GLuint relativeoffset, const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);
			void VertexArrayAttribLFormat(GLuint vaobj, GLuint attribindex, GLint size, GLenum type, GLuint relativeoffset, const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);
			void VertexArrayBindingDivisor(GLuint vaobj, GLuint bindingindex, GLuint divisor, const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);
			void VertexArrayElementBuffer(GLuint vaobj, GLuint buffer, const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);
			void VertexArrayVertexBuffer(GLuint vaobj, GLuint bindingindex, GLuint buffer, GLintptr offset, GLsizei stride, const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);
			void VertexArrayVertexBuffers(GLuint vaobj, GLuint first, GLsizei count, const GLuint *buffers, const GLintptr *offsets, const GLsizei *strides, const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);
			void VertexAttrib1d(GLuint index, GLdouble v0, const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);
			void VertexAttrib1dv(GLuint index, const GLdouble *v, const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);
			void VertexAttrib1f(GLuint index, GLfloat v0, const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);
			void VertexAttrib1fv(GLuint index, const GLfloat *v, const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);
			void VertexAttrib1s(GLuint index, GLshort v0, const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);
			void VertexAttrib1sv(GLuint index, const GLshort *v, const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);
			void VertexAttrib2d(GLuint index, GLdouble v0, GLdouble v1, const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);
			void VertexAttrib2dv(GLuint index, const GLdouble *v, const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);
			void VertexAttrib2f(GLuint index, GLfloat v0, GLfloat v1, const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);
			void VertexAttrib2fv(GLuint index, const GLfloat *v, const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);
			void VertexAttrib2s(GLuint index, GLshort v0, GLshort v1, const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);
			void VertexAttrib2sv(GLuint index, const GLshort *v, const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);
			void VertexAttrib3d(GLuint index, GLdouble v0, GLdouble v1, GLdouble v2, const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);
			void VertexAttrib3dv(GLuint index, const GLdouble *v, const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);
			void VertexAttrib3f(GLuint index, GLfloat v0, GLfloat v1, GLfloat v2, const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);
			void VertexAttrib3fv(GLuint index, const GLfloat *v, const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);
			void VertexAttrib3s(GLuint index, GLshort v0, GLshort v1, GLshort v2, const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);
			void VertexAttrib3sv(GLuint index, const GLshort *v, const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);
			void VertexAttrib4bv(GLuint index, const GLbyte *v, const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);
			void VertexAttrib4d(GLuint index, GLdouble v0, GLdouble v1, GLdouble v2, GLdouble v3, const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);
			void VertexAttrib4dv(GLuint index, const GLdouble *v, const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);
			void VertexAttrib4f(GLuint index, GLfloat v0, GLfloat v1, GLfloat v2, GLfloat v3, const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);
			void VertexAttrib4fv(GLuint index, const GLfloat *v, const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);
			void VertexAttrib4iv(GLuint index, const GLint *v, const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);
			void VertexAttrib4Nbv(GLuint index, const GLbyte *v, const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);
			void VertexAttrib4Niv(GLuint index, const GLint *v, const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);
			void VertexAttrib4Nsv(GLuint index, const GLshort *v, const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);
			void VertexAttrib4Nub(GLuint index, GLubyte v0, GLubyte v1, GLubyte v2, GLubyte v3, const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);
			void VertexAttrib4Nubv(GLuint index, const GLubyte *v, const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);
			void VertexAttrib4Nuiv(GLuint index, const GLuint *v, const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);
			void VertexAttrib4Nusv(GLuint index, const GLushort *v, const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);
			void VertexAttrib4s(GLuint index, GLshort v0, GLshort v1, GLshort v2, GLshort v3, const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);
			void VertexAttrib4sv(GLuint index, const GLshort *v, const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);
			void VertexAttrib4ubv(GLuint index, const GLubyte *v, const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);
			void VertexAttrib4uiv(GLuint index,const GLuint *v, const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);
			void VertexAttrib4usv(GLuint index, const GLushort *v, const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);
			void VertexAttribBinding(GLuint attribindex, GLuint bindingindex, const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);
			void VertexAttribDivisor(GLuint index, GLuint divisor, const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);
			void VertexAttribFormat(GLuint attribindex, GLint size, GLenum type, GLboolean normalized, GLuint relativeoffset, const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);
			void VertexAttribI1i(GLuint index, GLint v0, const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);
			void VertexAttribI1iv(GLuint index,const GLint *v, const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);
			void VertexAttribI1ui(GLuint index, GLuint v0, const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);
			void VertexAttribI1uiv(GLuint index, const GLuint *v, const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);
			void VertexAttribI2i(GLuint index, GLint v0, GLint v1, const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);
			void VertexAttribI2iv(GLuint index, const GLint *v, const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);
			void VertexAttribI2ui(GLuint index, GLuint v0, GLuint v1, const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);
			void VertexAttribI2uiv(GLuint index, const GLuint *v, const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);
			void VertexAttribI3i(GLuint index, GLint v0, GLint v1, GLint v2, const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);
			void VertexAttribI3iv(GLuint index, const GLint *v, const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);
			void VertexAttribI3ui(GLuint index, GLuint v0, GLuint v1, GLuint v2, const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);
			void VertexAttribI3uiv(GLuint index, const GLuint *v, const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);
			void VertexAttribI4bv(GLuint index, const GLbyte *v, const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);
			void VertexAttribI4i(GLuint index, GLint v0, GLint v1, GLint v2, GLint v3, const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);
			void VertexAttribI4iv(GLuint index, const GLint *v, const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);
			void VertexAttribI4sv(GLuint index, const GLshort *v, const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);
			void VertexAttribI4ubv(GLuint index, const GLubyte *v, const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);
			void VertexAttribI4ui(GLuint index, GLuint v0, GLuint v1, GLuint v2, GLuint v3, const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);
			void VertexAttribI4uiv(GLuint index, const GLuint *v, const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);
			void VertexAttribI4usv(GLuint index, const GLushort *v, const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);
			void VertexAttribIFormat(GLuint attribindex, GLint size, GLenum type, GLuint relativeoffset, const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);
			void VertexAttribIPointer(GLuint index, GLint size, GLenum type, GLsizei stride, const GLvoid * pointer, const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);
			void VertexAttribL1d(GLuint index, GLdouble v0, const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);
			void VertexAttribL1dv(GLuint index, const GLdouble *v, const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);
			void VertexAttribL2d(GLuint index, GLdouble v0, GLdouble v1, const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);
			void VertexAttribL2dv(GLuint index, const GLdouble *v, const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);
			void VertexAttribL3d(GLuint index, GLdouble v0, GLdouble v1, GLdouble v2, const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);
			void VertexAttribL3dv(GLuint index, const GLdouble *v, const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);
			void VertexAttribL4d(GLuint index, GLdouble v0, GLdouble v1, GLdouble v2, GLdouble v3, const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);
			void VertexAttribL4dv(GLuint index, const GLdouble *v, const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);
			void VertexAttribLFormat(GLuint attribindex, GLint size, GLenum type, GLuint relativeoffset, const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);
			void VertexAttribLPointer(GLuint index, GLint size, GLenum type, GLsizei stride, const GLvoid * pointer, const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);
			void VertexAttribP1ui(GLuint index, GLenum type, GLboolean normalized, GLuint value, const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);
			void VertexAttribP2ui(GLuint index, GLenum type, GLboolean normalized, GLuint value, const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);
			void VertexAttribP3ui(GLuint index, GLenum type, GLboolean normalized, GLuint value, const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);
			void VertexAttribP4ui(GLuint index, GLenum type, GLboolean normalized, GLuint value, const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);
			void VertexAttribPointer(GLuint index, GLint size, GLenum type, GLboolean normalized, GLsizei stride, const GLvoid * pointer, const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);
			void VertexBindingDivisor(GLuint bindingindex, GLuint divisor, const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);
			

			///-----------------------------------------------------------------------------------------------------------------------
			///blend
			void BlendColor(GLfloat red, GLfloat green, GLfloat blue, GLfloat alpha, const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);
			void BlendEquation(GLenum mode, const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);
			void BlendEquationi(GLuint buf, GLenum mode, const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);
			void BlendEquationSeparate(GLenum modeRGB, GLenum modeAlpha, const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);
			void BlendEquationSeparatei(GLuint buf, GLenum modeRGB, GLenum modeAlpha, const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);
			void BlendFunc(GLenum sfactor, GLenum dfactor, const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);
			void BlendFunci(GLuint buf, GLenum sfactor, GLenum dfactor, const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);
			void BlendFuncSeparate(GLenum srcRGB, GLenum dstRGB, GLenum srcAlpha, GLenum dstAlpha, const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);
			void BlendFuncSeparatei(GLuint buf, GLenum srcRGB, GLenum dstRGB, GLenum srcAlpha, GLenum dstAlpha, const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);


			///-----------------------------------------------------------------------------------------------------------------------
			///buffer
			void BindBuffer(GLenum target, GLuint buffer, const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);
			void BindBufferBase(GLenum target, GLuint index, GLuint buffer, const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);
			void BindBufferRange(GLenum target, GLuint index, GLuint buffer, GLintptr offset, GLsizeiptr size, const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);
			void BindBuffersBase(GLenum target, GLuint first, GLsizei count,	const GLuint *buffers, const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);
			void BindBuffersRange(GLenum target, GLuint first, GLsizei count, const GLuint *buffers, const GLintptr *offsets, const GLintptr *sizes, const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);
			void BufferData(GLenum target, GLsizeiptr size, const GLvoid * data,	GLenum usage, const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);
			void BufferStorage(GLenum target, GLsizei size, const void *data, GLbitfield flags, const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);
			void BufferSubData(GLenum target, GLintptr offset, GLsizeiptr size, const GLvoid * data, const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);
			void ClearBufferData(GLenum target, GLenum internalformat, GLenum format, GLenum type, const void * data, const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);
			void ClearBufferSubData(GLenum target, GLenum internalformat, GLintptr offset, GLsizeiptr size, GLenum format, GLenum type, const void * data, const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);
			void ClearNamedBufferData(GLuint buffer, GLenum internalformat, GLenum format, GLenum type, const void *data, const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);
			void ClearNamedBufferSubData(GLuint buffer, GLenum internalformat, GLintptr offset, GLsizei size, GLenum format, GLenum type, const void *data, const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);
			void CopyBufferSubData(GLenum readTarget, GLenum writeTarget, GLintptr readOffset, GLintptr writeOffset, GLsizeiptr size, const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);
			void CopyNamedBufferSubData(GLuint readBuffer, GLuint writeBuffer, GLintptr readOffset, GLintptr writeOffset, GLsizei size, const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);
			void CreateBuffers(GLsizei n, GLuint *buffers, const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);
			void DeleteBuffers(GLsizei n, const GLuint * buffers, const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);
			void FlushMappedBufferRange(GLenum target, GLintptr offset, GLsizeiptr length, const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);
			void FlushMappedNamedBufferRange(GLuint buffer, GLintptr offset, GLsizei length, const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);
			void GenBuffers(GLsizei n, GLuint * buffers, const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);
			void InvalidateBufferData(GLuint buffer, const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);
			void InvalidateBufferSubData(GLuint buffer, GLintptr offset, GLsizeiptr length, const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);
			void* MapBuffer(GLenum target, GLenum access, const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);
			void* MapBufferRange(GLenum target, GLintptr offset, GLsizeiptr length, GLbitfield access, const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);
			void* MapNamedBuffer(GLuint buffer, GLenum access, const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);
			void* MapNamedBufferRange(GLuint buffer, GLintptr offset, GLsizei length, GLbitfield access, const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);
			void NamedBufferData(GLuint buffer, GLsizei size, const void *data, GLenum usage, const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);
			void NamedBufferStorage(GLuint buffer, GLsizei size, const void *data, GLbitfield flags, const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);
			void NamedBufferSubData(GLuint buffer, GLintptr offset, GLsizei size, const void *data, const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);
			GLboolean UnmapBuffer(GLenum target, const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);
			GLboolean UnmapNamedBuffer(GLuint buffer, const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);
		
			
			///-----------------------------------------------------------------------------------------------------------------------
			/// compute
			void DispatchCompute(GLuint num_groups_x, GLuint num_groups_y, GLuint num_groups_z, const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);
			void DispatchComputeIndirect(GLintptr indirect, const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);


			///-----------------------------------------------------------------------------------------------------------------------
			/// culling
			void ClipControl(GLenum origin, GLenum depth, const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);
			void CullFace(GLenum mode, const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);
			void FrontFace(GLenum mode, const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);


			///-----------------------------------------------------------------------------------------------------------------------
			/// debug
			void DebugMessageCallback(GLDEBUGPROC callback, void * userParam, const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);
			void DebugMessageControl(GLenum source, GLenum type, GLenum severity, GLsizei count, const GLuint *ids, GLboolean enabled, const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);
			void DebugMessageInsert(GLenum source, GLenum type, GLuint id, GLenum severity, GLsizei length, const char *message, const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);
			void ObjectLabel(GLenum identifier, GLuint name, GLsizei length, const char * label, const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);
			void ObjectPtrLabel(void * ptr, GLsizei length, const char * label, const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);
			void PopDebugGroup(const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);
			void PushDebugGroup(GLenum source, GLuint id, GLsizei length, const char * message, const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);


			///-----------------------------------------------------------------------------------------------------------------------
			/// depth
			void ClearDepth(GLdouble depth, const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);
			void ClearDepthf(GLfloat depth, const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);
			void DepthFunc(GLenum func, const char* file = nullptr, const char* funcp = nullptr, unsigned int line = 0);
			void DepthMask(GLboolean flag, const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);
			void DepthRange(GLdouble nearVal, GLdouble farVal, const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);
			void DepthRangef(GLfloat nearVal, GLfloat farVal, const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);
			void DepthRangeArrayv(GLuint first, GLsizei count, const GLdouble *v, const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);
			void DepthRangeIndexed(GLuint index, GLdouble nearVal, GLdouble farVal, const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);


			///-----------------------------------------------------------------------------------------------------------------------
			/// draw
			void DrawArrays(GLenum mode, GLint first, GLsizei count, const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);
			void DrawArraysIndirect(GLenum mode, const void *indirect, const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);
			void DrawArraysInstanced(GLenum mode, GLint first, GLsizei count, GLsizei primcount, const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);
			void DrawArraysInstancedBaseInstance(GLenum mode, GLint first, GLsizei count, GLsizei primcount, GLuint baseinstance, const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);
			void DrawElements(GLenum mode, GLsizei count, GLenum type, const GLvoid * indices, const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);
			void DrawElementsBaseVertex(GLenum mode, GLsizei count, GLenum type, GLvoid *indices, GLint basevertex, const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);
			void DrawElementsIndirect(GLenum mode, GLenum type, const void *indirect, const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);
			void DrawElementsInstanced(GLenum mode, GLsizei count, GLenum type, const void * indices, GLsizei primcount,const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);
			void DrawElementsInstancedBaseInstance(GLenum mode,	GLsizei count, GLenum type, const void * indices, GLsizei primcount, GLuint baseinstance, const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);
			void DrawElementsInstancedBaseVertex(GLenum mode, GLsizei count, GLenum type, GLvoid *indices, GLsizei primcount, GLint basevertex, const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);
			void DrawElementsInstancedBaseVertexBaseInstance(GLenum mode, GLsizei count, GLenum type, GLvoid *indices, GLsizei primcount, GLint basevertex, GLuint baseinstance, const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);
			void DrawRangeElements(GLenum mode, GLuint start, GLuint end, GLsizei count, GLenum type, const GLvoid * indices, const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);
			void DrawRangeElementsBaseVertex(GLenum mode, GLuint start, GLuint end, GLsizei count, GLenum type, GLvoid *indices, GLint basevertex, const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);
			void DrawTransformFeedback(GLenum mode, GLuint id, const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);
			void DrawTransformFeedbackInstanced(GLenum mode, GLuint id, GLsizei primcount, const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);
			void DrawTransformFeedbackStream(GLenum mode, GLuint id, GLuint stream, const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);
			void DrawTransformFeedbackStreamInstanced(GLenum mode, GLuint id, GLuint stream, GLsizei primcount, const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);
			void MultiDrawArrays(GLenum mode, const GLint * first, const GLsizei * count, GLsizei drawcount, const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);
			void MultiDrawArraysIndirect(GLenum mode, const void *indirect, GLsizei drawcount, GLsizei stride, const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);
			void MultiDrawElements(GLenum mode, const GLsizei * count, GLenum type, const GLvoid ** indices, GLsizei drawcount, const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);
			void MultiDrawElementsBaseVertex(GLenum mode, const GLsizei *count, GLenum type, const GLvoid **indices, GLsizei drawcount, const GLint *basevertex, const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);
			void MultiDrawElementsIndirect(GLenum mode, GLenum type, const void *indirect, GLsizei drawcount, GLsizei stride, const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);


			///-----------------------------------------------------------------------------------------------------------------------
			/// enablers
			void Disable(GLenum cap, const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);
			void Disablei(GLenum cap, GLuint index, const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);
			void Enable(GLenum cap, const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);
			void Enablei(GLenum cap, GLuint index, const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);
			void DisableVertexArrayAttrib(GLuint vaobj, GLuint index, const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);
			void DisableVertexAttribArray(GLuint index, const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);
			void EnableVertexArrayAttrib(GLuint vaobj, GLuint index, const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);
			void EnableVertexAttribArray(GLuint index, const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);
			void Hint(GLenum target, GLenum mode, const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);

			
			///-----------------------------------------------------------------------------------------------------------------------
			/// framebuffer
			void BindFramebuffer(GLenum target, GLuint framebuffer, const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);
			void BindRenderbuffer(GLenum target, GLuint renderbuffer, const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);
			void BlitFramebuffer(GLint srcX0, GLint srcY0, GLint srcX1, GLint srcY1, GLint dstX0, GLint dstY0, GLint dstX1, GLint dstY1, GLbitfield mask, GLenum filter, const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);
			void BlitNamedFramebuffer(GLuint readFramebuffer, GLuint drawFramebuffer, GLint srcX0, GLint srcY0, GLint srcX1, GLint srcY1, GLint dstX0, GLint dstY0, GLint dstX1, GLint dstY1, GLbitfield mask, GLenum filter, const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);
			GLenum CheckFramebufferStatus(GLenum target, const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);
			GLenum CheckNamedFramebufferStatus(GLuint framebuffer, GLenum target, const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);
			void ClampColor(GLenum target, GLenum clamp, const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);
			void Clear(GLbitfield mask, const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);
			void ClearBufferfi(GLenum buffer, GLint drawbuffer, GLfloat depth, GLint stencil, const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);
			void ClearBufferfv(GLenum buffer,GLint drawbuffer, const GLfloat * value, const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);
			void ClearBufferiv(GLenum buffer, GLint drawbuffer, const GLint * value, const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);
			void ClearBufferuiv(GLenum buffer, GLint drawbuffer, const GLuint * value, const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);
			void ClearColor(GLfloat red, GLfloat green, GLfloat blue, GLfloat alpha, const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);
			void ClearNamedFramebufferfi(GLuint framebuffer, GLenum buffer, GLint drawbuffer, GLfloat depth, GLint stencil, const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);
			void ClearNamedFramebufferfv(GLuint framebuffer, GLenum buffer, GLint drawbuffer, const GLfloat * value, const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);
			void ClearNamedFramebufferiv(GLuint framebuffer, GLenum buffer, GLint drawbuffer, const GLint * value, const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);
			void ClearNamedFramebufferuiv(GLuint framebuffer, GLenum buffer, GLint drawbuffer, const GLuint * value, const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);
			void ColorMask(GLboolean red, GLboolean green, GLboolean blue, GLboolean alpha, const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);
			void ColorMaski(GLuint buf, GLboolean red, GLboolean green, GLboolean blue, GLboolean alpha, const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);
			void CreateFramebuffers(GLsizei n, GLuint *ids, const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);
			void CreateRenderbuffers(GLsizei n, GLuint *renderbuffers, const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);
			void DrawBuffer(GLenum buf, const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);
			void DrawBuffers(GLsizei n, const GLenum *bufs, const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);
			void DeleteFramebuffers(GLsizei n, GLuint *framebuffers, const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);
			void DeleteRenderbuffers(GLsizei n, GLuint *renderbuffers, const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);
			void FramebufferParameteri(GLenum target, GLenum pname,GLint param, const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);
			void FramebufferRenderbuffer(GLenum target, GLenum attachment, GLenum renderbuffertarget, GLuint renderbuffer, const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);
			void FramebufferTexture(GLenum target, GLenum attachment, GLuint texture, GLint level, const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);
			void FramebufferTexture1D(GLenum target, GLenum attachment, GLenum textarget, GLuint texture, GLint level, const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);
			void FramebufferTexture2D(GLenum target, GLenum attachment, GLenum textarget, GLuint texture, GLint level, const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);
			void FramebufferTexture3D(GLenum target, GLenum attachment, GLenum textarget, GLuint texture, GLint level, GLint layer, const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);
			void FramebufferTextureLayer(GLenum target, GLenum attachment, GLuint texture, GLint level, GLint layer, const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);
			void GenFramebuffers(GLsizei n, GLuint *ids, const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);
			void GenRenderbuffers(GLsizei n, GLuint *renderbuffers, const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);
			void InvalidateFramebuffer(GLenum target, GLsizei numAttachments, const GLenum * attachments, const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);
			void InvalidateNamedFramebufferData(GLuint framebuffer,GLsizei numAttachments,const GLenum *attachments, const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);
			void InvalidateNamedFramebufferSubData(GLuint framebuffer, GLsizei numAttachments, const GLenum * attachments, GLint x, GLint y, GLint width, GLint height, const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);
			void InvalidateSubFramebuffer(GLenum target, GLsizei numAttachments, const GLenum * attachments, GLint x, GLint y, GLint width, GLint height, const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);
			void NamedFramebufferDrawBuffer(GLuint framebuffer,	GLenum buf, const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);
			void NamedFramebufferDrawBuffers(GLuint framebuffer, GLsizei n, const GLenum *bufs, const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);
			void NamedFramebufferParameteri(GLuint texture, GLenum pname, GLint param, const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);
			void NamedFramebufferReadBuffer(GLuint framebuffer, GLenum mode, const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);
			void NamedFramebufferRenderbuffer(GLuint texture, GLenum attachment, GLenum renderbuffertarget, GLuint renderbuffer, const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);
			void NamedFramebufferTexture(GLuint framebuffer, GLenum attachment, GLuint texture, GLint level, const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);
			void NamedFramebufferTextureLayer(GLuint framebuffer, GLenum attachment, GLuint texture, GLint level, GLint layer, const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);
			void NamedRenderbufferStorage(GLuint renderbuffer, GLenum internalformat, GLsizei width, GLsizei height, const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);
			void NamedRenderbufferStorageMultisample(GLuint renderbuffer, GLsizei samples, GLenum internalformat, GLsizei width, GLsizei height, const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);
			void RenderbufferStorage(GLenum target, GLenum internalformat, GLsizei width, GLsizei height, const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);
			void RenderbufferStorageMultisample(GLenum target, GLsizei samples, GLenum internalformat, GLsizei width, GLsizei height, const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);
			void ReadBuffer(GLenum mode, const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);
			void ReadnPixels(GLint x, GLint y, GLsizei width, GLsizei height, GLenum format, GLenum type, GLsizei bufSize, void *data, const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);
			void ReadPixels(GLint x, GLint y, GLsizei width, GLsizei height, GLenum format, GLenum type, GLvoid * data, const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);


			///-----------------------------------------------------------------------------------------------------------------------
			/// image
			void BindImageTexture(GLuint unit, GLuint texture, GLint level, GLboolean layered, GLint layer, GLenum access, GLenum format, const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);
			void BindImageTextures(GLuint first, GLsizei count, const GLuint *textures, const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);


			///-----------------------------------------------------------------------------------------------------------------------
			/// query
			void BeginConditionalRender(GLuint id, GLenum mode, const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);
			void BeginQuery(GLenum target, GLuint id, const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);
			void BeginQueryIndexed(GLenum target, GLuint index, GLuint id, const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);
			void CreateQueries(GLenum target, GLsizei n, GLuint *ids, const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);
			void DeleteQueries(GLsizei n, const GLuint * ids, const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);
			void EndConditionalRender(const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);
			void EndQuery(GLenum target, const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);
			void EndQueryIndexed(GLenum target, GLuint index, const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);
			void GenQueries(GLsizei n, GLuint * ids, const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);
			void QueryCounter(GLuint id,GLenum target, const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);


			///-----------------------------------------------------------------------------------------------------------------------
			/// pixel store
			void PixelStoref(GLenum pname, GLfloat param, const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);
			void PixelStorei(GLenum pname, GLint param, const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);


			///-----------------------------------------------------------------------------------------------------------------------
			/// rasterization
			void LineWidth(GLfloat width, const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);
			void MinSampleShading(GLfloat value, const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);
			void PointParameterf(GLenum pname, GLfloat param, const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);
			void PointParameterfv(GLenum pname, const GLfloat* params, const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);
			void PointParameteri(GLenum pname, GLint param, const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);
			void PointParameteriv(GLenum pname, const GLint* params, const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);
			void PointSize(GLfloat size, const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);
			void PolygonMode(GLenum face, GLenum mode, const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);
			void PolygonOffset(GLfloat factor, GLfloat units, const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);
			void SampleCoverage(GLfloat value, GLboolean invert, const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);
			void SampleMaski(GLuint maskNumber, GLbitfield mask, const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);
			void LogicOp(GLenum opcode, const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);
			void Viewport(GLint x, GLint y, GLsizei width, GLsizei height, const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);
			void ViewportArrayv(GLuint first, GLsizei count, const GLfloat *v, const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);
			void ViewportIndexedf(GLuint index,	GLfloat x, GLfloat y, GLfloat w, GLfloat h, const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);
			void ViewportIndexedfv(GLuint index, const GLfloat *v, const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);


			///-----------------------------------------------------------------------------------------------------------------------
			/// sampler
			void BindSampler(GLuint unit, GLuint sampler, const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);
			void BindSamplers(GLuint first, GLsizei count, const GLuint *samplers, const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);
			void CreateSamplers(GLsizei n, GLuint *samplers, const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);
			void DeleteSamplers(GLsizei n, const GLuint * samplers, const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);
			void GenSamplers(GLsizei n, GLuint *samplers, const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);
			void SamplerParameterf(GLuint sampler, GLenum pname, GLfloat param, const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);
			void SamplerParameterfv(GLuint sampler, GLenum pname, const GLfloat * params, const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);
			void SamplerParameteri(GLuint sampler, GLenum pname, GLint param,const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);
			void SamplerParameterIiv(GLuint sampler, GLenum pname, const GLint *params, const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);
			void SamplerParameterIuiv(GLuint sampler, GLenum pname, const GLuint *params, const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);
			void SamplerParameteriv(GLuint sampler, GLenum pname, const GLint * params, const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);


			///-----------------------------------------------------------------------------------------------------------------------
			/// scissor
			void Scissor(GLint x, GLint y, GLsizei width, GLsizei height, const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);
			void ScissorArrayv(GLuint first, GLsizei count, const GLint *v, const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);
			void ScissorIndexed(GLuint index, GLint left, GLint bottom, GLsizei width, GLsizei height, const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);
			void ScissorIndexedv(GLuint index, const GLint *vconst, const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);
			
			///-----------------------------------------------------------------------------------------------------------------------
			/// shader
			void ActiveShaderProgram(GLuint pipeline, GLuint program, const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);
			void AttachShader(GLuint program, GLuint shader, const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);
			void BindAttribLocation(GLuint program, GLuint index, const GLchar *name, const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);
			void BindFragDataLocation(GLuint program, GLuint colorNumber, const char * name, const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);
			void BindFragDataLocationIndexed(GLuint program, GLuint colorNumber, GLuint index, const char* name, const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);
			void BindProgramPipeline(GLuint pipeline, const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);
			void CompileShader(GLuint shader, const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);
			GLuint CreateProgram( const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);
			void CreateProgramPipelines(GLsizei n, GLuint *pipelines, const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);
			GLuint CreateShader(GLenum shaderType, const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);
			GLuint CreateShaderProgramv(GLenum type, GLsizei count, const char **strings, const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);
			void DeleteProgram(GLuint program, const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);
			void DeleteProgramPipelines(GLsizei n, const GLuint *pipelines, const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);
			void DeleteShader(GLuint shader, const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);
			void DetachShader(GLuint program, GLuint shader, const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);
			void GenProgramPipelines(GLsizei n, GLuint *pipelines, const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);
			void LinkProgram(GLuint program, const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);
			void ProgramBinary(GLuint program, GLenum binaryFormat, const void *binary, GLsizei length, const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);
			void ProgramParameteri(GLuint program, GLenum pname, GLint value, const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);
			void ReleaseShaderCompiler( const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);
			void ShaderBinary(GLsizei count, const GLuint *shaders, GLenum binaryFormat, const void *binary, GLsizei length, const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);
			void ShaderSource(GLuint shader, GLsizei count, const GLchar **string, const GLint *length, const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);
			void ShaderStorageBlockBinding(GLuint program, GLuint storageBlockIndex, GLuint storageBlockBinding, const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);
			void UseProgram(GLuint program, const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);
			void UseProgramStages(GLuint pipeline, GLbitfield stages, GLuint program, const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);
			void ValidateProgram(GLuint program, const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);
			void ValidateProgramPipeline(GLuint pipeline, const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);


			///-----------------------------------------------------------------------------------------------------------------------
			/// stencil
			void ClearStencil(GLint s, const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);
			void StencilFunc(GLenum func, GLint ref, GLuint mask, const char* file = nullptr, const char* funcstr = nullptr, unsigned int line = 0);
			void StencilFuncSeparate(GLenum face, GLenum func, GLint ref, GLuint mask, const char* file = nullptr, const char* funcstr = nullptr, unsigned int line = 0);
			void StencilMask(GLuint mask, const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);
			void StencilMaskSeparate(GLenum face, GLuint mask, const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);
			void StencilOp(GLenum sfail, GLenum dpfail, GLenum dppass, const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);
			void StencilOpSeparate(GLenum face, GLenum sfail, GLenum dpfail, GLenum dppass, const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);

			
			///-----------------------------------------------------------------------------------------------------------------------
			/// synchronization
			GLenum ClientWaitSync(GLsync sync, GLbitfield flags, GLuint64 timeout, const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);
			void DeleteSync(GLsync sync, const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);
			GLsync FenceSync(GLenum condition, GLbitfield flags, const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);
			void Finish(const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);
			void Flush(const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);
			void MemoryBarrier(GLbitfield barriers, const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);
			void MemoryBarrierByRegion(GLbitfield barriers, const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);
			void TextureBarrier( const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);
			void WaitSync(GLsync sync, GLbitfield flags, GLuint64 timeout, const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);


			///-----------------------------------------------------------------------------------------------------------------------
			/// texture
			//void Internal
			void ActiveTexture(GLenum texture, const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);
			void BindTexture(GLenum target, GLuint texture, const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);
			void BindTextures(GLuint first, GLsizei count, const GLuint *textures, const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);
			void BindTextureUnit(GLuint unit, GLuint texture, const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);
			void ClearTexImage(GLuint texture, GLint level, GLenum format, GLenum type, const void * data, const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);
			void ClearTexSubImage(GLuint texture, GLint level, GLint xoffset, GLint yoffset, GLint zoffset, GLsizei width, GLsizei height, GLsizei depth, GLenum format, GLenum type, const void * data, const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);
			void CompressedTexImage1D(GLenum target, GLint level, GLenum internalformat, GLsizei width, GLint border, GLsizei imageSize, const GLvoid * data, const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);
			void CompressedTexImage2D(GLenum target, GLint level, GLenum internalformat, GLsizei width, GLsizei height, GLint border, GLsizei imageSize, const GLvoid * data, const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);
			void CompressedTexImage3D(GLenum target, GLint level, GLenum internalformat, GLsizei width, GLsizei height, GLsizei depth, GLint border, GLsizei imageSize, const GLvoid * data, const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);
			void CompressedTexSubImage1D(GLenum target, GLint level, GLint xoffset, GLsizei width, GLenum format, GLsizei imageSize, const GLvoid * data, const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);
			void CompressedTexSubImage2D(GLenum target, GLint level, GLint xoffset, GLint yoffset, GLsizei width, GLsizei height, GLenum format, GLsizei imageSize, const GLvoid * data, const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);
			void CompressedTexSubImage3D(GLenum target, GLint level, GLint xoffset, GLint yoffset, GLint zoffset, GLsizei width, GLsizei height, GLsizei depth, GLenum format, GLsizei imageSize, const GLvoid * data, const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);
			void CompressedTextureSubImage1D(GLuint texture, GLint level, GLint xoffset, GLsizei width, GLenum format, GLsizei imageSize, const GLvoid * data, const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);
			void CompressedTextureSubImage2D(GLuint texture, GLint level, GLint xoffset, GLint yoffset, GLsizei width, GLsizei height, GLenum format, GLsizei imageSize, const GLvoid * data, const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);
			void CompressedTextureSubImage3D(GLuint texture, GLint level, GLint xoffset, GLint yoffset, GLint zoffset, GLsizei width, GLsizei height, GLsizei depth, GLenum format, GLsizei imageSize, const GLvoid * data, const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);
			void CopyImageSubData(GLuint srcName, GLenum srcTarget, GLint srcLevel, GLint srcX, GLint srcY, GLint srcZ, GLuint dstName, GLenum dstTarget, GLint dstLevel, GLint dstX, GLint dstY, GLint dstZ, GLsizei srcWidth, GLsizei srcHeight, GLsizei srcDepth, const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);
			void CopyTexImage1D(GLenum target, GLint level, GLenum internalformat, GLint x, GLint y, GLsizei width, GLint border, const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);
			void CopyTexImage2D(GLenum target, GLint level, GLenum internalformat, GLint x, GLint y, GLsizei width, GLsizei height, GLint border, const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);
			void CopyTexSubImage1D(GLenum target, GLint level, GLint xoffset, GLint x, GLint y, GLsizei width, const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);
			void CopyTexSubImage2D(GLenum target, GLint level, GLint xoffset, GLint yoffset, GLint x, GLint y, GLsizei width, GLsizei height, const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);
			void CopyTexSubImage3D(GLenum target, GLint level, GLint xoffset, GLint yoffset, GLint zoffset, GLint x, GLint y, GLsizei width, GLsizei height, const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);
			void CopyTextureSubImage1D(GLuint texture, GLint level, GLint xoffset, GLint x, GLint y, GLsizei width, const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);
			void CopyTextureSubImage2D(GLuint texture, GLint level, GLint xoffset, GLint yoffset, GLint x, GLint y, GLsizei width, GLsizei height, const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);
			void CopyTextureSubImage3D(GLuint target, GLint level, GLint xoffset, GLint yoffset, GLint zoffset, GLint x, GLint y, GLsizei width, GLsizei height, const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);
			void CreateTextures(GLenum target, GLsizei n, GLuint *textures, const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);
			void DeleteTextures(GLsizei n, const GLuint *textures, const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);
			void GenerateMipmap(GLenum target, const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);
			void GenerateTextureMipmap(GLuint texture, const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);
			void GenTextures(GLsizei n, GLuint * textures, const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);
			void InvalidateTexImage(GLuint texture, GLint level, const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);
			void InvalidateTexSubImage(GLuint texture, GLint level, GLint xoffset, GLint yoffset, GLint zoffset, GLsizei width, GLsizei height, GLsizei depth, const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);
			void TexBuffer(GLenum target, GLenum internalFormat, GLuint buffer, const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);
			void TexBufferRange(GLenum target, GLenum internalFormat, GLuint buffer, GLintptr offset, GLsizeiptr size, const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);
			void TexImage1D(GLenum target, GLint level, GLint internalFormat, GLsizei width, GLint border, GLenum format, GLenum type, const GLvoid * data, const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);
			void TexImage2D(GLenum target, GLint level, GLint internalFormat, GLsizei width, GLsizei height, GLint border, GLenum format, GLenum type, const GLvoid * data, const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);
			void TexImage2DMultisample(GLenum target, GLsizei samples, GLenum internalformat, GLsizei width, GLsizei height, GLboolean fixedsamplelocations, const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);
			void TexImage3D(GLenum target, GLint level, GLint internalFormat, GLsizei width, GLsizei height, GLsizei depth, GLint border, GLenum format, GLenum type, const GLvoid * data, const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);
			void TexImage3DMultisample(GLenum target, GLsizei samples, GLenum internalformat, GLsizei width, GLsizei height, GLsizei depth, GLboolean fixedsamplelocations, const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);
			void TexParameterf(GLenum target, GLenum pname, GLfloat param, const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);
			void TexParameterfv(GLenum target, GLenum pname, const GLfloat * params, const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);
			void TexParameteri(GLenum target, GLenum pname, GLint param, const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);
			void TexParameterIiv(GLenum target, GLenum pname, const GLint * params, const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);
			void TexParameterIuiv(GLenum target, GLenum pname, const GLuint * params, const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);
			void TexParameteriv(GLenum target, GLenum pname, const GLint * params, const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);
			void TexStorage1D(GLenum target, GLsizei levels, GLenum internalformat, GLsizei width, const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);
			void TexStorage2D(GLenum target, GLsizei levels, GLenum internalformat, GLsizei width, GLsizei height, const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);
			void TexStorage2DMultisample(GLenum target, GLsizei samples, GLenum internalformat, GLsizei width, GLsizei height, GLboolean fixedsamplelocations, const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);
			void TexStorage3D(GLenum target, GLsizei levels, GLenum internalformat, GLsizei width, GLsizei height, GLsizei depth, const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);
			void TexStorage3DMultisample(GLenum target, GLsizei samples, GLenum internalformat, GLsizei width, GLsizei height, GLsizei depth, GLboolean fixedsamplelocations, const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);
			void TexSubImage1D(GLenum target, GLint level, GLint xoffset, GLsizei width, GLenum format, GLenum type, const GLvoid * pixels, const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);
			void TexSubImage2D(GLenum target, GLint level, GLint xoffset, GLint yoffset, GLsizei width, GLsizei height, GLenum format, GLenum type, const GLvoid * pixels, const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);
			void TexSubImage3D(GLenum target, GLint level, GLint xoffset, GLint yoffset, GLint zoffset, GLsizei width, GLsizei height, GLsizei depth, GLenum format, GLenum type, const GLvoid * pixels, const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);
			void TextureBuffer(GLuint texture, GLenum internalformat, GLuint buffer, const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);
			void TextureBufferRange(GLuint texture, GLenum internalformat, GLuint buffer, GLintptr offset, GLsizei size, const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);
			void TextureParameterf(GLuint texture, GLenum pname, GLfloat param, const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);
			void TextureParameterfv(GLuint texture, GLenum pname, const GLfloat * params, const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);
			void TextureParameteri(GLuint texture, GLenum pname, GLint param, const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);
			void TextureParameterIiv(GLuint texture, GLenum pname, const GLint * params, const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);
			void TextureParameterIuiv(GLuint texture, GLenum pname, const GLuint * params, const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);
			void TextureParameteriv(GLuint texture, GLenum pname, const GLint * params, const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);
			void TextureStorage1D(GLuint texture, GLsizei levels, GLenum internalformat, GLsizei width, const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);
			void TextureStorage2D(GLuint texture, GLsizei levels, GLenum internalformat, GLsizei width, GLsizei height, const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);
			void TextureStorage2DMultisample(GLuint texture, GLsizei samples, GLenum internalformat, GLsizei width, GLsizei height, GLboolean fixedsamplelocations, const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);
			void TextureStorage3D(GLuint texture, GLsizei levels, GLenum internalformat, GLsizei width, GLsizei height, GLsizei depth, const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);
			void TextureStorage3DMultisample(GLuint texture, GLsizei samples, GLenum internalformat, GLsizei width, GLsizei height, GLsizei depth, GLboolean fixedsamplelocations, const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);
			void TextureSubImage1D(GLuint texture, GLint level, GLint xoffset, GLsizei width, GLenum format, GLenum type, const GLvoid * pixels, const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);
			void TextureSubImage2D(GLuint texture, GLint level, GLint xoffset, GLint yoffset, GLsizei width, GLsizei height, GLenum format, GLenum type, const GLvoid * pixels, const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);
			void TextureSubImage3D(GLuint texture, GLint level, GLint xoffset, GLint yoffset, GLint zoffset, GLsizei width, GLsizei height, GLsizei depth, GLenum format, GLenum type, const GLvoid * pixels, const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);
			void TextureView(GLuint texture, GLenum target, GLuint origtexture, GLenum internalformat, GLuint minlevel, GLuint numlevels, GLuint minlayer, GLuint numlayers, const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);


			///-----------------------------------------------------------------------------------------------------------------------
			/// transform feedback
			void BeginTransformFeedback(GLenum primitiveMode, const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);
			void BindTransformFeedback(GLenum target, GLuint id, const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);
			void CreateTransformFeedbacks(GLsizei n, GLuint *ids, const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);
			void DeleteTransformFeedbacks(GLsizei n, const GLuint *ids, const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);
			void EndTransformFeedback(const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);
			void GenTransformFeedbacks(GLsizei n, GLuint *ids, const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);
			void PauseTransformFeedback(const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);
			void ResumeTransformFeedback(const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);
			void TransformFeedbackBufferBase(GLuint xfb, GLuint index, GLuint buffer, const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);
			void TransformFeedbackBufferRange(GLuint xfb, GLuint index, GLuint buffer, GLintptr offset, GLsizei size, const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);
			void TransformFeedbackVaryings(GLuint program, GLsizei count, const char **varyings, GLenum bufferMode, const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);


			///-----------------------------------------------------------------------------------------------------------------------
			/// uniform
			void InternalProgramUniform(GLuint program, GLuint location, GLfloat v0, GLfloat v1, GLfloat v2, GLfloat v3, GLint size, const char* file, const char* func, unsigned int line);
			void InternalProgramUniform(GLuint program, GLuint location, GLint count, GLvoid* v, GLenum type, GLint size, const char* file, const char* func, unsigned int line);
			void InternalProgramUniform(GLuint program, GLuint location, GLint count, GLboolean transpose, GLvoid* v, GLint size, const char* file, const char* func, unsigned int line, const char* detail = nullptr);
			void ProgramUniform1d(GLuint program, GLint location, GLdouble v0, const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);
			void ProgramUniform1dv(GLuint program, GLint location, GLsizei count, const GLdouble *value, const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);
			void ProgramUniform1f(GLuint program, GLint location, GLfloat v0, const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);
			void ProgramUniform1fv(GLuint program, GLint location, GLsizei count, const GLfloat *value, const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);
			void ProgramUniform1i(GLuint program, GLint location, GLint v0, const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);
			void ProgramUniform1iv(GLuint program, GLint location, GLsizei count, const GLint *value, const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);
			void ProgramUniform1ui(GLuint program, GLint location, GLuint v0, const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);
			void ProgramUniform1uiv(GLuint program, GLint location, GLsizei count, const GLuint *value, const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);
			void ProgramUniform2d(GLuint program, GLint location, GLdouble v0, GLdouble v1,const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);
			void ProgramUniform2dv(GLuint program, GLint location, GLsizei count, const GLdouble *value, const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);
			void ProgramUniform2f(GLuint program, GLint location, GLfloat v0, GLfloat v1, const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);
			void ProgramUniform2fv(GLuint program, GLint location, GLsizei count, const GLfloat *value, const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);
			void ProgramUniform2i(GLuint program, GLint location, GLint v0, GLint v1, const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);
			void ProgramUniform2iv(GLuint program, GLint location, GLsizei count, const GLint *value, const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);
			void ProgramUniform2ui(GLuint program, GLint location, GLuint v0, GLuint v1, const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);
			void ProgramUniform2uiv(GLuint program, GLint location, GLsizei count, const GLuint *value, const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);
			void ProgramUniform3d(GLuint program, GLint location, GLdouble v0, GLdouble v1, GLdouble v2, const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);
			void ProgramUniform3dv(GLuint program, GLint location, GLsizei count, const GLdouble *value, const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);
			void ProgramUniform3f(GLuint program, GLint location, GLfloat v0, GLfloat v1, GLfloat v2, const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);
			void ProgramUniform3fv(GLuint program, GLint location, GLsizei count, const GLfloat *value, const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);
			void ProgramUniform3i(GLuint program, GLint location, GLint v0, GLint v1, GLint v2, const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);
			void ProgramUniform3iv(GLuint program, GLint location, GLsizei count, const GLint *value, const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);
			void ProgramUniform3ui(GLuint program, GLint location, GLuint v0, GLuint v1, GLuint v2, const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);
			void ProgramUniform3uiv(GLuint program, GLint location, GLsizei count, const GLuint *value, const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);
			void ProgramUniform4d(GLuint program, GLint location, GLdouble v0, GLdouble v1, GLdouble v2, GLdouble v3, const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);
			void ProgramUniform4dv(GLuint program, GLint location, GLsizei count, const GLdouble *value, const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);
			void ProgramUniform4f(GLuint program, GLint location, GLfloat v0, GLfloat v1, GLfloat v2, GLfloat v3, const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);
			void ProgramUniform4fv(GLuint program, GLint location, GLsizei count, const GLfloat *value, const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);
			void ProgramUniform4i(GLuint program, GLint location, GLint v0, GLint v1, GLint v2, GLint v3, const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);
			void ProgramUniform4iv(GLuint program, GLint location, GLsizei count, const GLint *value, const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);
			void ProgramUniform4ui(GLuint program, GLint location, GLuint v0, GLuint v1, GLuint v2, GLuint v3, const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);
			void ProgramUniform4uiv(GLuint program, GLint location, GLsizei count, const GLuint *value, const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);
			void ProgramUniformMatrix2dv(GLuint program, GLint location, GLsizei count, GLboolean transpose, const GLdouble *value, const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);
			void ProgramUniformMatrix2fv(GLuint program, GLint location, GLsizei count, GLboolean transpose, const GLfloat *value, const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);
			void ProgramUniformMatrix2x3dv(GLuint program, GLint location, GLsizei count, GLboolean transpose, const GLdouble *value, const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);
			void ProgramUniformMatrix2x3fv(GLuint program, GLint location, GLsizei count, GLboolean transpose, const GLfloat *value, const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);
			void ProgramUniformMatrix2x4dv(GLuint program, GLint location, GLsizei count, GLboolean transpose, const GLdouble *value, const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);
			void ProgramUniformMatrix2x4fv(GLuint program, GLint location, GLsizei count, GLboolean transpose, const GLfloat *value, const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);
			void ProgramUniformMatrix3dv(GLuint program, GLint location, GLsizei count, GLboolean transpose, const GLdouble *value, const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);
			void ProgramUniformMatrix3fv(GLuint program, GLint location, GLsizei count, GLboolean transpose, const GLfloat *value, const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);
			void ProgramUniformMatrix3x2dv(GLuint program, GLint location, GLsizei count, GLboolean transpose, const GLdouble *value, const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);
			void ProgramUniformMatrix3x2fv(GLuint program, GLint location, GLsizei count, GLboolean transpose, const GLfloat *value, const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);
			void ProgramUniformMatrix3x4dv(GLuint program, GLint location, GLsizei count, GLboolean transpose, const GLdouble *value, const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);
			void ProgramUniformMatrix3x4fv(GLuint program, GLint location, GLsizei count, GLboolean transpose, const GLfloat *value, const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);
			void ProgramUniformMatrix4dv(GLuint program, GLint location, GLsizei count, GLboolean transpose, const GLdouble *value, const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);
			void ProgramUniformMatrix4fv(GLuint program, GLint location, GLsizei count, GLboolean transpose, const GLfloat *value, const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);
			void ProgramUniformMatrix4x2dv(GLuint program, GLint location, GLsizei count, GLboolean transpose, const GLdouble *value, const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);
			void ProgramUniformMatrix4x2fv(GLuint program, GLint location, GLsizei count, GLboolean transpose, const GLfloat *value, const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);
			void ProgramUniformMatrix4x3dv(GLuint program, GLint location, GLsizei count, GLboolean transpose, const GLdouble *value, const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);
			void ProgramUniformMatrix4x3fv(GLuint program, GLint location, GLsizei count, GLboolean transpose, const GLfloat *value, const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);
			void Uniform1d(GLint location, GLdouble v0, const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);
			void Uniform1dv(GLint location, GLsizei count, const GLdouble *value, const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);
			void Uniform1f(GLint location, GLfloat v0, const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);
			void Uniform1fv(GLint location, GLsizei count, const GLfloat *value, const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);
			void Uniform1i(GLint location, GLint v0, const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);
			void Uniform1iv(GLint location, GLsizei count, const GLint *value, const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);
			void Uniform1ui(GLint location, GLuint v0, const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);
			void Uniform1uiv(GLint location, GLsizei count, const GLuint *value, const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);
			void Uniform2d(GLint location, GLdouble v0, GLdouble v1, const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);
			void Uniform2dv(GLint location, GLsizei count, const GLdouble *value, const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);
			void Uniform2f(GLint location, GLfloat v0, GLfloat v1, const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);
			void Uniform2fv(GLint location, GLsizei count, const GLfloat *value, const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);
			void Uniform2i(GLint location, GLint v0, GLint v1, const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);
			void Uniform2iv(GLint location, GLsizei count, const GLint *value, const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);
			void Uniform2ui(GLint location, GLuint v0, GLuint v1, const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);
			void Uniform2uiv(GLint location, GLsizei count, const GLuint *value, const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);
			void Uniform3d(GLint location, GLdouble v0, GLdouble v1, GLdouble v2, const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);
			void Uniform3dv(GLint location, GLsizei count, const GLdouble *value, const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);
			void Uniform3f(GLint location, GLfloat v0, GLfloat v1, GLfloat v2, const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);
			void Uniform3fv(GLint location, GLsizei count, const GLfloat *value, const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);
			void Uniform3i(GLint location, GLint v0, GLint v1, GLint v2, const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);
			void Uniform3iv(GLint location, GLsizei count, const GLint *value, const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);
			void Uniform3ui(GLint location, GLuint v0, GLuint v1, GLuint v2, const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);
			void Uniform3uiv(GLint location, GLsizei count, const GLuint *value, const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);
			void Uniform4d(GLint location, GLdouble v0, GLdouble v1, GLdouble v2, GLdouble v3, const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);
			void Uniform4dv(GLint location, GLsizei count, const GLdouble *value, const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);
			void Uniform4f(GLint location, GLfloat v0, GLfloat v1, GLfloat v2, GLfloat v3, const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);
			void Uniform4fv(GLint location, GLsizei count, const GLfloat *value, const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);
			void Uniform4i(GLint location, GLint v0, GLint v1, GLint v2, GLint v3, const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);
			void Uniform4iv(GLint location, GLsizei count, const GLint *value, const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);
			void Uniform4ui(GLint location, GLuint v0, GLuint v1, GLuint v2, GLuint v3, const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);
			void Uniform4uiv(GLint location, GLsizei count, const GLuint *value, const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);
			void UniformBlockBinding(GLuint program, GLuint uniformBlockIndex, GLuint uniformBlockBinding, const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);
			void UniformMatrix2dv(GLint location, GLsizei count, GLboolean transpose, const GLdouble *value, const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);
			void UniformMatrix2fv(GLint location, GLsizei count, GLboolean transpose, const GLfloat *value, const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);
			void UniformMatrix2x3dv(GLint location, GLsizei count, GLboolean transpose, const GLdouble *value, const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);
			void UniformMatrix2x3fv(GLint location, GLsizei count, GLboolean transpose, const GLfloat *value, const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);
			void UniformMatrix2x4dv(GLint location, GLsizei count, GLboolean transpose, const GLdouble *value, const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);
			void UniformMatrix2x4fv(GLint location, GLsizei count, GLboolean transpose, const GLfloat *value, const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);
			void UniformMatrix3dv(GLint location, GLsizei count, GLboolean transpose, const GLdouble *value, const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);
			void UniformMatrix3fv(GLint location, GLsizei count, GLboolean transpose, const GLfloat *value, const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);
			void UniformMatrix3x2dv(GLint location, GLsizei count, GLboolean transpose, const GLdouble *value, const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);
			void UniformMatrix3x2fv(GLint location, GLsizei count, GLboolean transpose, const GLfloat *value, const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);
			void UniformMatrix3x4dv(GLint location, GLsizei count, GLboolean transpose, const GLdouble *value, const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);
			void UniformMatrix3x4fv(GLint location, GLsizei count, GLboolean transpose, const GLfloat *value, const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);
			void UniformMatrix4dv(GLint location, GLsizei count, GLboolean transpose, const GLdouble *value, const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);
			void UniformMatrix4fv(GLint location, GLsizei count, GLboolean transpose, const GLfloat *value, const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);
			void UniformMatrix4x2dv(GLint location, GLsizei count, GLboolean transpose, const GLdouble *value, const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);
			void UniformMatrix4x2fv(GLint location, GLsizei count, GLboolean transpose, const GLfloat *value, const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);
			void UniformMatrix4x3dv(GLint location, GLsizei count, GLboolean transpose, const GLdouble *value, const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);
			void UniformMatrix4x3fv(GLint location, GLsizei count, GLboolean transpose, const GLfloat *value, const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);
			void UniformSubroutinesuiv(GLenum shadertype, GLsizei count, const GLuint *indices, char* file = nullptr, const char* func = nullptr, unsigned int line = 0);
			

				
			///-----------------------------------------------------------------------------------------------------------------------
			/// gets
			GLenum GetError(const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);
			GLint GetUniformLocation(GLuint program, const GLchar *name, const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);

			void GetActiveAtomicCounterBufferiv(GLuint program, GLuint bufferIndex, GLenum pname, GLint *params, const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);
			void GetActiveAttrib(GLuint program, GLuint index, GLsizei bufSize, GLsizei *length, GLint *size, GLenum *type, GLchar *name, const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);
			void GetActiveSubroutineName(GLuint program, GLenum shadertype, GLuint index, GLsizei bufsize, GLsizei *length, GLchar *name, const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);
			void GetActiveSubroutineUniformiv(GLuint program, GLenum shadertype, GLuint index, GLenum pname, GLint *values, const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);
			void GetActiveSubroutineUniformName(GLuint program, GLenum shadertype, GLuint index, GLsizei bufsize, GLsizei *length, GLchar *name, const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);
			void GetActiveUniform(GLuint program, GLuint index, GLsizei bufSize, GLsizei *length, GLint *size, GLenum *type, GLchar *name, const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);
			void GetActiveUniformBlockiv(GLuint program, GLuint uniformBlockIndex, GLenum pname, GLint *params, const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);
			void GetActiveUniformBlockName(GLuint program, GLuint uniformBlockIndex, GLsizei bufSize, GLsizei *length, GLchar *uniformBlockName, const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);
			void GetActiveUniformName(GLuint program, GLuint uniformIndex, GLsizei bufSize, GLsizei *length, GLchar *uniformName, const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);
			void GetActiveUniformsiv(GLuint program, GLsizei uniformCount, const GLuint *uniformIndices, GLenum pname, GLint *params, const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);
			void GetAttachedShaders(GLuint program, GLsizei maxCount, GLsizei *count, GLuint *shaders, const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);
			GLint GetAttribLocation(GLuint program, const GLchar *name, const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);
			void GetBooleani_v(GLenum target, GLuint index, GLboolean * data, const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);
			void GetBooleanv(GLenum pname, GLboolean * data, const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);
			void GetBufferParameteri64v(GLenum target, GLenum value, GLint64 * data, const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);
			void GetBufferParameteriv(GLenum target, GLenum value, GLint * data, const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);
			void GetBufferPointerv(GLenum target, GLenum pname, GLvoid ** params, const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);
			void GetBufferSubData(GLenum target, GLintptr offset, GLsizeiptr size, GLvoid * data, const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);
			void GetCompressedTexImage(GLenum target, GLint level, GLvoid * pixels, const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);
			void GetCompressedTextureImage(GLuint texture, GLint level, GLsizei bufSize, void *pixels, const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);
			void GetCompressedTextureSubImage(GLuint texture, GLint level, GLint xoffset, GLint yoffset, GLint zoffset, GLsizei width, GLsizei height, GLsizei depth, GLsizei bufSize, void *pixels, const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);
			void GetDebugMessageLog(GLuint count, GLsizei bufSize, GLenum *sources, GLenum *types, GLuint *ids, GLenum *severities, GLsizei *lengths, GLchar *messageLog, const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);
			void GetDoublei_v(GLenum target, GLuint index, GLdouble * data, const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);
			void GetDoublev(GLenum pname, GLdouble * data, const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);
			void GetFloati_v(GLenum target, GLuint index, GLfloat * data, const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);
			void GetFloatv(GLenum pname, GLfloat * data, const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);
			GLint GetFragDataIndex(GLuint program, const char * name, const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);
			GLint GetFragDataLocation(GLuint program, const char * name, const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);
			void GetFramebufferAttachmentParameteriv(GLenum target, GLenum attachment, GLenum pname, GLint *params, const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);
			void GetFramebufferParameteriv(GLenum target, GLenum pname, GLint *params, const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);
			GLenum GetGraphicsResetStatus(const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);
			void GetInteger64i_v(GLenum target, GLuint index, GLint64 * data, const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);
			void GetInteger64v(GLenum pname, GLint64 * data, const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);
			void GetIntegeri_v(	GLenum target, GLuint index, GLint * data, const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);
			void GetIntegerv(GLenum pname, GLint * data, const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);
			void GetInternalformati64v(GLenum target, GLenum internalformat, GLenum pname, GLsizei bufSize, GLint64 *params, const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);
			void GetInternalformativ(GLenum target, GLenum internalformat, GLenum pname, GLsizei bufSize, GLint *params, const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);
			void GetMultisamplefv(GLenum pname, GLuint index, GLfloat *val, const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);
			void GetNamedBufferParameteri64v(GLuint buffer, GLenum pname, GLint64 *params, const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);
			void GetNamedBufferParameteriv(GLuint buffer, GLenum pname, GLint *params, const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);
			void GetNamedBufferPointerv(GLuint buffer, GLenum pname, void **params, const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);
			void GetNamedBufferSubData(GLuint buffer, GLintptr offset, GLsizei size, void *data, const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);
			void GetNamedFramebufferAttachmentParameteriv(GLuint framebuffer, GLenum attachment, GLenum pname, GLint *params, const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);
			void GetNamedFramebufferParameteriv(GLuint framebuffer, GLenum pname, GLint *param, const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);
			void GetNamedRenderbufferParameteriv(GLuint renderbuffer, GLenum pname, GLint *params, const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);
			void GetnCompressedTexImage(GLenum target, GLint level, GLsizei bufSize, void *pixels, const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);
			void GetnTexImage(GLenum target, GLint level,GLenum format, GLenum type,	GLsizei bufSize, void *pixels, const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);
			void GetObjectLabel(GLenum identifier, GLuint name, GLsizei bifSize, GLsizei * length, char * label, const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);
			void GetObjectPtrLabel(void * ptr, GLsizei bifSize,GLsizei * length, char * label, const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);
			void GetPointerv(GLenum pname, GLvoid ** params, const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);
			void GetProgramBinary(GLuint program, GLsizei bufsize, GLsizei *length, GLenum *binaryFormat, void *binary, const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);
			void GetProgramInfoLog(GLuint program, GLsizei maxLength, GLsizei *length, GLchar *infoLog, const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);
			void GetProgramInterfaceiv(GLuint program, GLenum programInterface, GLenum pname, GLint * params, const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);
			void GetProgramiv(GLuint program, GLenum pname, GLint *params, const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);
			void GetProgramPipelineInfoLog(GLuint pipeline, GLsizei bufSize, GLsizei *length, GLchar *infoLog, const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);
			void GetProgramPipelineiv(GLuint pipeline, GLenum pname, GLint *params, const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);
			GLuint GetProgramResourceIndex(GLuint program, GLenum programInterface, const char * name, const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);
			void GetProgramResourceiv(GLuint program, GLenum programInterface, GLuint index, GLsizei propCount, const GLenum * props, GLsizei bufSize, GLsizei * length, GLint * params, const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);
			GLuint GetProgramResourceLocation(GLuint program, GLenum programInterface, const char * name, const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);
			GLint GetProgramResourceLocationIndex(GLuint program, GLenum programInterface, const char * name, const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);
			void GetProgramResourceName(GLuint program, GLenum programInterface, GLuint index, GLsizei bufSize, GLsizei * length, char * name, const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);
			void GetProgramStageiv(GLuint program, GLenum shadertype, GLenum pname, GLint *values, const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);
			void GetQueryBufferObjecti64v(GLuint id, GLuint buffer, GLenum pname, GLintptr offset, const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);
			void GetQueryBufferObjectiv(GLuint id, GLuint buffer, GLenum pname, GLintptr offsetconst, char* file = nullptr, const char* func = nullptr, unsigned int line = 0);
			void GetQueryBufferObjectui64v(GLuint id, GLuint buffer, GLenum pname, GLintptr offset, const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);
			void GetQueryBufferObjectuiv(GLuint id, GLuint buffer, GLenum pname, GLintptr offset, const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);
			void GetQueryIndexediv(GLenum target, GLuint index, GLenum pname, GLint * params, const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);
			void GetQueryiv(GLenum target, GLenum pname, GLint * params, const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);
			void GetQueryObjecti64v(GLuint id, GLenum pname, GLint64 * params, const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);
			void GetQueryObjectiv(GLuint id, GLenum pname, GLint * params, const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);
			void GetQueryObjectui64v(GLuint id, GLenum pname, GLuint64 * params, const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);
			void GetQueryObjectuiv(GLuint id, GLenum pname, GLuint * params, const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);
			void GetRenderbufferParameteriv(GLenum target, GLenum pname, GLint *params, const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);
			void GetSamplerParameterfv(GLuint sampler, GLenum pname, GLfloat * params, const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);
			void GetSamplerParameterIiv(GLuint sampler, GLenum pname, GLint * params, const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);
			void GetSamplerParameterIuiv(GLuint sampler, GLenum pname, GLuint * params, const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);
			void GetSamplerParameteriv(GLuint sampler, GLenum pname, GLint * params, const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);
			void GetShaderInfoLog(GLuint shader, GLsizei maxLength, GLsizei *length, GLchar *infoLog, const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);
			void GetShaderiv(GLuint shader, GLenum pname, GLint *params, const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);
			void GetShaderPrecisionFormat(GLenum shaderType, GLenum precisionType, GLint *range, GLint *precision, const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);
			void GetShaderSource(GLuint shader, GLsizei bufSize, GLsizei *length, GLchar *source, const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);
			const GLubyte* GetString(GLenum name, const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);
			const GLubyte* GetStringi(GLenum name, GLuint index, const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);
			GLuint GetSubroutineIndex(GLuint program, GLenum shadertype, const GLchar *name, const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);
			GLint GetSubroutineUniformLocation(GLuint program, GLenum shadertype, const GLchar *name, const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);
			void GetSynciv(GLsync sync, GLenum pname, GLsizei bufSize, GLsizei *length, GLint *values, const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);
			void GetTexImage(GLenum target, GLint level, GLenum format, GLenum type, GLvoid * pixels, const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);
			void GetTexLevelParameterfv(GLenum target, GLint level, GLenum pname, GLfloat * params, const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);
			void GetTexLevelParameteriv(GLenum target, GLint level, GLenum pname, GLint * params, const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);
			void GetTexParameterfv(GLenum target, GLenum pname, GLfloat * params, const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);
			void GetTexParameterIiv(GLenum target, GLenum pname, GLint * params, const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);
			void GetTexParameterIuiv(GLenum target, GLenum pname, GLuint * params, const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);
			void GetTexParameteriv(GLenum target, GLenum pname, GLint * params, const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);
			void GetTextureImage(GLuint texture, GLint level, GLenum format, GLenum type, GLsizei bufSize, void *pixels, const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);
			void GetTextureLevelParameterfv(GLuint texture, GLint level, GLenum pname, GLfloat *params, const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);
			void GetTextureLevelParameteriv(GLuint texture, GLint level, GLenum pname, GLint *params, const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);
			void GetTextureParameterfv(GLuint texture, GLenum pname, GLfloat *params, const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);
			void GetTextureParameterIiv(GLuint texture, GLenum pname, GLint *params, const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);
			void GetTextureParameterIuiv(GLuint texture, GLenum pname, GLuint *params, const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);
			void GetTextureParameteriv(GLuint texture, GLenum pname, GLint *params, const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);
			void GetTextureSubImage(GLuint texture, GLint level, GLint xoffset, GLint yoffset, GLint zoffset, GLsizei width, GLsizei height, GLsizei depth, GLenum format, GLenum type, GLsizei bufSize, void *pixels, const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);
			void GetTransformFeedbacki_v(GLuint xfb, GLenum pname, GLuint index, GLint *param, const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);
			void GetTransformFeedbacki64_v(GLuint xfb, GLenum pname, GLuint index, GLint64 *param, const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);
			void GetTransformFeedbackiv(GLuint xfb, GLenum pname, GLint *param, const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);
			void GetTransformFeedbackVarying(GLuint program, GLuint index, GLsizei bufSize, GLsizei *length, GLsizei *size, GLenum *type, char *name, const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);
			GLuint GetUniformBlockIndex(GLuint program, const GLchar *uniformBlockName, const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);
			void GetUniformdv(GLuint program, GLint location, GLdouble *params, const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);
			void GetUniformfv(GLuint program, GLint location, GLfloat *params, const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);
			void GetUniformIndices(GLuint program, GLsizei uniformCount, const GLchar **uniformNames, GLuint *uniformIndices, const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);
			void GetUniformiv(GLuint program, GLint location, GLint *params, const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);
			void GetnUniformdv(GLuint program, GLint location, GLsizei bufSize, GLdouble *params, const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);
			void GetnUniformfv(GLuint program, GLint location, GLsizei bufSize, GLfloat *params, const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);
			void GetnUniformiv(GLuint program, GLint location, GLsizei bufSize, GLint *params, const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);
			void GetnUniformuiv(GLuint program, GLint location, GLsizei bufSize, GLuint *params, const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);
			void GetUniformSubroutineuiv(GLenum shadertype, GLint location, GLuint *values, const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);
			void GetUniformuiv(GLuint program, GLint location, GLuint *params, const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);
			void GetVertexArrayIndexed64iv(GLuint vaobj, GLuint index, GLenum pname, GLint64 *param, const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);
			void GetVertexArrayIndexediv(GLuint vaobj, GLuint index, GLenum pname, GLint *param, const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);
			void GetVertexArrayiv(GLuint vaobj, GLenum pname, GLint *param, const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);
			void GetVertexAttribdv(GLuint index, GLenum pname, GLdouble *params, const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);
			void GetVertexAttribfv(GLuint index, GLenum pname, GLfloat *params, const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);
			void GetVertexAttribIiv(GLuint index, GLenum pname, GLint *params, const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);
			void GetVertexAttribIuiv(GLuint index, GLenum pname, GLuint *params, const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);
			void GetVertexAttribiv(GLuint index, GLenum pname, GLint *params, const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);
			void GetVertexAttribLdv(GLuint index, GLenum pname, GLdouble *params, const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);
			void GetVertexAttribPointerv(GLuint index, GLenum pname, GLvoid **pointer, const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);
			GLboolean IsBuffer(GLuint buffer, const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);
			GLboolean IsEnabled(GLenum cap, const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);
			GLboolean IsEnabledi(GLenum cap, GLuint index, const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);
			GLboolean IsFramebuffer(GLuint framebuffer, const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);
			GLboolean IsProgram(GLuint program, const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);
			GLboolean IsProgramPipeline(GLuint pipeline, const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);
			GLboolean IsQuery(GLuint id, const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);
			GLboolean IsRenderbuffer(GLuint renderbuffer, const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);
			GLboolean IsSampler(GLuint id, const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);
			GLboolean IsShader(GLuint shader, const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);
			GLboolean IsSync(GLsync sync, const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);
			GLboolean IsTexture(GLuint texture, const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);
			GLboolean IsTransformFeedback(GLuint id, const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);
			GLboolean IsVertexArray(GLuint id, const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);

			
#ifdef LAP_GL_DEBUG_CONTEXT_COMPATIBILITY
			///-----------------------------------------------------------------------------------------------------------------------
			/// deprecated
			void Accum(GLenum op, GLfloat value, const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);
			void AlphaFunc(GLenum function, GLclampf ref, const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);
			GLboolean AreTexturesResident(GLsizei n, const GLuint* textures, GLboolean* residences, const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);
			void ArrayElement(GLint i, const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);
			void Begin(GLenum mode, const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);
			void Bitmap(GLsizei width, GLsizei height, GLfloat xorig, GLfloat yorig, GLfloat xmove, GLfloat ymove, const GLubyte * bitmap, const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);
			void CallList(GLuint list, const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);
			void CallLists(GLsizei  n, GLenum  type, const GLvoid *  lists, const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);
			void ClearAccum(GLfloat red, GLfloat green, GLfloat blue, GLfloat alpha, const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);
			void ClearIndex(GLfloat c, const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);
			void ClientActiveTexture(GLenum texture, const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);
			void ClipPlane(GLenum plane, const GLdouble * equation, const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);
			void Color3b(GLbyte  red, GLbyte  green, GLbyte  blue, const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);
			void Color3bv(const GLbyte* v, const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);
			void Color3d(GLdouble  red, GLdouble  green, GLdouble  blue, const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);
			void Color3dv(const GLdouble* v, const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);
			void Color3f(GLfloat  red, GLfloat  green, GLfloat  blue, const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);
			void Color3fv(const GLfloat *v, const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);
			void Color3i(GLint  red, GLint  green, GLint  blue, const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);
			void Color3iv(const GLint* v, const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);
			void Color3s(GLshort  red, GLshort  green, GLshort  blue, const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);
			void Color3sv(const GLshort *v, const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);
			void Color3ub(GLubyte  red, GLubyte  green, GLubyte  blue, const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);
			void Color3ubv(const GLubyte *v, const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);
			void Color3ui(GLuint  red, GLuint  green, GLuint  blue, const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);
			void Color3uiv(const GLuint *v, const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);
			void Color3us(GLushort  red, GLushort  green, GLushort  blue, const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);
			void Color3usv(const GLushort *v , const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);
			void Color4b(GLbyte  red, GLbyte  green, GLbyte  blue, GLbyte  alpha, const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);
			void Color4bv(const GLbyte *v, const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);
			void Color4d(GLdouble  red, GLdouble  green, GLdouble  blue, GLdouble  alpha, const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);
			void Color4dv(const GLdouble *v, const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);
			void Color4f(GLfloat  red, GLfloat  green, GLfloat  blue, GLfloat  alpha, const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);
			void Color4fv(const GLfloat *v, const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);
			void Color4i(GLint  red, GLint  green, GLint  blue, GLint  alpha, const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);
			void Color4iv(const GLint *v, const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);
			void Color4s(GLshort  red, GLshort  green, GLshort  blue, GLshort  alpha, const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);
			void Color4sv(const GLshort *v, const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);
			void Color4ub(GLubyte  red, GLubyte  green, GLubyte  blue, GLubyte  alpha, const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);
			void Color4ubv(const GLubyte *v, const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);
			void Color4ui(GLuint  red, GLuint  green, GLuint  blue, GLuint  alpha, const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);
			void Color4uiv(const GLuint *v, const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);
			void Color4us(GLushort  red, GLushort  green, GLushort  blue, GLushort  alpha, const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);
			void Color4usv(const GLushort *v, const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);
			void ColorMaterial(GLenum  face, GLenum  mode, const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);
			void ColorP3ui(GLenum type, GLuint color, const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);
			void ColorP3uiv(GLenum type, const GLuint *color, const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);
			void ColorP4ui(GLenum type, GLuint color, const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);
			void ColorP4uiv(GLenum type, const GLuint *color, const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);
			void ColorPointer(GLint size, GLenum type, GLsizei stride, const GLvoid * pointer, const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);
			void CopyPixels(GLint x, GLint y, GLsizei width, GLsizei height, GLenum type, const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);
			void DeleteLists(GLuint list, GLsizei range, const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);
			void DisableClientState(GLenum cap, const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);
			void DrawPixels(GLsizei  width, GLsizei  height, GLenum  format, GLenum  type, const GLvoid *  data, const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);
			void EdgeFlag(GLboolean  flag, const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);
			void EdgeFlagPointer(GLsizei  stride, const GLvoid *  pointer, const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);
			void EdgeFlagv(const GLboolean*  flag, const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);
			void EnableClientState(GLenum cap, const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);
			void End( const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);
			void EndList( const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);
			void EvalCoord1d(GLdouble u, const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);
			void EvalCoord1dv(const GLdouble * u, const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);
			void EvalCoord1f(GLfloat u, const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);
			void EvalCoord1fv(const GLfloat * u, const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);
			void EvalCoord2d(GLdouble u, GLdouble v, const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);
			void EvalCoord2dv(const GLdouble * u, const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);
			void EvalCoord2f(GLfloat u, GLfloat v, const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);
			void EvalCoord2fv(const GLfloat * u, const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);
			void EvalMesh1(GLenum mode, GLint i1, GLint i2, const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);
			void EvalMesh2(GLenum mode, GLint i1, GLint i2, GLint j1, GLint j2, const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);
			void EvalPoint1(GLint i, const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);
			void EvalPoint2(GLint i, GLint j, const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);
			void FeedbackBuffer(GLsizei size, GLenum type, GLfloat * buffer, const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);
			void FogCoordd(GLdouble coord, const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);
			void FogCoorddv(GLdouble* coord, const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);
			void FogCoordf(GLfloat coord, const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);
			void FogCoordfv(GLfloat* coord, const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);
			void FogCoordPointer(GLenum type, GLsizei stride, GLvoid * pointer, const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);
			void Fogf(GLenum pname, GLfloat param, const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);
			void Fogfv(GLenum pname, const GLfloat * param, const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);
			void Fogi(GLenum pname, GLint param, const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);
			void Fogiv(GLenum pname, const GLint * params, const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);
			void Frustum(GLdouble left, GLdouble right, GLdouble bottom, GLdouble top, GLdouble nearVal, GLdouble farVal, const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);
			GLuint GenLists(GLsizei  range, const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);
			void GetClipPlane(GLenum  plane, GLdouble *  equation, const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);
			void GetLightfv(GLenum  light, GLenum  pname, GLfloat *  params, const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);
			void GetLightiv(GLenum  light, GLenum  pname, GLint *  params, const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);
			void GetMapdv(GLenum  target, GLenum  query, GLdouble *  v, const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);
			void GetMapfv(GLenum  target, GLenum  query, GLfloat *  v, const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);
			void GetMapiv(GLenum  target, GLenum  query, GLint *  v, const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);
			void GetMaterialfv(GLenum face, GLenum pname, GLfloat * params, const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);
			void GetMaterialiv(GLenum face, GLenum pname, GLint * params, const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);
			void GetPixelMapfv(GLenum map, GLfloat * data, const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);
			void GetPixelMapuiv(GLenum map, GLuint * data, const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);
			void GetPixelMapusv(GLenum map, GLushort * data, const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);
			void GetPolygonStipple(GLubyte * pattern, const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);
			void GetTexEnvfv(GLenum target, GLenum pname, GLfloat * params, const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);
			void GetTexEnviv(GLenum target, GLenum pname, GLint * params, const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);
			void GetTexGendv(GLenum coord, GLenum pname, GLdouble * param, const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);
			void GetTexGenfv(GLenum coord, GLenum pname, GLfloat * params, const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);
			void GetTexGeniv(GLenum coord, GLenum pname, GLint * params, const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);
			void LoadIdentity( const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);
			void Indexd(GLdouble  c, const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);
			void Indexdv(const GLdouble*  c, const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);
			void Indexf(GLfloat  c, const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);
			void Indexfv(const GLfloat*  c, const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);
			void Indexi(GLint  c, const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);
			void Indexiv(const GLint*  c, const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);
			void IndexMask(GLuint mask, const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);
			void IndexPointer(GLenum type, GLsizei stride, const GLvoid * pointer, const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);
			void Indexs(GLshort  c, const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);
			void Indexsv(const GLshort *  c, const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);
			void Indexub(GLubyte  c, const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);
			void Indexubv(const GLubyte*  c, const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);
			void InitNames( const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);
			void InterleavedArrays(GLenum  format, GLsizei  stride, const GLvoid *  pointer, const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);
			GLboolean IsList(GLuint  list, const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);
			void Lightf(GLenum light, GLenum pname, GLfloat param, const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);
			void Lightfv(GLenum light, GLenum pname, const GLfloat * params, const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);
			void Lighti(GLenum light, GLenum pname, GLint param, const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);
			void Lightiv(GLenum light, GLenum pname, const GLint * params, const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);
			void LightModelf(GLenum  pname, GLfloat  param, const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);
			void LightModelfv(GLenum  pname, const GLfloat *  params, const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);
			void LightModeli(GLenum  pname, GLint  param, const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);
			void LightModeliv(GLenum  pname, const GLint *  params, const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);
			void LineStipple(GLint factor, GLushort pattern, const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);
			void ListBase(GLuint  base, const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);
			void LoadTransposeMatrixd(const GLdouble*  m, const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);
			void LoadTransposeMatrixf(const GLfloat*  m, const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);
			void Map1d(GLenum  target, GLfloat  u1, GLfloat  u2, GLint  stride, GLint  order, const GLdouble*  points, const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);
			void Map1f(GLenum  target, GLfloat  u1, GLfloat  u2, GLint  stride, GLint  order, const GLfloat*  points, const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);
			void Map2d(GLenum target, GLdouble u1, GLdouble u2, GLint ustride, GLint uorder, GLdouble v1, GLdouble v2, GLint vstride, GLint vorder, const GLdouble * points, const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);
			void Map2f(GLenum target, GLfloat u1, GLfloat u2, GLint ustride, GLint uorder, GLfloat v1, GLfloat v2, GLint vstride, GLint vorder, const GLfloat * points, const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);
			void MapGrid1d(GLint un, GLdouble u1, GLdouble u2, const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);
			void MapGrid1f(GLint un, GLfloat u1, GLfloat u2, const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);
			void MapGrid2d(GLint un, GLdouble u1, GLdouble u2, GLint vn, GLdouble v1, GLdouble v2, const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);
			void MapGrid2f(GLint un, GLfloat u1, GLfloat u2, GLint vn, GLfloat v1, GLfloat v2, const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);
			void Materialf(GLenum  face, GLenum  pname, GLfloat  param, const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);
			void Materialfv(GLenum  face, GLenum  pname, const GLfloat *  params, const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);
			void Materiali(GLenum  face, GLenum  pname, GLint  param, const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);
			void Materialiv(GLenum  face, GLenum  pname, const GLint *  params, const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);
			void MatrixMode(GLenum mode, const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);
			void MultiTexCoord1d(GLenum  target, GLdouble  s, const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);
			void MultiTexCoord1dv(GLenum  target, const GLdouble *  v, const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);
			void MultiTexCoord1f(GLenum  target, GLfloat  s, const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);
			void MultiTexCoord1fv(GLenum  target, const GLfloat *  v, const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);
			void MultiTexCoord1i(GLenum  target, GLint  s, const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);
			void MultiTexCoord1iv(GLenum  target, const GLint *  v, const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);
			void MultiTexCoord1s(GLenum  target, GLshort  s, const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);
			void MultiTexCoord1sv(GLenum  target, const GLshort *  v, const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);
			void MultiTexCoord2d(GLenum  target, GLdouble  s, GLdouble  t, const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);
			void MultiTexCoord2dv(GLenum  target, const GLdouble *  v, const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);
			void MultiTexCoord2f(GLenum  target, GLfloat  s, GLfloat  t, const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);
			void MultiTexCoord2fv(GLenum  target, const GLfloat *  v, const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);
			void MultiTexCoord2i(GLenum  target, GLint  s, GLint  t, const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);
			void MultiTexCoord2iv(GLenum  target, const GLint *  v, const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);
			void MultiTexCoord2s(GLenum  target, GLshort  s, GLshort  t, const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);
			void MultiTexCoord2sv(GLenum  target, const GLshort *  v, const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);
			void MultiTexCoord3d(GLenum  target, GLdouble  s, GLdouble  t, GLdouble  r, const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);
			void MultiTexCoord3dv(GLenum  target, const GLdouble *  v, const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);
			void MultiTexCoord3f(GLenum  target, GLfloat  s, GLfloat  t, GLfloat  r, const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);
			void MultiTexCoord3fv(GLenum  target, const GLfloat *  v, const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);
			void MultiTexCoord3i(GLenum  target, GLint  s, GLint  t, GLint  r, const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);
			void MultiTexCoord3iv(GLenum  target, const GLint *  v, const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);
			void MultiTexCoord3s(GLenum  target, GLshort  s, GLshort  t, GLshort  r, const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);
			void MultiTexCoord3sv(GLenum  target, const GLshort *  v, const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);
			void MultiTexCoord4d(GLenum  target, GLdouble  s, GLdouble  t, GLdouble  r, GLdouble  q, const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);
			void MultiTexCoord4dv(GLenum  target, const GLdouble *  v, const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);
			void MultiTexCoord4f(GLenum  target, GLfloat  s, GLfloat  t, GLfloat  r, GLfloat  q, const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);
			void MultiTexCoord4fv(GLenum  target, const GLfloat *  v, const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);
			void MultiTexCoord4i(GLenum  target, GLint  s, GLint  t, GLint  r, GLint  q, const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);
			void MultiTexCoord4iv(GLenum  target, const GLint *  v, const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);
			void MultiTexCoord4s(GLenum  target, GLshort  s, GLshort  t, GLshort  r, GLshort  q, const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);
			void MultiTexCoord4sv(GLenum  target, const GLshort *  v, const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);
			void MultiTexCoordP1ui(GLenum texture, GLenum type, GLuint coords, const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);
			void MultiTexCoordP1uiv(GLenum texture, GLenum type, const GLuint *coords, const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);
			void MultiTexCoordP2ui(GLenum texture, GLenum type, GLuint coords, const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);
			void MultiTexCoordP2uiv(GLenum texture, GLenum type, const GLuint *coords, const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);
			void MultiTexCoordP3ui(GLenum texture, GLenum type, GLuint coords, const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);
			void MultiTexCoordP3uiv(GLenum texture, GLenum type, const GLuint *coords, const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);
			void MultiTexCoordP4ui(GLenum texture, GLenum type, GLuint coords, const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);
			void MultiTexCoordP4uiv(GLenum texture, GLenum type, const GLuint *coords, const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);
			void MultMatrixd(const GLdouble * m, const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);
			void MultMatrixf(const GLfloat * m, const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);
			void MultTransposeMatrixd(const GLdouble *  m, const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);
			void MultTransposeMatrixf(const GLfloat *  m, const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);
			void NewList(GLuint list, GLenum mode, const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);
			void Normal3b(GLbyte nx, GLbyte ny, GLbyte nz, const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);
			void Normal3bv(const GLbyte * v, const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);
			void Normal3d(GLdouble nx, GLdouble ny, GLdouble nz, const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);
			void Normal3dv(const GLdouble * v, const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);
			void Normal3f(GLfloat nx, GLfloat ny, GLfloat nz, const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);
			void Normal3fv(const GLfloat * v, const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);
			void Normal3i(GLint nx, GLint ny, GLint nz, const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);
			void Normal3iv(const GLint * v, const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);
			void Normal3s(GLshort nx, GLshort ny, GLshort nz, const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);
			void Normal3sv(const GLshort * v, const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);
			void NormalP3ui(GLenum type, GLuint coords, const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);
			void NormalP3uiv(GLenum type, const GLuint *coords, const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);
			void NormalPointer(GLenum  type, GLsizei  stride, const GLvoid *  pointer, const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);
			void Ortho(GLdouble left, GLdouble right, GLdouble bottom, GLdouble top, GLdouble nearVal, GLdouble farVal, const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);
			void PassThrough(GLfloat token, const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);
			void PixelMapfv(GLenum map, GLsizei mapsize, const GLfloat * values, const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);
			void PixelMapuiv(GLenum map, GLsizei mapsize, const GLuint * values, const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);
			void PixelMapusv(GLenum map, GLsizei mapsize, const GLushort * values, const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);
			void PixelTransferf(GLenum pname, GLfloat param, const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);
			void PixelTransferi(GLenum pname, GLint param, const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);
			void PixelZoom(GLfloat  xfactor, GLfloat  yfactor, const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);
			void PolygonStipple(const GLubyte * pattern, const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);
			void PopAttrib( const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);
			void PopClientAttrib( const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);
			void PopMatrix( const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);
			void PopName( const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);
			void PrioritizeTextures(GLsizei n, const GLuint * textures, const GLclampf * priorities, const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);
			void PushAttrib(GLbitfield mask, const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);
			void PushClientAttrib(GLbitfield  mask, const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);
			void PushMatrix( const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);
			void PushName(GLuint  name, const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);
			void RasterPos2d(GLdouble  x, GLdouble  y, const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);
			void RasterPos2dv(const GLdouble *  v, const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);
			void RasterPos2f(GLfloat  x, GLfloat  y, const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);
			void RasterPos2fv(const GLfloat *  v, const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);
			void RasterPos2i(GLint  x, GLint  y, const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);
			void RasterPos2iv(const GLint *  v, const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);
			void RasterPos2s(GLshort  x, GLshort  y, const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);
			void RasterPos2sv(const GLshort *  v, const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);
			void RasterPos3d(GLdouble  x, GLdouble  y, GLdouble  z, const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);
			void RasterPos3dv(const GLdouble *  v, const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);
			void RasterPos3f(GLfloat  x, GLfloat  y, GLfloat  z, const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);
			void RasterPos3fv(const GLfloat *  v, const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);
			void RasterPos3i(GLint  x, GLint  y, GLint  z, const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);
			void RasterPos3iv(const GLint *  v, const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);
			void RasterPos3s(GLshort  x, GLshort  y, GLshort  z, const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);
			void RasterPos3sv(const GLshort *  v, const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);
			void RasterPos4d(GLdouble  x, GLdouble  y, GLdouble  z, GLdouble  w, const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);
			void RasterPos4dv(const GLdouble *  v, const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);
			void RasterPos4f(GLfloat  x, GLfloat  y, GLfloat  z, GLfloat  w, const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);
			void RasterPos4fv(const GLfloat *  v, const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);
			void RasterPos4i(GLint  x, GLint  y, GLint  z, GLint  w, const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);
			void RasterPos4iv(const GLint *  v, const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);
			void RasterPos4s(GLshort  x, GLshort  y, GLshort  z, GLshort  w, const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);
			void RasterPos4sv(const GLshort *  v, const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);
			void Rectd(GLdouble  x1, GLdouble  y1, GLdouble  x2, GLdouble  y2, const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);
			void Rectdv(const GLdouble *  v1, const GLdouble *  v2, const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);
			void Rectf(GLfloat  x1, GLfloat  y1, GLfloat  x2, GLfloat  y2, const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);
			void Rectfv(const GLfloat *  v1, const GLfloat *  v2, const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);
			void Recti(GLint  x1, GLint  y1, GLint  x2, GLint  y2, const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);
			void Rectiv(const GLint *  v1, const GLint *  v2, const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);
			void Rects(GLshort  x1, GLshort  y1, GLshort  x2, GLshort  y2, const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);
			void Rectsv(const GLshort *  v1, const GLshort *  v2, const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);
			GLint RenderMode(GLenum  mode, const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);
			void Rotated(GLdouble angle, GLdouble x, GLdouble y, GLdouble z, const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);
			void Rotatef(GLfloat angle, GLfloat x, GLfloat y, GLfloat z, const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);
			void Scaled(GLdouble  x, GLdouble  y, GLdouble  z, const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);
			void Scalef(GLfloat  x, GLfloat  y, GLfloat  z, const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);
			void SecondaryColor3b(GLbyte  red, GLbyte  green, GLbyte  blue, const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);
			void SecondaryColor3bv(const GLbyte *  v, const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);
			void SecondaryColor3d(GLdouble  red, GLdouble  green, GLdouble  blue, const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);
			void SecondaryColor3dv(const GLdouble *  v, const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);
			void SecondaryColor3f(GLfloat  red, GLfloat  green, GLfloat  blue, const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);
			void SecondaryColor3fv(const GLfloat *  v, const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);
			void SecondaryColor3i(GLint  red, GLint  green, GLint  blue, const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);
			void SecondaryColor3iv(const GLint *  v, const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);
			void SecondaryColor3s(GLshort  red, GLshort  green, GLshort  blue, const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);
			void SecondaryColor3sv(const GLshort *  v, const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);
			void SecondaryColor3ub(GLubyte  red, GLubyte  green, GLubyte  blue, const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);
			void SecondaryColor3ubv(const GLubyte *  v, const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);
			void SecondaryColor3ui(GLuint  red, GLuint  green, GLuint  blue, const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);
			void SecondaryColor3uiv(const GLuint *  v, const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);
			void SecondaryColor3us(GLushort  red, GLushort  green, GLushort  blue, const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);
			void SecondaryColor3usv(const GLushort *  v, const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);
			void SecondaryColorP3ui(GLenum type, GLuint color, const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);
			void SecondaryColorP3uiv(GLenum type, const GLuint *color, const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);
			void SecondaryColorPointer(GLint size, GLenum type, GLsizei stride, const GLvoid * pointer, const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);
			void SelectBuffer(GLsizei size, GLuint * buffer, const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);
			void ShadeModel(GLenum  mode, const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);
			void TexCoord1d(GLdouble  s, const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);
			void TexCoord1dv(const GLdouble *  v, const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);
			void TexCoord1f(GLfloat  s, const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);
			void TexCoord1fv(const GLfloat *  v, const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);
			void TexCoord1i(GLint  s, const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);
			void TexCoord1iv(const GLint *  v, const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);
			void TexCoord1s(GLshort  s, const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);
			void TexCoord1sv(const GLshort *  v, const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);
			void TexCoord2d(GLdouble  s, GLdouble  t, const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);
			void TexCoord2dv(const GLdouble *  v, const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);
			void TexCoord2f(GLfloat  s, GLfloat  t, const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);
			void TexCoord2fv(const GLfloat *  v, const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);
			void TexCoord2i(GLint  s, GLint  t, const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);
			void TexCoord2iv(const GLint *  v, const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);
			void TexCoord2s(GLshort  s, GLshort  t, const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);
			void TexCoord2sv(const GLshort *  v, const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);
			void TexCoord3d(GLdouble  s, GLdouble  t, GLdouble  r, const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);
			void TexCoord3dv(const GLdouble *  v, const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);
			void TexCoord3f(GLfloat  s, GLfloat  t, GLfloat  r, const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);
			void TexCoord3fv(const GLfloat *  v, const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);
			void TexCoord3i(GLint  s, GLint  t, GLint  r, const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);
			void TexCoord3iv(const GLint *  v, const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);
			void TexCoord3s(GLshort  s, GLshort  t, GLshort  r, const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);
			void TexCoord3sv(const GLshort *  v, const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);
			void TexCoord4d(GLdouble  s, GLdouble  t, GLdouble  r, GLdouble  q, const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);
			void TexCoord4dv(const GLdouble *  v, const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);
			void TexCoord4f(GLfloat  s, GLfloat  t, GLfloat  r, GLfloat  q, const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);
			void TexCoord4fv(const GLfloat *  v, const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);
			void TexCoord4i(GLint  s, GLint  t, GLint  r, GLint  q, const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);
			void TexCoord4iv(const GLint*  v, const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);
			void TexCoord4s(GLshort  s, GLshort  t, GLshort  r, GLshort  q, const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);
			void TexCoord4sv(const GLshort *  v, const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);
			void TexCoordP1ui(GLenum type, GLuint coords, const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);
			void TexCoordP1uiv(GLenum type, const GLuint *coords, const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);
			void TexCoordP2ui(GLenum type, GLuint coords, const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);
			void TexCoordP2uiv(GLenum type, const GLuint *coords, const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);
			void TexCoordP3ui(GLenum type, GLuint coords, const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);
			void TexCoordP3uiv(GLenum type, const GLuint *coords, const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);
			void TexCoordP4ui(GLenum type, GLuint coords, const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);
			void TexCoordP4uiv(GLenum type, const GLuint *coords, const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);
			void TexCoordPointer(GLint size, GLenum type, GLsizei stride, const GLvoid * pointer, const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);
			void TexEnvf(GLenum target, GLenum pname, GLfloat param, const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);
			void TexEnvfv(GLenum target, GLenum pname, const GLfloat * params, const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);
			void TexEnvi(GLenum target, GLenum pname, GLint param, const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);
			void TexEnviv(GLenum target, GLenum pname, const GLint * params, const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);
			void TexGend(GLenum coord, GLenum pname, GLdouble param, const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);
			void TexGendv(GLenum coord, GLenum pname, const GLdouble * params, const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);
			void TexGenf(GLenum coord, GLenum pname, GLfloat param, const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);
			void TexGenfv(GLenum coord, GLenum pname, const GLfloat * params, const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);
			void TexGeni(GLenum coord, GLenum pname, GLint param, const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);
			void TexGeniv(GLenum coord, GLenum pname, const GLint * params, const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);
			void Translated(GLdouble  x, GLdouble  y, GLdouble  z, const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);
			void Translatef(GLfloat  x, GLfloat  y, GLfloat  z, const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);
			void Vertex2d(GLdouble x, GLdouble y, const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);
			void Vertex2dv(const GLdouble * v, const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);
			void Vertex2f(GLfloat x, GLfloat y, const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);
			void Vertex2fv(const GLfloat * v, const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);
			void Vertex2i(GLint x, GLint y, const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);
			void Vertex2iv(const GLint * v, const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);
			void Vertex2s(GLshort x, GLshort y, const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);
			void Vertex2sv(const GLshort * v, const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);
			void Vertex3d(GLdouble x, GLdouble y, GLdouble z, const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);
			void Vertex3dv(const GLdouble * v, const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);
			void Vertex3f(GLfloat x, GLfloat y, GLfloat z, const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);
			void Vertex3fv(const GLfloat * v, const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);
			void Vertex3i(GLint x, GLint y, GLint z, const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);
			void Vertex3iv(const GLint * v, const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);
			void Vertex3s(GLshort x, GLshort y, GLshort z, const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);
			void Vertex3sv(const GLshort * v, const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);
			void Vertex4d(GLdouble x, GLdouble y, GLdouble z, GLdouble w, const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);
			void Vertex4dv(const GLdouble * v, const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);
			void Vertex4f(GLfloat x, GLfloat y, GLfloat z, GLfloat w, const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);
			void Vertex4fv(const GLfloat * v, const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);
			void Vertex4i(GLint x, GLint y, GLint z, GLint w, const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);
			void Vertex4iv(const GLint * v, const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);
			void Vertex4s(GLshort x, GLshort y, GLshort z, GLshort w, const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);
			void Vertex4sv(const GLshort * v, const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);
			void VertexP2ui(GLenum type, GLuint value, const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);
			void VertexP2uiv(GLenum type, const GLuint *value, const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);
			void VertexP3ui(GLenum type, GLuint value, const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);
			void VertexP3uiv(GLenum type, const GLuint *value, const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);
			void VertexP4ui(GLenum type, GLuint value, const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);
			void VertexP4uiv(GLenum type, const GLuint *value, const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);
			void VertexAttribP1uiv(GLuint index, GLenum type, GLboolean normalized, const GLuint *value, const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);
			void VertexAttribP2uiv(GLuint index, GLenum type, GLboolean normalized, const GLuint *value, const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);
			void VertexAttribP3uiv(GLuint index, GLenum type, GLboolean normalized, const GLuint *value, const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);
			void VertexAttribP4uiv(GLuint index, GLenum type, GLboolean normalized, const GLuint *value, const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);
			void VertexPointer(GLint  size, GLenum  type, GLsizei  stride, const GLvoid *  pointer, const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);
			void WindowPos2d(GLdouble x, GLdouble y, const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);
			void WindowPos2dv(const GLdouble * v, const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);
			void WindowPos2f(GLfloat x, GLfloat y, const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);
			void WindowPos2fv(const GLfloat * v, const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);
			void WindowPos2i(GLint x, GLint y, const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);
			void WindowPos2iv(const GLint * v, const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);
			void WindowPos2s(GLshort x, GLshort y, const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);
			void WindowPos2sv(const GLshort * v, const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);
			void WindowPos3d(GLdouble x, GLdouble y, GLdouble z, const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);
			void WindowPos3dv(const GLdouble * v, const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);
			void WindowPos3f(GLfloat x, GLfloat y, GLfloat z, const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);
			void WindowPos3fv(const GLfloat * v, const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);
			void WindowPos3i(GLint x, GLint y, GLint z, const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);
			void WindowPos3iv(const GLint * v, const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);
			void WindowPos3s(GLshort x, GLshort y, GLshort z, const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);
			void WindowPos3sv(const GLshort * v, const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);

			void GetnMapdv(GLenum target, GLenum query, GLsizei bufSize, GLdouble *v, const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);
			void GetnMapfv(GLenum target, GLenum query, GLsizei bufSize, GLfloat *v, const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);
			void GetnMapiv(GLenum target, GLenum query, GLsizei bufSize, GLint *v, const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);
			void GetnPixelMapfv(GLenum map, GLsizei bufSize, GLfloat *values, const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);
			void GetnPixelMapuiv(GLenum map, GLsizei bufSize, GLuint *values, const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);
			void GetnPixelMapusv(GLenum map, GLsizei bufSize, GLushort *values, const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);
			void GetnPolygonStipple(GLsizei bufSize, GLubyte *pattern, const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);


#ifdef LAP_GL_DEBUG_CONTEXT_COMPATIBILITY_IMAGING
			//v20
			void GetMinMax(GLenum  target, GLboolean  reset, GLenum  format, GLenum  types, GLvoid *  values, const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);
			void GetMinmaxParameterfv(GLenum target, GLenum pname, GLfloat * params, const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);
			void GetMinmaxParameteriv(GLenum target, GLenum pname, GLint * params, const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);
			void ConvolutionFilter1D(GLenum  target, GLenum  internalformat, GLsizei  width, GLenum  format, GLenum  type, const GLvoid *  data, const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);
			void ConvolutionFilter2D(GLenum target, GLenum internalformat, GLsizei width, GLsizei height, GLenum format, GLenum type, const GLvoid * data, const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);
			void ConvolutionParameterf(GLenum  target, GLenum  pname, GLfloat  params, const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);
			void ConvolutionParameteri(GLenum  target, GLenum  pname, GLint  params, const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);
			void CopyConvolutionFilter1D(GLenum  target, GLenum  internalformat, GLint  x, GLint  y, GLsizei  width, const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);
			void CopyConvolutionFilter2D(GLenum target, GLenum internalformat, GLint x, GLint y, GLsizei width, GLsizei height, const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);
			void ColorSubTable(GLenum target, GLsizei start, GLsizei count, GLenum format, GLenum type, const GLvoid * data, const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);
			void ColorTable(GLenum  target, GLenum  internalformat, GLsizei  width, GLenum  format, GLenum  type, const GLvoid *  data, const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);
			void ColorTableParameterfv(GLenum target, GLenum pname, const GLfloat * params, const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);
			void ColorTableParameteriv(GLenum target, GLenum pname, const GLint * params, const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);
			void GetSeparableFilter(GLenum target, GLenum format, GLenum type, GLvoid * row, GLvoid * column, GLvoid * span, const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);
			void GetColorTable(GLenum target, GLenum format, GLenum type, GLvoid * table, const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);
			void GetColorTableParameterfv(GLenum target, GLenum pname, GLfloat * params, const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);
			void GetColorTableParameteriv(GLenum target, GLenum pname, GLint * params, const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);
			void GetConvolutionFilter(GLenum  target, GLenum  format, GLenum  type, GLvoid *  image, const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);
			void GetConvolutionParameterfv(GLenum target, GLenum pname, GLfloat * params, const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);
			void GetConvolutionParameteriv(GLenum target, GLenum pname, GLint * params, const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);
			void GetHistogram(GLenum  target, GLboolean  reset, GLenum  format, GLenum  type, GLvoid *  values, const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);
			void GetHistogramParameterfv(GLenum  target, GLenum  pname, GLfloat *  params, const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);
			void GetHistogramParameteriv(GLenum  target, GLenum  pname, GLint *  params, const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);
			void Histogram(GLenum target, GLsizei width, GLenum internalformat, GLboolean sink, const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);
			void Minmax(GLenum  target, GLenum  internalformat, GLboolean  sink, const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);
			void ResetHistogram(GLenum  target, const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);
			void ResetMinmax(GLenum  target, const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);
			void SeparableFilter2D(GLenum target, GLenum internalformat, GLsizei width, GLsizei height, GLenum format, GLenum type, const GLvoid * row, const GLvoid * column, const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);

			//v32
			void GetnColorTable(GLenum target, GLenum format, GLenum type, GLsizei bufSize, void *table, const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);
			void GetnConvolutionFilter(GLenum target, GLenum format, GLenum type, GLsizei bufSize, void *image, const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);
			void GetnHistogram(GLenum target, GLboolean reset, GLenum format, GLenum type, GLsizei bufSize, void *values, const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);
			void GetnMinmax(GLenum target, GLboolean reset, GLenum format, GLenum type, GLsizei bufSize, void *values, const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);
			void GetnSeparableFilter(GLenum target, GLenum format, GLenum type, GLsizei rowBufSize, void *row, GLsizei columnBufSize, void *column, void *span, const char* file = nullptr, const char* func = nullptr, unsigned int line = 0);
#endif
#endif
			
		private:
			/// internal logging
			void InternalLog(const char* file, const char* func, unsigned int line);
			/// internal safety check 
			void InternalSafetyCheck();
			/// internal redundancy check 
			void InternalRedundancy(bool condition);
		private:
			//logger
			std::vector<bool> logger_state;
			std::ostream* logger;
			bool logfile;
			bool logfunc;
			bool logline;

			//callbacks
			std::function<void(const std::string&, const std::string&, const std::string&, GLenum, const char*, const char*, unsigned int )> callback_debug;
			int func_id;
			std::stringstream func_result;
			std::stringstream func_param;

			//call 
			const char* call_file;
			const char* call_func;
			unsigned int call_line;

			//debug context config
			bool opt_none;
			bool opt_stats;
			bool opt_state;
			bool opt_leak_checks;
			bool opt_safety_checks;
			bool opt_redundancy_checks;
			//context
			bool opt_core;
			bool opt_debug;
			bool opt_robustness;
			int opt_version_minor;
			int opt_version_major;

			//state
			State state;

			//stats
			Stats stats;
		};
	}
}



// script generated
#ifdef LAP_GL_CONTEXT_OVERRIDE_FUNCTIONS
	//v10
	#undef glCullFace
	#define glCullFace(...) lap::gl::detail::context->CullFace(__VA_ARGS__, __FILE__, __func__, __LINE__)
	#undef glFrontFace
	#define glFrontFace(...) lap::gl::detail::context->FrontFace(__VA_ARGS__, __FILE__, __func__, __LINE__)
	#undef glHint
	#define glHint(...) lap::gl::detail::context->Hint(__VA_ARGS__, __FILE__, __func__, __LINE__)
	#undef glLineWidth
	#define glLineWidth(...) lap::gl::detail::context->LineWidth(__VA_ARGS__, __FILE__, __func__, __LINE__)
	#undef glPointSize
	#define glPointSize(...) lap::gl::detail::context->PointSize(__VA_ARGS__, __FILE__, __func__, __LINE__)
	#undef glPolygonMode
	#define glPolygonMode(...) lap::gl::detail::context->PolygonMode(__VA_ARGS__, __FILE__, __func__, __LINE__)
	#undef glScissor
	#define glScissor(...) lap::gl::detail::context->Scissor(__VA_ARGS__, __FILE__, __func__, __LINE__)
	#undef glTexParameterf
	#define glTexParameterf(...) lap::gl::detail::context->TexParameterf(__VA_ARGS__, __FILE__, __func__, __LINE__)
	#undef glTexParameterfv
	#define glTexParameterfv(...) lap::gl::detail::context->TexParameterfv(__VA_ARGS__, __FILE__, __func__, __LINE__)
	#undef glTexParameteri
	#define glTexParameteri(...) lap::gl::detail::context->TexParameteri(__VA_ARGS__, __FILE__, __func__, __LINE__)
	#undef glTexParameteriv
	#define glTexParameteriv(...) lap::gl::detail::context->TexParameteriv(__VA_ARGS__, __FILE__, __func__, __LINE__)
	#undef glTexImage1D
	#define glTexImage1D(...) lap::gl::detail::context->TexImage1D(__VA_ARGS__, __FILE__ , __func__ , __LINE__)
	#undef glTexImage2D
	#define glTexImage2D(...) lap::gl::detail::context->TexImage2D(__VA_ARGS__, __FILE__, __func__, __LINE__)
	#undef glDrawBuffer
	#define glDrawBuffer(...) lap::gl::detail::context->DrawBuffer(__VA_ARGS__, __FILE__, __func__, __LINE__)
	#undef glClear
	#define glClear(...) lap::gl::detail::context->Clear(__VA_ARGS__, __FILE__, __func__, __LINE__)
	#undef glClearColor
	#define glClearColor(...) lap::gl::detail::context->ClearColor(__VA_ARGS__, __FILE__, __func__, __LINE__)
	#undef glClearStencil
	#define glClearStencil(...) lap::gl::detail::context->ClearStencil(__VA_ARGS__, __FILE__, __func__, __LINE__)
	#undef glClearDepth
	#define glClearDepth(...) lap::gl::detail::context->ClearDepth(__VA_ARGS__, __FILE__, __func__, __LINE__)
	#undef glStencilMask
	#define glStencilMask(...) lap::gl::detail::context->StencilMask(__VA_ARGS__, __FILE__, __func__, __LINE__)
	#undef glColorMask
	#define glColorMask(...) lap::gl::detail::context->ColorMask(__VA_ARGS__, __FILE__, __func__, __LINE__)
	#undef glDepthMask
	#define glDepthMask(...) lap::gl::detail::context->DepthMask(__VA_ARGS__, __FILE__, __func__, __LINE__)
	#undef glDisable
	#define glDisable(...) lap::gl::detail::context->Disable(__VA_ARGS__, __FILE__, __func__, __LINE__)
	#undef glEnable
	#define glEnable(...) lap::gl::detail::context->Enable(__VA_ARGS__, __FILE__, __func__, __LINE__)
	#undef glFinish
	#define glFinish() lap::gl::detail::context->Finish(__FILE__, __func__, __LINE__)
	#undef glFlush
	#define glFlush() lap::gl::detail::context->Flush(__FILE__, __func__, __LINE__)
	#undef glBlendFunc
	#define glBlendFunc(...) lap::gl::detail::context->BlendFunc(__VA_ARGS__, __FILE__, __func__, __LINE__)
	#undef glLogicOp
	#define glLogicOp(...) lap::gl::detail::context->LogicOp(__VA_ARGS__, __FILE__, __func__, __LINE__)
	#undef glStencilFunc
	#define glStencilFunc(...) lap::gl::detail::context->StencilFunc(__VA_ARGS__, __FILE__, __func__, __LINE__)
	#undef glStencilOp
	#define glStencilOp(...) lap::gl::detail::context->StencilOp(__VA_ARGS__, __FILE__, __func__, __LINE__)
	#undef glDepthFunc
	#define glDepthFunc(...) lap::gl::detail::context->DepthFunc(__VA_ARGS__, __FILE__, __func__, __LINE__)
	#undef glPixelStoref
	#define glPixelStoref(...) lap::gl::detail::context->PixelStoref(__VA_ARGS__, __FILE__, __func__, __LINE__)
	#undef glPixelStorei
	#define glPixelStorei(...) lap::gl::detail::context->PixelStorei(__VA_ARGS__, __FILE__, __func__, __LINE__)
	#undef glReadBuffer
	#define glReadBuffer(...) lap::gl::detail::context->ReadBuffer(__VA_ARGS__, __FILE__, __func__, __LINE__)
	#undef glReadPixels
	#define glReadPixels(...) lap::gl::detail::context->ReadPixels(__VA_ARGS__, __FILE__, __func__, __LINE__)
	#undef glGetBooleanv
	#define glGetBooleanv(...) lap::gl::detail::context->GetBooleanv(__VA_ARGS__, __FILE__, __func__, __LINE__)
	#undef glGetDoublev
	#define glGetDoublev(...) lap::gl::detail::context->GetDoublev(__VA_ARGS__, __FILE__, __func__, __LINE__)
	#undef glGetError
	#define glGetError() lap::gl::detail::context->GetError(__FILE__, __func__, __LINE__)
	#undef glGetFloatv
	#define glGetFloatv(...) lap::gl::detail::context->GetFloatv(__VA_ARGS__, __FILE__, __func__, __LINE__)
	#undef glGetIntegerv
	#define glGetIntegerv(...) lap::gl::detail::context->GetIntegerv(__VA_ARGS__, __FILE__, __func__, __LINE__)
	#undef glGetString
	#define glGetString(...) lap::gl::detail::context->GetString(__VA_ARGS__, __FILE__, __func__, __LINE__)
	#undef glGetTexImage
	#define glGetTexImage(...) lap::gl::detail::context->GetTexImage(__VA_ARGS__, __FILE__, __func__, __LINE__)
	#undef glGetTexParameterfv
	#define glGetTexParameterfv(...) lap::gl::detail::context->GetTexParameterfv(__VA_ARGS__, __FILE__, __func__, __LINE__)
	#undef glGetTexParameteriv
	#define glGetTexParameteriv(...) lap::gl::detail::context->GetTexParameteriv(__VA_ARGS__, __FILE__, __func__, __LINE__)
	#undef glGetTexLevelParameterfv
	#define glGetTexLevelParameterfv(...) lap::gl::detail::context->GetTexLevelParameterfv(__VA_ARGS__, __FILE__, __func__, __LINE__)
	#undef glGetTexLevelParameteriv
	#define glGetTexLevelParameteriv(...) lap::gl::detail::context->GetTexLevelParameteriv(__VA_ARGS__, __FILE__, __func__, __LINE__)
	#undef glIsEnabled
	#define glIsEnabled(...) lap::gl::detail::context->IsEnabled(__VA_ARGS__, __FILE__, __func__, __LINE__)
	#undef glDepthRange
	#define glDepthRange(...) lap::gl::detail::context->DepthRange(__VA_ARGS__, __FILE__, __func__, __LINE__)
	#undef glViewport
	#define glViewport(...) lap::gl::detail::context->Viewport(__VA_ARGS__, __FILE__, __func__, __LINE__)
	
	//v10 deprecated
	#undef glNewList
	#undef glEndList
	#undef glCallList
	#undef glCallLists
	#undef glDeleteLists
	#undef glGenLists
	#undef glListBase
	#undef glBegin
	#undef glBitmap
	#undef glColor3b
	#undef glColor3bv
	#undef glColor3d
	#undef glColor3dv
	#undef glColor3f
	#undef glColor3fv
	#undef glColor3i
	#undef glColor3iv
	#undef glColor3s
	#undef glColor3sv
	#undef glColor3ub
	#undef glColor3ubv
	#undef glColor3ui
	#undef glColor3uiv
	#undef glColor3us
	#undef glColor3usv
	#undef glColor4b
	#undef glColor4bv
	#undef glColor4d
	#undef glColor4dv
	#undef glColor4f
	#undef glColor4fv
	#undef glColor4i
	#undef glColor4iv
	#undef glColor4s
	#undef glColor4sv
	#undef glColor4ub
	#undef glColor4ubv
	#undef glColor4ui
	#undef glColor4uiv
	#undef glColor4us
	#undef glColor4usv
	#undef glEdgeFlag
	#undef glEdgeFlagv
	#undef glEnd
	#undef glIndexd
	#undef glIndexdv
	#undef glIndexf
	#undef glIndexfv
	#undef glIndexi
	#undef glIndexiv
	#undef glIndexs
	#undef glIndexsv
	#undef glNormal3b
	#undef glNormal3bv
	#undef glNormal3d
	#undef glNormal3dv
	#undef glNormal3f
	#undef glNormal3fv
	#undef glNormal3i
	#undef glNormal3iv
	#undef glNormal3s
	#undef glNormal3sv
	#undef glRasterPos2d
	#undef glRasterPos2dv
	#undef glRasterPos2f
	#undef glRasterPos2fv
	#undef glRasterPos2i
	#undef glRasterPos2iv
	#undef glRasterPos2s
	#undef glRasterPos2sv
	#undef glRasterPos3d
	#undef glRasterPos3dv
	#undef glRasterPos3f
	#undef glRasterPos3fv
	#undef glRasterPos3i
	#undef glRasterPos3iv
	#undef glRasterPos3s
	#undef glRasterPos3sv
	#undef glRasterPos4d
	#undef glRasterPos4dv
	#undef glRasterPos4f
	#undef glRasterPos4fv
	#undef glRasterPos4i
	#undef glRasterPos4iv
	#undef glRasterPos4s
	#undef glRasterPos4sv
	#undef glRectd
	#undef glRectdv
	#undef glRectf
	#undef glRectfv
	#undef glRecti
	#undef glRectiv
	#undef glRects
	#undef glRectsv
	#undef glTexCoord1d
	#undef glTexCoord1dv
	#undef glTexCoord1f
	#undef glTexCoord1fv
	#undef glTexCoord1i
	#undef glTexCoord1iv
	#undef glTexCoord1s
	#undef glTexCoord1sv
	#undef glTexCoord2d
	#undef glTexCoord2dv
	#undef glTexCoord2f
	#undef glTexCoord2fv
	#undef glTexCoord2i
	#undef glTexCoord2iv
	#undef glTexCoord2s
	#undef glTexCoord2sv
	#undef glTexCoord3d
	#undef glTexCoord3dv
	#undef glTexCoord3f
	#undef glTexCoord3fv
	#undef glTexCoord3i
	#undef glTexCoord3iv
	#undef glTexCoord3s
	#undef glTexCoord3sv
	#undef glTexCoord4d
	#undef glTexCoord4dv
	#undef glTexCoord4f
	#undef glTexCoord4fv
	#undef glTexCoord4i
	#undef glTexCoord4iv
	#undef glTexCoord4s
	#undef glTexCoord4sv
	#undef glVertex2d
	#undef glVertex2dv
	#undef glVertex2f
	#undef glVertex2fv
	#undef glVertex2i
	#undef glVertex2iv
	#undef glVertex2s
	#undef glVertex2sv
	#undef glVertex3d
	#undef glVertex3dv
	#undef glVertex3f
	#undef glVertex3fv
	#undef glVertex3i
	#undef glVertex3iv
	#undef glVertex3s
	#undef glVertex3sv
	#undef glVertex4d
	#undef glVertex4dv
	#undef glVertex4f
	#undef glVertex4fv
	#undef glVertex4i
	#undef glVertex4iv
	#undef glVertex4s
	#undef glVertex4sv
	#undef glClipPlane
	#undef glColorMaterial
	#undef glFogf
	#undef glFogfv
	#undef glFogi
	#undef glFogiv
	#undef glLightf
	#undef glLightfv
	#undef glLighti
	#undef glLightiv
	#undef glLightModelf
	#undef glLightModelfv
	#undef glLightModeli
	#undef glLightModeliv
	#undef glLineStipple
	#undef glMaterialf
	#undef glMaterialfv
	#undef glMateriali
	#undef glMaterialiv
	#undef glPolygonStipple
	#undef glShadeModel
	#undef glTexEnvf
	#undef glTexEnvfv
	#undef glTexEnvi
	#undef glTexEnviv
	#undef glTexGend
	#undef glTexGendv
	#undef glTexGenf
	#undef glTexGenfv
	#undef glTexGeni
	#undef glTexGeniv
	#undef glFeedbackBuffer
	#undef glSelectBuffer
	#undef glRenderMode
	#undef glInitNames
	#undef glName
	#undef glPassThrough
	#undef glPopName
	#undef glPushName
	#undef glClearAccum
	#undef glClearIndex
	#undef glIndexMask
	#undef glAccum
	#undef glPopAttrib
	#undef glPushAttrib
	#undef glMap1d
	#undef glMap1f
	#undef glMap2d
	#undef glMap2f
	#undef glMapGrid1d
	#undef glMapGrid1f
	#undef glMapGrid2d
	#undef glMapGrid2f
	#undef glEvalCoord1d
	#undef glEvalCoord1dv
	#undef glEvalCoord1f
	#undef glEvalCoord1fv
	#undef glEvalCoord2d
	#undef glEvalCoord2dv
	#undef glEvalCoord2f
	#undef glEvalCoord2fv
	#undef glEvalMesh1
	#undef glEvalPoint1
	#undef glEvalMesh2
	#undef glEvalPoint2
	#undef glAlphaFunc
	#undef glPixelZoom
	#undef glPixelTransferf
	#undef glPixelTransferi
	#undef glPixelMapfv
	#undef glPixelMapuiv
	#undef glPixelMapusv
	#undef glCopyPixels
	#undef glDrawPixels
	#undef glGetClipPlane
	#undef glGetLightfv
	#undef glGetLightiv
	#undef glGetMapdv
	#undef glGetMapfv
	#undef glGetMapiv
	#undef glGetMaterialfv
	#undef glGetMaterialiv
	#undef glGetPixelMapfv
	#undef glGetPixelMapuiv
	#undef glGetPixelMapusv
	#undef glGetPolygonStipple
	#undef glGetTexEnvfv
	#undef glGetTexEnviv
	#undef glGetTexGendv
	#undef glGetTexGenfv
	#undef glGetTexGeniv
	#undef glIsList
	#undef glFrustum
	#undef glLoadIdentity
	#undef glMatrixMode
	#undef glMultMatrixf
	#undef glMultMatrixd
	#undef glOrtho
	#undef glPopMatrix
	#undef glPushMatrix
	#undef glRotated
	#undef glRotatef
	#undef glScaled
	#undef glScalef
	#undef glTranslated
	#undef glTranslatef

#ifdef LAP_GL_DEBUG_CONTEXT_COMPATIBILITY
	#define glNewList(...) lap::gl::detail::context->NewList(__VA_ARGS__, __FILE__, __func__, __LINE__)
	#define glEndList() lap::gl::detail::context->EndList(__FILE__, __func__, __LINE__)
	#define glCallList(...) lap::gl::detail::context->CallList(__VA_ARGS__, __FILE__, __func__, __LINE__)
	#define glCallLists(...) lap::gl::detail::context->CallLists(__VA_ARGS__, __FILE__, __func__, __LINE__)
	#define glDeleteLists(...) lap::gl::detail::context->DeleteLists(__VA_ARGS__, __FILE__, __func__, __LINE__)
	#define glGenLists(...) lap::gl::detail::context->GenLists(__VA_ARGS__, __FILE__, __func__, __LINE__)
	#define glListBase(...) lap::gl::detail::context->ListBase(__VA_ARGS__, __FILE__, __func__, __LINE__)
	#define glBegin(...) lap::gl::detail::context->Begin(__VA_ARGS__, __FILE__, __func__, __LINE__)
	#define glBitmap(...) lap::gl::detail::context->Bitmap(__VA_ARGS__, __FILE__, __func__, __LINE__)
	#define glColor3b(...) lap::gl::detail::context->Color3b(__VA_ARGS__, __FILE__, __func__, __LINE__)
	#define glColor3bv(...) lap::gl::detail::context->Color3bv(__VA_ARGS__, __FILE__, __func__, __LINE__)
	#define glColor3d(...) lap::gl::detail::context->Color3d(__VA_ARGS__, __FILE__, __func__, __LINE__)
	#define glColor3dv(...) lap::gl::detail::context->Color3dv(__VA_ARGS__, __FILE__, __func__, __LINE__)
	#define glColor3f(...) lap::gl::detail::context->Color3f(__VA_ARGS__, __FILE__, __func__, __LINE__)
	#define glColor3fv(...) lap::gl::detail::context->Color3fv(__VA_ARGS__, __FILE__, __func__, __LINE__)
	#define glColor3i(...) lap::gl::detail::context->Color3i(__VA_ARGS__, __FILE__, __func__, __LINE__)
	#define glColor3iv(...) lap::gl::detail::context->Color3iv(__VA_ARGS__, __FILE__, __func__, __LINE__)
	#define glColor3s(...) lap::gl::detail::context->Color3s(__VA_ARGS__, __FILE__, __func__, __LINE__)
	#define glColor3sv(...) lap::gl::detail::context->Color3sv(__VA_ARGS__, __FILE__, __func__, __LINE__)
	#define glColor3ub(...) lap::gl::detail::context->Color3ub(__VA_ARGS__, __FILE__, __func__, __LINE__)
	#define glColor3ubv(...) lap::gl::detail::context->Color3ubv(__VA_ARGS__, __FILE__, __func__, __LINE__)
	#define glColor3ui(...) lap::gl::detail::context->Color3ui(__VA_ARGS__, __FILE__, __func__, __LINE__)
	#define glColor3uiv(...) lap::gl::detail::context->Color3uiv(__VA_ARGS__, __FILE__, __func__, __LINE__)
	#define glColor3us(...) lap::gl::detail::context->Color3us(__VA_ARGS__, __FILE__, __func__, __LINE__)
	#define glColor3usv(...) lap::gl::detail::context->Color3usv(__VA_ARGS__, __FILE__, __func__, __LINE__)
	#define glColor4b(...) lap::gl::detail::context->Color4b(__VA_ARGS__, __FILE__, __func__, __LINE__)
	#define glColor4bv(...) lap::gl::detail::context->Color4bv(__VA_ARGS__, __FILE__, __func__, __LINE__)
	#define glColor4d(...) lap::gl::detail::context->Color4d(__VA_ARGS__, __FILE__, __func__, __LINE__)
	#define glColor4dv(...) lap::gl::detail::context->Color4dv(__VA_ARGS__, __FILE__, __func__, __LINE__)
	#define glColor4f(...) lap::gl::detail::context->Color4f(__VA_ARGS__, __FILE__, __func__, __LINE__)
	#define glColor4fv(...) lap::gl::detail::context->Color4fv(__VA_ARGS__, __FILE__, __func__, __LINE__)
	#define glColor4i(...) lap::gl::detail::context->Color4i(__VA_ARGS__, __FILE__, __func__, __LINE__)
	#define glColor4iv(...) lap::gl::detail::context->Color4iv(__VA_ARGS__, __FILE__, __func__, __LINE__)
	#define glColor4s(...) lap::gl::detail::context->Color4s(__VA_ARGS__, __FILE__, __func__, __LINE__)
	#define glColor4sv(...) lap::gl::detail::context->Color4sv(__VA_ARGS__, __FILE__, __func__, __LINE__)
	#define glColor4ub(...) lap::gl::detail::context->Color4ub(__VA_ARGS__, __FILE__, __func__, __LINE__)
	#define glColor4ubv(...) lap::gl::detail::context->Color4ubv(__VA_ARGS__, __FILE__, __func__, __LINE__)
	#define glColor4ui(...) lap::gl::detail::context->Color4ui(__VA_ARGS__, __FILE__, __func__, __LINE__)
	#define glColor4uiv(...) lap::gl::detail::context->Color4uiv(__VA_ARGS__, __FILE__, __func__, __LINE__)
	#define glColor4us(...) lap::gl::detail::context->Color4us(__VA_ARGS__, __FILE__, __func__, __LINE__)
	#define glColor4usv(...) lap::gl::detail::context->Color4usv(__VA_ARGS__, __FILE__, __func__, __LINE__)
	#define glEdgeFlag(...) lap::gl::detail::context->EdgeFlag(__VA_ARGS__, __FILE__, __func__, __LINE__)
	#define glEdgeFlagv(...) lap::gl::detail::context->EdgeFlagv(__VA_ARGS__, __FILE__, __func__, __LINE__)
	#define glEnd() lap::gl::detail::context->End(__FILE__, __func__, __LINE__)
	#define glIndexd(...) lap::gl::detail::context->Indexd(__VA_ARGS__, __FILE__, __func__, __LINE__)
	#define glIndexdv(...) lap::gl::detail::context->Indexdv(__VA_ARGS__, __FILE__, __func__, __LINE__)
	#define glIndexf(...) lap::gl::detail::context->Indexf(__VA_ARGS__, __FILE__, __func__, __LINE__)
	#define glIndexfv(...) lap::gl::detail::context->Indexfv(__VA_ARGS__, __FILE__, __func__, __LINE__)
	#define glIndexi(...) lap::gl::detail::context->Indexi(__VA_ARGS__, __FILE__, __func__, __LINE__)
	#define glIndexiv(...) lap::gl::detail::context->Indexiv(__VA_ARGS__, __FILE__, __func__, __LINE__)
	#define glIndexs(...) lap::gl::detail::context->Indexs(__VA_ARGS__, __FILE__, __func__, __LINE__)
	#define glIndexsv(...) lap::gl::detail::context->Indexsv(__VA_ARGS__, __FILE__, __func__, __LINE__)
	#define glNormal3b(...) lap::gl::detail::context->Normal3b(__VA_ARGS__, __FILE__, __func__, __LINE__)
	#define glNormal3bv(...) lap::gl::detail::context->Normal3bv(__VA_ARGS__, __FILE__, __func__, __LINE__)
	#define glNormal3d(...) lap::gl::detail::context->Normal3d(__VA_ARGS__, __FILE__, __func__, __LINE__)
	#define glNormal3dv(...) lap::gl::detail::context->Normal3dv(__VA_ARGS__, __FILE__, __func__, __LINE__)
	#define glNormal3f(...) lap::gl::detail::context->Normal3f(__VA_ARGS__, __FILE__, __func__, __LINE__)
	#define glNormal3fv(...) lap::gl::detail::context->Normal3fv(__VA_ARGS__, __FILE__, __func__, __LINE__)
	#define glNormal3i(...) lap::gl::detail::context->Normal3i(__VA_ARGS__, __FILE__, __func__, __LINE__)
	#define glNormal3iv(...) lap::gl::detail::context->Normal3iv(__VA_ARGS__, __FILE__, __func__, __LINE__)
	#define glNormal3s(...) lap::gl::detail::context->Normal3s(__VA_ARGS__, __FILE__, __func__, __LINE__)
	#define glNormal3sv(...) lap::gl::detail::context->Normal3sv(__VA_ARGS__, __FILE__, __func__, __LINE__)
	#define glRasterPos2d(...) lap::gl::detail::context->RasterPos2d(__VA_ARGS__, __FILE__, __func__, __LINE__)
	#define glRasterPos2dv(...) lap::gl::detail::context->RasterPos2dv(__VA_ARGS__, __FILE__, __func__, __LINE__)
	#define glRasterPos2f(...) lap::gl::detail::context->RasterPos2f(__VA_ARGS__, __FILE__, __func__, __LINE__)
	#define glRasterPos2fv(...) lap::gl::detail::context->RasterPos2fv(__VA_ARGS__, __FILE__, __func__, __LINE__)
	#define glRasterPos2i(...) lap::gl::detail::context->RasterPos2i(__VA_ARGS__, __FILE__, __func__, __LINE__)
	#define glRasterPos2iv(...) lap::gl::detail::context->RasterPos2iv(__VA_ARGS__, __FILE__, __func__, __LINE__)
	#define glRasterPos2s(...) lap::gl::detail::context->RasterPos2s(__VA_ARGS__, __FILE__, __func__, __LINE__)
	#define glRasterPos2sv(...) lap::gl::detail::context->RasterPos2sv(__VA_ARGS__, __FILE__, __func__, __LINE__)
	#define glRasterPos3d(...) lap::gl::detail::context->RasterPos3d(__VA_ARGS__, __FILE__, __func__, __LINE__)
	#define glRasterPos3dv(...) lap::gl::detail::context->RasterPos3dv(__VA_ARGS__, __FILE__, __func__, __LINE__)
	#define glRasterPos3f(...) lap::gl::detail::context->RasterPos3f(__VA_ARGS__, __FILE__, __func__, __LINE__)
	#define glRasterPos3fv(...) lap::gl::detail::context->RasterPos3fv(__VA_ARGS__, __FILE__, __func__, __LINE__)
	#define glRasterPos3i(...) lap::gl::detail::context->RasterPos3i(__VA_ARGS__, __FILE__, __func__, __LINE__)
	#define glRasterPos3iv(...) lap::gl::detail::context->RasterPos3iv(__VA_ARGS__, __FILE__, __func__, __LINE__)
	#define glRasterPos3s(...) lap::gl::detail::context->RasterPos3s(__VA_ARGS__, __FILE__, __func__, __LINE__)
	#define glRasterPos3sv(...) lap::gl::detail::context->RasterPos3sv(__VA_ARGS__, __FILE__, __func__, __LINE__)
	#define glRasterPos4d(...) lap::gl::detail::context->RasterPos4d(__VA_ARGS__, __FILE__, __func__, __LINE__)
	#define glRasterPos4dv(...) lap::gl::detail::context->RasterPos4dv(__VA_ARGS__, __FILE__, __func__, __LINE__)
	#define glRasterPos4f(...) lap::gl::detail::context->RasterPos4f(__VA_ARGS__, __FILE__, __func__, __LINE__)
	#define glRasterPos4fv(...) lap::gl::detail::context->RasterPos4fv(__VA_ARGS__, __FILE__, __func__, __LINE__)
	#define glRasterPos4i(...) lap::gl::detail::context->RasterPos4i(__VA_ARGS__, __FILE__, __func__, __LINE__)
	#define glRasterPos4iv(...) lap::gl::detail::context->RasterPos4iv(__VA_ARGS__, __FILE__, __func__, __LINE__)
	#define glRasterPos4s(...) lap::gl::detail::context->RasterPos4s(__VA_ARGS__, __FILE__, __func__, __LINE__)
	#define glRasterPos4sv(...) lap::gl::detail::context->RasterPos4sv(__VA_ARGS__, __FILE__, __func__, __LINE__)
	#define glRectd(...) lap::gl::detail::context->Rectd(__VA_ARGS__, __FILE__, __func__, __LINE__)
	#define glRectdv(...) lap::gl::detail::context->Rectdv(__VA_ARGS__, __FILE__, __func__, __LINE__)
	#define glRectf(...) lap::gl::detail::context->Rectf(__VA_ARGS__, __FILE__, __func__, __LINE__)
	#define glRectfv(...) lap::gl::detail::context->Rectfv(__VA_ARGS__, __FILE__, __func__, __LINE__)
	#define glRecti(...) lap::gl::detail::context->Recti(__VA_ARGS__, __FILE__, __func__, __LINE__)
	#define glRectiv(...) lap::gl::detail::context->Rectiv(__VA_ARGS__, __FILE__, __func__, __LINE__)
	#define glRects(...) lap::gl::detail::context->Rects(__VA_ARGS__, __FILE__, __func__, __LINE__)
	#define glRectsv(...) lap::gl::detail::context->Rectsv(__VA_ARGS__, __FILE__, __func__, __LINE__)
	#define glTexCoord1d(...) lap::gl::detail::context->TexCoord1d(__VA_ARGS__, __FILE__, __func__, __LINE__)
	#define glTexCoord1dv(...) lap::gl::detail::context->TexCoord1dv(__VA_ARGS__, __FILE__, __func__, __LINE__)
	#define glTexCoord1f(...) lap::gl::detail::context->TexCoord1f(__VA_ARGS__, __FILE__, __func__, __LINE__)
	#define glTexCoord1fv(...) lap::gl::detail::context->TexCoord1fv(__VA_ARGS__, __FILE__, __func__, __LINE__)
	#define glTexCoord1i(...) lap::gl::detail::context->TexCoord1i(__VA_ARGS__, __FILE__, __func__, __LINE__)
	#define glTexCoord1iv(...) lap::gl::detail::context->TexCoord1iv(__VA_ARGS__, __FILE__, __func__, __LINE__)
	#define glTexCoord1s(...) lap::gl::detail::context->TexCoord1s(__VA_ARGS__, __FILE__, __func__, __LINE__)
	#define glTexCoord1sv(...) lap::gl::detail::context->TexCoord1sv(__VA_ARGS__, __FILE__, __func__, __LINE__)
	#define glTexCoord2d(...) lap::gl::detail::context->TexCoord2d(__VA_ARGS__, __FILE__, __func__, __LINE__)
	#define glTexCoord2dv(...) lap::gl::detail::context->TexCoord2dv(__VA_ARGS__, __FILE__, __func__, __LINE__)
	#define glTexCoord2f(...) lap::gl::detail::context->TexCoord2f(__VA_ARGS__, __FILE__, __func__, __LINE__)
	#define glTexCoord2fv(...) lap::gl::detail::context->TexCoord2fv(__VA_ARGS__, __FILE__, __func__, __LINE__)
	#define glTexCoord2i(...) lap::gl::detail::context->TexCoord2i(__VA_ARGS__, __FILE__, __func__, __LINE__)
	#define glTexCoord2iv(...) lap::gl::detail::context->TexCoord2iv(__VA_ARGS__, __FILE__, __func__, __LINE__)
	#define glTexCoord2s(...) lap::gl::detail::context->TexCoord2s(__VA_ARGS__, __FILE__, __func__, __LINE__)
	#define glTexCoord2sv(...) lap::gl::detail::context->TexCoord2sv(__VA_ARGS__, __FILE__, __func__, __LINE__)
	#define glTexCoord3d(...) lap::gl::detail::context->TexCoord3d(__VA_ARGS__, __FILE__, __func__, __LINE__)
	#define glTexCoord3dv(...) lap::gl::detail::context->TexCoord3dv(__VA_ARGS__, __FILE__, __func__, __LINE__)
	#define glTexCoord3f(...) lap::gl::detail::context->TexCoord3f(__VA_ARGS__, __FILE__, __func__, __LINE__)
	#define glTexCoord3fv(...) lap::gl::detail::context->TexCoord3fv(__VA_ARGS__, __FILE__, __func__, __LINE__)
	#define glTexCoord3i(...) lap::gl::detail::context->TexCoord3i(__VA_ARGS__, __FILE__, __func__, __LINE__)
	#define glTexCoord3iv(...) lap::gl::detail::context->TexCoord3iv(__VA_ARGS__, __FILE__, __func__, __LINE__)
	#define glTexCoord3s(...) lap::gl::detail::context->TexCoord3s(__VA_ARGS__, __FILE__, __func__, __LINE__)
	#define glTexCoord3sv(...) lap::gl::detail::context->TexCoord3sv(__VA_ARGS__, __FILE__, __func__, __LINE__)
	#define glTexCoord4d(...) lap::gl::detail::context->TexCoord4d(__VA_ARGS__, __FILE__, __func__, __LINE__)
	#define glTexCoord4dv(...) lap::gl::detail::context->TexCoord4dv(__VA_ARGS__, __FILE__, __func__, __LINE__)
	#define glTexCoord4f(...) lap::gl::detail::context->TexCoord4f(__VA_ARGS__, __FILE__, __func__, __LINE__)
	#define glTexCoord4fv(...) lap::gl::detail::context->TexCoord4fv(__VA_ARGS__, __FILE__, __func__, __LINE__)
	#define glTexCoord4i(...) lap::gl::detail::context->TexCoord4i(__VA_ARGS__, __FILE__, __func__, __LINE__)
	#define glTexCoord4iv(...) lap::gl::detail::context->TexCoord4iv(__VA_ARGS__, __FILE__, __func__, __LINE__)
	#define glTexCoord4s(...) lap::gl::detail::context->TexCoord4s(__VA_ARGS__, __FILE__, __func__, __LINE__)
	#define glTexCoord4sv(...) lap::gl::detail::context->TexCoord4sv(__VA_ARGS__, __FILE__, __func__, __LINE__)
	#define glVertex2d(...) lap::gl::detail::context->Vertex2d(__VA_ARGS__, __FILE__, __func__, __LINE__)
	#define glVertex2dv(...) lap::gl::detail::context->Vertex2dv(__VA_ARGS__, __FILE__, __func__, __LINE__)
	#define glVertex2f(...) lap::gl::detail::context->Vertex2f(__VA_ARGS__, __FILE__, __func__, __LINE__)
	#define glVertex2fv(...) lap::gl::detail::context->Vertex2fv(__VA_ARGS__, __FILE__, __func__, __LINE__)
	#define glVertex2i(...) lap::gl::detail::context->Vertex2i(__VA_ARGS__, __FILE__, __func__, __LINE__)
	#define glVertex2iv(...) lap::gl::detail::context->Vertex2iv(__VA_ARGS__, __FILE__, __func__, __LINE__)
	#define glVertex2s(...) lap::gl::detail::context->Vertex2s(__VA_ARGS__, __FILE__, __func__, __LINE__)
	#define glVertex2sv(...) lap::gl::detail::context->Vertex2sv(__VA_ARGS__, __FILE__, __func__, __LINE__)
	#define glVertex3d(...) lap::gl::detail::context->Vertex3d(__VA_ARGS__, __FILE__, __func__, __LINE__)
	#define glVertex3dv(...) lap::gl::detail::context->Vertex3dv(__VA_ARGS__, __FILE__, __func__, __LINE__)
	#define glVertex3f(...) lap::gl::detail::context->Vertex3f(__VA_ARGS__, __FILE__, __func__, __LINE__)
	#define glVertex3fv(...) lap::gl::detail::context->Vertex3fv(__VA_ARGS__, __FILE__, __func__, __LINE__)
	#define glVertex3i(...) lap::gl::detail::context->Vertex3i(__VA_ARGS__, __FILE__, __func__, __LINE__)
	#define glVertex3iv(...) lap::gl::detail::context->Vertex3iv(__VA_ARGS__, __FILE__, __func__, __LINE__)
	#define glVertex3s(...) lap::gl::detail::context->Vertex3s(__VA_ARGS__, __FILE__, __func__, __LINE__)
	#define glVertex3sv(...) lap::gl::detail::context->Vertex3sv(__VA_ARGS__, __FILE__, __func__, __LINE__)
	#define glVertex4d(...) lap::gl::detail::context->Vertex4d(__VA_ARGS__, __FILE__, __func__, __LINE__)
	#define glVertex4dv(...) lap::gl::detail::context->Vertex4dv(__VA_ARGS__, __FILE__, __func__, __LINE__)
	#define glVertex4f(...) lap::gl::detail::context->Vertex4f(__VA_ARGS__, __FILE__, __func__, __LINE__)
	#define glVertex4fv(...) lap::gl::detail::context->Vertex4fv(__VA_ARGS__, __FILE__, __func__, __LINE__)
	#define glVertex4i(...) lap::gl::detail::context->Vertex4i(__VA_ARGS__, __FILE__, __func__, __LINE__)
	#define glVertex4iv(...) lap::gl::detail::context->Vertex4iv(__VA_ARGS__, __FILE__, __func__, __LINE__)
	#define glVertex4s(...) lap::gl::detail::context->Vertex4s(__VA_ARGS__, __FILE__, __func__, __LINE__)
	#define glVertex4sv(...) lap::gl::detail::context->Vertex4sv(__VA_ARGS__, __FILE__, __func__, __LINE__)
	#define glClipPlane(...) lap::gl::detail::context->ClipPlane(__VA_ARGS__, __FILE__, __func__, __LINE__)
	#define glColorMaterial(...) lap::gl::detail::context->ColorMaterial(__VA_ARGS__, __FILE__, __func__, __LINE__)
	#define glFogf(...) lap::gl::detail::context->Fogf(__VA_ARGS__, __FILE__, __func__, __LINE__)
	#define glFogfv(...) lap::gl::detail::context->Fogfv(__VA_ARGS__, __FILE__, __func__, __LINE__)
	#define glFogi(...) lap::gl::detail::context->Fogi(__VA_ARGS__, __FILE__, __func__, __LINE__)
	#define glFogiv(...) lap::gl::detail::context->Fogiv(__VA_ARGS__, __FILE__, __func__, __LINE__)
	#define glLightf(...) lap::gl::detail::context->Lightf(__VA_ARGS__, __FILE__, __func__, __LINE__)
	#define glLightfv(...) lap::gl::detail::context->Lightfv(__VA_ARGS__, __FILE__, __func__, __LINE__)
	#define glLighti(...) lap::gl::detail::context->Lighti(__VA_ARGS__, __FILE__, __func__, __LINE__)
	#define glLightiv(...) lap::gl::detail::context->Lightiv(__VA_ARGS__, __FILE__, __func__, __LINE__)
	#define glLightModelf(...) lap::gl::detail::context->LightModelf(__VA_ARGS__, __FILE__, __func__, __LINE__)
	#define glLightModelfv(...) lap::gl::detail::context->LightModelfv(__VA_ARGS__, __FILE__, __func__, __LINE__)
	#define glLightModeli(...) lap::gl::detail::context->LightModeli(__VA_ARGS__, __FILE__, __func__, __LINE__)
	#define glLightModeliv(...) lap::gl::detail::context->LightModeliv(__VA_ARGS__, __FILE__, __func__, __LINE__)
	#define glLineStipple(...) lap::gl::detail::context->LineStipple(__VA_ARGS__, __FILE__, __func__, __LINE__)
	#define glMaterialf(...) lap::gl::detail::context->Materialf(__VA_ARGS__, __FILE__, __func__, __LINE__)
	#define glMaterialfv(...) lap::gl::detail::context->Materialfv(__VA_ARGS__, __FILE__, __func__, __LINE__)
	#define glMateriali(...) lap::gl::detail::context->Materiali(__VA_ARGS__, __FILE__, __func__, __LINE__)
	#define glMaterialiv(...) lap::gl::detail::context->Materialiv(__VA_ARGS__, __FILE__, __func__, __LINE__)
	#define glPolygonStipple(...) lap::gl::detail::context->PolygonStipple(__VA_ARGS__, __FILE__, __func__, __LINE__)
	#define glShadeModel(...) lap::gl::detail::context->ShadeModel(__VA_ARGS__, __FILE__, __func__, __LINE__)
	#define glTexEnvf(...) lap::gl::detail::context->TexEnvf(__VA_ARGS__, __FILE__, __func__, __LINE__)
	#define glTexEnvfv(...) lap::gl::detail::context->TexEnvfv(__VA_ARGS__, __FILE__, __func__, __LINE__)
	#define glTexEnvi(...) lap::gl::detail::context->TexEnvi(__VA_ARGS__, __FILE__, __func__, __LINE__)
	#define glTexEnviv(...) lap::gl::detail::context->TexEnviv(__VA_ARGS__, __FILE__, __func__, __LINE__)
	#define glTexGend(...) lap::gl::detail::context->TexGend(__VA_ARGS__, __FILE__, __func__, __LINE__)
	#define glTexGendv(...) lap::gl::detail::context->TexGendv(__VA_ARGS__, __FILE__, __func__, __LINE__)
	#define glTexGenf(...) lap::gl::detail::context->TexGenf(__VA_ARGS__, __FILE__, __func__, __LINE__)
	#define glTexGenfv(...) lap::gl::detail::context->TexGenfv(__VA_ARGS__, __FILE__, __func__, __LINE__)
	#define glTexGeni(...) lap::gl::detail::context->TexGeni(__VA_ARGS__, __FILE__, __func__, __LINE__)
	#define glTexGeniv(...) lap::gl::detail::context->TexGeniv(__VA_ARGS__, __FILE__, __func__, __LINE__)
	#define glFeedbackBuffer(...) lap::gl::detail::context->FeedbackBuffer(__VA_ARGS__, __FILE__, __func__, __LINE__)
	#define glSelectBuffer(...) lap::gl::detail::context->SelectBuffer(__VA_ARGS__, __FILE__, __func__, __LINE__)
	#define glRenderMode(...) lap::gl::detail::context->RenderMode(__VA_ARGS__, __FILE__, __func__, __LINE__)
	#define glInitNames(...) lap::gl::detail::context->InitNames(__VA_ARGS__, __FILE__, __func__, __LINE__)
	#define glPassThrough(...) lap::gl::detail::context->PassThrough(__VA_ARGS__, __FILE__, __func__, __LINE__)
	#define glPopName() lap::gl::detail::context->PopName(__FILE__, __func__, __LINE__)
	#define glPushName(...) lap::gl::detail::context->PushName(__VA_ARGS__, __FILE__, __func__, __LINE__)
	#define glClearAccum(...) lap::gl::detail::context->ClearAccum(__VA_ARGS__, __FILE__, __func__, __LINE__)
	#define glClearIndex(...) lap::gl::detail::context->ClearIndex(__VA_ARGS__, __FILE__, __func__, __LINE__)
	#define glIndexMask(...) lap::gl::detail::context->IndexMask(__VA_ARGS__, __FILE__, __func__, __LINE__)
	#define glAccum(...) lap::gl::detail::context->Accum(__VA_ARGS__, __FILE__, __func__, __LINE__)
	#define glPopAttrib() lap::gl::detail::context->PopAttrib(__FILE__, __func__, __LINE__)
	#define glPushAttrib(...) lap::gl::detail::context->PushAttrib(__VA_ARGS__, __FILE__, __func__, __LINE__)
	#define glMap1d(...) lap::gl::detail::context->Map1d(__VA_ARGS__, __FILE__, __func__, __LINE__)
	#define glMap1f(...) lap::gl::detail::context->Map1f(__VA_ARGS__, __FILE__, __func__, __LINE__)
	#define glMap2d(...) lap::gl::detail::context->Map2d(__VA_ARGS__, __FILE__, __func__, __LINE__)
	#define glMap2f(...) lap::gl::detail::context->Map2f(__VA_ARGS__, __FILE__, __func__, __LINE__)
	#define glMapGrid1d(...) lap::gl::detail::context->MapGrid1d(__VA_ARGS__, __FILE__, __func__, __LINE__)
	#define glMapGrid1f(...) lap::gl::detail::context->MapGrid1f(__VA_ARGS__, __FILE__, __func__, __LINE__)
	#define glMapGrid2d(...) lap::gl::detail::context->MapGrid2d(__VA_ARGS__, __FILE__, __func__, __LINE__)
	#define glMapGrid2f(...) lap::gl::detail::context->MapGrid2f(__VA_ARGS__, __FILE__, __func__, __LINE__)
	#define glEvalCoord1d(...) lap::gl::detail::context->EvalCoord1d(__VA_ARGS__, __FILE__, __func__, __LINE__)
	#define glEvalCoord1dv(...) lap::gl::detail::context->EvalCoord1dv(__VA_ARGS__, __FILE__, __func__, __LINE__)
	#define glEvalCoord1f(...) lap::gl::detail::context->EvalCoord1f(__VA_ARGS__, __FILE__, __func__, __LINE__)
	#define glEvalCoord1fv(...) lap::gl::detail::context->EvalCoord1fv(__VA_ARGS__, __FILE__, __func__, __LINE__)
	#define glEvalCoord2d(...) lap::gl::detail::context->EvalCoord2d(__VA_ARGS__, __FILE__, __func__, __LINE__)
	#define glEvalCoord2dv(...) lap::gl::detail::context->EvalCoord2dv(__VA_ARGS__, __FILE__, __func__, __LINE__)
	#define glEvalCoord2f(...) lap::gl::detail::context->EvalCoord2f(__VA_ARGS__, __FILE__, __func__, __LINE__)
	#define glEvalCoord2fv(...) lap::gl::detail::context->EvalCoord2fv(__VA_ARGS__, __FILE__, __func__, __LINE__)
	#define glEvalMesh1(...) lap::gl::detail::context->EvalMesh1(__VA_ARGS__, __FILE__, __func__, __LINE__)
	#define glEvalPoint1(...) lap::gl::detail::context->EvalPoint1(__VA_ARGS__, __FILE__, __func__, __LINE__)
	#define glEvalMesh2(...) lap::gl::detail::context->EvalMesh2(__VA_ARGS__, __FILE__, __func__, __LINE__)
	#define glEvalPoint2(...) lap::gl::detail::context->EvalPoint2(__VA_ARGS__, __FILE__, __func__, __LINE__)
	#define glAlphaFunc(...) lap::gl::detail::context->AlphaFunc(__VA_ARGS__, __FILE__, __func__, __LINE__)
	#define glPixelZoom(...) lap::gl::detail::context->PixelZoom(__VA_ARGS__, __FILE__, __func__, __LINE__)
	#define glPixelTransferf(...) lap::gl::detail::context->PixelTransferf(__VA_ARGS__, __FILE__, __func__, __LINE__)
	#define glPixelTransferi(...) lap::gl::detail::context->PixelTransferi(__VA_ARGS__, __FILE__, __func__, __LINE__)
	#define glPixelMapfv(...) lap::gl::detail::context->PixelMapfv(__VA_ARGS__, __FILE__, __func__, __LINE__)
	#define glPixelMapuiv(...) lap::gl::detail::context->PixelMapuiv(__VA_ARGS__, __FILE__, __func__, __LINE__)
	#define glPixelMapusv(...) lap::gl::detail::context->PixelMapusv(__VA_ARGS__, __FILE__, __func__, __LINE__)
	#define glCopyPixels(...) lap::gl::detail::context->CopyPixels(__VA_ARGS__, __FILE__, __func__, __LINE__)
	#define glDrawPixels(...) lap::gl::detail::context->DrawPixels(__VA_ARGS__, __FILE__, __func__, __LINE__)
	#define glGetClipPlane(...) lap::gl::detail::context->GetClipPlane(__VA_ARGS__, __FILE__, __func__, __LINE__)
	#define glGetLightfv(...) lap::gl::detail::context->GetLightfv(__VA_ARGS__, __FILE__, __func__, __LINE__)
	#define glGetLightiv(...) lap::gl::detail::context->GetLightiv(__VA_ARGS__, __FILE__, __func__, __LINE__)
	#define glGetMapdv(...) lap::gl::detail::context->GetMapdv(__VA_ARGS__, __FILE__, __func__, __LINE__)
	#define glGetMapfv(...) lap::gl::detail::context->GetMapfv(__VA_ARGS__, __FILE__, __func__, __LINE__)
	#define glGetMapiv(...) lap::gl::detail::context->GetMapiv(__VA_ARGS__, __FILE__, __func__, __LINE__)
	#define glGetMaterialfv(...) lap::gl::detail::context->GetMaterialfv(__VA_ARGS__, __FILE__, __func__, __LINE__)
	#define glGetMaterialiv(...) lap::gl::detail::context->GetMaterialiv(__VA_ARGS__, __FILE__, __func__, __LINE__)
	#define glGetPixelMapfv(...) lap::gl::detail::context->GetPixelMapfv(__VA_ARGS__, __FILE__, __func__, __LINE__)
	#define glGetPixelMapuiv(...) lap::gl::detail::context->GetPixelMapuiv(__VA_ARGS__, __FILE__, __func__, __LINE__)
	#define glGetPixelMapusv(...) lap::gl::detail::context->GetPixelMapusv(__VA_ARGS__, __FILE__, __func__, __LINE__)
	#define glGetPolygonStipple(...) lap::gl::detail::context->GetPolygonStipple(__VA_ARGS__, __FILE__, __func__, __LINE__)
	#define glGetTexEnvfv(...) lap::gl::detail::context->GetTexEnvfv(__VA_ARGS__, __FILE__, __func__, __LINE__)
	#define glGetTexEnviv(...) lap::gl::detail::context->GetTexEnviv(__VA_ARGS__, __FILE__, __func__, __LINE__)
	#define glGetTexGendv(...) lap::gl::detail::context->GetTexGendv(__VA_ARGS__, __FILE__, __func__, __LINE__)
	#define glGetTexGenfv(...) lap::gl::detail::context->GetTexGenfv(__VA_ARGS__, __FILE__, __func__, __LINE__)
	#define glGetTexGeniv(...) lap::gl::detail::context->GetTexGeniv(__VA_ARGS__, __FILE__, __func__, __LINE__)
	#define glIsList(...) lap::gl::detail::context->IsList(__VA_ARGS__, __FILE__, __func__, __LINE__)
	#define glFrustum(...) lap::gl::detail::context->Frustum(__VA_ARGS__, __FILE__, __func__, __LINE__)
	#define glLoadIdentity(...) lap::gl::detail::context->LoadIdentity(__FILE__, __func__, __LINE__)
	#define glMatrixMode(...) lap::gl::detail::context->MatrixMode(__VA_ARGS__, __FILE__, __func__, __LINE__)
	#define glMultMatrixf(...) lap::gl::detail::context->MultMatrixf(__VA_ARGS__, __FILE__, __func__, __LINE__)
	#define glMultMatrixd(...) lap::gl::detail::context->MultMatrixd(__VA_ARGS__, __FILE__, __func__, __LINE__)
	#define glOrtho(...) lap::gl::detail::context->Ortho(__VA_ARGS__, __FILE__, __func__, __LINE__)
	#define glPopMatrix() lap::gl::detail::context->PopMatrix(__FILE__, __func__, __LINE__)
	#define glPushMatrix(...) lap::gl::detail::context->PushMatrix(__VA_ARGS__, __FILE__, __func__, __LINE__)
	#define glRotated(...) lap::gl::detail::context->Rotated(__VA_ARGS__, __FILE__, __func__, __LINE__)
	#define glRotatef(...) lap::gl::detail::context->Rotatef(__VA_ARGS__, __FILE__, __func__, __LINE__)
	#define glScaled(...) lap::gl::detail::context->Scaled(__VA_ARGS__, __FILE__, __func__, __LINE__)
	#define glScalef(...) lap::gl::detail::context->Scalef(__VA_ARGS__, __FILE__, __func__, __LINE__)
	#define glTranslated(...) lap::gl::detail::context->Translated(__VA_ARGS__, __FILE__, __func__, __LINE__)
	#define glTranslatef(...) lap::gl::detail::context->Translatef(__VA_ARGS__, __FILE__, __func__, __LINE__)
	#endif
	//v11
	#undef glDrawArrays
	#define glDrawArrays(...) lap::gl::detail::context->DrawArrays(__VA_ARGS__, __FILE__, __func__, __LINE__)
	#undef glDrawElements
	#define glDrawElements(...) lap::gl::detail::context->DrawElements(__VA_ARGS__, __FILE__, __func__, __LINE__)
	#undef glPolygonOffset
	#define glPolygonOffset(...) lap::gl::detail::context->PolygonOffset(__VA_ARGS__, __FILE__, __func__, __LINE__)
	#undef glCopyTexImage1D
	#define glCopyTexImage1D(...) lap::gl::detail::context->CopyTexImage1D(__VA_ARGS__, __FILE__, __func__, __LINE__)
	#undef glCopyTexImage2D
	#define glCopyTexImage2D(...) lap::gl::detail::context->CopyTexImage2D(__VA_ARGS__, __FILE__, __func__, __LINE__)
	#undef glCopyTexSubImage1D
	#define glCopyTexSubImage1D(...) lap::gl::detail::context->CopyTexSubImage1D(__VA_ARGS__, __FILE__, __func__, __LINE__)
	#undef glCopyTexSubImage2D
	#define glCopyTexSubImage2D(...) lap::gl::detail::context->CopyTexSubImage2D(__VA_ARGS__, __FILE__, __func__, __LINE__)
	#undef glTexSubImage1D
	#define glTexSubImage1D(...) lap::gl::detail::context->TexSubImage1D(__VA_ARGS__, __FILE__, __func__, __LINE__)
	#undef glTexSubImage2D
	#define glTexSubImage2D(...) lap::gl::detail::context->TexSubImage2D(__VA_ARGS__, __FILE__, __func__, __LINE__)
	#undef glBindTexture
	#define glBindTexture(...) lap::gl::detail::context->BindTexture(__VA_ARGS__, __FILE__, __func__, __LINE__)
	#undef glDeleteTextures
	#define glDeleteTextures(...) lap::gl::detail::context->DeleteTextures(__VA_ARGS__, __FILE__, __func__, __LINE__)
	#undef glGenTextures
	#define glGenTextures(...) lap::gl::detail::context->GenTextures(__VA_ARGS__, __FILE__, __func__, __LINE__)
	#undef glIsTexture
	#define glIsTexture(...) lap::gl::detail::context->IsTexture(__VA_ARGS__, __FILE__, __func__, __LINE__)


	//v11 deprecated
	#undef glArrayElement
	#undef glColorPointer
	#undef glDisableClientState
	#undef glEdgeFlagPointer
	#undef glEnableClientState
	#undef glIndexPointer
	#undef glInterleavedArrays
	#undef glNormalPointer
	#undef glTexCoordPointer
	#undef glVertexPointer
	#undef glAreTexturesResident
	#undef glPrioritizeTextures
	#undef glIndexub
	#undef glIndexubv
	#undef glPopClientAttrib
	#undef glPushClientAttrib

	#ifdef LAP_GL_DEBUG_CONTEXT_COMPATIBILITY
	#define glArrayElement(...) lap::gl::detail::context->ArrayElement(__VA_ARGS__, __FILE__, __func__, __LINE__)
	#define glColorPointer(...) lap::gl::detail::context->ColorPointer(__VA_ARGS__, __FILE__, __func__, __LINE__)
	#define glDisableClientState(...) lap::gl::detail::context->DisableClientState(__VA_ARGS__, __FILE__, __func__, __LINE__)
	#define glEdgeFlagPointer(...) lap::gl::detail::context->EdgeFlagPointer(__VA_ARGS__, __FILE__, __func__, __LINE__)
	#define glEnableClientState(...) lap::gl::detail::context->EnableClientState(__VA_ARGS__, __FILE__, __func__, __LINE__)
	#define glIndexPointer(...) lap::gl::detail::context->IndexPointer(__VA_ARGS__, __FILE__, __func__, __LINE__)
	#define glInterleavedArrays(...) lap::gl::detail::context->InterleavedArrays(__VA_ARGS__, __FILE__, __func__, __LINE__)
	#define glNormalPointer(...) lap::gl::detail::context->NormalPointer(__VA_ARGS__, __FILE__, __func__, __LINE__)
	#define glTexCoordPointer(...) lap::gl::detail::context->TexCoordPointer(__VA_ARGS__, __FILE__, __func__, __LINE__)
	#define glVertexPointer(...) lap::gl::detail::context->VertexPointer(__VA_ARGS__, __FILE__, __func__, __LINE__)
	#define glAreTexturesResident(...) lap::gl::detail::context->AreTexturesResident(__VA_ARGS__, __FILE__, __func__, __LINE__)
	#define glPrioritizeTextures(...) lap::gl::detail::context->PrioritizeTextures(__VA_ARGS__, __FILE__, __func__, __LINE__)
	#define glIndexub(...) lap::gl::detail::context->Indexub(__VA_ARGS__, __FILE__, __func__, __LINE__)
	#define glIndexubv(...) lap::gl::detail::context->Indexubv(__VA_ARGS__, __FILE__, __func__, __LINE__)
	#define glPopClientAttrib(...) lap::gl::detail::context->PopClientAttrib(__FILE__, __func__, __LINE__)
	#define glPushClientAttrib(...) lap::gl::detail::context->PushClientAttrib(__VA_ARGS__, __FILE__, __func__, __LINE__)
#endif

	//v12
	#undef glDrawRangeElements
	#define glDrawRangeElements(...) lap::gl::detail::context->DrawRangeElements(__VA_ARGS__, __FILE__, __func__, __LINE__)
	#undef glTexImage3D
	#define glTexImage3D(...) lap::gl::detail::context->TexImage3D(__VA_ARGS__, __FILE__, __func__, __LINE__)
	#undef glTexSubImage3D
	#define glTexSubImage3D(...) lap::gl::detail::context->TexSubImage3D(__VA_ARGS__, __FILE__, __func__, __LINE__)
	#undef glCopyTexSubImage3D
	#define glCopyTexSubImage3D(...) lap::gl::detail::context->CopyTexSubImage3D(__VA_ARGS__, __FILE__, __func__, __LINE__)
	
	//v13
	#undef glActiveTexture
	#define glActiveTexture(...) lap::gl::detail::context->ActiveTexture(__VA_ARGS__, __FILE__, __func__, __LINE__)
	#undef glSampleCoverage
	#define glSampleCoverage(...) lap::gl::detail::context->SampleCoverage(__VA_ARGS__, __FILE__, __func__, __LINE__)
	#undef glCompressedTexImage3D
	#define glCompressedTexImage3D(...) lap::gl::detail::context->CompressedTexImage3D(__VA_ARGS__, __FILE__, __func__, __LINE__)
	#undef glCompressedTexImage2D
	#define glCompressedTexImage2D(...) lap::gl::detail::context->CompressedTexImage2D(__VA_ARGS__, __FILE__, __func__, __LINE__)
	#undef glCompressedTexImage1D
	#define glCompressedTexImage1D(...) lap::gl::detail::context->CompressedTexImage1D(__VA_ARGS__, __FILE__, __func__, __LINE__)
	#undef glCompressedTexSubImage3D
	#define glCompressedTexSubImage3D(...) lap::gl::detail::context->CompressedTexSubImage3D(__VA_ARGS__, __FILE__, __func__, __LINE__)
	#undef glCompressedTexSubImage2D
	#define glCompressedTexSubImage2D(...) lap::gl::detail::context->CompressedTexSubImage2D(__VA_ARGS__, __FILE__, __func__, __LINE__)
	#undef glCompressedTexSubImage1D
	#define glCompressedTexSubImage1D(...) lap::gl::detail::context->CompressedTexSubImage1D(__VA_ARGS__, __FILE__, __func__, __LINE__)
	#undef glGetCompressedTexImage
	#define glGetCompressedTexImage(...) lap::gl::detail::context->GetCompressedTexImage(__VA_ARGS__, __FILE__, __func__, __LINE__)
	
	//v13 deprecated
	#undef glClientActiveTexture
	#undef glMultiTexCoord1d
	#undef glMultiTexCoord1dv
	#undef glMultiTexCoord1f
	#undef glMultiTexCoord1fv
	#undef glMultiTexCoord1i
	#undef glMultiTexCoord1iv
	#undef glMultiTexCoord1s
	#undef glMultiTexCoord1sv
	#undef glMultiTexCoord2d
	#undef glMultiTexCoord2dv
	#undef glMultiTexCoord2f
	#undef glMultiTexCoord2fv
	#undef glMultiTexCoord2i
	#undef glMultiTexCoord2iv
	#undef glMultiTexCoord2s
	#undef glMultiTexCoord2sv
	#undef glMultiTexCoord3d
	#undef glMultiTexCoord3dv
	#undef glMultiTexCoord3f
	#undef glMultiTexCoord3fv
	#undef glMultiTexCoord3i
	#undef glMultiTexCoord3iv
	#undef glMultiTexCoord3s
	#undef glMultiTexCoord3sv
	#undef glMultiTexCoord4d
	#undef glMultiTexCoord4dv
	#undef glMultiTexCoord4f
	#undef glMultiTexCoord4fv
	#undef glMultiTexCoord4i
	#undef glMultiTexCoord4iv
	#undef glMultiTexCoord4s
	#undef glMultiTexCoord4sv
	#undef glLoadTransposeMatrixf
	#undef glLoadTransposeMatrixd
	#undef glMultTransposeMatrixf
	#undef glMultTransposeMatrixd
#ifdef LAP_GL_DEBUG_CONTEXT_COMPATIBILITY
	#define glClientActiveTexture(...) lap::gl::detail::context->ClientActiveTexture(__VA_ARGS__, __FILE__, __func__, __LINE__)
	#define glMultiTexCoord1d(...) lap::gl::detail::context->MultiTexCoord1d(__VA_ARGS__, __FILE__, __func__, __LINE__)
	#define glMultiTexCoord1dv(...) lap::gl::detail::context->MultiTexCoord1dv(__VA_ARGS__, __FILE__, __func__, __LINE__)
	#define glMultiTexCoord1f(...) lap::gl::detail::context->MultiTexCoord1f(__VA_ARGS__, __FILE__, __func__, __LINE__)
	#define glMultiTexCoord1fv(...) lap::gl::detail::context->MultiTexCoord1fv(__VA_ARGS__, __FILE__, __func__, __LINE__)
	#define glMultiTexCoord1i(...) lap::gl::detail::context->MultiTexCoord1i(__VA_ARGS__, __FILE__, __func__, __LINE__)
	#define glMultiTexCoord1iv(...) lap::gl::detail::context->MultiTexCoord1iv(__VA_ARGS__, __FILE__, __func__, __LINE__)
	#define glMultiTexCoord1s(...) lap::gl::detail::context->MultiTexCoord1s(__VA_ARGS__, __FILE__, __func__, __LINE__)
	#define glMultiTexCoord1sv(...) lap::gl::detail::context->MultiTexCoord1sv(__VA_ARGS__, __FILE__, __func__, __LINE__)
	#define glMultiTexCoord2d(...) lap::gl::detail::context->MultiTexCoord2d(__VA_ARGS__, __FILE__, __func__, __LINE__)
	#define glMultiTexCoord2dv(...) lap::gl::detail::context->MultiTexCoord2dv(__VA_ARGS__, __FILE__, __func__, __LINE__)
	#define glMultiTexCoord2f(...) lap::gl::detail::context->MultiTexCoord2f(__VA_ARGS__, __FILE__, __func__, __LINE__)
	#define glMultiTexCoord2fv(...) lap::gl::detail::context->MultiTexCoord2fv(__VA_ARGS__, __FILE__, __func__, __LINE__)
	#define glMultiTexCoord2i(...) lap::gl::detail::context->MultiTexCoord2i(__VA_ARGS__, __FILE__, __func__, __LINE__)
	#define glMultiTexCoord2iv(...) lap::gl::detail::context->MultiTexCoord2iv(__VA_ARGS__, __FILE__, __func__, __LINE__)
	#define glMultiTexCoord2s(...) lap::gl::detail::context->MultiTexCoord2s(__VA_ARGS__, __FILE__, __func__, __LINE__)
	#define glMultiTexCoord2sv(...) lap::gl::detail::context->MultiTexCoord2sv(__VA_ARGS__, __FILE__, __func__, __LINE__)
	#define glMultiTexCoord3d(...) lap::gl::detail::context->MultiTexCoord3d(__VA_ARGS__, __FILE__, __func__, __LINE__)
	#define glMultiTexCoord3dv(...) lap::gl::detail::context->MultiTexCoord3dv(__VA_ARGS__, __FILE__, __func__, __LINE__)
	#define glMultiTexCoord3f(...) lap::gl::detail::context->MultiTexCoord3f(__VA_ARGS__, __FILE__, __func__, __LINE__)
	#define glMultiTexCoord3fv(...) lap::gl::detail::context->MultiTexCoord3fv(__VA_ARGS__, __FILE__, __func__, __LINE__)
	#define glMultiTexCoord3i(...) lap::gl::detail::context->MultiTexCoord3i(__VA_ARGS__, __FILE__, __func__, __LINE__)
	#define glMultiTexCoord3iv(...) lap::gl::detail::context->MultiTexCoord3iv(__VA_ARGS__, __FILE__, __func__, __LINE__)
	#define glMultiTexCoord3s(...) lap::gl::detail::context->MultiTexCoord3s(__VA_ARGS__, __FILE__, __func__, __LINE__)
	#define glMultiTexCoord3sv(...) lap::gl::detail::context->MultiTexCoord3sv(__VA_ARGS__, __FILE__, __func__, __LINE__)
	#define glMultiTexCoord4d(...) lap::gl::detail::context->MultiTexCoord4d(__VA_ARGS__, __FILE__, __func__, __LINE__)
	#define glMultiTexCoord4dv(...) lap::gl::detail::context->MultiTexCoord4dv(__VA_ARGS__, __FILE__, __func__, __LINE__)
	#define glMultiTexCoord4f(...) lap::gl::detail::context->MultiTexCoord4f(__VA_ARGS__, __FILE__, __func__, __LINE__)
	#define glMultiTexCoord4fv(...) lap::gl::detail::context->MultiTexCoord4fv(__VA_ARGS__, __FILE__, __func__, __LINE__)
	#define glMultiTexCoord4i(...) lap::gl::detail::context->MultiTexCoord4i(__VA_ARGS__, __FILE__, __func__, __LINE__)
	#define glMultiTexCoord4iv(...) lap::gl::detail::context->MultiTexCoord4iv(__VA_ARGS__, __FILE__, __func__, __LINE__)
	#define glMultiTexCoord4s(...) lap::gl::detail::context->MultiTexCoord4s(__VA_ARGS__, __FILE__, __func__, __LINE__)
	#define glMultiTexCoord4sv(...) lap::gl::detail::context->MultiTexCoord4sv(__VA_ARGS__, __FILE__, __func__, __LINE__)
	#define glLoadTransposeMatrixf(...) lap::gl::detail::context->LoadTransposeMatrixf(__VA_ARGS__, __FILE__, __func__, __LINE__)
	#define glLoadTransposeMatrixd(...) lap::gl::detail::context->LoadTransposeMatrixd(__VA_ARGS__, __FILE__, __func__, __LINE__)
	#define glMultTransposeMatrixf(...) lap::gl::detail::context->MultTransposeMatrixf(__VA_ARGS__, __FILE__, __func__, __LINE__)
	#define glMultTransposeMatrixd(...) lap::gl::detail::context->MultTransposeMatrixd(__VA_ARGS__, __FILE__, __func__, __LINE__)
#endif
	//v14
	#undef glBlendFuncSeparate
	#define glBlendFuncSeparate(...) lap::gl::detail::context->BlendFuncSeparate(__VA_ARGS__, __FILE__, __func__, __LINE__)
	#undef glMultiDrawArrays
	#define glMultiDrawArrays(...) lap::gl::detail::context->MultiDrawArrays(__VA_ARGS__, __FILE__, __func__, __LINE__)
	#undef glMultiDrawElements
	#define glMultiDrawElements(...) lap::gl::detail::context->MultiDrawElements(__VA_ARGS__, __FILE__, __func__, __LINE__)
	#undef glPointParameterf
	#define glPointParameterf(...) lap::gl::detail::context->PointParameterf(__VA_ARGS__, __FILE__, __func__, __LINE__)
	#undef glPointParameterfv
	#define glPointParameterfv(...) lap::gl::detail::context->PointParameterfv(__VA_ARGS__, __FILE__, __func__, __LINE__)
	#undef glPointParameteri
	#define glPointParameteri(...) lap::gl::detail::context->PointParameteri(__VA_ARGS__, __FILE__, __func__, __LINE__)
	#undef glPointParameteriv
	#define glPointParameteriv(...) lap::gl::detail::context->PointParameteriv(__VA_ARGS__, __FILE__, __func__, __LINE__)
	#undef glBlendColor
	#define glBlendColor(...) lap::gl::detail::context->BlendColor(__VA_ARGS__, __FILE__, __func__, __LINE__)
	#undef glBlendEquation
	#define glBlendEquation(...) lap::gl::detail::context->BlendEquation(__VA_ARGS__, __FILE__, __func__, __LINE__)
	
	//14 deprecated
	#undef glFogCoordf
	#undef glFogCoordfv
	#undef glFogCoordd
	#undef glFogCoorddv
	#undef glFogCoordPointer
	#undef glSecondaryColor3b
	#undef glSecondaryColor3bv
	#undef glSecondaryColor3d
	#undef glSecondaryColor3dv
	#undef glSecondaryColor3f
	#undef glSecondaryColor3fv
	#undef glSecondaryColor3i
	#undef glSecondaryColor3iv
	#undef glSecondaryColor3s
	#undef glSecondaryColor3sv
	#undef glSecondaryColor3ub
	#undef glSecondaryColor3ubv
	#undef glSecondaryColor3ui
	#undef glSecondaryColor3uiv
	#undef glSecondaryColor3us
	#undef glSecondaryColor3usv
	#undef glSecondaryColorPointer
	#undef glWindowPos2d
	#undef glWindowPos2dv
	#undef glWindowPos2f
	#undef glWindowPos2fv
	#undef glWindowPos2i
	#undef glWindowPos2iv
	#undef glWindowPos2s
	#undef glWindowPos2sv
	#undef glWindowPos3d
	#undef glWindowPos3dv
	#undef glWindowPos3f
	#undef glWindowPos3fv
	#undef glWindowPos3i
	#undef glWindowPos3iv
	#undef glWindowPos3s
	#undef glWindowPos3sv
#ifdef LAP_GL_DEBUG_CONTEXT_COMPATIBILITY
	#define glFogCoordf(...) lap::gl::detail::context->FogCoordf(__VA_ARGS__, __FILE__, __func__, __LINE__)
	#define glFogCoordfv(...) lap::gl::detail::context->FogCoordfv(__VA_ARGS__, __FILE__, __func__, __LINE__)
	#define glFogCoordd(...) lap::gl::detail::context->FogCoordd(__VA_ARGS__, __FILE__, __func__, __LINE__)
	#define glFogCoorddv(...) lap::gl::detail::context->FogCoorddv(__VA_ARGS__, __FILE__, __func__, __LINE__)
	#define glFogCoordPointer(...) lap::gl::detail::context->FogCoordPointer(__VA_ARGS__, __FILE__, __func__, __LINE__)
	#define glSecondaryColor3b(...) lap::gl::detail::context->SecondaryColor3b(__VA_ARGS__, __FILE__, __func__, __LINE__)
	#define glSecondaryColor3bv(...) lap::gl::detail::context->SecondaryColor3bv(__VA_ARGS__, __FILE__, __func__, __LINE__)
	#define glSecondaryColor3d(...) lap::gl::detail::context->SecondaryColor3d(__VA_ARGS__, __FILE__, __func__, __LINE__)
	#define glSecondaryColor3dv(...) lap::gl::detail::context->SecondaryColor3dv(__VA_ARGS__, __FILE__, __func__, __LINE__)
	#define glSecondaryColor3f(...) lap::gl::detail::context->SecondaryColor3f(__VA_ARGS__, __FILE__, __func__, __LINE__)
	#define glSecondaryColor3fv(...) lap::gl::detail::context->SecondaryColor3fv(__VA_ARGS__, __FILE__, __func__, __LINE__)
	#define glSecondaryColor3i(...) lap::gl::detail::context->SecondaryColor3i(__VA_ARGS__, __FILE__, __func__, __LINE__)
	#define glSecondaryColor3iv(...) lap::gl::detail::context->SecondaryColor3iv(__VA_ARGS__, __FILE__, __func__, __LINE__)
	#define glSecondaryColor3s(...) lap::gl::detail::context->SecondaryColor3s(__VA_ARGS__, __FILE__, __func__, __LINE__)
	#define glSecondaryColor3sv(...) lap::gl::detail::context->SecondaryColor3sv(__VA_ARGS__, __FILE__, __func__, __LINE__)
	#define glSecondaryColor3ub(...) lap::gl::detail::context->SecondaryColor3ub(__VA_ARGS__, __FILE__, __func__, __LINE__)
	#define glSecondaryColor3ubv(...) lap::gl::detail::context->SecondaryColor3ubv(__VA_ARGS__, __FILE__, __func__, __LINE__)
	#define glSecondaryColor3ui(...) lap::gl::detail::context->SecondaryColor3ui(__VA_ARGS__, __FILE__, __func__, __LINE__)
	#define glSecondaryColor3uiv(...) lap::gl::detail::context->SecondaryColor3uiv(__VA_ARGS__, __FILE__, __func__, __LINE__)
	#define glSecondaryColor3us(...) lap::gl::detail::context->SecondaryColor3us(__VA_ARGS__, __FILE__, __func__, __LINE__)
	#define glSecondaryColor3usv(...) lap::gl::detail::context->SecondaryColor3usv(__VA_ARGS__, __FILE__, __func__, __LINE__)
	#define glSecondaryColorPointer(...) lap::gl::detail::context->SecondaryColorPointer(__VA_ARGS__, __FILE__, __func__, __LINE__)
	#define glWindowPos2d(...) lap::gl::detail::context->WindowPos2d(__VA_ARGS__, __FILE__, __func__, __LINE__)
	#define glWindowPos2dv(...) lap::gl::detail::context->WindowPos2dv(__VA_ARGS__, __FILE__, __func__, __LINE__)
	#define glWindowPos2f(...) lap::gl::detail::context->WindowPos2f(__VA_ARGS__, __FILE__, __func__, __LINE__)
	#define glWindowPos2fv(...) lap::gl::detail::context->WindowPos2fv(__VA_ARGS__, __FILE__, __func__, __LINE__)
	#define glWindowPos2i(...) lap::gl::detail::context->WindowPos2i(__VA_ARGS__, __FILE__, __func__, __LINE__)
	#define glWindowPos2iv(...) lap::gl::detail::context->WindowPos2iv(__VA_ARGS__, __FILE__, __func__, __LINE__)
	#define glWindowPos2s(...) lap::gl::detail::context->WindowPos2s(__VA_ARGS__, __FILE__, __func__, __LINE__)
	#define glWindowPos2sv(...) lap::gl::detail::context->WindowPos2sv(__VA_ARGS__, __FILE__, __func__, __LINE__)
	#define glWindowPos3d(...) lap::gl::detail::context->WindowPos3d(__VA_ARGS__, __FILE__, __func__, __LINE__)
	#define glWindowPos3dv(...) lap::gl::detail::context->WindowPos3dv(__VA_ARGS__, __FILE__, __func__, __LINE__)
	#define glWindowPos3f(...) lap::gl::detail::context->WindowPos3f(__VA_ARGS__, __FILE__, __func__, __LINE__)
	#define glWindowPos3fv(...) lap::gl::detail::context->WindowPos3fv(__VA_ARGS__, __FILE__, __func__, __LINE__)
	#define glWindowPos3i(...) lap::gl::detail::context->WindowPos3i(__VA_ARGS__, __FILE__, __func__, __LINE__)
	#define glWindowPos3iv(...) lap::gl::detail::context->WindowPos3iv(__VA_ARGS__, __FILE__, __func__, __LINE__)
	#define glWindowPos3s(...) lap::gl::detail::context->WindowPos3s(__VA_ARGS__, __FILE__, __func__, __LINE__)
	#define glWindowPos3sv(...) lap::gl::detail::context->WindowPos3sv(__VA_ARGS__, __FILE__, __func__, __LINE__)
#endif
	//v15
	#undef glGenQueries
	#define glGenQueries(...) lap::gl::detail::context->GenQueries(__VA_ARGS__, __FILE__, __func__, __LINE__)
	#undef glDeleteQueries
	#define glDeleteQueries(...) lap::gl::detail::context->DeleteQueries(__VA_ARGS__, __FILE__, __func__, __LINE__)
	#undef glIsQuery
	#define glIsQuery(...) lap::gl::detail::context->IsQuery(__VA_ARGS__, __FILE__, __func__, __LINE__)
	#undef glBeginQuery
	#define glBeginQuery(...) lap::gl::detail::context->BeginQuery(__VA_ARGS__, __FILE__, __func__, __LINE__)
	#undef glEndQuery
	#define glEndQuery(...) lap::gl::detail::context->EndQuery(__VA_ARGS__, __FILE__, __func__, __LINE__)
	#undef glGetQueryiv
	#define glGetQueryiv(...) lap::gl::detail::context->GetQueryiv(__VA_ARGS__, __FILE__, __func__, __LINE__)
	#undef glGetQueryObjectiv
	#define glGetQueryObjectiv(...) lap::gl::detail::context->GetQueryObjectiv(__VA_ARGS__, __FILE__, __func__, __LINE__)
	#undef glGetQueryObjectuiv
	#define glGetQueryObjectuiv(...) lap::gl::detail::context->GetQueryObjectuiv(__VA_ARGS__, __FILE__, __func__, __LINE__)
	#undef glBindBuffer
	#define glBindBuffer(...) lap::gl::detail::context->BindBuffer(__VA_ARGS__, __FILE__, __func__, __LINE__)
	#undef glDeleteBuffers
	#define glDeleteBuffers(...) lap::gl::detail::context->DeleteBuffers(__VA_ARGS__, __FILE__, __func__, __LINE__)
	#undef glGenBuffers
	#define glGenBuffers(...) lap::gl::detail::context->GenBuffers(__VA_ARGS__, __FILE__, __func__, __LINE__)
	#undef glIsBuffer
	#define glIsBuffer(...) lap::gl::detail::context->IsBuffer(__VA_ARGS__, __FILE__, __func__, __LINE__)
	#undef glBufferData
	#define glBufferData(...) lap::gl::detail::context->BufferData(__VA_ARGS__, __FILE__, __func__, __LINE__)
	#undef glBufferSubData
	#define glBufferSubData(...) lap::gl::detail::context->BufferSubData(__VA_ARGS__, __FILE__, __func__, __LINE__)
	#undef glGetBufferSubData
	#define glGetBufferSubData(...) lap::gl::detail::context->GetBufferSubData(__VA_ARGS__, __FILE__, __func__, __LINE__)
	#undef glMapBuffer
	#define glMapBuffer(...) lap::gl::detail::context->MapBuffer(__VA_ARGS__, __FILE__, __func__, __LINE__)
	#undef glUnmapBuffer
	#define glUnmapBuffer(...) lap::gl::detail::context->UnmapBuffer(__VA_ARGS__, __FILE__, __func__, __LINE__)
	#undef glGetBufferParameteriv
	#define glGetBufferParameteriv(...) lap::gl::detail::context->GetBufferParameteriv(__VA_ARGS__, __FILE__, __func__, __LINE__)
	#undef glGetBufferPointerv
	#define glGetBufferPointerv(...) lap::gl::detail::context->GetBufferPointerv(__VA_ARGS__, __FILE__, __func__, __LINE__)
	//v20
	#undef glBlendEquationSeparate
	#define glBlendEquationSeparate(...) lap::gl::detail::context->BlendEquationSeparate(__VA_ARGS__, __FILE__, __func__, __LINE__)
	#undef glDrawBuffers
	#define glDrawBuffers(...) lap::gl::detail::context->DrawBuffers(__VA_ARGS__, __FILE__, __func__, __LINE__)
	#undef glStencilOpSeparate
	#define glStencilOpSeparate(...) lap::gl::detail::context->StencilOpSeparate(__VA_ARGS__, __FILE__, __func__, __LINE__)
	#undef glStencilFuncSeparate
	#define glStencilFuncSeparate(...) lap::gl::detail::context->StencilFuncSeparate(__VA_ARGS__, __FILE__, __func__, __LINE__)
	#undef glStencilMaskSeparate
	#define glStencilMaskSeparate(...) lap::gl::detail::context->StencilMaskSeparate(__VA_ARGS__, __FILE__, __func__, __LINE__)
	#undef glAttachShader
	#define glAttachShader(...) lap::gl::detail::context->AttachShader(__VA_ARGS__, __FILE__, __func__, __LINE__)
	#undef glBindAttribLocation
	#define glBindAttribLocation(...) lap::gl::detail::context->BindAttribLocation(__VA_ARGS__, __FILE__, __func__, __LINE__)
	#undef glCompileShader
	#define glCompileShader(...) lap::gl::detail::context->CompileShader(__VA_ARGS__, __FILE__, __func__, __LINE__)
	#undef glCreateProgram
	#define glCreateProgram() lap::gl::detail::context->CreateProgram( __FILE__, __func__, __LINE__)
	#undef glCreateShader
	#define glCreateShader(...) lap::gl::detail::context->CreateShader(__VA_ARGS__, __FILE__, __func__, __LINE__)
	#undef glDeleteProgram
	#define glDeleteProgram(...) lap::gl::detail::context->DeleteProgram(__VA_ARGS__, __FILE__, __func__, __LINE__)
	#undef glDeleteShader
	#define glDeleteShader(...) lap::gl::detail::context->DeleteShader(__VA_ARGS__, __FILE__, __func__, __LINE__)
	#undef glDetachShader
	#define glDetachShader(...) lap::gl::detail::context->DetachShader(__VA_ARGS__, __FILE__, __func__, __LINE__)
	#undef glDisableVertexAttribArray
	#define glDisableVertexAttribArray(...) lap::gl::detail::context->DisableVertexAttribArray(__VA_ARGS__, __FILE__, __func__, __LINE__)
	#undef glEnableVertexAttribArray
	#define glEnableVertexAttribArray(...) lap::gl::detail::context->EnableVertexAttribArray(__VA_ARGS__, __FILE__, __func__, __LINE__)
	#undef glGetActiveAttrib
	#define glGetActiveAttrib(...) lap::gl::detail::context->GetActiveAttrib(__VA_ARGS__, __FILE__, __func__, __LINE__)
	#undef glGetActiveUniform
	#define glGetActiveUniform(...) lap::gl::detail::context->GetActiveUniform(__VA_ARGS__, __FILE__, __func__, __LINE__)
	#undef glGetAttachedShaders
	#define glGetAttachedShaders(...) lap::gl::detail::context->GetAttachedShaders(__VA_ARGS__, __FILE__, __func__, __LINE__)
	#undef glGetAttribLocation
	#define glGetAttribLocation(...) lap::gl::detail::context->GetAttribLocation(__VA_ARGS__, __FILE__, __func__, __LINE__)
	#undef glGetProgramiv
	#define glGetProgramiv(...) lap::gl::detail::context->GetProgramiv(__VA_ARGS__, __FILE__, __func__, __LINE__)
	#undef glGetProgramInfoLog
	#define glGetProgramInfoLog(...) lap::gl::detail::context->GetProgramInfoLog(__VA_ARGS__, __FILE__, __func__, __LINE__)
	#undef glGetShaderiv
	#define glGetShaderiv(...) lap::gl::detail::context->GetShaderiv(__VA_ARGS__, __FILE__, __func__, __LINE__)
	#undef glGetShaderInfoLog
	#define glGetShaderInfoLog(...) lap::gl::detail::context->GetShaderInfoLog(__VA_ARGS__, __FILE__, __func__, __LINE__)
	#undef glGetShaderSource
	#define glGetShaderSource(...) lap::gl::detail::context->GetShaderSource(__VA_ARGS__, __FILE__, __func__, __LINE__)
	#undef glGetUniformLocation
	#define glGetUniformLocation(...) lap::gl::detail::context->GetUniformLocation(__VA_ARGS__, __FILE__, __func__, __LINE__)
	#undef glGetUniformfv
	#define glGetUniformfv(...) lap::gl::detail::context->GetUniformfv(__VA_ARGS__, __FILE__, __func__, __LINE__)
	#undef glGetUniformiv
	#define glGetUniformiv(...) lap::gl::detail::context->GetUniformiv(__VA_ARGS__, __FILE__, __func__, __LINE__)
	#undef glGetVertexAttribdv
	#define glGetVertexAttribdv(...) lap::gl::detail::context->GetVertexAttribdv(__VA_ARGS__, __FILE__, __func__, __LINE__)
	#undef glGetVertexAttribfv
	#define glGetVertexAttribfv(...) lap::gl::detail::context->GetVertexAttribfv(__VA_ARGS__, __FILE__, __func__, __LINE__)
	#undef glGetVertexAttribiv
	#define glGetVertexAttribiv(...) lap::gl::detail::context->GetVertexAttribiv(__VA_ARGS__, __FILE__, __func__, __LINE__)
	#undef glGetVertexAttribPointerv
	#define glGetVertexAttribPointerv(...) lap::gl::detail::context->GetVertexAttribPointerv(__VA_ARGS__, __FILE__, __func__, __LINE__)
	#undef glIsProgram
	#define glIsProgram(...) lap::gl::detail::context->IsProgram(__VA_ARGS__, __FILE__, __func__, __LINE__)
	#undef glIsShader
	#define glIsShader(...) lap::gl::detail::context->IsShader(__VA_ARGS__, __FILE__, __func__, __LINE__)
	#undef glLinkProgram
	#define glLinkProgram(...) lap::gl::detail::context->LinkProgram(__VA_ARGS__, __FILE__, __func__, __LINE__)
	#undef glShaderSource
	#define glShaderSource(...) lap::gl::detail::context->ShaderSource(__VA_ARGS__, __FILE__, __func__, __LINE__)
	#undef glUseProgram
	#define glUseProgram(...) lap::gl::detail::context->UseProgram(__VA_ARGS__, __FILE__, __func__, __LINE__)
	#undef glUniform1f
	#define glUniform1f(...) lap::gl::detail::context->Uniform1f(__VA_ARGS__, __FILE__, __func__, __LINE__)
	#undef glUniform2f
	#define glUniform2f(...) lap::gl::detail::context->Uniform2f(__VA_ARGS__, __FILE__, __func__, __LINE__)
	#undef glUniform3f
	#define glUniform3f(...) lap::gl::detail::context->Uniform3f(__VA_ARGS__, __FILE__, __func__, __LINE__)
	#undef glUniform4f
	#define glUniform4f(...) lap::gl::detail::context->Uniform4f(__VA_ARGS__, __FILE__, __func__, __LINE__)
	#undef glUniform1i
	#define glUniform1i(...) lap::gl::detail::context->Uniform1i(__VA_ARGS__, __FILE__, __func__, __LINE__)
	#undef glUniform2i
	#define glUniform2i(...) lap::gl::detail::context->Uniform2i(__VA_ARGS__, __FILE__, __func__, __LINE__)
	#undef glUniform3i
	#define glUniform3i(...) lap::gl::detail::context->Uniform3i(__VA_ARGS__, __FILE__, __func__, __LINE__)
	#undef glUniform4i
	#define glUniform4i(...) lap::gl::detail::context->Uniform4i(__VA_ARGS__, __FILE__, __func__, __LINE__)
	#undef glUniform1fv
	#define glUniform1fv(...) lap::gl::detail::context->Uniform1fv(__VA_ARGS__, __FILE__, __func__, __LINE__)
	#undef glUniform2fv
	#define glUniform2fv(...) lap::gl::detail::context->Uniform2fv(__VA_ARGS__, __FILE__, __func__, __LINE__)
	#undef glUniform3fv
	#define glUniform3fv(...) lap::gl::detail::context->Uniform3fv(__VA_ARGS__, __FILE__, __func__, __LINE__)
	#undef glUniform4fv
	#define glUniform4fv(...) lap::gl::detail::context->Uniform4fv(__VA_ARGS__, __FILE__, __func__, __LINE__)
	#undef glUniform1iv
	#define glUniform1iv(...) lap::gl::detail::context->Uniform1iv(__VA_ARGS__, __FILE__, __func__, __LINE__)
	#undef glUniform2iv
	#define glUniform2iv(...) lap::gl::detail::context->Uniform2iv(__VA_ARGS__, __FILE__, __func__, __LINE__)
	#undef glUniform3iv
	#define glUniform3iv(...) lap::gl::detail::context->Uniform3iv(__VA_ARGS__, __FILE__, __func__, __LINE__)
	#undef glUniform4iv
	#define glUniform4iv(...) lap::gl::detail::context->Uniform4iv(__VA_ARGS__, __FILE__, __func__, __LINE__)
	#undef glUniformMatrix2fv
	#define glUniformMatrix2fv(...) lap::gl::detail::context->UniformMatrix2fv(__VA_ARGS__, __FILE__, __func__, __LINE__)
	#undef glUniformMatrix3fv
	#define glUniformMatrix3fv(...) lap::gl::detail::context->UniformMatrix3fv(__VA_ARGS__, __FILE__, __func__, __LINE__)
	#undef glUniformMatrix4fv
	#define glUniformMatrix4fv(...) lap::gl::detail::context->UniformMatrix4fv(__VA_ARGS__, __FILE__, __func__, __LINE__)
	#undef glValidateProgram
	#define glValidateProgram(...) lap::gl::detail::context->ValidateProgram(__VA_ARGS__, __FILE__, __func__, __LINE__)
	#undef glVertexAttrib1d
	#define glVertexAttrib1d(...) lap::gl::detail::context->VertexAttrib1d(__VA_ARGS__, __FILE__, __func__, __LINE__)
	#undef glVertexAttrib1dv
	#define glVertexAttrib1dv(...) lap::gl::detail::context->VertexAttrib1dv(__VA_ARGS__, __FILE__, __func__, __LINE__)
	#undef glVertexAttrib1f
	#define glVertexAttrib1f(...) lap::gl::detail::context->VertexAttrib1f(__VA_ARGS__, __FILE__, __func__, __LINE__)
	#undef glVertexAttrib1fv
	#define glVertexAttrib1fv(...) lap::gl::detail::context->VertexAttrib1fv(__VA_ARGS__, __FILE__, __func__, __LINE__)
	#undef glVertexAttrib1s
	#define glVertexAttrib1s(...) lap::gl::detail::context->VertexAttrib1s(__VA_ARGS__, __FILE__, __func__, __LINE__)
	#undef glVertexAttrib1sv
	#define glVertexAttrib1sv(...) lap::gl::detail::context->VertexAttrib1sv(__VA_ARGS__, __FILE__, __func__, __LINE__)
	#undef glVertexAttrib2d
	#define glVertexAttrib2d(...) lap::gl::detail::context->VertexAttrib2d(__VA_ARGS__, __FILE__, __func__, __LINE__)
	#undef glVertexAttrib2dv
	#define glVertexAttrib2dv(...) lap::gl::detail::context->VertexAttrib2dv(__VA_ARGS__, __FILE__, __func__, __LINE__)
	#undef glVertexAttrib2f
	#define glVertexAttrib2f(...) lap::gl::detail::context->VertexAttrib2f(__VA_ARGS__, __FILE__, __func__, __LINE__)
	#undef glVertexAttrib2fv
	#define glVertexAttrib2fv(...) lap::gl::detail::context->VertexAttrib2fv(__VA_ARGS__, __FILE__, __func__, __LINE__)
	#undef glVertexAttrib2s
	#define glVertexAttrib2s(...) lap::gl::detail::context->VertexAttrib2s(__VA_ARGS__, __FILE__, __func__, __LINE__)
	#undef glVertexAttrib2sv
	#define glVertexAttrib2sv(...) lap::gl::detail::context->VertexAttrib2sv(__VA_ARGS__, __FILE__, __func__, __LINE__)
	#undef glVertexAttrib3d
	#define glVertexAttrib3d(...) lap::gl::detail::context->VertexAttrib3d(__VA_ARGS__, __FILE__, __func__, __LINE__)
	#undef glVertexAttrib3dv
	#define glVertexAttrib3dv(...) lap::gl::detail::context->VertexAttrib3dv(__VA_ARGS__, __FILE__, __func__, __LINE__)
	#undef glVertexAttrib3f
	#define glVertexAttrib3f(...) lap::gl::detail::context->VertexAttrib3f(__VA_ARGS__, __FILE__, __func__, __LINE__)
	#undef glVertexAttrib3fv
	#define glVertexAttrib3fv(...) lap::gl::detail::context->VertexAttrib3fv(__VA_ARGS__, __FILE__, __func__, __LINE__)
	#undef glVertexAttrib3s
	#define glVertexAttrib3s(...) lap::gl::detail::context->VertexAttrib3s(__VA_ARGS__, __FILE__, __func__, __LINE__)
	#undef glVertexAttrib3sv
	#define glVertexAttrib3sv(...) lap::gl::detail::context->VertexAttrib3sv(__VA_ARGS__, __FILE__, __func__, __LINE__)
	#undef glVertexAttrib4Nbv
	#define glVertexAttrib4Nbv(...) lap::gl::detail::context->VertexAttrib4Nbv(__VA_ARGS__, __FILE__, __func__, __LINE__)
	#undef glVertexAttrib4Niv
	#define glVertexAttrib4Niv(...) lap::gl::detail::context->VertexAttrib4Niv(__VA_ARGS__, __FILE__, __func__, __LINE__)
	#undef glVertexAttrib4Nsv
	#define glVertexAttrib4Nsv(...) lap::gl::detail::context->VertexAttrib4Nsv(__VA_ARGS__, __FILE__, __func__, __LINE__)
	#undef glVertexAttrib4Nub
	#define glVertexAttrib4Nub(...) lap::gl::detail::context->VertexAttrib4Nub(__VA_ARGS__, __FILE__, __func__, __LINE__)
	#undef glVertexAttrib4Nubv
	#define glVertexAttrib4Nubv(...) lap::gl::detail::context->VertexAttrib4Nubv(__VA_ARGS__, __FILE__, __func__, __LINE__)
	#undef glVertexAttrib4Nuiv
	#define glVertexAttrib4Nuiv(...) lap::gl::detail::context->VertexAttrib4Nuiv(__VA_ARGS__, __FILE__, __func__, __LINE__)
	#undef glVertexAttrib4Nusv
	#define glVertexAttrib4Nusv(...) lap::gl::detail::context->VertexAttrib4Nusv(__VA_ARGS__, __FILE__, __func__, __LINE__)
	#undef glVertexAttrib4bv
	#define glVertexAttrib4bv(...) lap::gl::detail::context->VertexAttrib4bv(__VA_ARGS__, __FILE__, __func__, __LINE__)
	#undef glVertexAttrib4d
	#define glVertexAttrib4d(...) lap::gl::detail::context->VertexAttrib4d(__VA_ARGS__, __FILE__, __func__, __LINE__)
	#undef glVertexAttrib4dv
	#define glVertexAttrib4dv(...) lap::gl::detail::context->VertexAttrib4dv(__VA_ARGS__, __FILE__, __func__, __LINE__)
	#undef glVertexAttrib4f
	#define glVertexAttrib4f(...) lap::gl::detail::context->VertexAttrib4f(__VA_ARGS__, __FILE__, __func__, __LINE__)
	#undef glVertexAttrib4fv
	#define glVertexAttrib4fv(...) lap::gl::detail::context->VertexAttrib4fv(__VA_ARGS__, __FILE__, __func__, __LINE__)
	#undef glVertexAttrib4iv
	#define glVertexAttrib4iv(...) lap::gl::detail::context->VertexAttrib4iv(__VA_ARGS__, __FILE__, __func__, __LINE__)
	#undef glVertexAttrib4s
	#define glVertexAttrib4s(...) lap::gl::detail::context->VertexAttrib4s(__VA_ARGS__, __FILE__, __func__, __LINE__)
	#undef glVertexAttrib4sv
	#define glVertexAttrib4sv(...) lap::gl::detail::context->VertexAttrib4sv(__VA_ARGS__, __FILE__, __func__, __LINE__)
	#undef glVertexAttrib4ubv
	#define glVertexAttrib4ubv(...) lap::gl::detail::context->VertexAttrib4ubv(__VA_ARGS__, __FILE__, __func__, __LINE__)
	#undef glVertexAttrib4uiv
	#define glVertexAttrib4uiv(...) lap::gl::detail::context->VertexAttrib4uiv(__VA_ARGS__, __FILE__, __func__, __LINE__)
	#undef glVertexAttrib4usv
	#define glVertexAttrib4usv(...) lap::gl::detail::context->VertexAttrib4usv(__VA_ARGS__, __FILE__, __func__, __LINE__)
	#undef glVertexAttribPointer
	#define glVertexAttribPointer(...) lap::gl::detail::context->VertexAttribPointer(__VA_ARGS__, __FILE__, __func__, __LINE__)

	//v20 deprecated
	#undef GetMinMax
	#undef GetMinmaxParameterfv
	#undef GetMinmaxParameteriv
	#undef ConvolutionFilter1D
	#undef ConvolutionFilter2D
	#undef ConvolutionParameterf
	#undef ConvolutionParameteri
	#undef CopyConvolutionFilter1D
	#undef CopyConvolutionFilter2D
	#undef ColorSubTable
	#undef ColorTable
	#undef ColorTableParameterfv
	#undef ColorTableParameteriv
	#undef GetSeparableFilter
	#undef GetColorTable
	#undef GetColorTableParameterfv
	#undef GetColorTableParameteriv
	#undef GetConvolutionFilter
	#undef GetConvolutionParameterfv
	#undef GetConvolutionParameteriv
	#undef GetHistogram
	#undef GetHistogramParameterfv
	#undef GetHistogramParameteriv
	#undef Histogram
	#undef Minmax
	#undef ResetHistogram
	#undef ResetMinmax
	#undef SeparableFilter2D
#ifdef LAP_GL_DEBUG_CONTEXT_COMPATIBILITY
#ifdef LAP_GL_DEBUG_CONTEXT_COMPATIBILITY_IMAGING
	#define glGetMinMax(...) lap::gl::detail::context->GetMinMax(__VA_ARGS__, __FILE__, __func__, __LINE__)
	#define glGetMinmaxParameterfv(...) lap::gl::detail::context->GetMinmaxParameterfv(__VA_ARGS__, __FILE__, __func__, __LINE__)
	#define glGetMinmaxParameteriv(...) lap::gl::detail::context->GetMinmaxParameteriv(__VA_ARGS__, __FILE__, __func__, __LINE__)
	#define glConvolutionFilter1D(...) lap::gl::detail::context->ConvolutionFilter1D(__VA_ARGS__, __FILE__, __func__, __LINE__)
	#define glConvolutionFilter2D(...) lap::gl::detail::context->ConvolutionFilter2D(__VA_ARGS__, __FILE__, __func__, __LINE__)
	#define glConvolutionParameterf(...) lap::gl::detail::context->ConvolutionParameterf(__VA_ARGS__, __FILE__, __func__, __LINE__)
	#define glConvolutionParameteri(...) lap::gl::detail::context->ConvolutionParameteri(__VA_ARGS__, __FILE__, __func__, __LINE__)
	#define glCopyConvolutionFilter1D(...) lap::gl::detail::context->CopyConvolutionFilter1D(__VA_ARGS__, __FILE__, __func__, __LINE__)
	#define glCopyConvolutionFilter2D(...) lap::gl::detail::context->CopyConvolutionFilter2D(__VA_ARGS__, __FILE__, __func__, __LINE__)
	#define glColorSubTable(...) lap::gl::detail::context->ColorSubTable(__VA_ARGS__, __FILE__, __func__, __LINE__)
	#define glColorTable(...) lap::gl::detail::context->ColorTable(__VA_ARGS__, __FILE__, __func__, __LINE__)
	#define glColorTableParameterfv(...) lap::gl::detail::context->ColorTableParameterfv(__VA_ARGS__, __FILE__, __func__, __LINE__)
	#define glColorTableParameteriv(...) lap::gl::detail::context->ColorTableParameteriv(__VA_ARGS__, __FILE__, __func__, __LINE__)
	#define glGetSeparableFilter(...) lap::gl::detail::context->GetSeparableFilter(__VA_ARGS__, __FILE__, __func__, __LINE__)
	#define glGetColorTable(...) lap::gl::detail::context->GetColorTable(__VA_ARGS__, __FILE__, __func__, __LINE__)
	#define glGetColorTableParameterfv(...) lap::gl::detail::context->GetColorTableParameterfv(__VA_ARGS__, __FILE__, __func__, __LINE__)
	#define glGetColorTableParameteriv(...) lap::gl::detail::context->GetColorTableParameteriv(__VA_ARGS__, __FILE__, __func__, __LINE__)
	#define glGetConvolutionFilter(...) lap::gl::detail::context->GetConvolutionFilter(__VA_ARGS__, __FILE__, __func__, __LINE__)
	#define glGetConvolutionParameterfv(...) lap::gl::detail::context->GetConvolutionParameterfv(__VA_ARGS__, __FILE__, __func__, __LINE__)
	#define glGetConvolutionParameteriv(...) lap::gl::detail::context->GetConvolutionParameteriv(__VA_ARGS__, __FILE__, __func__, __LINE__)
	#define glGetHistogram(...) lap::gl::detail::context->GetHistogram(__VA_ARGS__, __FILE__, __func__, __LINE__)
	#define glGetHistogramParameterfv(...) lap::gl::detail::context->GetHistogramParameterfv(__VA_ARGS__, __FILE__, __func__, __LINE__)
	#define glGetHistogramParameteriv(...) lap::gl::detail::context->GetHistogramParameteriv(__VA_ARGS__, __FILE__, __func__, __LINE__)
	#define glHistogram(...) lap::gl::detail::context->Histogram(__VA_ARGS__, __FILE__, __func__, __LINE__)
	#define glMinmax(...) lap::gl::detail::context->Minmax(__VA_ARGS__, __FILE__, __func__, __LINE__)
	#define glResetHistogram(...) lap::gl::detail::context->ResetHistogram(__VA_ARGS__, __FILE__, __func__, __LINE__)
	#define glResetMinmax(...) lap::gl::detail::context->ResetMinmax(__VA_ARGS__, __FILE__, __func__, __LINE__)
	#define glSeparableFilter2D(...) lap::gl::detail::context->SeparableFilter2D(__VA_ARGS__, __FILE__, __func__, __LINE__)
#endif
#endif





	//v21
	#undef glUniformMatrix2x3fv
	#define glUniformMatrix2x3fv(...) lap::gl::detail::context->UniformMatrix2x3fv(__VA_ARGS__, __FILE__, __func__, __LINE__)
	#undef glUniformMatrix3x2fv
	#define glUniformMatrix3x2fv(...) lap::gl::detail::context->UniformMatrix3x2fv(__VA_ARGS__, __FILE__, __func__, __LINE__)
	#undef glUniformMatrix2x4fv
	#define glUniformMatrix2x4fv(...) lap::gl::detail::context->UniformMatrix2x4fv(__VA_ARGS__, __FILE__, __func__, __LINE__)
	#undef glUniformMatrix4x2fv
	#define glUniformMatrix4x2fv(...) lap::gl::detail::context->UniformMatrix4x2fv(__VA_ARGS__, __FILE__, __func__, __LINE__)
	#undef glUniformMatrix3x4fv
	#define glUniformMatrix3x4fv(...) lap::gl::detail::context->UniformMatrix3x4fv(__VA_ARGS__, __FILE__, __func__, __LINE__)
	#undef glUniformMatrix4x3fv
	#define glUniformMatrix4x3fv(...) lap::gl::detail::context->UniformMatrix4x3fv(__VA_ARGS__, __FILE__, __func__, __LINE__)
	//v30
	#undef glColorMaski
	#define glColorMaski(...) lap::gl::detail::context->ColorMaski(__VA_ARGS__, __FILE__, __func__, __LINE__)
	#undef glGetBooleani_v
	#define glGetBooleani_v(...) lap::gl::detail::context->GetBooleani_v(__VA_ARGS__, __FILE__, __func__, __LINE__)
	#undef glGetIntegeri_v
	#define glGetIntegeri_v(...) lap::gl::detail::context->GetIntegeri_v(__VA_ARGS__, __FILE__, __func__, __LINE__)
	#undef glEnablei
	#define glEnablei(...) lap::gl::detail::context->Enablei(__VA_ARGS__, __FILE__, __func__, __LINE__)
	#undef glDisablei
	#define glDisablei(...) lap::gl::detail::context->Disablei(__VA_ARGS__, __FILE__, __func__, __LINE__)
	#undef glIsEnabledi
	#define glIsEnabledi(...) lap::gl::detail::context->IsEnabledi(__VA_ARGS__, __FILE__, __func__, __LINE__)
	#undef glBeginTransformFeedback
	#define glBeginTransformFeedback(...) lap::gl::detail::context->BeginTransformFeedback(__VA_ARGS__, __FILE__, __func__, __LINE__)
	#undef glEndTransformFeedback
	#define glEndTransformFeedback() lap::gl::detail::context->EndTransformFeedback(__FILE__, __func__, __LINE__)
	#undef glBindBufferRange
	#define glBindBufferRange(...) lap::gl::detail::context->BindBufferRange(__VA_ARGS__, __FILE__, __func__, __LINE__)
	#undef glBindBufferBase
	#define glBindBufferBase(...) lap::gl::detail::context->BindBufferBase(__VA_ARGS__, __FILE__, __func__, __LINE__)
	#undef glTransformFeedbackVaryings
	#define glTransformFeedbackVaryings(...) lap::gl::detail::context->TransformFeedbackVaryings(__VA_ARGS__, __FILE__, __func__, __LINE__)
	#undef glGetTransformFeedbackVarying
	#define glGetTransformFeedbackVarying(...) lap::gl::detail::context->GetTransformFeedbackVarying(__VA_ARGS__, __FILE__, __func__, __LINE__)
	#undef glClampColor
	#define glClampColor(...) lap::gl::detail::context->ClampColor(__VA_ARGS__, __FILE__, __func__, __LINE__)
	#undef glBeginConditionalRender
	#define glBeginConditionalRender(...) lap::gl::detail::context->BeginConditionalRender(__VA_ARGS__, __FILE__, __func__, __LINE__)
	#undef glEndConditionalRender
	#define glEndConditionalRender() lap::gl::detail::context->EndConditionalRender(__FILE__, __func__, __LINE__)
	#undef glVertexAttribIPointer
	#define glVertexAttribIPointer(...) lap::gl::detail::context->VertexAttribIPointer(__VA_ARGS__, __FILE__, __func__, __LINE__)
	#undef glGetVertexAttribIiv
	#define glGetVertexAttribIiv(...) lap::gl::detail::context->GetVertexAttribIiv(__VA_ARGS__, __FILE__, __func__, __LINE__)
	#undef glGetVertexAttribIuiv
	#define glGetVertexAttribIuiv(...) lap::gl::detail::context->GetVertexAttribIuiv(__VA_ARGS__, __FILE__, __func__, __LINE__)
	#undef glVertexAttribI1i
	#define glVertexAttribI1i(...) lap::gl::detail::context->VertexAttribI1i(__VA_ARGS__, __FILE__, __func__, __LINE__)
	#undef glVertexAttribI2i
	#define glVertexAttribI2i(...) lap::gl::detail::context->VertexAttribI2i(__VA_ARGS__, __FILE__, __func__, __LINE__)
	#undef glVertexAttribI3i
	#define glVertexAttribI3i(...) lap::gl::detail::context->VertexAttribI3i(__VA_ARGS__, __FILE__, __func__, __LINE__)
	#undef glVertexAttribI4i
	#define glVertexAttribI4i(...) lap::gl::detail::context->VertexAttribI4i(__VA_ARGS__, __FILE__, __func__, __LINE__)
	#undef glVertexAttribI1ui
	#define glVertexAttribI1ui(...) lap::gl::detail::context->VertexAttribI1ui(__VA_ARGS__, __FILE__, __func__, __LINE__)
	#undef glVertexAttribI2ui
	#define glVertexAttribI2ui(...) lap::gl::detail::context->VertexAttribI2ui(__VA_ARGS__, __FILE__, __func__, __LINE__)
	#undef glVertexAttribI3ui
	#define glVertexAttribI3ui(...) lap::gl::detail::context->VertexAttribI3ui(__VA_ARGS__, __FILE__, __func__, __LINE__)
	#undef glVertexAttribI4ui
	#define glVertexAttribI4ui(...) lap::gl::detail::context->VertexAttribI4ui(__VA_ARGS__, __FILE__, __func__, __LINE__)
	#undef glVertexAttribI1iv
	#define glVertexAttribI1iv(...) lap::gl::detail::context->VertexAttribI1iv(__VA_ARGS__, __FILE__, __func__, __LINE__)
	#undef glVertexAttribI2iv
	#define glVertexAttribI2iv(...) lap::gl::detail::context->VertexAttribI2iv(__VA_ARGS__, __FILE__, __func__, __LINE__)
	#undef glVertexAttribI3iv
	#define glVertexAttribI3iv(...) lap::gl::detail::context->VertexAttribI3iv(__VA_ARGS__, __FILE__, __func__, __LINE__)
	#undef glVertexAttribI4iv
	#define glVertexAttribI4iv(...) lap::gl::detail::context->VertexAttribI4iv(__VA_ARGS__, __FILE__, __func__, __LINE__)
	#undef glVertexAttribI1uiv
	#define glVertexAttribI1uiv(...) lap::gl::detail::context->VertexAttribI1uiv(__VA_ARGS__, __FILE__, __func__, __LINE__)
	#undef glVertexAttribI2uiv
	#define glVertexAttribI2uiv(...) lap::gl::detail::context->VertexAttribI2uiv(__VA_ARGS__, __FILE__, __func__, __LINE__)
	#undef glVertexAttribI3uiv
	#define glVertexAttribI3uiv(...) lap::gl::detail::context->VertexAttribI3uiv(__VA_ARGS__, __FILE__, __func__, __LINE__)
	#undef glVertexAttribI4uiv
	#define glVertexAttribI4uiv(...) lap::gl::detail::context->VertexAttribI4uiv(__VA_ARGS__, __FILE__, __func__, __LINE__)
	#undef glVertexAttribI4bv
	#define glVertexAttribI4bv(...) lap::gl::detail::context->VertexAttribI4bv(__VA_ARGS__, __FILE__, __func__, __LINE__)
	#undef glVertexAttribI4sv
	#define glVertexAttribI4sv(...) lap::gl::detail::context->VertexAttribI4sv(__VA_ARGS__, __FILE__, __func__, __LINE__)
	#undef glVertexAttribI4ubv
	#define glVertexAttribI4ubv(...) lap::gl::detail::context->VertexAttribI4ubv(__VA_ARGS__, __FILE__, __func__, __LINE__)
	#undef glVertexAttribI4usv
	#define glVertexAttribI4usv(...) lap::gl::detail::context->VertexAttribI4usv(__VA_ARGS__, __FILE__, __func__, __LINE__)
	#undef glGetUniformuiv
	#define glGetUniformuiv(...) lap::gl::detail::context->GetUniformuiv(__VA_ARGS__, __FILE__, __func__, __LINE__)
	#undef glBindFragDataLocation
	#define glBindFragDataLocation(...) lap::gl::detail::context->BindFragDataLocation(__VA_ARGS__, __FILE__, __func__, __LINE__)
	#undef glGetFragDataLocation
	#define glGetFragDataLocation(...) lap::gl::detail::context->GetFragDataLocation(__VA_ARGS__, __FILE__, __func__, __LINE__)
	#undef glUniform1ui
	#define glUniform1ui(...) lap::gl::detail::context->Uniform1ui(__VA_ARGS__, __FILE__, __func__, __LINE__)
	#undef glUniform2ui
	#define glUniform2ui(...) lap::gl::detail::context->Uniform2ui(__VA_ARGS__, __FILE__, __func__, __LINE__)
	#undef glUniform3ui
	#define glUniform3ui(...) lap::gl::detail::context->Uniform3ui(__VA_ARGS__, __FILE__, __func__, __LINE__)
	#undef glUniform4ui
	#define glUniform4ui(...) lap::gl::detail::context->Uniform4ui(__VA_ARGS__, __FILE__, __func__, __LINE__)
	#undef glUniform1uiv
	#define glUniform1uiv(...) lap::gl::detail::context->Uniform1uiv(__VA_ARGS__, __FILE__, __func__, __LINE__)
	#undef glUniform2uiv
	#define glUniform2uiv(...) lap::gl::detail::context->Uniform2uiv(__VA_ARGS__, __FILE__, __func__, __LINE__)
	#undef glUniform3uiv
	#define glUniform3uiv(...) lap::gl::detail::context->Uniform3uiv(__VA_ARGS__, __FILE__, __func__, __LINE__)
	#undef glUniform4uiv
	#define glUniform4uiv(...) lap::gl::detail::context->Uniform4uiv(__VA_ARGS__, __FILE__, __func__, __LINE__)
	#undef glTexParameterIiv
	#define glTexParameterIiv(...) lap::gl::detail::context->TexParameterIiv(__VA_ARGS__, __FILE__, __func__, __LINE__)
	#undef glTexParameterIuiv
	#define glTexParameterIuiv(...) lap::gl::detail::context->TexParameterIuiv(__VA_ARGS__, __FILE__, __func__, __LINE__)
	#undef glGetTexParameterIiv
	#define glGetTexParameterIiv(...) lap::gl::detail::context->GetTexParameterIiv(__VA_ARGS__, __FILE__, __func__, __LINE__)
	#undef glGetTexParameterIuiv
	#define glGetTexParameterIuiv(...) lap::gl::detail::context->GetTexParameterIuiv(__VA_ARGS__, __FILE__, __func__, __LINE__)
	#undef glClearBufferiv
	#define glClearBufferiv(...) lap::gl::detail::context->ClearBufferiv(__VA_ARGS__, __FILE__, __func__, __LINE__)
	#undef glClearBufferuiv
	#define glClearBufferuiv(...) lap::gl::detail::context->ClearBufferuiv(__VA_ARGS__, __FILE__, __func__, __LINE__)
	#undef glClearBufferfv
	#define glClearBufferfv(...) lap::gl::detail::context->ClearBufferfv(__VA_ARGS__, __FILE__, __func__, __LINE__)
	#undef glClearBufferfi
	#define glClearBufferfi(...) lap::gl::detail::context->ClearBufferfi(__VA_ARGS__, __FILE__, __func__, __LINE__)
	#undef glGetStringi
	#define glGetStringi(...) lap::gl::detail::context->GetStringi(__VA_ARGS__, __FILE__, __func__, __LINE__)
	#undef glIsRenderbuffer
	#define glIsRenderbuffer(...) lap::gl::detail::context->IsRenderbuffer(__VA_ARGS__, __FILE__, __func__, __LINE__)
	#undef glBindRenderbuffer
	#define glBindRenderbuffer(...) lap::gl::detail::context->BindRenderbuffer(__VA_ARGS__, __FILE__, __func__, __LINE__)
	#undef glDeleteRenderbuffers
	#define glDeleteRenderbuffers(...) lap::gl::detail::context->DeleteRenderbuffers(__VA_ARGS__, __FILE__, __func__, __LINE__)
	#undef glGenRenderbuffers
	#define glGenRenderbuffers(...) lap::gl::detail::context->GenRenderbuffers(__VA_ARGS__, __FILE__, __func__, __LINE__)
	#undef glRenderbufferStorage
	#define glRenderbufferStorage(...) lap::gl::detail::context->RenderbufferStorage(__VA_ARGS__, __FILE__, __func__, __LINE__)
	#undef glGetRenderbufferParameteriv
	#define glGetRenderbufferParameteriv(...) lap::gl::detail::context->GetRenderbufferParameteriv(__VA_ARGS__, __FILE__, __func__, __LINE__)
	#undef glIsFramebuffer
	#define glIsFramebuffer(...) lap::gl::detail::context->IsFramebuffer(__VA_ARGS__, __FILE__, __func__, __LINE__)
	#undef glBindFramebuffer
	#define glBindFramebuffer(...) lap::gl::detail::context->BindFramebuffer(__VA_ARGS__, __FILE__, __func__, __LINE__)
	#undef glDeleteFramebuffers
	#define glDeleteFramebuffers(...) lap::gl::detail::context->DeleteFramebuffers(__VA_ARGS__, __FILE__, __func__, __LINE__)
	#undef glGenFramebuffers
	#define glGenFramebuffers(...) lap::gl::detail::context->GenFramebuffers(__VA_ARGS__, __FILE__, __func__, __LINE__)
	#undef glCheckFramebufferStatus
	#define glCheckFramebufferStatus(...) lap::gl::detail::context->CheckFramebufferStatus(__VA_ARGS__, __FILE__, __func__, __LINE__)
	#undef glFramebufferTexture1D
	#define glFramebufferTexture1D(...) lap::gl::detail::context->FramebufferTexture1D(__VA_ARGS__, __FILE__, __func__, __LINE__)
	#undef glFramebufferTexture2D
	#define glFramebufferTexture2D(...) lap::gl::detail::context->FramebufferTexture2D(__VA_ARGS__, __FILE__, __func__, __LINE__)
	#undef glFramebufferTexture3D
	#define glFramebufferTexture3D(...) lap::gl::detail::context->FramebufferTexture3D(__VA_ARGS__, __FILE__, __func__, __LINE__)
	#undef glFramebufferRenderbuffer
	#define glFramebufferRenderbuffer(...) lap::gl::detail::context->FramebufferRenderbuffer(__VA_ARGS__, __FILE__, __func__, __LINE__)
	#undef glGetFramebufferAttachmentParameteriv
	#define glGetFramebufferAttachmentParameteriv(...) lap::gl::detail::context->GetFramebufferAttachmentParameteriv(__VA_ARGS__, __FILE__, __func__, __LINE__)
	#undef glGenerateMipmap
	#define glGenerateMipmap(...) lap::gl::detail::context->GenerateMipmap(__VA_ARGS__, __FILE__, __func__, __LINE__)
	#undef glBlitFramebuffer
	#define glBlitFramebuffer(...) lap::gl::detail::context->BlitFramebuffer(__VA_ARGS__, __FILE__, __func__, __LINE__)
	#undef glRenderbufferStorageMultisample
	#define glRenderbufferStorageMultisample(...) lap::gl::detail::context->RenderbufferStorageMultisample(__VA_ARGS__, __FILE__, __func__, __LINE__)
	#undef glFramebufferTextureLayer
	#define glFramebufferTextureLayer(...) lap::gl::detail::context->FramebufferTextureLayer(__VA_ARGS__, __FILE__, __func__, __LINE__)
	#undef glMapBufferRange
	#define glMapBufferRange(...) lap::gl::detail::context->MapBufferRange(__VA_ARGS__, __FILE__, __func__, __LINE__)
	#undef glFlushMappedBufferRange
	#define glFlushMappedBufferRange(...) lap::gl::detail::context->FlushMappedBufferRange(__VA_ARGS__, __FILE__, __func__, __LINE__)
	#undef glBindVertexArray
	#define glBindVertexArray(...) lap::gl::detail::context->BindVertexArray(__VA_ARGS__, __FILE__, __func__, __LINE__)
	#undef glDeleteVertexArrays
	#define glDeleteVertexArrays(...) lap::gl::detail::context->DeleteVertexArrays(__VA_ARGS__, __FILE__, __func__, __LINE__)
	#undef glGenVertexArrays
	#define glGenVertexArrays(...) lap::gl::detail::context->GenVertexArrays(__VA_ARGS__, __FILE__, __func__, __LINE__)
	#undef glIsVertexArray
	#define glIsVertexArray(...) lap::gl::detail::context->IsVertexArray(__VA_ARGS__, __FILE__, __func__, __LINE__)
	//v31
	#undef glDrawArraysInstanced
	#define glDrawArraysInstanced(...) lap::gl::detail::context->DrawArraysInstanced(__VA_ARGS__, __FILE__, __func__, __LINE__)
	#undef glDrawElementsInstanced
	#define glDrawElementsInstanced(...) lap::gl::detail::context->DrawElementsInstanced(__VA_ARGS__, __FILE__, __func__, __LINE__)
	#undef glTexBuffer
	#define glTexBuffer(...) lap::gl::detail::context->TexBuffer(__VA_ARGS__, __FILE__, __func__, __LINE__)
	#undef glPrimitiveRestartIndex
	#define glPrimitiveRestartIndex(...) lap::gl::detail::context->PrimitiveRestartIndex(__VA_ARGS__, __FILE__, __func__, __LINE__)
	#undef glCopyBufferSubData
	#define glCopyBufferSubData(...) lap::gl::detail::context->CopyBufferSubData(__VA_ARGS__, __FILE__, __func__, __LINE__)
	#undef glGetUniformIndices
	#define glGetUniformIndices(...) lap::gl::detail::context->GetUniformIndices(__VA_ARGS__, __FILE__, __func__, __LINE__)
	#undef glGetActiveUniformsiv
	#define glGetActiveUniformsiv(...) lap::gl::detail::context->GetActiveUniformsiv(__VA_ARGS__, __FILE__, __func__, __LINE__)
	#undef glGetActiveUniformName
	#define glGetActiveUniformName(...) lap::gl::detail::context->GetActiveUniformName(__VA_ARGS__, __FILE__, __func__, __LINE__)
	#undef glGetUniformBlockIndex
	#define glGetUniformBlockIndex(...) lap::gl::detail::context->GetUniformBlockIndex(__VA_ARGS__, __FILE__, __func__, __LINE__)
	#undef glGetActiveUniformBlockiv
	#define glGetActiveUniformBlockiv(...) lap::gl::detail::context->GetActiveUniformBlockiv(__VA_ARGS__, __FILE__, __func__, __LINE__)
	#undef glGetActiveUniformBlockName
	#define glGetActiveUniformBlockName(...) lap::gl::detail::context->GetActiveUniformBlockName(__VA_ARGS__, __FILE__, __func__, __LINE__)
	#undef glUniformBlockBinding
	#define glUniformBlockBinding(...) lap::gl::detail::context->UniformBlockBinding(__VA_ARGS__, __FILE__, __func__, __LINE__)
	//v32
	#undef glDrawElementsBaseVertex
	#define glDrawElementsBaseVertex(...) lap::gl::detail::context->DrawElementsBaseVertex(__VA_ARGS__, __FILE__, __func__, __LINE__)
	#undef glDrawRangeElementsBaseVertex
	#define glDrawRangeElementsBaseVertex(...) lap::gl::detail::context->DrawRangeElementsBaseVertex(__VA_ARGS__, __FILE__, __func__, __LINE__)
	#undef glDrawElementsInstancedBaseVertex
	#define glDrawElementsInstancedBaseVertex(...) lap::gl::detail::context->DrawElementsInstancedBaseVertex(__VA_ARGS__, __FILE__, __func__, __LINE__)
	#undef glMultiDrawElementsBaseVertex
	#define glMultiDrawElementsBaseVertex(...) lap::gl::detail::context->MultiDrawElementsBaseVertex(__VA_ARGS__, __FILE__, __func__, __LINE__)
	#undef glProvokingVertex
	#define glProvokingVertex(...) lap::gl::detail::context->ProvokingVertex(__VA_ARGS__, __FILE__, __func__, __LINE__)
	#undef glFenceSync
	#define glFenceSync(...) lap::gl::detail::context->FenceSync(__VA_ARGS__, __FILE__, __func__, __LINE__)
	#undef glIsSync
	#define glIsSync(...) lap::gl::detail::context->IsSync(__VA_ARGS__, __FILE__, __func__, __LINE__)
	#undef glDeleteSync
	#define glDeleteSync(...) lap::gl::detail::context->DeleteSync(__VA_ARGS__, __FILE__, __func__, __LINE__)
	#undef glClientWaitSync
	#define glClientWaitSync(...) lap::gl::detail::context->ClientWaitSync(__VA_ARGS__, __FILE__, __func__, __LINE__)
	#undef glWaitSync
	#define glWaitSync(...) lap::gl::detail::context->WaitSync(__VA_ARGS__, __FILE__, __func__, __LINE__)
	#undef glGetInteger64v
	#define glGetInteger64v(...) lap::gl::detail::context->GetInteger64v(__VA_ARGS__, __FILE__, __func__, __LINE__)
	#undef glGetSynciv
	#define glGetSynciv(...) lap::gl::detail::context->GetSynciv(__VA_ARGS__, __FILE__, __func__, __LINE__)
	#undef glGetInteger64i_v
	#define glGetInteger64i_v(...) lap::gl::detail::context->GetInteger64i_v(__VA_ARGS__, __FILE__, __func__, __LINE__)
	#undef glGetBufferParameteri64v
	#define glGetBufferParameteri64v(...) lap::gl::detail::context->GetBufferParameteri64v(__VA_ARGS__, __FILE__, __func__, __LINE__)
	#undef glFramebufferTexture
	#define glFramebufferTexture(...) lap::gl::detail::context->FramebufferTexture(__VA_ARGS__, __FILE__, __func__, __LINE__)
	#undef glTexImage2DMultisample
	#define glTexImage2DMultisample(...) lap::gl::detail::context->TexImage2DMultisample(__VA_ARGS__, __FILE__, __func__, __LINE__)
	#undef glTexImage3DMultisample
	#define glTexImage3DMultisample(...) lap::gl::detail::context->TexImage3DMultisample(__VA_ARGS__, __FILE__, __func__, __LINE__)
	#undef glGetMultisamplefv
	#define glGetMultisamplefv(...) lap::gl::detail::context->GetMultisamplefv(__VA_ARGS__, __FILE__, __func__, __LINE__)
	#undef glSampleMaski
	#define glSampleMaski(...) lap::gl::detail::context->SampleMaski(__VA_ARGS__, __FILE__, __func__, __LINE__)

	//v32 deprecated (vertex_type_2_10_10_10_rev and arb_robustness)
	#undef glVertexP2ui
	#undef glVertexP2uiv
	#undef glVertexP3ui
	#undef glVertexP3uiv
	#undef glVertexP4ui
	#undef glVertexP4uiv
	#undef glTexCoordP1ui
	#undef glTexCoordP1uiv
	#undef glTexCoordP2ui
	#undef glTexCoordP2uiv
	#undef glTexCoordP3ui
	#undef glTexCoordP3uiv
	#undef glTexCoordP4ui
	#undef glTexCoordP4uiv
	#undef glMultiTexCoordP1ui
	#undef glMultiTexCoordP1uiv
	#undef glMultiTexCoordP2ui
	#undef glMultiTexCoordP2uiv
	#undef glMultiTexCoordP3ui
	#undef glMultiTexCoordP3uiv
	#undef glMultiTexCoordP4ui
	#undef glMultiTexCoordP4uiv
	#undef glNormalP3ui
	#undef glNormalP3uiv
	#undef glColorP3ui
	#undef glColorP3uiv
	#undef glColorP4ui
	#undef glColorP4uiv
	#undef glSecondaryColorP3ui
	#undef glSecondaryColorP3uiv
	#undef glGetnMapdv
	#undef glGetnMapfv
	#undef glGetnMapiv
	#undef glGetnPixelMapfv
	#undef glGetnPixelMapuiv
	#undef glGetnPixelMapusv
	#undef glGetnPolygonStipple

	#undef glGetnColorTable
	#undef glGetnConvolutionFilter
	#undef glGetnSeparableFilter
	#undef glGetnHistogram
	#undef glGetnMinmax

#ifdef LAP_GL_DEBUG_CONTEXT_COMPATIBILITY
	//v32 deprecated (one extension brought them all..)
	#define glVertexP2ui(...) lap::gl::detail::context->VertexP2ui(__VA_ARGS__, __FILE__, __func__, __LINE__)
	#define glVertexP2uiv(...) lap::gl::detail::context->VertexP2uiv(__VA_ARGS__, __FILE__, __func__, __LINE__)
	#define glVertexP3ui(...) lap::gl::detail::context->VertexP3ui(__VA_ARGS__, __FILE__, __func__, __LINE__)
	#define glVertexP3uiv(...) lap::gl::detail::context->VertexP3uiv(__VA_ARGS__, __FILE__, __func__, __LINE__)
	#define glVertexP4ui(...) lap::gl::detail::context->VertexP4ui(__VA_ARGS__, __FILE__, __func__, __LINE__)
	#define glVertexP4uiv(...) lap::gl::detail::context->VertexP4uiv(__VA_ARGS__, __FILE__, __func__, __LINE__)
	#define glTexCoordP1ui(...) lap::gl::detail::context->TexCoordP1ui(__VA_ARGS__, __FILE__, __func__, __LINE__)
	#define glTexCoordP1uiv(...) lap::gl::detail::context->TexCoordP1uiv(__VA_ARGS__, __FILE__, __func__, __LINE__)
	#define glTexCoordP2ui(...) lap::gl::detail::context->TexCoordP2ui(__VA_ARGS__, __FILE__, __func__, __LINE__)
	#define glTexCoordP2uiv(...) lap::gl::detail::context->TexCoordP2uiv(__VA_ARGS__, __FILE__, __func__, __LINE__)
	#define glTexCoordP3ui(...) lap::gl::detail::context->TexCoordP3ui(__VA_ARGS__, __FILE__, __func__, __LINE__)
	#define glTexCoordP3uiv(...) lap::gl::detail::context->TexCoordP3uiv(__VA_ARGS__, __FILE__, __func__, __LINE__)
	#define glTexCoordP4ui(...) lap::gl::detail::context->TexCoordP4ui(__VA_ARGS__, __FILE__, __func__, __LINE__)
	#define glTexCoordP4uiv(...) lap::gl::detail::context->TexCoordP4uiv(__VA_ARGS__, __FILE__, __func__, __LINE__)
	#define glMultiTexCoordP1ui(...) lap::gl::detail::context->MultiTexCoordP1ui(__VA_ARGS__, __FILE__, __func__, __LINE__)
	#define glMultiTexCoordP1uiv(...) lap::gl::detail::context->MultiTexCoordP1uiv(__VA_ARGS__, __FILE__, __func__, __LINE__)
	#define glMultiTexCoordP2ui(...) lap::gl::detail::context->MultiTexCoordP2ui(__VA_ARGS__, __FILE__, __func__, __LINE__)
	#define glMultiTexCoordP2uiv(...) lap::gl::detail::context->MultiTexCoordP2uiv(__VA_ARGS__, __FILE__, __func__, __LINE__)
	#define glMultiTexCoordP3ui(...) lap::gl::detail::context->MultiTexCoordP3ui(__VA_ARGS__, __FILE__, __func__, __LINE__)
	#define glMultiTexCoordP3uiv(...) lap::gl::detail::context->MultiTexCoordP3uiv(__VA_ARGS__, __FILE__, __func__, __LINE__)
	#define glMultiTexCoordP4ui(...) lap::gl::detail::context->MultiTexCoordP4ui(__VA_ARGS__, __FILE__, __func__, __LINE__)
	#define glMultiTexCoordP4uiv(...) lap::gl::detail::context->MultiTexCoordP4uiv(__VA_ARGS__, __FILE__, __func__, __LINE__)
	#define glNormalP3ui(...) lap::gl::detail::context->NormalP3ui(__VA_ARGS__, __FILE__, __func__, __LINE__)
	#define glNormalP3uiv(...) lap::gl::detail::context->NormalP3uiv(__VA_ARGS__, __FILE__, __func__, __LINE__)
	#define glColorP3ui(...) lap::gl::detail::context->ColorP3ui(__VA_ARGS__, __FILE__, __func__, __LINE__)
	#define glColorP3uiv(...) lap::gl::detail::context->ColorP3uiv(__VA_ARGS__, __FILE__, __func__, __LINE__)
	#define glColorP4ui(...) lap::gl::detail::context->ColorP4ui(__VA_ARGS__, __FILE__, __func__, __LINE__)
	#define glColorP4uiv(...) lap::gl::detail::context->ColorP4uiv(__VA_ARGS__, __FILE__, __func__, __LINE__)
	#define glSecondaryColorP3ui(...) lap::gl::detail::context->SecondaryColorP3ui(__VA_ARGS__, __FILE__, __func__, __LINE__)
	#define glSecondaryColorP3uiv(...) lap::gl::detail::context->SecondaryColorP3uiv(__VA_ARGS__, __FILE__, __func__, __LINE__)
	#define glGetnMapdv(...) lap::gl::detail::context->GetnMapdv(__VA_ARGS__, __FILE__, __func__, __LINE__)
	#define glGetnMapfv(...) lap::gl::detail::context->GetnMapfv(__VA_ARGS__, __FILE__, __func__, __LINE__)
	#define glGetnMapiv(...) lap::gl::detail::context->GetnMapiv(__VA_ARGS__, __FILE__, __func__, __LINE__)
	#define glGetnPixelMapfv(...) lap::gl::detail::context->GetnPixelMapfv(__VA_ARGS__, __FILE__, __func__, __LINE__)
	#define glGetnPixelMapuiv(...) lap::gl::detail::context->GetnPixelMapuiv(__VA_ARGS__, __FILE__, __func__, __LINE__)
	#define glGetnPixelMapusv(...) lap::gl::detail::context->GetnPixelMapusv(__VA_ARGS__, __FILE__, __func__, __LINE__)
	#define glGetnPolygonStipple(...) lap::gl::detail::context->GetnPolygonStipple(__VA_ARGS__, __FILE__, __func__, __LINE__)
#ifdef LAP_GL_DEBUG_CONTEXT_COMPATIBILITY_IMAGING
	#define glGetnColorTable(...) lap::gl::detail::context->GetnColorTable(__VA_ARGS__, __FILE__, __func__, __LINE__)
	#define glGetnConvolutionFilter(...) lap::gl::detail::context->GetnConvolutionFilter(__VA_ARGS__, __FILE__, __func__, __LINE__)
	#define glGetnSeparableFilter(...) lap::gl::detail::context->GetnSeparableFilter(__VA_ARGS__, __FILE__, __func__, __LINE__)
	#define glGetnHistogram(...) lap::gl::detail::context->GetnHistogram(__VA_ARGS__, __FILE__, __func__, __LINE__)
	#define glGetnMinmax(...) lap::gl::detail::context->GetnMinmax(__VA_ARGS__, __FILE__, __func__, __LINE__)
#endif
#endif






	//v33
	#undef glBindFragDataLocationIndexed
	#define glBindFragDataLocationIndexed(...) lap::gl::detail::context->BindFragDataLocationIndexed(__VA_ARGS__, __FILE__, __func__, __LINE__)
	#undef glGetFragDataIndex
	#define glGetFragDataIndex(...) lap::gl::detail::context->GetFragDataIndex(__VA_ARGS__, __FILE__, __func__, __LINE__)
	#undef glGenSamplers
	#define glGenSamplers(...) lap::gl::detail::context->GenSamplers(__VA_ARGS__, __FILE__, __func__, __LINE__)
	#undef glDeleteSamplers
	#define glDeleteSamplers(...) lap::gl::detail::context->DeleteSamplers(__VA_ARGS__, __FILE__, __func__, __LINE__)
	#undef glIsSampler
	#define glIsSampler(...) lap::gl::detail::context->IsSampler(__VA_ARGS__, __FILE__, __func__, __LINE__)
	#undef glBindSampler
	#define glBindSampler(...) lap::gl::detail::context->BindSampler(__VA_ARGS__, __FILE__, __func__, __LINE__)
	#undef glSamplerParameteri
	#define glSamplerParameteri(...) lap::gl::detail::context->SamplerParameteri(__VA_ARGS__, __FILE__, __func__, __LINE__)
	#undef glSamplerParameteriv
	#define glSamplerParameteriv(...) lap::gl::detail::context->SamplerParameteriv(__VA_ARGS__, __FILE__, __func__, __LINE__)
	#undef glSamplerParameterf
	#define glSamplerParameterf(...) lap::gl::detail::context->SamplerParameterf(__VA_ARGS__, __FILE__, __func__, __LINE__)
	#undef glSamplerParameterfv
	#define glSamplerParameterfv(...) lap::gl::detail::context->SamplerParameterfv(__VA_ARGS__, __FILE__, __func__, __LINE__)
	#undef glSamplerParameterIiv
	#define glSamplerParameterIiv(...) lap::gl::detail::context->SamplerParameterIiv(__VA_ARGS__, __FILE__, __func__, __LINE__)
	#undef glSamplerParameterIuiv
	#define glSamplerParameterIuiv(...) lap::gl::detail::context->SamplerParameterIuiv(__VA_ARGS__, __FILE__, __func__, __LINE__)
	#undef glGetSamplerParameteriv
	#define glGetSamplerParameteriv(...) lap::gl::detail::context->GetSamplerParameteriv(__VA_ARGS__, __FILE__, __func__, __LINE__)
	#undef glGetSamplerParameterIiv
	#define glGetSamplerParameterIiv(...) lap::gl::detail::context->GetSamplerParameterIiv(__VA_ARGS__, __FILE__, __func__, __LINE__)
	#undef glGetSamplerParameterfv
	#define glGetSamplerParameterfv(...) lap::gl::detail::context->GetSamplerParameterfv(__VA_ARGS__, __FILE__, __func__, __LINE__)
	#undef glGetSamplerParameterIuiv
	#define glGetSamplerParameterIuiv(...) lap::gl::detail::context->GetSamplerParameterIuiv(__VA_ARGS__, __FILE__, __func__, __LINE__)
	#undef glQueryCounter
	#define glQueryCounter(...) lap::gl::detail::context->QueryCounter(__VA_ARGS__, __FILE__, __func__, __LINE__)
	#undef glGetQueryObjecti64v
	#define glGetQueryObjecti64v(...) lap::gl::detail::context->GetQueryObjecti64v(__VA_ARGS__, __FILE__, __func__, __LINE__)
	#undef glGetQueryObjectui64v
	#define glGetQueryObjectui64v(...) lap::gl::detail::context->GetQueryObjectui64v(__VA_ARGS__, __FILE__, __func__, __LINE__)
	#undef glVertexAttribDivisor
	#define glVertexAttribDivisor(...) lap::gl::detail::context->VertexAttribDivisor(__VA_ARGS__, __FILE__, __func__, __LINE__)
	#undef glVertexAttribP1ui
	#define glVertexAttribP1ui(...) lap::gl::detail::context->VertexAttribP1ui(__VA_ARGS__, __FILE__, __func__, __LINE__)
	#undef glVertexAttribP1uiv
	#define glVertexAttribP1uiv(...) lap::gl::detail::context->VertexAttribP1uiv(__VA_ARGS__, __FILE__, __func__, __LINE__)
	#undef glVertexAttribP2ui
	#define glVertexAttribP2ui(...) lap::gl::detail::context->VertexAttribP2ui(__VA_ARGS__, __FILE__, __func__, __LINE__)
	#undef glVertexAttribP2uiv
	#define glVertexAttribP2uiv(...) lap::gl::detail::context->VertexAttribP2uiv(__VA_ARGS__, __FILE__, __func__, __LINE__)
	#undef glVertexAttribP3ui
	#define glVertexAttribP3ui(...) lap::gl::detail::context->VertexAttribP3ui(__VA_ARGS__, __FILE__, __func__, __LINE__)
	#undef glVertexAttribP3uiv
	#define glVertexAttribP3uiv(...) lap::gl::detail::context->VertexAttribP3uiv(__VA_ARGS__, __FILE__, __func__, __LINE__)
	#undef glVertexAttribP4ui
	#define glVertexAttribP4ui(...) lap::gl::detail::context->VertexAttribP4ui(__VA_ARGS__, __FILE__, __func__, __LINE__)
	#undef glVertexAttribP4uiv
	#define glVertexAttribP4uiv(...) lap::gl::detail::context->VertexAttribP4uiv(__VA_ARGS__, __FILE__, __func__, __LINE__)
	//v40
	#undef glMinSampleShading
	#define glMinSampleShading(...) lap::gl::detail::context->MinSampleShading(__VA_ARGS__, __FILE__, __func__, __LINE__)
	#undef glBlendEquationi
	#define glBlendEquationi(...) lap::gl::detail::context->BlendEquationi(__VA_ARGS__, __FILE__, __func__, __LINE__)
	#undef glBlendEquationSeparatei
	#define glBlendEquationSeparatei(...) lap::gl::detail::context->BlendEquationSeparatei(__VA_ARGS__, __FILE__, __func__, __LINE__)
	#undef glBlendFunci
	#define glBlendFunci(...) lap::gl::detail::context->BlendFunci(__VA_ARGS__, __FILE__, __func__, __LINE__)
	#undef glBlendFuncSeparatei
	#define glBlendFuncSeparatei(...) lap::gl::detail::context->BlendFuncSeparatei(__VA_ARGS__, __FILE__, __func__, __LINE__)
	#undef glDrawArraysIndirect
	#define glDrawArraysIndirect(...) lap::gl::detail::context->DrawArraysIndirect(__VA_ARGS__, __FILE__, __func__, __LINE__)
	#undef glDrawElementsIndirect
	#define glDrawElementsIndirect(...) lap::gl::detail::context->DrawElementsIndirect(__VA_ARGS__, __FILE__, __func__, __LINE__)
	#undef glUniform1d
	#define glUniform1d(...) lap::gl::detail::context->Uniform1d(__VA_ARGS__, __FILE__, __func__, __LINE__)
	#undef glUniform2d
	#define glUniform2d(...) lap::gl::detail::context->Uniform2d(__VA_ARGS__, __FILE__, __func__, __LINE__)
	#undef glUniform3d
	#define glUniform3d(...) lap::gl::detail::context->Uniform3d(__VA_ARGS__, __FILE__, __func__, __LINE__)
	#undef glUniform4d
	#define glUniform4d(...) lap::gl::detail::context->Uniform4d(__VA_ARGS__, __FILE__, __func__, __LINE__)
	#undef glUniform1dv
	#define glUniform1dv(...) lap::gl::detail::context->Uniform1dv(__VA_ARGS__, __FILE__, __func__, __LINE__)
	#undef glUniform2dv
	#define glUniform2dv(...) lap::gl::detail::context->Uniform2dv(__VA_ARGS__, __FILE__, __func__, __LINE__)
	#undef glUniform3dv
	#define glUniform3dv(...) lap::gl::detail::context->Uniform3dv(__VA_ARGS__, __FILE__, __func__, __LINE__)
	#undef glUniform4dv
	#define glUniform4dv(...) lap::gl::detail::context->Uniform4dv(__VA_ARGS__, __FILE__, __func__, __LINE__)
	#undef glUniformMatrix2dv
	#define glUniformMatrix2dv(...) lap::gl::detail::context->UniformMatrix2dv(__VA_ARGS__, __FILE__, __func__, __LINE__)
	#undef glUniformMatrix3dv
	#define glUniformMatrix3dv(...) lap::gl::detail::context->UniformMatrix3dv(__VA_ARGS__, __FILE__, __func__, __LINE__)
	#undef glUniformMatrix4dv
	#define glUniformMatrix4dv(...) lap::gl::detail::context->UniformMatrix4dv(__VA_ARGS__, __FILE__, __func__, __LINE__)
	#undef glUniformMatrix2x3dv
	#define glUniformMatrix2x3dv(...) lap::gl::detail::context->UniformMatrix2x3dv(__VA_ARGS__, __FILE__, __func__, __LINE__)
	#undef glUniformMatrix2x4dv
	#define glUniformMatrix2x4dv(...) lap::gl::detail::context->UniformMatrix2x4dv(__VA_ARGS__, __FILE__, __func__, __LINE__)
	#undef glUniformMatrix3x2dv
	#define glUniformMatrix3x2dv(...) lap::gl::detail::context->UniformMatrix3x2dv(__VA_ARGS__, __FILE__, __func__, __LINE__)
	#undef glUniformMatrix3x4dv
	#define glUniformMatrix3x4dv(...) lap::gl::detail::context->UniformMatrix3x4dv(__VA_ARGS__, __FILE__, __func__, __LINE__)
	#undef glUniformMatrix4x2dv
	#define glUniformMatrix4x2dv(...) lap::gl::detail::context->UniformMatrix4x2dv(__VA_ARGS__, __FILE__, __func__, __LINE__)
	#undef glUniformMatrix4x3dv
	#define glUniformMatrix4x3dv(...) lap::gl::detail::context->UniformMatrix4x3dv(__VA_ARGS__, __FILE__, __func__, __LINE__)
	#undef glGetUniformdv
	#define glGetUniformdv(...) lap::gl::detail::context->GetUniformdv(__VA_ARGS__, __FILE__, __func__, __LINE__)
	#undef glGetSubroutineUniformLocation
	#define glGetSubroutineUniformLocation(...) lap::gl::detail::context->GetSubroutineUniformLocation(__VA_ARGS__, __FILE__, __func__, __LINE__)
	#undef glGetSubroutineIndex
	#define glGetSubroutineIndex(...) lap::gl::detail::context->GetSubroutineIndex(__VA_ARGS__, __FILE__, __func__, __LINE__)
	#undef glGetActiveSubroutineUniformiv
	#define glGetActiveSubroutineUniformiv(...) lap::gl::detail::context->GetActiveSubroutineUniformiv(__VA_ARGS__, __FILE__, __func__, __LINE__)
	#undef glGetActiveSubroutineUniformName
	#define glGetActiveSubroutineUniformName(...) lap::gl::detail::context->GetActiveSubroutineUniformName(__VA_ARGS__, __FILE__, __func__, __LINE__)
	#undef glGetActiveSubroutineName
	#define glGetActiveSubroutineName(...) lap::gl::detail::context->GetActiveSubroutineName(__VA_ARGS__, __FILE__, __func__, __LINE__)
	#undef glUniformSubroutinesuiv
	#define glUniformSubroutinesuiv(...) lap::gl::detail::context->UniformSubroutinesuiv(__VA_ARGS__, __FILE__, __func__, __LINE__)
	#undef glGetUniformSubroutineuiv
	#define glGetUniformSubroutineuiv(...) lap::gl::detail::context->GetUniformSubroutineuiv(__VA_ARGS__, __FILE__, __func__, __LINE__)
	#undef glGetProgramStageiv
	#define glGetProgramStageiv(...) lap::gl::detail::context->GetProgramStageiv(__VA_ARGS__, __FILE__, __func__, __LINE__)
	#undef glPatchParameteri
	#define glPatchParameteri(...) lap::gl::detail::context->PatchParameteri(__VA_ARGS__, __FILE__, __func__, __LINE__)
	#undef glPatchParameterfv
	#define glPatchParameterfv(...) lap::gl::detail::context->PatchParameterfv(__VA_ARGS__, __FILE__, __func__, __LINE__)
	#undef glBindTransformFeedback
	#define glBindTransformFeedback(...) lap::gl::detail::context->BindTransformFeedback(__VA_ARGS__, __FILE__, __func__, __LINE__)
	#undef glDeleteTransformFeedbacks
	#define glDeleteTransformFeedbacks(...) lap::gl::detail::context->DeleteTransformFeedbacks(__VA_ARGS__, __FILE__, __func__, __LINE__)
	#undef glGenTransformFeedbacks
	#define glGenTransformFeedbacks(...) lap::gl::detail::context->GenTransformFeedbacks(__VA_ARGS__, __FILE__, __func__, __LINE__)
	#undef glIsTransformFeedback
	#define glIsTransformFeedback(...) lap::gl::detail::context->IsTransformFeedback(__VA_ARGS__, __FILE__, __func__, __LINE__)
	#undef glPauseTransformFeedback
	#define glPauseTransformFeedback() lap::gl::detail::context->PauseTransformFeedback( __FILE__, __func__, __LINE__)
	#undef glResumeTransformFeedback
	#define glResumeTransformFeedback() lap::gl::detail::context->ResumeTransformFeedback( __FILE__, __func__, __LINE__)
	#undef glDrawTransformFeedback
	#define glDrawTransformFeedback(...) lap::gl::detail::context->DrawTransformFeedback(__VA_ARGS__, __FILE__, __func__, __LINE__)
	#undef glDrawTransformFeedbackStream
	#define glDrawTransformFeedbackStream(...) lap::gl::detail::context->DrawTransformFeedbackStream(__VA_ARGS__, __FILE__, __func__, __LINE__)
	#undef glBeginQueryIndexed
	#define glBeginQueryIndexed(...) lap::gl::detail::context->BeginQueryIndexed(__VA_ARGS__, __FILE__, __func__, __LINE__)
	#undef glEndQueryIndexed
	#define glEndQueryIndexed(...) lap::gl::detail::context->EndQueryIndexed(__VA_ARGS__, __FILE__, __func__, __LINE__)
	#undef glGetQueryIndexediv
	#define glGetQueryIndexediv(...) lap::gl::detail::context->GetQueryIndexediv(__VA_ARGS__, __FILE__, __func__, __LINE__)
	//v41
	#undef glReleaseShaderCompiler
	#define glReleaseShaderCompiler() lap::gl::detail::context->ReleaseShaderCompiler(__FILE__, __func__, __LINE__)
	#undef glShaderBinary
	#define glShaderBinary(...) lap::gl::detail::context->ShaderBinary(__VA_ARGS__, __FILE__, __func__, __LINE__)
	#undef glGetShaderPrecisionFormat
	#define glGetShaderPrecisionFormat(...) lap::gl::detail::context->GetShaderPrecisionFormat(__VA_ARGS__, __FILE__, __func__, __LINE__)
	#undef glDepthRangef
	#define glDepthRangef(...) lap::gl::detail::context->DepthRangef(__VA_ARGS__, __FILE__, __func__, __LINE__)
	#undef glClearDepthf
	#define glClearDepthf(...) lap::gl::detail::context->ClearDepthf(__VA_ARGS__, __FILE__, __func__, __LINE__)
	#undef glGetProgramBinary
	#define glGetProgramBinary(...) lap::gl::detail::context->GetProgramBinary(__VA_ARGS__, __FILE__, __func__, __LINE__)
	#undef glProgramBinary
	#define glProgramBinary(...) lap::gl::detail::context->ProgramBinary(__VA_ARGS__, __FILE__, __func__, __LINE__)
	#undef glProgramParameteri
	#define glProgramParameteri(...) lap::gl::detail::context->ProgramParameteri(__VA_ARGS__, __FILE__, __func__, __LINE__)
	#undef glUseProgramStages
	#define glUseProgramStages(...) lap::gl::detail::context->UseProgramStages(__VA_ARGS__, __FILE__, __func__, __LINE__)
	#undef glActiveShaderProgram
	#define glActiveShaderProgram(...) lap::gl::detail::context->ActiveShaderProgram(__VA_ARGS__, __FILE__, __func__, __LINE__)
	#undef glCreateShaderProgramv
	#define glCreateShaderProgramv(...) lap::gl::detail::context->CreateShaderProgramv(__VA_ARGS__, __FILE__, __func__, __LINE__)
	#undef glBindProgramPipeline
	#define glBindProgramPipeline(...) lap::gl::detail::context->BindProgramPipeline(__VA_ARGS__, __FILE__, __func__, __LINE__)
	#undef glDeleteProgramPipelines
	#define glDeleteProgramPipelines(...) lap::gl::detail::context->DeleteProgramPipelines(__VA_ARGS__, __FILE__, __func__, __LINE__)
	#undef glGenProgramPipelines
	#define glGenProgramPipelines(...) lap::gl::detail::context->GenProgramPipelines(__VA_ARGS__, __FILE__, __func__, __LINE__)
	#undef glIsProgramPipeline
	#define glIsProgramPipeline(...) lap::gl::detail::context->IsProgramPipeline(__VA_ARGS__, __FILE__, __func__, __LINE__)
	#undef glGetProgramPipelineiv
	#define glGetProgramPipelineiv(...) lap::gl::detail::context->GetProgramPipelineiv(__VA_ARGS__, __FILE__, __func__, __LINE__)
	#undef glProgramUniform1i
	#define glProgramUniform1i(...) lap::gl::detail::context->ProgramUniform1i(__VA_ARGS__, __FILE__, __func__, __LINE__)
	#undef glProgramUniform1iv
	#define glProgramUniform1iv(...) lap::gl::detail::context->ProgramUniform1iv(__VA_ARGS__, __FILE__, __func__, __LINE__)
	#undef glProgramUniform1f
	#define glProgramUniform1f(...) lap::gl::detail::context->ProgramUniform1f(__VA_ARGS__, __FILE__, __func__, __LINE__)
	#undef glProgramUniform1fv
	#define glProgramUniform1fv(...) lap::gl::detail::context->ProgramUniform1fv(__VA_ARGS__, __FILE__, __func__, __LINE__)
	#undef glProgramUniform1d
	#define glProgramUniform1d(...) lap::gl::detail::context->ProgramUniform1d(__VA_ARGS__, __FILE__, __func__, __LINE__)
	#undef glProgramUniform1dv
	#define glProgramUniform1dv(...) lap::gl::detail::context->ProgramUniform1dv(__VA_ARGS__, __FILE__, __func__, __LINE__)
	#undef glProgramUniform1ui
	#define glProgramUniform1ui(...) lap::gl::detail::context->ProgramUniform1ui(__VA_ARGS__, __FILE__, __func__, __LINE__)
	#undef glProgramUniform1uiv
	#define glProgramUniform1uiv(...) lap::gl::detail::context->ProgramUniform1uiv(__VA_ARGS__, __FILE__, __func__, __LINE__)
	#undef glProgramUniform2i
	#define glProgramUniform2i(...) lap::gl::detail::context->ProgramUniform2i(__VA_ARGS__, __FILE__, __func__, __LINE__)
	#undef glProgramUniform2iv
	#define glProgramUniform2iv(...) lap::gl::detail::context->ProgramUniform2iv(__VA_ARGS__, __FILE__, __func__, __LINE__)
	#undef glProgramUniform2f
	#define glProgramUniform2f(...) lap::gl::detail::context->ProgramUniform2f(__VA_ARGS__, __FILE__, __func__, __LINE__)
	#undef glProgramUniform2fv
	#define glProgramUniform2fv(...) lap::gl::detail::context->ProgramUniform2fv(__VA_ARGS__, __FILE__, __func__, __LINE__)
	#undef glProgramUniform2d
	#define glProgramUniform2d(...) lap::gl::detail::context->ProgramUniform2d(__VA_ARGS__, __FILE__, __func__, __LINE__)
	#undef glProgramUniform2dv
	#define glProgramUniform2dv(...) lap::gl::detail::context->ProgramUniform2dv(__VA_ARGS__, __FILE__, __func__, __LINE__)
	#undef glProgramUniform2ui
	#define glProgramUniform2ui(...) lap::gl::detail::context->ProgramUniform2ui(__VA_ARGS__, __FILE__, __func__, __LINE__)
	#undef glProgramUniform2uiv
	#define glProgramUniform2uiv(...) lap::gl::detail::context->ProgramUniform2uiv(__VA_ARGS__, __FILE__, __func__, __LINE__)
	#undef glProgramUniform3i
	#define glProgramUniform3i(...) lap::gl::detail::context->ProgramUniform3i(__VA_ARGS__, __FILE__, __func__, __LINE__)
	#undef glProgramUniform3iv
	#define glProgramUniform3iv(...) lap::gl::detail::context->ProgramUniform3iv(__VA_ARGS__, __FILE__, __func__, __LINE__)
	#undef glProgramUniform3f
	#define glProgramUniform3f(...) lap::gl::detail::context->ProgramUniform3f(__VA_ARGS__, __FILE__, __func__, __LINE__)
	#undef glProgramUniform3fv
	#define glProgramUniform3fv(...) lap::gl::detail::context->ProgramUniform3fv(__VA_ARGS__, __FILE__, __func__, __LINE__)
	#undef glProgramUniform3d
	#define glProgramUniform3d(...) lap::gl::detail::context->ProgramUniform3d(__VA_ARGS__, __FILE__, __func__, __LINE__)
	#undef glProgramUniform3dv
	#define glProgramUniform3dv(...) lap::gl::detail::context->ProgramUniform3dv(__VA_ARGS__, __FILE__, __func__, __LINE__)
	#undef glProgramUniform3ui
	#define glProgramUniform3ui(...) lap::gl::detail::context->ProgramUniform3ui(__VA_ARGS__, __FILE__, __func__, __LINE__)
	#undef glProgramUniform3uiv
	#define glProgramUniform3uiv(...) lap::gl::detail::context->ProgramUniform3uiv(__VA_ARGS__, __FILE__, __func__, __LINE__)
	#undef glProgramUniform4i
	#define glProgramUniform4i(...) lap::gl::detail::context->ProgramUniform4i(__VA_ARGS__, __FILE__, __func__, __LINE__)
	#undef glProgramUniform4iv
	#define glProgramUniform4iv(...) lap::gl::detail::context->ProgramUniform4iv(__VA_ARGS__, __FILE__, __func__, __LINE__)
	#undef glProgramUniform4f
	#define glProgramUniform4f(...) lap::gl::detail::context->ProgramUniform4f(__VA_ARGS__, __FILE__, __func__, __LINE__)
	#undef glProgramUniform4fv
	#define glProgramUniform4fv(...) lap::gl::detail::context->ProgramUniform4fv(__VA_ARGS__, __FILE__, __func__, __LINE__)
	#undef glProgramUniform4d
	#define glProgramUniform4d(...) lap::gl::detail::context->ProgramUniform4d(__VA_ARGS__, __FILE__, __func__, __LINE__)
	#undef glProgramUniform4dv
	#define glProgramUniform4dv(...) lap::gl::detail::context->ProgramUniform4dv(__VA_ARGS__, __FILE__, __func__, __LINE__)
	#undef glProgramUniform4ui
	#define glProgramUniform4ui(...) lap::gl::detail::context->ProgramUniform4ui(__VA_ARGS__, __FILE__, __func__, __LINE__)
	#undef glProgramUniform4uiv
	#define glProgramUniform4uiv(...) lap::gl::detail::context->ProgramUniform4uiv(__VA_ARGS__, __FILE__, __func__, __LINE__)
	#undef glProgramUniformMatrix2fv
	#define glProgramUniformMatrix2fv(...) lap::gl::detail::context->ProgramUniformMatrix2fv(__VA_ARGS__, __FILE__, __func__, __LINE__)
	#undef glProgramUniformMatrix3fv
	#define glProgramUniformMatrix3fv(...) lap::gl::detail::context->ProgramUniformMatrix3fv(__VA_ARGS__, __FILE__, __func__, __LINE__)
	#undef glProgramUniformMatrix4fv
	#define glProgramUniformMatrix4fv(...) lap::gl::detail::context->ProgramUniformMatrix4fv(__VA_ARGS__, __FILE__, __func__, __LINE__)
	#undef glProgramUniformMatrix2dv
	#define glProgramUniformMatrix2dv(...) lap::gl::detail::context->ProgramUniformMatrix2dv(__VA_ARGS__, __FILE__, __func__, __LINE__)
	#undef glProgramUniformMatrix3dv
	#define glProgramUniformMatrix3dv(...) lap::gl::detail::context->ProgramUniformMatrix3dv(__VA_ARGS__, __FILE__, __func__, __LINE__)
	#undef glProgramUniformMatrix4dv
	#define glProgramUniformMatrix4dv(...) lap::gl::detail::context->ProgramUniformMatrix4dv(__VA_ARGS__, __FILE__, __func__, __LINE__)
	#undef glProgramUniformMatrix2x3fv
	#define glProgramUniformMatrix2x3fv(...) lap::gl::detail::context->ProgramUniformMatrix2x3fv(__VA_ARGS__, __FILE__, __func__, __LINE__)
	#undef glProgramUniformMatrix3x2fv
	#define glProgramUniformMatrix3x2fv(...) lap::gl::detail::context->ProgramUniformMatrix3x2fv(__VA_ARGS__, __FILE__, __func__, __LINE__)
	#undef glProgramUniformMatrix2x4fv
	#define glProgramUniformMatrix2x4fv(...) lap::gl::detail::context->ProgramUniformMatrix2x4fv(__VA_ARGS__, __FILE__, __func__, __LINE__)
	#undef glProgramUniformMatrix4x2fv
	#define glProgramUniformMatrix4x2fv(...) lap::gl::detail::context->ProgramUniformMatrix4x2fv(__VA_ARGS__, __FILE__, __func__, __LINE__)
	#undef glProgramUniformMatrix3x4fv
	#define glProgramUniformMatrix3x4fv(...) lap::gl::detail::context->ProgramUniformMatrix3x4fv(__VA_ARGS__, __FILE__, __func__, __LINE__)
	#undef glProgramUniformMatrix4x3fv
	#define glProgramUniformMatrix4x3fv(...) lap::gl::detail::context->ProgramUniformMatrix4x3fv(__VA_ARGS__, __FILE__, __func__, __LINE__)
	#undef glProgramUniformMatrix2x3dv
	#define glProgramUniformMatrix2x3dv(...) lap::gl::detail::context->ProgramUniformMatrix2x3dv(__VA_ARGS__, __FILE__, __func__, __LINE__)
	#undef glProgramUniformMatrix3x2dv
	#define glProgramUniformMatrix3x2dv(...) lap::gl::detail::context->ProgramUniformMatrix3x2dv(__VA_ARGS__, __FILE__, __func__, __LINE__)
	#undef glProgramUniformMatrix2x4dv
	#define glProgramUniformMatrix2x4dv(...) lap::gl::detail::context->ProgramUniformMatrix2x4dv(__VA_ARGS__, __FILE__, __func__, __LINE__)
	#undef glProgramUniformMatrix4x2dv
	#define glProgramUniformMatrix4x2dv(...) lap::gl::detail::context->ProgramUniformMatrix4x2dv(__VA_ARGS__, __FILE__, __func__, __LINE__)
	#undef glProgramUniformMatrix3x4dv
	#define glProgramUniformMatrix3x4dv(...) lap::gl::detail::context->ProgramUniformMatrix3x4dv(__VA_ARGS__, __FILE__, __func__, __LINE__)
	#undef glProgramUniformMatrix4x3dv
	#define glProgramUniformMatrix4x3dv(...) lap::gl::detail::context->ProgramUniformMatrix4x3dv(__VA_ARGS__, __FILE__, __func__, __LINE__)
	#undef glValidateProgramPipeline
	#define glValidateProgramPipeline(...) lap::gl::detail::context->ValidateProgramPipeline(__VA_ARGS__, __FILE__, __func__, __LINE__)
	#undef glGetProgramPipelineInfoLog
	#define glGetProgramPipelineInfoLog(...) lap::gl::detail::context->GetProgramPipelineInfoLog(__VA_ARGS__, __FILE__, __func__, __LINE__)
	#undef glVertexAttribL1d
	#define glVertexAttribL1d(...) lap::gl::detail::context->VertexAttribL1d(__VA_ARGS__, __FILE__, __func__, __LINE__)
	#undef glVertexAttribL2d
	#define glVertexAttribL2d(...) lap::gl::detail::context->VertexAttribL2d(__VA_ARGS__, __FILE__, __func__, __LINE__)
	#undef glVertexAttribL3d
	#define glVertexAttribL3d(...) lap::gl::detail::context->VertexAttribL3d(__VA_ARGS__, __FILE__, __func__, __LINE__)
	#undef glVertexAttribL4d
	#define glVertexAttribL4d(...) lap::gl::detail::context->VertexAttribL4d(__VA_ARGS__, __FILE__, __func__, __LINE__)
	#undef glVertexAttribL1dv
	#define glVertexAttribL1dv(...) lap::gl::detail::context->VertexAttribL1dv(__VA_ARGS__, __FILE__, __func__, __LINE__)
	#undef glVertexAttribL2dv
	#define glVertexAttribL2dv(...) lap::gl::detail::context->VertexAttribL2dv(__VA_ARGS__, __FILE__, __func__, __LINE__)
	#undef glVertexAttribL3dv
	#define glVertexAttribL3dv(...) lap::gl::detail::context->VertexAttribL3dv(__VA_ARGS__, __FILE__, __func__, __LINE__)
	#undef glVertexAttribL4dv
	#define glVertexAttribL4dv(...) lap::gl::detail::context->VertexAttribL4dv(__VA_ARGS__, __FILE__, __func__, __LINE__)
	#undef glVertexAttribLPointer
	#define glVertexAttribLPointer(...) lap::gl::detail::context->VertexAttribLPointer(__VA_ARGS__, __FILE__, __func__, __LINE__)
	#undef glGetVertexAttribLdv
	#define glGetVertexAttribLdv(...) lap::gl::detail::context->GetVertexAttribLdv(__VA_ARGS__, __FILE__, __func__, __LINE__)
	#undef glViewportArrayv
	#define glViewportArrayv(...) lap::gl::detail::context->ViewportArrayv(__VA_ARGS__, __FILE__, __func__, __LINE__)
	#undef glViewportIndexedf
	#define glViewportIndexedf(...) lap::gl::detail::context->ViewportIndexedf(__VA_ARGS__, __FILE__, __func__, __LINE__)
	#undef glViewportIndexedfv
	#define glViewportIndexedfv(...) lap::gl::detail::context->ViewportIndexedfv(__VA_ARGS__, __FILE__, __func__, __LINE__)
	#undef glScissorArrayv
	#define glScissorArrayv(...) lap::gl::detail::context->ScissorArrayv(__VA_ARGS__, __FILE__, __func__, __LINE__)
	#undef glScissorIndexed
	#define glScissorIndexed(...) lap::gl::detail::context->ScissorIndexed(__VA_ARGS__, __FILE__, __func__, __LINE__)
	#undef glScissorIndexedv
	#define glScissorIndexedv(...) lap::gl::detail::context->ScissorIndexedv(__VA_ARGS__, __FILE__, __func__, __LINE__)
	#undef glDepthRangeArrayv
	#define glDepthRangeArrayv(...) lap::gl::detail::context->DepthRangeArrayv(__VA_ARGS__, __FILE__, __func__, __LINE__)
	#undef glDepthRangeIndexed
	#define glDepthRangeIndexed(...) lap::gl::detail::context->DepthRangeIndexed(__VA_ARGS__, __FILE__, __func__, __LINE__)
	#undef glGetFloati_v
	#define glGetFloati_v(...) lap::gl::detail::context->GetFloati_v(__VA_ARGS__, __FILE__, __func__, __LINE__)
	#undef glGetDoublei_v
	#define glGetDoublei_v(...) lap::gl::detail::context->GetDoublei_v(__VA_ARGS__, __FILE__, __func__, __LINE__)
	//v42
	#undef glDrawArraysInstancedBaseInstance
	#define glDrawArraysInstancedBaseInstance(...) lap::gl::detail::context->DrawArraysInstancedBaseInstance(__VA_ARGS__, __FILE__, __func__, __LINE__)
	#undef glDrawElementsInstancedBaseInstance
	#define glDrawElementsInstancedBaseInstance(...) lap::gl::detail::context->DrawElementsInstancedBaseInstance(__VA_ARGS__, __FILE__, __func__, __LINE__)
	#undef glDrawElementsInstancedBaseVertexBaseInstance
	#define glDrawElementsInstancedBaseVertexBaseInstance(...) lap::gl::detail::context->DrawElementsInstancedBaseVertexBaseInstance(__VA_ARGS__, __FILE__, __func__, __LINE__)
	#undef glGetInternalformativ
	#define glGetInternalformativ(...) lap::gl::detail::context->GetInternalformativ(__VA_ARGS__, __FILE__, __func__, __LINE__)
	#undef glGetActiveAtomicCounterBufferiv
	#define glGetActiveAtomicCounterBufferiv(...) lap::gl::detail::context->GetActiveAtomicCounterBufferiv(__VA_ARGS__, __FILE__, __func__, __LINE__)
	#undef glBindImageTexture
	#define glBindImageTexture(...) lap::gl::detail::context->BindImageTexture(__VA_ARGS__, __FILE__, __func__, __LINE__)
	#undef glMemoryBarrier
	#define glMemoryBarrier(...) lap::gl::detail::context->MemoryBarrier(__VA_ARGS__, __FILE__, __func__, __LINE__)
	#undef glTexStorage1D
	#define glTexStorage1D(...) lap::gl::detail::context->TexStorage1D(__VA_ARGS__, __FILE__, __func__, __LINE__)
	#undef glTexStorage2D
	#define glTexStorage2D(...) lap::gl::detail::context->TexStorage2D(__VA_ARGS__, __FILE__, __func__, __LINE__)
	#undef glTexStorage3D
	#define glTexStorage3D(...) lap::gl::detail::context->TexStorage3D(__VA_ARGS__, __FILE__, __func__, __LINE__)
	#undef glDrawTransformFeedbackInstanced
	#define glDrawTransformFeedbackInstanced(...) lap::gl::detail::context->DrawTransformFeedbackInstanced(__VA_ARGS__, __FILE__, __func__, __LINE__)
	#undef glDrawTransformFeedbackStreamInstanced
	#define glDrawTransformFeedbackStreamInstanced(...) lap::gl::detail::context->DrawTransformFeedbackStreamInstanced(__VA_ARGS__, __FILE__, __func__, __LINE__)
	//v43
	#undef glClearBufferData
	#define glClearBufferData(...) lap::gl::detail::context->ClearBufferData(__VA_ARGS__, __FILE__, __func__, __LINE__)
	#undef glClearBufferSubData
	#define glClearBufferSubData(...) lap::gl::detail::context->ClearBufferSubData(__VA_ARGS__, __FILE__, __func__, __LINE__)
	#undef glDispatchCompute
	#define glDispatchCompute(...) lap::gl::detail::context->DispatchCompute(__VA_ARGS__, __FILE__, __func__, __LINE__)
	#undef glDispatchComputeIndirect
	#define glDispatchComputeIndirect(...) lap::gl::detail::context->DispatchComputeIndirect(__VA_ARGS__, __FILE__, __func__, __LINE__)
	#undef glCopyImageSubData
	#define glCopyImageSubData(...) lap::gl::detail::context->CopyImageSubData(__VA_ARGS__, __FILE__, __func__, __LINE__)
	#undef glFramebufferParameteri
	#define glFramebufferParameteri(...) lap::gl::detail::context->FramebufferParameteri(__VA_ARGS__, __FILE__, __func__, __LINE__)
	#undef glGetFramebufferParameteriv
	#define glGetFramebufferParameteriv(...) lap::gl::detail::context->GetFramebufferParameteriv(__VA_ARGS__, __FILE__, __func__, __LINE__)
	#undef glGetInternalformati64v
	#define glGetInternalformati64v(...) lap::gl::detail::context->GetInternalformati64v(__VA_ARGS__, __FILE__, __func__, __LINE__)
	#undef glInvalidateTexSubImage
	#define glInvalidateTexSubImage(...) lap::gl::detail::context->InvalidateTexSubImage(__VA_ARGS__, __FILE__, __func__, __LINE__)
	#undef glInvalidateTexImage
	#define glInvalidateTexImage(...) lap::gl::detail::context->InvalidateTexImage(__VA_ARGS__, __FILE__, __func__, __LINE__)
	#undef glInvalidateBufferSubData
	#define glInvalidateBufferSubData(...) lap::gl::detail::context->InvalidateBufferSubData(__VA_ARGS__, __FILE__, __func__, __LINE__)
	#undef glInvalidateBufferData
	#define glInvalidateBufferData(...) lap::gl::detail::context->InvalidateBufferData(__VA_ARGS__, __FILE__, __func__, __LINE__)
	#undef glInvalidateFramebuffer
	#define glInvalidateFramebuffer(...) lap::gl::detail::context->InvalidateFramebuffer(__VA_ARGS__, __FILE__, __func__, __LINE__)
	#undef glInvalidateSubFramebuffer
	#define glInvalidateSubFramebuffer(...) lap::gl::detail::context->InvalidateSubFramebuffer(__VA_ARGS__, __FILE__, __func__, __LINE__)
	#undef glMultiDrawArraysIndirect
	#define glMultiDrawArraysIndirect(...) lap::gl::detail::context->MultiDrawArraysIndirect(__VA_ARGS__, __FILE__, __func__, __LINE__)
	#undef glMultiDrawElementsIndirect
	#define glMultiDrawElementsIndirect(...) lap::gl::detail::context->MultiDrawElementsIndirect(__VA_ARGS__, __FILE__, __func__, __LINE__)
	#undef glGetProgramInterfaceiv
	#define glGetProgramInterfaceiv(...) lap::gl::detail::context->GetProgramInterfaceiv(__VA_ARGS__, __FILE__, __func__, __LINE__)
	#undef glGetProgramResourceIndex
	#define glGetProgramResourceIndex(...) lap::gl::detail::context->GetProgramResourceIndex(__VA_ARGS__, __FILE__, __func__, __LINE__)
	#undef glGetProgramResourceName
	#define glGetProgramResourceName(...) lap::gl::detail::context->GetProgramResourceName(__VA_ARGS__, __FILE__, __func__, __LINE__)
	#undef glGetProgramResourceiv
	#define glGetProgramResourceiv(...) lap::gl::detail::context->GetProgramResourceiv(__VA_ARGS__, __FILE__, __func__, __LINE__)
	#undef glGetProgramResourceLocation
	#define glGetProgramResourceLocation(...) lap::gl::detail::context->GetProgramResourceLocation(__VA_ARGS__, __FILE__, __func__, __LINE__)
	#undef glGetProgramResourceLocationIndex
	#define glGetProgramResourceLocationIndex(...) lap::gl::detail::context->GetProgramResourceLocationIndex(__VA_ARGS__, __FILE__, __func__, __LINE__)
	#undef glShaderStorageBlockBinding
	#define glShaderStorageBlockBinding(...) lap::gl::detail::context->ShaderStorageBlockBinding(__VA_ARGS__, __FILE__, __func__, __LINE__)
	#undef glTexBufferRange
	#define glTexBufferRange(...) lap::gl::detail::context->TexBufferRange(__VA_ARGS__, __FILE__, __func__, __LINE__)
	#undef glTexStorage2DMultisample
	#define glTexStorage2DMultisample(...) lap::gl::detail::context->TexStorage2DMultisample(__VA_ARGS__, __FILE__, __func__, __LINE__)
	#undef glTexStorage3DMultisample
	#define glTexStorage3DMultisample(...) lap::gl::detail::context->TexStorage3DMultisample(__VA_ARGS__, __FILE__, __func__, __LINE__)
	#undef glTextureView
	#define glTextureView(...) lap::gl::detail::context->TextureView(__VA_ARGS__, __FILE__, __func__, __LINE__)
	#undef glBindVertexBuffer
	#define glBindVertexBuffer(...) lap::gl::detail::context->BindVertexBuffer(__VA_ARGS__, __FILE__, __func__, __LINE__)
	#undef glVertexAttribFormat
	#define glVertexAttribFormat(...) lap::gl::detail::context->VertexAttribFormat(__VA_ARGS__, __FILE__, __func__, __LINE__)
	#undef glVertexAttribIFormat
	#define glVertexAttribIFormat(...) lap::gl::detail::context->VertexAttribIFormat(__VA_ARGS__, __FILE__, __func__, __LINE__)
	#undef glVertexAttribLFormat
	#define glVertexAttribLFormat(...) lap::gl::detail::context->VertexAttribLFormat(__VA_ARGS__, __FILE__, __func__, __LINE__)
	#undef glVertexAttribBinding
	#define glVertexAttribBinding(...) lap::gl::detail::context->VertexAttribBinding(__VA_ARGS__, __FILE__, __func__, __LINE__)
	#undef glVertexBindingDivisor
	#define glVertexBindingDivisor(...) lap::gl::detail::context->VertexBindingDivisor(__VA_ARGS__, __FILE__, __func__, __LINE__)
	#undef glDebugMessageControl
	#define glDebugMessageControl(...) lap::gl::detail::context->DebugMessageControl(__VA_ARGS__, __FILE__, __func__, __LINE__)
	#undef glDebugMessageInsert
	#define glDebugMessageInsert(...) lap::gl::detail::context->DebugMessageInsert(__VA_ARGS__, __FILE__, __func__, __LINE__)
	#undef glDebugMessageCallback
	#define glDebugMessageCallback(...) lap::gl::detail::context->DebugMessageCallback(__VA_ARGS__, __FILE__, __func__, __LINE__)
	#undef glGetDebugMessageLog
	#define glGetDebugMessageLog(...) lap::gl::detail::context->GetDebugMessageLog(__VA_ARGS__, __FILE__, __func__, __LINE__)
	#undef glPushDebugGroup
	#define glPushDebugGroup(...) lap::gl::detail::context->PushDebugGroup(__VA_ARGS__, __FILE__, __func__, __LINE__)
	#undef glPopDebugGroup
	#define glPopDebugGroup() lap::gl::detail::context->PopDebugGroup( __FILE__, __func__, __LINE__)
	#undef glObjectLabel
	#define glObjectLabel(...) lap::gl::detail::context->ObjectLabel(__VA_ARGS__, __FILE__, __func__, __LINE__)
	#undef glGetObjectLabel
	#define glGetObjectLabel(...) lap::gl::detail::context->GetObjectLabel(__VA_ARGS__, __FILE__, __func__, __LINE__)
	#undef glObjectPtrLabel
	#define glObjectPtrLabel(...) lap::gl::detail::context->ObjectPtrLabel(__VA_ARGS__, __FILE__, __func__, __LINE__)
	#undef glGetObjectPtrLabel
	#define glGetObjectPtrLabel(...) lap::gl::detail::context->GetObjectPtrLabel(__VA_ARGS__, __FILE__, __func__, __LINE__)
	#undef glGetPointerv
	#define glGetPointerv(...) lap::gl::detail::context->GetPointerv(__VA_ARGS__, __FILE__, __func__, __LINE__)
	//v44
	#undef glBufferStorage
	#define glBufferStorage(...) lap::gl::detail::context->BufferStorage(__VA_ARGS__, __FILE__, __func__, __LINE__)
	#undef glClearTexImage
	#define glClearTexImage(...) lap::gl::detail::context->ClearTexImage(__VA_ARGS__, __FILE__, __func__, __LINE__)
	#undef glClearTexSubImage
	#define glClearTexSubImage(...) lap::gl::detail::context->ClearTexSubImage(__VA_ARGS__, __FILE__, __func__, __LINE__)
	#undef glBindBuffersBase
	#define glBindBuffersBase(...) lap::gl::detail::context->BindBuffersBase(__VA_ARGS__, __FILE__, __func__, __LINE__)
	#undef glBindBuffersRange
	#define glBindBuffersRange(...) lap::gl::detail::context->BindBuffersRange(__VA_ARGS__, __FILE__, __func__, __LINE__)
	#undef glBindTextures
	#define glBindTextures(...) lap::gl::detail::context->BindTextures(__VA_ARGS__, __FILE__, __func__, __LINE__)
	#undef glBindSamplers
	#define glBindSamplers(...) lap::gl::detail::context->BindSamplers(__VA_ARGS__, __FILE__, __func__, __LINE__)
	#undef glBindImageTextures
	#define glBindImageTextures(...) lap::gl::detail::context->BindImageTextures(__VA_ARGS__, __FILE__, __func__, __LINE__)
	#undef glBindVertexBuffers
	#define glBindVertexBuffers(...) lap::gl::detail::context->BindVertexBuffers(__VA_ARGS__, __FILE__, __func__, __LINE__)
	//v45
	#undef glClipControl
	#define glClipControl(...) lap::gl::detail::context->ClipControl(__VA_ARGS__, __FILE__, __func__, __LINE__)
	#undef glCreateTransformFeedbacks
	#define glCreateTransformFeedbacks(...) lap::gl::detail::context->CreateTransformFeedbacks(__VA_ARGS__, __FILE__, __func__, __LINE__)
	#undef glTransformFeedbackBufferBase
	#define glTransformFeedbackBufferBase(...) lap::gl::detail::context->TransformFeedbackBufferBase(__VA_ARGS__, __FILE__, __func__, __LINE__)
	#undef glTransformFeedbackBufferRange
	#define glTransformFeedbackBufferRange(...) lap::gl::detail::context->TransformFeedbackBufferRange(__VA_ARGS__, __FILE__, __func__, __LINE__)
	#undef glGetTransformFeedbackiv
	#define glGetTransformFeedbackiv(...) lap::gl::detail::context->GetTransformFeedbackiv(__VA_ARGS__, __FILE__, __func__, __LINE__)
	#undef glGetTransformFeedbacki_v
	#define glGetTransformFeedbacki_v(...) lap::gl::detail::context->GetTransformFeedbacki_v(__VA_ARGS__, __FILE__, __func__, __LINE__)
	#undef glGetTransformFeedbacki64_v
	#define glGetTransformFeedbacki64_v(...) lap::gl::detail::context->GetTransformFeedbacki64_v(__VA_ARGS__, __FILE__, __func__, __LINE__)
	#undef glCreateBuffers
	#define glCreateBuffers(...) lap::gl::detail::context->CreateBuffers(__VA_ARGS__, __FILE__, __func__, __LINE__)
	#undef glNamedBufferStorage
	#define glNamedBufferStorage(...) lap::gl::detail::context->NamedBufferStorage(__VA_ARGS__, __FILE__, __func__, __LINE__)
	#undef glNamedBufferData
	#define glNamedBufferData(...) lap::gl::detail::context->NamedBufferData(__VA_ARGS__, __FILE__, __func__, __LINE__)
	#undef glNamedBufferSubData
	#define glNamedBufferSubData(...) lap::gl::detail::context->NamedBufferSubData(__VA_ARGS__, __FILE__, __func__, __LINE__)
	#undef glCopyNamedBufferSubData
	#define glCopyNamedBufferSubData(...) lap::gl::detail::context->CopyNamedBufferSubData(__VA_ARGS__, __FILE__, __func__, __LINE__)
	#undef glClearNamedBufferData
	#define glClearNamedBufferData(...) lap::gl::detail::context->ClearNamedBufferData(__VA_ARGS__, __FILE__, __func__, __LINE__)
	#undef glClearNamedBufferSubData
	#define glClearNamedBufferSubData(...) lap::gl::detail::context->ClearNamedBufferSubData(__VA_ARGS__, __FILE__, __func__, __LINE__)
	#undef glMapNamedBuffer
	#define glMapNamedBuffer(...) lap::gl::detail::context->MapNamedBuffer(__VA_ARGS__, __FILE__, __func__, __LINE__)
	#undef glMapNamedBufferRange
	#define glMapNamedBufferRange(...) lap::gl::detail::context->MapNamedBufferRange(__VA_ARGS__, __FILE__, __func__, __LINE__)
	#undef glUnmapNamedBuffer
	#define glUnmapNamedBuffer(...) lap::gl::detail::context->UnmapNamedBuffer(__VA_ARGS__, __FILE__, __func__, __LINE__)
	#undef glFlushMappedNamedBufferRange
	#define glFlushMappedNamedBufferRange(...) lap::gl::detail::context->FlushMappedNamedBufferRange(__VA_ARGS__, __FILE__, __func__, __LINE__)
	#undef glGetNamedBufferParameteriv
	#define glGetNamedBufferParameteriv(...) lap::gl::detail::context->GetNamedBufferParameteriv(__VA_ARGS__, __FILE__, __func__, __LINE__)
	#undef glGetNamedBufferParameteri64v
	#define glGetNamedBufferParameteri64v(...) lap::gl::detail::context->GetNamedBufferParameteri64v(__VA_ARGS__, __FILE__, __func__, __LINE__)
	#undef glGetNamedBufferPointerv
	#define glGetNamedBufferPointerv(...) lap::gl::detail::context->GetNamedBufferPointerv(__VA_ARGS__, __FILE__, __func__, __LINE__)
	#undef glGetNamedBufferSubData
	#define glGetNamedBufferSubData(...) lap::gl::detail::context->GetNamedBufferSubData(__VA_ARGS__, __FILE__, __func__, __LINE__)
	#undef glCreateFramebuffers
	#define glCreateFramebuffers(...) lap::gl::detail::context->CreateFramebuffers(__VA_ARGS__, __FILE__, __func__, __LINE__)
	#undef glNamedFramebufferRenderbuffer
	#define glNamedFramebufferRenderbuffer(...) lap::gl::detail::context->NamedFramebufferRenderbuffer(__VA_ARGS__, __FILE__, __func__, __LINE__)
	#undef glNamedFramebufferParameteri
	#define glNamedFramebufferParameteri(...) lap::gl::detail::context->NamedFramebufferParameteri(__VA_ARGS__, __FILE__, __func__, __LINE__)
	#undef glNamedFramebufferTexture
	#define glNamedFramebufferTexture(...) lap::gl::detail::context->NamedFramebufferTexture(__VA_ARGS__, __FILE__, __func__, __LINE__)
	#undef glNamedFramebufferTextureLayer
	#define glNamedFramebufferTextureLayer(...) lap::gl::detail::context->NamedFramebufferTextureLayer(__VA_ARGS__, __FILE__, __func__, __LINE__)
	#undef glNamedFramebufferDrawBuffer
	#define glNamedFramebufferDrawBuffer(...) lap::gl::detail::context->NamedFramebufferDrawBuffer(__VA_ARGS__, __FILE__, __func__, __LINE__)
	#undef glNamedFramebufferDrawBuffers
	#define glNamedFramebufferDrawBuffers(...) lap::gl::detail::context->NamedFramebufferDrawBuffers(__VA_ARGS__, __FILE__, __func__, __LINE__)
	#undef glNamedFramebufferReadBuffer
	#define glNamedFramebufferReadBuffer(...) lap::gl::detail::context->NamedFramebufferReadBuffer(__VA_ARGS__, __FILE__, __func__, __LINE__)
	#undef glInvalidateNamedFramebufferData
	#define glInvalidateNamedFramebufferData(...) lap::gl::detail::context->InvalidateNamedFramebufferData(__VA_ARGS__, __FILE__, __func__, __LINE__)
	#undef glInvalidateNamedFramebufferSubData
	#define glInvalidateNamedFramebufferSubData(...) lap::gl::detail::context->InvalidateNamedFramebufferSubData(__VA_ARGS__, __FILE__, __func__, __LINE__)
	#undef glClearNamedFramebufferiv
	#define glClearNamedFramebufferiv(...) lap::gl::detail::context->ClearNamedFramebufferiv(__VA_ARGS__, __FILE__, __func__, __LINE__)
	#undef glClearNamedFramebufferuiv
	#define glClearNamedFramebufferuiv(...) lap::gl::detail::context->ClearNamedFramebufferuiv(__VA_ARGS__, __FILE__, __func__, __LINE__)
	#undef glClearNamedFramebufferfv
	#define glClearNamedFramebufferfv(...) lap::gl::detail::context->ClearNamedFramebufferfv(__VA_ARGS__, __FILE__, __func__, __LINE__)
	#undef glClearNamedFramebufferfi
	#define glClearNamedFramebufferfi(...) lap::gl::detail::context->ClearNamedFramebufferfi(__VA_ARGS__, __FILE__, __func__, __LINE__)
	#undef glBlitNamedFramebuffer
	#define glBlitNamedFramebuffer(...) lap::gl::detail::context->BlitNamedFramebuffer(__VA_ARGS__, __FILE__, __func__, __LINE__)
	#undef glCheckNamedFramebufferStatus
	#define glCheckNamedFramebufferStatus(...) lap::gl::detail::context->CheckNamedFramebufferStatus(__VA_ARGS__, __FILE__, __func__, __LINE__)
	#undef glGetNamedFramebufferParameteriv
	#define glGetNamedFramebufferParameteriv(...) lap::gl::detail::context->GetNamedFramebufferParameteriv(__VA_ARGS__, __FILE__, __func__, __LINE__)
	#undef glGetNamedFramebufferAttachmentParameteriv
	#define glGetNamedFramebufferAttachmentParameteriv(...) lap::gl::detail::context->GetNamedFramebufferAttachmentParameteriv(__VA_ARGS__, __FILE__, __func__, __LINE__)
	#undef glCreateRenderbuffers
	#define glCreateRenderbuffers(...) lap::gl::detail::context->CreateRenderbuffers(__VA_ARGS__, __FILE__, __func__, __LINE__)
	#undef glNamedRenderbufferStorage
	#define glNamedRenderbufferStorage(...) lap::gl::detail::context->NamedRenderbufferStorage(__VA_ARGS__, __FILE__, __func__, __LINE__)
	#undef glNamedRenderbufferStorageMultisample
	#define glNamedRenderbufferStorageMultisample(...) lap::gl::detail::context->NamedRenderbufferStorageMultisample(__VA_ARGS__, __FILE__, __func__, __LINE__)
	#undef glGetNamedRenderbufferParameteriv
	#define glGetNamedRenderbufferParameteriv(...) lap::gl::detail::context->GetNamedRenderbufferParameteriv(__VA_ARGS__, __FILE__, __func__, __LINE__)
	#undef glCreateTextures
	#define glCreateTextures(...) lap::gl::detail::context->CreateTextures(__VA_ARGS__, __FILE__, __func__, __LINE__)
	#undef glTextureBuffer
	#define glTextureBuffer(...) lap::gl::detail::context->TextureBuffer(__VA_ARGS__, __FILE__, __func__, __LINE__)
	#undef glTextureBufferRange
	#define glTextureBufferRange(...) lap::gl::detail::context->TextureBufferRange(__VA_ARGS__, __FILE__, __func__, __LINE__)
	#undef glTextureStorage1D
	#define glTextureStorage1D(...) lap::gl::detail::context->TextureStorage1D(__VA_ARGS__, __FILE__, __func__, __LINE__)
	#undef glTextureStorage2D
	#define glTextureStorage2D(...) lap::gl::detail::context->TextureStorage2D(__VA_ARGS__, __FILE__, __func__, __LINE__)
	#undef glTextureStorage3D
	#define glTextureStorage3D(...) lap::gl::detail::context->TextureStorage3D(__VA_ARGS__, __FILE__, __func__, __LINE__)
	#undef glTextureStorage2DMultisample
	#define glTextureStorage2DMultisample(...) lap::gl::detail::context->TextureStorage2DMultisample(__VA_ARGS__, __FILE__, __func__, __LINE__)
	#undef glTextureStorage3DMultisample
	#define glTextureStorage3DMultisample(...) lap::gl::detail::context->TextureStorage3DMultisample(__VA_ARGS__, __FILE__, __func__, __LINE__)
	#undef glTextureSubImage1D
	#define glTextureSubImage1D(...) lap::gl::detail::context->TextureSubImage1D(__VA_ARGS__, __FILE__, __func__, __LINE__)
	#undef glTextureSubImage2D
	#define glTextureSubImage2D(...) lap::gl::detail::context->TextureSubImage2D(__VA_ARGS__, __FILE__, __func__, __LINE__)
	#undef glTextureSubImage3D
	#define glTextureSubImage3D(...) lap::gl::detail::context->TextureSubImage3D(__VA_ARGS__, __FILE__, __func__, __LINE__)
	#undef glCompressedTextureSubImage1D
	#define glCompressedTextureSubImage1D(...) lap::gl::detail::context->CompressedTextureSubImage1D(__VA_ARGS__, __FILE__, __func__, __LINE__)
	#undef glCompressedTextureSubImage2D
	#define glCompressedTextureSubImage2D(...) lap::gl::detail::context->CompressedTextureSubImage2D(__VA_ARGS__, __FILE__, __func__, __LINE__)
	#undef glCompressedTextureSubImage3D
	#define glCompressedTextureSubImage3D(...) lap::gl::detail::context->CompressedTextureSubImage3D(__VA_ARGS__, __FILE__, __func__, __LINE__)
	#undef glCopyTextureSubImage1D
	#define glCopyTextureSubImage1D(...) lap::gl::detail::context->CopyTextureSubImage1D(__VA_ARGS__, __FILE__, __func__, __LINE__)
	#undef glCopyTextureSubImage2D
	#define glCopyTextureSubImage2D(...) lap::gl::detail::context->CopyTextureSubImage2D(__VA_ARGS__, __FILE__, __func__, __LINE__)
	#undef glCopyTextureSubImage3D
	#define glCopyTextureSubImage3D(...) lap::gl::detail::context->CopyTextureSubImage3D(__VA_ARGS__, __FILE__, __func__, __LINE__)
	#undef glTextureParameterf
	#define glTextureParameterf(...) lap::gl::detail::context->TextureParameterf(__VA_ARGS__, __FILE__, __func__, __LINE__)
	#undef glTextureParameterfv
	#define glTextureParameterfv(...) lap::gl::detail::context->TextureParameterfv(__VA_ARGS__, __FILE__, __func__, __LINE__)
	#undef glTextureParameteri
	#define glTextureParameteri(...) lap::gl::detail::context->TextureParameteri(__VA_ARGS__, __FILE__, __func__, __LINE__)
	#undef glTextureParameterIiv
	#define glTextureParameterIiv(...) lap::gl::detail::context->TextureParameterIiv(__VA_ARGS__, __FILE__, __func__, __LINE__)
	#undef glTextureParameterIuiv
	#define glTextureParameterIuiv(...) lap::gl::detail::context->TextureParameterIuiv(__VA_ARGS__, __FILE__, __func__, __LINE__)
	#undef glTextureParameteriv
	#define glTextureParameteriv(...) lap::gl::detail::context->TextureParameteriv(__VA_ARGS__, __FILE__, __func__, __LINE__)
	#undef glGenerateTextureMipmap
	#define glGenerateTextureMipmap(...) lap::gl::detail::context->GenerateTextureMipmap(__VA_ARGS__, __FILE__, __func__, __LINE__)
	#undef glBindTextureUnit
	#define glBindTextureUnit(...) lap::gl::detail::context->BindTextureUnit(__VA_ARGS__, __FILE__, __func__, __LINE__)
	#undef glGetTextureImage
	#define glGetTextureImage(...) lap::gl::detail::context->GetTextureImage(__VA_ARGS__, __FILE__, __func__, __LINE__)
	#undef glGetCompressedTextureImage
	#define glGetCompressedTextureImage(...) lap::gl::detail::context->GetCompressedTextureImage(__VA_ARGS__, __FILE__, __func__, __LINE__)
	#undef glGetTextureLevelParameterfv
	#define glGetTextureLevelParameterfv(...) lap::gl::detail::context->GetTextureLevelParameterfv(__VA_ARGS__, __FILE__, __func__, __LINE__)
	#undef glGetTextureLevelParameteriv
	#define glGetTextureLevelParameteriv(...) lap::gl::detail::context->GetTextureLevelParameteriv(__VA_ARGS__, __FILE__, __func__, __LINE__)
	#undef glGetTextureParameterfv
	#define glGetTextureParameterfv(...) lap::gl::detail::context->GetTextureParameterfv(__VA_ARGS__, __FILE__, __func__, __LINE__)
	#undef glGetTextureParameterIiv
	#define glGetTextureParameterIiv(...) lap::gl::detail::context->GetTextureParameterIiv(__VA_ARGS__, __FILE__, __func__, __LINE__)
	#undef glGetTextureParameterIuiv
	#define glGetTextureParameterIuiv(...) lap::gl::detail::context->GetTextureParameterIuiv(__VA_ARGS__, __FILE__, __func__, __LINE__)
	#undef glGetTextureParameteriv
	#define glGetTextureParameteriv(...) lap::gl::detail::context->GetTextureParameteriv(__VA_ARGS__, __FILE__, __func__, __LINE__)
	#undef glCreateVertexArrays
	#define glCreateVertexArrays(...) lap::gl::detail::context->CreateVertexArrays(__VA_ARGS__, __FILE__, __func__, __LINE__)
	#undef glDisableVertexArrayAttrib
	#define glDisableVertexArrayAttrib(...) lap::gl::detail::context->DisableVertexArrayAttrib(__VA_ARGS__, __FILE__, __func__, __LINE__)
	#undef glEnableVertexArrayAttrib
	#define glEnableVertexArrayAttrib(...) lap::gl::detail::context->EnableVertexArrayAttrib(__VA_ARGS__, __FILE__, __func__, __LINE__)
	#undef glVertexArrayElementBuffer
	#define glVertexArrayElementBuffer(...) lap::gl::detail::context->VertexArrayElementBuffer(__VA_ARGS__, __FILE__, __func__, __LINE__)
	#undef glVertexArrayVertexBuffer
	#define glVertexArrayVertexBuffer(...) lap::gl::detail::context->VertexArrayVertexBuffer(__VA_ARGS__, __FILE__, __func__, __LINE__)
	#undef glVertexArrayVertexBuffers
	#define glVertexArrayVertexBuffers(...) lap::gl::detail::context->VertexArrayVertexBuffers(__VA_ARGS__, __FILE__, __func__, __LINE__)
	#undef glVertexArrayAttribBinding
	#define glVertexArrayAttribBinding(...) lap::gl::detail::context->VertexArrayAttribBinding(__VA_ARGS__, __FILE__, __func__, __LINE__)
	#undef glVertexArrayAttribFormat
	#define glVertexArrayAttribFormat(...) lap::gl::detail::context->VertexArrayAttribFormat(__VA_ARGS__, __FILE__, __func__, __LINE__)
	#undef glVertexArrayAttribIFormat
	#define glVertexArrayAttribIFormat(...) lap::gl::detail::context->VertexArrayAttribIFormat(__VA_ARGS__, __FILE__, __func__, __LINE__)
	#undef glVertexArrayAttribLFormat
	#define glVertexArrayAttribLFormat(...) lap::gl::detail::context->VertexArrayAttribLFormat(__VA_ARGS__, __FILE__, __func__, __LINE__)
	#undef glVertexArrayBindingDivisor
	#define glVertexArrayBindingDivisor(...) lap::gl::detail::context->VertexArrayBindingDivisor(__VA_ARGS__, __FILE__, __func__, __LINE__)
	#undef glGetVertexArrayiv
	#define glGetVertexArrayiv(...) lap::gl::detail::context->GetVertexArrayiv(__VA_ARGS__, __FILE__, __func__, __LINE__)
	#undef glGetVertexArrayIndexediv
	#define glGetVertexArrayIndexediv(...) lap::gl::detail::context->GetVertexArrayIndexediv(__VA_ARGS__, __FILE__, __func__, __LINE__)
	#undef glGetVertexArrayIndexed64iv
	#define glGetVertexArrayIndexed64iv(...) lap::gl::detail::context->GetVertexArrayIndexed64iv(__VA_ARGS__, __FILE__, __func__, __LINE__)
	#undef glCreateSamplers
	#define glCreateSamplers(...) lap::gl::detail::context->CreateSamplers(__VA_ARGS__, __FILE__, __func__, __LINE__)
	#undef glCreateProgramPipelines
	#define glCreateProgramPipelines(...) lap::gl::detail::context->CreateProgramPipelines(__VA_ARGS__, __FILE__, __func__, __LINE__)
	#undef glCreateQueries
	#define glCreateQueries(...) lap::gl::detail::context->CreateQueries(__VA_ARGS__, __FILE__, __func__, __LINE__)
	#undef glGetQueryBufferObjecti64v
	#define glGetQueryBufferObjecti64v(...) lap::gl::detail::context->GetQueryBufferObjecti64v(__VA_ARGS__, __FILE__, __func__, __LINE__)
	#undef glGetQueryBufferObjectiv
	#define glGetQueryBufferObjectiv(...) lap::gl::detail::context->GetQueryBufferObjectiv(__VA_ARGS__, __FILE__, __func__, __LINE__)
	#undef glGetQueryBufferObjectui64v
	#define glGetQueryBufferObjectui64v(...) lap::gl::detail::context->GetQueryBufferObjectui64v(__VA_ARGS__, __FILE__, __func__, __LINE__)
	#undef glGetQueryBufferObjectuiv
	#define glGetQueryBufferObjectuiv(...) lap::gl::detail::context->GetQueryBufferObjectuiv(__VA_ARGS__, __FILE__, __func__, __LINE__)
	#undef glMemoryBarrierByRegion
	#define glMemoryBarrierByRegion(...) lap::gl::detail::context->MemoryBarrierByRegion(__VA_ARGS__, __FILE__, __func__, __LINE__)
	#undef glGetTextureSubImage
	#define glGetTextureSubImage(...) lap::gl::detail::context->GetTextureSubImage(__VA_ARGS__, __FILE__, __func__, __LINE__)
	#undef glGetCompressedTextureSubImage
	#define glGetCompressedTextureSubImage(...) lap::gl::detail::context->GetCompressedTextureSubImage(__VA_ARGS__, __FILE__, __func__, __LINE__)
	#undef glGetGraphicsResetStatus
	#define glGetGraphicsResetStatus() lap::gl::detail::context->GetGraphicsResetStatus( __FILE__, __func__, __LINE__)
	#undef glGetnCompressedTexImage
	#define glGetnCompressedTexImage(...) lap::gl::detail::context->GetnCompressedTexImage(__VA_ARGS__, __FILE__, __func__, __LINE__)
	#undef glGetnTexImage
	#define glGetnTexImage(...) lap::gl::detail::context->GetnTexImage(__VA_ARGS__, __FILE__, __func__, __LINE__)
	#undef glGetnUniformdv
	#define glGetnUniformdv(...) lap::gl::detail::context->GetnUniformdv(__VA_ARGS__, __FILE__, __func__, __LINE__)
	#undef glGetnUniformfv
	#define glGetnUniformfv(...) lap::gl::detail::context->GetnUniformfv(__VA_ARGS__, __FILE__, __func__, __LINE__)
	#undef glGetnUniformiv
	#define glGetnUniformiv(...) lap::gl::detail::context->GetnUniformiv(__VA_ARGS__, __FILE__, __func__, __LINE__)
	#undef glGetnUniformuiv
	#define glGetnUniformuiv(...) lap::gl::detail::context->GetnUniformuiv(__VA_ARGS__, __FILE__, __func__, __LINE__)
	#undef glReadnPixels
	#define glReadnPixels(...) lap::gl::detail::context->ReadnPixels(__VA_ARGS__, __FILE__, __func__, __LINE__)
	#undef glTextureBarrier
	#define glTextureBarrier() lap::gl::detail::context->TextureBarrier( __FILE__, __func__, __LINE__)
#endif


#endif