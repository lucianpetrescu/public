# README #

DebugContext is a debugging tool for OpenGL based programs. It is written for the OpenGL Core 4.5 profile but it also includes 
previous compatibility functions. This tool is useful for tracking the OpenGL state (which buffers are created, which sampler is
bound to which unit, which shaders are attached to which program, etc), OpenGL errors and OpenGL stats (memory usage, number of
commands, state changes, etc) and logging them into a user provided stream.

While lacking the functionality of established software such as glDebugger, glIntercept and so on, LAP GL DebugContext is friendlier
towards programmers, by giving programmable access into the application (callbacks on errors, stats for code sections, etc), minimizing
scope creep. This feature is very valuable when debugging large applications where only a tiny portion of the code is of interest. 
The OpenGL functions are not intercepted through hooking but through define redefinition (e.g. #undef glClear, #define glClear .... )
LAP GL DebugContext is compatible with both macro-based and function-based OpenGL function loaders, and should be included just after
the OpenGL function loader. 

version: 0.38

author: Lucian Petrescu( lucian (dot) petrescu (dot) 24 (at) gmail (dot) com )


### SCREENSHOTS ###

Functions are bundled into groups

 ![functions are bundled into groups](img/groups.png) 

Leak checking

 ![leak checking](img/leaks.png) 

Logging example, warnings and errors are issued when they occur, on the logging stream and on the debug callback

 ![logging example, warnings and errors are issued when they occur](img/logging.png) 

A wide variety of statistics can be queried

 ![a wide variety of statistics can be queried](img/stats.png) 


###What is this software useful for###

- debugging OpenGL programs
- optimization of OpenGL programs
- teaching OpenGL in depth through state inspection
- monitoring OpenGL resource usage
- logging OpenGL commands

###Properties###

- c++11, no exceptions
- overrides normal OpenGL calls (a DebugContext object has to be bound and LAP_GL_CONTEXT_OVERRIDE_FUNCTIONS macro has to be defined)
- written for 4.5 core (thus nu extra core extensions such as sparse buffers/textures)
- intentionally not thread safe (like OpenGL)
- does not overwrite the OpenGL function loader, just plug and play
- easy debugging and state inspection, full core state except default texture objects (which should never be used..)
- offers various parameters which control what work is performed and which state is tracked
	- safety\_checks	use extensive safety checks? (will call glGetError after each GL call and will call the debug callback on GL errors)
	- keep\_state		maintain an internal state machine (almost complete, some things (glsl compiler related) can only be queried through GL)
	- compute\_stats		compute statistics, many memory stats are computed only if keep\_state = true, redundancy stats are computed only if redundancy\_checks = true
	- redundancy\_checks check redundancies? (will call the debug callback on warnings), if true it automatically sets keep\_state=true
	- leak\_checks		check for leaks on context destruction (will call the debug callback on leaks), if true it automatically sets keep\_state=true
	- logger			log all OpenGL commands to this stream
- if no options are selected then DebugContext is just a minimal wrapper of the OpenGL functions.
- provides command groups (e.g. draw commands, buffer commands, etc)
	- logging per command group
- provides a debug callback even if debug is not supported
- stats
	- state switches
	- draw calls
	- query commands
	- memory usage and stats
	- synchronization commands
	- gpu-cpu and gpu-gpu transfers
	- etc
- inside context the spec-style naming is used (e.g. CullFace, Hint, etc)
- cleans up defines, can exclude compatibility functions and defines
- file / func / line on each OpenGL call (not available on direct class calls)
- descriptors are created on generation and creation (the name is reserved, the descriptor is contains no other data or size)
- indexed operations intentionally crash if out of bounds. No checks besides internal access checks. OpenGL errors 
  are preferrable to cpp asserts, so DebugContext asserts only where it is necessary (object found/not found)
- cpu data transfers assume that the gpu will make a local copy (thus multichannel images loaded from the cpu are loaded entirely,
	not per-channel). Since many drivers do this, a convservative approximation should be a better choice.
- large parts of interface and implementation are script generated, but logic and advanced stats are manually implemented

###Limitations###

- As there is no OpenGL api for context switching (the api is platform based and EGL is still very recent and not widely supported), 
	multiple contexts can be supported through manual context switching and assignment, see 
	- void setDebugContext(DebugContext* context)
	- DebugContext* getDebugContext();	
	Basically the user creates a DebugContext object for each context and sets the current DebugContext object when the OpenGL context is changed.
	This behavior is compliant to OpenGL principles, where only one context per thread can be active. 
 - The DebugContext object has zero thread safety, as the OpenGL driver is singlethreaded and will synchronize commands from concurrent contexts 
	on different threads. Therefore, manual synchronization from the user is required as it is with OpenGL.
- State is kept only for the core functions, therefore there are no state/stats/redundancy checks/etc for deprecated functions.
- a very small number of default objects (the default textures) are not maintained. It has to be noted that such objects should never be used
	in production code and that they are remnants of deprecated OpenGL.

###The Anatomy of a Call###

		Type0 DebugContext::FunctionName(Type1 arg1, Type2 arg2, Type3 arg3, const char* file, const char* func, unsigned int line) {
			Type0 result = glFunctionName(arg1, arg2, arg3);
			if (no options) return result;	// quick return if no options are provided 
			if (logging || safety checks || redundancy checks) {
				create result string
				create arguments string
			}
			if (logging ) do logging
			if (safety checks) do safety checks
			if (stats) compute non-memory related stats (e.g. num commands)
			if (state){
				maintain state
				if(stats) compute memory and state based stats (e.g. gpu-cpu transfer size)
			}
			return result;
		}
		#undef glFunctionName
		#define glFunctionName (...) lap::gl::detail::context->FunctionName(__VA_ARGS__, __FILE__, __func__, __LINE__)

###Compiling and linking###

Visual studio 2015 project&solution files can be found in the **vstudio** folder. Makefiles for gnu/gcc ( **makefile** ) and for visual studio ( **makefilevs.bat** ) are provided in the root folder. All methods will generate objs, pdbs, exes, etc in **bin**.

- Windows:
	- no special setup for visual studio projects
	- -pthread -lgdi32 otherwise
	- an example visual studio project+solution is provide in the vstudio folder
	- the makefilevs.bat from the root folder can also be used
- Linux:
	- -ldl -lGL -lX11 -lXxf86vm -lXrandr -lXi -lXinerama -lXcursor -lrt -lm
	- the wayland and mir backends are not used by lapwic (but they can easily be added in the future when they mature, right now they are experimental in glfw)
	- an example makefile is provided in the root folder
	- to install the project dependencies get:
		- sudo apt-get install xorg-dev
		- sudo apt-get install libgl-dev
	- also compiles in a VMWARE virtual machine, but needs a special mesa driver http://mesa3d.org/vmware-guest.html. It will crash as the mesa driver does not support OpenGL 4.5.
- OSX
	- -framework Cocoa -framework OpenGL -framework IOKit -framework CoreVideo -framework CoreFoundation -framework Carbon
	- an example makefile is provided in the root folder\n
	

###Usage###

- basic usage

		... include function loader header ...
		#define LAP_GL_CONTEXT_OVERRIDE_FUNCTIONS
		#include "lap_gl_debug_context.hpp"
		...
		... create OpenGL context...
		...
		lap::gl::DebugContext dc(true, true, true, true, true, &std::cout, true, true, true);	// queries the native OpenGL context and creates a Context object with identical properties and blank state
		dc.setCurrent(true);															// set the newly created context as the current context
		auto callback = [](const std::string& function, const std::string& result, const std::string& parameters, GLenum errorcode, const char* file, const char* func, unsigned int line) {
			std::cout << "           **" << ((errorcode == GL_NONE) ? "ERROR" : "WARNING") << " from function " << function << "(" << parameters << ")";
			if (result.size() > 0) std::cout << " = " << result << " ";
			std::cout << " errorcode = " << errorcode << std::endl;
			if (file) std::cout << "           **call came from FILE=" << file;
			if (func) std::cout << " FUNC=" << func;
			if (line) std::cout << " LINE=" << line;
			std::cout << std::endl;
		};
		dc.setCallbackDebug(callback);	///< will get debug messages on this callback (anything capturable by a std::function can be placed here)
		//usage example #1 : call OpenGL functions directly through the Context object --BUT filename, function name and line will be BLANK--
		dc.BlendEquation(GL_FUNC_REVERSE_SUBTRACT);			//goes through the DebugContext
		//usage example #2 : call global OpenGL functions which are overrided with #define LAP_GL_DEBUG_CONTEXT_OVERRIDE_FUNCTIONS . Doesn't work without this define.
		glBlendEquation(GL_FUNC_ADD);						//goes through the DebugContext


- function groups

		... include function loader header ...
		#define LAP_GL_CONTEXT_OVERRIDE_FUNCTIONS
		#include "lap_gl_debug_context.hpp"
		...
		... create OpenGL context...
		...
		lap::gl::DebugContext dc(true, true, true, true, true, &std::cout, true, true, true);	// queries the native OpenGL context and creates a Context object with identical properties and blank state
		dc.setCurrent(true);															// set the newly created context as the current context
		auto names = dc.getFunctionGroupNames(lap::gl::DebugContext::GROUP::V30);
		std::cout << "These functions were introduced by OpenGL v30:" << std::endl;
		for (const auto& s : names) std::cout << s << std::endl;
		#ifdef LAP_GL_DEBUG_CONTEXT_COMPATIBILITY
		names = dc.getFunctionGroupNames(lap::gl::DebugContext::GROUP::V14_DEPRECATED);
		std::cout << "These functions were introduced by OpenGL v14, but are now deprecated:" << std::endl;
		for (const auto& s : names) std::cout << s << std::endl;
		#endif


- logging

		... include function loader header ...
		#define LAP_GL_CONTEXT_OVERRIDE_FUNCTIONS
		#include "lap_gl_debug_context.hpp"
		...
		... create OpenGL context...
		...
		lap::gl::DebugContext dc(true, true, true, true, true, &std::cout, true, true, true);	// queries the native OpenGL context and creates a Context object with identical properties and blank state
		dc.setCurrent(true);															// set the newly created context as the current context
		glBlendEquationSeparate(GL_FUNC_ADD, GL_FUNC_SUBTRACT);		//not logged (no ostream!)
		dc.setLogger(&std::cout);
		glBlendEquationSeparatei(0, GL_FUNC_ADD, GL_FUNC_SUBTRACT);	//logged (by default all groups are logged if a log ostream is provided)
		dc.setLogGroup(DebugContext::GROUP::BLEND, false);			//disable logging of blend functions
		glBlendColor(0.5f, 0.5f, 1.0f, 0.0f);						//not logged (disabled)
		dc.setLogGroup(DebugContext::GROUP::BLEND, true);			//re-enable logging of blend functions
		glBlendColor(0.9f, 0.9f, 0.9f, 0.9f);						//logged (enabled)
		dc.setLogger(&std::cout, false, false, false);				//disable __FILE__, __LINE__, __func__
		glBlendFunc(GL_ZERO, GL_ONE);								//logged
		glBlendFunci(3, GL_ONE_MINUS_SRC_ALPHA, GL_DST_ALPHA);		//logged
		glBlendFuncSeparate(GL_ZERO, GL_ONE_MINUS_DST_ALPHA, GL_ONE, GL_ONE_MINUS_SRC_COLOR);	//logged
		glBlendFuncSeparatei(0, GL_ZERO, GL_ONE_MINUS_DST_ALPHA, GL_ONE, GL_ONE_MINUS_SRC_COLOR);//logged
		@endcode

- stats and state

		... include function loader header ...
		#define LAP_GL_CONTEXT_OVERRIDE_FUNCTIONS
		#include "lap_gl_debug_context.hpp"
		...
		... create OpenGL context...
		...
		lap::gl::DebugContext dc(true, true, true, true, true, &std::cout, true, true, true);	// queries the native OpenGL context and creates a Context object with identical properties and blank state
		dc.setCurrent(true);															// set the newly created context as the current context
		...
		... do some OpenGL work ...
		...
		auto& state = dc.getState()
		auto& stats = dc.getStats()
		std::cout << stats.commands.all << " OpenGL commands, of which:" << std::endl;
		std::cout << stats.commands.attribute << " attribute commands" << std::endl;
		std::cout << stats.commands.blend << " blend commands" << std::endl;
		std::cout << stats.commands.buffer << " buffer commands" << std::endl;
		...

###TODO LIST###

more testing on other platforms and with different extension loaders

### DOCUMENTATION ###

a Doxyfile is provided in **doc**, which can be used with Doxygen to generate  the entire documentation in HTML format. 
Note: doxygen has problems with arguments of std::function type function arguments (used for callbacks) and will issue a large number of warnings.


### COMPATIBILITY ###

tested with visual studio 2015, MinGW x64 4.0, Ubuntu 15 + gcc 4.8.4, MacOS El Capitan + clang 3.7.1.



