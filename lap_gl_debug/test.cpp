///---------------------------------------------------------------------------------------------------------------------
/// The MIT License (MIT)
/// 
/// Copyright (c) 2016 Lucian Petrescu, lucian (dot) petrescu (dot) 24 (at) gmail (dot) com
/// 
/// Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation 
/// files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, 
/// modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software 
/// is furnished to do so, subject to the following conditions:
/// 
/// The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
/// 
/// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
/// OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE 
/// LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR 
/// IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
///---------------------------------------------------------------------------------------------------------------------


/// this define is NEEDED to redirect all OpenGL functions through the currently bound DebugContext. it will only interact with OpenGL functions.
#define LAP_GL_CONTEXT_OVERRIDE_FUNCTIONS

#include "lap_gl_debug_context.hpp"
#include <iostream>
#include <cassert>
#include <tuple>
#include <algorithm>
#include <cstring>

//-------------------------------------------------------------------------------------------------
void testBasic() {
	std::cout << "**************************************************************" << std::endl << "Test Basic" << std::endl;
	//window (need this for context)
	lap::wic::WICSystem wicsystem(nullptr, false);
	lap::wic::WindowProperties wp; lap::wic::FramebufferProperties fp; lap::wic::ContextProperties cp; lap::wic::InputProperties ip;
	wp.visible = false;	cp.version_major = 4; cp.version_minor = 5; cp.debug_context = true; cp.profile_core = true;
	cp.robustness = lap::wic::ContextProperties::RobustnessType::LOSE_CONTEXT_ON_RESET;
	lap::wic::Window window = lap::wic::Window(wp, fp, cp, ip);

	// create a debug context (with various options)
	// also, see line 21 in this file for #define LAP_GL_CONTEXT_OVERRIDE_FUNCTIONS
	using namespace lap::gl;
	DebugContext dc(true, true, true, true, true, &std::cout, true, true, true);	// queries the native OpenGL context and creates a Context object with identical properties
	dc.setCurrent(true);															// set the newly created context as the current context

	auto callback = [](const std::string& function, const std::string& result, const std::string& parameters, GLenum errorcode, const char* file, const char* func, unsigned int line) {
		std::cout << "           **" << ((errorcode == GL_NONE) ? "ERROR" : "WARNING") << " from function " << function << "(" << parameters << ")";
		if (result.size() > 0) std::cout << " = " << result << " ";
		std::cout <<" errorcode = " << errorcode << std::endl;
		if (file) std::cout << "           **call came from FILE=" << file;
		if (func) std::cout << " FUNC=" << func;
		if (line) std::cout << " LINE=" << line;
		std::cout << std::endl;
	};
	dc.setCallbackDebug(callback);	///< will get debug messages on this callback (anything capturable by a std::function can be placed here)
	
	//usage example #1 : call OpenGL functions directly through the Context object --BUT filename, function name and line will be WRONG--
	dc.BlendEquation(GL_FUNC_REVERSE_SUBTRACT);

	//usage example #2 : call global OpenGL functions which are overrided with #define LAP_GL_DEBUG_CONTEXT_OVERRIDE_FUNCTIONS . Doesn't work without this define.
	glBlendEquation(GL_FUNC_ADD);
}
//-------------------------------------------------------------------------------------------------
void testGroupNames() {
	std::cout << "**************************************************************" << std::endl << "Test Group Names" << std::endl;
	//window (need this for context)
	lap::wic::WICSystem wicsystem(nullptr, false);
	lap::wic::WindowProperties wp; lap::wic::FramebufferProperties fp; lap::wic::ContextProperties cp; lap::wic::InputProperties ip;
	wp.visible = false;	cp.version_major = 4; cp.version_minor = 5; cp.debug_context = true; cp.profile_core = true;
	cp.robustness = lap::wic::ContextProperties::RobustnessType::LOSE_CONTEXT_ON_RESET;
	lap::wic::Window window = lap::wic::Window(wp, fp, cp, ip);

	// create a debug context (with various options)
	// also, see line 21 in this file for #define LAP_GL_CONTEXT_OVERRIDE_FUNCTIONS
	using namespace lap::gl;
	DebugContext dc(true, true, true, true, true, &std::cout);	// queries the native OpenGL context and creates a Context object with identical properties
	dc.setCurrent(true);										// set the newly created context as the current context

	auto test_func = [&](const std::string& group_name, DebugContext::GROUP group) {
		std::vector<std::string> names;
		names = dc.getFunctionGroupNames(group);
		std::cout << "==============================================" << std::endl << group_name << " contains the following functions" << std::endl << "----------------" << std::endl;
		for (const auto& s : names) std::cout << s << std::endl;

		std::sort(names.begin(), names.end());
		for (unsigned int i = 1; i < names.size(); i++) assert(names[i - 1] != names[i]);
		
	};

	test_func("ALL", DebugContext::GROUP::ALL);
	test_func("V10 CORE", DebugContext::GROUP::V10);
	test_func("V11 CORE", DebugContext::GROUP::V11);
	test_func("V12 CORE", DebugContext::GROUP::V12);
	test_func("V13 CORE", DebugContext::GROUP::V13);
	test_func("V14 CORE", DebugContext::GROUP::V14);
	test_func("V15 CORE", DebugContext::GROUP::V15);
	test_func("V20 CORE", DebugContext::GROUP::V20);
	test_func("V21 CORE", DebugContext::GROUP::V21);
	test_func("V30 CORE", DebugContext::GROUP::V30);
	test_func("V31 CORE", DebugContext::GROUP::V31);
	test_func("V32 CORE", DebugContext::GROUP::V32);
	test_func("V33 CORE", DebugContext::GROUP::V33);
	test_func("V40 CORE", DebugContext::GROUP::V40);
	test_func("V41 CORE", DebugContext::GROUP::V41);
	test_func("V42 CORE", DebugContext::GROUP::V42);
	test_func("V43 CORE", DebugContext::GROUP::V43);
	test_func("V44 CORE", DebugContext::GROUP::V44);
	test_func("V45 CORE", DebugContext::GROUP::V45);
	test_func("ATTRIBUTE", DebugContext::GROUP::ATTRIBUTE);
	test_func("BLEND", DebugContext::GROUP::BLEND);
	test_func("BUFFER", DebugContext::GROUP::BUFFER);
	test_func("COMPUTE", DebugContext::GROUP::COMPUTE);
	test_func("CULLING", DebugContext::GROUP::CULLING);
	test_func("DEBUG", DebugContext::GROUP::DEBUG);
	test_func("DEPTH", DebugContext::GROUP::DEPTH);
	test_func("DRAW", DebugContext::GROUP::DRAW);
	test_func("ENABLERS", DebugContext::GROUP::ENABLERS);
	test_func("FRAMEBUFFER", DebugContext::GROUP::FRAMEBUFFER);
	test_func("GET", DebugContext::GROUP::GET);
	test_func("IMAGE", DebugContext::GROUP::IMAGE);
	test_func("QUERY", DebugContext::GROUP::QUERY);
	test_func("PIXEL_STORE", DebugContext::GROUP::PIXEL_STORE);
	test_func("RASTERIZATION", DebugContext::GROUP::RASTERIZATION);
	test_func("SAMPLER", DebugContext::GROUP::SAMPLER);
	test_func("SCISSOR", DebugContext::GROUP::SCISSOR);
	test_func("SHADER", DebugContext::GROUP::SHADER);
	test_func("STENCIL", DebugContext::GROUP::STENCIL);
	test_func("SYNC", DebugContext::GROUP::SYNC);
	test_func("TEXTURE", DebugContext::GROUP::TEXTURE);
	test_func("TRANSFORM_FEEDBACK", DebugContext::GROUP::TRANSFORM_FEEDBACK);
	test_func("UNIFORM", DebugContext::GROUP::UNIFORM);


	//deprecated function groups and deprecated functions are ONLY defined / implemented if LAP_GL_DEBUG_CONTEXT_COMPATIBILITY is defined
	//also LAP_GL_DEBUG_CONTEXT_COMPATIBILITY_IMAGING needs to be defined for the arb_imaginging ex-core now compatibility extensions
#ifdef LAP_GL_DEBUG_CONTEXT_COMPATIBILITY
	test_func("V10_DEPRECATED", DebugContext::GROUP::V10_DEPRECATED);
	test_func("V11_DEPRECATED", DebugContext::GROUP::V11_DEPRECATED);
	test_func("V13_DEPRECATED", DebugContext::GROUP::V13_DEPRECATED);
	test_func("V14_DEPRECATED", DebugContext::GROUP::V14_DEPRECATED);
	test_func("V20_DEPRECATED", DebugContext::GROUP::V20_DEPRECATED);
	test_func("V32_DEPRECATED", DebugContext::GROUP::V32_DEPRECATED);
	test_func("DEPRECATED", DebugContext::GROUP::DEPRECATED);
#endif

}
//-------------------------------------------------------------------------------------------------
void testLogging() {
	std::cout << "**************************************************************" << std::endl << "Test Logging" << std::endl;
	//window (need this for context)
	lap::wic::WICSystem wicsystem(nullptr, false);
	lap::wic::WindowProperties wp; lap::wic::FramebufferProperties fp; lap::wic::ContextProperties cp; lap::wic::InputProperties ip;
	wp.visible = false;	cp.version_major = 4; cp.version_minor = 5; cp.debug_context = true; cp.profile_core = true;
	cp.robustness = lap::wic::ContextProperties::RobustnessType::LOSE_CONTEXT_ON_RESET;
	lap::wic::Window window = lap::wic::Window(wp, fp, cp, ip);

	// create a debug context (with various options)
	// also logging __FILE__, __LINE__, __func__ for global overriden functions
	// also, see line 21 in this file for #define LAP_GL_CONTEXT_OVERRIDE_FUNCTIONS
	using namespace lap::gl;
	DebugContext dc(true, true, true, true, true, nullptr, true, true, true);	// queries the native OpenGL context and creates a Context object with identical properties
	dc.setCurrent(true);														// set the newly created context as the current context

	//logging commands
	glBlendEquationSeparate(GL_FUNC_ADD, GL_FUNC_SUBTRACT);		//not logged (no ostream!)
	dc.setLogger(&std::cout);
	glBlendEquationSeparatei(0, GL_FUNC_ADD, GL_FUNC_SUBTRACT);	//logged (by default all groups are logged if a log ostream is provided)
	dc.setLogGroup(DebugContext::GROUP::BLEND, false);			//disable logging of blend functions
	glBlendColor(0.5f, 0.5f, 1.0f, 0.0f);						//not logged (disabled)
	dc.setLogGroup(DebugContext::GROUP::BLEND, true);			//re-enable logging of blend functions
	glBlendColor(0.9f, 0.9f, 0.9f, 0.9f);						//logged (enabled)
	dc.setLogger(&std::cout, false, false, false);				//disable __FILE__, __LINE__, __func__
	glBlendFunc(GL_ZERO, GL_ONE);								//logged
	glBlendFunci(3, GL_ONE_MINUS_SRC_ALPHA, GL_DST_ALPHA);		//logged
	glBlendFuncSeparate(GL_ZERO, GL_ONE_MINUS_DST_ALPHA, GL_ONE, GL_ONE_MINUS_SRC_COLOR);	//logged
	glBlendFuncSeparatei(0, GL_ZERO, GL_ONE_MINUS_DST_ALPHA, GL_ONE, GL_ONE_MINUS_SRC_COLOR);//logged
}
//-------------------------------------------------------------------------------------------------
void testStatsAndLeaks() {
	std::cout << "**************************************************************" << std::endl << "Test Leaks" << std::endl;
	//window (need this for context)
	lap::wic::WICSystem wicsystem(nullptr, false);
	lap::wic::WindowProperties wp; lap::wic::FramebufferProperties fp; lap::wic::ContextProperties cp; lap::wic::InputProperties ip;
	wp.visible = false;	cp.version_major = 4; cp.version_minor = 5; cp.debug_context = true; cp.profile_core = true;
	cp.robustness = lap::wic::ContextProperties::RobustnessType::LOSE_CONTEXT_ON_RESET;
	lap::wic::Window window = lap::wic::Window(wp, fp, cp, ip);

	// create a debug context (with various options)
	// also, see line 21 in this file for #define LAP_GL_CONTEXT_OVERRIDE_FUNCTIONS
	using namespace lap::gl;
	DebugContext dc(true, true, true, true, true, &std::cout);	// queries the native OpenGL context and creates a Context object with identical properties
	dc.setCurrent(true);										// set the newly created context as the current context
	
	const bool NO_LEAKS = false;

	//general commands
	glClearColor(1, 0, 0, 0);
	glClearDepth(0);
	glClear(GL_COLOR_BUFFER_BIT);

	//buffers
	GLuint buf[5];
	float bufdata[5] = { 1.0f, 2.0f, 3.0f, 4.0f, 5.0f };
	glGenBuffers(5, buf);
	for (int i = 0; i < 5; i++) {
		glBindBuffer(GL_ARRAY_BUFFER, buf[i]);
		glBufferData(GL_ARRAY_BUFFER, sizeof(float) * 5, bufdata, GL_STATIC_DRAW);
		glNamedBufferStorage(buf[i], sizeof(float) * 5, bufdata, GL_MAP_PERSISTENT_BIT);
		glBindBuffer(GL_ARRAY_BUFFER, 0);
		glBindBuffer(GL_ARRAY_BUFFER, buf[i]);
		glBindBuffer(GL_ARRAY_BUFFER, buf[i]);
		glBindBuffer(GL_ARRAY_BUFFER, buf[i]);
		glBindBuffer(GL_ARRAY_BUFFER, buf[i]);
		glBindBuffer(GL_ARRAY_BUFFER, buf[i]);
		if(NO_LEAKS) glDeleteBuffers(1, &buf[i]);	//testing leaks
	}

	//vaos
	GLuint vao[4];
	glGenVertexArrays(4, vao);
	glBindVertexArray(vao[2]);
	glBindVertexArray(0);
	if (NO_LEAKS) glDeleteVertexArrays(4, vao);	//testing leaks

	//framebuffers
	GLuint fbo[3];
	glGenFramebuffers(3, fbo);
	glBindFramebuffer(GL_FRAMEBUFFER, fbo[2]);
	glBindFramebuffer(GL_FRAMEBUFFER, 0);
	if (NO_LEAKS) glDeleteFramebuffers(3, fbo);	//testing leaks

	//renderbuffers
	GLuint rbo[7];
	glGenRenderbuffers(7, rbo);
	for (int i = 0; i < 7; i++) glNamedRenderbufferStorage(rbo[i], GL_R8, 100, 100);
	if (NO_LEAKS) glDeleteRenderbuffers(7, rbo); // testing leaks

	//programs
	GLuint programs[10];
	for (int i = 0; i < 10; i++) {
		programs[i] = glCreateProgram();
		if (NO_LEAKS) glDeleteProgram(programs[i]); //testing leaks
	}

	//shaders
	GLuint shaders[8];
	for (int i = 0; i < 8; i++) {
		shaders[i] = glCreateShader(GL_VERTEX_SHADER);
		if (NO_LEAKS) glDeleteShader(shaders[i]); //testing leaks
	}

	//program pipelines
	GLuint pipelines[12];
	glCreateProgramPipelines(12, pipelines);
	if (NO_LEAKS) glDeleteProgramPipelines(12, pipelines); //testing leaks
	

	//draws
	glDrawArrays(GL_POINTS, 0, 5);
	glDrawArraysInstanced(GL_POINTS, 0, 1, 50);

	//gets
	GLboolean boolean = glIsBuffer(buf[0]);
	GLboolean boolean2 = glIsBuffer(4323);
	int activetex;
	glGetIntegerv(GL_ACTIVE_TEXTURE, &activetex);

	//compute, use program and redundancy
	GLuint shader = glCreateShader(GL_COMPUTE_SHADER);
	const char* shadercode = "#version 450\n layout(local_size_x=1, local_size_y=1, local_size_z=1) in;\n void main(){ }\n";
	int shadercodelen = (int)std::strlen(shadercode);
	glShaderSource(shader, 1, &shadercode, &shadercodelen );	//does nothing.
	glCompileShader(shader);
	GLuint program = glCreateProgram();
	glAttachShader(program, shader);
	glLinkProgram(program);
	glUseProgram(program);
	glUseProgram(program);
	glUseProgram(program);
	glUseProgram(program);
	glUseProgram(program);
	glDispatchCompute(1, 1, 1);
	glDeleteShader(shader);
	glDeleteProgram(program);

	//query
	GLuint query[9];
	glGenQueries(9, query);
	glBeginQuery(GL_TIME_ELAPSED, query[3]);
	glEndQuery(GL_TIME_ELAPSED);
	if (NO_LEAKS) glDeleteQueries(9, query); //testing leaks

	//samplers
	GLuint samplers[18];
	glGenSamplers(15, samplers);
	if (NO_LEAKS) glDeleteSamplers(15, samplers);

	//textures
	GLuint texture[15];
	float texturedata[] = { 1.0f, 1.0f, 1.0f, 1.0f, 1.0f, 1.0f, 1.0f, 1.0f };
	for (int i = 0; i < 15; i++) {
		glGenTextures(1, &texture[i]);
		glActiveTexture(GL_TEXTURE0);
		glBindTexture(GL_TEXTURE_1D, texture[i]);
		glTexImage1D(GL_TEXTURE_1D, 0, GL_RED, 8, 0, GL_RED, GL_FLOAT, texturedata);
		if (NO_LEAKS)glDeleteTextures(1, &texture[i]); //testing leaks
	}
	
	//syncs
	GLsync syncs[4];
	for (int i = 0; i < 4; i++) {
		syncs[i] = glFenceSync(GL_SYNC_GPU_COMMANDS_COMPLETE, 0);
		glWaitSync(syncs[i], 0, GL_TIMEOUT_IGNORED);
		if (NO_LEAKS)glDeleteSync(syncs[i]);	//testing leaks
	}

	//feedbacks
	GLuint xfb[9];
	glCreateTransformFeedbacks(9, xfb);
	if (NO_LEAKS)glDeleteTransformFeedbacks(9, xfb);	//testing leaks

	
	//stats
	const lap::gl::Stats& stats = dc.getStats();
	std::cout << "Ran the test with the following stats:" << std::endl;
	std::cout << stats.commands.all << " OpenGL commands, of which:" << std::endl;
	std::cout << stats.commands.attribute << " attribute commands" << std::endl;
	std::cout << stats.commands.blend << " blend commands" << std::endl;
	std::cout << stats.commands.buffer << " buffer commands" << std::endl;
	std::cout << stats.commands.compute << " compute commands" << std::endl;
	std::cout << stats.commands.culling << " culling and clipping commands" << std::endl;
	std::cout << stats.commands.debug << " debug commands" << std::endl;
	std::cout << stats.commands.depth << " depth commands" << std::endl;
	std::cout << stats.commands.deprecated << " deprecated commands" << std::endl;
	std::cout << stats.commands.draw << " draw commands" << std::endl;
	std::cout << stats.commands.enabler << " enabler commands" << std::endl;
	std::cout << stats.commands.framebuffer << " framebuffer commands" << std::endl;
	std::cout << stats.commands.get << " get commands" << std::endl;
	std::cout << stats.commands.image << " image commands" << std::endl;
	std::cout << stats.commands.program_shader << " program and shader commands" << std::endl;
	std::cout << stats.commands.pixel_storage << " pixel storage commands" << std::endl;
	std::cout << stats.commands.query << " query commands" << std::endl;
	std::cout << stats.commands.rasterization << " rasterization (related) commands" << std::endl;
	std::cout << stats.commands.redundant << " redundant commands ***(WARNING)***" << std::endl;
	std::cout << stats.commands.sampler << " sampler commands" << std::endl;
	std::cout << stats.commands.scissor << " scissor commands" << std::endl;
	std::cout << stats.commands.stencil << " stencil commands" << std::endl;
	std::cout << stats.commands.sync << " sync commands" << std::endl;
	std::cout << stats.commands.texture << " texture commands" << std::endl;
	std::cout << stats.commands.transform_feedback << " transform feedback commands" << std::endl;
	std::cout << stats.commands.uniform << " uniform commands" << std::endl;
	std::cout << "---------" << std::endl;
	std::cout << stats.memory.allocated << " allocated memory" << std::endl;
	std::cout << stats.memory.allocated_buffer << " allocated memory (buffers)" << std::endl;
	std::cout << stats.memory.allocated_texture << " allocated memory (textures)" << std::endl;
	std::cout << stats.memory.allocated_renderbuffer << " allocated memory (renderbuffers)" << std::endl;
	std::cout << stats.memory.deallocated << " deallocated memory" << std::endl;
	std::cout << stats.memory.deallocated_buffer << " deallocated memory (buffers)" << std::endl;
	std::cout << stats.memory.deallocated_texture << " deallocated memory (textures)" << std::endl;
	std::cout << stats.memory.deallocated_renderbuffer << " deallocated memory (renderbuffers)" << std::endl;
	std::cout << stats.memory.allocations << " number of all allocations" << std::endl;
	std::cout << stats.memory.deallocations << " number of all deallocations" << std::endl;
	std::cout << stats.memory.allocations_buffer << " number of all buffer allocations" << std::endl;
	std::cout << stats.memory.deallocations_buffer << " number of all buffer deallocations" << std::endl;
	std::cout << stats.memory.allocations_texture << " number of all texture allocations" << std::endl;
	std::cout << stats.memory.deallocations_texture << " number of all texture deallocations" << std::endl;
	std::cout << stats.memory.allocations_renderbuffer << " number of all renderbuffer allocations" << std::endl;
	std::cout << stats.memory.allocations_renderbuffer << " number of all renderbuffer deallocations" << std::endl;
	std::cout << stats.memory.current << " current memory (data only)" << std::endl;
	std::cout << stats.memory.transfers_cpu << " total number of gpu-cpu data transfers, totalling " << stats.memory.transfers_cpu_size << " bytes." << std::endl;
	std::cout << stats.memory.transfers_gpu << " total number of gpu-gpu data transfers, totalling  " << stats.memory.transfers_gpu_size << " bytes." << std::endl;
	std::cout << "---------" << std::endl;
	std::cout << stats.objects_created << " create/generated objects" << std::endl;
	std::cout << stats.objects_destroyed << " destroyed objects" << std::endl;
	std::cout << stats.program_changes << " total number of program changes (glUseProgramStages, glUseProgram)" << std::endl;
	std::cout << stats.pipeline_flushes << " total number of pipeline flushes (glFlush, glFinish, glUseProgram, glUseProgramStages)" << std::endl;
	std::cout << stats.pipeline_barriers << " total number of memory barriers (glMemoryBarrier, glMemoryBarrierByRegion, glTextureBarrier)" << std::endl;
	std::cout << stats.primitives_issued << " total number of primitives sent to GPU (without GPU amplification, indirect rendering and primitive restart but considering instancing)" << std::endl;
	std::cout << stats.vertices_issued << " total number of vertices sent to GPU (without GPU amplification and indirect rendering but considering instancing)" << std::endl;
	std::cout << stats.state_changes << " total number of state changes" << std::endl;
	std::cout << stats.errors << " total number of errors." << std::endl;
}
//-------------------------------------------------------------------------------------------------
void testCaps() {
	std::cout << "**************************************************************" << std::endl << "Test Capabilities" << std::endl;
	//window (need this for context)
	lap::wic::WICSystem wicsystem(nullptr, false);
	lap::wic::WindowProperties wp; lap::wic::FramebufferProperties fp; lap::wic::ContextProperties cp; lap::wic::InputProperties ip;
	wp.visible = false;	cp.version_major = 4; cp.version_minor = 5; cp.debug_context = true; cp.profile_core = true;
	cp.robustness = lap::wic::ContextProperties::RobustnessType::LOSE_CONTEXT_ON_RESET;
	lap::wic::Window window = lap::wic::Window(wp, fp, cp, ip);

	// create a debug context (with various options)
	// also, see line 21 in this file for #define LAP_GL_CONTEXT_OVERRIDE_FUNCTIONS
	using namespace lap::gl;
	DebugContext dc(true, true, true, true, true, &std::cout);	// queries the native OpenGL context and creates a Context object with identical properties
	dc.setCurrent(true);										// set the newly created context as the current context

	const State& state = dc.getState();

	std::cout << "core = " <<((state.constants.core==GL_TRUE)?"true":"false") << std::endl;
	std::cout << "debug = " << ((state.constants.debug == GL_TRUE) ? "true" : "false") << std::endl;
	std::cout << "robustness = " << ((state.constants.robustness == GL_TRUE) ? "true" : "false") << std::endl;
	std::cout << "MAJOR_VERSION = " << state.constants.MAJOR_VERSION << std::endl;
	std::cout << "MINOR_VERSION = " << state.constants.MINOR_VERSION << std::endl;
	std::cout << "CONTEXT_FLAGS = " << state.constants.CONTEXT_FLAGS << std::endl;
	std::cout << "MAX_COMPUTE_SHADER_STORAGE_BLOCKS = " << state.constants.MAX_COMPUTE_SHADER_STORAGE_BLOCKS << std::endl;
	std::cout << "MAX_COMBINED_SHADER_STORAGE_BLOCKS = " << state.constants.MAX_COMBINED_SHADER_STORAGE_BLOCKS << std::endl;
	std::cout << "MAX_COMPUTE_UNIFORM_BLOCKS = " << state.constants.MAX_COMPUTE_UNIFORM_BLOCKS << std::endl;
	std::cout << "MAX_COMPUTE_TEXTURE_IMAGE_UNITS = " << state.constants.MAX_COMPUTE_TEXTURE_IMAGE_UNITS << std::endl;
	std::cout << "MAX_COMPUTE_UNIFORM_COMPONENTS = " << state.constants.MAX_COMPUTE_UNIFORM_COMPONENTS << std::endl;
	std::cout << "MAX_COMPUTE_ATOMIC_COUNTERS = " << state.constants.MAX_COMPUTE_ATOMIC_COUNTERS << std::endl;
	std::cout << "MAX_COMPUTE_ATOMIC_COUNTER_BUFFERS = " << state.constants.MAX_COMPUTE_ATOMIC_COUNTER_BUFFERS << std::endl;
	std::cout << "MAX_COMBINED_COMPUTE_UNIFORM_COMPONENTS = " << state.constants.MAX_COMBINED_COMPUTE_UNIFORM_COMPONENTS << std::endl;
	std::cout << "MAX_COMPUTE_WORK_GROUP_INVOCATIONS = " << state.constants.MAX_COMPUTE_WORK_GROUP_INVOCATIONS << std::endl;
	std::cout << "MAX_COMPUTE_WORK_GROUP_COUNT = [" << state.constants.MAX_COMPUTE_WORK_GROUP_COUNT[0] << " , " << state.constants.MAX_COMPUTE_WORK_GROUP_COUNT[1] << " , " << state.constants.MAX_COMPUTE_WORK_GROUP_COUNT[2] << "]" << std::endl;
	std::cout << "MAX_COMPUTE_WORK_GROUP_SIZE = [" << state.constants.MAX_COMPUTE_WORK_GROUP_SIZE[0] << " , " << state.constants.MAX_COMPUTE_WORK_GROUP_SIZE[1] << " , " << state.constants.MAX_COMPUTE_WORK_GROUP_SIZE[2] << "]" << std::endl;
	std::cout << "MAX_DEBUG_GROUP_STACK_DEPTH = " << state.constants.MAX_DEBUG_GROUP_STACK_DEPTH << std::endl;
	std::cout << "MAX_3D_TEXTURE_SIZE = " << state.constants.MAX_3D_TEXTURE_SIZE << std::endl;
	std::cout << "MAX_ARRAY_TEXTURE_LAYERS = " << state.constants.MAX_ARRAY_TEXTURE_LAYERS << std::endl;
	std::cout << "MAX_CLIP_DISTANCES = " << state.constants.MAX_CLIP_DISTANCES << std::endl;
	std::cout << "MAX_COLOR_TEXTURE_SAMPLES = " << state.constants.MAX_COLOR_TEXTURE_SAMPLES << std::endl;
	std::cout << "MAX_COMBINED_ATOMIC_COUNTERS = " << state.constants.MAX_COMBINED_ATOMIC_COUNTERS << std::endl;
	std::cout << "MAX_COMBINED_FRAGMENT_UNIFORM_COMPONENTS = " << state.constants.MAX_COMBINED_FRAGMENT_UNIFORM_COMPONENTS << std::endl;
	std::cout << "MAX_COMBINED_GEOMETRY_UNIFORM_COMPONENTS = " << state.constants.MAX_COMBINED_GEOMETRY_UNIFORM_COMPONENTS << std::endl;
	std::cout << "MAX_COMBINED_TEXTURE_IMAGE_UNITS = " << state.constants.MAX_COMBINED_TEXTURE_IMAGE_UNITS << std::endl;
	std::cout << "MAX_COMBINED_UNIFORM_BLOCKS = " << state.constants.MAX_COMBINED_UNIFORM_BLOCKS << std::endl;
	std::cout << "MAX_COMBINED_VERTEX_UNIFORM_COMPONENTS = " << state.constants.MAX_COMBINED_VERTEX_UNIFORM_COMPONENTS << std::endl;
	std::cout << "MAX_CUBE_MAP_TEXTURE_SIZE = " << state.constants.MAX_CUBE_MAP_TEXTURE_SIZE << std::endl;
	std::cout << "MAX_DEPTH_TEXTURE_SAMPLES = " << state.constants.MAX_DEPTH_TEXTURE_SAMPLES << std::endl;
	std::cout << "MAX_DRAW_BUFFERS = " << state.constants.MAX_DRAW_BUFFERS << std::endl;
	std::cout << "MAX_COLOR_ATTACHMENTS = " << state.constants.MAX_COLOR_ATTACHMENTS << std::endl;
	std::cout << "MAX_DUAL_SOURCE_DRAW_BUFFERS = " << state.constants.MAX_DUAL_SOURCE_DRAW_BUFFERS << std::endl;
	std::cout << "MAX_ELEMENTS_INDICES = " << state.constants.MAX_ELEMENTS_INDICES << std::endl;
	std::cout << "MAX_ELEMENTS_VERTICES = " << state.constants.MAX_ELEMENTS_VERTICES << std::endl;
	std::cout << "MAX_FRAGMENT_ATOMIC_COUNTERS = " << state.constants.MAX_FRAGMENT_ATOMIC_COUNTERS << std::endl;
	std::cout << "MAX_FRAGMENT_SHADER_STORAGE_BLOCKS = " << state.constants.MAX_FRAGMENT_SHADER_STORAGE_BLOCKS << std::endl;
	std::cout << "MAX_FRAGMENT_INPUT_COMPONENTS = " << state.constants.MAX_FRAGMENT_INPUT_COMPONENTS << std::endl;
	std::cout << "MAX_FRAGMENT_UNIFORM_COMPONENTS = " << state.constants.MAX_FRAGMENT_UNIFORM_COMPONENTS << std::endl;
	std::cout << "MAX_FRAGMENT_UNIFORM_VECTORS = " << state.constants.MAX_FRAGMENT_UNIFORM_VECTORS << std::endl;
	std::cout << "MAX_FRAGMENT_UNIFORM_BLOCKS = " << state.constants.MAX_FRAGMENT_UNIFORM_BLOCKS << std::endl;
	std::cout << "MAX_FRAMEBUFFER_WIDTH = " << state.constants.MAX_FRAMEBUFFER_WIDTH << std::endl;
	std::cout << "MAX_FRAMEBUFFER_HEIGHT = " << state.constants.MAX_FRAMEBUFFER_HEIGHT << std::endl;
	std::cout << "MAX_FRAMEBUFFER_LAYERS = " << state.constants.MAX_FRAMEBUFFER_LAYERS << std::endl;
	std::cout << "MAX_FRAMEBUFFER_SAMPLES = " << state.constants.MAX_FRAMEBUFFER_SAMPLES << std::endl;
	std::cout << "MAX_GEOMETRY_ATOMIC_COUNTERS = " << state.constants.MAX_GEOMETRY_ATOMIC_COUNTERS << std::endl;
	std::cout << "MAX_GEOMETRY_SHADER_STORAGE_BLOCKS = " << state.constants.MAX_GEOMETRY_SHADER_STORAGE_BLOCKS << std::endl;
	std::cout << "MAX_GEOMETRY_INPUT_COMPONENTS = " << state.constants.MAX_GEOMETRY_INPUT_COMPONENTS << std::endl;
	std::cout << "MAX_GEOMETRY_OUTPUT_COMPONENTS = " << state.constants.MAX_GEOMETRY_OUTPUT_COMPONENTS << std::endl;
	std::cout << "MAX_GEOMETRY_TEXTURE_IMAGE_UNITS = " << state.constants.MAX_GEOMETRY_TEXTURE_IMAGE_UNITS << std::endl;
	std::cout << "MAX_GEOMETRY_UNIFORM_BLOCKS = " << state.constants.MAX_GEOMETRY_UNIFORM_BLOCKS << std::endl;
	std::cout << "MAX_GEOMETRY_UNIFORM_COMPONENTS = " << state.constants.MAX_GEOMETRY_UNIFORM_COMPONENTS << std::endl;
	std::cout << "MIN_MAP_BUFFER_ALIGNMENT = " << state.constants.MIN_MAP_BUFFER_ALIGNMENT << std::endl;
	std::cout << "MAX_INTEGER_SAMPLES = " << state.constants.MAX_INTEGER_SAMPLES << std::endl;
	std::cout << "MAX_LABEL_LENGTH = " << state.constants.MAX_LABEL_LENGTH << std::endl;
	std::cout << "MAX_PROGRAM_TEXEL_OFFSET = " << state.constants.MAX_PROGRAM_TEXEL_OFFSET << std::endl;
	std::cout << "MIN_PROGRAM_TEXEL_OFFSET = " << state.constants.MIN_PROGRAM_TEXEL_OFFSET << std::endl;
	std::cout << "MAX_RECTANGLE_TEXTURE_SIZE = " << state.constants.MAX_RECTANGLE_TEXTURE_SIZE << std::endl;
	std::cout << "MAX_RENDERBUFFER_SIZE = " << state.constants.MAX_RENDERBUFFER_SIZE << std::endl;
	std::cout << "MAX_SAMPLE_MASK_WORDS = " << state.constants.MAX_SAMPLE_MASK_WORDS << std::endl;
	std::cout << "MAX_SERVER_WAIT_TIMEOUT = " << state.constants.MAX_SERVER_WAIT_TIMEOUT << std::endl;
	std::cout << "MAX_SHADER_STORAGE_BUFFER_BINDINGS = " << state.constants.MAX_SHADER_STORAGE_BUFFER_BINDINGS << std::endl;
	std::cout << "MAX_TESS_CONTROL_ATOMIC_COUNTERS = " << state.constants.MAX_TESS_CONTROL_ATOMIC_COUNTERS << std::endl;
	std::cout << "MAX_TESS_EVALUATION_ATOMIC_COUNTERS = " << state.constants.MAX_TESS_EVALUATION_ATOMIC_COUNTERS << std::endl;
	std::cout << "MAX_TESS_CONTROL_SHADER_STORAGE_BLOCKS = " << state.constants.MAX_TESS_CONTROL_SHADER_STORAGE_BLOCKS << std::endl;
	std::cout << "MAX_TESS_EVALUATION_SHADER_STORAGE_BLOCKS = " << state.constants.MAX_TESS_EVALUATION_SHADER_STORAGE_BLOCKS << std::endl;
	std::cout << "MAX_TEXTURE_BUFFER_SIZE = " << state.constants.MAX_TEXTURE_BUFFER_SIZE << std::endl;
	std::cout << "MAX_TEXTURE_IMAGE_UNITS = " << state.constants.MAX_TEXTURE_IMAGE_UNITS << std::endl;
	std::cout << "MAX_TEXTURE_LOD_BIAS = " << state.constants.MAX_TEXTURE_LOD_BIAS << std::endl;
	std::cout << "MAX_TEXTURE_SIZE = " << state.constants.MAX_TEXTURE_SIZE << std::endl;
	std::cout << "MAX_UNIFORM_BUFFER_BINDINGS = " << state.constants.MAX_UNIFORM_BUFFER_BINDINGS << std::endl;
	std::cout << "MAX_UNIFORM_BLOCK_SIZE = " << state.constants.MAX_UNIFORM_BLOCK_SIZE << std::endl;
	std::cout << "MAX_UNIFORM_LOCATIONS = " << state.constants.MAX_UNIFORM_LOCATIONS << std::endl;
	std::cout << "MAX_VARYING_COMPONENTS = " << state.constants.MAX_VARYING_COMPONENTS << std::endl;
	std::cout << "MAX_VARYING_VECTORS = " << state.constants.MAX_VARYING_VECTORS << std::endl;
	std::cout << "MAX_VARYING_FLOATS = " << state.constants.MAX_VARYING_FLOATS << std::endl;
	std::cout << "MAX_VERTEX_ATOMIC_COUNTERS = " << state.constants.MAX_VERTEX_ATOMIC_COUNTERS << std::endl;
	std::cout << "MAX_VERTEX_ATTRIBS = " << state.constants.MAX_VERTEX_ATTRIBS << std::endl;
	std::cout << "MAX_VERTEX_SHADER_STORAGE_BLOCKS = " << state.constants.MAX_VERTEX_SHADER_STORAGE_BLOCKS << std::endl;
	std::cout << "MAX_VERTEX_TEXTURE_IMAGE_UNITS = " << state.constants.MAX_VERTEX_TEXTURE_IMAGE_UNITS << std::endl;
	std::cout << "MAX_VERTEX_UNIFORM_COMPONENTS = " << state.constants.MAX_VERTEX_UNIFORM_COMPONENTS << std::endl;
	std::cout << "MAX_VERTEX_UNIFORM_VECTORS = " << state.constants.MAX_VERTEX_UNIFORM_VECTORS << std::endl;
	std::cout << "MAX_VERTEX_OUTPUT_COMPONENTS = " << state.constants.MAX_VERTEX_OUTPUT_COMPONENTS << std::endl;
	std::cout << "MAX_VERTEX_UNIFORM_BLOCKS = " << state.constants.MAX_VERTEX_UNIFORM_BLOCKS << std::endl;
	std::cout << "MAX_VIEWPORT_DIMS = " << state.constants.MAX_VIEWPORT_DIMS << std::endl;
	std::cout << "MAX_VIEWPORTS = " << state.constants.MAX_VIEWPORTS << std::endl;
	std::cout << "MAX_VERTEX_ATTRIB_RELATIVE_OFFSET = " << state.constants.MAX_VERTEX_ATTRIB_RELATIVE_OFFSET << std::endl;
	std::cout << "MAX_VERTEX_ATTRIB_BINDINGS = " << state.constants.MAX_VERTEX_ATTRIB_BINDINGS << std::endl;
	std::cout << "MAX_ELEMENT_INDEX = " << state.constants.MAX_ELEMENT_INDEX << std::endl;
	std::cout << "MAX_ATOMIC_COUNTER_BUFFER_BINDINGS = " << state.constants.MAX_ATOMIC_COUNTER_BUFFER_BINDINGS << std::endl;
	std::cout << "MAX_ATOMIC_COUNTER_BUFFER_SIZE = " << state.constants.MAX_ATOMIC_COUNTER_BUFFER_SIZE << std::endl;
	std::cout << "MAX_COMBINED_SHADER_OUTPUT_RESOURCES = " << state.constants.MAX_COMBINED_SHADER_OUTPUT_RESOURCES << std::endl;
	std::cout << "MAX_COMBINED_TESS_CONTROL_UNIFORM_COMPONENTS = " << state.constants.MAX_COMBINED_TESS_CONTROL_UNIFORM_COMPONENTS << std::endl;
	std::cout << "MAX_COMBINED_TESS_EVALUATION_UNIFORM_COMPONENTS = " << state.constants.MAX_COMBINED_TESS_EVALUATION_UNIFORM_COMPONENTS << std::endl;
	std::cout << "MAX_COMPUTE_IMAGE_UNIFORMS = " << state.constants.MAX_COMPUTE_IMAGE_UNIFORMS << std::endl;
	std::cout << "MAX_COMPUTE_SHARED_MEMORY_SIZE = " << state.constants.MAX_COMPUTE_SHARED_MEMORY_SIZE << std::endl;
	std::cout << "MAX_DEBUG_LOGGED_MESSAGES = " << state.constants.MAX_DEBUG_LOGGED_MESSAGES << std::endl;
	std::cout << "MAX_DEBUG_MESSAGE_LENGTH = " << state.constants.MAX_DEBUG_MESSAGE_LENGTH << std::endl;
	std::cout << "MAX_FRAGMENT_ATOMIC_COUNTER_BUFFERS = " << state.constants.MAX_FRAGMENT_ATOMIC_COUNTER_BUFFERS << std::endl;
	std::cout << "MAX_FRAGMENT_IMAGE_UNIFORMS = " << state.constants.MAX_FRAGMENT_IMAGE_UNIFORMS << std::endl;
	std::cout << "MAX_FRAGMENT_INTERPOLATION_OFFSET = " << state.constants.MAX_FRAGMENT_INTERPOLATION_OFFSET << std::endl;
	std::cout << "MAX_GEOMETRY_ATOMIC_COUNTER_BUFFERS = " << state.constants.MAX_GEOMETRY_ATOMIC_COUNTER_BUFFERS << std::endl;
	std::cout << "MAX_GEOMETRY_IMAGE_UNIFORMS = " << state.constants.MAX_GEOMETRY_IMAGE_UNIFORMS << std::endl;
	std::cout << "MAX_GEOMETRY_OUTPUT_VERTICES = " << state.constants.MAX_GEOMETRY_OUTPUT_VERTICES << std::endl;
	std::cout << "MAX_GEOMETRY_SHADER_INVOCATIONS = " << state.constants.MAX_GEOMETRY_SHADER_INVOCATIONS << std::endl;
	std::cout << "MAX_GEOMETRY_TOTAL_OUTPUT_COMPONENTS = " << state.constants.MAX_GEOMETRY_TOTAL_OUTPUT_COMPONENTS << std::endl;
	std::cout << "MAX_IMAGE_SAMPLES = " << state.constants.MAX_IMAGE_SAMPLES << std::endl;
	std::cout << "MAX_IMAGE_UNITS = " << state.constants.MAX_IMAGE_UNITS << std::endl;
	std::cout << "MAX_PATCH_VERTICES = " << state.constants.MAX_PATCH_VERTICES << std::endl;
	std::cout << "MAX_PROGRAM_TEXTURE_GATHER_OFFSET = " << state.constants.MAX_PROGRAM_TEXTURE_GATHER_OFFSET << std::endl;
	std::cout << "MAX_SHADER_STORAGE_BLOCK_SIZE = " << state.constants.MAX_SHADER_STORAGE_BLOCK_SIZE << std::endl;
	std::cout << "MAX_SUBROUTINE_UNIFORM_LOCATIONS = " << state.constants.MAX_SUBROUTINE_UNIFORM_LOCATIONS << std::endl;
	std::cout << "MAX_SUBROUTINES = " << state.constants.MAX_SUBROUTINES << std::endl;
	std::cout << "MAX_TESS_CONTROL_ATOMIC_COUNTER_BUFFERS = " << state.constants.MAX_TESS_CONTROL_ATOMIC_COUNTER_BUFFERS << std::endl;
	std::cout << "MAX_TESS_CONTROL_IMAGE_UNIFORMS = " << state.constants.MAX_TESS_CONTROL_IMAGE_UNIFORMS << std::endl;
	std::cout << "MAX_TESS_CONTROL_INPUT_COMPONENTS = " << state.constants.MAX_TESS_CONTROL_INPUT_COMPONENTS << std::endl;
	std::cout << "MAX_TESS_CONTROL_OUTPUT_COMPONENTS = " << state.constants.MAX_TESS_CONTROL_OUTPUT_COMPONENTS << std::endl;
	std::cout << "MAX_TESS_CONTROL_TEXTURE_IMAGE_UNITS = " << state.constants.MAX_TESS_CONTROL_TEXTURE_IMAGE_UNITS << std::endl;
	std::cout << "MAX_TESS_CONTROL_TOTAL_OUTPUT_COMPONENTS = " << state.constants.MAX_TESS_CONTROL_TOTAL_OUTPUT_COMPONENTS << std::endl;
	std::cout << "MAX_TESS_CONTROL_UNIFORM_BLOCKS = " << state.constants.MAX_TESS_CONTROL_UNIFORM_BLOCKS << std::endl;
	std::cout << "MAX_TESS_CONTROL_UNIFORM_COMPONENTS = " << state.constants.MAX_TESS_CONTROL_UNIFORM_COMPONENTS << std::endl;
	std::cout << "MAX_TESS_EVALUATION_ATOMIC_COUNTER_BUFFERS = " << state.constants.MAX_TESS_EVALUATION_ATOMIC_COUNTER_BUFFERS << std::endl;
	std::cout << "MAX_TESS_EVALUATION_IMAGE_UNIFORMS = " << state.constants.MAX_TESS_EVALUATION_IMAGE_UNIFORMS << std::endl;
	std::cout << "MAX_TESS_EVALUATION_INPUT_COMPONENTS = " << state.constants.MAX_TESS_EVALUATION_INPUT_COMPONENTS << std::endl;
	std::cout << "MAX_TESS_EVALUATION_OUTPUT_COMPONENTS = " << state.constants.MAX_TESS_EVALUATION_OUTPUT_COMPONENTS << std::endl;
	std::cout << "MAX_TESS_EVALUATION_TEXTURE_IMAGE_UNITS = " << state.constants.MAX_TESS_EVALUATION_TEXTURE_IMAGE_UNITS << std::endl;
	std::cout << "MAX_TESS_EVALUATION_UNIFORM_BLOCKS = " << state.constants.MAX_TESS_EVALUATION_UNIFORM_BLOCKS << std::endl;
	std::cout << "MAX_TESS_EVALUATION_UNIFORM_COMPONENTS = " << state.constants.MAX_TESS_EVALUATION_UNIFORM_COMPONENTS << std::endl;
	std::cout << "MAX_TESS_GEN_LEVEL = " << state.constants.MAX_TESS_GEN_LEVEL << std::endl;
	std::cout << "MAX_TESS_PATCH_COMPONENTS = " << state.constants.MAX_TESS_PATCH_COMPONENTS << std::endl;
	std::cout << "MAX_TRANSFORM_FEEDBACK_INTERLEAVED_COMPONENTS = " << state.constants.MAX_TRANSFORM_FEEDBACK_INTERLEAVED_COMPONENTS << std::endl;
	std::cout << "MAX_TRANSFORM_FEEDBACK_SEPARATE_ATTRIBS = " << state.constants.MAX_TRANSFORM_FEEDBACK_SEPARATE_ATTRIBS << std::endl;
	std::cout << "MAX_TRANSFORM_FEEDBACK_SEPARATE_COMPONENTS = " << state.constants.MAX_TRANSFORM_FEEDBACK_SEPARATE_COMPONENTS << std::endl;
	std::cout << "MAX_VERTEX_ATOMIC_COUNTER_BUFFERS = " << state.constants.MAX_VERTEX_ATOMIC_COUNTER_BUFFERS << std::endl;
	std::cout << "MAX_VERTEX_ATTRIB_STRIDE = " << state.constants.MAX_VERTEX_ATTRIB_STRIDE << std::endl;
	std::cout << "MAX_VERTEX_IMAGE_UNIFORMS = " << state.constants.MAX_VERTEX_IMAGE_UNIFORMS << std::endl;
	std::cout << "MAX_VERTEX_STREAMS = " << state.constants.MAX_VERTEX_STREAMS << std::endl;
	std::cout << "MIN_FRAGMENT_INTERPOLATION_OFFSET = " << state.constants.MIN_FRAGMENT_INTERPOLATION_OFFSET << std::endl;
	std::cout << "MIN_PROGRAM_TEXTURE_GATHER_OFFSET = " << state.constants.MIN_PROGRAM_TEXTURE_GATHER_OFFSET << std::endl;
	std::cout << "NUM_PROGRAM_BINARY_FORMATS = " << state.constants.NUM_PROGRAM_BINARY_FORMATS << std::endl;
	std::cout << "PROGRAM_BINARY_FORMATS = " << state.constants.PROGRAM_BINARY_FORMATS << std::endl;
	std::cout << "SHADER_COMPILER = " << state.constants.SHADER_COMPILER << std::endl;
	std::cout << "SHADER_STORAGE_BUFFER_OFFSET_ALIGNMENT = " << state.constants.SHADER_STORAGE_BUFFER_OFFSET_ALIGNMENT << std::endl;
	std::cout << "UNIFORM_BUFFER_OFFSET_ALIGNMENT = " << state.constants.UNIFORM_BUFFER_OFFSET_ALIGNMENT << std::endl;
	std::cout << "ALIASED_LINE_WIDTH_RANGE = [" << state.constants.ALIASED_LINE_WIDTH_RANGE[0] << " , " << state.constants.ALIASED_LINE_WIDTH_RANGE[1] << "]" << std::endl;
	std::cout << "SMOOTH_LINE_WIDTH_RANGE = [" << state.constants.SMOOTH_LINE_WIDTH_RANGE[0] << " , " << state.constants.SMOOTH_LINE_WIDTH_RANGE[1] << "]" << std::endl;
	std::cout << "POINT_SIZE_RANGE = [" << state.constants.POINT_SIZE_RANGE[0] << " , " << state.constants.POINT_SIZE_RANGE[1] << "]" << std::endl;
	std::cout << "VIEWPORT_BOUNDS_RANGE = [" << state.constants.VIEWPORT_BOUNDS_RANGE[0] <<" , " << state.constants.VIEWPORT_BOUNDS_RANGE[1] << "]" << std::endl;
	std::cout << "NUM_COMPRESSED_TEXTURE_FORMATS = " << state.constants.NUM_COMPRESSED_TEXTURE_FORMATS << std::endl;
	if (state.constants.NUM_COMPRESSED_TEXTURE_FORMATS > 0) std::cout << "Supported compressed texture formats" << std::endl;
	for (const auto& e : state.constants.COMPRESSED_TEXTURE_FORMATS) std::cout << e << std::endl;
	std::cout << "NUM_SHADER_BINARY_FORMATS = " << state.constants.NUM_SHADER_BINARY_FORMATS << std::endl;
	if (state.constants.NUM_SHADER_BINARY_FORMATS > 0) std::cout << "Supported shader binary formats" << std::endl;
	for (const auto& e : state.constants.SHADER_BINARY_FORMATS) std::cout << e << std::endl;
	std::cout << "VENDOR = " << state.constants.VENDOR << std::endl;
	std::cout << "RENDERER = " << state.constants.RENDERER << std::endl;
	std::cout << "VERSION = " << state.constants.VERSION << std::endl;
	std::cout << "SHADING_LANGUAGE_VERSION = " << state.constants.SHADING_LANGUAGE_VERSION << std::endl;
}

//-------------------------------------------------------------------------------------------------
//#define TEST_INVOKE(func,...) dc.func(__VA_ARGS__);
#define TEST_INVOKE(func,...) gl##func(__VA_ARGS__);

#define TEST_INVOKE_REDUNDANTLY(func,...){\
	gl##func(__VA_ARGS__);\
	gl##func(__VA_ARGS__);\
}
//dc.func(__VA_ARGS__);
//dc.func(__VA_ARGS__);

void APIENTRY testOpenGLCallback(GLenum source, GLenum type, GLuint id, GLenum severity, GLsizei length, const GLchar* message, const void* userParam) {
	std::cout << "---------------------------------------------------------------------" << std::endl;
	std::cout << "OpenGL callback from with message: " << message << std::endl;
	std::cout << "source: ";
	switch (source) {
	case GL_DEBUG_SOURCE_API: std::cout << "API"; break;
	case GL_DEBUG_SOURCE_WINDOW_SYSTEM: std::cout << "WINDOW_SYSTEM"; break;
	case GL_DEBUG_SOURCE_SHADER_COMPILER: std::cout << "SHADER_COMPILER"; break;
	case GL_DEBUG_SOURCE_THIRD_PARTY: std::cout << "THIRD_PARTY"; break;
	case GL_DEBUG_SOURCE_APPLICATION: std::cout << "APPLICATION"; break;
	case GL_DEBUG_SOURCE_OTHER: std::cout << "OTHER"; break;
	}
	std::cout << " type: ";
	switch (type) {
	case GL_DEBUG_TYPE_ERROR: std::cout << "ERROR";	break;
	case GL_DEBUG_TYPE_DEPRECATED_BEHAVIOR: std::cout << "DEPRECATED_BEHAVIOR";	break;
	case GL_DEBUG_TYPE_UNDEFINED_BEHAVIOR:	std::cout << "UNDEFINED_BEHAVIOR";	break;
	case GL_DEBUG_TYPE_PORTABILITY:	std::cout << "PORTABILITY";	break;
	case GL_DEBUG_TYPE_PERFORMANCE:	std::cout << "PERFORMANCE";	break;
	case GL_DEBUG_TYPE_MARKER: std::cout << "MARKER";	break;
	case GL_DEBUG_TYPE_PUSH_GROUP: std::cout << "PUSH GROUP";	break;
	case GL_DEBUG_TYPE_POP_GROUP: std::cout << "POP GROUP";	break;
	case GL_DEBUG_TYPE_OTHER: std::cout << "OTHER";	break;
	}
	std::cout << " id: " << id << " severity: ";
	switch (severity) {
	case GL_DEBUG_SEVERITY_NOTIFICATION: std::cout << "NOTIFICATION"; break;
	case GL_DEBUG_SEVERITY_LOW:	std::cout << "LOW"; break;
	case GL_DEBUG_SEVERITY_MEDIUM: std::cout << "MEDIUM"; break;
	case GL_DEBUG_SEVERITY_HIGH: std::cout << "HIGH"; break;
	}
	std::cout << std::endl;
}

//this function is partially script generated
void testOpenGLfunctions() {
	std::cout << "**************************************************************" << std::endl << "Test Functions" << std::endl;
	//window (need this for context)
	lap::wic::WICSystem wicsystem(nullptr, false);
	lap::wic::WindowProperties wp; lap::wic::FramebufferProperties fp; lap::wic::ContextProperties cp; lap::wic::InputProperties ip;
	wp.visible = false;	cp.version_major = 4; cp.version_minor = 5; cp.debug_context = true; cp.profile_core = true;
	cp.robustness = lap::wic::ContextProperties::RobustnessType::LOSE_CONTEXT_ON_RESET;
	lap::wic::Window window = lap::wic::Window(wp, fp, cp, ip);

	// create a debug context (with various options)
	// also, see line 21 in this file for #define LAP_GL_CONTEXT_OVERRIDE_FUNCTIONS
	using namespace lap::gl;
	DebugContext dc(true, true, true, true, true, &std::cout);	// queries the native OpenGL context and creates a Context object with identical properties
	dc.setCurrent(true);										// set the newly created context as the current context

	bool callback_crash = true;
	auto callback = [&](const std::string& function, const std::string& result, const std::string& parameters, GLenum errorcode, const char* file, const char* func, unsigned int line) {
		if (errorcode == GL_NO_ERROR) return;
		std::cout << "-------------------------------------------------" << std::endl;
		std::cout << "ERROR " << function << "(" << parameters << ") ";
		if (result.size() > 0) std::cout << " = " << result<<" ";
		switch (errorcode) {
		case GL_INVALID_ENUM: std::cout << "GL_INVALID_ENUM"; break;
		case GL_INVALID_VALUE: std::cout << "GL_INVALID_VALUE"; break;
		case GL_INVALID_OPERATION: std::cout << "GL_INVALID_OPERATION"; break;
		case GL_INVALID_FRAMEBUFFER_OPERATION: std::cout << "GL_INVALID_FRAMEBUFFER_OPERATION"; break;
		case GL_OUT_OF_MEMORY: std::cout << "GL_OUT_OF_MEMORY"; break;
		case GL_STACK_UNDERFLOW: std::cout << "GL_STACK_UNDERFLOW"; break;
		case GL_STACK_OVERFLOW: std::cout << "GL_STACK_OVERFLOW"; break;
		}
		std::cout << "(" << errorcode << ")" << std::endl;
		if(callback_crash) assert(false);
	};
	dc.setCallbackDebug(callback);

	//per command group
	const bool check_attributes = true;
	const bool check_blend = true;
	const bool check_buffer = true;
	const bool check_compute = true;
	const bool check_culling = true;
	const bool check_debug = true;
	const bool check_depth = true;
	const bool check_draw = true;
	const bool check_enablers = true;
	const bool check_framebuffer = true;
	const bool check_image = true;
	const bool check_query = true;
	const bool check_pixel_store = true;
	const bool check_rasterization = true;
	const bool check_sampler = true;
	const bool check_scissor = true;
	const bool check_shader = true;
	const bool check_stencil = true;
	const bool check_synchronization = true;
	const bool check_texture = true;
	const bool check_transform_feedback = true;
	const bool check_uniform = true;
	const bool check_deprecated = true;

	const State& state = dc.getState();
	const Stats& stats = dc.getStats();
	Stats totalstats;

	if (check_debug) {
		std::cout << "***********************************************************************************************" << std::endl;
		std::cout << "******************************************* DEBUG *********************************************" << std::endl;
		std::cout << "***********************************************************************************************" << std::endl;
		dc.resetStats();
		callback_crash = false;
		//context is debug enabled, not synchronous
		TEST_INVOKE(DebugMessageCallback, testOpenGLCallback, nullptr);
		GLuint ids = 0;
		TEST_INVOKE(DebugMessageControl, GL_DONT_CARE, GL_DEBUG_TYPE_ERROR, GL_DEBUG_SEVERITY_LOW, 0, &ids, GL_TRUE);
		std::string message = "custom debug message";
		//testing functions not behavior
		TEST_INVOKE(DebugMessageInsert, GL_DEBUG_SOURCE_APPLICATION, GL_DEBUG_TYPE_ERROR, 55, GL_DEBUG_SEVERITY_MEDIUM, (GLsizei)message.size(), message.c_str());
		TEST_INVOKE(ObjectLabel, GL_PROGRAM_PIPELINE, 12, (GLsizei)message.size(), message.c_str());
		TEST_INVOKE(ObjectPtrLabel, &ids, (GLsizei)message.size(), message.c_str());
		TEST_INVOKE(PushDebugGroup, GL_DEBUG_SOURCE_THIRD_PARTY, 99, (GLsizei)message.size(), message.c_str());
		TEST_INVOKE(PopDebugGroup);
		assert(stats.commands.all == 7);
		assert(stats.commands.debug == 7);
		assert(stats.errors == 2);
		callback_crash = true;
		totalstats.add(dc.getStats());
	}

	if (check_attributes) {
		std::cout << "***********************************************************************************************" << std::endl;
		std::cout << "******************************************* ATTRIBUTES ****************************************" << std::endl;
		std::cout << "***********************************************************************************************" << std::endl;
		dc.resetStats();
		GLuint vao;
		TEST_INVOKE(GenVertexArrays, 1, &vao);
		const DescVertexArrayObject& dvao = state.attributes.vertex_array_objects.at(vao);
		const DescVertexArrayObject::Attribute& attr0 = dvao.attributes[0];
		const DescVertexArrayObject::Attribute& attr1 = dvao.attributes[1];
		const DescVertexArrayObject::Attribute& attr2 = dvao.attributes[2];
		const DescVertexArrayObject::Attribute& attr3 = dvao.attributes[3];
		const DescVertexArrayObject::Attribute& attr4 = dvao.attributes[4];
		const DescVertexArrayObject::Attribute& attr5 = dvao.attributes[5];
		const DescVertexArrayObject::BufferBinding& buf0 = dvao.vertex_buffer_bindings[0];
		const DescVertexArrayObject::BufferBinding& buf1 = dvao.vertex_buffer_bindings[1];
		const DescVertexArrayObject::BufferBinding& buf2 = dvao.vertex_buffer_bindings[2];
		const DescVertexArrayObject::BufferBinding& buf3 = dvao.vertex_buffer_bindings[3];
		const DescVertexArrayObject::BufferBinding& buf4 = dvao.vertex_buffer_bindings[4];
		const DescVertexArrayObject::BufferBinding& buf5 = dvao.vertex_buffer_bindings[5];
		assert(state.attributes.vertex_array_objects.size() == 1);
		TEST_INVOKE(BindVertexArray, vao);
		TEST_INVOKE(BindVertexArray, 0);
		TEST_INVOKE(BindVertexArray, vao);
		assert(state.attributes.current_vertex_array == vao);
		TEST_INVOKE_REDUNDANTLY(EnableVertexArrayAttrib, vao, 0);
		assert(dvao.attributes[0].enabled == GL_TRUE);
		TEST_INVOKE_REDUNDANTLY(EnableVertexArrayAttrib, vao, 1);
		assert(dvao.attributes[1].enabled == GL_TRUE);
		TEST_INVOKE_REDUNDANTLY(EnableVertexArrayAttrib, vao, 2);
		assert(dvao.attributes[2].enabled == GL_TRUE);
		TEST_INVOKE_REDUNDANTLY(EnableVertexArrayAttrib, vao, 3);
		assert(dvao.attributes[3].enabled == GL_TRUE);
		TEST_INVOKE_REDUNDANTLY(EnableVertexArrayAttrib, vao, 4);
		assert(dvao.attributes[4].enabled == GL_TRUE);
		TEST_INVOKE_REDUNDANTLY(EnableVertexArrayAttrib, vao, 5);
		assert(dvao.attributes[5].enabled == GL_TRUE);
		assert(state.attributes.vertex_array_objects.at(vao).attributes.at(1).enabled == GL_TRUE);
		TEST_INVOKE_REDUNDANTLY(DisableVertexArrayAttrib, vao, 1);
		assert(state.attributes.vertex_array_objects.at(vao).attributes.at(1).enabled == GL_FALSE);
		TEST_INVOKE_REDUNDANTLY(EnableVertexAttribArray, 0);
		assert(state.attributes.vertex_array_objects.at(vao).attributes.at(0).enabled == GL_TRUE);
		GLuint buffer[55];
		TEST_INVOKE(GenBuffers, 55, buffer);
		float data[20] = { 1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20 };
		TEST_INVOKE(BindBuffer, GL_ARRAY_BUFFER, buffer[0]);
		TEST_INVOKE(BufferData, GL_ARRAY_BUFFER, sizeof(data), data, GL_STATIC_DRAW);
		TEST_INVOKE(BindBuffer, GL_ARRAY_BUFFER, buffer[54]);
		TEST_INVOKE(BufferData, GL_ARRAY_BUFFER, sizeof(data), data, GL_STATIC_DRAW);
		TEST_INVOKE(BindBuffer, GL_COPY_READ_BUFFER, buffer[1]);
		TEST_INVOKE(BufferData, GL_COPY_READ_BUFFER, sizeof(data), data, GL_STATIC_DRAW);

		TEST_INVOKE_REDUNDANTLY(BindVertexBuffer, 1, buffer[0], 5, 4);
		assert(buf1.vertex_buffer == buffer[0] && buf1.offset == 5 && buf1.stride == 4);
		TEST_INVOKE_REDUNDANTLY(VertexArrayVertexBuffer, vao, 0, buffer[0], 8, 0);
		assert(buf0.vertex_buffer == buffer[0] && buf0.offset == 8 && buf0.stride == 0);

		GLuint buffers[3] = { buffer[1], buffer[1], buffer[0] };
		GLintptr offsets[3] = { 0, 4, 8 };
		GLsizei strides[3] = { 4, 0, 4 };
		GLuint buffers2[3] = { buffer[0], buffer[0], buffer[1] };
		GLintptr offsets2[3] = { 4, 8, 0 };
		GLsizei strides2[3] = { 0, 4, 4 };
		TEST_INVOKE_REDUNDANTLY(BindVertexBuffers, 0, 3, buffers, offsets, strides);
		assert(buf0.vertex_buffer == buffers[0] && buf0.offset == offsets[0] && buf0.stride == strides[0]);
		assert(buf1.vertex_buffer == buffers[1] && buf1.offset == offsets[1] && buf1.stride == strides[1]);
		assert(buf2.vertex_buffer == buffers[2] && buf2.offset == offsets[2] && buf2.stride == strides[2]);
		TEST_INVOKE_REDUNDANTLY(VertexArrayVertexBuffers, vao, 0, 3, buffers2, offsets2, strides2);
		assert(buf0.vertex_buffer == buffers2[0] && buf0.offset == offsets2[0] && buf0.stride == strides2[0]);
		assert(buf1.vertex_buffer == buffers2[1] && buf1.offset == offsets2[1] && buf1.stride == strides2[1]);
		assert(buf2.vertex_buffer == buffers2[2] && buf2.offset == offsets2[2] && buf2.stride == strides2[2]);

		GLfloat ppo[4] = { 2, 3, 4, 1 };
		GLfloat ppi[2] = { 12, 3 };
		TEST_INVOKE_REDUNDANTLY(PatchParameterfv, GL_PATCH_DEFAULT_OUTER_LEVEL, ppo);
		assert(state.attributes.patch.default_outer_level[0] == 2);
		assert(state.attributes.patch.default_outer_level[1] == 3);
		assert(state.attributes.patch.default_outer_level[2] == 4);
		assert(state.attributes.patch.default_outer_level[3] == 1);
		TEST_INVOKE_REDUNDANTLY(PatchParameterfv, GL_PATCH_DEFAULT_INNER_LEVEL, ppi);
		assert(state.attributes.patch.default_inner_level[0] == 12);
		assert(state.attributes.patch.default_inner_level[1] == 3);
		TEST_INVOKE_REDUNDANTLY(PatchParameteri, GL_PATCH_VERTICES, 16);
		assert(state.attributes.patch.vertices == 16);

		TEST_INVOKE_REDUNDANTLY(PrimitiveRestartIndex, 2);
		assert(state.attributes.provoking_vertex.index == 2);
		TEST_INVOKE_REDUNDANTLY(ProvokingVertex, GL_FIRST_VERTEX_CONVENTION);
		assert(state.attributes.provoking_vertex.mode == GL_FIRST_VERTEX_CONVENTION);
		TEST_INVOKE_REDUNDANTLY(ProvokingVertex, GL_LAST_VERTEX_CONVENTION);
		assert(state.attributes.provoking_vertex.mode == GL_LAST_VERTEX_CONVENTION);

		TEST_INVOKE_REDUNDANTLY(VertexArrayAttribBinding, vao, 4, 1);
		assert(attr4.vertex_buffer_binding == 1);
		TEST_INVOKE_REDUNDANTLY(VertexAttribBinding, 3, 3);
		assert(attr3.vertex_buffer_binding == 3);

		TEST_INVOKE_REDUNDANTLY(VertexArrayAttribFormat, vao, 1, 2, GL_BYTE, GL_TRUE, 4);
		assert(attr1.size == 2 && attr1.type == GL_BYTE && attr1.normalized == GL_TRUE && attr1.relative_offset == 4);
		TEST_INVOKE_REDUNDANTLY(VertexArrayAttribIFormat, vao, 2, 4, GL_INT, 16);
		assert(attr2.size == 4 && attr2.type == GL_INT && attr2.relative_offset == 16);
		TEST_INVOKE_REDUNDANTLY(VertexArrayAttribLFormat, vao, 3, 3, GL_DOUBLE, 8);
		assert(attr3.size == 3 && attr3.type == GL_DOUBLE && attr3.relative_offset == 8);

		TEST_INVOKE_REDUNDANTLY(VertexAttribFormat, 4, 2, GL_BYTE, GL_TRUE, 4);
		assert(attr4.size == 2 && attr4.type == GL_BYTE && attr4.normalized == GL_TRUE && attr4.relative_offset == 4);
		TEST_INVOKE_REDUNDANTLY(VertexAttribIFormat, 1, 3, GL_INT, 8);
		assert(attr1.size == 3 && attr1.type == GL_INT && attr1.relative_offset == 8);
		TEST_INVOKE_REDUNDANTLY(VertexAttribLFormat, 4, 3, GL_DOUBLE, 8);
		assert(attr4.size == 3 && attr4.type == GL_DOUBLE && attr4.relative_offset == 8);

		TEST_INVOKE_REDUNDANTLY(VertexArrayBindingDivisor, vao, 2, 4);
		assert(buf2.divisor == 4);
		TEST_INVOKE_REDUNDANTLY(VertexBindingDivisor, 2, 3);
		assert(buf2.divisor == 3);
		TEST_INVOKE_REDUNDANTLY(VertexArrayElementBuffer, vao, buffer[1]);
		assert(dvao.element_array_buffer.id == buffer[1] && dvao.element_array_buffer.offset == 0 && dvao.element_array_buffer.size == state.buffer.buffers.at(buffer[1]).size);

		TEST_INVOKE_REDUNDANTLY(VertexAttribIPointer, 3, 2, GL_INT, 8, (void*)0);
		assert(attr3.size == 2 && attr3.type == GL_INT && attr3.vertex_buffer_binding == 3 && buf3.offset == 0 && buf3.stride == 8 && buf3.vertex_buffer == buffer[54]);
		TEST_INVOKE_REDUNDANTLY(VertexAttribLPointer, 1, 4, GL_DOUBLE, 24, (void*)0);
		assert(attr1.size == 4 && attr1.type == GL_DOUBLE && attr1.vertex_buffer_binding == 1 && buf1.offset == 0 && buf1.stride == 24 && buf1.vertex_buffer == buffer[54]);
		TEST_INVOKE_REDUNDANTLY(VertexAttribPointer, 5, 1, GL_FLOAT, GL_TRUE, 16, (void*)0);
		assert(attr5.size == 1 && attr5.type == GL_FLOAT && attr5.vertex_buffer_binding == 5 && buf5.offset == 0 && buf5.stride == 16 && buf5.vertex_buffer == buffer[54]);

		TEST_INVOKE_REDUNDANTLY(VertexAttribDivisor, 3, 12);
		assert(buf3.divisor == 12);

		GLdouble datad[4] = { 78.3, 32.2, 982.38, 823.9 };
		GLdouble maxd = (GLdouble)std::numeric_limits<GLdouble>::max();
		GLfloat dataf[4] = { 178.3f, 332.2f, 9823.38f, 83.9f };
		GLdouble maxf = (GLdouble)std::numeric_limits<GLfloat>::max();
		GLshort datas[4] = { 9, 43, 21, 32 };
		GLdouble maxs = (GLdouble)std::numeric_limits<GLshort>::max();
		GLushort dataus[4] = { 92, 2, 5, 1 };
		GLdouble maxus = (GLdouble)std::numeric_limits<GLushort>::max();
		GLint datai[4] = { 8374, 437, 322, 127 };
		GLdouble maxi = (GLdouble)std::numeric_limits<GLint>::max();
		GLuint dataui[4] = { 67, 21, 72, 66 };
		GLdouble maxui = (GLdouble)std::numeric_limits<GLuint>::max();
		GLbyte datab[4] = { 4, 5, 2, 6 };
		GLdouble maxb = (GLdouble)std::numeric_limits<GLbyte>::max();
		GLubyte dataub[4] = { 14, 15, 12, 16 };
		GLdouble maxub = (GLdouble)std::numeric_limits<GLubyte>::max();
		TEST_INVOKE_REDUNDANTLY(VertexAttrib1d, 2, datad[3]);
		assert(attr2.generic[0] == datad[3]);
		TEST_INVOKE_REDUNDANTLY(VertexAttrib1dv, 1, datad);
		assert(attr1.generic[0] == datad[0]);
		TEST_INVOKE_REDUNDANTLY(VertexAttrib1f, 5, dataf[3]);
		assert(attr5.generic[0] == dataf[3]);
		TEST_INVOKE_REDUNDANTLY(VertexAttrib1fv, 1, dataf);
		assert(attr1.generic[0] == dataf[0]);
		TEST_INVOKE_REDUNDANTLY(VertexAttrib1s, 5, datas[3]);
		assert(attr5.generic[0] == datas[3]);
		TEST_INVOKE_REDUNDANTLY(VertexAttrib1sv, 1, datas);
		assert(attr1.generic[0] == datas[0]);
		
		TEST_INVOKE_REDUNDANTLY(VertexAttrib2d, 5, datad[3], datad[2]);
		assert(attr5.generic[0] == datad[3] && attr5.generic[1] == datad[2]);
		TEST_INVOKE_REDUNDANTLY(VertexAttrib2dv, 1, datad);
		assert(attr1.generic[0] == datad[0] && attr1.generic[1] == datad[1]);
		TEST_INVOKE_REDUNDANTLY(VertexAttrib2f, 5, dataf[3], dataf[2]);
		assert(attr5.generic[0] == dataf[3] && attr5.generic[1] == dataf[2]);
		TEST_INVOKE_REDUNDANTLY(VertexAttrib2fv, 1, dataf);
		assert(attr1.generic[0] == dataf[0] && attr1.generic[1] == dataf[1]);
		TEST_INVOKE_REDUNDANTLY(VertexAttrib2s, 5, datas[3], datas[2]);
		assert(attr5.generic[0] == datas[3] && attr5.generic[1] == datas[2]);
		TEST_INVOKE_REDUNDANTLY(VertexAttrib2sv, 1, datas);
		assert(attr1.generic[0] == datas[0] && attr1.generic[1] == datas[1]);
		
		TEST_INVOKE_REDUNDANTLY(VertexAttrib3d, 5, datad[3], datad[2], datad[1]);
		assert(attr5.generic[0] == datad[3] && attr5.generic[1] == datad[2] && attr5.generic[2] == datad[1]);
		TEST_INVOKE_REDUNDANTLY(VertexAttrib3dv, 1, datad);
		assert(attr1.generic[0] == datad[0] && attr1.generic[1] == datad[1] && attr1.generic[2] == datad[2]);
		TEST_INVOKE_REDUNDANTLY(VertexAttrib3f, 5, dataf[3], dataf[2], dataf[1]);
		assert(attr5.generic[0] == dataf[3] && attr5.generic[1] == dataf[2] && attr5.generic[2] == dataf[1]);
		TEST_INVOKE_REDUNDANTLY(VertexAttrib3fv, 1, dataf);
		assert(attr1.generic[0] == dataf[0] && attr1.generic[1] == dataf[1] && attr1.generic[2] == dataf[2]);
		TEST_INVOKE_REDUNDANTLY(VertexAttrib3s, 5, datas[3], datas[2], datas[1]);
		assert(attr5.generic[0] == datas[3] && attr5.generic[1] == datas[2] && attr5.generic[2] == datas[1]);
		TEST_INVOKE_REDUNDANTLY(VertexAttrib3sv, 1, datas);
		assert(attr1.generic[0] == datas[0] && attr1.generic[1] == datas[1] && attr1.generic[2] == datas[2]);

		
		TEST_INVOKE_REDUNDANTLY(VertexAttrib4bv, 5, datab);
		assert(attr5.generic[0] == datab[0] && attr5.generic[1] == datab[1] && attr5.generic[2] == datab[2] && attr5.generic[3] == datab[3]);
		TEST_INVOKE_REDUNDANTLY(VertexAttrib4d, 5, datad[3], datad[2], datad[1], datad[0]);
		assert(attr5.generic[0] == datad[3] && attr5.generic[1] == datad[2] && attr5.generic[2] == datad[1] && attr5.generic[3] == datad[0]);
		TEST_INVOKE_REDUNDANTLY(VertexAttrib4dv, 1, datad);
		assert(attr1.generic[0] == datad[0] && attr1.generic[1] == datad[1] && attr1.generic[2] == datad[2] && attr1.generic[3] == datad[3]);
		TEST_INVOKE_REDUNDANTLY(VertexAttrib4f, 5, dataf[3], dataf[2], dataf[1], dataf[0]);
		assert(attr5.generic[0] == dataf[3] && attr5.generic[1] == dataf[2] && attr5.generic[2] == dataf[1] && attr5.generic[3] == dataf[0]);
		TEST_INVOKE_REDUNDANTLY(VertexAttrib4fv, 1, dataf);
		assert(attr1.generic[0] == dataf[0] && attr1.generic[1] == dataf[1] && attr1.generic[2] == dataf[2] && attr1.generic[3] == dataf[3]);
		TEST_INVOKE_REDUNDANTLY(VertexAttrib4iv, 1, datai);
		assert(attr1.generic[0] == datai[0] && attr1.generic[1] == datai[1] && attr1.generic[2] == datai[2] && attr1.generic[3] == datai[3]);

		TEST_INVOKE_REDUNDANTLY(VertexAttrib4s, 5, datas[3], datas[2], datas[1], datas[0]);
		assert(attr5.generic[0] == datas[3] && attr5.generic[1] == datas[2] && attr5.generic[2] == datas[1] && attr5.generic[3] == datas[0]);
		TEST_INVOKE_REDUNDANTLY(VertexAttrib4sv, 1, datas);
		assert(attr1.generic[0] == datas[0] && attr1.generic[1] == datas[1] && attr1.generic[2] == datas[2] && attr1.generic[3] == datas[3]);
		TEST_INVOKE_REDUNDANTLY(VertexAttrib4ubv, 1, dataub);
		assert(attr1.generic[0] == dataub[0] && attr1.generic[1] == dataub[1] && attr1.generic[2] == dataub[2] && attr1.generic[3] == dataub[3]);
		TEST_INVOKE_REDUNDANTLY(VertexAttrib4uiv, 1, dataui);
		assert(attr1.generic[0] == dataui[0] && attr1.generic[1] == dataui[1] && attr1.generic[2] == dataui[2] && attr1.generic[3] == dataui[3]);
		TEST_INVOKE_REDUNDANTLY(VertexAttrib4usv, 1, dataus);
		assert(attr1.generic[0] == dataus[0] && attr1.generic[1] == dataus[1] && attr1.generic[2] == dataus[2] && attr1.generic[3] == dataus[3]);

		TEST_INVOKE_REDUNDANTLY(VertexAttrib4Nbv, 1, datab);
		assert(attr1.generic[0] == datab[0]/maxb && attr1.generic[1] == datab[1]/maxb && attr1.generic[2] == datab[2]/maxb && attr1.generic[3] == datab[3]/maxb);
		TEST_INVOKE_REDUNDANTLY(VertexAttrib4Niv, 1, datai);
		assert(attr1.generic[0] == datai[0] / maxi && attr1.generic[1] == datai[1] / maxi && attr1.generic[2] == datai[2] / maxi && attr1.generic[3] == datai[3] / maxi);
		TEST_INVOKE_REDUNDANTLY(VertexAttrib4Nsv, 1, datas);
		assert(attr1.generic[0] == datas[0] / maxs && attr1.generic[1] == datas[1] / maxs && attr1.generic[2] == datas[2] / maxs && attr1.generic[3] == datas[3] / maxs);
		TEST_INVOKE_REDUNDANTLY(VertexAttrib4Nub, 1, dataub[1], dataub[3], dataub[0], dataub[2]);
		assert(attr1.generic[0] == dataub[1] / maxub && attr1.generic[1] == dataub[3] / maxub && attr1.generic[2] == dataub[0] / maxub && attr1.generic[3] == dataub[2] / maxub);

		TEST_INVOKE_REDUNDANTLY(VertexAttrib4Nubv, 1, dataub);
		assert(attr1.generic[0] == dataub[0] / maxub && attr1.generic[1] == dataub[1] / maxub && attr1.generic[2] == dataub[2] / maxub && attr1.generic[3] == dataub[3] / maxub);
		TEST_INVOKE_REDUNDANTLY(VertexAttrib4Nuiv, 1, dataui);
		assert(attr1.generic[0] == dataui[0] / maxui && attr1.generic[1] == dataui[1] / maxui && attr1.generic[2] == dataui[2] / maxui && attr1.generic[3] == dataui[3] / maxui);
		TEST_INVOKE_REDUNDANTLY(VertexAttrib4Nusv, 1, dataus);
		assert(attr1.generic[0] == dataus[0] / maxus && attr1.generic[1] == dataus[1] / maxus && attr1.generic[2] == dataus[2] / maxus && attr1.generic[3] == dataus[3] / maxus);
		
		TEST_INVOKE_REDUNDANTLY(VertexAttribI1i, 1, datai[2]);
		assert(attr1.generic[0] == datai[2]);
		TEST_INVOKE_REDUNDANTLY(VertexAttribI1iv, 1, datai);
		assert(attr1.generic[0] == datai[0]);
		TEST_INVOKE_REDUNDANTLY(VertexAttribI1ui, 1, dataui[3]);
		assert(attr1.generic[0] == dataui[3]);
		TEST_INVOKE_REDUNDANTLY(VertexAttribI1uiv, 1, dataui);
		assert(attr1.generic[0] == dataui[0]);

		TEST_INVOKE_REDUNDANTLY(VertexAttribI2i, 1, datai[2], datai[3]);
		assert(attr1.generic[0] == datai[2] && attr1.generic[1] == datai[3]);
		TEST_INVOKE_REDUNDANTLY(VertexAttribI2iv, 1, datai);
		assert(attr1.generic[0] == datai[0] && attr1.generic[1] == datai[1]);
		TEST_INVOKE_REDUNDANTLY(VertexAttribI2ui, 1, dataui[3], dataui[2]);
		assert(attr1.generic[0] == dataui[3] && attr1.generic[1] == dataui[2]);
		TEST_INVOKE_REDUNDANTLY(VertexAttribI2uiv, 1, dataui);
		assert(attr1.generic[0] == dataui[0] && attr1.generic[1] == dataui[1]);
		
		TEST_INVOKE_REDUNDANTLY(VertexAttribI3i, 1, datai[2], datai[3], datai[0]);
		assert(attr1.generic[0] == datai[2] && attr1.generic[1] == datai[3] && attr1.generic[2] == datai[0]);
		TEST_INVOKE_REDUNDANTLY(VertexAttribI3iv, 1, datai);
		assert(attr1.generic[0] == datai[0] && attr1.generic[1] == datai[1] && attr1.generic[2] == datai[2]);
		TEST_INVOKE_REDUNDANTLY(VertexAttribI3ui, 1, dataui[3], dataui[2], dataui[0]);
		assert(attr1.generic[0] == dataui[3] && attr1.generic[1] == dataui[2] && attr1.generic[2] == dataui[0]);
		TEST_INVOKE_REDUNDANTLY(VertexAttribI3uiv, 1, dataui);
		assert(attr1.generic[0] == dataui[0] && attr1.generic[1] == dataui[1] && attr1.generic[2] == dataui[2]);

		TEST_INVOKE_REDUNDANTLY(VertexAttribI4i, 1, datai[2], datai[3], datai[0], datai[1]);
		assert(attr1.generic[0] == datai[2] && attr1.generic[1] == datai[3] && attr1.generic[2] == datai[0] && attr1.generic[3] == datai[1]);
		TEST_INVOKE_REDUNDANTLY(VertexAttribI4iv, 1, datai);
		assert(attr1.generic[0] == datai[0] && attr1.generic[1] == datai[1] && attr1.generic[2] == datai[2] && attr1.generic[3] == datai[3]);
		TEST_INVOKE_REDUNDANTLY(VertexAttribI4ui, 1, dataui[3], dataui[2], dataui[0], dataui[1]);
		assert(attr1.generic[0] == dataui[3] && attr1.generic[1] == dataui[2] && attr1.generic[2] == dataui[0] && attr1.generic[3] == dataui[1]);
		TEST_INVOKE_REDUNDANTLY(VertexAttribI4uiv, 1, dataui);
		assert(attr1.generic[0] == dataui[0] && attr1.generic[1] == dataui[1] && attr1.generic[2] == dataui[2] && attr1.generic[3] == dataui[3]);

		TEST_INVOKE_REDUNDANTLY(VertexAttribI4bv, 1, datab);
		assert(attr1.generic[0] == datab[0] && attr1.generic[1] == datab[1] && attr1.generic[2] == datab[2] && attr1.generic[3] == datab[3]);
		TEST_INVOKE_REDUNDANTLY(VertexAttribI4sv, 1, datas);
		assert(attr1.generic[0] == datas[0] && attr1.generic[1] == datas[1] && attr1.generic[2] == datas[2] && attr1.generic[3] == datas[3]);
		TEST_INVOKE_REDUNDANTLY(VertexAttribI4ubv, 1, dataub);
		assert(attr1.generic[0] == dataub[0] && attr1.generic[1] == dataub[1] && attr1.generic[2] == dataub[2] && attr1.generic[3] == dataub[3]);
		TEST_INVOKE_REDUNDANTLY(VertexAttribI4usv, 1, dataus);
		assert(attr1.generic[0] == dataus[0] && attr1.generic[1] == dataus[1] && attr1.generic[2] == dataus[2] && attr1.generic[3] == dataus[3]);

		TEST_INVOKE_REDUNDANTLY(VertexAttribL1d, 2, datad[3]);
		assert(attr2.generic[0] == datad[3]);
		TEST_INVOKE_REDUNDANTLY(VertexAttribL1dv, 1, datad);
		assert(attr1.generic[0] == datad[0]);
		TEST_INVOKE_REDUNDANTLY(VertexAttribL2d, 5, datad[3], datad[2]);
		assert(attr5.generic[0] == datad[3] && attr5.generic[1] == datad[2]);
		TEST_INVOKE_REDUNDANTLY(VertexAttribL2dv, 1, datad);
		assert(attr1.generic[0] == datad[0] && attr1.generic[1] == datad[1]);
		TEST_INVOKE_REDUNDANTLY(VertexAttribL3d, 5, datad[3], datad[2], datad[1]);
		assert(attr5.generic[0] == datad[3] && attr5.generic[1] == datad[2] && attr5.generic[2] == datad[1]);
		TEST_INVOKE_REDUNDANTLY(VertexAttribL3dv, 1, datad);
		assert(attr1.generic[0] == datad[0] && attr1.generic[1] == datad[1] && attr1.generic[2] == datad[2]);
		TEST_INVOKE_REDUNDANTLY(VertexAttribL4d, 5, datad[3], datad[2], datad[1], datad[0]);
		assert(attr5.generic[0] == datad[3] && attr5.generic[1] == datad[2] && attr5.generic[2] == datad[1] && attr5.generic[3] == datad[0]);
		TEST_INVOKE_REDUNDANTLY(VertexAttribL4dv, 1, datad);
		assert(attr1.generic[0] == datad[0] && attr1.generic[1] == datad[1] && attr1.generic[2] == datad[2] && attr1.generic[3] == datad[3]);

		TEST_INVOKE_REDUNDANTLY(VertexAttribP1ui, 3, GL_INT_2_10_10_10_REV, GL_TRUE, 432377642);
		TEST_INVOKE_REDUNDANTLY(VertexAttribP2ui, 3, GL_UNSIGNED_INT_2_10_10_10_REV, GL_FALSE, 96537377);
		TEST_INVOKE_REDUNDANTLY(VertexAttribP3ui, 3, GL_INT_2_10_10_10_REV, GL_TRUE, 823575812);
		TEST_INVOKE_REDUNDANTLY(VertexAttribP4ui, 3, GL_INT_2_10_10_10_REV, GL_FALSE, 989976355);
		//68
		
		TEST_INVOKE(DeleteBuffers, 55, buffer);
		TEST_INVOKE_REDUNDANTLY(DisableVertexAttribArray, 0);
		assert(state.attributes.vertex_array_objects.at(vao).attributes.at(0).enabled == GL_FALSE);
		TEST_INVOKE(DeleteVertexArrays, 1, &vao);
		TEST_INVOKE(BindVertexArray, 0);

		TEST_INVOKE(BindVertexArray, 0);
		GLuint badids[3] = { 0, 0, 0 };
		TEST_INVOKE(DeleteVertexArrays, 3, badids);

		assert(stats.commands.all == 220);
		assert(stats.commands.attribute == 212);
		assert(stats.commands.buffer == 8);
		assert(stats.commands.redundant == 105);
		assert(stats.state_changes == 44);
		totalstats.add(dc.getStats());
	}
	if (check_blend) {
		std::cout << "***********************************************************************************************" << std::endl;
		std::cout << "******************************************* BLEND *********************************************" << std::endl;
		std::cout << "***********************************************************************************************" << std::endl;
		dc.resetStats();
		auto& bc = state.blend.constant_color;
		TEST_INVOKE_REDUNDANTLY(BlendColor, 0.5f, 0.3f, 0.2f, 0.1f); 
		assert(bc.r == 0.5f && bc.g == 0.3f && bc.b == 0.2f && bc.a == 0.1f);

		auto& b = state.blend.drawbuffer;
		TEST_INVOKE_REDUNDANTLY(BlendEquation, GL_MIN);
		for (int i = 0; i < b.size();i++) assert(b[i].equation.mode_rgb == GL_MIN && b[i].equation.mode_alpha == GL_MIN);
		
		TEST_INVOKE_REDUNDANTLY(BlendEquationi, 1, GL_FUNC_REVERSE_SUBTRACT);
		assert(b[1].equation.mode_rgb == GL_FUNC_REVERSE_SUBTRACT && b[1].equation.mode_alpha == GL_FUNC_REVERSE_SUBTRACT);

		TEST_INVOKE_REDUNDANTLY(BlendEquationSeparate, GL_FUNC_SUBTRACT, GL_MAX );
		for (int i = 0; i < b.size(); i++) assert(b[i].equation.mode_rgb == GL_FUNC_SUBTRACT && b[i].equation.mode_alpha == GL_MAX);

		TEST_INVOKE_REDUNDANTLY(BlendEquationSeparatei, 1, GL_FUNC_ADD, GL_FUNC_ADD);
		assert(b[1].equation.mode_rgb == GL_FUNC_ADD && b[1].equation.mode_alpha == GL_FUNC_ADD);

		TEST_INVOKE_REDUNDANTLY(BlendFunc, GL_SRC_COLOR, GL_ONE);
		for (int i = 0; i < b.size(); i++) {
			assert(b[i].function.src_rgb_factor == GL_SRC_COLOR && b[i].function.src_alpha_factor == GL_SRC_COLOR);
			assert(b[i].function.dst_rgb_factor == GL_ONE && b[i].function.dst_alpha_factor == GL_ONE);
		}

		TEST_INVOKE_REDUNDANTLY(BlendFunci, 0, GL_ONE_MINUS_CONSTANT_ALPHA, GL_ONE_MINUS_CONSTANT_ALPHA);
		assert(b[0].function.src_rgb_factor == GL_ONE_MINUS_CONSTANT_ALPHA && b[0].function.src_alpha_factor == GL_ONE_MINUS_CONSTANT_ALPHA);
		assert(b[0].function.dst_rgb_factor == GL_ONE_MINUS_CONSTANT_ALPHA && b[0].function.dst_alpha_factor == GL_ONE_MINUS_CONSTANT_ALPHA);

		TEST_INVOKE_REDUNDANTLY(BlendFuncSeparate, GL_CONSTANT_ALPHA, GL_SRC_COLOR, GL_ONE_MINUS_DST_COLOR, GL_CONSTANT_COLOR);
		for (int i = 0; i < b.size(); i++) {
			assert(b[i].function.src_rgb_factor == GL_CONSTANT_ALPHA && b[i].function.src_alpha_factor == GL_ONE_MINUS_DST_COLOR);
			assert(b[i].function.dst_rgb_factor == GL_SRC_COLOR && b[i].function.dst_alpha_factor == GL_CONSTANT_COLOR);
		}

		TEST_INVOKE_REDUNDANTLY(BlendFuncSeparatei, 1, GL_CONSTANT_ALPHA, GL_SRC_COLOR, GL_ONE_MINUS_DST_COLOR, GL_CONSTANT_COLOR);
		assert(b[1].function.src_rgb_factor == GL_CONSTANT_ALPHA && b[1].function.src_alpha_factor == GL_ONE_MINUS_DST_COLOR);
		assert(b[1].function.dst_rgb_factor == GL_SRC_COLOR && b[1].function.dst_alpha_factor == GL_CONSTANT_COLOR);

		assert(stats.commands.all == 18);
		assert(stats.commands.blend == 18);
		assert(stats.commands.redundant == 9);
		assert(stats.state_changes == 18);
		totalstats.add(dc.getStats());
	}
	if (check_buffer) {
		std::cout << "***********************************************************************************************" << std::endl;
		std::cout << "******************************************* BUFFER ********************************************" << std::endl;
		std::cout << "***********************************************************************************************" << std::endl;
		dc.resetStats();
		auto& b = state.buffer;

		GLuint buf[3], buff[3];
		TEST_INVOKE(CreateBuffers, 3, buf);
		assert(b.buffers.size()==3);
		for(auto& bit : b.buffers) assert(bit.second.created == GL_TRUE);
		TEST_INVOKE(GenBuffers, 3, buff);
		assert(b.buffers.size() == 6);
		TEST_INVOKE(DeleteBuffers, 3, buff);
		assert(b.buffers.size() == 3);

		TEST_INVOKE_REDUNDANTLY(BindBuffer, GL_ARRAY_BUFFER, buf[0]);
		assert(b.ARRAY_BUFFER.buffer == buf[0] && b.ARRAY_BUFFER.offset == 0 && b.ARRAY_BUFFER.size == 0);
		TEST_INVOKE_REDUNDANTLY(BindBufferBase, GL_ATOMIC_COUNTER_BUFFER, 1, buf[0]);
		assert(b.ATOMIC_COUNTER_BUFFER[1].buffer == buf[0] && b.ATOMIC_COUNTER_BUFFER[1].offset == 0 && b.ATOMIC_COUNTER_BUFFER[1].size == 0);
		TEST_INVOKE_REDUNDANTLY(BindBuffer, GL_COPY_READ_BUFFER, buf[0]);
		assert(b.COPY_READ_BUFFER.buffer == buf[0] && b.COPY_READ_BUFFER.offset == 0 && b.COPY_READ_BUFFER.size == 0);
		TEST_INVOKE_REDUNDANTLY(BindBuffer, GL_COPY_WRITE_BUFFER, buf[0]);
		assert(b.COPY_WRITE_BUFFER.buffer == buf[0] && b.COPY_WRITE_BUFFER.offset == 0 && b.COPY_WRITE_BUFFER.size == 0);
		TEST_INVOKE_REDUNDANTLY(BindBuffer, GL_DISPATCH_INDIRECT_BUFFER, buf[0]);
		assert(b.DISPATCH_INDIRECT_BUFFER.buffer == buf[0] && b.DISPATCH_INDIRECT_BUFFER.offset == 0 && b.DISPATCH_INDIRECT_BUFFER.size == 0);
		TEST_INVOKE_REDUNDANTLY(BindBuffer, GL_DRAW_INDIRECT_BUFFER, buf[0]);
		assert(b.DRAW_INDIRECT_BUFFER.buffer == buf[0] && b.DRAW_INDIRECT_BUFFER.offset == 0 && b.DRAW_INDIRECT_BUFFER.size == 0);
		TEST_INVOKE_REDUNDANTLY(BindBuffer, GL_ELEMENT_ARRAY_BUFFER, buf[0]);
		assert(b.ELEMENT_ARRAY_BUFFER.buffer == buf[0] && b.ELEMENT_ARRAY_BUFFER.offset == 0 && b.ELEMENT_ARRAY_BUFFER.size == 0);
		TEST_INVOKE_REDUNDANTLY(BindBuffer, GL_PIXEL_PACK_BUFFER, buf[0]);
		assert(b.PIXEL_PACK_BUFFER.buffer == buf[0] && b.PIXEL_PACK_BUFFER.offset == 0 && b.PIXEL_PACK_BUFFER.size == 0);
		TEST_INVOKE_REDUNDANTLY(BindBuffer, GL_PIXEL_UNPACK_BUFFER, buf[0]);
		assert(b.PIXEL_UNPACK_BUFFER.buffer == buf[0] && b.PIXEL_UNPACK_BUFFER.offset == 0 && b.PIXEL_UNPACK_BUFFER.size == 0);
		TEST_INVOKE_REDUNDANTLY(BindBuffer, GL_QUERY_BUFFER, buf[0]);
		assert(b.QUERY_BUFFER.buffer == buf[0] && b.QUERY_BUFFER.offset == 0 && b.QUERY_BUFFER.size == 0);
		TEST_INVOKE_REDUNDANTLY(BindBufferBase, GL_SHADER_STORAGE_BUFFER,2, buf[0]);
		assert(b.SHADER_STORAGE_BUFFER[2].buffer == buf[0] && b.SHADER_STORAGE_BUFFER[2].offset == 0 && b.SHADER_STORAGE_BUFFER[2].size == 0);
		TEST_INVOKE_REDUNDANTLY(BindBuffer, GL_TEXTURE_BUFFER, buf[0]);
		assert(b.TEXTURE_BUFFER.buffer == buf[0] && b.TEXTURE_BUFFER.offset == 0 && b.TEXTURE_BUFFER.size == 0);
		TEST_INVOKE_REDUNDANTLY(BindBufferBase, GL_TRANSFORM_FEEDBACK_BUFFER, 0, buf[0]);
		assert(b.TRANSFORM_FEEDBACK_BUFFER[0].buffer == buf[0] && b.TRANSFORM_FEEDBACK_BUFFER[0].offset == 0 && b.TRANSFORM_FEEDBACK_BUFFER[0].size == 0);
		TEST_INVOKE_REDUNDANTLY(BindBufferBase, GL_UNIFORM_BUFFER, 3, buf[0]);
		assert(b.UNIFORM_BUFFER[3].buffer == buf[0] && b.UNIFORM_BUFFER[3].offset == 0 && b.UNIFORM_BUFFER[3].size == 0);

		int data[20] = { 0,1,2,4393,32,0,1,2,4393,32,0,1,2,4393,32 ,0,1,2,4393,32 };
		GLuint id = buf[0];			const DescBuffer& descb = b.buffers.at(id);
		TEST_INVOKE(BufferData, GL_QUERY_BUFFER, 20 * sizeof(int), (GLvoid*)data, GL_STATIC_DRAW);
		assert(descb.id == id && descb.size == 20 * sizeof(int) && descb.flags == 0 && descb.immutable == GL_FALSE && descb.usage == GL_STATIC_DRAW);
		TEST_INVOKE(BufferData, GL_QUERY_BUFFER, 20 * sizeof(int), (GLvoid*)data, GL_STREAM_DRAW);
		TEST_INVOKE(BufferData, GL_QUERY_BUFFER, 20 * sizeof(int), (GLvoid*)data, GL_STREAM_READ);
		TEST_INVOKE(BufferData, GL_QUERY_BUFFER, 20 * sizeof(int), (GLvoid*)data, GL_STREAM_COPY);
		TEST_INVOKE(BufferData, GL_QUERY_BUFFER, 20 * sizeof(int), (GLvoid*)data, GL_STATIC_DRAW);
		TEST_INVOKE(BufferData, GL_QUERY_BUFFER, 20 * sizeof(int), (GLvoid*)data, GL_STATIC_READ);
		TEST_INVOKE(BufferData, GL_QUERY_BUFFER, 20 * sizeof(int), (GLvoid*)data, GL_STATIC_COPY);
		TEST_INVOKE(BufferData, GL_QUERY_BUFFER, 20 * sizeof(int), (GLvoid*)data, GL_DYNAMIC_DRAW);
		TEST_INVOKE(BufferData, GL_QUERY_BUFFER, 20 * sizeof(int), (GLvoid*)data, GL_DYNAMIC_READ);
		TEST_INVOKE(BufferData, GL_QUERY_BUFFER, 20 * sizeof(int), (GLvoid*)data, GL_DYNAMIC_COPY);

		TEST_INVOKE(BufferStorage, GL_QUERY_BUFFER, 20 * sizeof(int), (GLvoid*)data, GL_DYNAMIC_STORAGE_BIT | GL_MAP_READ_BIT | GL_MAP_WRITE_BIT | GL_MAP_PERSISTENT_BIT | GL_MAP_COHERENT_BIT | GL_CLIENT_STORAGE_BIT);
		assert(descb.flags == (GL_DYNAMIC_STORAGE_BIT | GL_MAP_READ_BIT | GL_MAP_WRITE_BIT | GL_MAP_PERSISTENT_BIT | GL_MAP_COHERENT_BIT | GL_CLIENT_STORAGE_BIT) && descb.immutable == GL_TRUE);

		TEST_INVOKE(BufferSubData, GL_COPY_READ_BUFFER, 12, 20, (GLvoid*)data);
		TEST_INVOKE(NamedBufferData, buf[1], 20 * sizeof(int), (GLvoid*)data, GL_DYNAMIC_COPY);
		GLuint id1 = buf[1];			const DescBuffer& descb1 = b.buffers.at(id1);
		assert(descb1.id == id1 && descb1.size == 20 * sizeof(int) && descb1.flags == 0 && descb1.immutable == GL_FALSE && descb1.usage == GL_DYNAMIC_COPY);

		TEST_INVOKE(NamedBufferStorage, buf[1], 20 * sizeof(int), (GLvoid*)data, GL_DYNAMIC_STORAGE_BIT | GL_MAP_READ_BIT | GL_MAP_WRITE_BIT | GL_MAP_PERSISTENT_BIT | GL_MAP_COHERENT_BIT | GL_CLIENT_STORAGE_BIT);
		assert(descb1.flags == (GL_DYNAMIC_STORAGE_BIT | GL_MAP_READ_BIT | GL_MAP_WRITE_BIT | GL_MAP_PERSISTENT_BIT | GL_MAP_COHERENT_BIT | GL_CLIENT_STORAGE_BIT) && descb1.immutable == GL_TRUE);
		TEST_INVOKE(NamedBufferSubData, buf[1], 12, 20, (GLvoid*)data);

		TEST_INVOKE_REDUNDANTLY(BindBufferRange, GL_ATOMIC_COUNTER_BUFFER, 3, buf[1], 8, 24);
		assert(b.ATOMIC_COUNTER_BUFFER[3].buffer == buf[1] && b.ATOMIC_COUNTER_BUFFER[3].offset == 8 && b.ATOMIC_COUNTER_BUFFER[3].size == 24);

		TEST_INVOKE_REDUNDANTLY(BindBuffersBase, GL_SHADER_STORAGE_BUFFER, 0, 3, buf);
		assert(b.SHADER_STORAGE_BUFFER[0].buffer == buf[0] && b.SHADER_STORAGE_BUFFER[0].offset == 0 && b.SHADER_STORAGE_BUFFER[0].size == 80);
		assert(b.SHADER_STORAGE_BUFFER[1].buffer == buf[1] && b.SHADER_STORAGE_BUFFER[1].offset == 0 && b.SHADER_STORAGE_BUFFER[1].size == 80);
		assert(b.SHADER_STORAGE_BUFFER[2].buffer == buf[2] && b.SHADER_STORAGE_BUFFER[2].offset == 0 && b.SHADER_STORAGE_BUFFER[2].size == 0);

		GLintptr offsets[3] = { 4, 16, 20 };
		GLintptr sizes[3] = { 24, 28, 32 };
		TEST_INVOKE_REDUNDANTLY(BindBuffersRange, GL_TRANSFORM_FEEDBACK_BUFFER, 0, 3, buf, offsets, sizes );
		assert(b.TRANSFORM_FEEDBACK_BUFFER[0].buffer == buf[0] && b.TRANSFORM_FEEDBACK_BUFFER[0].offset == offsets[0] && b.TRANSFORM_FEEDBACK_BUFFER[0].size == sizes[0]);
		assert(b.TRANSFORM_FEEDBACK_BUFFER[1].buffer == buf[1] && b.TRANSFORM_FEEDBACK_BUFFER[1].offset == offsets[1] && b.TRANSFORM_FEEDBACK_BUFFER[1].size == sizes[1]);
		assert(b.TRANSFORM_FEEDBACK_BUFFER[2].buffer == buf[2] && b.TRANSFORM_FEEDBACK_BUFFER[2].offset == offsets[2] && b.TRANSFORM_FEEDBACK_BUFFER[2].size == sizes[2]);

		TEST_INVOKE(BindBuffer, GL_COPY_READ_BUFFER, buf[0]);
		TEST_INVOKE(BindBuffer, GL_COPY_WRITE_BUFFER, buf[1]);
		TEST_INVOKE(CopyBufferSubData, GL_COPY_READ_BUFFER, GL_COPY_WRITE_BUFFER, 2, 4, 20);
		TEST_INVOKE(CopyNamedBufferSubData, buf[0], buf[1], 2, 4, 20);

		int value = 0;
		TEST_INVOKE(ClearBufferData, GL_COPY_READ_BUFFER, GL_R32UI, GL_RED, GL_INT, &value);
		TEST_INVOKE(ClearBufferSubData, GL_COPY_READ_BUFFER, GL_R32UI, 4, 12, GL_RED, GL_INT, &value);
		TEST_INVOKE(ClearNamedBufferData, buf[0], GL_R32UI, GL_RED, GL_INT, &value);
		TEST_INVOKE(ClearNamedBufferSubData, buf[0], GL_R32UI, 12, 8, GL_RED, GL_INT, &value);
		TEST_INVOKE(InvalidateBufferData, buf[0]);
		TEST_INVOKE(InvalidateBufferSubData, buf[0], 4, 24);

		void* ptr = TEST_INVOKE(MapBuffer, GL_ARRAY_BUFFER, GL_READ_ONLY);
		assert(ptr);
		GLboolean result = TEST_INVOKE(UnmapBuffer, GL_ARRAY_BUFFER);
		assert(result);
		ptr = TEST_INVOKE(MapBufferRange, GL_ARRAY_BUFFER, 0, 16, GL_MAP_READ_BIT | GL_MAP_WRITE_BIT | GL_MAP_FLUSH_EXPLICIT_BIT);
		TEST_INVOKE(FlushMappedBufferRange, GL_ARRAY_BUFFER, 4, 8);
		assert(ptr);
		result = TEST_INVOKE(UnmapBuffer, GL_ARRAY_BUFFER);
		assert(result);
		ptr = TEST_INVOKE(MapNamedBuffer, buf[0], GL_READ_WRITE);
		assert(ptr);
		result = TEST_INVOKE(UnmapNamedBuffer, buf[0]);
		assert(result);
		ptr = TEST_INVOKE(MapNamedBufferRange, buf[0], 4, 16, GL_MAP_READ_BIT | GL_MAP_WRITE_BIT | GL_MAP_FLUSH_EXPLICIT_BIT);
		assert(ptr);
		TEST_INVOKE(FlushMappedNamedBufferRange, buf[0], 4, 8);
		result = TEST_INVOKE(UnmapNamedBuffer, buf[0]);
		assert(result);
		assert(stats.memory.current == 160);
		TEST_INVOKE(DeleteBuffers, 3, buf);

		TEST_INVOKE_REDUNDANTLY(BindBuffer, GL_ARRAY_BUFFER, 0);
		TEST_INVOKE_REDUNDANTLY(BindBufferBase, GL_ATOMIC_COUNTER_BUFFER, 1, 0);
		TEST_INVOKE_REDUNDANTLY(BindBuffersBase, GL_UNIFORM_BUFFER, 1, 6, nullptr);
		TEST_INVOKE_REDUNDANTLY(BindBufferRange, GL_ATOMIC_COUNTER_BUFFER, 3, 0, 8, 24);
		GLuint zerobuf[3] = { 0,0,0 };
		TEST_INVOKE_REDUNDANTLY(BindBuffersRange, GL_TRANSFORM_FEEDBACK_BUFFER, 0, 3, zerobuf, offsets, sizes);
		TEST_INVOKE_REDUNDANTLY(BindBuffersBase, GL_TRANSFORM_FEEDBACK_BUFFER, 0, 3, nullptr);

		GLuint badids[3] = { 0, 0, 0 };
		TEST_INVOKE(DeleteBuffers, 3, badids);

		assert(stats.commands.all == 86);
		assert(stats.commands.redundant == 28);
		assert(stats.commands.buffer == 86);
		assert(stats.state_changes == 48);
		assert(stats.memory.transfers_cpu == 15);
		assert(stats.memory.transfers_cpu_size == 1080);
		assert(stats.memory.transfers_gpu == 2);
		assert(stats.memory.transfers_gpu_size == 40);
		assert(stats.memory.allocations == 2);
		assert(stats.memory.deallocations == 2);
		assert(stats.memory.allocations_buffer == 2);
		assert(stats.memory.deallocations_buffer == 2);
		assert(stats.memory.allocated == 160);
		assert(stats.memory.deallocated == 160);
		assert(stats.memory.allocated_buffer == 160);
		assert(stats.memory.deallocated_buffer == 160);
		totalstats.add(dc.getStats());
	}
	if (check_compute) {
		std::cout << "***********************************************************************************************" << std::endl;
		std::cout << "******************************************* COMPUTE *******************************************" << std::endl;
		std::cout << "***********************************************************************************************" << std::endl;
		dc.resetStats();
		callback_crash = false;
		TEST_INVOKE(DispatchCompute, 1, 2, 3);
		TEST_INVOKE(DispatchCompute, 1, 2, 3);
		assert(stats.commands.all == 2);
		assert(stats.errors == 2);
		assert(stats.commands.compute == 2);
		callback_crash = true;
		totalstats.add(dc.getStats());
	}
	if (check_culling) {
		std::cout << "***********************************************************************************************" << std::endl;
		std::cout << "******************************************* CULLING *******************************************" << std::endl;
		std::cout << "***********************************************************************************************" << std::endl;
		dc.resetStats();
		TEST_INVOKE_REDUNDANTLY(ClipControl, GL_UPPER_LEFT, GL_ZERO_TO_ONE);
		TEST_INVOKE_REDUNDANTLY(CullFace, GL_FRONT_AND_BACK);
		TEST_INVOKE_REDUNDANTLY(FrontFace, GL_CW);
		assert(stats.commands.all == 6);
		assert(stats.commands.culling == 6);
		assert(stats.commands.redundant == 3);
		assert(stats.state_changes == 6);
		totalstats.add(dc.getStats());
	}
	if (check_depth) {
		std::cout << "***********************************************************************************************" << std::endl;
		std::cout << "******************************************* DEPTH *********************************************" << std::endl;
		std::cout << "***********************************************************************************************" << std::endl;
		dc.resetStats();
		TEST_INVOKE_REDUNDANTLY(ClearDepth, 0.5);
		assert(state.depth.clear_value == 0.5);
		TEST_INVOKE_REDUNDANTLY(ClearDepthf, 0.2f);
		assert(state.depth.clear_value == 0.2f);
		TEST_INVOKE_REDUNDANTLY(DepthFunc, GL_NEVER);
		assert(state.depth.func == GL_NEVER);
		TEST_INVOKE_REDUNDANTLY(DepthMask, GL_FALSE);
		assert(state.depth.enabled_write == GL_FALSE);
		TEST_INVOKE_REDUNDANTLY(DepthRange, 0.5, 0.9);
		for (int i = 0; i < state.constants.MAX_VIEWPORTS; i++) assert(state.depth.range[i].near_value == 0.5 && state.depth.range[i].far_value == 0.9);
		TEST_INVOKE_REDUNDANTLY(DepthRange, 0.1f, 0.3f);
		for (int i = 0; i < state.constants.MAX_VIEWPORTS; i++) assert(state.depth.range[i].near_value == 0.1f && state.depth.range[i].far_value == 0.3f);
		double ranges[] = { 0.1, 0.5, 0.2, 0.4, 0.2, 0.6, 0.9, 0.4, 0.2, 0.7 };
		TEST_INVOKE_REDUNDANTLY(DepthRangeArrayv, 2, 5,  ranges);
		for (int i = 0; i < 5; i++) assert(state.depth.range[i + 2].near_value == ranges[i * 2] && state.depth.range[i + 2].far_value == ranges[i * 2 + 1]);
		TEST_INVOKE_REDUNDANTLY(DepthRangeIndexed, 1, 0.15, 0.88);
		assert(state.depth.range[1].near_value == 0.15 && state.depth.range[1].far_value == 0.88);
		assert(stats.commands.all == 16);
		assert(stats.commands.depth == 16);
		assert(stats.commands.redundant == 8);
		assert(stats.state_changes == 16);
		totalstats.add(dc.getStats());
	}
	if (check_draw) {
		std::cout << "***********************************************************************************************" << std::endl;
		std::cout << "******************************************* DRAW **********************************************" << std::endl;
		std::cout << "***********************************************************************************************" << std::endl;
		dc.resetStats();
		callback_crash = false;
		GLuint names[5];
		TEST_INVOKE(GenVertexArrays, 1, &names[0]);
		TEST_INVOKE(BindVertexArray, names[0]);

		TEST_INVOKE(GenTransformFeedbacks, 1, &names[4]);
		TEST_INVOKE(BindTransformFeedback, GL_TRANSFORM_FEEDBACK, names[4]);
		TEST_INVOKE(CreateBuffers, 3, &names[1]);
		GLfloat vertices[20] = { 0.0, 2.0f, 3.0f, 4.0f, 5.0f, 1.0f, 2.0f, 3.0f, 4.0f, 5.0f, 1.0f, 2.0f, 3.0f, 4.0f, 5.0f, 1.0f, 2.0f, 3.0f, 4.0f, 5.0f };
		TEST_INVOKE(NamedBufferData, names[1], sizeof(vertices), vertices, GL_STATIC_DRAW);
		GLint indices[20] = { 0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19 };
		TEST_INVOKE(NamedBufferData, names[2], sizeof(indices), indices, GL_STATIC_DRAW);
		GLuint indirect[5] = { 2, 1, 0, 0, 0 };
		TEST_INVOKE(NamedBufferData, names[3], sizeof(indirect), indirect, GL_STATIC_DRAW);
		TEST_INVOKE(BindBuffer, GL_ARRAY_BUFFER, names[1]);
		TEST_INVOKE(BindBuffer, GL_ELEMENT_ARRAY_BUFFER, names[2]);
		TEST_INVOKE(BindBuffer, GL_DRAW_INDIRECT_BUFFER, names[3]);
		TEST_INVOKE(MemoryBarrier, GL_COMMAND_BARRIER_BIT);

		TEST_INVOKE(DrawArrays, GL_POINTS, 0, 20);
		TEST_INVOKE(DrawArrays, GL_LINE_STRIP, 0, 20);
		TEST_INVOKE(DrawArrays, GL_LINE_LOOP, 0, 20);
		TEST_INVOKE(DrawArrays, GL_LINES, 0, 20);
		TEST_INVOKE(DrawArrays, GL_LINE_STRIP_ADJACENCY, 0, 20);
		TEST_INVOKE(DrawArrays, GL_LINES_ADJACENCY, 0, 20);
		TEST_INVOKE(DrawArrays, GL_TRIANGLE_STRIP, 0, 20);
		TEST_INVOKE(DrawArrays, GL_TRIANGLE_FAN, 0, 20);
		TEST_INVOKE(DrawArrays, GL_TRIANGLES, 0, 20);
		TEST_INVOKE(DrawArrays, GL_TRIANGLE_STRIP_ADJACENCY, 0, 20);
		TEST_INVOKE(DrawArrays, GL_TRIANGLES_ADJACENCY, 0, 20);
		TEST_INVOKE(DrawArrays, GL_PATCHES, 0, 20);

		TEST_INVOKE(DrawArraysIndirect, GL_POINTS, 0); 
		TEST_INVOKE(DrawArraysInstanced, GL_TRIANGLES, 0, 15, 3);
		TEST_INVOKE(DrawArraysInstancedBaseInstance, GL_TRIANGLES, 0, 15, 3, 0);
		TEST_INVOKE(DrawElements, GL_TRIANGLES, 15, GL_UNSIGNED_INT, 0);
		TEST_INVOKE(DrawElementsBaseVertex, GL_TRIANGLES, 15, GL_UNSIGNED_INT, 0, 0);
		TEST_INVOKE(DrawElementsIndirect, GL_TRIANGLES, GL_UNSIGNED_INT, 0);
		TEST_INVOKE(DrawElementsInstanced, GL_TRIANGLES, 6, GL_UNSIGNED_INT, 0, 2);
		TEST_INVOKE(DrawElementsInstancedBaseInstance, GL_TRIANGLES, 6, GL_UNSIGNED_INT, 0, 2, 0);
		TEST_INVOKE(DrawElementsInstancedBaseVertex, GL_TRIANGLES, 6, GL_UNSIGNED_INT, 0, 2, 0);
		TEST_INVOKE(DrawElementsInstancedBaseVertexBaseInstance, GL_TRIANGLES, 6, GL_UNSIGNED_INT, 0, 2, 0, 0);
		TEST_INVOKE(DrawRangeElements, GL_TRIANGLES, 5, 15, 10, GL_UNSIGNED_INT, 0);
		TEST_INVOKE(DrawRangeElementsBaseVertex, GL_TRIANGLES, 5, 15, 10, GL_UNSIGNED_INT, 0, 0);

		TEST_INVOKE(DrawTransformFeedback, GL_TRIANGLES, names[4]);
		TEST_INVOKE(DrawTransformFeedbackInstanced, GL_TRIANGLES, names[4], 3);
		TEST_INVOKE(DrawTransformFeedbackStream, GL_TRIANGLES, names[4], 3);
		TEST_INVOKE(DrawTransformFeedbackStreamInstanced, GL_TRIANGLES, names[4], 3, 2);
		GLint firsts[5] = { 0,2,4,5,8 };
		GLsizei counts[5] = { 3,6,3,6,3 };
		TEST_INVOKE(MultiDrawArrays, GL_TRIANGLES, firsts, counts, 5);
		TEST_INVOKE(MultiDrawArraysIndirect, GL_TRIANGLES, 0, 1, 4);
		GLint indexlist[5][8] = { { 1, 6, 8, 0, 2, 1, 3, 4 },{ 1, 5, 2, 1, 5, 2, 3, 4 },{ 5, 2, 1, 2, 3, 2, 3, 1 },{ 4, 9, 1, 2, 3, 4, 9, 1 },{ 6, 4, 1, 9, 2, 9, 2, 2 } };
		TEST_INVOKE(MultiDrawElements, GL_TRIANGLES, counts, GL_UNSIGNED_INT, (const GLvoid**)indexlist, 5);
		GLint baseindices[5] = { 0, 1, 0, 1, 0 };
		TEST_INVOKE(MultiDrawElementsBaseVertex, GL_TRIANGLES, counts, GL_UNSIGNED_INT, (const GLvoid**)indexlist, 5, baseindices);
		TEST_INVOKE(MultiDrawElementsIndirect, GL_TRIANGLES, GL_UNSIGNED_INT, 0, 1, 4);
				
		TEST_INVOKE(BindTransformFeedback, GL_TRANSFORM_FEEDBACK, 0);
		TEST_INVOKE(DeleteTransformFeedbacks, 1, &names[4]);
		TEST_INVOKE(BindBuffer, GL_ARRAY_BUFFER, 0);
		TEST_INVOKE(BindBuffer, GL_DRAW_INDIRECT_BUFFER, 0);
		TEST_INVOKE(DeleteBuffers, 3, &names[1]);
		TEST_INVOKE(BindVertexArray, 0);
		TEST_INVOKE(DeleteVertexArrays, 1, &names[0]);

		assert(stats.commands.all == 52);
		assert(stats.commands.attribute == 4);
		assert(stats.commands.draw == 33);
		assert(stats.state_changes == 9);
		assert(stats.errors == 4);
		assert(stats.objects_created == 5);
		assert(stats.objects_destroyed == 5);
		assert(stats.primitives_issued == 223);
		assert(stats.vertices_issued == 491);
		callback_crash = true;
		totalstats.add(dc.getStats());
	}

	if (check_enablers) {
		std::cout << "***********************************************************************************************" << std::endl;
		std::cout << "******************************************* ENABLERS ******************************************" << std::endl;
		std::cout << "***********************************************************************************************" << std::endl;
		dc.resetStats();

		TEST_INVOKE_REDUNDANTLY(Enable, GL_CLIP_DISTANCE2);
		assert(state.culling.enabled_clip_distance[2] == GL_TRUE);
		TEST_INVOKE_REDUNDANTLY(Disable, GL_CLIP_DISTANCE2);
		assert(state.culling.enabled_clip_distance[2] == GL_FALSE);
		
		TEST_INVOKE_REDUNDANTLY(Enable, GL_BLEND);
		for (int i = 0; i < state.blend.drawbuffer.size();i++) assert(state.blend.drawbuffer[i].enabled == GL_TRUE);
		TEST_INVOKE_REDUNDANTLY(Disable, GL_BLEND);
		for (int i = 0; i < state.blend.drawbuffer.size(); i++) assert(state.blend.drawbuffer[i].enabled == GL_FALSE);
		TEST_INVOKE_REDUNDANTLY(Enablei, GL_BLEND, 2);
		assert(state.blend.drawbuffer[2].enabled == GL_TRUE);
		TEST_INVOKE_REDUNDANTLY(Disablei, GL_BLEND, 2);
		assert(state.blend.drawbuffer[2].enabled == GL_FALSE);

		TEST_INVOKE_REDUNDANTLY(Enable, GL_COLOR_LOGIC_OP);
		assert(state.rasterization.logic.enabled == GL_TRUE);
		TEST_INVOKE_REDUNDANTLY(Disable, GL_COLOR_LOGIC_OP);
		assert(state.rasterization.logic.enabled == GL_FALSE);

		TEST_INVOKE_REDUNDANTLY(Enable, GL_CULL_FACE);
		assert(state.culling.enabled == GL_TRUE);
		TEST_INVOKE_REDUNDANTLY(Disable, GL_CULL_FACE);
		assert(state.culling.enabled == GL_FALSE);

		TEST_INVOKE_REDUNDANTLY(Disable, GL_DEBUG_OUTPUT);
		assert(state.debug.enabled_output == GL_FALSE);
		TEST_INVOKE_REDUNDANTLY(Enable, GL_DEBUG_OUTPUT);
		assert(state.debug.enabled_output == GL_TRUE);
		
		TEST_INVOKE_REDUNDANTLY(Enable, GL_DEBUG_OUTPUT_SYNCHRONOUS);
		assert(state.debug.enabled_synchronous == GL_TRUE);
		TEST_INVOKE_REDUNDANTLY(Disable, GL_DEBUG_OUTPUT_SYNCHRONOUS);
		assert(state.debug.enabled_synchronous == GL_FALSE);

		TEST_INVOKE_REDUNDANTLY(Enable, GL_DEPTH_CLAMP);
		assert(state.depth.clamped == GL_TRUE);
		TEST_INVOKE_REDUNDANTLY(Disable, GL_DEPTH_CLAMP);
		assert(state.depth.clamped == GL_FALSE);

		TEST_INVOKE_REDUNDANTLY(Enable, GL_DEPTH_TEST);
		assert(state.depth.enabled_test == GL_TRUE);
		TEST_INVOKE_REDUNDANTLY(Disable, GL_DEPTH_TEST);
		assert(state.depth.enabled_test == GL_FALSE);

		TEST_INVOKE_REDUNDANTLY(Disable, GL_DITHER);
		assert(state.rasterization.dithered == GL_FALSE);
		TEST_INVOKE_REDUNDANTLY(Enable, GL_DITHER);
		assert(state.rasterization.dithered == GL_TRUE);
		
		TEST_INVOKE_REDUNDANTLY(Enable, GL_FRAMEBUFFER_SRGB);
		assert(state.framebuffer.enabled_srgb == GL_TRUE);
		TEST_INVOKE_REDUNDANTLY(Disable, GL_FRAMEBUFFER_SRGB);
		assert(state.framebuffer.enabled_srgb == GL_FALSE);
		
		TEST_INVOKE_REDUNDANTLY(Enable, GL_LINE_SMOOTH);
		assert(state.rasterization.line.smoothed == GL_TRUE);
		TEST_INVOKE_REDUNDANTLY(Disable, GL_LINE_SMOOTH);
		assert(state.rasterization.line.smoothed == GL_FALSE);

		TEST_INVOKE_REDUNDANTLY(Disable, GL_MULTISAMPLE);
		assert(state.rasterization.sampling.multisampled == GL_FALSE);
		TEST_INVOKE_REDUNDANTLY(Enable, GL_MULTISAMPLE);
		assert(state.rasterization.sampling.multisampled == GL_TRUE);
		
		TEST_INVOKE_REDUNDANTLY(Enable, GL_POLYGON_OFFSET_FILL);
		assert(state.rasterization.polygons.offset == GL_TRUE);
		TEST_INVOKE_REDUNDANTLY(Disable, GL_POLYGON_OFFSET_FILL);
		assert(state.rasterization.polygons.offset == GL_FALSE);
		
		TEST_INVOKE_REDUNDANTLY(Enable, GL_POLYGON_OFFSET_LINE);
		assert(state.rasterization.line.offset == GL_TRUE);
		TEST_INVOKE_REDUNDANTLY(Disable, GL_POLYGON_OFFSET_LINE);
		assert(state.rasterization.line.offset == GL_FALSE);
		
		TEST_INVOKE_REDUNDANTLY(Enable, GL_POLYGON_OFFSET_POINT);
		assert(state.rasterization.point.offset == GL_TRUE);
		TEST_INVOKE_REDUNDANTLY(Disable, GL_POLYGON_OFFSET_POINT);
		assert(state.rasterization.point.offset == GL_FALSE);
		
		TEST_INVOKE_REDUNDANTLY(Enable, GL_POLYGON_SMOOTH);
		assert(state.rasterization.polygons.smooth == GL_TRUE);
		TEST_INVOKE_REDUNDANTLY(Disable, GL_POLYGON_SMOOTH);
		assert(state.rasterization.polygons.smooth == GL_FALSE);
		
		TEST_INVOKE_REDUNDANTLY(Enable, GL_PRIMITIVE_RESTART);
		assert(state.attributes.provoking_vertex.enabled == GL_TRUE);
		TEST_INVOKE_REDUNDANTLY(Disable, GL_PRIMITIVE_RESTART);
		assert(state.attributes.provoking_vertex.enabled == GL_FALSE);
		
		TEST_INVOKE_REDUNDANTLY(Enable, GL_PRIMITIVE_RESTART_FIXED_INDEX);
		assert(state.attributes.provoking_vertex.enabled_fixed_index == GL_TRUE);
		TEST_INVOKE_REDUNDANTLY(Disable, GL_PRIMITIVE_RESTART_FIXED_INDEX);
		assert(state.attributes.provoking_vertex.enabled_fixed_index == GL_FALSE);
		
		TEST_INVOKE_REDUNDANTLY(Enable, GL_RASTERIZER_DISCARD);
		assert(state.rasterization.discard == GL_TRUE);
		TEST_INVOKE_REDUNDANTLY(Disable, GL_RASTERIZER_DISCARD);
		assert(state.rasterization.discard == GL_FALSE);
		
		TEST_INVOKE_REDUNDANTLY(Enable, GL_SAMPLE_ALPHA_TO_COVERAGE);
		assert(state.rasterization.sampling.alpha_to_coverage == GL_TRUE);
		TEST_INVOKE_REDUNDANTLY(Disable, GL_SAMPLE_ALPHA_TO_COVERAGE);
		assert(state.rasterization.sampling.alpha_to_coverage == GL_FALSE);
		
		TEST_INVOKE_REDUNDANTLY(Enable, GL_SAMPLE_ALPHA_TO_ONE);
		assert(state.rasterization.sampling.alpha_to_one == GL_TRUE);
		TEST_INVOKE_REDUNDANTLY(Disable, GL_SAMPLE_ALPHA_TO_ONE);
		assert(state.rasterization.sampling.alpha_to_one == GL_FALSE);

		TEST_INVOKE_REDUNDANTLY(Enable, GL_SAMPLE_COVERAGE);
		assert(state.rasterization.sampling.coverage_mask_enabled == GL_TRUE);
		TEST_INVOKE_REDUNDANTLY(Disable, GL_SAMPLE_COVERAGE);
		assert(state.rasterization.sampling.coverage_mask_enabled == GL_FALSE);
		
		TEST_INVOKE_REDUNDANTLY(Enable, GL_SAMPLE_SHADING);
		assert(state.rasterization.sampling.sample_shading == GL_TRUE);
		TEST_INVOKE_REDUNDANTLY(Disable, GL_SAMPLE_SHADING);
		assert(state.rasterization.sampling.sample_shading == GL_FALSE);
		
		TEST_INVOKE_REDUNDANTLY(Enable, GL_SAMPLE_MASK);
		assert(state.rasterization.sampling.sample_mask == GL_TRUE);
		TEST_INVOKE_REDUNDANTLY(Disable, GL_SAMPLE_MASK);
		assert(state.rasterization.sampling.sample_mask == GL_FALSE);
		
		TEST_INVOKE_REDUNDANTLY(Enable, GL_SCISSOR_TEST);
		for (int i = 0; i < state.scissor.viewports.size(); i++) assert(state.scissor.viewports[i].enabled == GL_TRUE);
		TEST_INVOKE_REDUNDANTLY(Disable, GL_SCISSOR_TEST);
		for (int i = 0; i < state.scissor.viewports.size(); i++) assert(state.scissor.viewports[i].enabled == GL_FALSE);
		TEST_INVOKE_REDUNDANTLY(Enablei, GL_SCISSOR_TEST, 2);
		assert(state.scissor.viewports[2].enabled == GL_TRUE);
		TEST_INVOKE_REDUNDANTLY(Disablei, GL_SCISSOR_TEST, 2);
		assert(state.scissor.viewports[2].enabled == GL_FALSE);

		TEST_INVOKE_REDUNDANTLY(Enable, GL_STENCIL_TEST);
		assert(state.stencil.enabled == GL_TRUE);
		TEST_INVOKE_REDUNDANTLY(Disable, GL_STENCIL_TEST);
		assert(state.stencil.enabled == GL_FALSE);
		
		TEST_INVOKE_REDUNDANTLY(Enable, GL_TEXTURE_CUBE_MAP_SEAMLESS);
		assert(state.rasterization.texture_cubemap_seamless == GL_TRUE);
		TEST_INVOKE_REDUNDANTLY(Disable, GL_TEXTURE_CUBE_MAP_SEAMLESS);
		assert(state.rasterization.texture_cubemap_seamless == GL_FALSE);
		
		TEST_INVOKE_REDUNDANTLY(Enable, GL_PROGRAM_POINT_SIZE);
		assert(state.rasterization.point.program_point_size == GL_TRUE);
		TEST_INVOKE_REDUNDANTLY(Disable, GL_PROGRAM_POINT_SIZE);
		assert(state.rasterization.point.program_point_size == GL_FALSE);
		
		assert(stats.commands.all == 120);
		assert(stats.commands.enabler ==  120);
		assert(stats.commands.redundant == 60);
		assert(stats.state_changes == 120);
		totalstats.add(dc.getStats());

		dc.resetStats();
		TEST_INVOKE_REDUNDANTLY(Hint, GL_FRAGMENT_SHADER_DERIVATIVE_HINT, GL_NICEST);
		assert(state.program.fragment_shader_derivative_hint == GL_NICEST);
		TEST_INVOKE_REDUNDANTLY(Hint, GL_FRAGMENT_SHADER_DERIVATIVE_HINT, GL_DONT_CARE);
		assert(state.program.fragment_shader_derivative_hint == GL_DONT_CARE);
		TEST_INVOKE_REDUNDANTLY(Hint, GL_FRAGMENT_SHADER_DERIVATIVE_HINT, GL_FASTEST);
		assert(state.program.fragment_shader_derivative_hint == GL_FASTEST);
		TEST_INVOKE_REDUNDANTLY(Hint, GL_LINE_SMOOTH_HINT, GL_NICEST);
		assert(state.rasterization.line.smooth_hint == GL_NICEST);
		TEST_INVOKE_REDUNDANTLY(Hint, GL_POLYGON_SMOOTH_HINT, GL_NICEST);
		assert(state.rasterization.polygons.smooth_hint == GL_NICEST);
		TEST_INVOKE_REDUNDANTLY(Hint, GL_TEXTURE_COMPRESSION_HINT, GL_NICEST);
		assert(state.texture.compression_hint == GL_NICEST);

		assert(stats.commands.all == 12);
		assert(stats.commands.enabler == 12);
		assert(stats.commands.redundant == 6);
		assert(stats.state_changes == 12);
		totalstats.add(dc.getStats());

	}
	if (check_framebuffer) {
		std::cout << "***********************************************************************************************" << std::endl;
		std::cout << "******************************************* FRAMEBUFFER ***************************************" << std::endl;
		std::cout << "***********************************************************************************************" << std::endl;
		dc.resetStats();

		GLuint fbo[5], rbo[5];

		TEST_INVOKE(GenFramebuffers, 2, fbo);
		TEST_INVOKE(CreateFramebuffers, 3, &fbo[2]);
		TEST_INVOKE(GenRenderbuffers, 2, rbo);
		TEST_INVOKE(CreateRenderbuffers, 3, &rbo[2]);

		TEST_INVOKE_REDUNDANTLY(BindFramebuffer, GL_FRAMEBUFFER, fbo[0]);
		assert(state.framebuffer.DRAW_FRAMEBUFFER == fbo[0] && state.framebuffer.READ_FRAMEBUFFER == fbo[0]);
		TEST_INVOKE_REDUNDANTLY(BindFramebuffer, GL_FRAMEBUFFER, 0);
		assert(state.framebuffer.DRAW_FRAMEBUFFER == 0 && state.framebuffer.READ_FRAMEBUFFER == 0);
		TEST_INVOKE_REDUNDANTLY(BindFramebuffer, GL_READ_FRAMEBUFFER, fbo[1]);
		assert(state.framebuffer.READ_FRAMEBUFFER == fbo[1]);
		TEST_INVOKE_REDUNDANTLY(BindFramebuffer, GL_DRAW_FRAMEBUFFER, fbo[2]);
		assert(state.framebuffer.DRAW_FRAMEBUFFER == fbo[2]);
		
		TEST_INVOKE_REDUNDANTLY(BindRenderbuffer, GL_RENDERBUFFER, rbo[1]);
		assert(state.framebuffer.RENDERBUFFER == rbo[1]);
		TEST_INVOKE_REDUNDANTLY(BindRenderbuffer, GL_RENDERBUFFER, 0);
		assert(state.framebuffer.RENDERBUFFER == 0);
		
		auto& fb4 = state.framebuffer.framebuffers.at(fbo[4]);
		auto& fbd = state.framebuffer.framebuffers.at(fbo[2]);
		auto& fbr = state.framebuffer.framebuffers.at(fbo[1]);
		TEST_INVOKE_REDUNDANTLY(FramebufferParameteri, GL_FRAMEBUFFER, GL_FRAMEBUFFER_DEFAULT_WIDTH, 20);
		assert(fbd.parameters.default_width == 20);
		TEST_INVOKE_REDUNDANTLY(NamedFramebufferParameteri, fbo[4], GL_FRAMEBUFFER_DEFAULT_HEIGHT, 40);
		assert(fb4.parameters.default_height == 40);
		TEST_INVOKE_REDUNDANTLY(FramebufferParameteri, GL_DRAW_FRAMEBUFFER, GL_FRAMEBUFFER_DEFAULT_LAYERS, 3);
		assert(fbd.parameters.default_layers == 3);
		TEST_INVOKE_REDUNDANTLY(FramebufferParameteri, GL_READ_FRAMEBUFFER, GL_FRAMEBUFFER_DEFAULT_SAMPLES, 4);
		assert(fbr.parameters.default_samples == 4);
		TEST_INVOKE_REDUNDANTLY(FramebufferParameteri, GL_READ_FRAMEBUFFER, GL_FRAMEBUFFER_DEFAULT_FIXED_SAMPLE_LOCATIONS, GL_TRUE);
		assert(fbr.parameters.default_fixed_sample_locations == GL_TRUE);

		TEST_INVOKE_REDUNDANTLY(FramebufferRenderbuffer, GL_READ_FRAMEBUFFER, GL_COLOR_ATTACHMENT2, GL_RENDERBUFFER, rbo[3]);
		assert(fbr.color_buffer[2].id == rbo[3] && fbr.color_buffer[2].target == GL_RENDERBUFFER);
		TEST_INVOKE_REDUNDANTLY(FramebufferRenderbuffer, GL_READ_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, GL_RENDERBUFFER, rbo[2]);
		assert(fbr.depth_buffer.id == rbo[2] && fbr.depth_buffer.target == GL_RENDERBUFFER);
		TEST_INVOKE_REDUNDANTLY(NamedFramebufferRenderbuffer, fbo[4], GL_STENCIL_ATTACHMENT, GL_RENDERBUFFER, rbo[3]);
		assert(fb4.stencil_buffer.id == rbo[3] && fb4.stencil_buffer.target == GL_RENDERBUFFER);
		TEST_INVOKE_REDUNDANTLY(NamedFramebufferRenderbuffer, fbo[4], GL_DEPTH_STENCIL_ATTACHMENT, GL_RENDERBUFFER, rbo[4]);
		assert(fb4.stencil_buffer.id == rbo[4] && fb4.stencil_buffer.target == GL_RENDERBUFFER && fb4.depth_buffer.id == rbo[4] && fb4.depth_buffer.target == GL_RENDERBUFFER);
		TEST_INVOKE_REDUNDANTLY(NamedFramebufferRenderbuffer, fbo[4], GL_COLOR_ATTACHMENT0, GL_RENDERBUFFER, rbo[3]);
		assert(fb4.color_buffer[0].id == rbo[3] && fb4.color_buffer[0].target == GL_RENDERBUFFER);
		
		GLuint tex[5]; 
		TEST_INVOKE(GenTextures, 5, tex);
		TEST_INVOKE(BindTexture, GL_TEXTURE_1D, tex[0]);	TEST_INVOKE(TexStorage1D, GL_TEXTURE_1D, 4, GL_R8, 32);	
		TEST_INVOKE(BindTexture, GL_TEXTURE_2D, tex[1]);	TEST_INVOKE(TexStorage2D, GL_TEXTURE_2D, 4, GL_DEPTH24_STENCIL8, 32, 32);	
		TEST_INVOKE(BindTexture, GL_TEXTURE_2D_MULTISAMPLE, tex[2]);	TEST_INVOKE(TexStorage2DMultisample, GL_TEXTURE_2D_MULTISAMPLE, 8, GL_R8, 16, 16, GL_TRUE);
		TEST_INVOKE(BindTexture, GL_TEXTURE_3D, tex[3]);	TEST_INVOKE(TexStorage3D, GL_TEXTURE_3D, 5, GL_R8, 64, 64, 64);
		TEST_INVOKE(BindTexture, GL_TEXTURE_2D_MULTISAMPLE_ARRAY, tex[4]);	TEST_INVOKE(TexStorage3DMultisample, GL_TEXTURE_2D_MULTISAMPLE_ARRAY, 5, GL_R8, 32, 32, 6, GL_TRUE);

		TEST_INVOKE_REDUNDANTLY(FramebufferTexture, GL_READ_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, tex[0], 0);
		assert(fbr.depth_buffer.id == tex[0] && fbr.depth_buffer.color_encoding == GL_LINEAR && fbr.depth_buffer.layer == 0 && fbr.depth_buffer.target == GL_TEXTURE_1D);
		TEST_INVOKE_REDUNDANTLY(NamedFramebufferTexture, fbo[4], GL_DEPTH_ATTACHMENT, tex[0], 0);
		assert(fb4.depth_buffer.id == tex[0] && fb4.depth_buffer.color_encoding == GL_LINEAR && fb4.depth_buffer.layer == 0 && fb4.depth_buffer.target == GL_TEXTURE_1D);

		TEST_INVOKE_REDUNDANTLY(FramebufferTexture1D, GL_DRAW_FRAMEBUFFER, GL_STENCIL_ATTACHMENT, GL_TEXTURE_1D, 0, 3);
		assert(fbd.stencil_buffer.id == tex[0] && fbd.stencil_buffer.color_encoding == GL_LINEAR && fbd.stencil_buffer.layer == 0 && fbd.stencil_buffer.target == GL_TEXTURE_1D);
		TEST_INVOKE_REDUNDANTLY(FramebufferTexture2D, GL_READ_FRAMEBUFFER, GL_DEPTH_STENCIL_ATTACHMENT, GL_TEXTURE_2D, 0, 3);
		assert(fbr.depth_buffer.id == tex[1] && fbr.depth_buffer.color_encoding == GL_LINEAR && fbr.depth_buffer.layer == 0 && fbr.depth_buffer.target == GL_TEXTURE_2D);
		assert(fbr.stencil_buffer.id == tex[1] && fbr.stencil_buffer.color_encoding == GL_LINEAR && fbr.stencil_buffer.layer == 0 && fbr.stencil_buffer.target == GL_TEXTURE_2D);
		TEST_INVOKE_REDUNDANTLY(FramebufferTexture3D, GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_TEXTURE_2D_MULTISAMPLE_ARRAY, 0, 3, 4);
		assert(fbd.color_buffer[0].id == tex[4] && fbd.color_buffer[0].color_encoding == GL_LINEAR && fbd.color_buffer[0].layer == 4 && fbd.color_buffer[0].target == GL_TEXTURE_2D_MULTISAMPLE_ARRAY);

		TEST_INVOKE_REDUNDANTLY(FramebufferTextureLayer, GL_READ_FRAMEBUFFER, GL_COLOR_ATTACHMENT2, tex[4], 0, 3);
		assert(fbr.color_buffer[2].id == tex[4] && fbr.color_buffer[2].color_encoding == GL_LINEAR && fbr.color_buffer[2].layer == 3 && fbr.color_buffer[2].level == 0 && fbr.color_buffer[2].target == GL_TEXTURE_2D_MULTISAMPLE_ARRAY);
		TEST_INVOKE_REDUNDANTLY(NamedFramebufferTextureLayer, fbo[4], GL_COLOR_ATTACHMENT0, tex[4], 0, 5);
		assert(fb4.color_buffer[0].id == tex[4] && fb4.color_buffer[0].color_encoding == GL_LINEAR && fb4.color_buffer[0].layer == 5 && fb4.color_buffer[0].level == 0 && fb4.color_buffer[0].target == GL_TEXTURE_2D_MULTISAMPLE_ARRAY);


		assert(stats.memory.allocated == 337852);
		GLuint renb[4];
		TEST_INVOKE(CreateRenderbuffers, 4, renb);
		auto& drb0 = state.framebuffer.renderbuffers.at(renb[0]);
		auto& drb1 = state.framebuffer.renderbuffers.at(renb[1]);
		auto& drb2 = state.framebuffer.renderbuffers.at(renb[2]);
		auto& drb3 = state.framebuffer.renderbuffers.at(renb[3]);
		TEST_INVOKE(NamedRenderbufferStorage, renb[0], GL_RGBA16F, 60, 80);
		assert(drb0.alpha_bits == 16 && drb0.blue_bits == 16 && drb0.red_bits == 16 && drb0.green_bits == 16 && drb0.width == 60 && drb0.height == 80);
		assert(drb0.bps == 64 && drb0.internal_format == GL_RGBA16F && drb0.samples == 1 && drb0.size == 38400 && drb0.stencil_bits == 0 && drb0.depth_bits == 0);
		assert(stats.memory.allocated == 376252);
		assert(stats.memory.allocations_renderbuffer == 1 && stats.memory.allocated_renderbuffer == 38400);
		TEST_INVOKE(BindRenderbuffer, GL_RENDERBUFFER, renb[1]);
		TEST_INVOKE(RenderbufferStorage, GL_RENDERBUFFER, GL_DEPTH_COMPONENT24, 20, 20);
		assert(drb1.depth_bits == 24 && drb1.size == 1200);
		assert(stats.memory.allocated == 377452);
		assert(stats.memory.allocations_renderbuffer == 2 && stats.memory.allocated_renderbuffer == 39600);
		TEST_INVOKE(NamedRenderbufferStorageMultisample, renb[2], 3, GL_RG8I, 40, 40);
		assert(drb2.stencil_bits == 0 && drb2.size == 9600);
		assert(stats.memory.allocated == 387052);
		assert(stats.memory.allocations_renderbuffer == 3 && stats.memory.allocated_renderbuffer == 49200);
		TEST_INVOKE(BindRenderbuffer, GL_RENDERBUFFER, renb[3]);
		TEST_INVOKE(RenderbufferStorageMultisample, GL_RENDERBUFFER, 5, GL_DEPTH32F_STENCIL8, 10, 10);
		assert(drb3.stencil_bits == 8 && drb3.depth_bits == 32 && drb3.size == 2500);
		assert(stats.memory.allocated == 389552);
		assert(stats.memory.allocations_renderbuffer == 4 && stats.memory.allocated_renderbuffer == 51700);
		TEST_INVOKE(DeleteRenderbuffers, 4, renb);
		assert(stats.memory.deallocations_renderbuffer == 4 && stats.memory.deallocated_renderbuffer == 51700);

		
		GLuint framebuffer[2]; GLuint texture[10];
		TEST_INVOKE(GenTextures, 10, texture);
		for (int i = 0; i < 8; i++) {
			TEST_INVOKE(BindTexture, GL_TEXTURE_2D, texture[i]);	
			TEST_INVOKE(TexStorage2D, GL_TEXTURE_2D, 4, GL_RGBA8, 128, 128);
		}
		for (int i = 8; i < 10; i++) {
			TEST_INVOKE(BindTexture, GL_TEXTURE_2D, texture[i]);
			TEST_INVOKE(TexStorage2D, GL_TEXTURE_2D, 4, GL_DEPTH24_STENCIL8, 128, 128);
		}

		TEST_INVOKE(BindTexture, GL_TEXTURE_2D, 0);
		TEST_INVOKE(GenFramebuffers, 2, framebuffer);
		TEST_INVOKE(BindFramebuffer, GL_DRAW_FRAMEBUFFER, framebuffer[0]);
		TEST_INVOKE_REDUNDANTLY(FramebufferTexture, GL_DRAW_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, texture[0], 0);
		TEST_INVOKE_REDUNDANTLY(FramebufferTexture, GL_DRAW_FRAMEBUFFER, GL_COLOR_ATTACHMENT1, texture[1], 0);
		TEST_INVOKE_REDUNDANTLY(FramebufferTexture, GL_DRAW_FRAMEBUFFER, GL_COLOR_ATTACHMENT2, texture[2], 0);
		TEST_INVOKE_REDUNDANTLY(FramebufferTexture, GL_DRAW_FRAMEBUFFER, GL_COLOR_ATTACHMENT3, texture[3], 0);
		TEST_INVOKE_REDUNDANTLY(FramebufferTexture, GL_DRAW_FRAMEBUFFER, GL_DEPTH_STENCIL_ATTACHMENT, texture[8], 0);
		TEST_INVOKE(BindFramebuffer, GL_READ_FRAMEBUFFER, framebuffer[1]);
		TEST_INVOKE_REDUNDANTLY(FramebufferTexture, GL_READ_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, texture[4], 0);
		TEST_INVOKE_REDUNDANTLY(FramebufferTexture, GL_READ_FRAMEBUFFER, GL_COLOR_ATTACHMENT1, texture[5], 0);
		TEST_INVOKE_REDUNDANTLY(FramebufferTexture, GL_READ_FRAMEBUFFER, GL_COLOR_ATTACHMENT2, texture[6], 0);
		TEST_INVOKE_REDUNDANTLY(FramebufferTexture, GL_READ_FRAMEBUFFER, GL_COLOR_ATTACHMENT3, texture[7], 0);
		TEST_INVOKE_REDUNDANTLY(FramebufferTexture, GL_READ_FRAMEBUFFER, GL_DEPTH_STENCIL_ATTACHMENT, texture[9], 0);


		//drawbuffers on 0
		TEST_INVOKE(BindFramebuffer, GL_DRAW_FRAMEBUFFER, 0);
		GLenum drawbufferszero[] = { GL_NONE, GL_NONE, GL_NONE };
		TEST_INVOKE_REDUNDANTLY(DrawBuffer, GL_FRONT);
		TEST_INVOKE_REDUNDANTLY(NamedFramebufferDrawBuffer, 0, GL_BACK);
		TEST_INVOKE_REDUNDANTLY(DrawBuffers, 3, drawbufferszero);
		TEST_INVOKE_REDUNDANTLY(NamedFramebufferDrawBuffers, 0, 3, drawbufferszero);

		TEST_INVOKE(BindFramebuffer, GL_DRAW_FRAMEBUFFER, framebuffer[0]);
		GLenum drawbuffers[] = { GL_COLOR_ATTACHMENT1, GL_COLOR_ATTACHMENT0, GL_COLOR_ATTACHMENT3, GL_COLOR_ATTACHMENT2 };
		TEST_INVOKE_REDUNDANTLY(DrawBuffer, GL_COLOR_ATTACHMENT0);
		TEST_INVOKE_REDUNDANTLY(NamedFramebufferDrawBuffer, framebuffer[0], GL_COLOR_ATTACHMENT2);
		TEST_INVOKE_REDUNDANTLY(DrawBuffers, 4, drawbuffers);
		TEST_INVOKE_REDUNDANTLY(NamedFramebufferDrawBuffers, framebuffer[0], 4, drawbuffers);
				
		GLenum result = TEST_INVOKE(CheckFramebufferStatus, GL_DRAW_FRAMEBUFFER);
		assert(result == GL_FRAMEBUFFER_COMPLETE);
		result = TEST_INVOKE(CheckNamedFramebufferStatus, framebuffer[0], GL_READ_FRAMEBUFFER);
		assert(result == GL_FRAMEBUFFER_COMPLETE);
		result = TEST_INVOKE(CheckNamedFramebufferStatus, framebuffer[1], GL_READ_FRAMEBUFFER);
		assert(result == GL_FRAMEBUFFER_COMPLETE);

		TEST_INVOKE(ClearBufferfi, GL_DEPTH_STENCIL, 0, 0.4f, 0);
		TEST_INVOKE(ClearNamedFramebufferfi, framebuffer[0], GL_DEPTH_STENCIL, 0, 0.4f, 0);
		float bogusf[32]; for (int i = 0; i < 32; i++) bogusf[i] = i*342.f;
		int bogusi[32]; for (int i = 0; i < 32; i++) bogusi[i] = i;
		unsigned int bogusui[32]; for (int i = 0; i < 32; i++) bogusui[i] = i;

		TEST_INVOKE(ClearBufferfv, GL_COLOR, 1, bogusf);
		TEST_INVOKE(ClearNamedFramebufferfv, framebuffer[0], GL_COLOR, 2, bogusf);
		TEST_INVOKE(ClearBufferiv, GL_COLOR, 3, bogusi);
		TEST_INVOKE(ClearNamedFramebufferiv, framebuffer[0], GL_COLOR, 1, bogusi);
		TEST_INVOKE(ClearBufferuiv, GL_COLOR, 0, bogusui);
		TEST_INVOKE(ClearNamedFramebufferuiv, framebuffer[0], GL_COLOR, 3, bogusui);

		assert(stats.memory.transfers_gpu_size == 0);
		TEST_INVOKE(BlitFramebuffer, 10, 10, 30, 30, 4, 4, 24, 24, GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT | GL_STENCIL_BUFFER_BIT, GL_NEAREST);
		assert(stats.memory.transfers_gpu_size == 8000);	//rgba8 * 4 targets + d24 + s8
		TEST_INVOKE(BlitNamedFramebuffer, framebuffer[0], framebuffer[1], 10, 10, 30, 30, 4, 4, 24, 24, GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT | GL_STENCIL_BUFFER_BIT, GL_NEAREST);
		assert(stats.memory.transfers_gpu_size == 16000);
		TEST_INVOKE(BlitNamedFramebuffer, 0, framebuffer[1], 10, 10, 30, 30, 4, 4, 24, 24, GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT | GL_STENCIL_BUFFER_BIT, GL_NEAREST);
		assert(stats.memory.transfers_gpu_size == 32000);
		TEST_INVOKE(BlitNamedFramebuffer, framebuffer[0], 0, 10, 10, 30, 30, 4, 4, 24, 24, GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT | GL_STENCIL_BUFFER_BIT, GL_NEAREST);
		assert(stats.memory.transfers_gpu_size == 35200);
		TEST_INVOKE(BlitNamedFramebuffer, 0, 0, 10, 10, 30, 30, 4, 4, 24, 24, GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT | GL_STENCIL_BUFFER_BIT, GL_NEAREST);
		assert(stats.memory.transfers_gpu_size == 41600);

		TEST_INVOKE_REDUNDANTLY(ClampColor, GL_CLAMP_READ_COLOR, GL_TRUE);
		assert(state.framebuffer.readpixels.enabled_clamp_color == GL_TRUE);
		TEST_INVOKE(ReadBuffer, GL_COLOR_ATTACHMENT2);
		TEST_INVOKE(NamedFramebufferReadBuffer, 0, GL_FRONT_LEFT);

		TEST_INVOKE(Clear, GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT | GL_STENCIL_BUFFER_BIT);
		TEST_INVOKE_REDUNDANTLY(ClearColor, 0.5f, 0.3f, 0.2f, 0.65f);
		assert(state.framebuffer.clear_color.r == 0.5f);
		assert(state.framebuffer.clear_color.g == 0.3f);
		assert(state.framebuffer.clear_color.b == 0.2f);
		assert(state.framebuffer.clear_color.a == 0.65f);

		TEST_INVOKE_REDUNDANTLY(ColorMask, GL_TRUE, GL_FALSE, GL_TRUE, GL_FALSE);
		for (auto &e : state.framebuffer.colormask) {
			assert(e.r == GL_TRUE && e.g == GL_FALSE && e.b == GL_TRUE && e.a == GL_FALSE);
		}
		TEST_INVOKE_REDUNDANTLY(ColorMaski, 3, GL_FALSE, GL_FALSE, GL_TRUE, GL_FALSE);
		auto& cm = state.framebuffer.colormask[3];
		assert(cm.r == GL_FALSE && cm.g == GL_FALSE && cm.b == GL_TRUE && cm.a == GL_FALSE);

		unsigned char data[500];
		assert(stats.memory.transfers_cpu_size == 0);
		TEST_INVOKE(ReadnPixels, 10, 15, 5, 5, GL_DEPTH_STENCIL, GL_UNSIGNED_INT_24_8, 500, data);
		assert(stats.memory.transfers_cpu_size == 100);
		TEST_INVOKE(ReadPixels, 10, 15, 5, 5, GL_RGBA, GL_UNSIGNED_INT_8_8_8_8_REV, data);
		assert(stats.memory.transfers_cpu_size == 200);


		GLenum attachments[3] = { GL_DEPTH_ATTACHMENT , GL_COLOR_ATTACHMENT0, GL_COLOR_ATTACHMENT1 };
		TEST_INVOKE(InvalidateSubFramebuffer, GL_READ_FRAMEBUFFER, 3, attachments, 5, 5, 50, 50);
		TEST_INVOKE(InvalidateNamedFramebufferSubData, framebuffer[0], 3, attachments, 2, 2, 33, 12);
		TEST_INVOKE(InvalidateFramebuffer, GL_READ_FRAMEBUFFER, 3, attachments);
		TEST_INVOKE(InvalidateNamedFramebufferData, framebuffer[0], 3, attachments);

		TEST_INVOKE(DeleteTextures, 10, texture);
		TEST_INVOKE(DeleteFramebuffers, 2, framebuffer);
		TEST_INVOKE(DeleteTextures, 5, tex);
		TEST_INVOKE(DeleteFramebuffers, 5, fbo);
		TEST_INVOKE(DeleteRenderbuffers, 5, rbo);

		assert(stats.commands.all == 170);
		assert(stats.commands.framebuffer == 135);
		assert(stats.commands.redundant == 47);
		assert(stats.state_changes == 34);
		assert(stats.objects_created == 31);
		assert(stats.objects_destroyed == 31);
		assert(stats.memory.allocated == 1259952);
		assert(stats.memory.allocations == 19);
		assert(stats.memory.deallocations == 19);
		assert(stats.memory.deallocated == 1259952);

		totalstats.add(dc.getStats());
	}

	if (check_image) {
		std::cout << "***********************************************************************************************" << std::endl;
		std::cout << "******************************************* IMAGE *********************************************" << std::endl;
		std::cout << "***********************************************************************************************" << std::endl;
		dc.resetStats();

		GLuint badids[5] = { 0,0,0,0,0 };
		GLuint tex[5];
		TEST_INVOKE(CreateTextures, GL_TEXTURE_2D, 5, tex);
		assert(state.texture.textures.size() == 5);
		for (int i = 0; i < 5; i++) {
			TEST_INVOKE(TextureStorage2D, tex[i], 1, GL_R8, 100, 100);
		}
	
		const auto& p = state.image.unit;
		TEST_INVOKE_REDUNDANTLY(BindImageTexture, 2, tex[3], 0, GL_TRUE, 3, GL_READ_ONLY, GL_R8);
		assert(p[2].texture == tex[3] && p[2].access == GL_READ_ONLY && p[2].format == GL_R8 && p[2].layer == 3 && p[2].layered == GL_TRUE && p[2].level == 0);
		TEST_INVOKE_REDUNDANTLY(BindImageTextures, 0, 5, tex);
		for (int i = 0; i < 5; i++)	assert(p[i].texture == tex[i]);
		TEST_INVOKE_REDUNDANTLY(BindImageTexture, 2, 0, 0, GL_TRUE, 3, GL_READ_ONLY, GL_R8);
		assert(p[2].texture == 0&& p[2].access == GL_READ_ONLY && p[2].format == GL_R8 && p[2].layer == 3 && p[2].layered == GL_TRUE && p[2].level == 0);
		TEST_INVOKE_REDUNDANTLY(BindImageTextures, 0, 5, badids);
		for (int i = 0; i < 5; i++)	assert(p[i].texture == 0);

		TEST_INVOKE(DeleteTextures, 5, tex);
		assert(state.texture.textures.size() == 0);
		TEST_INVOKE(DeleteTextures, 5, badids);

		assert(stats.commands.all == 16);
		assert(stats.commands.image == 8);
		assert(stats.commands.redundant == 4);
		assert(stats.state_changes == 8);

		totalstats.add(dc.getStats());
	}
	if (check_query) {
		std::cout << "***********************************************************************************************" << std::endl;
		std::cout << "******************************************* QUERY *********************************************" << std::endl;
		std::cout << "***********************************************************************************************" << std::endl;
		dc.resetStats();
		GLuint ids[4]; GLuint timequery;
		TEST_INVOKE(GenQueries, 3, ids);
		assert(state.queries.queries.size() == 3);
		TEST_INVOKE(DeleteQueries, 3, ids);
		assert(state.queries.queries.size() == 0);
		TEST_INVOKE(CreateQueries, GL_ANY_SAMPLES_PASSED, 1, &ids[0]);
		assert(state.queries.queries.at(ids[0]).target == GL_ANY_SAMPLES_PASSED);
		TEST_INVOKE(CreateQueries, GL_TRANSFORM_FEEDBACK_PRIMITIVES_WRITTEN, 1, &ids[1]);
		assert(state.queries.queries.at(ids[1]).target == GL_TRANSFORM_FEEDBACK_PRIMITIVES_WRITTEN);
		TEST_INVOKE(CreateQueries, GL_TRANSFORM_FEEDBACK_PRIMITIVES_WRITTEN, 1, &ids[2]);
		assert(state.queries.queries.at(ids[2]).target == GL_TRANSFORM_FEEDBACK_PRIMITIVES_WRITTEN);
		TEST_INVOKE(CreateQueries, GL_PRIMITIVES_GENERATED, 1, &ids[3]);
		assert(state.queries.queries.at(ids[3]).target == GL_PRIMITIVES_GENERATED);
		TEST_INVOKE(BeginConditionalRender, ids[0], GL_QUERY_WAIT);
		assert(state.queries.conditional_rendering.enabled == GL_TRUE && state.queries.conditional_rendering.mode == GL_QUERY_WAIT && state.queries.conditional_rendering.query == ids[0]);
		TEST_INVOKE(EndConditionalRender);
		assert(state.queries.conditional_rendering.enabled == GL_FALSE && state.queries.conditional_rendering.mode == GL_NONE && state.queries.conditional_rendering.query == 0);
		TEST_INVOKE(BeginQuery, GL_PRIMITIVES_GENERATED, ids[3]);
		assert(state.queries.PRIMITIVES_GENERATED[0] == ids[3]);
		TEST_INVOKE(BeginQuery, GL_ANY_SAMPLES_PASSED, ids[0]);
		assert(state.queries.ANY_SAMPLES_PASSED == ids[0]);
		TEST_INVOKE(EndQuery, GL_PRIMITIVES_GENERATED)
		assert(state.queries.PRIMITIVES_GENERATED[0] == 0);
		TEST_INVOKE(EndQuery, GL_ANY_SAMPLES_PASSED);
		assert(state.queries.ANY_SAMPLES_PASSED == 0);
		TEST_INVOKE(BeginQueryIndexed, GL_TRANSFORM_FEEDBACK_PRIMITIVES_WRITTEN, 1, ids[1]);
		assert(state.queries.TRANSFORM_FEEDBACK_PRIMITIVES_WRITTEN[1] == ids[1]);
		TEST_INVOKE(BeginQueryIndexed, GL_TRANSFORM_FEEDBACK_PRIMITIVES_WRITTEN, 3, ids[2]);
		assert(state.queries.TRANSFORM_FEEDBACK_PRIMITIVES_WRITTEN[3] == ids[2]);
		TEST_INVOKE(EndQueryIndexed, GL_TRANSFORM_FEEDBACK_PRIMITIVES_WRITTEN, 1);
		assert(state.queries.TRANSFORM_FEEDBACK_PRIMITIVES_WRITTEN[1] == 0);
		TEST_INVOKE(EndQueryIndexed, GL_TRANSFORM_FEEDBACK_PRIMITIVES_WRITTEN, 3);
		assert(state.queries.TRANSFORM_FEEDBACK_PRIMITIVES_WRITTEN[3] == 0);
		TEST_INVOKE(DeleteQueries, 4, ids);
		assert(state.queries.queries.size() == 0);
		TEST_INVOKE(GenQueries, 1, &timequery);
		TEST_INVOKE(QueryCounter, timequery, GL_TIMESTAMP);
		TEST_INVOKE(DeleteQueries, 1, &timequery);

		GLuint badids[3] = { 0, 0, 0 };
		TEST_INVOKE(DeleteQueries, 3, badids);

		assert(stats.commands.all == 21);
		assert(stats.commands.query == 21);
		assert(stats.commands.redundant == 0);
		assert(stats.state_changes == 10);
		assert(stats.objects_created == 8);
		assert(stats.objects_destroyed == 8);
		totalstats.add(dc.getStats());
	}
	if (check_pixel_store) {
		std::cout << "***********************************************************************************************" << std::endl;
		std::cout << "******************************************* PIXEL STORE ***************************************" << std::endl;
		std::cout << "***********************************************************************************************" << std::endl;
		dc.resetStats();
		TEST_INVOKE_REDUNDANTLY(PixelStoref, GL_PACK_SWAP_BYTES, GL_TRUE);
		assert(state.pixel_storage.pack_swap_bytes == GL_TRUE);
		TEST_INVOKE_REDUNDANTLY(PixelStorei, GL_PACK_LSB_FIRST, GL_TRUE);
		assert(state.pixel_storage.pack_lsb_first == GL_TRUE);
		TEST_INVOKE_REDUNDANTLY(PixelStoref, GL_PACK_ROW_LENGTH, 12.0f);
		assert(state.pixel_storage.pack_row_length == 12);
		TEST_INVOKE_REDUNDANTLY(PixelStorei, GL_PACK_IMAGE_HEIGHT, 12);
		assert(state.pixel_storage.pack_image_height == 12);
		TEST_INVOKE_REDUNDANTLY(PixelStoref, GL_PACK_SKIP_PIXELS, 8);
		assert(state.pixel_storage.pack_skip_pixels == 8);
		TEST_INVOKE_REDUNDANTLY(PixelStorei, GL_PACK_SKIP_ROWS, 4);
		assert(state.pixel_storage.pack_skip_rows == 4);
		TEST_INVOKE_REDUNDANTLY(PixelStoref, GL_PACK_SKIP_IMAGES, 12);
		assert(state.pixel_storage.pack_skip_images == 12);
		TEST_INVOKE_REDUNDANTLY(PixelStorei, GL_PACK_ALIGNMENT, 1);
		assert(state.pixel_storage.pack_alignment == 1);
		TEST_INVOKE_REDUNDANTLY(PixelStoref, GL_UNPACK_SWAP_BYTES, GL_TRUE);
		assert(state.pixel_storage.unpack_swap_bytes == GL_TRUE);
		TEST_INVOKE_REDUNDANTLY(PixelStorei, GL_UNPACK_LSB_FIRST, GL_TRUE);
		assert(state.pixel_storage.unpack_lsb_first == GL_TRUE);
		TEST_INVOKE_REDUNDANTLY(PixelStoref, GL_UNPACK_ROW_LENGTH, 8);
		assert(state.pixel_storage.unpack_row_length == 8);
		TEST_INVOKE_REDUNDANTLY(PixelStorei, GL_UNPACK_IMAGE_HEIGHT, 4);
		assert(state.pixel_storage.unpack_image_height == 4);
		TEST_INVOKE_REDUNDANTLY(PixelStoref, GL_UNPACK_SKIP_PIXELS, 16);
		assert(state.pixel_storage.unpack_skip_pixels == 16);
		TEST_INVOKE_REDUNDANTLY(PixelStorei, GL_UNPACK_SKIP_ROWS, 4);
		assert(state.pixel_storage.unpack_skip_rows == 4);
		TEST_INVOKE_REDUNDANTLY(PixelStoref, GL_UNPACK_SKIP_IMAGES, 8);
		assert(state.pixel_storage.unpack_skip_images == 8);
		TEST_INVOKE_REDUNDANTLY(PixelStorei, GL_UNPACK_ALIGNMENT, 2);
		assert(state.pixel_storage.unpack_alignment == 2);

		TEST_INVOKE_REDUNDANTLY(PixelStoref, GL_PACK_SWAP_BYTES, GL_FALSE);
		TEST_INVOKE_REDUNDANTLY(PixelStorei, GL_PACK_LSB_FIRST, GL_FALSE);
		TEST_INVOKE_REDUNDANTLY(PixelStoref, GL_PACK_ROW_LENGTH, 0);
		TEST_INVOKE_REDUNDANTLY(PixelStorei, GL_PACK_IMAGE_HEIGHT, 0);
		TEST_INVOKE_REDUNDANTLY(PixelStoref, GL_PACK_SKIP_PIXELS, 0);
		TEST_INVOKE_REDUNDANTLY(PixelStorei, GL_PACK_SKIP_ROWS, 0);
		TEST_INVOKE_REDUNDANTLY(PixelStoref, GL_PACK_SKIP_IMAGES, 0);
		TEST_INVOKE_REDUNDANTLY(PixelStorei, GL_PACK_ALIGNMENT, 4);
		TEST_INVOKE_REDUNDANTLY(PixelStoref, GL_UNPACK_SWAP_BYTES, GL_FALSE);
		TEST_INVOKE_REDUNDANTLY(PixelStorei, GL_UNPACK_LSB_FIRST, GL_FALSE);
		TEST_INVOKE_REDUNDANTLY(PixelStoref, GL_UNPACK_ROW_LENGTH, 0);
		TEST_INVOKE_REDUNDANTLY(PixelStorei, GL_UNPACK_IMAGE_HEIGHT, 0);
		TEST_INVOKE_REDUNDANTLY(PixelStoref, GL_UNPACK_SKIP_PIXELS, 0);
		TEST_INVOKE_REDUNDANTLY(PixelStorei, GL_UNPACK_SKIP_ROWS, 0);
		TEST_INVOKE_REDUNDANTLY(PixelStoref, GL_UNPACK_SKIP_IMAGES, 0);
		TEST_INVOKE_REDUNDANTLY(PixelStorei, GL_UNPACK_ALIGNMENT, 4);

		assert(stats.commands.all == 64);
		assert(stats.commands.pixel_storage == 64);
		assert(stats.commands.redundant == 32);
		assert(stats.state_changes == 64);
		totalstats.add(dc.getStats());
	}
	if (check_rasterization) {
		std::cout << "***********************************************************************************************" << std::endl;
		std::cout << "******************************************* RASTERIZATION *************************************" << std::endl;
		std::cout << "***********************************************************************************************" << std::endl;
		dc.resetStats();
		TEST_INVOKE_REDUNDANTLY(LineWidth, 2.0f);
		assert(state.rasterization.line.width == 2.0f);
		
		TEST_INVOKE_REDUNDANTLY(MinSampleShading, 0.88f);
		assert(state.rasterization.sampling.sample_min_shading == 0.88f);

		TEST_INVOKE_REDUNDANTLY(PointParameterf, GL_POINT_FADE_THRESHOLD_SIZE,  2.0f);
		assert(state.rasterization.point.fade_threshold_size == 2.0f);
		TEST_INVOKE_REDUNDANTLY(PointParameterf, GL_POINT_SPRITE_COORD_ORIGIN, GL_LOWER_LEFT);
		assert(state.rasterization.point.sprite_coord_origin == GL_LOWER_LEFT);
		TEST_INVOKE_REDUNDANTLY(PointParameteri, GL_POINT_FADE_THRESHOLD_SIZE, 3);
		assert(state.rasterization.point.fade_threshold_size == 3);
		TEST_INVOKE_REDUNDANTLY(PointParameteri, GL_POINT_SPRITE_COORD_ORIGIN, GL_UPPER_LEFT);
		assert(state.rasterization.point.sprite_coord_origin == GL_UPPER_LEFT);
		float floatptr[2] = { 4.0f, GL_LOWER_LEFT };
		TEST_INVOKE_REDUNDANTLY(PointParameterfv, GL_POINT_FADE_THRESHOLD_SIZE, &floatptr[0]);
		assert(state.rasterization.point.fade_threshold_size == floatptr[0]);
		TEST_INVOKE_REDUNDANTLY(PointParameterfv, GL_POINT_SPRITE_COORD_ORIGIN, &floatptr[1]);
		assert(state.rasterization.point.sprite_coord_origin == floatptr[1]);
		int intptr[2] = { 3, GL_UPPER_LEFT };
		TEST_INVOKE_REDUNDANTLY(PointParameteriv, GL_POINT_FADE_THRESHOLD_SIZE, &intptr[0]);
		assert(state.rasterization.point.fade_threshold_size == intptr[0]);
		TEST_INVOKE_REDUNDANTLY(PointParameteriv, GL_POINT_SPRITE_COORD_ORIGIN, &intptr[1]);
		assert(state.rasterization.point.sprite_coord_origin == intptr[1]);

		TEST_INVOKE_REDUNDANTLY(PointSize, 3.0f);
		assert(state.rasterization.point.size == 3.0f);

		TEST_INVOKE_REDUNDANTLY(PolygonMode, GL_FRONT_AND_BACK, GL_LINE);
		assert(state.rasterization.polygons.mode == GL_LINE);
		TEST_INVOKE_REDUNDANTLY(PolygonMode, GL_FRONT_AND_BACK, GL_POINT);
		assert(state.rasterization.polygons.mode == GL_POINT);
		TEST_INVOKE_REDUNDANTLY(PolygonMode, GL_FRONT_AND_BACK, GL_FILL);
		assert(state.rasterization.polygons.mode == GL_FILL);

		TEST_INVOKE_REDUNDANTLY(PolygonOffset, 1, 2);
		assert(state.rasterization.depth_offset.factor == 1 && state.rasterization.depth_offset.units == 2);

		TEST_INVOKE_REDUNDANTLY(SampleCoverage, 0.66f, GL_TRUE);
		assert(state.rasterization.sampling.coverage_mask_percentage == 0.66f && state.rasterization.sampling.coverage_mask_inverted == GL_TRUE);
		TEST_INVOKE_REDUNDANTLY(SampleMaski, 1, 0b1010101011);
		assert(state.rasterization.sampling.sample_mask_words[1] == 0b1010101011);

		TEST_INVOKE_REDUNDANTLY(LogicOp, GL_CLEAR);
		assert(state.rasterization.logic.operation == GL_CLEAR);
		TEST_INVOKE_REDUNDANTLY(LogicOp, GL_SET);
		TEST_INVOKE_REDUNDANTLY(LogicOp, GL_COPY);
		TEST_INVOKE_REDUNDANTLY(LogicOp, GL_COPY_INVERTED);
		TEST_INVOKE_REDUNDANTLY(LogicOp, GL_NOOP);
		TEST_INVOKE_REDUNDANTLY(LogicOp, GL_INVERT);
		TEST_INVOKE_REDUNDANTLY(LogicOp, GL_AND);
		TEST_INVOKE_REDUNDANTLY(LogicOp, GL_NAND);
		TEST_INVOKE_REDUNDANTLY(LogicOp, GL_OR);
		TEST_INVOKE_REDUNDANTLY(LogicOp, GL_NOR);
		TEST_INVOKE_REDUNDANTLY(LogicOp, GL_XOR);
		TEST_INVOKE_REDUNDANTLY(LogicOp, GL_EQUIV);
		TEST_INVOKE_REDUNDANTLY(LogicOp, GL_AND_REVERSE);
		TEST_INVOKE_REDUNDANTLY(LogicOp, GL_AND_INVERTED);
		TEST_INVOKE_REDUNDANTLY(LogicOp, GL_OR_REVERSE);
		TEST_INVOKE_REDUNDANTLY(LogicOp, GL_OR_INVERTED);

		auto& srv = state.rasterization.viewports;
		TEST_INVOKE_REDUNDANTLY(Viewport, 10, 20, 150, 250);
		for (int i = 0; i < srv.size();i++) assert(srv[i].x == 10 && srv[i].y == 20 && srv[i].width == 150 && srv[i].height == 250);
		float v[16] = { 11, 12,13, 14, 15, 16, 17, 18, 99, 100, 101, 102, 103, 104, 105, 106 };
		TEST_INVOKE_REDUNDANTLY(ViewportArrayv, 2, 4, v);
		for (int i = 0; i < 4; i++) assert(srv[i + 2].x == v[i * 4] && srv[i + 2].y == v[i * 4 + 1] && srv[i + 2].width == v[i * 4 + 2] && srv[i + 2].height == v[i * 4 + 3]);
		TEST_INVOKE_REDUNDANTLY(ViewportIndexedf, 1,  20, 20, 150, 250);
		assert(srv[1].x == 20 && srv[1].y == 20 && srv[1].width == 150 && srv[1].height == 250);
		TEST_INVOKE_REDUNDANTLY(ViewportIndexedfv, 6, v);
		assert(srv[6].x == v[0] && srv[6].y == v[1] && srv[6].width == v[2] && srv[6].height == v[3]);

		assert(stats.commands.all == 74);
		assert(stats.commands.rasterization == 74);
		assert(stats.commands.redundant == 37);
		assert(stats.state_changes == 74);
		totalstats.add(dc.getStats());
	}
	if (check_sampler) {
		std::cout << "***********************************************************************************************" << std::endl;
		std::cout << "******************************************* SAMPLER *******************************************" << std::endl;
		std::cout << "***********************************************************************************************" << std::endl;
		dc.resetStats();
		GLuint ids[3]; GLuint ids2[3];
		TEST_INVOKE(CreateSamplers, 3, ids);
		assert(state.texture.samplers.size() == 3);
		TEST_INVOKE(GenSamplers, 3, ids2);
		assert(state.texture.samplers.size() == 6);
		TEST_INVOKE(DeleteSamplers, 3, ids2);
		assert(state.texture.samplers.size() == 3);

		TEST_INVOKE_REDUNDANTLY(BindSampler, 2, ids[1]);
		assert(state.texture.unit[2].sampler == ids[1]);
		TEST_INVOKE_REDUNDANTLY(BindSamplers, 4, 3, ids);
		assert(state.texture.unit[4].sampler == ids[0]);
		assert(state.texture.unit[5].sampler == ids[1]);
		assert(state.texture.unit[6].sampler == ids[2]);
		TEST_INVOKE_REDUNDANTLY(SamplerParameterf, ids[0], GL_TEXTURE_MIN_FILTER, GL_NEAREST);
		assert(state.texture.samplers.at(ids[0]).filter_min == GL_NEAREST);
		TEST_INVOKE_REDUNDANTLY(SamplerParameteri, ids[0], GL_TEXTURE_MAG_FILTER, GL_NEAREST);
		assert(state.texture.samplers.at(ids[0]).filter_mag == GL_NEAREST);
		TEST_INVOKE_REDUNDANTLY(SamplerParameterf, ids[0], GL_TEXTURE_MIN_LOD, -800);
		assert(state.texture.samplers.at(ids[0]).lod_min == -800);
		TEST_INVOKE_REDUNDANTLY(SamplerParameteri, ids[0], GL_TEXTURE_MAX_LOD, 432);
		assert(state.texture.samplers.at(ids[0]).lod_max == 432);
		TEST_INVOKE_REDUNDANTLY(SamplerParameterf, ids[0], GL_TEXTURE_WRAP_S, GL_MIRROR_CLAMP_TO_EDGE);
		assert(state.texture.samplers.at(ids[0]).wrap_s == GL_MIRROR_CLAMP_TO_EDGE);
		TEST_INVOKE_REDUNDANTLY(SamplerParameteri, ids[0], GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
		assert(state.texture.samplers.at(ids[0]).wrap_t == GL_CLAMP_TO_EDGE);
		TEST_INVOKE_REDUNDANTLY(SamplerParameteri, ids[0], GL_TEXTURE_WRAP_R, GL_MIRRORED_REPEAT);
		assert(state.texture.samplers.at(ids[0]).wrap_r == GL_MIRRORED_REPEAT);
		//TEST_INVOKE_REDUNDANTLY(SamplerParameteri, ids[0], GL_TEXTURE_BORDER_COLOR, 1);
		//assert(state.texture.samplers.at(ids[0]).bordercolor.r == 1);

		GLint icomparemode = GL_COMPARE_REF_TO_TEXTURE;
		TEST_INVOKE_REDUNDANTLY(SamplerParameterIiv, ids[0], GL_TEXTURE_COMPARE_MODE, &icomparemode);
		assert(state.texture.samplers.at(ids[0]).compare_mode == icomparemode);
		GLuint uicomparefunc = GL_NOTEQUAL;
		TEST_INVOKE_REDUNDANTLY(SamplerParameterIuiv, ids[0], GL_TEXTURE_COMPARE_FUNC, &uicomparefunc);
		assert(state.texture.samplers.at(ids[0]).compare_func == uicomparefunc);
		
		GLfloat fcolor[4] = { 0.1f, 0.2f, 0.3f, 0.4f };
		GLint icolor[4] = { -1,2,-3,4 };
		GLuint uicolor[4] = { 3,4,2,1 };
		TEST_INVOKE_REDUNDANTLY(SamplerParameterfv, ids[0], GL_TEXTURE_BORDER_COLOR, fcolor);
		assert(state.texture.samplers.at(ids[0]).bordercolor.r == fcolor[0]);
		assert(state.texture.samplers.at(ids[0]).bordercolor.g == fcolor[1]);
		assert(state.texture.samplers.at(ids[0]).bordercolor.b == fcolor[2]);
		assert(state.texture.samplers.at(ids[0]).bordercolor.a == fcolor[3]);
		TEST_INVOKE_REDUNDANTLY(SamplerParameterIiv, ids[0], GL_TEXTURE_BORDER_COLOR, icolor);
		assert(state.texture.samplers.at(ids[0]).bordercolor.r == icolor[0]);
		assert(state.texture.samplers.at(ids[0]).bordercolor.g == icolor[1]);
		assert(state.texture.samplers.at(ids[0]).bordercolor.b == icolor[2]);
		assert(state.texture.samplers.at(ids[0]).bordercolor.a == icolor[3]);
		TEST_INVOKE_REDUNDANTLY(SamplerParameterIuiv, ids[0], GL_TEXTURE_BORDER_COLOR, uicolor);
		assert(state.texture.samplers.at(ids[0]).bordercolor.r == uicolor[0]);
		assert(state.texture.samplers.at(ids[0]).bordercolor.g == uicolor[1]);
		assert(state.texture.samplers.at(ids[0]).bordercolor.b == uicolor[2]);
		assert(state.texture.samplers.at(ids[0]).bordercolor.a == uicolor[3]);
		TEST_INVOKE_REDUNDANTLY(SamplerParameteriv, ids[0], GL_TEXTURE_BORDER_COLOR, icolor);
		assert(state.texture.samplers.at(ids[0]).bordercolor.r == icolor[0]);
		assert(state.texture.samplers.at(ids[0]).bordercolor.g == icolor[1]);
		assert(state.texture.samplers.at(ids[0]).bordercolor.b == icolor[2]);
		assert(state.texture.samplers.at(ids[0]).bordercolor.a == icolor[3]);
		
		TEST_INVOKE(DeleteSamplers, 3, ids);
		assert(state.texture.samplers.size() == 0);

		TEST_INVOKE(BindSampler, 3, 0);
		GLuint badids[3] = { 0, 0, 0};
		TEST_INVOKE(DeleteSamplers, 3, badids);


		assert(stats.commands.all == 36);
		assert(stats.commands.sampler == 36);
		assert(stats.commands.redundant == 16);
		assert(stats.state_changes == 5);
		assert(stats.objects_created == 6);
		assert(stats.objects_destroyed == 6);
		totalstats.add(dc.getStats());
	}
	if (check_scissor) {
		std::cout << "***********************************************************************************************" << std::endl;
		std::cout << "******************************************* SCISSOR *******************************************" << std::endl;
		std::cout << "***********************************************************************************************" << std::endl;
		dc.resetStats();
		const auto& sv = state.scissor.viewports;
		TEST_INVOKE_REDUNDANTLY(Scissor, 3, 6, 235, 329);
		for (int i = 0; i < sv.size(); i++) assert(sv[i].x == 3 && sv[i].y == 6 && sv[i].width == 235 && sv[i].height == 329);
		GLint data[12] = { 5,6,23,43, 43,9,32,843, 32, 4, 43,92 };
		TEST_INVOKE_REDUNDANTLY(ScissorArrayv, 5, 3, data);
		for (int i = 0; i < 3; i++) assert(sv[i + 5].x == data[4 * i] && sv[i + 5].y == data[4 * i + 1] && sv[i + 5].width == data[4 * i + 2] && sv[i + 5].height == data[4 * i + 3]);
		TEST_INVOKE_REDUNDANTLY(ScissorIndexed, 4, 55, 43, 90, 180);
		assert(sv[4].x == 55 && sv[4].y == 43 && sv[4].width == 90 && sv[4].height == 180);
		GLint v[4] = { 65, 32, 654, 923 };
		TEST_INVOKE_REDUNDANTLY(ScissorIndexedv, 9, v);
		assert(sv[9].x == v[0] && sv[9].y == v[1] && sv[9].width == v[2] && sv[9].height == v[3]);
		assert(stats.commands.all == 8);
		assert(stats.commands.scissor == 8);
		assert(stats.commands.redundant == 4);
		assert(stats.state_changes == 8);
		totalstats.add(dc.getStats());
	}
	if (check_shader) {
		std::cout << "***********************************************************************************************" << std::endl;
		std::cout << "******************************************* SHADER ********************************************" << std::endl;
		std::cout << "***********************************************************************************************" << std::endl;
		dc.resetStats();
		
		std::string stringv = "#version 450\nvoid main(){gl_Position = vec4(1,0,0,0);}";
		std::string stringtc = "#version 450\nlayout(vertices=1) out;\nvoid main(){gl_out[gl_InvocationID].gl_Position = gl_in[gl_InvocationID].gl_Position;}";
		std::string stringte = "#version 450\nlayout(triangles, equal_spacing, ccw) in;void main(){ gl_Position = vec4(1,0,0,0);}";
		std::string stringg = "#version 450\n layout(points) in;\n layout(points, max_vertices = 3) out; void main(){gl_Position = vec4(1,0,0,0); EmitVertex();}";
		std::string stringf = "#version 450\nlayout(location = 0) out vec3 o1;\n layout(location = 1) out vec3 o2;\n void main(){o1 = o2 = vec3(1,0,0);}";
		std::string stringc = "#version 450\n layout(local_size_x = 8, local_size_y = 8, local_size_z = 8) in; \n void main(){ }\n";

		GLuint pipeid[6], progid[4], shader_id[6];
		TEST_INVOKE(GenProgramPipelines, 3, pipeid);
		assert(state.program.program_pipelines.size() == 3);
		TEST_INVOKE(CreateProgramPipelines, 3, &pipeid[3]);
		assert(state.program.program_pipelines.size() == 6);
		TEST_INVOKE_REDUNDANTLY(BindProgramPipeline, pipeid[3]);
		assert(state.program.current_program_pipeline == pipeid[3]);
		TEST_INVOKE_REDUNDANTLY(BindProgramPipeline, 0);
		assert(state.program.current_program_pipeline == 0);
		
		progid[0] = TEST_INVOKE(CreateProgram);
		progid[1] = TEST_INVOKE(CreateProgram);

		TEST_INVOKE_REDUNDANTLY(ProgramParameteri, progid[0], GL_PROGRAM_BINARY_RETRIEVABLE_HINT, GL_TRUE);
		assert(state.program.programs.at(progid[0]).binary_retrievable_hint == GL_TRUE);
		TEST_INVOKE_REDUNDANTLY(ProgramParameteri, progid[0], GL_PROGRAM_BINARY_RETRIEVABLE_HINT, GL_FALSE);
		TEST_INVOKE_REDUNDANTLY(ProgramParameteri, progid[0], GL_PROGRAM_SEPARABLE, GL_TRUE);
		assert(state.program.programs.at(progid[0]).separable == GL_TRUE);
		TEST_INVOKE_REDUNDANTLY(ProgramParameteri, progid[0], GL_PROGRAM_SEPARABLE, GL_FALSE);

		assert(state.program.programs.size() == 2);
		const char* strings[5] = { stringv.c_str(), stringtc.c_str(), stringte.c_str(), stringg.c_str(), stringf.c_str()};
		const char* strings2[1] = { stringc.c_str() };
		progid[2] = TEST_INVOKE(CreateShaderProgramv, GL_GEOMETRY_SHADER, 1, &strings[3]);
		progid[3] = TEST_INVOKE(CreateShaderProgramv, GL_COMPUTE_SHADER, 1, strings2);

		shader_id[0] = TEST_INVOKE(CreateShader, GL_VERTEX_SHADER);
		assert(state.program.shaders.at(shader_id[0]).id == shader_id[0] && state.program.shaders.at(shader_id[0]).type == GL_VERTEX_SHADER);
		shader_id[1] = TEST_INVOKE(CreateShader, GL_TESS_CONTROL_SHADER);
		assert(state.program.shaders.at(shader_id[1]).id == shader_id[1] && state.program.shaders.at(shader_id[1]).type == GL_TESS_CONTROL_SHADER);
		shader_id[2] = TEST_INVOKE(CreateShader, GL_TESS_EVALUATION_SHADER);
		assert(state.program.shaders.at(shader_id[2]).id == shader_id[2] && state.program.shaders.at(shader_id[2]).type == GL_TESS_EVALUATION_SHADER);
		shader_id[3] = TEST_INVOKE(CreateShader, GL_GEOMETRY_SHADER);
		assert(state.program.shaders.at(shader_id[3]).id == shader_id[3] && state.program.shaders.at(shader_id[3]).type == GL_GEOMETRY_SHADER);
		shader_id[4] = TEST_INVOKE(CreateShader, GL_FRAGMENT_SHADER);
		assert(state.program.shaders.at(shader_id[4]).id == shader_id[4] && state.program.shaders.at(shader_id[4]).type == GL_FRAGMENT_SHADER);
		shader_id[5] = TEST_INVOKE(CreateShader, GL_COMPUTE_SHADER);
		assert(state.program.shaders.at(shader_id[5]).id == shader_id[5] && state.program.shaders.at(shader_id[5]).type == GL_COMPUTE_SHADER);
		assert(state.program.shaders.size() == 6);

		auto funcshadersource = [&](GLuint shaderid, std::string& source) {
			GLint auxsize = (GLint)source.size();
			const char* auxstr = source.c_str();
			TEST_INVOKE(ShaderSource, shaderid, 1, &auxstr, &auxsize);
		};
		funcshadersource(shader_id[0], stringv);
		funcshadersource(shader_id[1], stringtc);
		funcshadersource(shader_id[2], stringte);
		funcshadersource(shader_id[3], stringg);
		funcshadersource(shader_id[4], stringf);
		funcshadersource(shader_id[5], stringc);
		
		TEST_INVOKE(CompileShader, shader_id[0]);
		assert(state.program.shaders.at(shader_id[0]).compile_status == GL_TRUE);
		TEST_INVOKE(CompileShader, shader_id[1]);
		assert(state.program.shaders.at(shader_id[1]).compile_status == GL_TRUE);
		TEST_INVOKE(CompileShader, shader_id[2]);
		assert(state.program.shaders.at(shader_id[2]).compile_status == GL_TRUE);
		TEST_INVOKE(CompileShader, shader_id[3]);
		assert(state.program.shaders.at(shader_id[3]).compile_status == GL_TRUE);
		TEST_INVOKE(CompileShader, shader_id[4]);
		assert(state.program.shaders.at(shader_id[4]).compile_status == GL_TRUE);
		TEST_INVOKE(CompileShader, shader_id[5]);
		assert(state.program.shaders.at(shader_id[5]).compile_status == GL_TRUE);

		TEST_INVOKE(AttachShader, progid[0], shader_id[0]);
		assert(state.program.programs.at(progid[0]).attached_shaders[0] == shader_id[0]);
		TEST_INVOKE(AttachShader, progid[0], shader_id[1]);
		assert(state.program.programs.at(progid[0]).attached_shaders[1] == shader_id[1]);
		TEST_INVOKE(AttachShader, progid[0], shader_id[2]);
		assert(state.program.programs.at(progid[0]).attached_shaders[2] == shader_id[2]);
		TEST_INVOKE(AttachShader, progid[0], shader_id[3]);
		assert(state.program.programs.at(progid[0]).attached_shaders[3] == shader_id[3]);
		TEST_INVOKE(AttachShader, progid[0], shader_id[4]);
		assert(state.program.programs.at(progid[0]).attached_shaders[4] == shader_id[4]);
		TEST_INVOKE(DetachShader, progid[0], shader_id[0]);
		assert(state.program.programs.at(progid[0]).attached_shaders.size() == 4);
		TEST_INVOKE(DetachShader, progid[0], shader_id[1]);
		assert(state.program.programs.at(progid[0]).attached_shaders.size() == 3);
		TEST_INVOKE(DetachShader, progid[0], shader_id[2]);
		assert(state.program.programs.at(progid[0]).attached_shaders.size() == 2);
		TEST_INVOKE(DetachShader, progid[0], shader_id[3]);
		assert(state.program.programs.at(progid[0]).attached_shaders.size() == 1);
		TEST_INVOKE(DetachShader, progid[0], shader_id[4]);
		assert(state.program.programs.at(progid[0]).attached_shaders.size() == 0);
		assert(state.program.programs.at(progid[0]).has_vertex_shader == GL_FALSE);
		assert(state.program.programs.at(progid[0]).has_tessellation_control_shader == GL_FALSE);
		assert(state.program.programs.at(progid[0]).has_tessellation_evaluation_shader == GL_FALSE);
		assert(state.program.programs.at(progid[0]).has_geometry_shader == GL_FALSE);
		assert(state.program.programs.at(progid[0]).has_fragment_shader == GL_FALSE);
		assert(state.program.programs.at(progid[0]).has_compute_shader == GL_FALSE);
		TEST_INVOKE(AttachShader, progid[0], shader_id[0]);
		TEST_INVOKE(AttachShader, progid[0], shader_id[1]);
		TEST_INVOKE(AttachShader, progid[0], shader_id[2]);
		TEST_INVOKE(AttachShader, progid[0], shader_id[3]);
		TEST_INVOKE(AttachShader, progid[0], shader_id[4]);
		assert(state.program.programs.at(progid[0]).has_vertex_shader == GL_TRUE);
		assert(state.program.programs.at(progid[0]).has_tessellation_control_shader == GL_TRUE);
		assert(state.program.programs.at(progid[0]).has_tessellation_evaluation_shader == GL_TRUE);
		assert(state.program.programs.at(progid[0]).has_geometry_shader == GL_TRUE);
		assert(state.program.programs.at(progid[0]).has_fragment_shader == GL_TRUE);
		TEST_INVOKE(AttachShader, progid[1], shader_id[5]);
		assert(state.program.programs.at(progid[1]).has_compute_shader == GL_TRUE);
		assert(state.program.programs.at(progid[1]).attached_shaders[0] == shader_id[5]);


		TEST_INVOKE_REDUNDANTLY(LinkProgram, progid[0]);
		assert(state.program.programs.at(progid[0]).link_status == GL_TRUE);
		TEST_INVOKE_REDUNDANTLY(LinkProgram, progid[1]);
		assert(state.program.programs.at(progid[1]).link_status == GL_TRUE);

		TEST_INVOKE(ValidateProgram, progid[0]);
		assert(state.program.programs.at(progid[0]).validate_status == GL_TRUE);
		TEST_INVOKE(ValidateProgram, progid[1]);
		assert(state.program.programs.at(progid[1]).validate_status == GL_TRUE);

		TEST_INVOKE_REDUNDANTLY(BindAttribLocation, progid[0], 2, "some apples");
		assert(state.program.programs.at(progid[0]).bindings.attribute_index_to_variables[2].at(0) == "some apples");
		TEST_INVOKE_REDUNDANTLY(BindAttribLocation, progid[0], 4, "some apples");
		assert(state.program.programs.at(progid[0]).bindings.attribute_index_to_variables[2].size() == 0);
		assert(state.program.programs.at(progid[0]).bindings.attribute_index_to_variables[4].at(0) == "some apples");
		TEST_INVOKE_REDUNDANTLY(BindAttribLocation, progid[0], 4, "some pears");
		assert(state.program.programs.at(progid[0]).bindings.attribute_index_to_variables[4].at(1) == "some pears");
		TEST_INVOKE_REDUNDANTLY(BindAttribLocation, progid[0], 1, "one");
		assert(state.program.programs.at(progid[0]).bindings.attribute_index_to_variables[1].at(0) == "one");

		TEST_INVOKE_REDUNDANTLY(BindFragDataLocation, progid[0], 2, "o2");
		assert(state.program.programs.at(progid[0]).bindings.color_index_to_varying[0][2] == "o2");
		//TEST_INVOKE_REDUNDANTLY(BindFragDataLocationIndexed, progid[0], 1, 1, "o1");
		//assert(state.program.programs.at(progid[0]).bindings.color_index_to_varying[0][2] == "o1");

		//TEST_INVOKE_REDUNDANTLY(ShaderStorageBlockBinding, progid[0], 1, 2);
		//assert(state.program.programs.at(progid[0]).bindings.shader_storage_index_to_local_block[1] == 2);
		//TEST_INVOKE_REDUNDANTLY(ShaderStorageBlockBinding, progid[0], 0, 1);
		//assert(state.program.programs.at(progid[0]).bindings.shader_storage_index_to_local_block[0] == 1);
		
		TEST_INVOKE_REDUNDANTLY(UseProgram, progid[0]);
		assert(state.program.current_program == progid[0]);
		TEST_INVOKE_REDUNDANTLY(UseProgram, 0);
		assert(state.program.current_program == 0);
		callback_crash = false;
		TEST_INVOKE_REDUNDANTLY(UseProgramStages, pipeid[0], GL_VERTEX_SHADER_BIT | GL_FRAGMENT_SHADER_BIT | GL_GEOMETRY_SHADER_BIT | GL_TESS_CONTROL_SHADER_BIT | GL_TESS_EVALUATION_SHADER_BIT, progid[0]);
		assert(state.program.program_pipelines.at(pipeid[0]).vertex_shader_program == progid[0]);
		assert(state.program.program_pipelines.at(pipeid[0]).fragment_shader_program == progid[0]);
		assert(state.program.program_pipelines.at(pipeid[0]).tess_control_shader_program == progid[0]);
		assert(state.program.program_pipelines.at(pipeid[0]).tess_eval_shader_program == progid[0]);
		assert(state.program.program_pipelines.at(pipeid[0]).geometry_shader_program == progid[0]);
		TEST_INVOKE_REDUNDANTLY(ValidateProgramPipeline, pipeid[0]);
		//assert(state.program.program_pipelines.at(pipeid[0]).validate_status == GL_TRUE);

		TEST_INVOKE_REDUNDANTLY(UseProgramStages, pipeid[1], GL_COMPUTE_SHADER_BIT, progid[1]);
		assert(state.program.program_pipelines.at(pipeid[1]).compute_shader_program == progid[1]);
		TEST_INVOKE_REDUNDANTLY(ValidateProgramPipeline, pipeid[1]);
		//assert(state.program.program_pipelines.at(pipeid[1]).validate_status == GL_TRUE);
		
		TEST_INVOKE_REDUNDANTLY(ActiveShaderProgram, pipeid[0], progid[0]);
		assert(state.program.program_pipelines.at(pipeid[0]).active_program == progid[0]);
		callback_crash = true;
		TEST_INVOKE(DeleteShader, shader_id[0]);
		TEST_INVOKE(DeleteShader, shader_id[1]);
		TEST_INVOKE(DeleteShader, shader_id[2]);
		TEST_INVOKE(DeleteShader, shader_id[3]);
		TEST_INVOKE(DeleteShader, shader_id[4]);
		TEST_INVOKE(DeleteShader, shader_id[5]);
		assert(state.program.shaders.size() == 0);
			
		TEST_INVOKE_REDUNDANTLY(ReleaseShaderCompiler);

		TEST_INVOKE(DeleteProgram, progid[0]);
		TEST_INVOKE(DeleteProgram, progid[1]);
		TEST_INVOKE(DeleteProgram, progid[2]);
		TEST_INVOKE(DeleteProgram, progid[3]);
		assert(state.program.programs.size() == 0);
		TEST_INVOKE(DeleteProgramPipelines, 6, pipeid);

		assert(state.program.program_pipelines.size() == 0);
		assert(state.program.current_program_pipeline == 0);

		TEST_INVOKE(DeleteShader, 0);
		TEST_INVOKE(DeleteProgram, 0);
		GLuint badid[5] = { 0,0, 0, 0,0 };
		TEST_INVOKE(DeleteProgramPipelines, 5, badid);
		
		assert(stats.commands.all == 98);
		assert(stats.commands.program_shader == 98);
		assert(stats.commands.redundant == 16);
		assert(stats.objects_created == 16);
		assert(stats.objects_destroyed == 16);
		assert(stats.state_changes == 14);
		assert(stats.pipeline_flushes == 8);
		assert(stats.program_changes == 8);
		assert(stats.errors == 4);
		totalstats.add(dc.getStats());
	}
	if (check_stencil) {
		std::cout << "***********************************************************************************************" << std::endl;
		std::cout << "******************************************* STENCIL *******************************************" << std::endl;
		std::cout << "***********************************************************************************************" << std::endl;
		dc.resetStats();
		const auto& ss = state.stencil;
		TEST_INVOKE_REDUNDANTLY(ClearStencil, 128);
		assert(ss.clear_value == 128);
		TEST_INVOKE_REDUNDANTLY(StencilFunc, GL_GREATER, 256, 0);
		assert(ss.front_facing.func == GL_GREATER && ss.front_facing.func_ref == 256 && ss.front_facing.func_mask == 0);
		assert(ss.back_facing.func == GL_GREATER && ss.back_facing.func_ref == 256 && ss.back_facing.func_mask ==0);
		TEST_INVOKE_REDUNDANTLY(StencilFuncSeparate, GL_FRONT, GL_EQUAL, 214, 32);
		assert(ss.front_facing.func == GL_EQUAL && ss.front_facing.func_ref == 214 && ss.front_facing.func_mask == 32);
		TEST_INVOKE_REDUNDANTLY(StencilFuncSeparate, GL_BACK, GL_LESS, 232, 55);
		assert(ss.back_facing.func == GL_LESS && ss.back_facing.func_ref == 232 && ss.back_facing.func_mask == 55);
		TEST_INVOKE_REDUNDANTLY(StencilFuncSeparate, GL_FRONT_AND_BACK, GL_GREATER, 256, 0);
		assert(ss.front_facing.func == GL_GREATER && ss.front_facing.func_ref == 256 && ss.front_facing.func_mask == 0);
		assert(ss.back_facing.func == GL_GREATER && ss.back_facing.func_ref == 256 && ss.back_facing.func_mask == 0);
		
		TEST_INVOKE_REDUNDANTLY(StencilMask, 36266);
		assert(ss.front_facing.write_mask == 36266 && ss.back_facing.write_mask == 36266);
		TEST_INVOKE_REDUNDANTLY(StencilMaskSeparate, GL_FRONT, 78237);
		assert(ss.front_facing.write_mask == 78237);
		TEST_INVOKE_REDUNDANTLY(StencilMaskSeparate, GL_BACK, 79937);
		assert(ss.back_facing.write_mask == 79937);
		TEST_INVOKE_REDUNDANTLY(StencilMaskSeparate, GL_FRONT_AND_BACK, 26363);
		assert(ss.front_facing.write_mask == 26363 && ss.back_facing.write_mask == 26363);
		
		TEST_INVOKE_REDUNDANTLY(StencilOp, GL_INCR, GL_ZERO, GL_DECR_WRAP);
		assert(ss.front_facing.sfail == GL_INCR && ss.back_facing.sfail == GL_INCR);
		assert(ss.front_facing.dpfail == GL_ZERO && ss.back_facing.dpfail == GL_ZERO);
		assert(ss.front_facing.dppass == GL_DECR_WRAP && ss.back_facing.dppass == GL_DECR_WRAP);

		TEST_INVOKE_REDUNDANTLY(StencilOpSeparate, GL_FRONT, GL_KEEP, GL_REPLACE, GL_ZERO);
		assert(ss.front_facing.sfail == GL_KEEP && ss.front_facing.dpfail == GL_REPLACE && ss.front_facing.dppass == GL_ZERO);
		TEST_INVOKE_REDUNDANTLY(StencilOpSeparate, GL_BACK, GL_KEEP, GL_REPLACE, GL_ZERO);
		assert(ss.back_facing.sfail == GL_KEEP && ss.back_facing.dpfail == GL_REPLACE && ss.back_facing.dppass == GL_ZERO);
		
		TEST_INVOKE_REDUNDANTLY(StencilOpSeparate, GL_FRONT_AND_BACK, GL_INVERT, GL_KEEP, GL_REPLACE);
		assert(ss.front_facing.sfail == GL_INVERT && ss.back_facing.sfail == GL_INVERT);
		assert(ss.front_facing.dpfail == GL_KEEP && ss.back_facing.dpfail == GL_KEEP);
		assert(ss.front_facing.dppass == GL_REPLACE && ss.back_facing.dppass == GL_REPLACE);

		assert(stats.commands.all == 26);
		assert(stats.commands.stencil == 26);
		assert(stats.commands.redundant == 13);
		assert(stats.state_changes == 26);
		totalstats.add(dc.getStats());
	}
	if (check_synchronization) {
		std::cout << "***********************************************************************************************" << std::endl;
		std::cout << "******************************************* SYNCHRONIZATION ***********************************" << std::endl;
		std::cout << "***********************************************************************************************" << std::endl;
		dc.resetStats();
		GLsync sync = TEST_INVOKE(FenceSync, GL_SYNC_GPU_COMMANDS_COMPLETE, 0);
		assert(state.sync.sync_objects.size() == 1);
		TEST_INVOKE(DeleteSync, sync);
		assert(state.sync.sync_objects.size() == 0);
		sync = TEST_INVOKE(FenceSync, GL_SYNC_GPU_COMMANDS_COMPLETE, 0);
		TEST_INVOKE(ClientWaitSync, sync, GL_SYNC_FLUSH_COMMANDS_BIT, 1000);
		TEST_INVOKE(WaitSync, sync, 0, GL_TIMEOUT_IGNORED);
		TEST_INVOKE(Flush);
		TEST_INVOKE(Finish);
		TEST_INVOKE(MemoryBarrier, GL_ALL_BARRIER_BITS);
		TEST_INVOKE(MemoryBarrier, GL_VERTEX_ATTRIB_ARRAY_BARRIER_BIT | GL_ELEMENT_ARRAY_BARRIER_BIT | GL_UNIFORM_BARRIER_BIT |
			GL_TEXTURE_FETCH_BARRIER_BIT | GL_SHADER_IMAGE_ACCESS_BARRIER_BIT | GL_COMMAND_BARRIER_BIT | GL_PIXEL_BUFFER_BARRIER_BIT |
			GL_TEXTURE_UPDATE_BARRIER_BIT | GL_BUFFER_UPDATE_BARRIER_BIT | GL_FRAMEBUFFER_BARRIER_BIT | GL_TRANSFORM_FEEDBACK_BARRIER_BIT |
			GL_ATOMIC_COUNTER_BARRIER_BIT | GL_SHADER_STORAGE_BARRIER_BIT);
		TEST_INVOKE(MemoryBarrierByRegion, GL_ALL_BARRIER_BITS);
		TEST_INVOKE(MemoryBarrierByRegion, GL_ATOMIC_COUNTER_BARRIER_BIT | GL_FRAMEBUFFER_BARRIER_BIT | GL_SHADER_IMAGE_ACCESS_BARRIER_BIT |
			GL_SHADER_STORAGE_BARRIER_BIT | GL_TEXTURE_FETCH_BARRIER_BIT | GL_UNIFORM_BARRIER_BIT);
		TEST_INVOKE(TextureBarrier);
		TEST_INVOKE(DeleteSync, sync);

		TEST_INVOKE(DeleteSync, 0);

		assert(stats.commands.all == 14);
		assert(stats.commands.sync == 14);
		assert(stats.pipeline_flushes == 3);
		assert(stats.pipeline_barriers == 5);
		assert(stats.objects_created == 2);
		assert(stats.objects_destroyed == 2);
		totalstats.add(dc.getStats());
	}

	if (check_texture) {
		std::cout << "***********************************************************************************************" << std::endl;
		std::cout << "******************************************* TEXTURE *******************************************" << std::endl;
		std::cout << "***********************************************************************************************" << std::endl;
		dc.resetStats();
		TEST_INVOKE_REDUNDANTLY(ActiveTexture, GL_TEXTURE1);
		assert(state.texture.active_unit == 1);
		TEST_INVOKE_REDUNDANTLY(ActiveTexture, GL_TEXTURE0);
		assert(state.texture.active_unit == 0);
		
		GLuint badids[5] = { 0,0,0,0,0 };
		GLuint tex[11];
		TEST_INVOKE(GenTextures, 5, tex);
		assert(state.texture.textures.size() == 5);
		TEST_INVOKE(DeleteTextures, 5, tex);
		assert(state.texture.textures.size() == 0);
		TEST_INVOKE(CreateTextures, GL_TEXTURE_1D, 1, &tex[0]);
		TEST_INVOKE(CreateTextures, GL_TEXTURE_2D, 1, &tex[1]);
		TEST_INVOKE(CreateTextures, GL_TEXTURE_3D, 1, &tex[2]);
		TEST_INVOKE(CreateTextures, GL_TEXTURE_1D_ARRAY, 1, &tex[3]);
		TEST_INVOKE(CreateTextures, GL_TEXTURE_2D_ARRAY, 1, &tex[4]);
		TEST_INVOKE(CreateTextures, GL_TEXTURE_RECTANGLE, 1, &tex[5]);
		TEST_INVOKE(CreateTextures, GL_TEXTURE_CUBE_MAP, 1, &tex[6]);
		TEST_INVOKE(CreateTextures, GL_TEXTURE_CUBE_MAP_ARRAY, 1, &tex[7]);
		TEST_INVOKE(CreateTextures, GL_TEXTURE_BUFFER, 1, &tex[8]);
		TEST_INVOKE(CreateTextures, GL_TEXTURE_2D_MULTISAMPLE, 1, &tex[9]);
		TEST_INVOKE(CreateTextures, GL_TEXTURE_2D_MULTISAMPLE_ARRAY, 1, &tex[10]);
		assert(state.texture.textures.size() == 11);

		TEST_INVOKE_REDUNDANTLY(BindTextureUnit, 1, tex[0]);	assert(state.texture.unit[1].target_1d == tex[0]);
		TEST_INVOKE_REDUNDANTLY(BindTextureUnit, 1, tex[1]);	assert(state.texture.unit[1].target_2d == tex[1]);
		TEST_INVOKE_REDUNDANTLY(BindTextureUnit, 1, tex[2]);	assert(state.texture.unit[1].target_3d == tex[2]);
		TEST_INVOKE_REDUNDANTLY(BindTextureUnit, 1, tex[3]);	assert(state.texture.unit[1].target_1d_array == tex[3]);
		TEST_INVOKE_REDUNDANTLY(BindTextureUnit, 1, tex[4]);	assert(state.texture.unit[1].target_2d_array == tex[4]);
		TEST_INVOKE_REDUNDANTLY(BindTextureUnit, 1, tex[5]);	assert(state.texture.unit[1].target_rectangle == tex[5]);
		TEST_INVOKE_REDUNDANTLY(BindTextureUnit, 1, tex[6]);	assert(state.texture.unit[1].target_cubemap == tex[6]);
		TEST_INVOKE_REDUNDANTLY(BindTextureUnit, 1, tex[7]);	assert(state.texture.unit[1].target_cubemap_array== tex[7]);
		TEST_INVOKE_REDUNDANTLY(BindTextureUnit, 1, tex[8]);	assert(state.texture.unit[1].target_buffer == tex[8]);
		TEST_INVOKE_REDUNDANTLY(BindTextureUnit, 1, tex[9]);	assert(state.texture.unit[1].target_2d_multisample == tex[9]);
		TEST_INVOKE_REDUNDANTLY(BindTextureUnit, 1, tex[10]);	assert(state.texture.unit[1].target_2d_multisample_array == tex[10]);
		
		TEST_INVOKE_REDUNDANTLY(BindTexture, GL_TEXTURE_CUBE_MAP_ARRAY, tex[7]);
		assert(state.texture.unit[0].target_cubemap_array == tex[7]);
		TEST_INVOKE_REDUNDANTLY(BindTexture, GL_TEXTURE_CUBE_MAP_ARRAY, 0);
		assert(state.texture.unit[0].target_cubemap_array == 0);
		TEST_INVOKE_REDUNDANTLY(BindTextureUnit, 3, tex[7]);
		assert(state.texture.unit[3].target_cubemap_array == tex[7]);
		TEST_INVOKE_REDUNDANTLY(BindTextureUnit, 3, 0);
		assert(state.texture.unit[3].target_1d == 0);
		assert(state.texture.unit[3].target_2d == 0);
		assert(state.texture.unit[3].target_3d == 0);
		assert(state.texture.unit[3].target_1d_array == 0);
		assert(state.texture.unit[3].target_2d_array == 0);
		assert(state.texture.unit[3].target_rectangle == 0);
		assert(state.texture.unit[3].target_cubemap == 0);
		assert(state.texture.unit[3].target_cubemap_array == 0);
		assert(state.texture.unit[3].target_buffer == 0);
		assert(state.texture.unit[3].target_2d_multisample == 0);
		assert(state.texture.unit[3].target_2d_multisample_array == 0);
		TEST_INVOKE_REDUNDANTLY(BindTextures, 2, 6, tex);
		assert(state.texture.unit[2].target_1d == tex[0]);
		assert(state.texture.unit[3].target_2d == tex[1]);
		assert(state.texture.unit[4].target_3d == tex[2]);
		assert(state.texture.unit[5].target_1d_array == tex[3]);
		assert(state.texture.unit[6].target_2d_array == tex[4]);
		assert(state.texture.unit[7].target_rectangle == tex[5]);
		TEST_INVOKE_REDUNDANTLY(BindTextures, 2, 5, nullptr);
		for (int i = 2; i < 7; i++) {
			assert(state.texture.unit[i].target_1d == 0);
			assert(state.texture.unit[i].target_2d == 0);
			assert(state.texture.unit[i].target_3d == 0);
			assert(state.texture.unit[i].target_1d_array == 0);
			assert(state.texture.unit[i].target_2d_array == 0);
			assert(state.texture.unit[i].target_rectangle == 0);
			assert(state.texture.unit[i].target_cubemap == 0);
			assert(state.texture.unit[i].target_cubemap_array == 0);
			assert(state.texture.unit[i].target_buffer == 0);
			assert(state.texture.unit[i].target_2d_multisample == 0);
			assert(state.texture.unit[i].target_2d_multisample_array == 0);
		}

		const DescTexture& d0 = state.texture.textures.at(tex[0]);
		const DescTexture& d1 = state.texture.textures.at(tex[1]);
		const DescTexture& d2 = state.texture.textures.at(tex[2]);
		const DescTexture& d3 = state.texture.textures.at(tex[3]);
		const DescTexture& d4 = state.texture.textures.at(tex[4]);
		const DescTexture& d5 = state.texture.textures.at(tex[5]);
		const DescTexture& d6 = state.texture.textures.at(tex[6]);
		const DescTexture& d7 = state.texture.textures.at(tex[7]);

		unsigned char datauc[80] = { 0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0 };
		char datac[80] = { 0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0 };
		unsigned short dataus[80] = { 0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0 };
		short datas[80] = { 0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0 };
		unsigned int dataui[80] = { 0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0 };
		int datai[80] = { 0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0 };
		float dataf[80] = {0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0};
		
		
		TEST_INVOKE_REDUNDANTLY(BindTexture, GL_TEXTURE_1D, tex[0]);
		TEST_INVOKE(TexImage1D, GL_PROXY_TEXTURE_1D, 0, GL_RED, 10, 0, GL_RED, GL_UNSIGNED_BYTE, datauc);
		TEST_INVOKE(TexImage1D, GL_TEXTURE_1D, 0, GL_RED, 10, 0, GL_RED, GL_UNSIGNED_BYTE, datauc);
		auto& d0s = d0.storage;
		auto& d0p = d0.pixelformat;
		assert(d0.target == GL_TEXTURE_1D && d0s.faces[0].layer[0].mipmap.size() == 1 && d0s.base_width == 10 && d0s.base_height == 1 && d0s.base_depth == 1 && d0s.base_size == 10 );
		assert(d0p.red_bits == 8 && d0p.green_bits == 0 && d0p.blue_bits == 0 && d0p.alpha_bits == 0 && d0p.bps == 8 && d0p.format == GL_RED && d0p.depth_bits == 0 && d0p.stencil_bits == 0);
		assert(d0p.data_type == GL_UNSIGNED_NORMALIZED && d0p.compressed == GL_FALSE && d0p.samples == 1);
		assert(stats.memory.transfers_cpu_size == 10 * sizeof(unsigned char));

		
		//source types																										//starting from 10 (1x 10 pixels @uchar)
		GLint tr = 10 * sizeof(unsigned char);
		TEST_INVOKE(TexImage1D, GL_TEXTURE_1D, 0, GL_RED, 10, 0, GL_RED, GL_UNSIGNED_BYTE, datauc);		tr += 10 * sizeof(unsigned char);	assert(stats.memory.transfers_cpu_size == tr);
		TEST_INVOKE(TexImage1D, GL_TEXTURE_1D, 0, GL_RED, 10, 0, GL_RG, GL_UNSIGNED_BYTE, datauc);		tr += 20 * sizeof(unsigned char);	assert(stats.memory.transfers_cpu_size == tr);
		TEST_INVOKE(TexImage1D, GL_TEXTURE_1D, 0, GL_RED, 10, 0, GL_RGB, GL_UNSIGNED_BYTE, datauc);		tr += 30 * sizeof(unsigned char);	assert(stats.memory.transfers_cpu_size == tr);
		TEST_INVOKE(TexImage1D, GL_TEXTURE_1D, 0, GL_RED, 10, 0, GL_BGR, GL_UNSIGNED_BYTE, datauc);		tr += 30 * sizeof(unsigned char);	assert(stats.memory.transfers_cpu_size == tr);
		TEST_INVOKE(TexImage1D, GL_TEXTURE_1D, 0, GL_RED, 10, 0, GL_RGBA, GL_UNSIGNED_BYTE, datauc);	tr += 40 * sizeof(unsigned char);	assert(stats.memory.transfers_cpu_size == tr);
		TEST_INVOKE(TexImage1D, GL_TEXTURE_1D, 0, GL_RED, 10, 0, GL_BGRA, GL_UNSIGNED_BYTE, datauc);	tr += 40 * sizeof(unsigned char);	assert(stats.memory.transfers_cpu_size == tr);
		TEST_INVOKE(TexImage1D, GL_TEXTURE_1D, 0, GL_R8I, 10, 0, GL_RED_INTEGER, GL_INT, datai);		tr += 10 * sizeof(int);				assert(stats.memory.transfers_cpu_size == tr);
		TEST_INVOKE(TexImage1D, GL_TEXTURE_1D, 0, GL_R8I, 10, 0, GL_RG_INTEGER, GL_INT, datai);			tr += 20 * sizeof(int);				assert(stats.memory.transfers_cpu_size == tr);
		TEST_INVOKE(TexImage1D, GL_TEXTURE_1D, 0, GL_R8I, 10, 0, GL_RGB_INTEGER, GL_INT, datai);		tr += 30 * sizeof(int);				assert(stats.memory.transfers_cpu_size == tr);
		TEST_INVOKE(TexImage1D, GL_TEXTURE_1D, 0, GL_R8I, 10, 0, GL_BGR_INTEGER, GL_INT, datai);		tr += 30 * sizeof(int);				assert(stats.memory.transfers_cpu_size == tr);
		TEST_INVOKE(TexImage1D, GL_TEXTURE_1D, 0, GL_R8I, 10, 0, GL_RGBA_INTEGER, GL_INT, datai);		tr += 40 * sizeof(int);				assert(stats.memory.transfers_cpu_size == tr);
		TEST_INVOKE(TexImage1D, GL_TEXTURE_1D, 0, GL_R8I, 10, 0, GL_BGRA_INTEGER, GL_INT, datai);		tr += 40 * sizeof(int);				assert(stats.memory.transfers_cpu_size == tr);
				
		TEST_INVOKE(TexImage1D, GL_TEXTURE_1D, 0, GL_RED, 10, 0, GL_RED, GL_UNSIGNED_BYTE, datauc);		tr += 10 * sizeof(unsigned char);	assert(stats.memory.transfers_cpu_size == tr);
		TEST_INVOKE(TexImage1D, GL_TEXTURE_1D, 0, GL_RED, 10, 0, GL_RED, GL_BYTE, datac);				tr += 10 * sizeof(char);			assert(stats.memory.transfers_cpu_size == tr);
		TEST_INVOKE(TexImage1D, GL_TEXTURE_1D, 0, GL_R8I, 10, 0, GL_RED_INTEGER, GL_INT, datai);				tr += 10 * sizeof(int);				assert(stats.memory.transfers_cpu_size == tr);
		TEST_INVOKE(TexImage1D, GL_TEXTURE_1D, 0, GL_R8I, 10, 0, GL_RED_INTEGER, GL_UNSIGNED_SHORT, dataus);	tr += 10 * sizeof(unsigned short);	assert(stats.memory.transfers_cpu_size == tr);
		TEST_INVOKE(TexImage1D, GL_TEXTURE_1D, 0, GL_R8I, 10, 0, GL_RED_INTEGER, GL_SHORT, dataus);				tr += 10 * sizeof(short);			assert(stats.memory.transfers_cpu_size == tr);
		TEST_INVOKE(TexImage1D, GL_TEXTURE_1D, 0, GL_R8I, 10, 0, GL_RED_INTEGER, GL_UNSIGNED_INT, dataus);		tr += 10 * sizeof(unsigned int);	assert(stats.memory.transfers_cpu_size == tr);
		TEST_INVOKE(TexImage1D, GL_TEXTURE_1D, 0, GL_R8I, 10, 0, GL_RED_INTEGER, GL_INT, dataus);				tr += 10 * sizeof(int);				assert(stats.memory.transfers_cpu_size == tr);
		TEST_INVOKE(TexImage1D, GL_TEXTURE_1D, 0, GL_R16F, 10, 0, GL_RED, GL_FLOAT, dataus);					tr += 10 * sizeof(float);			assert(stats.memory.transfers_cpu_size == tr);
		TEST_INVOKE(TexImage1D, GL_TEXTURE_1D, 0, GL_RED, 10, 0, GL_RGB, GL_UNSIGNED_BYTE_3_3_2, datauc);		tr += 10 * sizeof(unsigned char);	assert(stats.memory.transfers_cpu_size == tr);
		TEST_INVOKE(TexImage1D, GL_TEXTURE_1D, 0, GL_RED, 10, 0, GL_RGB, GL_UNSIGNED_BYTE_2_3_3_REV, datauc);	tr += 10 * sizeof(unsigned char);	assert(stats.memory.transfers_cpu_size == tr);
		TEST_INVOKE(TexImage1D, GL_TEXTURE_1D, 0, GL_RED, 10, 0, GL_RGB, GL_UNSIGNED_SHORT_5_6_5, dataus);		tr += 10 * sizeof(unsigned short);	assert(stats.memory.transfers_cpu_size == tr);
		TEST_INVOKE(TexImage1D, GL_TEXTURE_1D, 0, GL_RED, 10, 0, GL_RGB, GL_UNSIGNED_SHORT_5_6_5_REV, dataus);	tr += 10 * sizeof(unsigned short);	assert(stats.memory.transfers_cpu_size == tr);
		TEST_INVOKE(TexImage1D, GL_TEXTURE_1D, 0, GL_RED, 10, 0, GL_BGRA, GL_UNSIGNED_SHORT_4_4_4_4, dataus);	tr += 10 * sizeof(unsigned short);	assert(stats.memory.transfers_cpu_size == tr);
		TEST_INVOKE(TexImage1D, GL_TEXTURE_1D, 0, GL_RED, 10, 0, GL_BGRA, GL_UNSIGNED_SHORT_4_4_4_4_REV, dataus);tr += 10 * sizeof(unsigned short);	assert(stats.memory.transfers_cpu_size == tr);
		TEST_INVOKE(TexImage1D, GL_TEXTURE_1D, 0, GL_RED, 10, 0, GL_BGRA, GL_UNSIGNED_SHORT_5_5_5_1, dataus);	tr += 10 * sizeof(unsigned short);	assert(stats.memory.transfers_cpu_size == tr);
		TEST_INVOKE(TexImage1D, GL_TEXTURE_1D, 0, GL_RED, 10, 0, GL_BGRA, GL_UNSIGNED_SHORT_1_5_5_5_REV, dataus);tr += 10 * sizeof(unsigned short);	assert(stats.memory.transfers_cpu_size == tr);
		TEST_INVOKE(TexImage1D, GL_TEXTURE_1D, 0, GL_RED, 10, 0, GL_BGRA, GL_UNSIGNED_INT_8_8_8_8, dataus);		tr += 10 * sizeof(unsigned int);	assert(stats.memory.transfers_cpu_size == tr);
		TEST_INVOKE(TexImage1D, GL_TEXTURE_1D, 0, GL_RED, 10, 0, GL_BGRA, GL_UNSIGNED_INT_8_8_8_8_REV, dataus);	tr += 10 * sizeof(unsigned int);	assert(stats.memory.transfers_cpu_size == tr);
		TEST_INVOKE(TexImage1D, GL_TEXTURE_1D, 0, GL_RED, 10, 0, GL_BGRA, GL_UNSIGNED_INT_10_10_10_2, dataus);	tr += 10 * sizeof(unsigned int);	assert(stats.memory.transfers_cpu_size == tr);
		TEST_INVOKE(TexImage1D, GL_TEXTURE_1D, 0, GL_RED, 10, 0, GL_BGRA, GL_UNSIGNED_INT_2_10_10_10_REV, dataus);tr += 10 * sizeof(unsigned int);	assert(stats.memory.transfers_cpu_size == tr);


		//types
		TEST_INVOKE(TexImage1D, GL_TEXTURE_1D, 0, GL_R3_G3_B2, 10, 0, GL_RGBA, GL_UNSIGNED_BYTE, datauc);	tr+= 40 * sizeof(unsigned char);
		assert(d0p.format == GL_R3_G3_B2 && d0p.data_type == GL_UNSIGNED_NORMALIZED && d0p.red_bits == 3 && d0p.green_bits == 3 && d0p.blue_bits == 2 && d0p.bps == 8);
		TEST_INVOKE(TexImage1D, GL_TEXTURE_1D, 0, GL_RGB9_E5, 10, 0, GL_RGBA, GL_UNSIGNED_BYTE, datauc);	tr += 40 * sizeof(unsigned char);
		assert(d0p.format == GL_RGB9_E5 && d0p.data_type == GL_UNSIGNED_NORMALIZED && d0p.red_bits == 9 && d0p.green_bits == 9 && d0p.blue_bits == 9 && d0p.bps == 32);
		TEST_INVOKE(TexImage1D, GL_TEXTURE_1D, 0, GL_RGBA32F, 10, 0, GL_RGBA, GL_FLOAT, dataf);	tr += 40 * sizeof(float);
		assert(d0p.format == GL_RGBA32F && d0p.data_type == GL_FLOAT && d0p.red_bits == 32 && d0p.green_bits == 32 && d0p.blue_bits == 32 && d0p.alpha_bits == 32 && d0p.bps == 128);
		TEST_INVOKE(TexImage1D, GL_TEXTURE_1D, 0, GL_R16UI, 10, 0, GL_RGBA_INTEGER, GL_UNSIGNED_INT, dataui);	tr += 40 * sizeof(unsigned int);
		assert(d0p.format == GL_R16UI && d0p.data_type == GL_UNSIGNED_INT && d0p.red_bits == 16 && d0p.green_bits == 0 && d0p.blue_bits == 0 && d0p.alpha_bits == 0 && d0p.bps == 16);
		TEST_INVOKE(TexImage1D, GL_TEXTURE_1D, 0, GL_RGBA8_SNORM, 10, 0, GL_RGBA, GL_UNSIGNED_BYTE, datauc);	tr += 40 * sizeof(unsigned char);
		assert(d0p.format == GL_RGBA8_SNORM && d0p.data_type == GL_SIGNED_NORMALIZED && d0p.red_bits == 8 && d0p.green_bits == 8 && d0p.blue_bits == 8 && d0p.alpha_bits == 8 && d0p.bps == 32);
		TEST_INVOKE(TexImage1D, GL_TEXTURE_1D, 0, GL_SRGB8, 10, 0, GL_RGBA, GL_UNSIGNED_BYTE, datauc);	tr += 40 * sizeof(unsigned char);
		assert(d0p.srgb == GL_TRUE && d0p.format == GL_SRGB8 && d0p.data_type == GL_UNSIGNED_NORMALIZED && d0p.red_bits == 8 && d0p.green_bits == 8 && d0p.blue_bits == 8 && d0p.alpha_bits == 0 && d0p.bps == 24);

		//commands
		TEST_INVOKE_REDUNDANTLY(BindTexture, GL_TEXTURE_2D, tex[1]);
		TEST_INVOKE(TexImage2D, GL_PROXY_TEXTURE_2D, 0, GL_RED, 10, 4, 0, GL_RED, GL_UNSIGNED_BYTE, datauc);
		TEST_INVOKE(TexImage2D, GL_TEXTURE_2D, 0, GL_RED, 10, 4, 0, GL_RED, GL_UNSIGNED_BYTE, datauc);
		const DescTexture* p = &state.texture.textures.at(tex[1]);	const DescTexture::Storage* ps = &p->storage;	const DescTexture::PixelFormat* pp = &p->pixelformat;
		assert(p->target == GL_TEXTURE_2D && ps->faces[0].layer[0].mipmap.size() == 1 && ps->base_width == 10 && ps->base_height == 4);
		assert(ps->base_depth == 1 && ps->base_size == 40 && pp->compressed == GL_FALSE && pp->samples == 1);
		tr+= 40 * sizeof(unsigned char);	assert(stats.memory.transfers_cpu_size == tr);
		
		TEST_INVOKE_REDUNDANTLY(BindTexture, GL_TEXTURE_1D_ARRAY, tex[3]);
		TEST_INVOKE(TexImage2D, GL_PROXY_TEXTURE_1D_ARRAY, 0, GL_RED, 8, 3, 0, GL_RED, GL_UNSIGNED_BYTE, datauc);
		TEST_INVOKE(TexImage2D, GL_TEXTURE_1D_ARRAY, 0, GL_RED, 8, 3, 0, GL_RED, GL_UNSIGNED_BYTE, datauc);
		p = &state.texture.textures.at(tex[3]);	ps = &p->storage;	pp = &p->pixelformat;
		assert(p->target == GL_TEXTURE_1D_ARRAY && ps->faces[0].layer.size() == 3 && ps->base_width == 8 && ps->base_height == 1);
		assert(ps->base_depth == 1 && ps->base_size == 8 && pp->compressed == GL_FALSE && pp->samples == 1);
		tr += 24 * sizeof(unsigned char);	assert(stats.memory.transfers_cpu_size == tr);

		
		assert(stats.memory.current == 94);
		TEST_INVOKE_REDUNDANTLY(BindTexture, GL_TEXTURE_CUBE_MAP, tex[6]);
		p = &state.texture.textures.at(tex[6]);	ps = &p->storage;	pp = &p->pixelformat;
		for (int i = 0; i < 6; i++) {
			TEST_INVOKE(TexImage2D, GL_TEXTURE_CUBE_MAP_POSITIVE_X + i, 0, GL_RED, 4, 4, 0, GL_RED, GL_UNSIGNED_BYTE, datauc);
			assert(p->target == GL_TEXTURE_CUBE_MAP && ps->faces[i].layer[0].mipmap.size() == 1 && ps->base_width == 4 && ps->base_height == 4);
			assert(ps->base_depth == 1 && ps->base_size == 16 && pp->compressed == GL_FALSE && pp->samples == 1);
			tr += 16 * sizeof(unsigned char);	assert(stats.memory.transfers_cpu_size == tr);
			assert(stats.memory.current == 190);
		}
			

		TEST_INVOKE_REDUNDANTLY(BindTexture, GL_TEXTURE_3D, tex[2]);
		TEST_INVOKE(TexImage3D, GL_PROXY_TEXTURE_3D, 0, GL_RED, 4, 3, 5, 0, GL_RED, GL_UNSIGNED_BYTE, datauc);
		TEST_INVOKE(TexImage3D, GL_TEXTURE_3D, 0, GL_RED, 4, 3, 5, 0, GL_RED, GL_UNSIGNED_BYTE, datauc);
		p = &state.texture.textures.at(tex[2]);	ps = &p->storage;	pp = &p->pixelformat;
		assert(p->target == GL_TEXTURE_3D && ps->faces[0].layer.size() == 1 && ps->base_width == 4 && ps->base_height == 3);
		assert(ps->base_depth == 5 && ps->base_size == 60 && pp->compressed == GL_FALSE && pp->samples == 1);
		tr += 60 * sizeof(unsigned char);	assert(stats.memory.transfers_cpu_size == tr);

		TEST_INVOKE_REDUNDANTLY(BindTexture, GL_TEXTURE_2D_ARRAY, tex[4]);
		TEST_INVOKE(TexImage3D, GL_PROXY_TEXTURE_2D_ARRAY, 0, GL_RED, 4, 3, 5, 0, GL_RED, GL_UNSIGNED_BYTE, datauc);
		TEST_INVOKE(TexImage3D, GL_TEXTURE_2D_ARRAY, 0, GL_RED, 4, 3, 5, 0, GL_RED, GL_UNSIGNED_BYTE, datauc);
		p = &state.texture.textures.at(tex[4]);	ps = &p->storage;	pp = &p->pixelformat;
		assert(p->target == GL_TEXTURE_2D_ARRAY && ps->faces[0].layer.size() == 5 && ps->base_width == 4 && ps->base_height == 3);
		assert(ps->base_depth == 1 && ps->base_size == 12 && pp->compressed == GL_FALSE && pp->samples == 1);
		tr += 60 * sizeof(unsigned char);	assert(stats.memory.transfers_cpu_size == tr);

		TEST_INVOKE_REDUNDANTLY(BindTexture, GL_TEXTURE_2D_MULTISAMPLE, tex[9]);
		TEST_INVOKE(TexImage2DMultisample, GL_PROXY_TEXTURE_2D_MULTISAMPLE, 4, GL_RED, 14, 22, GL_TRUE);
		TEST_INVOKE(TexImage2DMultisample, GL_TEXTURE_2D_MULTISAMPLE, 4, GL_RED, 14, 22, GL_TRUE);
		p = &state.texture.textures.at(tex[9]);	ps = &p->storage;	pp = &p->pixelformat;
		assert(p->target == GL_TEXTURE_2D_MULTISAMPLE && ps->faces[0].layer[0].mipmap.size() == 1 && ps->base_width == 14 && ps->base_height == 22);
		assert(ps->base_depth == 1 && ps->base_size == 1232  && pp->compressed == GL_FALSE && pp->samples == 4);

		TEST_INVOKE_REDUNDANTLY(BindTexture, GL_TEXTURE_2D_MULTISAMPLE_ARRAY, tex[10]);
		TEST_INVOKE(TexImage3DMultisample, GL_PROXY_TEXTURE_2D_MULTISAMPLE_ARRAY, 4, GL_RED, 14, 22, 5, GL_TRUE);
		TEST_INVOKE(TexImage3DMultisample, GL_TEXTURE_2D_MULTISAMPLE_ARRAY, 4, GL_RED, 14, 22, 5, GL_TRUE);
		p = &state.texture.textures.at(tex[10]);	ps = &p->storage;	pp = &p->pixelformat;
		assert(p->target == GL_TEXTURE_2D_MULTISAMPLE_ARRAY && ps->faces[0].layer.size()==5 && ps->base_width == 14 && ps->base_height == 22);
		assert(ps->base_depth == 1 && ps->base_size == 1232 && pp->compressed == GL_FALSE && pp->samples == 4);

		
		TEST_INVOKE_REDUNDANTLY(BindTexture, GL_TEXTURE_CUBE_MAP_ARRAY, tex[7]);
		TEST_INVOKE(TexImage3D, GL_PROXY_TEXTURE_CUBE_MAP_ARRAY, 0, GL_RED, 3, 3, 6, 0, GL_RED, GL_UNSIGNED_BYTE, datauc);
		TEST_INVOKE(TexImage3D, GL_TEXTURE_CUBE_MAP_ARRAY, 0, GL_RED, 3, 3, 6, 0, GL_RED, GL_UNSIGNED_BYTE, datauc);
		p = &state.texture.textures.at(tex[7]);	ps = &p->storage;	pp = &p->pixelformat;
		assert(p->target == GL_TEXTURE_CUBE_MAP_ARRAY && ps->faces.size()==6 && ps->faces[0].layer.size() ==6 && ps->base_width == 3 && ps->base_height == 3);
		assert(ps->base_depth == 1 && ps->base_size == 9 && pp->compressed == GL_FALSE && pp->samples == 1);
		

		TEST_INVOKE(CompressedTexImage2D, GL_TEXTURE_2D, 0, GL_COMPRESSED_SIGNED_RED_RGTC1, 8, 8, 0, 32, datauc);
		p = &state.texture.textures.at(tex[1]);	ps = &p->storage;	pp = &p->pixelformat;
		assert(ps->faces.size() == 1 && ps->faces[0].layer.size() == 1 && ps->base_width == 8 && ps->base_height == 8);
		assert(ps->base_depth == 1 && ps->base_size == 32 && pp->compressed == GL_TRUE && pp->samples == 1);
		
		assert(stats.memory.current == 8018);

		TEST_INVOKE(GenerateMipmap, GL_TEXTURE_2D);	//8x8 4x4 compressed -> 32 + 8
		assert(stats.memory.current == 8026);
		TEST_INVOKE(GenerateMipmap, GL_TEXTURE_3D); // 4x3x5 -> 2x1x2 -> 1x1x1 -> 60 + 4 + 1
		assert(stats.memory.current == 8031);
		TEST_INVOKE(GenerateTextureMipmap, tex[2]);	
		assert(stats.memory.current == 8031);


		//storage functions
		TEST_INVOKE(TexStorage1D, GL_TEXTURE_1D, 4, GL_R8, 32);	//32, 16, 8, 4 => 60									-30 original size
		assert(state.texture.textures.at(tex[0]).storage.immutable == GL_TRUE);		 assert(stats.memory.current == 8061);
		TEST_INVOKE(TexStorage2D, GL_TEXTURE_2D, 4, GL_R8, 32, 32);	//32x32, 16x16, 8x8, 4x4 => 1360					-40 original size
		assert(state.texture.textures.at(tex[1]).storage.immutable == GL_TRUE);		 assert(stats.memory.current == 9381);
		TEST_INVOKE(TexStorage2DMultisample, GL_TEXTURE_2D_MULTISAMPLE, 8, GL_R8, 16, 16, GL_TRUE); //16x16x8 => 2048	-1232 original size
		assert(state.texture.textures.at(tex[9]).storage.immutable == GL_TRUE);		 assert(stats.memory.current == 10197);
		TEST_INVOKE(TexStorage3D, GL_TEXTURE_3D, 5, GL_R8, 64, 64, 64);	// 64x64x64 32x32x32 16x16x16 8x8x8 4x4x4 => 299584				-65 original size
		assert(state.texture.textures.at(tex[2]).storage.immutable == GL_TRUE);		 assert(stats.memory.current == 309716);
		TEST_INVOKE(TexStorage3DMultisample, GL_TEXTURE_2D_MULTISAMPLE_ARRAY, 5, GL_R8, 32, 32, 6, GL_TRUE); //32x32x6x5 => 30720		-6160 original size
		assert(state.texture.textures.at(tex[10]).storage.immutable == GL_TRUE);	assert(stats.memory.current == 334276);
		
		GLuint texx;
		TEST_INVOKE(GenTextures, 1, &texx);
		TEST_INVOKE(TextureView, texx, GL_TEXTURE_1D, tex[0], GL_R8, 0, 1, 0, 1);
		TEST_INVOKE(DeleteTextures, 1, &texx);

		TEST_INVOKE(DeleteTextures, 1, &tex[0]);	TEST_INVOKE(CreateTextures, GL_TEXTURE_1D, 1, &tex[0]);	TEST_INVOKE(BindTexture, GL_TEXTURE_1D, tex[0]);
		TEST_INVOKE(DeleteTextures, 1, &tex[1]);	TEST_INVOKE(CreateTextures, GL_TEXTURE_2D, 1, &tex[1]);	TEST_INVOKE(BindTexture, GL_TEXTURE_2D, tex[1]);
		TEST_INVOKE(DeleteTextures, 1, &tex[9]);	TEST_INVOKE(CreateTextures, GL_TEXTURE_2D_MULTISAMPLE, 1, &tex[9]);	TEST_INVOKE(BindTexture, GL_TEXTURE_2D_MULTISAMPLE, tex[9]);
		TEST_INVOKE(DeleteTextures, 1, &tex[2]);	TEST_INVOKE(CreateTextures, GL_TEXTURE_3D, 1, &tex[2]);	TEST_INVOKE(BindTexture, GL_TEXTURE_3D, tex[2]);
		TEST_INVOKE(DeleteTextures, 1, &tex[10]);	TEST_INVOKE(CreateTextures, GL_TEXTURE_2D_MULTISAMPLE_ARRAY, 1, &tex[10]); TEST_INVOKE(BindTexture, GL_TEXTURE_2D_MULTISAMPLE_ARRAY, tex[10]);
		assert(stats.memory.current == 504);

		//storage functions
		TEST_INVOKE(TextureStorage1D, tex[0], 4, GL_R8, 32);	//32, 16, 8, 4 => 60									
		assert(state.texture.textures.at(tex[0]).storage.immutable == GL_TRUE);		 assert(stats.memory.current == 564);
		TEST_INVOKE(TextureStorage2D, tex[1], 4, GL_R8, 32, 32);	//32x32, 16x16, 8x8, 4x4 => 1360					
		assert(state.texture.textures.at(tex[1]).storage.immutable == GL_TRUE);		 assert(stats.memory.current == 1924);
		TEST_INVOKE(TextureStorage2DMultisample, tex[9], 8, GL_R8, 16, 16, GL_TRUE); //16x16x8 => 2048
		assert(state.texture.textures.at(tex[9]).storage.immutable == GL_TRUE);		 assert(stats.memory.current == 3972);
		TEST_INVOKE(TextureStorage3D, tex[2], 5, GL_R8, 64, 64, 64);	// 64x64x64 32x32x32 16x16x16 8x8x8 4x4x4 => 299584	
		assert(state.texture.textures.at(tex[2]).storage.immutable == GL_TRUE);		 assert(stats.memory.current == 303556);
		TEST_INVOKE(TextureStorage3DMultisample, tex[10], 5, GL_R8, 32, 32, 6, GL_TRUE); //32x32x6x5 => 30720
		assert(state.texture.textures.at(tex[10]).storage.immutable == GL_TRUE);	assert(stats.memory.current == 334276);


		TEST_INVOKE(DeleteTextures, 1, &tex[0]);	TEST_INVOKE(CreateTextures, GL_TEXTURE_1D, 1, &tex[0]);	TEST_INVOKE(BindTexture, GL_TEXTURE_1D, tex[0]);
		TEST_INVOKE(DeleteTextures, 1, &tex[1]);	TEST_INVOKE(CreateTextures, GL_TEXTURE_2D, 1, &tex[1]);	TEST_INVOKE(BindTexture, GL_TEXTURE_2D, tex[1]);
		TEST_INVOKE(DeleteTextures, 1, &tex[2]);	TEST_INVOKE(CreateTextures, GL_TEXTURE_3D, 1, &tex[2]);	TEST_INVOKE(BindTexture, GL_TEXTURE_3D, tex[2]);

		//subdata functions
		unsigned char datauclong[4096]; 
		//TEST_INVOKE(CompressedTexImage1D, GL_TEXTURE_1D, 0, GL_COMPRESSED_SIGNED_RED_RGTC1, 128, 0, 64, datauclong);
		TEST_INVOKE(CompressedTexImage2D, GL_TEXTURE_2D, 0, GL_COMPRESSED_SIGNED_RED_RGTC1, 128, 128, 0, 128 * 128 /2, datauclong);
		//TEST_INVOKE(CompressedTexImage3D, GL_TEXTURE_3D, 0, GL_COMPRESSED_SIGNED_RED_RGTC1, 16, 16, 16, 0, 16*16*16/2, datauclong);
		//TEST_INVOKE(CompressedTexSubImage1D, GL_TEXTURE_1D, 0, 16, 32, GL_COMPRESSED_SIGNED_RED_RGTC1, 4096, datauclong);
		//TEST_INVOKE(CompressedTextureSubImage1D, tex[0], 0, 16, 32, GL_COMPRESSED_SIGNED_RED_RGTC1, 4096, datauclong);
		assert(stats.memory.transfers_cpu_size == 10688);
		
		TEST_INVOKE(CompressedTexSubImage2D, GL_TEXTURE_2D, 0, 16, 32, 64, 32, GL_COMPRESSED_SIGNED_RED_RGTC1, 1024, datauclong);
		assert(stats.memory.transfers_cpu_size == 11712);
		TEST_INVOKE(CompressedTextureSubImage2D, tex[1], 0, 16, 32, 64, 32, GL_COMPRESSED_SIGNED_RED_RGTC1, 1024, datauclong);
		assert(stats.memory.transfers_cpu_size == 12736);
		//TEST_INVOKE(CompressedTexSubImage3D, GL_TEXTURE_3D, 0, 4, 4, 4, 8, 8, 8, GL_COMPRESSED_SIGNED_RED_RGTC1, 4096, datauclong);
		//TEST_INVOKE(CompressedTextureSubImage3D, tex[2], 0, 4, 4, 4, 8, 8, 8, GL_COMPRESSED_SIGNED_RED_RGTC1, 4096, datauclong);


		TEST_INVOKE(DeleteTextures, 1, &tex[0]);	TEST_INVOKE(CreateTextures, GL_TEXTURE_1D, 1, &tex[0]);	TEST_INVOKE(BindTexture, GL_TEXTURE_1D, tex[0]);
		TEST_INVOKE(DeleteTextures, 1, &tex[1]);	TEST_INVOKE(CreateTextures, GL_TEXTURE_2D, 1, &tex[1]);	TEST_INVOKE(BindTexture, GL_TEXTURE_2D, tex[1]);
		TEST_INVOKE(DeleteTextures, 1, &tex[2]);	TEST_INVOKE(CreateTextures, GL_TEXTURE_3D, 1, &tex[2]);	TEST_INVOKE(BindTexture, GL_TEXTURE_3D, tex[2]);
		TEST_INVOKE(TexImage1D, GL_TEXTURE_1D, 0, GL_R8, 100, 0, GL_RED, GL_UNSIGNED_BYTE, datauclong);
		TEST_INVOKE(TexImage2D, GL_TEXTURE_2D, 0, GL_R8, 22, 32, 0, GL_RED, GL_UNSIGNED_BYTE, datauclong);
		TEST_INVOKE(TexImage3D, GL_TEXTURE_3D, 0, GL_R8, 22, 32, 5, 0,  GL_RED, GL_UNSIGNED_BYTE, datauclong);

		TEST_INVOKE(ClearTexImage, tex[0], 0, GL_RED, GL_UNSIGNED_BYTE, datauclong);
		TEST_INVOKE(ClearTexSubImage, tex[2], 0, 4, 4, 0, 4, 4, 4, GL_RED, GL_UNSIGNED_BYTE, datauclong);
		TEST_INVOKE(InvalidateTexImage, tex[2], 0);
		TEST_INVOKE(InvalidateTexSubImage, tex[2], 0, 3, 2, 1, 5, 2, 2);
		

		GLuint buf;
		TEST_INVOKE(CreateBuffers, 1, &buf);
		TEST_INVOKE(NamedBufferData, buf, 100, datauclong, GL_STATIC_DRAW);
		TEST_INVOKE(TexBuffer, GL_TEXTURE_BUFFER, GL_R8, buf);
		TEST_INVOKE(TextureBuffer, tex[8], GL_R8, buf);
		TEST_INVOKE(TexBufferRange, GL_TEXTURE_BUFFER, GL_R8, buf, 0, 32);
		TEST_INVOKE(TextureBufferRange, tex[8], GL_R8, buf, 0, 32);
		TEST_INVOKE(DeleteBuffers, 1, &buf);



		//sub image
		assert(stats.memory.transfers_gpu_size == 0);
		TEST_INVOKE(CopyTexImage1D, GL_TEXTURE_1D, 0, GL_RED, 0, 0, 10, 0);
		assert(stats.memory.transfers_gpu_size == 10);
		TEST_INVOKE(CopyTexImage2D, GL_TEXTURE_2D, 0, GL_RED, 0, 0, 10, 10, 0);
		assert(stats.memory.transfers_gpu_size == 110);

		TEST_INVOKE(CopyTexSubImage1D, GL_TEXTURE_1D, 0, 0, 0, 0, 10);
		assert(stats.memory.transfers_gpu_size == 120);
		TEST_INVOKE(CopyTextureSubImage1D, tex[0], 0, 0, 0, 0, 10);
		assert(stats.memory.transfers_gpu_size == 130);
		TEST_INVOKE(CopyTexSubImage2D, GL_TEXTURE_2D, 0, 0, 0, 0, 0, 10, 10);
		assert(stats.memory.transfers_gpu_size == 230);
		TEST_INVOKE(CopyTextureSubImage2D, tex[1], 0, 0, 0, 0, 0, 10, 10);
		assert(stats.memory.transfers_gpu_size == 330);
		TEST_INVOKE(CopyTexSubImage3D, GL_TEXTURE_3D, 0, 0, 0, 0, 0, 0, 10, 10);
		assert(stats.memory.transfers_gpu_size == 430);
		TEST_INVOKE(CopyTextureSubImage3D, tex[2], 0, 0, 0, 0, 0, 0, 10, 10);
		assert(stats.memory.transfers_gpu_size == 530);
		
		assert(stats.memory.transfers_cpu_size == 17160);
		TEST_INVOKE(TexSubImage1D, GL_TEXTURE_1D, 0, 0, 10, GL_RED, GL_UNSIGNED_BYTE, datauclong);
		assert(stats.memory.transfers_cpu_size == 17170);
		TEST_INVOKE(TextureSubImage1D, tex[0], 0, 0, 10, GL_RED, GL_UNSIGNED_BYTE, datauclong);
		assert(stats.memory.transfers_cpu_size == 17180);
		TEST_INVOKE(TexSubImage2D, GL_TEXTURE_2D, 0, 0, 0, 10, 10, GL_RED, GL_UNSIGNED_BYTE, datauclong);
		assert(stats.memory.transfers_cpu_size == 17280);
		TEST_INVOKE(TextureSubImage2D, tex[1], 0, 0, 0, 10, 10, GL_RED, GL_UNSIGNED_BYTE, datauclong);
		assert(stats.memory.transfers_cpu_size == 17380);
		TEST_INVOKE(TexSubImage3D, GL_TEXTURE_3D, 0, 0, 0, 0, 10, 10, 3, GL_RED, GL_UNSIGNED_BYTE, datauclong);
		assert(stats.memory.transfers_cpu_size == 17680);
		TEST_INVOKE(TextureSubImage3D, tex[2], 0, 0, 0, 0, 10, 10, 3, GL_RED, GL_UNSIGNED_BYTE, datauclong);
		assert(stats.memory.transfers_cpu_size == 17980);


		//DO PARAMETER
		float colorf[100];
		int colori[100];
		unsigned int colorui[100];
		for (int i = 0; i < 100; i++) { 
			colorf[i] = i / 100.0f; 
			colori[i] = i;
			colorui[i] = i + 3;
		}
		assert(stats.commands.redundant == 28);
		auto& param = state.texture.textures.at(tex[1]).parameters;
		TEST_INVOKE_REDUNDANTLY(TexParameterf, GL_TEXTURE_2D, GL_DEPTH_STENCIL_TEXTURE_MODE, GL_STENCIL_INDEX);
		assert(param.depth_stencil_texture_mode == GL_STENCIL_INDEX);
		TEST_INVOKE_REDUNDANTLY(TextureParameterf, tex[1], GL_TEXTURE_BASE_LEVEL, 2);
		assert(param.lod_base == 2);
		TEST_INVOKE_REDUNDANTLY(TexParameterfv, GL_TEXTURE_2D, GL_TEXTURE_BORDER_COLOR, colorf);
		assert(param.bordercolor.r == colorf[0] && param.bordercolor.g == colorf[1] && param.bordercolor.b == colorf[2] && param.bordercolor.a == colorf[3]);
		TEST_INVOKE_REDUNDANTLY(TextureParameterfv, tex[1], GL_TEXTURE_BORDER_COLOR, &colorf[30]);
		assert(param.bordercolor.r == colorf[30] && param.bordercolor.g == colorf[31] && param.bordercolor.b == colorf[32] && param.bordercolor.a == colorf[33]);
		TEST_INVOKE_REDUNDANTLY(TextureParameteri, tex[1], GL_TEXTURE_COMPARE_FUNC, GL_GEQUAL);
		assert(param.compare_func == GL_GEQUAL);
		TEST_INVOKE_REDUNDANTLY(TexParameteri, GL_TEXTURE_2D, GL_TEXTURE_COMPARE_MODE, GL_COMPARE_REF_TO_TEXTURE);
		assert(param.compare_mode == GL_COMPARE_REF_TO_TEXTURE);
		TEST_INVOKE_REDUNDANTLY(TexParameterf, GL_TEXTURE_2D, GL_TEXTURE_LOD_BIAS, 0.5f);
		assert(param.lod_bias == 0.5f);
		TEST_INVOKE_REDUNDANTLY(TexParameterf, GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_NEAREST);
		assert(param.filter_min == GL_LINEAR_MIPMAP_NEAREST);
		TEST_INVOKE_REDUNDANTLY(TexParameterf, GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
		assert(param.filter_mag == GL_LINEAR);
		TEST_INVOKE_REDUNDANTLY(TexParameterf, GL_TEXTURE_2D, GL_TEXTURE_MIN_LOD, -5);
		assert(param.lod_min == -5);
		TEST_INVOKE_REDUNDANTLY(TexParameterf, GL_TEXTURE_2D, GL_TEXTURE_MAX_LOD, 5);
		assert(param.lod_max == 5);
		TEST_INVOKE_REDUNDANTLY(TexParameterf, GL_TEXTURE_2D, 0x84FE, 4);	//TEXTURE_MAX_ANISOTROPY_EXT
		assert(param.max_anisotropy == 4);
		TEST_INVOKE_REDUNDANTLY(TexParameterf, GL_TEXTURE_2D, GL_TEXTURE_SWIZZLE_R, GL_BLUE);
		assert(param.swizzle_rgba.r == GL_BLUE);
		TEST_INVOKE_REDUNDANTLY(TexParameterf, GL_TEXTURE_2D, GL_TEXTURE_SWIZZLE_G, GL_RED);
		assert(param.swizzle_rgba.g == GL_RED);
		TEST_INVOKE_REDUNDANTLY(TexParameterf, GL_TEXTURE_2D, GL_TEXTURE_SWIZZLE_B, GL_ALPHA);
		assert(param.swizzle_rgba.b == GL_ALPHA);
		TEST_INVOKE_REDUNDANTLY(TexParameterf, GL_TEXTURE_2D, GL_TEXTURE_SWIZZLE_A, GL_ONE);
		assert(param.swizzle_rgba.a == GL_ONE);
		GLint swizzle[4] = { GL_RED, GL_BLUE, GL_ONE, GL_GREEN };
		TEST_INVOKE_REDUNDANTLY(TexParameteriv, GL_TEXTURE_2D, GL_TEXTURE_SWIZZLE_RGBA, swizzle);
		assert(param.swizzle_rgba.r == swizzle[0] && param.swizzle_rgba.g == swizzle[1] && param.swizzle_rgba.b == swizzle[2] && param.swizzle_rgba.a == swizzle[3]);
		TEST_INVOKE_REDUNDANTLY(TexParameterf, GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_BORDER);
		assert(param.wrap_s== GL_CLAMP_TO_BORDER);
		TEST_INVOKE_REDUNDANTLY(TexParameterf, GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_MIRRORED_REPEAT);
		assert(param.wrap_t == GL_MIRRORED_REPEAT);
		TEST_INVOKE_REDUNDANTLY(TexParameterf, GL_TEXTURE_2D, GL_TEXTURE_WRAP_R, GL_MIRROR_CLAMP_TO_EDGE);
		assert(param.wrap_r == GL_MIRROR_CLAMP_TO_EDGE);

		TEST_INVOKE_REDUNDANTLY(TexParameterIiv, GL_TEXTURE_2D, GL_TEXTURE_BORDER_COLOR, &colori[10]);
		assert(param.bordercolor.r == colori[10] && param.bordercolor.g == colori[11] && param.bordercolor.b == colori[12] && param.bordercolor.a == colori[13]);
		TEST_INVOKE_REDUNDANTLY(TextureParameterIiv, tex[1], GL_TEXTURE_BORDER_COLOR, &colori[20]);
		assert(param.bordercolor.r == colori[20] && param.bordercolor.g == colori[21] && param.bordercolor.b == colori[22] && param.bordercolor.a == colori[23]);
		TEST_INVOKE_REDUNDANTLY(TexParameterIuiv, GL_TEXTURE_2D, GL_TEXTURE_BORDER_COLOR, &colorui[30]);
		assert(param.bordercolor.r == colorui[30] && param.bordercolor.g == colorui[31] && param.bordercolor.b == colorui[32] && param.bordercolor.a == colorui[33]);
		TEST_INVOKE_REDUNDANTLY(TextureParameterIuiv, tex[1], GL_TEXTURE_BORDER_COLOR, &colorui[40]);
		assert(param.bordercolor.r == colorui[40] && param.bordercolor.g == colorui[41] && param.bordercolor.b == colorui[42] && param.bordercolor.a == colorui[43]);
		TEST_INVOKE_REDUNDANTLY(TexParameteriv, GL_TEXTURE_2D, GL_TEXTURE_BORDER_COLOR, &colori[10]);
		assert(param.bordercolor.r == colori[10] && param.bordercolor.g == colori[11] && param.bordercolor.b == colori[12] && param.bordercolor.a == colori[13]);
		TEST_INVOKE_REDUNDANTLY(TextureParameteriv, tex[1], GL_TEXTURE_BORDER_COLOR, &colori[20]);
		assert(param.bordercolor.r == colori[20] && param.bordercolor.g == colori[21] && param.bordercolor.b == colori[22] && param.bordercolor.a == colori[23]);
		assert(stats.commands.redundant == 55);
		
		TEST_INVOKE(DeleteTextures, 11, tex);
		assert(state.texture.textures.size() == 0);
		TEST_INVOKE(DeleteTextures, 5, badids);
		
		assert(stats.commands.all == 223);
		assert(stats.commands.texture == 220);
		assert(stats.commands.redundant == 55);
		assert(stats.state_changes == 67);
				
		assert(stats.memory.allocated == 688628);
		assert(stats.memory.allocated_texture == 688528);
		assert(stats.memory.allocations == 34);
		assert(stats.memory.allocations_texture == 33);
		assert(stats.memory.deallocated == 688628);
		assert(stats.memory.deallocated_texture == 688528);
		assert(stats.memory.deallocations == 34);
		assert(stats.memory.deallocations_texture == 33);
		assert(stats.memory.current == 0);

		assert(stats.memory.transfers_cpu == 64);
		assert(stats.memory.transfers_cpu_size == 17980);
		assert(stats.memory.transfers_gpu == 8);
		assert(stats.memory.transfers_gpu_size ==530);
		totalstats.add(dc.getStats());
	}	
	if (check_transform_feedback) {
		std::cout << "***********************************************************************************************" << std::endl;
		std::cout << "******************************************* TRANSFORM FEEDBACK ********************************" << std::endl;
		std::cout << "***********************************************************************************************" << std::endl;
		dc.resetStats();
		auto& stf = state.transform_feedback.transform_feedbacks;
		GLuint xfb[5]; 
		TEST_INVOKE(GenTransformFeedbacks, 3, xfb);
		assert(stf.size() == 3);
		TEST_INVOKE(CreateTransformFeedbacks, 2, &xfb[3]);
		assert(stf.size() == 5);
		
		GLuint buf[2];
		TEST_INVOKE(GenBuffers, 2, buf);
		assert(state.buffer.buffers.size() == 2);
		TEST_INVOKE(BindBuffer, GL_COPY_READ_BUFFER, buf[0]);
		TEST_INVOKE(BufferData, GL_COPY_READ_BUFFER, 128, nullptr, GL_STATIC_DRAW);
		TEST_INVOKE(BindBuffer, GL_COPY_READ_BUFFER, buf[1]);
		TEST_INVOKE(BufferData, GL_COPY_READ_BUFFER, 128, nullptr, GL_STATIC_DRAW);

		TEST_INVOKE_REDUNDANTLY(TransformFeedbackBufferBase, xfb[3], 1, buf[0]);
		auto& stfx = stf.at(xfb[3]);
		assert(stfx.id == xfb[3] && stfx.target_buffers[1].id == buf[0] && stfx.target_buffers[1].offset == 0 && stfx.target_buffers[1].size == 128);
		TEST_INVOKE_REDUNDANTLY(TransformFeedbackBufferRange, xfb[4], 2, buf[1], 4, 16);
		auto& stfxx = stf.at(xfb[4]);
		assert(stfxx.id == xfb[4] && stfxx.target_buffers[2].id == buf[1] && stfxx.target_buffers[2].offset == 4 && stfxx.target_buffers[2].size == 16);
		
		TEST_INVOKE_REDUNDANTLY(BindTransformFeedback, GL_TRANSFORM_FEEDBACK, xfb[4]);
		//no shader active
		//TEST_INVOKE(BeginTransformFeedback, GL_POINTS);
		//TEST_INVOKE_REDUNDANTLY(PauseTransformFeedback);
		//TEST_INVOKE_REDUNDANTLY(ResumeTransformFeedback);
		//TEST_INVOKE(EndTransformFeedback);

		TEST_INVOKE(DeleteBuffers, 2, buf);
		assert(state.buffer.buffers.size() == 0);

		TEST_INVOKE(DeleteTransformFeedbacks, 3, xfb);
		assert(state.transform_feedback.transform_feedbacks.size() == 2);
		TEST_INVOKE(DeleteTransformFeedbacks, 2, &xfb[3]);
		assert(state.transform_feedback.transform_feedbacks.size() == 0);
		TEST_INVOKE(BindTransformFeedback, GL_TRANSFORM_FEEDBACK, 0);

		GLuint badids[3] = { 0, 0, 0 };
		TEST_INVOKE(DeleteTransformFeedbacks, 3, badids);

		assert(stats.commands.all == 18);
		assert(stats.state_changes == 5);
		assert(stats.commands.transform_feedback == 12);
		assert(stats.commands.redundant == 4);
		assert(stats.objects_created == 7);
		assert(stats.objects_destroyed == 7);
		totalstats.add(dc.getStats());
	}

	if (check_shader) {
		std::cout << "***********************************************************************************************" << std::endl;
		std::cout << "******************************************* SHADER UNIFORM ************************************" << std::endl;
		std::cout << "***********************************************************************************************" << std::endl;
		dc.resetStats();

		std::string stringc = "#version 450\n layout(local_size_x = 8, local_size_y = 8, local_size_z = 8) in;"
			"uniform float f1[3];"
			"uniform vec2 f2[3];"
			"uniform vec3 f3[3];"
			"uniform vec4 f4[3];"
			"uniform int i1[3];"
			"uniform ivec2 i2[3];"
			"uniform ivec3 i3[3];"
			"uniform ivec4 i4[3];"
			"uniform uint u1[3];"
			"uniform uvec2 u2[3];"
			"uniform uvec3 u3[3];"
			"uniform uvec4 u4[3];"
			"uniform double d1[3];"
			"uniform dvec2 d2[3];"
			"uniform dvec3 d3[3];"
			"uniform dvec4 d4[3];"
			"uniform dmat2x2 dm22[3];"
			"uniform dmat2x3 dm23[3];"
			"uniform dmat3x2 dm32[3];"
			"uniform dmat2x4 dm24[3];"
			"uniform dmat4x2 dm42[3];"
			"uniform dmat3x4 dm34[3];"
			"uniform dmat4x3 dm43[3];"
			"uniform dmat4x4 dm44[3];"
			"uniform mat2x2 m22[3];"
			"uniform mat2x3 m23[3];"
			"uniform mat3x2 m32[3];"
			"uniform mat2x4 m24[3];"
			"uniform mat4x2 m42[3];"
			"uniform mat3x4 m34[3];"
			"uniform mat4x3 m43[3];"
			"uniform mat4x4 m44[3];"
			"uniform writeonly image2D something; //not executed so no harm done, just need the locations\n"
			"void main() { float x = 0;\n"
			"x = f1[0]*f2[0].x*f3[0].x*f4[0].x*i1[0]*i2[0].x*i3[0].x*i4[0].x*u1[0]*u2[0].x*u3[0].x*u4[0].x*float(d1[0])*float(d2[0].x)*float(d3[0].x)*float(d4[0].x); \n"
			"x = x*m22[0][0][0]*m23[0][0][0]*m32[0][0][0]*m24[0][0][0]*m42[0][0][0]*m34[0][0][0]*m43[0][0][0]*m44[0][0][0];\n"
			"x = x*float(dm22[0][0][0])*float(dm23[0][0][0])*float(dm32[0][0][0])*float(dm24[0][0][0])*float(dm42[0][0][0])*float(dm34[0][0][0])*float(dm43[0][0][0])*float(dm44[0][0][0]);\n"
			"imageStore(something, ivec2(gl_GlobalInvocationID.xy), vec4(x,x,x,x));\n"
			"}\n";

		GLuint progid, shaderid;
		progid = TEST_INVOKE(CreateProgram);
		shaderid = TEST_INVOKE(CreateShader, GL_COMPUTE_SHADER);
		GLint auxsize = (GLint)stringc.size();
		const char* auxstr = stringc.c_str();
		TEST_INVOKE(ShaderSource, shaderid, 1, &auxstr, &auxsize);
		TEST_INVOKE(CompileShader, shaderid);
		TEST_INVOKE(AttachShader, progid, shaderid);
		TEST_INVOKE(LinkProgram, progid);
		TEST_INVOKE(UseProgram, progid);
		const DescProgram& p = state.program.programs.at(progid);
		assert(p.link_status == GL_TRUE);
		
		//uniforms are shader data, not state, just checking stats
		GLdouble d[12] = { 432.0, 857.54, 934.9, 833.9, 432.0, 857.54, 934.9, 833.9, 432.0, 857.54, 934.9, 833.9 };
		GLfloat f[12] = { 4312.0f, 8517.54f, 9134.9f, 8313.9f, 4312.0f, 8517.54f, 9134.9f, 8133.9f, 4321.0f, 8517.54f, 1934.9f, 1833.9f };
		GLint i[12] = { 12, 32, 34, 34, 21, 23, 23, 432, 4, 43, 43, 43 };
		GLuint ui[12] = { 112, 332, 345, 634, 2321, 223, 123, 2432, 34, 543, 463, 243 };
		int loc = TEST_INVOKE(GetUniformLocation, progid, "d1");
		TEST_INVOKE_REDUNDANTLY(ProgramUniform1d, progid, loc, 84.3);
		TEST_INVOKE_REDUNDANTLY(Uniform1d, loc, 8444.3);
		TEST_INVOKE_REDUNDANTLY(ProgramUniform1dv, progid, loc, 3, d);
		TEST_INVOKE_REDUNDANTLY(Uniform1dv, loc, 3, d);

		loc = TEST_INVOKE(GetUniformLocation, progid, "f1");	
		TEST_INVOKE_REDUNDANTLY(ProgramUniform1f, progid, loc, 843.3f);
		TEST_INVOKE_REDUNDANTLY(Uniform1f, loc, 23.3f);
		TEST_INVOKE_REDUNDANTLY(ProgramUniform1fv, progid, loc, 3, f);
		TEST_INVOKE_REDUNDANTLY(Uniform1fv, loc, 3, f);

		loc = TEST_INVOKE(GetUniformLocation, progid, "i1");	
		TEST_INVOKE_REDUNDANTLY(ProgramUniform1i, progid, loc, 82);			
		TEST_INVOKE_REDUNDANTLY(Uniform1i, loc, 82);
		TEST_INVOKE_REDUNDANTLY(ProgramUniform1iv, progid, loc, 3, i);
		TEST_INVOKE_REDUNDANTLY(Uniform1iv, loc, 3, i);

		loc = TEST_INVOKE(GetUniformLocation, progid, "u1"); 
		TEST_INVOKE_REDUNDANTLY(ProgramUniform1ui, progid, loc, 12);
		TEST_INVOKE_REDUNDANTLY(Uniform1ui, loc, 12);
		TEST_INVOKE_REDUNDANTLY(ProgramUniform1uiv, progid, loc, 3, ui);
		TEST_INVOKE_REDUNDANTLY(Uniform1uiv, loc, 3, ui);


		loc = TEST_INVOKE(GetUniformLocation, progid, "d2");
		TEST_INVOKE_REDUNDANTLY(ProgramUniform2d, progid, loc, 24.3, 83);
		TEST_INVOKE_REDUNDANTLY(Uniform2d, loc, 8444.3, 74.3);
		TEST_INVOKE_REDUNDANTLY(ProgramUniform2dv, progid, loc, 3, d);
		TEST_INVOKE_REDUNDANTLY(Uniform2dv, loc, 3, d);

		loc = TEST_INVOKE(GetUniformLocation, progid, "f2");
		TEST_INVOKE_REDUNDANTLY(ProgramUniform2f, progid, loc, 24.3f, 83.f);
		TEST_INVOKE_REDUNDANTLY(Uniform2f, loc, 8444.3f, 74.3f);
		TEST_INVOKE_REDUNDANTLY(ProgramUniform2fv, progid, loc, 3, f);
		TEST_INVOKE_REDUNDANTLY(Uniform2fv, loc, 3, f);

		loc = TEST_INVOKE(GetUniformLocation, progid, "i2");
		TEST_INVOKE_REDUNDANTLY(ProgramUniform2i, progid, loc, 3, 4);
		TEST_INVOKE_REDUNDANTLY(Uniform2i, loc, 3, 2);
		TEST_INVOKE_REDUNDANTLY(ProgramUniform2iv, progid, loc, 3, i);
		TEST_INVOKE_REDUNDANTLY(Uniform2iv, loc, 3, i);

		loc = TEST_INVOKE(GetUniformLocation, progid, "u2");
		TEST_INVOKE_REDUNDANTLY(ProgramUniform2ui, progid, loc, 13, 24);
		TEST_INVOKE_REDUNDANTLY(Uniform2ui, loc, 23, 32);
		TEST_INVOKE_REDUNDANTLY(ProgramUniform2uiv, progid, loc, 3, ui);
		TEST_INVOKE_REDUNDANTLY(Uniform2uiv, loc, 3, ui);

		loc = TEST_INVOKE(GetUniformLocation, progid, "d3");
		TEST_INVOKE_REDUNDANTLY(ProgramUniform3d, progid, loc, 313.9, 24.21, 88.4);
		TEST_INVOKE_REDUNDANTLY(Uniform3d, loc, 23.4, 32.43, 832.9);
		TEST_INVOKE_REDUNDANTLY(ProgramUniform3dv, progid, loc, 3, d);
		TEST_INVOKE_REDUNDANTLY(Uniform3dv, loc, 3, d);

		loc = TEST_INVOKE(GetUniformLocation, progid, "f3");
		TEST_INVOKE_REDUNDANTLY(ProgramUniform3f, progid, loc, 13.0f, 24.0f, 38.9f);
		TEST_INVOKE_REDUNDANTLY(Uniform3f, loc, 39.0f, 23.0f, 88.0f);
		TEST_INVOKE_REDUNDANTLY(ProgramUniform3fv, progid, loc, 3, f);
		TEST_INVOKE_REDUNDANTLY(Uniform3fv, loc, 3, f);

		loc = TEST_INVOKE(GetUniformLocation, progid, "i3");
		TEST_INVOKE_REDUNDANTLY(ProgramUniform3i, progid, loc, 13, 24, 8);
		TEST_INVOKE_REDUNDANTLY(Uniform3i, loc, 23, 32, 0);
		TEST_INVOKE_REDUNDANTLY(ProgramUniform3iv, progid, loc, 3, i);
		TEST_INVOKE_REDUNDANTLY(Uniform3iv, loc, 3, i);

		loc = TEST_INVOKE(GetUniformLocation, progid, "u3");
		TEST_INVOKE_REDUNDANTLY(ProgramUniform3ui, progid, loc, 9, 13, 24);
		TEST_INVOKE_REDUNDANTLY(Uniform3ui, loc, 43, 23, 32);
		TEST_INVOKE_REDUNDANTLY(ProgramUniform3uiv, progid, loc, 3, ui);
		TEST_INVOKE_REDUNDANTLY(Uniform3uiv, loc, 3, ui);

		loc = TEST_INVOKE(GetUniformLocation, progid, "d4");
		TEST_INVOKE_REDUNDANTLY(ProgramUniform4d, progid, loc, 84.8, 483.9, 313.9, 24.21);
		TEST_INVOKE_REDUNDANTLY(Uniform4d, loc, 23.4, 32.43, 843.9, 348.9);
		TEST_INVOKE_REDUNDANTLY(ProgramUniform4dv, progid, loc, 3, d);
		TEST_INVOKE_REDUNDANTLY(Uniform4dv, loc, 3, d);

		loc = TEST_INVOKE(GetUniformLocation, progid, "f4");
		TEST_INVOKE_REDUNDANTLY(ProgramUniform4f, progid, loc, 184.8f, 483.9f, 313.9f, 24.21f);
		TEST_INVOKE_REDUNDANTLY(Uniform4f, loc, 23.4f, 432.43f, 8433.9f, 348.9f);
		TEST_INVOKE_REDUNDANTLY(ProgramUniform4fv, progid, loc, 3, f);
		TEST_INVOKE_REDUNDANTLY(Uniform4fv, loc, 3, f);

		loc = TEST_INVOKE(GetUniformLocation, progid, "i4");
		TEST_INVOKE_REDUNDANTLY(ProgramUniform4i, progid, loc, 1,2,3,1);
		TEST_INVOKE_REDUNDANTLY(Uniform4i, loc, 4, 9, 0, 32);
		TEST_INVOKE_REDUNDANTLY(ProgramUniform4iv, progid, loc, 3, i);
		TEST_INVOKE_REDUNDANTLY(Uniform4iv, loc, 3, i);

		loc = TEST_INVOKE(GetUniformLocation, progid, "u4");
		TEST_INVOKE_REDUNDANTLY(ProgramUniform4ui, progid, loc, 9, 2, 3, 1);
		TEST_INVOKE_REDUNDANTLY(Uniform4ui, loc, 76, 9, 0, 32);
		TEST_INVOKE_REDUNDANTLY(ProgramUniform4uiv, progid, loc, 3, ui);
		TEST_INVOKE_REDUNDANTLY(Uniform4uiv, loc, 3, ui);

		//matrix
		double md[60] = { 1.0, 20., 32.0, 43., 453., 453., 453., 43., 958., 8754.,
			1.0, 20., 32.0, 43., 453., 453., 453., 43., 958., 8754.,
			1.0, 20., 32.0, 43., 453., 453., 453., 43., 958., 8754.,
			1.0, 20., 32.0, 43., 453., 453., 453., 43., 958., 8754.,
			1.0, 20., 32.0, 43., 453., 453., 453., 43., 958., 8754.,
			1.0, 20., 32.0, 43., 453., 453., 453., 43., 958., 8754. };
		float mf[60] = { 1.0f, 20.f, 32.0f, 43.f, 453.f, 453.f, 453.f, 43.f, 958.f, 8754.f,
			1.0f, 20.f, 32.0f, 43.f, 453.f, 453.f, 453.f, 43.f, 958.f, 8754.f,
			1.0f, 20.f, 32.0f, 43.f, 453.f, 453.f, 453.f, 43.f, 958.f, 8754.f,
			1.0f, 20.f, 32.0f, 43.f, 453.f, 453.f, 453.f, 43.f, 958.f, 8754.f,
			1.0f, 20.f, 32.0f, 43.f, 453.f, 453.f, 453.f, 43.f, 958.f, 8754.f,
			1.0f, 20.f, 32.0f, 43.f, 453.f, 453.f, 453.f, 43.f, 958.f, 8754.f };
		loc = TEST_INVOKE(GetUniformLocation, progid, "dm22");
		TEST_INVOKE_REDUNDANTLY(ProgramUniformMatrix2dv, progid, loc, 3, GL_TRUE, md);
		TEST_INVOKE_REDUNDANTLY(UniformMatrix2dv, loc, 3, GL_FALSE, md);
		loc = TEST_INVOKE(GetUniformLocation, progid, "m22");
		TEST_INVOKE_REDUNDANTLY(ProgramUniformMatrix2fv, progid, loc, 3, GL_TRUE, mf);
		TEST_INVOKE_REDUNDANTLY(UniformMatrix2fv, loc, 3, GL_FALSE, mf);
		loc = TEST_INVOKE(GetUniformLocation, progid, "dm23");
		TEST_INVOKE_REDUNDANTLY(ProgramUniformMatrix2x3dv, progid, loc, 3, GL_TRUE, md);
		TEST_INVOKE_REDUNDANTLY(UniformMatrix2x3dv, loc, 3, GL_FALSE, md);
		loc = TEST_INVOKE(GetUniformLocation, progid, "m23");
		TEST_INVOKE_REDUNDANTLY(ProgramUniformMatrix2x3fv, progid, loc, 3, GL_TRUE, mf);
		TEST_INVOKE_REDUNDANTLY(UniformMatrix2x3fv, loc, 3, GL_FALSE, mf);
		loc = TEST_INVOKE(GetUniformLocation, progid, "dm24");
		TEST_INVOKE_REDUNDANTLY(ProgramUniformMatrix2x4dv, progid, loc, 3, GL_TRUE, md);
		TEST_INVOKE_REDUNDANTLY(UniformMatrix2x4dv, loc, 3, GL_FALSE, md);
		loc = TEST_INVOKE(GetUniformLocation, progid, "m24");
		TEST_INVOKE_REDUNDANTLY(ProgramUniformMatrix2x4fv, progid, loc, 3, GL_TRUE, mf);
		TEST_INVOKE_REDUNDANTLY(UniformMatrix2x4fv, loc, 3, GL_FALSE, mf);

		loc = TEST_INVOKE(GetUniformLocation, progid, "dm33");
		TEST_INVOKE_REDUNDANTLY(ProgramUniformMatrix3dv, progid, loc, 3, GL_TRUE, md);
		TEST_INVOKE_REDUNDANTLY(UniformMatrix3dv, loc, 3, GL_FALSE, md);
		loc = TEST_INVOKE(GetUniformLocation, progid, "m33");
		TEST_INVOKE_REDUNDANTLY(ProgramUniformMatrix3fv, progid, loc, 3, GL_TRUE, mf);
		TEST_INVOKE_REDUNDANTLY(UniformMatrix3fv, loc, 3, GL_FALSE, mf);
		loc = TEST_INVOKE(GetUniformLocation, progid, "dm32");
		TEST_INVOKE_REDUNDANTLY(ProgramUniformMatrix3x2dv, progid, loc, 3, GL_TRUE, md);
		TEST_INVOKE_REDUNDANTLY(UniformMatrix3x2dv, loc, 3, GL_FALSE, md);
		loc = TEST_INVOKE(GetUniformLocation, progid, "m32");
		TEST_INVOKE_REDUNDANTLY(ProgramUniformMatrix3x2fv, progid, loc, 3, GL_TRUE, mf);
		TEST_INVOKE_REDUNDANTLY(UniformMatrix3x2fv, loc, 3, GL_FALSE, mf);
		loc = TEST_INVOKE(GetUniformLocation, progid, "dm34");
		TEST_INVOKE_REDUNDANTLY(ProgramUniformMatrix3x4dv, progid, loc, 3, GL_TRUE, md);
		TEST_INVOKE_REDUNDANTLY(UniformMatrix3x4dv, loc, 3, GL_FALSE, md);
		loc = TEST_INVOKE(GetUniformLocation, progid, "m34");
		TEST_INVOKE_REDUNDANTLY(ProgramUniformMatrix3x4fv, progid, loc, 3, GL_TRUE, mf);
		TEST_INVOKE_REDUNDANTLY(UniformMatrix3x4fv, loc, 3, GL_FALSE, mf);


		loc = TEST_INVOKE(GetUniformLocation, progid, "dm44");
		TEST_INVOKE_REDUNDANTLY(ProgramUniformMatrix4dv, progid, loc, 3, GL_TRUE, md);
		TEST_INVOKE_REDUNDANTLY(UniformMatrix4dv, loc, 3, GL_FALSE, md);
		loc = TEST_INVOKE(GetUniformLocation, progid, "m44");
		TEST_INVOKE_REDUNDANTLY(ProgramUniformMatrix4fv, progid, loc, 3, GL_TRUE, mf);
		TEST_INVOKE_REDUNDANTLY(UniformMatrix4fv, loc, 3, GL_FALSE, mf);
		loc = TEST_INVOKE(GetUniformLocation, progid, "dm42");
		TEST_INVOKE_REDUNDANTLY(ProgramUniformMatrix4x2dv, progid, loc, 3, GL_TRUE, md);
		TEST_INVOKE_REDUNDANTLY(UniformMatrix4x2dv, loc, 3, GL_FALSE, md);
		loc = TEST_INVOKE(GetUniformLocation, progid, "m42");
		TEST_INVOKE_REDUNDANTLY(ProgramUniformMatrix4x2fv, progid, loc, 3, GL_TRUE, mf);
		TEST_INVOKE_REDUNDANTLY(UniformMatrix4x2fv, loc, 3, GL_FALSE, mf);
		loc = TEST_INVOKE(GetUniformLocation, progid, "dm43");
		TEST_INVOKE_REDUNDANTLY(ProgramUniformMatrix4x3dv, progid, loc, 3, GL_TRUE, md);
		TEST_INVOKE_REDUNDANTLY(UniformMatrix4x3dv, loc, 3, GL_FALSE, md);
		loc = TEST_INVOKE(GetUniformLocation, progid, "m43");
		TEST_INVOKE_REDUNDANTLY(ProgramUniformMatrix4x3fv, progid, loc, 3, GL_TRUE, mf);
		TEST_INVOKE_REDUNDANTLY(UniformMatrix4x3fv, loc, 3, GL_FALSE, mf);
		
		//TEST_INVOKE_REDUNDANTLY(UniformBlockBinding, progid, 2, 3);
		//assert(state.program.programs.at(progid).uniforms.uniform_block_to_local_block[2] == 3);
		//GLuint indices[4] = { 0,1,2,3 };
		//TEST_INVOKE_REDUNDANTLY(UniformSubroutinesuiv, GL_COMPUTE_SHADER, 4,  indices);

		TEST_INVOKE(UseProgram, 0);
		TEST_INVOKE(DeleteProgram, progid);
		TEST_INVOKE(DeleteShader, shaderid);

		assert(stats.commands.all == 244);
		assert(stats.state_changes == 2);
		assert(stats.commands.uniform == 200);
		assert(stats.commands.redundant == 0);	//data not state
		assert(stats.objects_created == 2);
		assert(stats.objects_destroyed == 2);
		totalstats.add(dc.getStats());
	}

	std::cout << "Ran the test with the following stats:" << std::endl;
	std::cout << totalstats.commands.all << " OpenGL commands, of which:" << std::endl;
	std::cout << totalstats.commands.attribute << " attribute commands" << std::endl;
	std::cout << totalstats.commands.blend << " blend commands" << std::endl;
	std::cout << totalstats.commands.buffer << " buffer commands" << std::endl;
	std::cout << totalstats.commands.compute << " compute commands" << std::endl;
	std::cout << totalstats.commands.culling << " culling and clipping commands" << std::endl;
	std::cout << totalstats.commands.debug << " debug commands" << std::endl;
	std::cout << totalstats.commands.depth << " depth commands" << std::endl;
	std::cout << totalstats.commands.deprecated << " deprecated commands" << std::endl;
	std::cout << totalstats.commands.draw << " draw commands" << std::endl;
	std::cout << totalstats.commands.enabler << " enabler commands" << std::endl;
	std::cout << totalstats.commands.framebuffer << " framebuffer commands" << std::endl;
	std::cout << totalstats.commands.get << " get commands" << std::endl;
	std::cout << totalstats.commands.image << " image commands" << std::endl;
	std::cout << totalstats.commands.program_shader << " program and shader commands" << std::endl;
	std::cout << totalstats.commands.pixel_storage << " pixel storage commands" << std::endl;
	std::cout << totalstats.commands.query << " query commands" << std::endl;
	std::cout << totalstats.commands.rasterization << " rasterization (related) commands" << std::endl;
	std::cout << totalstats.commands.redundant << " redundant commands ***(WARNING)***" << std::endl;
	std::cout << totalstats.commands.sampler << " sampler commands" << std::endl;
	std::cout << totalstats.commands.scissor << " scissor commands" << std::endl;
	std::cout << totalstats.commands.stencil << " stencil commands" << std::endl;
	std::cout << totalstats.commands.sync << " sync commands" << std::endl;
	std::cout << totalstats.commands.texture << " texture commands" << std::endl;
	std::cout << totalstats.commands.transform_feedback << " transform feedback commands" << std::endl;
	std::cout << totalstats.commands.uniform << " uniform commands" << std::endl;
	std::cout << "---------" << std::endl;
	std::cout << totalstats.memory.allocated << " allocated memory" << std::endl;
	std::cout << totalstats.memory.allocated_buffer << " allocated memory (buffers)" << std::endl;
	std::cout << totalstats.memory.allocated_texture << " allocated memory (textures)" << std::endl;
	std::cout << totalstats.memory.allocated_renderbuffer << " allocated memory (renderbuffers)" << std::endl;
	std::cout << totalstats.memory.deallocated << " deallocated memory" << std::endl;
	std::cout << totalstats.memory.deallocated_buffer << " deallocated memory (buffers)" << std::endl;
	std::cout << totalstats.memory.deallocated_texture << " deallocated memory (textures)" << std::endl;
	std::cout << totalstats.memory.deallocated_renderbuffer << " deallocated memory (renderbuffers)" << std::endl;
	std::cout << totalstats.memory.allocations << " number of all allocations" << std::endl;
	std::cout << totalstats.memory.deallocations << " number of all deallocations" << std::endl;
	std::cout << totalstats.memory.allocations_buffer << " number of all buffer allocations" << std::endl;
	std::cout << totalstats.memory.deallocations_buffer << " number of all buffer deallocations" << std::endl;
	std::cout << totalstats.memory.allocations_texture << " number of all texture allocations" << std::endl;
	std::cout << totalstats.memory.deallocations_texture << " number of all texture deallocations" << std::endl;
	std::cout << totalstats.memory.allocations_renderbuffer << " number of all renderbuffer allocations" << std::endl;
	std::cout << totalstats.memory.allocations_renderbuffer << " number of all renderbuffer deallocations" << std::endl;
	std::cout << totalstats.memory.current << " current memory (data only)" << std::endl;
	std::cout << totalstats.memory.transfers_cpu << " total number of gpu-cpu data transfers, totalling " << totalstats.memory.transfers_cpu_size << " bytes." << std::endl;
	std::cout << totalstats.memory.transfers_gpu << " total number of gpu-gpu data transfers, totalling  " << totalstats.memory.transfers_gpu_size << " bytes." << std::endl;
	std::cout << "---------" << std::endl;
	std::cout << totalstats.objects_created << " create/generated objects" << std::endl;
	std::cout << totalstats.objects_destroyed << " destroyed objects" << std::endl;
	std::cout << totalstats.program_changes << " total number of program changes (glUseProgramStages, glUseProgram)" << std::endl;
	std::cout << totalstats.pipeline_flushes << " total number of pipeline flushes (glFlush, glFinish, glUseProgram, glUseProgramStages)" << std::endl;
	std::cout << totalstats.pipeline_barriers << " total number of memory barriers (glMemoryBarrier, glMemoryBarrierByRegion, glTextureBarrier)" << std::endl;
	std::cout << totalstats.primitives_issued << " total number of primitives sent to GPU (without GPU amplification, indirect rendering and primitive restart but considering instancing)" << std::endl;
	std::cout << totalstats.vertices_issued << " total number of vertices sent to GPU (without GPU amplification and indirect rendering but considering instancing)" << std::endl;
	std::cout << totalstats.state_changes << " total number of state changes" << std::endl;
	std::cout << totalstats.errors << " total number of errors." << std::endl;
}

//-------------------------------------------------------------------------------------------------
int main(int argc, char* argv[]){

	testBasic();
	testGroupNames();
	testLogging(); 
	testCaps();
	testOpenGLfunctions();
	testStatsAndLeaks();

	std::cout << "Finished" << std::endl;
	std::cin.get();
	return 0;
}
