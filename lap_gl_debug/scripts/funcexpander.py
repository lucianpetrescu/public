#-------------------------------------------------------------------------------------------------------------------------------
# This function generates function skeletons from signatures (for lap_gl_debug)
# takes a list of function signatures and expands them to lap_gl functions
#
# Usage: python funcexpander.py
#-------------------------------------------------------------------------------------------------------------------------------

import os.path

   
#source / target
source = open('funcdef.txt','r')
target = open('funcsource.txt','w+')

#for each function declaration create a definition
lines = source.readlines()
for l in lines:
    if l.strip() == "":
        continue
    words = l.replace(';',',').replace(')',',').replace('(',',').split(',')

    returntype = ""
    funcname = ""
    arguments = []

    ################################################################################
    #signature
    length = len(words)
    for i in range (0,length):
        #type + function name
        if i == 0:
            #deterime returntype and funcname
            tokens = words[0].strip().split(" ")
            funcname = tokens[-1]
            for j in range(0, len(tokens)-1):
                returntype += tokens[j]+" "
            returntype = returntype.strip()

            #write part of signature
            target.write("\t\t" + returntype + " ")
            target.write("DebugContext::" + funcname +"(")
        #arguments
        elif i < length-2:
            #for each argument determine type and name
            argument = words[i].strip().rsplit('=')[0].strip()
            argtype = ""
            argname = ""
            delimpos = argument.rfind('*')
            if delimpos ==-1:
                delimpos = argument.rfind(' ')
            argname = argument[delimpos+1:len(argument)].strip()
            argtype = argument[0:delimpos+1].strip()
            if argname !="" and argtype !="" and i<length-5:
                arguments.append([argtype, argname])
            #print("["+argtype+"]["+argname+"]")

            #write part of signature
            target.write(argtype+" "+argname)
            if i < length-3:
                target.write(", ")
    target.write("){\n")

    for a in arguments:
        print(a[0] + "|" + a[1])

    ################################################################################
    #content
    
    #glUniformBlockBinding(program, uniformBlockIndex, uniformBlockBinding);	func_id = idUniformBlockBinding;
    if(returntype != "void"):
        target.write("\t\t\t" + returntype + " result = ")
    else:
        target.write("\t\t\t")
    target.write("gl"+funcname+"(")
    length = len(arguments)
    for i in range(0,length):
        target.write(arguments[i][1])
        if i < length-1:
            target.write(",")
    target.write("); \tfunc_id = id"+funcname+";\n")
    
    #if (opt_none) return;
    target.write("\t\t\tif (opt_none) return")
    if(returntype != "void"):
        target.write(" result")
    target.write(";\n")

    #if ((logger && logger_state[func_id]) || opt_safety_checks || opt_redundancy_checks) {
    #   func_result.str(""); 
    #   func_param.str(""); func_param << program << " " << uniformBlockIndex << " " << uniformBlockBinding;
    #}
    target.write("\t\t\tif ((logger && logger_state[func_id]) || opt_safety_checks || opt_redundancy_checks) {\n")
    target.write("""\t\t\t\tfunc_result.str("");""")
    if returntype != "void":
        if returntype == "GLenum":
            target.write(" func_result << getName[result];\n")
        elif returntype == "GLboolean":
            target.write(" func_result << getNameTF(result);\n")
        else:
            target.write(" func_result << result;\n")
    else:
        target.write("\n")
    target.write("""\t\t\t\tfunc_param.str(""); """)
    length = len(arguments)
    if length > 0:
        target.write("func_param")
        for i in range(0,length):
            argtype = arguments[i][0]
            argname = arguments[i][1]
            target.write("""<< " " << """)
            if argtype == "GLenum":
                target.write("getName["+arguments[i][1]+"]")
            elif argtype == "GLboolean":
                target.write("getNameTF("+arguments[i][1]+")")
            else:
                target.write(arguments[i][1])
        target.write(";")
    target.write("\n\t\t\t}\n")
    
    #InternalLog(file, func, line);
    #InternalSafetyCheck();
    #if (opt_stats) {
    #   stats.commands.all++;
    #   stats.commands.get++;
    #   stats.commands.deprecated++
    #}
    target.write("""\t\t\tInternalLog(file, func, line);
    \t\tInternalSafetyCheck();
    \t\tif (opt_stats) {
    \t\t\tstats.commands.all++;
    \t\t\tstats.commands.deprecated++;
    \t\t}\n""")

    #final return for non void types
    if(returntype != "void"):
        target.write("\t\t\treturn result;\n")

    target.write("\t\t}\n\n")
source.close()
target.close()

    
   

