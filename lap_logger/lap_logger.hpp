///---------------------------------------------------------------------------------------------------------------------
/// The MIT License (MIT)
/// 
/// Copyright (c) 2016 Lucian Petrescu, lucian (dot) petrescu (dot) 24 (at) gmail (dot) com
/// 
/// Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation 
/// files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, 
/// modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software 
/// is furnished to do so, subject to the following conditions:
/// 
/// The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
/// 
/// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
/// OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE 
/// LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR 
/// IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
///---------------------------------------------------------------------------------------------------------------------

///------------------------------------------------------------------------------------------------
/// @file
/// @mainpage LAP Logger
/// LAP Logger (LAPLOG) is a fast, thread-safe, channel-based logger with formatting support, which
/// supports both printf style outputting and stream style outputting. Compared to other micro loggers LAPLOG 
/// uses channels to finely control the logging process, making it easy to control the text output of multiple
/// systems. A number of default channels are also provided. LAPLOG uses a special output thread to minimize lock 
/// contention, offering logging performance comparable to raw printf. The logger can output formatted headers for
/// each message, with various types of default header types containing information about time, filename, function name,
/// line number or channel name. The headers can also be aligned.
///	@version 1.01
///	@par Properties:
///		- c++11
///		- thread-safe
///		- no cpp exceptions (while more elegant, exceptions are not available / desired in / on some environments / platforms)
///		- channel based
///		- provides a custom assert, which uses this logger, e.g LAP_ASSERT(0==1, "expected assert")
///		- can log printf-like (LOG) or stream-like (<<) (LOGS), "s" comes from stream.
///			- e.g. LOG(LOGFTL, "Something fatal happened: %s", string_about_bad);
///			- e.g. LOGS(LOGINF) << "Something intereseting happened "<<interseting<< "\n"<<std::endl;
///		- default channels (in lap namespace):
///			- LOGFTL(0 , "FATAL");	, use this to log fatal errors, usually just before a crash (e.g. assert)
///			- LOGERR(1 , "ERROR");	, use this to log errors (e.g. could not connect to database/server)
///			- LOGWRN(2 , "WARNING");, warning, use this to log warnings (e.g. could not find file)
///			- LOGINF(3 , "INFO");	, info, use this to log general information (e.g. monitoring)
///			- LOGDBG(4 , "DEBUG");	, debug, use this to log debugging information
///			- LOGVRB(5 , "VERBOSE");, verbose, use this to log verbose
///			- LOGTRC(6 , "TRACE");	, trace, tracing logging channel
///			- NOTE: initially only LOGFTL is enabled, with the output target on std::cout, the other default channels can be
///			either manually registered or registered in one command with Logger::setDefault(), which also closes all
///			other channels.
///		- logging is done through a separate thread, which flushes all outputs at regular intervals (default 100ms)
///			channels are also flushed on close and program exit. The flush interval can be changed.
///		- logger uses channels ,e.g. LOG(INFO,...) LOG("INFO", ...) LOGS(INFO)<<... LOGS("INFO")<< ... LOGS(3)<< 
///		- the logger can use integer, strings and predefined/custom Channels to open/close channels. 
///			This property is especially useful for multi-system applications (e.g.: channel "AUDIO", "RENDERER", "FILESYSTEM"
///			, etc). Without hardcoding, mapping a name to a number could be done in either O(1) with high space complexity through 
///			a dictionary/hashtables (std::unordered map) or in O(log(num names)) through a left leaning red black tree (std::map). 
///			Because the number of channels is expected to be very small LAPLOG computes this mapping process with LLRB trees.
///			The time complexity of this operation is dwarfed by the IO time.
///		- each channel is opened with one or more output targets (std::ostream, which is a base class for std::stringstream, 
///			std::fstream, std::ofstream, std::ostringstream) or FILE. Since the problem of ownership can be put in multiple ways 
///			for a logger (logger has exclusive|shared|temporary exclusive|temporary shared ownership) the openChannel functions 
///			receive the output targets as shared_ptrs (these can be nullptr inside). It is encouraged to use the custom deleter 
///			functions provided in the logger FILEdeleter and OSTREAMdeleter, as these will close the file/stream on destruction 
///			and will ignore the deletion of stdout, stdin, stderr, std::cout and std::cin (making the api homogenous). On the 
///			other hand if the employed ownership model is guaranteed to be controlled and temporary a non-custom deleter shared 
///			pointer should suffice as long as streams are not overwritten
///		- the lack of wide character support is intended, as std::string can hold (in UTF-8 form) any type of string
///			see http://utf8everywhere.org/ for more UTF issues. Logger uses only std::string equality comparisons which will
///			work on any stored UTF-8 byte stream. For string comparisons a cast to basic_string over char32_t is ncessary.
///		- the longest logging command should generate a byte stream less than DEFAULT_PRINT_BUFFER_SIZE(4096).
///		- the logger can add a header to every outputted information, the structure of this header can be specified with
///			There are 2 types of headers, align(with aligned, formatted, entries) and normal. Formatting space depends on
///			the console/file viewer font. While aligned header formats use more space, they make it easy to read varied 
///			entries (with varying channel names, filenames, numbers, thread numbers, etc)
///			Some examples of predefined headers:
///			- Logger::HEADER_NONE								->...message goes here...
///			- Logger::HEADER_THREAD								->[94372] ...message goes here...
///			- Logger::HEADER_CHANNEL							->[INFO=3] ...message goes here...
///			- Logger::HEADER_CHANNEL_THREAD						->[INFO=3][94372] ...message goes here...
///			- Logger::HEADER_CHANNEL_FILE_LINE_FUNC_THREAD		->[INFO=3][filename.cpp][20][void function(int, int)][94372] ...message goes here...
///			- Logger::HEADER_THREAD_ALIGN						->[94372]	 ...message goes here...
///			- Logger::HEADER_CHANNEL_ALIGN						->[INFO=3]		 ...message goes here...
///			- Logger::HEADER_CHANNEL_FILE_LINE_ALIGN			->[INFO=3]		[filename.cpp]	[20]	 ...message goes here...
///			- Logger::HEADER_CHANNEL_FILE_LINE_THREAD_ALIGN		->[INFO=3]		[filename.cpp]	[20]	[94372] ...message goes here...
///			- by default HEADER_CHANNEL_ALIGN is used.
///		- the logger offers competitive logging times to raw no-channels fprintf in single-threaded setups, on a very large 
///			volume (probably not doing realtime with that volume). The logger is slightly faster than a raw no-channels fprintf 
///			in multi-threaded high volume setups because it has less lock contention, caused by storing the messages internally 
///			and outputting only at regular intervals instead of per-message.
///		-no cpp exceptions(while more elegant, exceptions are not available / desired in / on some environments / platforms)
///		-comes with a logger-enabled assert macro, LAP_ASSERT. the macro can be disabled by defining LAP_NO_ASSERT before including
///			lap_logger, working like NDEBUG for assert.
///		- defines a macro, LAP_FUNC, with the name of the current function (platform independent for FUNC, FUNCTION, etc)
///
///	@author Lucian Petrescu( lucian (dot) petrescu (dot) 24 (at) gmail (dot) com )
///
///	@par Usage:
///		- basic usage
///		@code
///		#include "lap_logger.hpp"
///		....
///		lap::Logger::getInstance().setDefault(true, true);
///		...
///		LOG(lap::LOGFTL, "FATAL some format text including string %s, int %d, float %f, scientific %e\n", "m s a a", 329, 3.1416, 3.1416);
///		LOGS(lap::LOGFTL) << "FATAL some format text including string " << "m s a a" << " int " << 329 << ", float " << 3.1416 << ", scientific " << 3.1416 << std::endl;
///		LOG(2, "WARNING some text %s\n", "some other text");
///		LOGS("DEBUG")<<" some debug text "<<1<<" "<<2<<" "<<3<<" and support for endline"<<std::endl;
///		@endcode
///		- assert usage
///		@code
///		// #define LAP_NO_ASSERT				//disable LAP_ASSERT, just like NDEBUG disables assert.
///		#include "lap_logger.hpp"
///		...
///		lap::Logger::getInstance().setDefault(true, true);
///		...
///		LAP_ASSERT(1 == 1, "true");
///		@endcode
///		- custom channels
///		@code
///		#include "lap_logger.hpp"
///		...
///		lap::Logger::Channel ch5(4532, "paris");
///		lap::Logger::Channel ch3(11, "berlin");
///		lap::Logger::Channel ch1(1999, "london");
///		lap::Logger::getInstance().openChannel(ch5, std::shared_ptr<std::ostream>(&std::cout, lap::Logger::OSTREAMdeleter));
///		lap::Logger::getInstance().openChannel(ch3, std::shared_ptr<std::ostream>(&std::cout, lap::Logger::OSTREAMdeleter));
///		lap::Logger::getInstance().openChannel(ch1, "london.log", false);
///		LOG(11, "a european city ... to stdout\n");
///		LOG("berlin", "the same european city ... to stdout ... when all channels are closed std::cout won't be deleted because OSTREAMdeleter will not close it. \n");
///		LOG("paris", "another european city ... to stdout\n");
///		LOGS(1999)<< "this european city goes to london.log\n");
///		lap::Logger::getInstance().openChannel(ch1, "london.log", false);
///		LOGS(1999)<< "just reopened this channel, now it this message goes to london.log and std::cout\n");
///		lap::Logger::Channel ch9(85, "rome");
///		lap::Logger::getInstance().openChannel(ch9, std::shared_ptr<std::file>(stdout, lap::Logger::FILEdeleter));
///		LOGS("rome")<< "a new european city ... to stdout ... when all channels are closed stdout won't be deleted because FILEdeleter will not close it or stderr. "<<std::endl;
///		lap::Logger::pauseChannel(ch9);
///		LOGS("rome")<<" will not print"<<std::endl;
///		@endcode
///		- custom headers
///		@code
///		lap::Logger::setHeaderType(HEADER_TIME_CHANNEL_FILE_ALIGN);
///		lap::Logger::Channel ch600(600, "moscow");
///		lap::Logger::getInstance().openChannel(ch600,std::shared_ptr<std::file>(stdout, lap::Logger::FILEdeleter));
///		LOGS(600)<<" will output 2016-03-03 13:01:50  [moscow|600]    ["call_filename.cpp"]   your message goes here"<<std:endl;
///		@endcode
///------------------------------------------------------------------------------------------------

#ifndef LAP_LOGGER_H_
#define LAP_LOGGER_H_

#include <cassert>
#include <cstdio>
#include <cstdarg>
#include <mutex>
#include <vector>
#include <list>
#include <sstream>
#include <string>
#include <thread>
#include <functional>
#include <map>
#include <memory>
#include <iostream>
#if defined(__STDC_LIB_EXT1__) && !defined(_MSC_VER)
	//set flag for localtime_s, but not on windows, where it is guaranteed to exist
	#define __STDC_WANT_LIB_EXT1__ 1
#endif
#include <ctime>

///------------------------------------------------------------------------------------------------
//platform specific includes
/// function macro, inspired from http://www.boost.org/doc/libs/1_59_0/boost/current_function.hpp 
#ifndef LAP_FUNC
	#if (defined(__cplusplus) && (__cplusplus >= 201103)) || (defined(__STDC_VERSION__) && (__STDC_VERSION__ >= 199901))
		#define LAP_FUNC __func__
	#elif defined(__GNUC__) || (defined(__MWERKS__) && (__MWERKS__ >= 0x3000)) || (defined(__ICC) && (__ICC >= 600)) || defined(__ghs__) || (defined(__DMC__) && (__DMC__ >= 0x810))
		#define LAP_FUNC __PRETTY_FUNCTION__
	#elif defined(__FUNCSIG__) || defined(_MSC_VER)
		#define LAP_FUNC __FUNCSIG__
	#elif (defined(__INTEL_COMPILER) && (__INTEL_COMPILER >= 600)) || (defined(__IBMCPP__) && (__IBMCPP__ >= 500))
		#define LAP_FUNC __FUNCTION__
	#elif defined(__BORLANDC__) && (__BORLANDC__ >= 0x550)
		#define LAP_FUNC __FUNC__
	#else
		"(unknown)"
	#endif
#endif




namespace lap{
	
	///--------------------------------------------------------------------------------------------
	/// LAPLOG
	class Logger{
	public:
	
		///--------------------------------------------------------------------------------------------
		/// a logging channel.\n
		///	It is defined by both a number and a name and can be directly used in the logging process, 
		/// for example all the following lines of codes produce the :\n
		/// LOG(Channel(myint, mystring))<<"some text"<<std::endl;
		/// LOG(myint) <<"some text"<<std::endl;
		/// LOG(mystring) <<"some text"<<std::endl;
		class Channel{
			const static int TYPE_NONE = 0;
			const static int TYPE_NUMBER = 1;
			const static int TYPE_NAME = 2;
		public:
			/// creates an empty channel (shouldn't be used directly)
			Channel();
			/// creates a channel identified only by a number
			/// @param number 	the number of the channel
			explicit Channel(unsigned int number);
			/// creates a channel identified only by a number
			/// @param number 	the number of the channel
			Channel(int number);
			/// creates a channel identified only by its name
			/// @param name 	the name of the channel
			explicit Channel(const char* name);
			/// creates a channel identified only by its name
			/// @param name 	the name of the channel
			explicit Channel(const std::string& name);
			/// creates a channel identified by its name and by its number
			/// @param number 	the number of the channel
			/// @param name 	the name of the channel
			Channel(unsigned int number, const char* name);
			/// creates a channel identified by its name and by its number
			/// @param number 	the number of the channel
			/// @param name 	the name of the channel
			Channel(unsigned int number, const std::string& name);
			Channel(const Channel& rhs)=default;
			Channel(Channel&& rhs) = default;
			Channel& operator=(const Channel& rhs) =default;
			Channel& operator=(Channel&& rhs) = default;
			bool operator==(const Channel& rhs) const;
			/// returns the header string of the channel
			/// e.g. name|number or name or number, depending on  thechannel structure
			/// @return the header string of the channel
			std::string getHeaderString() const;
			unsigned int type;
			unsigned int number;
			std::string name;
		};
	

		///--------------------------------------------------------------------------------------------
		/// header type
		/// if this class is to be extended than the Logger::internalPrintHeader has to be extended too,
		class HeaderType{
		public:
			const static unsigned int NTHREAD = 1;	///< format for normal thread
			const static unsigned int ATHREAD = 2;	///< format for aligned thread
			const static unsigned int NCHANNEL = 3;	///< format for normal channel
			const static unsigned int ACHANNEL = 4;	///< format for aligned channel
			const static unsigned int NTIME = 5;	///< format for normal date+time
			const static unsigned int ATIME = 6;	///< format for aligned date+time
			const static unsigned int NFILE = 7;	///< format for normal file
			const static unsigned int AFILE = 8;	///< format for aligned file
			const static unsigned int NLINE = 9;	///< format for normal line
			const static unsigned int ALINE = 10;	///< format for aligned line
			const static unsigned int NFUNC = 11;	///< format for normal func
			const static unsigned int AFUNC = 12;	///< format for aligned func

			/// creates an empty header
			HeaderType();
			/// creates a single descriptor header
			/// @param d1 the first descriptor 
			HeaderType(unsigned int d1);
			/// creates a double descriptor header
			/// @param d1 the first descriptor 
			/// @param d2 the second descriptor 
			HeaderType(unsigned int d1, unsigned int d2);
			/// creates a triple descriptor header
			/// @param d1 the first descriptor 
			/// @param d2 the second descriptor 
			/// @param d3 the third descriptor 
			HeaderType(unsigned int d1, unsigned int d2, unsigned int d3);
			/// creates a quadruple descriptor header
			/// @param d1 the first descriptor 
			/// @param d2 the second descriptor 
			/// @param d3 the third descriptor 
			/// @param d4 the fourth descriptor 
			HeaderType(unsigned int d1, unsigned int d2, unsigned int d3, unsigned int d4);
			/// creates a quintuple descriptor header
			/// @param d1 the first descriptor 
			/// @param d2 the second descriptor 
			/// @param d3 the third descriptor 
			/// @param d4 the fourth descriptor
			/// @param d5 the fifth descriptor
			HeaderType(unsigned int d1, unsigned int d2, unsigned int d3, unsigned int d4, unsigned int d5);
			/// creates a sextuple descriptor header
			/// @param d1 the first descriptor 
			/// @param d2 the second descriptor 
			/// @param d3 the third descriptor 
			/// @param d4 the fourth descriptor
			/// @param d5 the fifth descriptor
			/// @param d6 the sixth descriptor
			HeaderType(unsigned int d1, unsigned int d2, unsigned int d3, unsigned int d4, unsigned int d5, unsigned int d6);
			HeaderType(const HeaderType& rhs) = default;
			HeaderType(HeaderType&& rhs) = default;
			HeaderType& operator=(const HeaderType& rhs) = default;
			HeaderType& operator=(HeaderType&& rhs) = default;
			bool operator==(const HeaderType& rhs) const;
			/// returns the descriptors of the header
			/// @return the descriptors of the header
			const std::vector<std::pair<unsigned int, std::string>>& getDescriptors() const;
			/// returns the existence of a time descriptor in the header
			/// @return the existence of a time descriptor in the header
			bool hasTime() const;
			/// returns the existence of a filename descriptor in the header
			/// @return the existence of a filename descriptor in the header
			bool hasFile() const;
			/// returns the existence of a line descriptor in the header
			/// @return the existence of a line descriptor in the header
			bool hasLine() const;
			/// returns the existence of a function descriptor in the header
			/// @return the existence of a function descriptor in the header
			bool hasFunc() const;
			/// returns the existence of a thread descriptor in the header
			/// @return the existence of a thread descriptor in the header
			bool hasThread() const;
			/// returns the existence of a channel descriptor in the header
			/// @return the existence of a channel descriptor in the header
			bool hasChannel() const;
		private:
			std::string internalParse(unsigned int d);
			std::vector < std::pair<unsigned int, std::string >> descriptors;
			bool has_time, has_file, has_line, has_func, has_thread, has_channel;
		};
		
		const static HeaderType HEADER_NONE;							///< header type with no content

		const static HeaderType HEADER_THREAD;							///< header type with no [threadID]
		const static HeaderType HEADER_CHANNEL;							///< header type with no [ChannelName|ChannelID]
		const static HeaderType HEADER_CHANNEL_THREAD;					///< header type with no [ChannelName|ChannelID][threadID]
		const static HeaderType HEADER_CHANNEL_FILE;					///< header type with no [ChannelName|ChannelID][filename]
		const static HeaderType HEADER_CHANNEL_FILE_THREAD;				///< header type with no [ChannelName|ChannelID][threadID][threadID]
		const static HeaderType HEADER_CHANNEL_FILE_LINE;				///< header type with no [ChannelName|ChannelID][filename][line]
		const static HeaderType HEADER_CHANNEL_FILE_LINE_THREAD;		///< header type with no [ChannelName|ChannelID][filename][line][threadID]
		const static HeaderType HEADER_CHANNEL_FILE_LINE_FUNC;			///< header type with no [ChannelName|ChannelID][filename][line][func]
		const static HeaderType HEADER_CHANNEL_FILE_LINE_FUNC_THREAD;	///< header type with no [ChannelName|ChannelID][filename][line][func][threadID]

		const static HeaderType HEADER_TIME_THREAD;						///< header type with no [date&time][threadID]
		const static HeaderType HEADER_TIME_CHANNEL;					///< header type with no [date&time][ChannelName|ChannelID]
		const static HeaderType HEADER_TIME_CHANNEL_THREAD;				///< header type with no [date&time][ChannelName|ChannelID][threadID]
		const static HeaderType HEADER_TIME_CHANNEL_FILE;				///< header type with no [date&time][ChannelName|ChannelID][filename]
		const static HeaderType HEADER_TIME_CHANNEL_FILE_THREAD;		///< header type with no [date&time][ChannelName|ChannelID][filename][thread]
		const static HeaderType HEADER_TIME_CHANNEL_FILE_LINE;			///< header type with no [date&time][ChannelName|ChannelID][filename][line]
		const static HeaderType HEADER_TIME_CHANNEL_FILE_LINE_THREAD;	///< header type with no [date&time][ChannelName|ChannelID][filename][line][thread]
		const static HeaderType HEADER_TIME_CHANNEL_FILE_LINE_FUNC;		///< header type with no [date&time][ChannelName|ChannelID][filename][line][func]
		const static HeaderType HEADER_TIME_CHANNEL_FILE_LINE_FUNC_THREAD;///< header type with no [date&time][ChannelName|ChannelID][filename][line][func]


		const static HeaderType HEADER_THREAD_ALIGN;						///< aligned header type with no [threadID]
		const static HeaderType HEADER_CHANNEL_ALIGN;						///< aligned header type with no [ChannelName|ChannelID]
		const static HeaderType HEADER_CHANNEL_THREAD_ALIGN;				///< aligned header type with no [ChannelName|ChannelID][threadID]
		const static HeaderType HEADER_CHANNEL_FILE_ALIGN;					///< aligned header type with no [ChannelName|ChannelID][filename]
		const static HeaderType HEADER_CHANNEL_FILE_THREAD_ALIGN;			///< aligned header type with no [ChannelName|ChannelID][threadID][threadID]
		const static HeaderType HEADER_CHANNEL_FILE_LINE_ALIGN;				///< aligned header type with no [ChannelName|ChannelID][filename][line]
		const static HeaderType HEADER_CHANNEL_FILE_LINE_THREAD_ALIGN;		///< aligned header type with no [ChannelName|ChannelID][filename][line][threadID]
		const static HeaderType HEADER_CHANNEL_FILE_LINE_FUNC_ALIGN;		///< aligned header type with no [ChannelName|ChannelID][filename][line][func]
		const static HeaderType HEADER_CHANNEL_FILE_LINE_FUNC_THREAD_ALIGN;	///< aligned header type with no [ChannelName|ChannelID][filename][line][func][threadID]

		const static HeaderType HEADER_TIME_THREAD_ALIGN;					///< aligned header type with no [date&time][threadID]
		const static HeaderType HEADER_TIME_CHANNEL_ALIGN;					///< aligned header type with no [date&time][ChannelName|ChannelID]
		const static HeaderType HEADER_TIME_CHANNEL_THREAD_ALIGN;			///< aligned header type with no [date&time][ChannelName|ChannelID][threadID]
		const static HeaderType HEADER_TIME_CHANNEL_FILE_ALIGN;				///< aligned header type with no [date&time][ChannelName|ChannelID][filename]
		const static HeaderType HEADER_TIME_CHANNEL_FILE_THREAD_ALIGN;		///< aligned header type with no [date&time][ChannelName|ChannelID][filename][thread]
		const static HeaderType HEADER_TIME_CHANNEL_FILE_LINE_ALIGN;		///< aligned header type with no [date&time][ChannelName|ChannelID][filename][line]
		const static HeaderType HEADER_TIME_CHANNEL_FILE_LINE_THREAD_ALIGN;	///< aligned header type with no [date&time][ChannelName|ChannelID][filename][line][thread]
		const static HeaderType HEADER_TIME_CHANNEL_FILE_LINE_FUNC_ALIGN;	///< aligned header type with no [date&time][ChannelName|ChannelID][filename][line][func]
		const static HeaderType HEADER_TIME_CHANNEL_FILE_LINE_FUNC_THREAD_ALIGN;///< aligned header type with no [date&time][ChannelName|ChannelID][filename][line][func]
		

	private:
		class ChannelTarget{
		public:
			ChannelTarget();
			ChannelTarget(const std::shared_ptr<FILE> &file, const std::shared_ptr<std::ostream> &ostream);
			ChannelTarget(const ChannelTarget&) =default;
			ChannelTarget(ChannelTarget&&)=default;
			ChannelTarget& operator=(const ChannelTarget& rhs) = default;
			ChannelTarget& operator=(ChannelTarget&& rhs) = default;
			std::shared_ptr<std::ostream> out_ostream;		
			std::shared_ptr<std::FILE> out_file;			
			bool active;
		};
		class Command{
		public:
			Command();
			Command(const std::shared_ptr<FILE> &file, const std::shared_ptr<std::ostream>& ostream);
			Command(const std::shared_ptr<FILE> &file, const std::shared_ptr<std::ostream>& ostream, const std::string& content);
			Command(const Command& rhs) = default;
			Command(Command&&) = default;
			Command& operator=(const Command& rhs) = default;
			Command& operator=(Command&& rhs) = default;
			std::shared_ptr<std::ostream> out_ostream;
			std::shared_ptr<std::FILE> out_file;
			std::string content;
		};
	private:
		const static unsigned int DEFAULT_THREAD_FLUSH_MS = 100;			// default logging interval
		const static unsigned int DEFAULT_LOCAL_BUFFER_SIZE = 4096;			// default 1 memory page (extra long <- utf8 storage)
		const static std::string DEFAULT_LOG_FILE;
		const static unsigned int CHANNEL_NONE_INT = -1;
		const static std::string CHANNEL_NONE_STR;
	
		Logger();
		Logger(const Logger&) = delete;
		Logger(Logger&&) = delete;
		Logger& operator=(const Logger&) = delete;
		Logger& operator=(Logger&&) = delete;
		~Logger();
		void internalFlush();
		void internalThreadFlush();
		std::shared_ptr<FILE> internalMakeSharedFILE(const char* filename, bool append);
		void internalPrintHeader(const std::string& channel, const char* filename, unsigned int line, const char* function, std::string& output);
		void internalCloseChannel(unsigned int number);
		void internalCloseChannel(const char* name);
		void internalActiveChannel(unsigned int number, bool active);
		void internalActiveChannel(const char* name, bool active);
	public:
		/// a deleter function for std::shared_ptr<FILE>
		/// e.g.: std::shared_ptr<FILE> (myfopenedFILE, FILEdeleter);
		/// @param file a FILE object (the one that is provided to the std::shared_ptr)
		static void FILEdeleter(FILE* file);
		/// a deleter function for std::ostream derived objects (sstream, fstream, ofstream, etc)
		/// @param stream an ostream derived object (the one that is provided to the std::shared_ptr)
		static void OSTREAMdeleter(std::ostream* stream);
		/// returns the instance of the logger (singleton)
		/// @return the (singleton) instance of the logger
		static Logger& getInstance();


		/// sets the default logging channels.If use_fileout is true then it sets the streams to also
		/// log into the specified (or standard) output file. This operations closes all already existing
		/// channels.
		/// @param use_stdout	if true the default channels will log to the standard out
		/// @param use_fileout	if true the default channels will log to the standard file
		/// @param filename		the log file	
		void setDefault(bool use_stdout, bool use_fileout, const std::string& filename = DEFAULT_LOG_FILE);

		/// opens a log channel
		/// closes and overwrites the previous channel associated with this channel's name or number
		/// @param channel		the new Channel
		/// @param filename		the log file name
		/// @param append		append to the log file (on true) or overwrite it (on false)
		void openChannel(const Channel& channel, const char* filename, bool append);
		/// opens a log channel
		/// closes and overwrites the previous channel associated with this channel's name or number
		/// @param channel		the new Channel
		/// @param out_file		a shared_ptr over FILE, which will be used as a log target
		void openChannel(const Channel& channel, const std::shared_ptr<FILE>& out_file);
		/// opens a log channel
		/// closes and overwrites the previous channel associated with this channel's name or number
		/// @param channel		the new Channel
		/// @param out_ostream	a shared_ptr over ostream
		void openChannel(const Channel& channel, const std::shared_ptr<std::ostream>& out_ostream);
		/// opens a log channel
		/// closes and overwrites the previous channel associated with this channel's name or number
		/// @param channel		the new Channel
		/// @param out_file		a shared_ptr over FILE, which will be used as a log target
		/// @param out_ostream	a shared_ptr over ostream, which will be used as a log target
		void openChannel(const Channel& channel, const std::shared_ptr<FILE>& out_file, const std::shared_ptr<std::ostream>& out_ostream);
		/// opens a log channel
		/// closes and overwrites the previous channel associated with this channel's name or number
		/// @param channel		the new Channel
		/// @param filename		the log file name
		/// @param append		append to the log file (on true) or overwrite it (on false)
		/// @param out_ostream	a shared_ptr over ostream, which will be used as a log target
		void openChannel(const Channel& channel, const char* filename, bool append, const std::shared_ptr<std::ostream>& out_ostream);


		/// opens a log channel
		/// closes and overwrites the previous channel associated with this channel's number and number
		/// @param number		the new channel number
		/// @param name			the new channel name
		/// @param filename		the log file name
		/// @param append		append to the log file (on true) or overwrite it (on false)
		void openChannel(unsigned int number, const char* name, const char* filename, bool append);
		/// opens a log channel
		/// closes and overwrites the previous channel associated with this channel's number and number
		/// @param number		the new channel number
		/// @param name			the new channel name
		/// @param out_file		a shared_ptr over FILE, which will be used as a log target
		void openChannel(unsigned int number, const char* name, const std::shared_ptr<FILE>& out_file);
		/// opens a log channel
		/// closes and overwrites the previous channel associated with this channel's number and number
		/// @param number		the new channel number
		/// @param name			the new channel name
		/// @param out_ostream	a shared_ptr over ostream
		void openChannel(unsigned int number, const char* name, const std::shared_ptr<std::ostream>& out_ostream);
		/// opens a log channel
		/// closes and overwrites the previous channel associated with this channel's number and number
		/// @param number		the new channel number
		/// @param name			the new channel name
		/// @param out_file		a shared_ptr over FILE, which will be used as a log target
		/// @param out_ostream	a shared_ptr over ostream, which will be used as a log target
		void openChannel(unsigned int number, const char* name, const std::shared_ptr<FILE>& out_file, const std::shared_ptr<std::ostream>& out_ostream);
		/// opens a log channel
		/// closes and overwrites the previous channel associated with this channel's number and number
		/// @param number		the new channel number
		/// @param name			the new channel name
		/// @param filename		the log file name
		/// @param append		append to the log file (on true) or overwrite it (on false)
		/// @param out_ostream	a shared_ptr over ostream, which will be used as a log target
		void openChannel(unsigned int number, const char* name, const char* filename, bool append, const std::shared_ptr<std::ostream>& out_ostream);

		/// opens a log channel
		/// closes and overwrites the previous channel associated with this channel's number
		/// @param number		the new channel number
		/// @param filename		the log file name
		/// @param append		append to the log file (on true) or overwrite it (on false)
		void openChannel(unsigned int number, const char* filename, bool append);
		/// opens a log channel
		/// closes and overwrites the previous channel associated with this channel's number
		/// @param number		the new channel number
		/// @param out_file		a shared_ptr over FILE, which will be used as a log target
		void openChannel(unsigned int number, const std::shared_ptr<FILE>& out_file);
		/// opens a log channel
		/// closes and overwrites the previous channel associated with this channel's number
		/// @param number		the new channel number
		/// @param out_ostream	a shared_ptr over ostream
		void openChannel(unsigned int number, const std::shared_ptr<std::ostream>& out_ostream);
		/// opens a log channel
		/// closes and overwrites the previous channel associated with this channel's number
		/// @param number		the new channel number
		/// @param out_file		a shared_ptr over FILE, which will be used as a log target
		/// @param out_ostream	a shared_ptr over ostream, which will be used as a log target
		void openChannel(unsigned int number, const std::shared_ptr<FILE>& out_file, const std::shared_ptr<std::ostream>& out_ostream);
		/// opens a log channel
		/// closes and overwrites the previous channel associated with this channel's number
		/// @param number		the new channel number
		/// @param filename		the log file name
		/// @param append		append to the log file (on true) or overwrite it (on false)
		/// @param out_ostream	a shared_ptr over ostream, which will be used as a log target
		void openChannel(unsigned int number, const char* filename, bool append, const std::shared_ptr<std::ostream>& out_ostream);

		/// opens a log channel
		/// closes and overwrites the previous channel associated with this channel's name 
		/// @param name			the new channel name
		/// @param filename		the log file name
		/// @param append		append to the log file (on true) or overwrite it (on false)
		void openChannel(const char* name, const char* filename, bool append);
		/// opens a log channel
		/// closes and overwrites the previous channel associated with this channel's name
		/// @param name			the new channel name
		/// @param out_file		a shared_ptr over FILE, which will be used as a log target
		void openChannel(const char* name, const std::shared_ptr<FILE>& out_file);
		/// opens a log channel
		/// closes and overwrites the previous channel associated with this channel's name
		/// @param name			the new channel name
		/// @param out_ostream	a shared_ptr over ostream
		void openChannel(const char* name, const std::shared_ptr<std::ostream>& out_ostream);
		/// opens a log channel
		/// closes and overwrites the previous channel associated with this channel's name
		/// @param name			the new channel name
		/// @param out_file		a shared_ptr over FILE, which will be used as a log target
		/// @param out_ostream	a shared_ptr over ostream, which will be used as a log target
		void openChannel(const char* name, const std::shared_ptr<FILE>& out_file, const std::shared_ptr<std::ostream>& out_ostream);
		/// opens a log channel
		/// closes and overwrites the previous channel associated with this channel's name
		/// @param name			the new channel name
		/// @param filename		the log file name
		/// @param append		append to the log file (on true) or overwrite it (on false)
		/// @param out_ostream	a shared_ptr over ostream, which will be used as a log target
		void openChannel(const char* name, const char* filename, bool append, const std::shared_ptr<std::ostream>& out_ostream);

		/// opens a log channel
		/// closes and overwrites the previous channel associated with this channel's name 
		/// @param name			the new channel name
		/// @param filename		the log file name
		/// @param append		append to the log file (on true) or overwrite it (on false)
		void openChannel(const std::string& name, const char* filename, bool append);
		/// opens a log channel
		/// closes and overwrites the previous channel associated with this channel's name
		/// @param name			the new channel name
		/// @param out_file		a shared_ptr over FILE, which will be used as a log target
		void openChannel(const std::string& name, const std::shared_ptr<FILE>& out_file);
		/// opens a log channel
		/// closes and overwrites the previous channel associated with this channel's name
		/// @param name			the new channel name
		/// @param out_ostream	a shared_ptr over ostream
		void openChannel(const std::string& name, const std::shared_ptr<std::ostream>& out_ostream);
		/// opens a log channel
		/// closes and overwrites the previous channel associated with this channel's name
		/// @param name			the new channel name
		/// @param out_file		a shared_ptr over FILE, which will be used as a log target
		/// @param out_ostream	a shared_ptr over ostream, which will be used as a log target
		void openChannel(const std::string& name, const std::shared_ptr<FILE>& out_file, const std::shared_ptr<std::ostream>& out_ostream);
		/// opens a log channel
		/// closes and overwrites the previous channel associated with this channel's name
		/// @param name			the new channel name
		/// @param filename		the log file name
		/// @param append		append to the log file (on true) or overwrite it (on false)
		/// @param out_ostream	a shared_ptr over ostream, which will be used as a log target
		void openChannel(const std::string& name, const char* filename, bool append, const std::shared_ptr<std::ostream>& out_ostream);
		
		/// closes a log channel
		/// @param channel		the Channel to be closed
		void closeChannel(const Channel& channel);
		/// closes a log channel
		/// @param name			the name of the channel to be closed
		void closeChannel(const std::string& name);
		/// closes a log channel
		/// @param number		the number of the channel to be closed
		void closeChannel(unsigned int number);
		/// closes a log channel
		/// @param name			the name of the channel to be closed
		void closeChannel(const char* name);
		/// closes all log channel
		void closeAllChannels();

		/// pauses all log channels
		void pauseAllChannels();
		/// resumes all log channels
		void resumeAllChannels();

		/// pauses all but one log channel
		/// @param channel	the Channel that will not be paused
		void pauseAllButChannel(const Channel& channel);
		/// pauses all but one log channel
		/// @param name		the name of the channel that will not be paused
		void pauseAllButChannel(const std::string& name);
		/// pauses all but one log channel
		/// @param name		the name of the channel that will not be paused
		void pauseAllButChannel(const char* name);
		/// pauses all but one log channel
		/// @param number	the number of the channel that will not be paused
		void pauseAllButChannel(unsigned int number);
		
		/// resumes all but one log channel
		/// @param channel	the Channel that will not be resumed
		void resumeAllButChannel(const Channel& channel);
		/// resumes all but one log channel
		/// @param name		the name of the channel that will not be resumed
		void resumeAllButChannel(const std::string& name);
		/// resumes all but one log channel
		/// @param name		the name of the channel that will not be resumed
		void resumeAllButChannel(const char* name);
		/// resumes all but one log channel
		/// @param number		the number of the channel that will not be resumed
		void resumeAllButChannel(unsigned int number);
		
		/// pauses a log channel
		/// @param channel	the Channel that will be paused
		void pauseChannel(const Channel& channel);
		/// pauses a log channel
		/// @param name		the name of the channel that will be paused
		void pauseChannel(const std::string& name);
		/// pauses a log channel
		/// @param number	the number of the channel that will be paused
		void pauseChannel(unsigned int number);
		/// pauses a log channel
		/// @param name		the name of the channel that will be paused
		void pauseChannel(const char* name);
		
		/// resumes a log channel
		/// @param number	the number of the channel that will be resumed
		void resumeChannel(unsigned int number);
		/// resumes a log channel
		/// @param name		the name of the channel that will be resumed
		void resumeChannel(const char* name);
		/// resumes a log channel
		/// @param name		the name of the channel that will be resumed
		void resumeChannel(const std::string& name);
		/// resumes a log channel
		/// @param channel	the Channel that will be resumed
		void resumeChannel(const Channel& channel);
		
		/// checks if a log channel is active
		/// @param channel	the Channel that will be tested
		bool isChannelActive(const Channel& channel);
		/// checks if a log channel is active
		/// @param name		the name of the channel that will be tested
		bool isChannelActive(const std::string& name);
		/// checks if a log channel is active
		/// @param name		the name of the channel that will be tested
		bool isChannelActive(const char* name);
		/// checks if a log channel is active
		/// @param number	the number of the channel that will be tested
		bool isChannelActive(unsigned int number);

		/// sets the flush interval
		/// @param ms	the duration in milliseconds between 2 consecutive flushes
		void setOutputFlushInterval(unsigned int ms);
		/// returns the flush interval
		/// @return the flush interval
		unsigned int getOutputFlushInterval();

		/// sets the HeaderType
		/// @param type		the new HeaderType 
		void setHeaderType(const HeaderType& type);
		/// returns the current HeaderType
		/// @return the current HeaderType
		HeaderType getHeaderType();
		
		/// immediately force flushes all the messages
		void flush();

		/// prints data to log targets (out, streams or files). THIS FUNCTION SHOULDN'T BE CALLED MANUALLY
		/// @param channel	the channel on which data is printed
		/// @param filename	the name of source file from which data is logged
		/// @param line		the line in the souce file from which data is logged
		/// @param function	the function in the source from which the data is logged
		/// @param format	the format of the logged data
		/// @param ...		the logged data
		void print(const Channel& channel, const char* filename, unsigned int line, const char* function, const char* format, ...);
		/// prints data to log targets (out, streams or files). THIS FUNCTION SHOULDN'T BE CALLED MANUALLY
		/// @param channel	the number of the channel on which data is printed
		/// @param filename	the name of source file from which data is logged
		/// @param line		the line in the souce file from which data is logged
		/// @param function	the function in the source from which the data is logged
		/// @param format	the format of the logged data
		/// @param ...		the logged data
		void print(int channel, const char* filename, unsigned int line, const char* function, const char* format, ...);
		/// prints data to log targets (out, streams or files). THIS FUNCTION SHOULDN'T BE CALLED MANUALLY
		/// @param channel	the name of the channel on which data is printed
		/// @param filename	the name of source file from which data is logged
		/// @param line		the line in the souce file from which data is logged
		/// @param function	the function in the source from which the data is logged
		/// @param format	the format of the logged data
		/// @param ...		the logged data
		void print(const char* channel, const char* filename, unsigned int line, const char* function, const char* format, ...);
		/// prints data to log targets (out, streams or files). THIS FUNCTION SHOULDN'T BE CALLED MANUALLY
		/// @param channel	the name of the channel on which data is printed
		/// @param filename	the name of source file from which data is logged
		/// @param line		the line in the souce file from which data is logged
		/// @param function	the function in the source from which the data is logged
		/// @param format	the format of the logged data
		/// @param ...		the logged data
		void print(const std::string& channel, const char* filename, unsigned int line, const char* function, const char* format, ...);

		/// prints data to log targets (out, streams or files). THIS FUNCTION SHOULDN'T BE CALLED MANUALLY
		/// @param channel	the channel on which data is printed
		/// @param filename	the name of source file from which data is logged
		/// @param line		the line in the souce file from which data is logged
		/// @param function	the function in the source from which the data is logged
		/// @param contents	the logged data
		void printStream(const Channel& channel, const char* filename, unsigned int line, const char* function, const char* contents);
		/// prints data to log targets (out, streams or files). THIS FUNCTION SHOULDN'T BE CALLED MANUALLY
		/// @param channel	the number of the channel on which data is printed
		/// @param filename	the name of source file from which data is logged
		/// @param line		the line in the souce file from which data is logged
		/// @param function	the function in the source from which the data is logged
		/// @param contents	the logged data
		void printStream(int channel, const char* filename, unsigned int line, const char* function, const char* contents);
		/// prints data to log targets (out, streams or files). THIS FUNCTION SHOULDN'T BE CALLED MANUALLY
		/// @param channel	the name of the channel on which data is printed
		/// @param filename	the name of source file from which data is logged
		/// @param line		the line in the souce file from which data is logged
		/// @param function	the function in the source from which the data is logged
		/// @param contents	the logged data
		void printStream(const char* channel, const char* filename, unsigned int line, const char* function, const char* contents);
		/// prints data to log targets (out, streams or files). THIS FUNCTION SHOULDN'T BE CALLED MANUALLY
		/// @param channel	the name of the channel on which data is printed
		/// @param filename	the name of source file from which data is logged
		/// @param line		the line in the souce file from which data is logged
		/// @param function	the function in the source from which the data is logged
		/// @param contents	the logged data
		void printStream(const std::string& channel, const char* filename, unsigned int line, const char* function, const char* contents);

	private:
		static Logger instance;

		HeaderType header_type;

		char local_buffer[DEFAULT_LOCAL_BUFFER_SIZE];

		std::mutex mutex;
		std::map<unsigned int, std::pair<std::string,ChannelTarget>> channel_int_to_target;
		std::map<std::string, std::pair<unsigned int, ChannelTarget>> channel_string_to_target;
		std::vector<Command> commands;		//commands for writer thread

		std::thread thread;
		unsigned int thread_flush_ms;
		bool thread_close_flag;
	};

	extern const Logger::Channel LOGFTL;					///< (0, "FATAL")  , use this to log fatal errors, usually just before a crash (e.g. assert)
	extern const Logger::Channel LOGERR;					///< (1, "ERROR")  , use this to log errors (e.g. could not connect to database/server)
	extern const Logger::Channel LOGWRN;					///< (2, "WARNING"), warning, use this to log warnings (e.g. could not find file)
	extern const Logger::Channel LOGINF;					///< (3, "INFO")   , info, use this to log general information (e.g. monitoring)
	extern const Logger::Channel LOGDBG;					///< (4, "DEBUG")  , debug, use this to log debugging information
	extern const Logger::Channel LOGVRB;					///< (5, "VERBOSE"), verbose, use this to log verbose
	extern const Logger::Channel LOGTRC;					///< (6, "TRACE")  , trace, trace logging channel

	namespace detail{
		//inline in class
		class LoggerStreamCollector : public std::ostringstream{
		public:
			const unsigned int TYPE_INT = 1;
			const unsigned int TYPE_CHARP = 2;
			LoggerStreamCollector(const Logger::Channel& ch, const char* f, unsigned int ln, const char* fn);
			LoggerStreamCollector(const char *ch, const char* f, unsigned int ln, const char* fn);
			LoggerStreamCollector(unsigned int ch, const char* f, unsigned int ln, const char* fn);
			LoggerStreamCollector(int ch, const char* f, unsigned int ln, const char* fn);
			~LoggerStreamCollector();
		private:
			union{
				const char* channel_c;
				unsigned int channel_i;
			};
			unsigned int type;
			const char* filename;
			unsigned int line;
			const char* function;
		};
	}

	/// log to channel (Channel | name | number), e.g.: LOG(lap::LOGFTL, "%s\n", "sometext");
#define LOG(CHANNEL,...) lap::Logger::getInstance().print(CHANNEL, __FILE__, __LINE__, LAP_FUNC, __VA_ARGS__)
/// log as stream to channel (Channel | name | number), e.g.: LOG(lap::LOGFTL) << "sometext"<<std::endl;
#define LOGS(CHANNEL) lap::detail::LoggerStreamCollector(CHANNEL, __FILE__, __LINE__, LAP_FUNC)




	///--------------------------------------------------------------------------------------------
	/// custom assert
	#ifdef LAP_NO_ASSERT
		#ifndef LAP_ASSERT
			#define LAP_ASSERT(condition , message) {}
		#endif
	#else
		#ifndef LAP_ASSERT
			#define LAP_ASSERT(condition, message)\
								if (!(condition)){\
									LOG(lap::LOGFTL,"***[ASSERTION FAILURE]***\n\t\t%s\n\t\tpress any key to terminate\n", message);\
									lap::Logger::getInstance().flush();\
									std::cin.get();\
									std::terminate();\
								}
		#endif	
	#endif
}

//include guard
#endif