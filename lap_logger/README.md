# README #

LAP Logger (LAPLOG) is a fast, thread-safe, channel-based logger with formatting support, which
supports both printf style outputting and stream style outputting. Compared to other micro loggers LAPLOG 
uses channels to finely control the logging process, making it easy to control the text output of multiple
systems. A number of default channels are also provided. LAPLOG uses a special output thread to minimize lock 
contention, offering logging performance comparable to raw printf. The logger can output formatted headers for
each message, with various types of default header types containing information about time, filename, function name,
line number or channel name. The headers can also be aligned.

version 1.01


### SCREENSHOTS ###

Logging with minimal headers (each channel is described by a name and number, e.g. FATAL = 0. Depending on setup the output can be all sent to a single logging stream or to multiple (per-group/per-channel/etc) streams.

 ![functions are bundled into groups](img/noheader.png) 

Types of predefined headers contain various combinations of: (date, time, filename, function, line-number, channel name, channel id) with or without alignment.

 ![functions are bundled into groups](img/types_of_headers.png) 

Logging with user defined channels (channel name = "roma", channel id = 982)

 ![functions are bundled into groups](img/user_channels.png) 

The logger supports assertions:

 ![functions are bundled into groups](img/assert.png) 

###PROPERTIES###
- c++11
- thread-safe
- no cpp exceptions (while more elegant, exceptions are not available/desired in/on some environments/platforms)
- channel based
- provides a custom assert, which uses this logger, e.g LAP_ASSERT(0==1, "expected assert")
- can log printf-like (LOG) or stream-like (<<) (LOGS), "s" comes from stream.
	- e.g. LOG(LOGFTL, "Something fatal happened: %s", string\_about\_bad);
	- e.g. LOGS(LOGINF) << "Something interesting happened "<<interesting<< "\n"<<std::endl;
- default channels (in lap namespace):
	- LOGFTL(0 , "FATAL");	, use this to log fatal errors, usually just before a crash (e.g. assert)
	- LOGERR(1 , "ERROR");	, use this to log errors (e.g. could not connect to database/server)
	- LOGWRN(2 , "WARNING");, warning, use this to log warnings (e.g. could not find file)
	- LOGINF(3 , "INFO");	, info, use this to log general information (e.g. monitoring)
	- LOGDBG(4 , "DEBUG");	, debug, use this to log debugging information
	- LOGVRB(5 , "VERBOSE");, verbose, use this to log verbose
	- LOGTRC(6 , "TRACE");	, trace, tracing logging channel
	- NOTE: initially only LOGFTL is enabled, with the output target on std::cout, the other default channels can be
		either manually registered or registered in one command with Logger::setDefault(), which also closes all
		other channels.
- logging is done through a separate thread, which flushes all outputs at regular intervals (default 100ms)
		channels are also flushed on close and program exit. The flush interval can be changed.
- logger uses channels ,e.g. LOG(INFO,...) LOG("INFO", ...) LOGS(INFO)<<... LOGS("INFO")<< ... LOGS(3)<< 
- the logger can use integer, strings and predefined/custom Channels to open/close channels. 
	This property is especially useful for multi-system applications (e.g.: channel "AUDIO", "RENDERER", "FILESYSTEM"
	, etc). Without hardcoding, mapping a name to a number could be done in either O(1) with high space complexity through 
	a dictionary/hashtables (std::unordered map) or in O(log(num names)) through a left leaning red black tree (std::map). 
	Because the number of channels is expected to be very small LAPLOG computes this mapping process with LLRB trees.
	The time complexity of this operation is dwarfed by the IO time.
- each channel is opened with one or more output targets (std::ostream, which is a base class for std::stringstream, 
	std::fstream, std::ofstream, std::ostringstream) or FILE. Since the problem of ownership can be put in multiple ways 
	for a logger (logger has exclusive|shared|temporary exclusive|temporary shared ownership) the openChannel functions 
	receive the output targets as shared_ptrs (these can be nullptr inside). It is encouraged to use the custom deleter 
	functions provided in the logger FILEdeleter and OSTREAMdeleter, as these will close the file/stream on destruction 
	and will ignore the deletion of stdout, stdin, stderr, std::cout and std::cin (making the api homogenous). On the 
	other hand if the employed ownership model is guaranteed to be controlled and temporary a non-custom deleter shared 
	pointer should suffice as long as streams are not overwritten
- the lack of wide character support is intended, as std::string can hold (in UTF-8 form) any type of string
	see http://utf8everywhere.org/ for more UTF issues. Logger uses only std::string equality comparisons which will
	work on any stored UTF-8 byte stream. For string comparisons a cast to basic_string over char32_t is ncessary.
- the longest logging command should generate a byte stream less than DEFAULT\_PRINT\_BUFFER\_SIZE(4096).
- the logger can add a header to every outputted information, the structure of this header can be specified with
	There are 2 types of headers, align(with aligned, formatted, entries) and normal. Formatting space depends on
	the console/file viewer font. While aligned header formats use more space, they make it easy to read varied 
	entries (with varying channel names, filenames, numbers, thread numbers, etc)
	Some examples of predefined headers:
	- Logger::HEADER\_NONE								->...message goes here...
	- Logger::HEADER\_THREAD								->[94372] ...message goes here...
	- Logger::HEADER\_CHANNEL							->[INFO=3] ...message goes here...
	- Logger::HEADER\_CHANNEL\_THREAD						->[INFO=3][94372] ...message goes here...
	- Logger::HEADER\_CHANNEL\_FILE\_LINE\_FUNC\_THREAD		->[INFO=3][filename.cpp][20][void function(int, int)][94372] ...message goes here...
	- Logger::HEADER\_THREAD\_ALIGN						->[94372]	 ...message goes here...
	- Logger::HEADER\_CHANNEL\_ALIGN						->[INFO=3]		 ...message goes here...
	- Logger::HEADER\_CHANNEL\_FILE\_LINE\_ALIGN			->[INFO=3]		[filename.cpp]	[20]	 ...message goes here...
	- Logger::HEADER\_CHANNEL\_FILE\_LINE\_THREAD\_ALIGN		->[INFO=3]		[filename.cpp]	[20]	[94372] ...message goes here...
	- by default HEADER\_CHANNEL\_ALIGN is used.
- the logger offers competitive logging times to raw no-channels fprintf in single-threaded setups, on a very large 
	volume (probably not doing realtime with that volume). The logger is slightly faster than a raw no-channels fprintf 
	in multi-threaded high volume setups because it has less lock contention, caused by storing the messages internally 
	and outputting only at regular intervals instead of per-message.
- no cpp exceptions (while more elegant, exceptions are not available/desired in/on some environments/platforms)
-comes with a logger-enabled assert macro, LAP\_ASSERT. the macro can be disabled by defining LAP\_NO\_ASSERT before including
	lap_logger, working like NDEBUG for assert.
- defines a macro, LAP\_FUNC, with the name of the current function (platform independent for \_\_func\_\_, \_\_function\_\_, etc)

### COMPILING ###

Visual studio 2015 project&solution files can be found in the **vstudio** folder. Makefiles for gnu/gcc ( **makefile** ) and for visual studio ( **makefilevs.bat** ) are provided in the root folder. All methods will generate objs, pdbs, exes, etc in **bin**.


### EXAMPLES ###

- basic usage

		#include "lap_logger.hpp"
		...
		lap::Logger::getInstance().setDefault(true, true);
		...
		LOG(lap::LOGFTL, "FATAL some format text including string %s, int %d, float %f, scientific %e\n", "m s a a", 329, 3.1416, 3.1416);
		LOGS(lap::LOGFTL) << "FATAL some format text including string " << "m s a a" << " int " << 329 << ", float " << 3.1416 << ", scientific " << 3.1416 << std::endl;
		LOG(2, "WARNING some text %s\n", "some other text");
		LOGS("DEBUG")<<" some debug text "<<1<<" "<<2<<" "<<3<<" and support for endline"<<std::endl;

- assert usage

		// #define LAP_NO_ASSERT									//disable LAP_ASSERT, just like NDEBUG disables assert.
		#include "lap_logger.hpp"
		...
		lap::Logger::getInstance().setDefault(true, true);
		...
		LAP_ASSERT(1 == 1, "true");

- custom channels

		#include "lap_logger.hpp"
		...
		lap::Logger::Channel ch5(4532, "paris");
		lap::Logger::Channel ch3(11, "berlin");
		lap::Logger::Channel ch1(1999, "london");
		lap::Logger::getInstance().openChannel(ch5, std::shared_ptr<std::ostream>(&std::cout, lap::Logger::OSTREAMdeleter));
		lap::Logger::getInstance().openChannel(ch3, std::shared_ptr<std::ostream>(&std::cout, lap::Logger::OSTREAMdeleter));
		lap::Logger::getInstance().openChannel(ch1, "london.log", false);
		LOG(11, "a european city ... to stdout\n");
		LOG("berlin", "the same european city ... to stdout ... when all channels are closed std::cout won't be deleted because OSTREAMdeleter will not close it. \n");
		LOG("paris", "another european city ... to stdout\n");
		LOGS(1999)<< "this european city goes to london.log\n");
		lap::Logger::getInstance().openChannel(ch1, "london.log", false);
		LOGS(1999)<< "just reopened this channel, now it this message goes to london.log and std::cout\n");
		lap::Logger::Channel ch9(85, "rome");
		lap::Logger::getInstance().openChannel(ch9, std::shared_ptr<std::file>(stdout, lap::Logger::FILEdeleter));
		LOGS("rome")<< "a new european city ... to stdout ... when all channels are closed stdout won't be deleted because FILEdeleter will not close it or stderr. "<<std::endl;
		lap::Logger::pauseChannel(ch9);
		LOGS("rome")<<" will not print"<<std::endl;

- custom headers

		lap::Logger::setHeaderType(HEADER_TIME_CHANNEL_FILE_ALIGN);
		lap::Logger::Channel ch600(600, "moscow");
		lap::Logger::getInstance().openChannel(ch600,std::shared_ptr<std::file>(stdout, lap::Logger::FILEdeleter));
		LOGS(600)<<" will output 2016-03-03 13:01:50  [moscow|600]    ["call_filename.cpp"]   your message goes here"<<std:endl;



### DOCUMENTATION ###

a Doxyfile is provided in **doc**, which can be used with Doxygen to generate  the entire documentation in HTML format. 

### COMPATIBILITY ###

tested with visual studio 2015, MSYS2 with MinGW x64 and GCC 6.20, Linux Mint 18 and GCC 5.4, MacOS El Capitan + clang 3.7.1.
