///---------------------------------------------------------------------------------------------------------------------
/// The MIT License (MIT)
/// 
/// Copyright (c) 2016 Lucian Petrescu, lucian (dot) petrescu (dot) 24 (at) gmail (dot) com
/// 
/// Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation 
/// files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, 
/// modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software 
/// is furnished to do so, subject to the following conditions:
/// 
/// The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
/// 
/// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
/// OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE 
/// LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR 
/// IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
///---------------------------------------------------------------------------------------------------------------------


#define LAP_USE_ASSERT
#include "lap_logger.hpp"
#include <iostream>


void test_mt_thread(){ 
	LOG(lap::LOGFTL, "FATAL some format text including string %s, int %d, float %f, scientific %e\n", "m s a a", 329, 3.1416, 3.1416);
	LOG(lap::LOGERR, "ERROR format text including string %s, int %d, float %f, scientific %e\n", "m s a a", 329, 3.1416, 3.1416);
	LOG(lap::LOGWRN, "WARNING format text including string %s, int %d, float %f, scientific %e\n",  "m s a a", 329, 3.1416, 3.1416);
	LOG(lap::LOGINF, "INFORMATION format text including string %s, int %d, float %f, scientific %e\n", "m s a a", 329, 3.1416, 3.1416);
	LOG(lap::LOGDBG, "DEBUG format text including string %s, int %d, float %f, scientific %e\n", "m s a a", 329, 3.1416, 3.1416);
	LOG(lap::LOGVRB, "VERBOSE format text including string %s, int %d, float %f, scientific %e\n",  "m s a a", 329, 3.1416, 3.1416);
	LOG(lap::LOGTRC, "TRACE format text including string %s, int %d, float %f, scientific %e\n",  "m s a a", 329, 3.1416, 3.1416);
}
void test_mt(){
	lap::Logger::getInstance().setDefault(false, true);
	const int COUNT = 1000;
	std::thread *t[COUNT];
	for (int i = 0; i < COUNT; i++) t[i] = new std::thread(test_mt_thread);
	for (int i = 0; i < COUNT; i++) t[i]->join();
	for (int i = 0; i < COUNT; i++) delete t[i];
}
void test_st_basics(){

	lap::Logger::getInstance().setDefault(true, true);

	//LAP_ASSERT(0==1,"fail!")
	LAP_ASSERT(1 == 1, "true");

	LOG(lap::LOGFTL, "FATAL some format text including string %s, int %d, float %f, scientific %e\n", "m s a a", 329, 3.1416, 3.1416);
	LOG(lap::LOGERR, "ERROR format text including string %s, int %d, float %f, scientific %e\n", "m s a a", 329, 3.1416, 3.1416);
	LOG(lap::LOGWRN, "WARNING format text including string %s, int %d, float %f, scientific %e\n", "m s a a", 329, 3.1416, 3.1416);
	LOG(lap::LOGINF, "INFORMATION format text including string %s, int %d, float %f, scientific %e\n", "m s a a", 329, 3.1416, 3.1416);
	LOG(lap::LOGDBG, "DEBUG format text including string %s, int %d, float %f, scientific %e\n", "m s a a", 329, 3.1416, 3.1416);
	LOG(lap::LOGVRB, "VERBOSE format text including string %s, int %d, float %f, scientific %e\n", "m s a a", 329, 3.1416, 3.1416);
	LOG(lap::LOGTRC, "TRACE format text including string %s, int %d, float %f, scientific %e\n", "m s a a", 329, 3.1416, 3.1416);

	LOG(lap::LOGFTL, "******************************************\n");
	LOG(lap::LOGFTL, "******************************************\n");
	LOG(0, "FATAL format text including string %s, int %d, float %f, scientific %e\n", "m s a a", 329, 3.1416, 3.1416);
	LOG(1, "ERROR format text including string %s, int %d, float %f, scientific %e\n", "m s a a", 329, 3.1416, 3.1416);
	LOG(2, "WARNING format text including string %s, int %d, float %f, scientific %e\n", "m s a a", 329, 3.1416, 3.1416);
	LOG(3, "INFO format text including string %s, int %d, float %f, scientific %e\n", "m s a a", 329, 3.1416, 3.1416);
	LOG("VERBOSE", "VERBOSE format text including string %s, int %d, float %f, scientific %e\n", "m s a a", 329, 3.1416, 3.1416);
	LOG("INFO", "INFO format text including string %s, int %d, float %f, scientific %e\n", "m s a a", 329, 3.1416, 3.1416);
	LOG("TRACE", "TRACE format text including string %s, int %d, float %f, scientific %e\n", "m s a a", 329, 3.1416, 3.1416);
	LOG("DEBUG", "DEBUG format text including string %s, int %d, float %f, scientific %e\n", "m s a a", 329, 3.1416, 3.1416);
	LOG("ERROR", "ERROR format text including string %s, int %d, float %f, scientific %e\n", "m s a a", 329, 3.1416, 3.1416);
	LOG("WARNING", "WARNING format text including string %s, int %d, float %f, scientific %e\n", "m s a a", 329, 3.1416, 3.1416);
	LOG("FATAL", "FATAL format text including string %s, int %d, float %f, scientific %e\n", "m s a a", 329, 3.1416, 3.1416);

	LOG(lap::LOGFTL, "******************************************\n");
	LOG(lap::LOGFTL, "******************************************\n");
	LOGS(lap::LOGFTL) << "FATAL some format text including string "<<"m s a a"<<" int "<<329<<", float "<<3.1416<<", scientific "<< 3.1416 << std::endl;
	LOGS(lap::LOGERR) << "ERROR some format text including string " << "m s a a" << " int " << 329 << ", float " << 3.1416 << ", scientific " << 3.1416 << std::endl;
	LOGS(lap::LOGWRN) << "WARNING some format text including string " << "m s a a" << " int " << 329 << ", float " << 3.1416 << ", scientific " << 3.1416 << std::endl;
	LOGS(lap::LOGINF) << "INFORMATION some format text including string " << "m s a a" << " int " << 329 << ", float " << 3.1416 << ", scientific " << 3.1416 << std::endl;
	LOGS(lap::LOGDBG) << "DEBUG some format text including string " << "m s a a" << " int " << 329 << ", float " << 3.1416 << ", scientific " << 3.1416 << std::endl;
	LOGS(lap::LOGVRB) << "VERBOSE some format text including string " << "m s a a" << " int " << 329 << ", float " << 3.1416 << ", scientific " << 3.1416 << std::endl;
	LOGS(lap::LOGTRC) << "TRACE some format text including string " << "m s a a" << " int " << 329 << ", float " << 3.1416 << ", scientific " << 3.1416 << std::endl;

	LOG(lap::LOGFTL, "******************************************\n");
	LOG(lap::LOGFTL, "******************************************\n");

	lap::Logger::getInstance().print(lap::LOGFTL, "MY FILE", 9999, "MY FUNC", "%s %d %e\n", "str", 3, 3.14);
	lap::Logger::getInstance().print("FATAL", "MY FILE", 9999, "MY FUNC", "%s %d %e\n", "str", 3, 3.14);
	lap::Logger::getInstance().print(std::string("FATAL"), "MY FILE", 9999, "MY FUNC", "%s %d %e\n", "str", 3, 3.14);
	lap::Logger::getInstance().print(0, "MY FILE", 9999, "MY FUNC", "%s %d %e\n", "str", 3, 3.14);

	lap::Logger::getInstance().setHeaderType(lap::Logger::HEADER_CHANNEL_FILE_LINE_FUNC_THREAD_ALIGN);
	lap::Logger::getInstance().printStream(lap::LOGFTL, "MY FILE1", 11, "MY FUNC1", "some user 1 content\n");
	lap::Logger::getInstance().printStream("FATAL", "MY FILE2", 22, "MY FUNC2", "some user 2 content\n");
	lap::Logger::getInstance().printStream(std::string("FATAL"), "MY FILE3", 33, "MY FUNC3", "some user 3 content\n");
	lap::Logger::getInstance().printStream(0, "MY FILE4", 44, "MY FUNC4", "some user 4 content\n");

}
void test_st_channel(){
	lap::Logger::Channel ch1;
	lap::Logger::Channel ch2(0);
	lap::Logger::Channel ch3("amsterdam");
	lap::Logger::Channel ch4(std::string("berlin"));
	lap::Logger::Channel ch5(4532, "paris");
	lap::Logger::Channel ch6(982, std::string("roma"));
	lap::Logger::Channel ch7(982, std::string("roma"));
	std::cout << (ch6 == ch7) << "   "<<ch6.getHeaderString()<<std::endl;
	std::cout << ch1.getHeaderString() << std::endl;
	std::cout << ch2.getHeaderString() << std::endl;
	std::cout << ch3.getHeaderString() << std::endl;
	std::cout << ch4.getHeaderString() << std::endl;
	std::cout << ch5.getHeaderString() << std::endl;

	lap::Logger::getInstance().openChannel(ch6, std::shared_ptr<FILE>(stdout, lap::Logger::FILEdeleter));
	LOG("roma", "some text\n");
	LOGS(982) << "some other text" << std::endl;
	LOGS("roma") << "some other text 2\n";
}
void test_st_headers() {
	lap::Logger::getInstance().setDefault(true, true);
	lap::Logger::getInstance().setHeaderType(lap::Logger::HEADER_NONE);										LOG(lap::LOGINF, "none\n");

	lap::Logger::getInstance().setHeaderType(lap::Logger::HEADER_CHANNEL);									LOG(lap::LOGINF, "c\n");
	lap::Logger::getInstance().setHeaderType(lap::Logger::HEADER_CHANNEL_ALIGN);							LOG(lap::LOGINF, "ca\n");
	lap::Logger::getInstance().setHeaderType(lap::Logger::HEADER_CHANNEL_FILE);								LOG(lap::LOGINF, "cf\n");
	lap::Logger::getInstance().setHeaderType(lap::Logger::HEADER_CHANNEL_FILE_ALIGN);						LOG(lap::LOGINF, "cfa\n");
	lap::Logger::getInstance().setHeaderType(lap::Logger::HEADER_CHANNEL_FILE_LINE);						LOG(lap::LOGINF, "cfl\n");
	lap::Logger::getInstance().setHeaderType(lap::Logger::HEADER_CHANNEL_FILE_LINE_ALIGN);					LOG(lap::LOGINF, "cfla\n");
	lap::Logger::getInstance().setHeaderType(lap::Logger::HEADER_CHANNEL_FILE_LINE_FUNC);					LOG(lap::LOGINF, "cflf\n");
	lap::Logger::getInstance().setHeaderType(lap::Logger::HEADER_CHANNEL_FILE_LINE_FUNC_ALIGN);				LOG(lap::LOGINF, "cflfa\n");
	lap::Logger::getInstance().setHeaderType(lap::Logger::HEADER_CHANNEL_FILE_LINE_FUNC_THREAD);			LOG(lap::LOGINF, "cflft\n");
	lap::Logger::getInstance().setHeaderType(lap::Logger::HEADER_CHANNEL_FILE_LINE_FUNC_THREAD_ALIGN);		LOG(lap::LOGINF, "cflfta\n");
	lap::Logger::getInstance().setHeaderType(lap::Logger::HEADER_CHANNEL_FILE_LINE_THREAD);					LOG(lap::LOGINF, "cflt\n");
	lap::Logger::getInstance().setHeaderType(lap::Logger::HEADER_CHANNEL_FILE_LINE_THREAD_ALIGN);			LOG(lap::LOGINF, "cflta\n");
	lap::Logger::getInstance().setHeaderType(lap::Logger::HEADER_CHANNEL_FILE_THREAD);						LOG(lap::LOGINF, "cft\n");
	lap::Logger::getInstance().setHeaderType(lap::Logger::HEADER_CHANNEL_FILE_THREAD_ALIGN);				LOG(lap::LOGINF, "cfta\n");
	lap::Logger::getInstance().setHeaderType(lap::Logger::HEADER_CHANNEL_THREAD);							LOG(lap::LOGINF, "ct\n");
	lap::Logger::getInstance().setHeaderType(lap::Logger::HEADER_CHANNEL_THREAD_ALIGN);						LOG(lap::LOGINF, "cta\n");

	lap::Logger::getInstance().setHeaderType(lap::Logger::HEADER_TIME_THREAD);								LOG(lap::LOGINF, "tt\n");
	lap::Logger::getInstance().setHeaderType(lap::Logger::HEADER_TIME_THREAD_ALIGN);						LOG(lap::LOGINF, "tta\n");
	lap::Logger::getInstance().setHeaderType(lap::Logger::HEADER_TIME_CHANNEL);								LOG(lap::LOGINF, "tc\n");
	lap::Logger::getInstance().setHeaderType(lap::Logger::HEADER_TIME_CHANNEL_ALIGN);						LOG(lap::LOGINF, "tca\n");
	lap::Logger::getInstance().setHeaderType(lap::Logger::HEADER_TIME_CHANNEL_FILE);						LOG(lap::LOGINF, "tcf\n");
	lap::Logger::getInstance().setHeaderType(lap::Logger::HEADER_TIME_CHANNEL_FILE_ALIGN);					LOG(lap::LOGINF, "tcfa\n");
	lap::Logger::getInstance().setHeaderType(lap::Logger::HEADER_TIME_CHANNEL_FILE_LINE);					LOG(lap::LOGINF, "tcfl\n");
	lap::Logger::getInstance().setHeaderType(lap::Logger::HEADER_TIME_CHANNEL_FILE_LINE_ALIGN);				LOG(lap::LOGINF, "tcfla\n");
	lap::Logger::getInstance().setHeaderType(lap::Logger::HEADER_TIME_CHANNEL_FILE_LINE_FUNC);				LOG(lap::LOGINF, "tcflf\n");
	lap::Logger::getInstance().setHeaderType(lap::Logger::HEADER_TIME_CHANNEL_FILE_LINE_FUNC_ALIGN);		LOG(lap::LOGINF, "tcflfa\n");
	lap::Logger::getInstance().setHeaderType(lap::Logger::HEADER_TIME_CHANNEL_FILE_LINE_FUNC_THREAD);		LOG(lap::LOGINF, "tcflft\n");
	lap::Logger::getInstance().setHeaderType(lap::Logger::HEADER_TIME_CHANNEL_FILE_LINE_FUNC_THREAD_ALIGN);	LOG(lap::LOGINF, "tctflfta\n");
	lap::Logger::getInstance().setHeaderType(lap::Logger::HEADER_TIME_CHANNEL_FILE_LINE_THREAD);			LOG(lap::LOGINF, "tcflt\n");
	lap::Logger::getInstance().setHeaderType(lap::Logger::HEADER_TIME_CHANNEL_FILE_LINE_THREAD_ALIGN);		LOG(lap::LOGINF, "tcflta\n");
	lap::Logger::getInstance().setHeaderType(lap::Logger::HEADER_TIME_CHANNEL_FILE_THREAD);					LOG(lap::LOGINF, "tcft\n");
	lap::Logger::getInstance().setHeaderType(lap::Logger::HEADER_TIME_CHANNEL_FILE_THREAD_ALIGN);			LOG(lap::LOGINF, "tcfta\n");
	lap::Logger::getInstance().setHeaderType(lap::Logger::HEADER_TIME_CHANNEL_THREAD);						LOG(lap::LOGINF, "tct\n");
	lap::Logger::getInstance().setHeaderType(lap::Logger::HEADER_TIME_CHANNEL_THREAD_ALIGN);				LOG(lap::LOGINF, "tcta\n");
}
void test_st_open_close() {
	lap::Logger::getInstance().closeAllChannels();
	using namespace lap;
#ifdef _MSC_VER
	FILE* f; fopen_s(&f, "myfile.log", "w");
#else
	FILE* f = fopen("myfile.log", "w");
#endif
	std::shared_ptr<FILE> file(f, Logger::FILEdeleter);
	std::shared_ptr<std::ostream> ostr(&std::cout, Logger::OSTREAMdeleter);
	Logger::getInstance().openChannel(Logger::Channel(0, "ZERO"), "0zeroappend.log", true);		LOG(0, "something for zero\n");
	Logger::getInstance().openChannel(Logger::Channel(1, "ONE"), "1oneoverwrite.log", false);	LOG(1, "something for %d\n",1);
	Logger::getInstance().openChannel(Logger::Channel(2, "TWO"), file);							LOG("TWO", "something for %s\n", "TWO");
	Logger::getInstance().openChannel(Logger::Channel(3, "THREE"), ostr);						LOG("THREE", "something for number %d\n", 3);
	Logger::getInstance().openChannel(Logger::Channel(4, "FOUR"), ostr);						LOG("FOUR", "something for number %d\n", 4);
	Logger::getInstance().openChannel(Logger::Channel(5, "FIVE"), file, ostr);					LOG("FIVE", "number five=%d\n", 5);
	Logger::getInstance().openChannel(Logger::Channel(6, "SIX"), "6six.log", true, ostr);		LOG(6, "something for number %d\n", 6);

	Logger::getInstance().openChannel(7, "SEVEN", "7seven.log", true);		LOG(7, "something for 7\n");
	Logger::getInstance().openChannel(8, "EIGHT", file);					LOG(8, "something for 8\n");
	Logger::getInstance().openChannel(9, "NINE", ostr);						LOG(9, "something for 9\n");
	Logger::getInstance().openChannel(10, "TEN", file, ostr);				LOG(10, "something for 10\n");
	Logger::getInstance().openChannel(11, "ELEVEN", "9nine.log", false, ostr);LOG(11, "something for ELEVEN\n");

	Logger::getInstance().openChannel(12, "12.log", true);			LOG(12, "something for 12\n");
	Logger::getInstance().openChannel(13, file);					LOG(13, "something for 13\n");
	Logger::getInstance().openChannel(14, ostr);					LOG(14, "something for 14\n");
	Logger::getInstance().openChannel(15, file,ostr);				LOG(15, "something for 15\n");
	Logger::getInstance().openChannel(16, "16.log", false, ostr);	LOG(16, "something for 16\n");

	Logger::getInstance().openChannel("F17", "17.log", true);			LOG("F17", "something for 17\n");
	Logger::getInstance().openChannel("F18", file);						LOG("F18", "something for 18\n");
	Logger::getInstance().openChannel("F19", ostr);						LOG("F19", "something for 19\n");
	Logger::getInstance().openChannel("F20", file, ostr);				LOG("F20", "something for 20\n");
	Logger::getInstance().openChannel("F21", "21.log", false, ostr);	LOG("F21", "something for 21\n");

	Logger::getInstance().openChannel(std::string("F22"), "22.log", true);			LOG("F22", "something for 22\n");
	Logger::getInstance().openChannel(std::string("F23"), file);					LOG("F23", "something for 23\n");
	Logger::getInstance().openChannel(std::string("F24"), ostr);					LOG("F24", "something for 24\n");
	Logger::getInstance().openChannel(std::string("F25"), file, ostr);				LOG("F25", "something for 25\n");
	Logger::getInstance().openChannel(std::string("F26"), "26.log", false, ostr);	LOG("F26", "something for 26\n");
	
	
	Logger::getInstance().openChannel(Logger::Channel(44, "FOFO"), "FOFOappend.log", true);
	Logger::getInstance().openChannel(Logger::Channel(44, "FOFO"), ostr);						
	LOG("FOFO", "something for number %d\n", 44);

	Logger::getInstance().openChannel(std::string("GGG"), ostr);		
	Logger::getInstance().openChannel("GGG", file);						
	LOG("GGG", "something for GGG\n");

	Logger::getInstance().openChannel(99, ostr);	
	Logger::getInstance().openChannel(99, file, ostr);				
	LOG(99, "something for 15\n");
	

	Logger::getInstance().openChannel(Logger::Channel(123, "MA123"), ostr);
	Logger::getInstance().openChannel(Logger::Channel(124, "MA124"), ostr);
	Logger::getInstance().openChannel(Logger::Channel(125, "MA125"), ostr);
	Logger::getInstance().openChannel(Logger::Channel(126, "MA126"), ostr);

	Logger::getInstance().closeChannel(123);
	Logger::getInstance().closeChannel("MA124");
	Logger::getInstance().closeChannel(std::string("MA125"));
	Logger::getInstance().closeChannel(Logger::Channel(126, "MA126"));

	Logger::getInstance().closeAllChannels();
}

void test_st_pause_resume() {
	lap::Logger::getInstance().closeAllChannels();
	using namespace lap;
	Logger::HeaderType ht = Logger::getInstance().getHeaderType();

	std::shared_ptr<std::ostream> ostr(&std::cout, Logger::OSTREAMdeleter);
	Logger::getInstance().openChannel(Logger::Channel(0, "ZERO"), "0zeroappend.log", true);		LOG(0, "something for zero\n");
	Logger::getInstance().openChannel(Logger::Channel(1, "ONE"), "1oneoverwrite.log", false);	LOG(1, "something for %d\n", 1);
	Logger::getInstance().openChannel(Logger::Channel(2, "TWO"), ostr);							LOG("TWO", "something for %s\n", "TWO");
	Logger::getInstance().openChannel(Logger::Channel(3, "THREE"), ostr);						LOG("THREE", "something for number %d\n", 3);
	Logger::getInstance().openChannel(Logger::Channel(4, "FOUR"), ostr);						LOG("FOUR", "something for number %d\n", 4);
	Logger::getInstance().openChannel(Logger::Channel(5, "FIVE"),  ostr);						LOG("FIVE", "number five=%d\n", 5);
	Logger::getInstance().openChannel(Logger::Channel(6, "SIX"), "6six.log", true, ostr);		LOG(6, "something for number %d\n", 6);

	Logger::getInstance().pauseAllButChannel("SIX");
	LOG(0, "---\n"); LOG(1, "---\n"); LOG(2, "---\n"); LOG(3, "---\n"); LOG(4, "---\n"); LOG(5, "---\n"); LOG(6, "ok\n");
	Logger::getInstance().resumeAllButChannel("SIX");
	Logger::getInstance().pauseAllButChannel(6);
	LOG(0, "---\n"); LOG(1, "---\n"); LOG(2, "---\n"); LOG(3, "---\n"); LOG(4, "---\n"); LOG(5, "---\n"); LOG("SIX", "ok\n");
	Logger::getInstance().resumeAllButChannel(6);
	Logger::getInstance().pauseAllButChannel(std::string("SIX"));
	LOG(0, "---\n"); LOG(1, "---\n"); LOG(2, "---\n"); LOG(3, "---\n"); LOG(4, "---\n"); LOG(5, "---\n"); LOG(6, "ok\n");
	Logger::getInstance().resumeAllButChannel(std::string("SIX"));
	Logger::getInstance().pauseAllButChannel(Logger::Channel(6, "SIX"));
	LOG(0, "---\n"); LOG(1, "---\n"); LOG(2, "---\n"); LOG(3, "---\n"); LOG(4, "---\n"); LOG(5, "---\n"); LOG(6, "ok\n");
	Logger::getInstance().resumeAllButChannel(Logger::Channel(6, "SIX"));
	Logger::getInstance().resumeAllChannels();

	Logger::getInstance().pauseAllChannels();
	Logger::getInstance().resumeAllButChannel("SIX");						LOG(6, "---\n");		Logger::getInstance().pauseAllChannels();
	Logger::getInstance().resumeAllButChannel(std::string("SIX"));			LOG(6, "---\n");		Logger::getInstance().pauseAllChannels();
	Logger::getInstance().resumeAllButChannel(6);							LOG("SIX", "---\n");		Logger::getInstance().pauseAllChannels();
	Logger::getInstance().resumeAllButChannel(Logger::Channel(6, "SIX"));	LOG(6, "---\n");		Logger::getInstance().pauseAllChannels();

	Logger::getInstance().resumeAllChannels();
	Logger::getInstance().pauseChannel(6);		LOGS(0) << (bool)Logger::getInstance().isChannelActive(6) << std::endl;
	Logger::getInstance().resumeChannel(6);		LOGS(0) << Logger::getInstance().isChannelActive(6) << std::endl;
	Logger::getInstance().pauseChannel("SIX");	LOGS(0) << Logger::getInstance().isChannelActive("SIX") << std::endl;
	Logger::getInstance().resumeChannel("SIX");	LOGS(0) << Logger::getInstance().isChannelActive("SIX") << std::endl;
	Logger::getInstance().pauseChannel(std::string("SIX"));			LOGS(0) << Logger::getInstance().isChannelActive(std::string("SIX")) << std::endl;
	Logger::getInstance().resumeChannel(std::string( "SIX"));		LOGS(0) << Logger::getInstance().isChannelActive(std::string("SIX")) << std::endl;
	Logger::getInstance().pauseChannel(Logger::Channel(6, "SIX"));	LOGS(0) << Logger::getInstance().isChannelActive(Logger::Channel(6, "SIX")) << std::endl;
	Logger::getInstance().resumeChannel(Logger::Channel(6, "SIX"));	LOGS(0) << Logger::getInstance().isChannelActive(Logger::Channel(6, "SIX")) << std::endl;

}

std::mutex lock;
void test_speed_compare(FILE *f) {
	lock.lock();
	fprintf(f, "%-12s FATAL some format text including string %s, int %d, float %f, scientific %e\n", "[FATAL|0]", "m s a a", 329, 3.1416, 3.1416);
	lock.unlock();	
	std::this_thread::yield();
	lock.lock();
	fprintf(f, "%-12s ERROR format text including string %s, int %d, float %f, scientific %e\n", "[ERROR|0]", "m s a a", 329, 3.1416, 3.1416);
	lock.unlock();
	std::this_thread::yield();
	lock.lock();
	fprintf(f, "%-12s WARNING format text including string %s, int %d, float %f, scientific %e\n", "[WARNING|0]", "m s a a", 329, 3.1416, 3.1416);
	lock.unlock();
	std::this_thread::yield();
	lock.lock();
	fprintf(f, "%-12s INFORMATION format text including string %s, int %d, float %f, scientific %e\n", "[INFO|0]", "m s a a", 329, 3.1416, 3.1416);
	lock.unlock();
	std::this_thread::yield();
	lock.lock();
	fprintf(f, "%-12s DEBUG format text including string %s, int %d, float %f, scientific %e\n", "[DEBUG|0]", "m s a a", 329, 3.1416, 3.1416);
	lock.unlock(); 
	std::this_thread::yield();
	lock.lock();
	fprintf(f, "%-12s VERBOSE format text including string %s, int %d, float %f, scientific %e\n", "[VERBOSE|0]", "m s a a", 329, 3.1416, 3.1416);
	lock.unlock();
	std::this_thread::yield();
	lock.lock();
	fprintf(f, "%-12s TRACE format text including string %s, int %d, float %f, scientific %e\n", "[TRACE|0]", "m s a a", 329, 3.1416, 3.1416);
	lock.unlock();
}
void test_speed(){
	lap::Logger::getInstance().closeAllChannels();
	lap::Logger::getInstance().setHeaderType(lap::Logger::HEADER_CHANNEL_FILE_LINE_ALIGN);
	double begin = std::clock();
	lap::Logger::getInstance().setDefault(false, true);
	const int COUNT = 2000;
	std::thread *t[COUNT];
	for (int i = 0; i < COUNT; i++) t[i] = new std::thread(test_mt_thread);
	for (int i = 0; i < COUNT; i++) t[i]->join();
	for (int i = 0; i < COUNT; i++) delete t[i];
	std::cout <<"MULTI LOGGER (detailed header) FINISHED WITH ="<< (std::clock() - begin) / CLOCKS_PER_SEC << std::endl;

	lap::Logger::getInstance().setHeaderType(lap::Logger::HEADER_CHANNEL_ALIGN);
	begin = std::clock();
	lap::Logger::getInstance().setDefault(false, true);
	for (int i = 0; i < COUNT; i++) t[i] = new std::thread(test_mt_thread);
	for (int i = 0; i < COUNT; i++) t[i]->join();
	for (int i = 0; i < COUNT; i++) delete t[i];
	std::cout << "MULTI LOGGER (expected header) FINISHED WITH =" << (std::clock() - begin) / CLOCKS_PER_SEC << std::endl;

	lap::Logger::getInstance().setHeaderType(lap::Logger::HEADER_NONE);
	begin = std::clock();
	lap::Logger::getInstance().setDefault(false, true);
	for (int i = 0; i < COUNT; i++) t[i] = new std::thread(test_mt_thread);
	for (int i = 0; i < COUNT; i++) t[i]->join();
	for (int i = 0; i < COUNT; i++) delete t[i];
	std::cout << "MULTI LOGGER (no header) FINISHED WITH =" << (std::clock() - begin) / CLOCKS_PER_SEC << std::endl;

	begin = std::clock();
#ifdef _MSC_VER
	FILE* f; fopen_s(&f, "noheader.log", "w");
#else
	FILE* f = fopen("noheader.log", "w");
#endif
	for (int i = 0; i < COUNT; i++) t[i] = new std::thread(test_speed_compare, f);
	for (int i = 0; i < COUNT; i++) t[i]->join();
	for (int i = 0; i < COUNT; i++) delete t[i];
	fclose(f);
	std::cout << "MULTI RAW FINISHED WITH =" << (std::clock() - begin) / CLOCKS_PER_SEC << std::endl;



	lap::Logger::getInstance().setHeaderType(lap::Logger::HEADER_CHANNEL_FILE_LINE_ALIGN);
	begin = std::clock();
	for (int i = 0; i < COUNT; i++) test_mt_thread();
	std::cout << "SINGLE LOGGER (detailed header) FINISHED WITH =" << (std::clock() - begin) / CLOCKS_PER_SEC << std::endl;

	lap::Logger::getInstance().setHeaderType(lap::Logger::HEADER_CHANNEL_ALIGN);
	begin = std::clock();
	for (int i = 0; i < COUNT; i++) test_mt_thread();
	std::cout << "SINGLE LOGGER (expected header) FINISHED WITH =" << (std::clock() - begin) / CLOCKS_PER_SEC << std::endl;

	lap::Logger::getInstance().setHeaderType(lap::Logger::HEADER_NONE);
	begin = std::clock();
	for (int i = 0; i < COUNT; i++) test_mt_thread();
	std::cout << "SINGLE LOGGER (no header) FINISHED WITH =" << (std::clock() - begin) / CLOCKS_PER_SEC << std::endl;

	begin = std::clock();
#ifdef _MSC_VER
	fopen_s(&f, "noheader.log", "w");
#else
	f = fopen("noheader.log", "w");
#endif
	for (int i = 0; i < COUNT; i++) test_speed_compare(f);
	fclose(f);
	std::cout << "SINGLE RAW FINISHED WITH =" << (std::clock() - begin) / CLOCKS_PER_SEC << std::endl;
}

void test_all() {
	test_st_basics();
	test_st_channel();
	test_st_headers();
	test_st_open_close();
	test_st_pause_resume();
	test_mt();
	test_speed();
}

int main(int argc, char* argv[]){
	
	test_all();
	//test_st_basics();
	//test_mt();
	//LAP_ASSERT(0==1,"fail!");
	
	std::cout << "Main Thread Finished" << std::endl;
	std::cin.get();
	return 0;
}
