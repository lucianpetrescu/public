///---------------------------------------------------------------------------------------------------------------------
/// The MIT License (MIT)
/// 
/// Copyright (c) 2016 Lucian Petrescu, lucian (dot) petrescu (dot) 24 (at) gmail (dot) com
/// 
/// Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation 
/// files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, 
/// modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software 
/// is furnished to do so, subject to the following conditions:
/// 
/// The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
/// 
/// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
/// OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE 
/// LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR 
/// IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
///---------------------------------------------------------------------------------------------------------------------
#include "lap_logger.hpp"


namespace lap{
	
	const Logger::Channel LOGFTL(0, "FATAL");	
	const Logger::Channel LOGERR(1, "ERROR");	
	const Logger::Channel LOGWRN(2, "WARNING");	
	const Logger::Channel LOGINF(3, "INFO");	
	const Logger::Channel LOGDBG(4, "DEBUG");	
	const Logger::Channel LOGVRB(5, "VERBOSE");	
	const Logger::Channel LOGTRC(6, "TRACE");	

	const Logger::HeaderType Logger::HEADER_NONE;

	const Logger::HeaderType Logger::HEADER_THREAD(Logger::HeaderType::NTHREAD);
	const Logger::HeaderType Logger::HEADER_CHANNEL(Logger::HeaderType::NCHANNEL);
	const Logger::HeaderType Logger::HEADER_CHANNEL_THREAD(Logger::HeaderType::NCHANNEL, Logger::HeaderType::NTHREAD);
	const Logger::HeaderType Logger::HEADER_CHANNEL_FILE(Logger::HeaderType::NCHANNEL, Logger::HeaderType::NFILE);
	const Logger::HeaderType Logger::HEADER_CHANNEL_FILE_THREAD(Logger::HeaderType::NCHANNEL, Logger::HeaderType::NFILE, Logger::HeaderType::NTHREAD);
	const Logger::HeaderType Logger::HEADER_CHANNEL_FILE_LINE(Logger::HeaderType::NCHANNEL, Logger::HeaderType::NFILE, Logger::HeaderType::NLINE);
	const Logger::HeaderType Logger::HEADER_CHANNEL_FILE_LINE_THREAD(Logger::HeaderType::NCHANNEL, Logger::HeaderType::NFILE, Logger::HeaderType::NLINE, Logger::HeaderType::NTHREAD);
	const Logger::HeaderType Logger::HEADER_CHANNEL_FILE_LINE_FUNC(Logger::HeaderType::NCHANNEL, Logger::HeaderType::NFILE, Logger::HeaderType::NLINE, Logger::HeaderType::NFUNC);
	const Logger::HeaderType Logger::HEADER_CHANNEL_FILE_LINE_FUNC_THREAD(Logger::HeaderType::NCHANNEL, Logger::HeaderType::NFILE, Logger::HeaderType::NLINE, Logger::HeaderType::NFUNC, Logger::HeaderType::NTHREAD);

	const Logger::HeaderType Logger::HEADER_TIME_THREAD(Logger::HeaderType::NTIME, Logger::HeaderType::NTHREAD);
	const Logger::HeaderType Logger::HEADER_TIME_CHANNEL(Logger::HeaderType::NTIME, Logger::HeaderType::NCHANNEL);
	const Logger::HeaderType Logger::HEADER_TIME_CHANNEL_THREAD(Logger::HeaderType::NTIME, Logger::HeaderType::NCHANNEL, Logger::HeaderType::NTHREAD);
	const Logger::HeaderType Logger::HEADER_TIME_CHANNEL_FILE(Logger::HeaderType::NTIME, Logger::HeaderType::NCHANNEL, Logger::HeaderType::NFILE);
	const Logger::HeaderType Logger::HEADER_TIME_CHANNEL_FILE_THREAD(Logger::HeaderType::NTIME, Logger::HeaderType::NCHANNEL, Logger::HeaderType::NFILE, Logger::HeaderType::NTHREAD);
	const Logger::HeaderType Logger::HEADER_TIME_CHANNEL_FILE_LINE(Logger::HeaderType::NTIME, Logger::HeaderType::NCHANNEL, Logger::HeaderType::NFILE, Logger::HeaderType::NLINE);
	const Logger::HeaderType Logger::HEADER_TIME_CHANNEL_FILE_LINE_THREAD(Logger::HeaderType::NTIME, Logger::HeaderType::NCHANNEL, Logger::HeaderType::NFILE, Logger::HeaderType::NLINE, Logger::HeaderType::NTHREAD);
	const Logger::HeaderType Logger::HEADER_TIME_CHANNEL_FILE_LINE_FUNC(Logger::HeaderType::NTIME, Logger::HeaderType::NCHANNEL, Logger::HeaderType::NFILE, Logger::HeaderType::NLINE, Logger::HeaderType::NFUNC);
	const Logger::HeaderType Logger::HEADER_TIME_CHANNEL_FILE_LINE_FUNC_THREAD(Logger::HeaderType::NTIME, Logger::HeaderType::NCHANNEL, Logger::HeaderType::NFILE, Logger::HeaderType::NLINE, Logger::HeaderType::NFUNC, Logger::HeaderType::NTHREAD);

	const Logger::HeaderType Logger::HEADER_THREAD_ALIGN(Logger::HeaderType::ATHREAD);
	const Logger::HeaderType Logger::HEADER_CHANNEL_ALIGN(Logger::HeaderType::ACHANNEL);
	const Logger::HeaderType Logger::HEADER_CHANNEL_THREAD_ALIGN(Logger::HeaderType::ACHANNEL, Logger::HeaderType::ATHREAD);
	const Logger::HeaderType Logger::HEADER_CHANNEL_FILE_ALIGN(Logger::HeaderType::ACHANNEL, Logger::HeaderType::AFILE);
	const Logger::HeaderType Logger::HEADER_CHANNEL_FILE_THREAD_ALIGN(Logger::HeaderType::ACHANNEL, Logger::HeaderType::AFILE, Logger::HeaderType::ATHREAD);
	const Logger::HeaderType Logger::HEADER_CHANNEL_FILE_LINE_ALIGN(Logger::HeaderType::ACHANNEL, Logger::HeaderType::AFILE, Logger::HeaderType::ALINE);
	const Logger::HeaderType Logger::HEADER_CHANNEL_FILE_LINE_THREAD_ALIGN(Logger::HeaderType::ACHANNEL, Logger::HeaderType::AFILE, Logger::HeaderType::ALINE, Logger::HeaderType::ATHREAD);
	const Logger::HeaderType Logger::HEADER_CHANNEL_FILE_LINE_FUNC_ALIGN(Logger::HeaderType::ACHANNEL, Logger::HeaderType::AFILE, Logger::HeaderType::ALINE, Logger::HeaderType::AFUNC);
	const Logger::HeaderType Logger::HEADER_CHANNEL_FILE_LINE_FUNC_THREAD_ALIGN(Logger::HeaderType::ACHANNEL, Logger::HeaderType::AFILE, Logger::HeaderType::ALINE, Logger::HeaderType::AFUNC, Logger::HeaderType::ATHREAD);

	const Logger::HeaderType Logger::HEADER_TIME_THREAD_ALIGN(Logger::HeaderType::ATIME, Logger::HeaderType::ATHREAD);
	const Logger::HeaderType Logger::HEADER_TIME_CHANNEL_ALIGN(Logger::HeaderType::ATIME, Logger::HeaderType::ACHANNEL);
	const Logger::HeaderType Logger::HEADER_TIME_CHANNEL_THREAD_ALIGN(Logger::HeaderType::ATIME, Logger::HeaderType::ACHANNEL, Logger::HeaderType::ATHREAD);
	const Logger::HeaderType Logger::HEADER_TIME_CHANNEL_FILE_ALIGN(Logger::HeaderType::ATIME, Logger::HeaderType::ACHANNEL, Logger::HeaderType::AFILE);
	const Logger::HeaderType Logger::HEADER_TIME_CHANNEL_FILE_THREAD_ALIGN(Logger::HeaderType::ATIME, Logger::HeaderType::ACHANNEL, Logger::HeaderType::AFILE, Logger::HeaderType::ATHREAD);
	const Logger::HeaderType Logger::HEADER_TIME_CHANNEL_FILE_LINE_ALIGN(Logger::HeaderType::ATIME, Logger::HeaderType::ACHANNEL, Logger::HeaderType::AFILE, Logger::HeaderType::ALINE);
	const Logger::HeaderType Logger::HEADER_TIME_CHANNEL_FILE_LINE_THREAD_ALIGN(Logger::HeaderType::ATIME, Logger::HeaderType::ACHANNEL, Logger::HeaderType::AFILE, Logger::HeaderType::ALINE, Logger::HeaderType::ATHREAD);
	const Logger::HeaderType Logger::HEADER_TIME_CHANNEL_FILE_LINE_FUNC_ALIGN(Logger::HeaderType::ATIME, Logger::HeaderType::ACHANNEL, Logger::HeaderType::AFILE, Logger::HeaderType::ALINE, Logger::HeaderType::AFUNC);
	const Logger::HeaderType Logger::HEADER_TIME_CHANNEL_FILE_LINE_FUNC_THREAD_ALIGN(Logger::HeaderType::ATIME, Logger::HeaderType::ACHANNEL, Logger::HeaderType::AFILE, Logger::HeaderType::ALINE, Logger::HeaderType::AFUNC, Logger::HeaderType::ATHREAD);

	
	const std::string Logger::DEFAULT_LOG_FILE = "lap.log";
	const std::string Logger::CHANNEL_NONE_STR = "";

	const unsigned int Logger::CHANNEL_NONE_INT;	//gcc -1 assignment is actually static cast, needs address.

	///--------------------------------------------------------------------------------------------
	Logger::Channel::Channel(){
		type = TYPE_NONE;
		number = -1;
		name = "";
	}
	Logger::Channel::Channel(int n) {
		type = TYPE_NUMBER;
		number = n;
		name = "";
	}
	Logger::Channel::Channel(unsigned int n){
		type = TYPE_NUMBER;
		number = n;
		name = "";
	}
	Logger::Channel::Channel(const char* n) : name(n) {
		type = TYPE_NAME;
		number = -1;
	}
	Logger::Channel::Channel(const std::string& n) : name(n){
		type = TYPE_NAME;
		number = -1;
	}
	Logger::Channel::Channel(unsigned int num, const char* nam) : number(num), name(nam) {
		type = TYPE_NAME | TYPE_NUMBER;
	}
	Logger::Channel::Channel(unsigned int num, const std::string& nam) : number(num), name(nam){ 
		type = TYPE_NAME | TYPE_NUMBER;
	}
	bool Logger::Channel::operator==(const Channel & rhs) const{
		if((type & TYPE_NUMBER) && (rhs.type & TYPE_NUMBER)) return number==rhs.number;
		if((type & TYPE_NAME) && (rhs.type & TYPE_NAME)) return name == rhs.name;
		return false;
	}
	std::string Logger::Channel::getHeaderString() const {
		if ((type & TYPE_NUMBER) && (type & TYPE_NUMBER)) return name+"|"+std::to_string(number);
		if (type & TYPE_NAME) return name;
		if (type & TYPE_NUMBER) return std::to_string(number);
		return "";
	}

	///--------------------------------------------------------------------------------------------
	Logger::HeaderType::HeaderType() {
	}
	Logger::HeaderType::HeaderType(unsigned int d1) {
		descriptors.push_back({ d1, internalParse(d1) });
	}
	Logger::HeaderType::HeaderType(unsigned int d1, unsigned int d2) {
		descriptors.push_back({ d1, internalParse(d1) });
		descriptors.push_back({ d2, internalParse(d2) });
	}
	Logger::HeaderType::HeaderType(unsigned int d1, unsigned int d2, unsigned int d3) {
		descriptors.push_back({ d1, internalParse(d1) });
		descriptors.push_back({ d2, internalParse(d2) });
		descriptors.push_back({ d3, internalParse(d3) });
	}
	Logger::HeaderType::HeaderType(unsigned int d1, unsigned int d2, unsigned int d3, unsigned int d4) {
		descriptors.push_back({ d1, internalParse(d1) });
		descriptors.push_back({ d2, internalParse(d2) });
		descriptors.push_back({ d3, internalParse(d3) });
		descriptors.push_back({ d4, internalParse(d4) });
	}
	Logger::HeaderType::HeaderType(unsigned int d1, unsigned int d2, unsigned int d3, unsigned int d4, unsigned int d5) {
		descriptors.push_back({ d1, internalParse(d1) });
		descriptors.push_back({ d2, internalParse(d2) });
		descriptors.push_back({ d3, internalParse(d3) });
		descriptors.push_back({ d4, internalParse(d4) });
		descriptors.push_back({ d5, internalParse(d5) });
	}
	Logger::HeaderType::HeaderType(unsigned int d1, unsigned int d2, unsigned int d3, unsigned int d4, unsigned int d5, unsigned int d6) {
		descriptors.push_back({ d1, internalParse(d1) });
		descriptors.push_back({ d2, internalParse(d2) });
		descriptors.push_back({ d3, internalParse(d3) });
		descriptors.push_back({ d4, internalParse(d4) });
		descriptors.push_back({ d5, internalParse(d5) });
		descriptors.push_back({ d6, internalParse(d6) });
	}
	bool Logger::HeaderType::operator==(const HeaderType & rhs) const
	{
		return descriptors == rhs.descriptors;
	}
	std::string Logger::HeaderType::internalParse(unsigned int d) {
		switch (d) {
		case ACHANNEL: has_channel = true; return "%-15s";
		case ATHREAD: has_thread = true; return "%-8s";
		case ATIME: has_time = true;  return "%-21s";
		case AFILE: has_file = true;  return "%-25s";
		case ALINE: has_line = true;  return "%-8s";
		case AFUNC: has_func = true;  return "%-40s";
		case NCHANNEL: has_channel = true;  return "%s";
		case NTHREAD: has_thread = true;  return "%s";
		case NTIME: has_time = true;  return "%s";
		case NFILE: has_file = true;  return "%s";
		case NLINE: has_line = true;  return "%s";
		case NFUNC: has_func = true;  return "%s";
			default:
				return "";
		}
	}
	bool Logger::HeaderType::hasTime() const {
		return has_time;
	}
	bool Logger::HeaderType::hasFile() const {
		return has_file;
	}
	bool Logger::HeaderType::hasLine() const {
		return has_line;
	}
	bool Logger::HeaderType::hasFunc() const {
		return has_func;
	}
	bool Logger::HeaderType::hasThread() const {
		return has_thread;
	}
	bool Logger::HeaderType::hasChannel() const {
		return has_channel;
	}
	const std::vector<std::pair<unsigned int, std::string>>& Logger::HeaderType::getDescriptors() const {
		return descriptors;
	}




	///--------------------------------------------------------------------------------------------
	Logger::ChannelTarget::ChannelTarget(){
		active = false;
	}
	Logger::ChannelTarget::ChannelTarget(const std::shared_ptr<FILE> &file, const std::shared_ptr<std::ostream> &ostream){
		if(file) out_file = file;
		if (ostream) out_ostream = ostream;
		active = true;
	}
	

	///--------------------------------------------------------------------------------------------
	Logger::Command::Command(){
	}
	Logger::Command::Command(const std::shared_ptr<FILE> &file, const std::shared_ptr<std::ostream>& ostream) {
		out_file = file;
		out_ostream = ostream;
	}
	Logger::Command::Command(const std::shared_ptr<FILE> &file, const std::shared_ptr<std::ostream>& ostream, const std::string& cont) {
		out_file = file;
		out_ostream = ostream;
		content = content;
	}

	
		
	///--------------------------------------------------------------------------------------------
	Logger Logger::instance;
	Logger::Logger(){

		//register fatal channel
		std::shared_ptr<std::ostream> ostream_shared_ptr(&std::cout, OSTREAMdeleter);
		std::shared_ptr<FILE> file_shared_ptr(nullptr);
		ChannelTarget target = ChannelTarget(file_shared_ptr, ostream_shared_ptr);
		channel_int_to_target[LOGFTL.number] = { LOGFTL.name, target };
		channel_string_to_target[LOGFTL.name] = { LOGFTL.number, target };
		
		//defaults
		header_type = HEADER_CHANNEL_ALIGN;
		thread_flush_ms = DEFAULT_THREAD_FLUSH_MS;
		thread_close_flag = false;
		thread = std::thread(std::bind(&Logger::internalThreadFlush, this));
	}
	Logger::~Logger(){
		//close logging thread
		mutex.lock();
		thread_close_flag = true;
		mutex.unlock();
		thread.join();
		//RAII automatically does the rest
	}
	void Logger::internalFlush() {
		//already sync'd
		for (const auto& cmd : commands) {
			if(cmd.out_ostream) (*cmd.out_ostream.get())<<cmd.content;
			if(cmd.out_file) fprintf(cmd.out_file.get(),"%s",cmd.content.c_str());
		}
		commands.clear();
	}
	void Logger::internalThreadFlush(){
		unsigned int sleep_duration = DEFAULT_THREAD_FLUSH_MS;
		while (true){
			//sleep
			std::this_thread::sleep_for(std::chrono::milliseconds(sleep_duration));

			//lock and work
			std::lock_guard<std::mutex> lock(mutex);
			sleep_duration = thread_flush_ms;

			//flush all active channels that have data
			internalFlush();

			//if done retrun
			if (thread_close_flag) return;
		}
	}
	std::shared_ptr<std::FILE> Logger::internalMakeSharedFILE(const char* filename, bool append){
		std::FILE* file;
	#ifdef _MSC_VER
		fopen_s(&file, filename, "w");
	#else
		file = fopen(filename, "w");
	#endif
		return std::shared_ptr<std::FILE>(file, FILEdeleter);
	}


	void Logger::FILEdeleter(FILE* file){
		//don't close special c files
		if (file == stdout || file == stderr) return;
		fclose(file);
	}
	void Logger::OSTREAMdeleter(std::ostream* stream){
		//don't close std::cout
		if (stream == &std::cout) return;
		delete stream;
	}

	Logger& Logger::getInstance(){
		return instance;
	}


	void Logger::internalCloseChannel(unsigned int number){
		//already synchronized code
		auto it = channel_int_to_target.find(number);
		if (it != channel_int_to_target.end()) {

			//if this channel has an associated name erase the name mapping
			if (it->second.first != CHANNEL_NONE_STR) {
				auto it2 = channel_string_to_target.find(it->second.first);
				if(it2 != channel_string_to_target.end()){
					channel_string_to_target.erase(it2);
				}
			}

			//erase the number mapping
			channel_int_to_target.erase(it);
		}
	}
	void Logger::internalCloseChannel(const char* name) {
		//already synchronized code
		auto it = channel_string_to_target.find(name);
		if (it != channel_string_to_target.end()) {

			//if this channel has an associated number erase the number mapping
			if (it->second.first != CHANNEL_NONE_INT) {
				auto it2 = channel_int_to_target.find(it->second.first);
				if (it2 != channel_int_to_target.end()) {
					channel_int_to_target.erase(it2);
				}
			}

			//erase the name mapping
			channel_string_to_target.erase(it);
		}
	}

	void Logger::internalActiveChannel(unsigned int number, bool active) {
		//already synchronized code

		//set active state for the number mapping
		auto it = channel_int_to_target.find(number);
		if (it == channel_int_to_target.end()) return;
		it->second.second.active = active;

		//if a string mapping exist, set active state for it
		if (it->second.first != CHANNEL_NONE_STR) {
			auto it2 = channel_string_to_target.find(it->second.first);
			if (it2 != channel_string_to_target.end()) it2->second.second.active = active;
		}
	}
	void Logger::internalActiveChannel(const char* name, bool active) {
		//already synchronized code

		//set active state for the number mapping
		auto it = channel_string_to_target.find(name);
		if (it == channel_string_to_target.end()) return;
		it->second.second.active = active;

		//if a string mapping exist, set active state for it
		if (it->second.first != CHANNEL_NONE_INT) {
			auto it2 = channel_int_to_target.find(it->second.first);
			if (it2 != channel_int_to_target.end()) it2->second.second.active = active;
		}
	}

	void Logger::setDefault(bool use_stdout, bool use_fileout, const std::string& filename){
		std::lock_guard<std::mutex> lock(mutex);
		
		//flush all active channels that have data
		internalFlush();

		//clear mappings
		channel_int_to_target.clear();
		channel_string_to_target.clear();

		std::shared_ptr<std::ostream> ostream_shared_ptr(nullptr);
		std::shared_ptr<FILE> file_shared_ptr(nullptr);
		if (use_stdout)	ostream_shared_ptr = std::shared_ptr<std::ostream>(&std::cout, OSTREAMdeleter);
		if (use_fileout) file_shared_ptr = internalMakeSharedFILE(filename.c_str(), false);

		//populate maps
		ChannelTarget target = ChannelTarget(file_shared_ptr, ostream_shared_ptr);

		channel_int_to_target[LOGFTL.number] = { LOGFTL.name, target }; //pairs
		channel_int_to_target[LOGERR.number] = { LOGERR.name, target };
		channel_int_to_target[LOGWRN.number] = { LOGWRN.name, target };
		channel_int_to_target[LOGINF.number] = { LOGINF.name, target };
		channel_int_to_target[LOGDBG.number] = { LOGDBG.name, target };
		channel_int_to_target[LOGVRB.number] = { LOGVRB.name, target };
		channel_int_to_target[LOGTRC.number] = { LOGTRC.name, target };

		channel_string_to_target[LOGFTL.name] = { LOGFTL.number, target }; //pairs
		channel_string_to_target[LOGERR.name] = { LOGERR.number, target };
		channel_string_to_target[LOGWRN.name] = { LOGWRN.number, target };
		channel_string_to_target[LOGINF.name] = { LOGINF.number, target };
		channel_string_to_target[LOGDBG.name] = { LOGDBG.number, target };
		channel_string_to_target[LOGVRB.name] = { LOGVRB.number, target };
		channel_string_to_target[LOGTRC.name] = { LOGTRC.number, target };
	}





	void Logger::openChannel(const Logger::Channel& channel, const char* filename, bool append) {
		std::shared_ptr<FILE> shared_ptr_file = internalMakeSharedFILE(filename, append);
		std::lock_guard<std::mutex> lock(mutex);
		internalCloseChannel(channel.number);
		channel_int_to_target[channel.number] = { channel.name, ChannelTarget(shared_ptr_file, nullptr) };
		channel_string_to_target[channel.name] = { channel.number, ChannelTarget(shared_ptr_file, nullptr)};
	}
	void Logger::openChannel(const Logger::Channel& channel, const std::shared_ptr<FILE>& out_file) {
		std::lock_guard<std::mutex> lock(mutex);
		internalCloseChannel(channel.number);
		channel_int_to_target[channel.number] = { channel.name, ChannelTarget(out_file, nullptr) };
		channel_string_to_target[channel.name] = { channel.number, ChannelTarget(out_file, nullptr) };
	}
	void Logger::openChannel(const Logger::Channel& channel, const std::shared_ptr<std::ostream>& out_ostream) {
		std::lock_guard<std::mutex> lock(mutex);
		internalCloseChannel(channel.number);
		channel_int_to_target[channel.number] = { channel.name, ChannelTarget(nullptr, out_ostream) };
		channel_string_to_target[channel.name] = { channel.number, ChannelTarget(nullptr, out_ostream) };
	}
	void Logger::openChannel(const Logger::Channel& channel, const std::shared_ptr<FILE>& out_file, const std::shared_ptr<std::ostream>& out_ostream) {
		std::lock_guard<std::mutex> lock(mutex);
		internalCloseChannel(channel.number);
		channel_int_to_target[channel.number] = { channel.name, ChannelTarget(out_file, out_ostream) };
		channel_string_to_target[channel.name] = { channel.number, ChannelTarget(out_file, out_ostream) };
	}

	void Logger::openChannel(const Logger::Channel& channel, const char* filename, bool append, const std::shared_ptr<std::ostream>& out_ostream) {
		std::shared_ptr<FILE> shared_ptr_file = internalMakeSharedFILE(filename, append);
		std::lock_guard<std::mutex> lock(mutex);
		internalCloseChannel(channel.number);
		channel_int_to_target[channel.number] = { channel.name, ChannelTarget(shared_ptr_file, out_ostream) };
		channel_string_to_target[channel.name] = { channel.number, ChannelTarget(shared_ptr_file, out_ostream) };
	}


	void Logger::openChannel(unsigned int number, const char* name, const char* filename, bool append) {
		std::shared_ptr<FILE> file_shared_ptr = internalMakeSharedFILE(filename, append);
		std::lock_guard<std::mutex> lock(mutex);
		internalCloseChannel(number);
		channel_int_to_target[number] = { name, ChannelTarget(file_shared_ptr, nullptr) };
		channel_string_to_target[name] = { number, ChannelTarget(file_shared_ptr, nullptr) };
	}
	void Logger::openChannel(unsigned int number, const char* name, const std::shared_ptr<FILE>& out_file) {
		std::lock_guard<std::mutex> lock(mutex);
		internalCloseChannel(number);
		channel_int_to_target[number] = { name, ChannelTarget(out_file, nullptr) };
		channel_string_to_target[name] = { number, ChannelTarget(out_file, nullptr) };
	}
	void Logger::openChannel(unsigned int number, const char* name, const std::shared_ptr<std::ostream>& out_ostream) {
		std::lock_guard<std::mutex> lock(mutex);
		internalCloseChannel(number);
		channel_int_to_target[number] = { name, ChannelTarget(nullptr, out_ostream) };
		channel_string_to_target[name] = { number, ChannelTarget(nullptr, out_ostream) };
	}
	void Logger::openChannel(unsigned int number, const char* name, const std::shared_ptr<FILE>& out_file, const std::shared_ptr<std::ostream>& out_ostream) {
		std::lock_guard<std::mutex> lock(mutex);
		internalCloseChannel(number);
		channel_int_to_target[number] = { name, ChannelTarget(out_file, out_ostream) };
		channel_string_to_target[name] = { number, ChannelTarget(out_file, out_ostream) };
	}
	void Logger::openChannel(unsigned int number, const char* name, const char* filename, bool append, const std::shared_ptr<std::ostream>& out_ostream) {
		std::shared_ptr<FILE> file_shared_ptr = internalMakeSharedFILE(filename, append);
		std::lock_guard<std::mutex> lock(mutex);
		internalCloseChannel(number);
		channel_int_to_target[number] = { name, ChannelTarget(file_shared_ptr, out_ostream) };
		channel_string_to_target[name] = { number, ChannelTarget(file_shared_ptr, out_ostream) };
	}
	

	void Logger::openChannel(unsigned int number, const char* filename, bool append) {
		std::shared_ptr<FILE> file_shared_ptr = internalMakeSharedFILE(filename, append);
		std::lock_guard<std::mutex> lock(mutex);
		internalCloseChannel(number);
		channel_int_to_target[number] = { CHANNEL_NONE_STR, ChannelTarget(file_shared_ptr, nullptr) };
	}
	void Logger::openChannel(unsigned int number, const std::shared_ptr<FILE>& out_file) {
		std::lock_guard<std::mutex> lock(mutex);
		internalCloseChannel(number);
		channel_int_to_target[number] = { CHANNEL_NONE_STR, ChannelTarget(out_file, nullptr) };
	}
	void Logger::openChannel(unsigned int number, const std::shared_ptr<std::ostream>& out_ostream) {
		std::lock_guard<std::mutex> lock(mutex);
		internalCloseChannel(number);
		channel_int_to_target[number] = { CHANNEL_NONE_STR, ChannelTarget(nullptr, out_ostream) };
	}
	void Logger::openChannel(unsigned int number, const std::shared_ptr<FILE>& out_file, const std::shared_ptr<std::ostream>& out_ostream) {
		std::lock_guard<std::mutex> lock(mutex);
		internalCloseChannel(number);
		channel_int_to_target[number] = { CHANNEL_NONE_STR, ChannelTarget(out_file, out_ostream) };
	}
	void Logger::openChannel(unsigned int number, const char* filename, bool append, const std::shared_ptr<std::ostream>& out_ostream) {
		std::shared_ptr<FILE> file_shared_ptr = internalMakeSharedFILE(filename, append);
		std::lock_guard<std::mutex> lock(mutex);
		internalCloseChannel(number);
		channel_int_to_target[number] = { CHANNEL_NONE_STR, ChannelTarget(file_shared_ptr, out_ostream) };
	}

	
	void Logger::openChannel(const char* name, const char* filename, bool append){
		std::shared_ptr<FILE> file_shared_ptr = internalMakeSharedFILE(filename, append);
		std::lock_guard<std::mutex> lock(mutex);
		internalCloseChannel(name);
		channel_string_to_target[name] = { CHANNEL_NONE_INT, ChannelTarget(file_shared_ptr, nullptr) };
	}
	void Logger::openChannel(const char* name, const std::shared_ptr<FILE>& out_file){
		std::lock_guard<std::mutex> lock(mutex);
		internalCloseChannel(name);
		channel_string_to_target[name] = { CHANNEL_NONE_INT, ChannelTarget(out_file, nullptr) };
	}
	void Logger::openChannel(const char* name, const std::shared_ptr<std::ostream>& out_ostream){
		std::lock_guard<std::mutex> lock(mutex);
		internalCloseChannel(name);
		channel_string_to_target[name] = { CHANNEL_NONE_INT, ChannelTarget(nullptr, out_ostream) };
	}
	void Logger::openChannel(const char* name, const std::shared_ptr<FILE>& out_file, const std::shared_ptr<std::ostream>& out_ostream){
		std::lock_guard<std::mutex> lock(mutex);
		internalCloseChannel(name);
		channel_string_to_target[name] = { CHANNEL_NONE_INT, ChannelTarget(out_file, out_ostream) };
	}
	void Logger::openChannel(const char* name, const char* filename, bool append, const std::shared_ptr<std::ostream>& out_ostream) {
		std::shared_ptr<FILE> file_shared_ptr = internalMakeSharedFILE(filename, append);
		std::lock_guard<std::mutex> lock(mutex);
		internalCloseChannel(name);
		channel_string_to_target[name] = { CHANNEL_NONE_INT, ChannelTarget(file_shared_ptr, out_ostream) };
	}


	void Logger::openChannel(const std::string& name, const char* filename, bool append){
		std::shared_ptr<FILE> file_shared_ptr = internalMakeSharedFILE(filename, append);
		std::lock_guard<std::mutex> lock(mutex);
		internalCloseChannel(name.c_str());
		channel_string_to_target[name] = { CHANNEL_NONE_INT, ChannelTarget(file_shared_ptr, nullptr) };
	}
	void Logger::openChannel(const std::string& name, const std::shared_ptr<FILE>& out_file){
		std::lock_guard<std::mutex> lock(mutex);
		internalCloseChannel(name.c_str());
		channel_string_to_target[name] = { CHANNEL_NONE_INT,ChannelTarget(out_file, nullptr) };
	}
	void Logger::openChannel(const std::string& name, const std::shared_ptr<std::ostream>& out_ostream){
		std::lock_guard<std::mutex> lock(mutex);
		internalCloseChannel(name.c_str());
		channel_string_to_target[name] = { CHANNEL_NONE_INT,ChannelTarget(nullptr, out_ostream) };
	}
	void Logger::openChannel(const std::string& name, const std::shared_ptr<FILE>& out_file, const std::shared_ptr<std::ostream>& out_ostream){
		std::lock_guard<std::mutex> lock(mutex);
		internalCloseChannel(name.c_str());
		channel_string_to_target[name] = { CHANNEL_NONE_INT,ChannelTarget(out_file, out_ostream) };
	}
	void Logger::openChannel(const std::string& name, const char* filename, bool append, const std::shared_ptr<std::ostream>& out_ostream) {
		std::shared_ptr<FILE> file_shared_ptr = internalMakeSharedFILE(filename, append);
		std::lock_guard<std::mutex> lock(mutex);
		internalCloseChannel(name.c_str());
		channel_string_to_target[name] = { CHANNEL_NONE_INT,ChannelTarget(file_shared_ptr, out_ostream) };
	}

	

	void Logger::closeChannel(const Logger::Channel& channel){
		std::lock_guard<std::mutex> lock(mutex);
		internalFlush();
		internalCloseChannel(channel.number);
	}
	void Logger::closeChannel(const std::string& name){
		std::lock_guard<std::mutex> lock(mutex);
		internalFlush();
		internalCloseChannel(name.c_str());
	}
	void Logger::closeChannel(unsigned int number){
		std::lock_guard<std::mutex> lock(mutex);
		internalFlush();
		internalCloseChannel(number);
	}
	void Logger::closeChannel(const char* name){
		std::lock_guard<std::mutex> lock(mutex);
		internalFlush();
		internalCloseChannel(name);
	}
	void Logger::closeAllChannels(){
		std::lock_guard<std::mutex> lock(mutex);
		internalFlush();
		channel_int_to_target.clear();
		channel_string_to_target.clear();
	}



	void Logger::pauseAllChannels(){
		std::lock_guard<std::mutex> lock(mutex);
		for (auto& e : channel_int_to_target) e.second.second.active = false;
		for (auto& e : channel_string_to_target) e.second.second.active = false;
	}
	void Logger::resumeAllChannels(){
		std::lock_guard<std::mutex> lock(mutex);
		for (auto& e : channel_int_to_target) e.second.second.active = true;
		for (auto& e : channel_string_to_target) e.second.second.active = true;
	}




	void Logger::pauseAllButChannel(const Logger::Channel& channel){
		std::lock_guard<std::mutex> lock(mutex);
		for (auto& e : channel_int_to_target) if(e.first != channel.number) e.second.second.active = false;
		for (auto& e : channel_string_to_target) if(e.first != channel.name) e.second.second.active = false;
	}
	void Logger::pauseAllButChannel(const std::string& name){
		std::lock_guard<std::mutex> lock(mutex);
		unsigned int mapped_int = CHANNEL_NONE_INT;
		for (auto& e : channel_string_to_target) if (e.first != name) e.second.second.active = false;
												else mapped_int = e.second.first;
		//if name is also mapped to an int
		if(mapped_int!=CHANNEL_NONE_INT) for (auto& e : channel_int_to_target) if (e.first != mapped_int) e.second.second.active = false;
	}
	void Logger::pauseAllButChannel(const char* name){
		std::lock_guard<std::mutex> lock(mutex);
		unsigned int mapped_int = CHANNEL_NONE_INT;
		for (auto& e : channel_string_to_target) if (e.first != name) e.second.second.active = false;
												else mapped_int = e.second.first;
		//if name is also mapped to an int
		if (mapped_int != CHANNEL_NONE_INT) for (auto& e : channel_int_to_target) if (e.first != mapped_int) e.second.second.active = false;
	}
	void Logger::pauseAllButChannel(unsigned int number){
		std::lock_guard<std::mutex> lock(mutex);
		std::string mapped_str = CHANNEL_NONE_STR;
		for (auto& e : channel_int_to_target) if (e.first != number) e.second.second.active = false;
												else mapped_str = e.second.first;
		//if name is also mapped to an int
		if (mapped_str != CHANNEL_NONE_STR) for (auto& e : channel_string_to_target) if (e.first != mapped_str) e.second.second.active = false;
	}




	void Logger::resumeAllButChannel(const Logger::Channel& channel) {
		std::lock_guard<std::mutex> lock(mutex);
		for (auto& e : channel_int_to_target) if (e.first != channel.number) e.second.second.active = true;
		for (auto& e : channel_string_to_target) if (e.first != channel.name) e.second.second.active = true;
	}
	void Logger::resumeAllButChannel(const std::string& name) {
		std::lock_guard<std::mutex> lock(mutex);
		unsigned int mapped_int = CHANNEL_NONE_INT;
		for (auto& e : channel_string_to_target) if (e.first != name) e.second.second.active = true;
		else mapped_int = e.second.first;
		//if name is also mapped to an int
		if (mapped_int != CHANNEL_NONE_INT) for (auto& e : channel_int_to_target) if (e.first != mapped_int) e.second.second.active = true;
	}
	void Logger::resumeAllButChannel(const char* name) {
		std::lock_guard<std::mutex> lock(mutex);
		unsigned int mapped_int = CHANNEL_NONE_INT;
		for (auto& e : channel_string_to_target) if (e.first != name) e.second.second.active = true;
		else mapped_int = e.second.first;
		//if name is also mapped to an int
		if (mapped_int != CHANNEL_NONE_INT) for (auto& e : channel_int_to_target) if (e.first != mapped_int) e.second.second.active = true;
	}
	void Logger::resumeAllButChannel(unsigned int number) {
		std::lock_guard<std::mutex> lock(mutex);
		std::string mapped_str = CHANNEL_NONE_STR;
		for (auto& e : channel_int_to_target) if (e.first != number) e.second.second.active = true;
		else mapped_str = e.second.first;
		//if number is also mapped to an string
		if (mapped_str != CHANNEL_NONE_STR) for (auto& e : channel_string_to_target) if (e.first != mapped_str) e.second.second.active = true;
	}




	void Logger::pauseChannel(unsigned int number){
		std::lock_guard<std::mutex> lock(mutex);
		internalActiveChannel(number, false);
	}
	void Logger::pauseChannel(const char* name){
		std::lock_guard<std::mutex> lock(mutex);
		internalActiveChannel(name, false);
	}
	void Logger::pauseChannel(const std::string& name){
		std::lock_guard<std::mutex> lock(mutex);
		internalActiveChannel(name.c_str(), false);
	}
	void Logger::pauseChannel(const Logger::Channel& channel){
		std::lock_guard<std::mutex> lock(mutex);
		internalActiveChannel(channel.number, false);
	}




	void Logger::resumeChannel(unsigned int number) {
		std::lock_guard<std::mutex> lock(mutex);
		internalActiveChannel(number, true);
	}
	void Logger::resumeChannel(const char* name) {
		std::lock_guard<std::mutex> lock(mutex);
		internalActiveChannel(name, true);
	}
	void Logger::resumeChannel(const std::string& name) {
		std::lock_guard<std::mutex> lock(mutex);
		internalActiveChannel(name.c_str(), true);
	}
	void Logger::resumeChannel(const Logger::Channel& channel) {
		std::lock_guard<std::mutex> lock(mutex);
		internalActiveChannel(channel.number, true);
	}



	bool Logger::isChannelActive(const Logger::Channel& channel){
		std::lock_guard<std::mutex> lock(mutex);
		auto it = channel_int_to_target.find(channel.number);
		if (it != channel_int_to_target.end()) return it->second.second.active;
		//does not exist
		return false;
	}
	bool Logger::isChannelActive(const std::string& name){
		std::lock_guard<std::mutex> lock(mutex);
		auto it = channel_string_to_target.find(name);
		if (it != channel_string_to_target.end()) return it->second.second.active;
		//does not exist
		return false;
	}
	bool Logger::isChannelActive(const char* name){
		std::lock_guard<std::mutex> lock(mutex);
		auto it = channel_string_to_target.find(name);
		if (it != channel_string_to_target.end()) return it->second.second.active;
		//does not exist
		return false;
	}
	bool Logger::isChannelActive(unsigned int number){
		std::lock_guard<std::mutex> lock(mutex);
		auto it = channel_int_to_target.find(number);
		if (it != channel_int_to_target.end()) return it->second.second.active;
		//does not exist
		return false;
	}



	void Logger::setOutputFlushInterval(unsigned int ms){
		std::lock_guard<std::mutex> lock(mutex);
		if (ms == 0) ms = 1;
		thread_flush_ms = ms;
	}
	unsigned int Logger::getOutputFlushInterval(){
		std::lock_guard<std::mutex> lock(mutex);
		return thread_flush_ms;
	}


	void Logger::setHeaderType(const HeaderType& type){
		std::lock_guard<std::mutex> lock(mutex);
		header_type = type;
	}
	Logger::HeaderType Logger::getHeaderType(){
		std::lock_guard<std::mutex> lock(mutex);
		return header_type;
	}


	void Logger::flush() {
		std::lock_guard<std::mutex> lock(mutex);
		internalFlush();
	}

	

	void Logger::internalPrintHeader(const std::string& channel, const char* file, unsigned int line, const char* func, std::string& output){
		//no header? 
		if (header_type == HEADER_NONE) return;

		//this function is called only from sync'd code, so why construct more than once?
		static std::string channelstr, filestr, linestr, funcstr, threadstr;
		static std::stringstream ss;
		static char timestr[100];

		if (header_type.hasChannel()) channelstr = "["+channel+"]";
		if (header_type.hasFile()) filestr = std::string("[")+file+"]";
		if (header_type.hasLine()) linestr = "["+std::to_string(line)+"]";
		if (header_type.hasFunc()) funcstr = std::string("[") +func+"]";
		if (header_type.hasThread()) {
			ss.str("");
			ss.clear();
			ss << std::this_thread::get_id();
			threadstr = "[" + ss.str() + "]";
		}
		if (header_type.hasTime()) {
			std::time_t now = std::time(nullptr);
			std::tm timeinfo;
#if defined(_MSC_VER) || defined(__STDC_LIB_EXT1__)
			localtime_s(&timeinfo, &now);
#else
			localtime_r(&now, &timeinfo);
#endif
			std::strftime(timestr, 100, "%Y-%m-%d %H:%M:%S", &timeinfo);
		}

		unsigned int dlbs = DEFAULT_LOCAL_BUFFER_SIZE;
		const auto& descriptors = header_type.getDescriptors();
		unsigned int idx = 0;
		for (unsigned int i = 0; i < descriptors.size(); i++) {
			const std::string& descformat = descriptors[i].second;
			switch (descriptors[i].first) {
				case HeaderType::ACHANNEL :	idx += snprintf(&local_buffer[idx], dlbs - idx, descformat.c_str(), channelstr.c_str());	 break;
				case HeaderType::AFILE :	idx += snprintf(&local_buffer[idx], dlbs - idx, descformat.c_str(), filestr.c_str());	break;
				case HeaderType::AFUNC :	idx += snprintf(&local_buffer[idx], dlbs - idx, descformat.c_str(), funcstr.c_str()); break;
				case HeaderType::ALINE :	idx += snprintf(&local_buffer[idx], dlbs - idx, descformat.c_str(), linestr.c_str()); break;
				case HeaderType::ATHREAD :	idx += snprintf(&local_buffer[idx], dlbs - idx, descformat.c_str(), threadstr.c_str()); break;
				case HeaderType::ATIME :	idx += snprintf(&local_buffer[idx], dlbs - idx, descformat.c_str(), timestr); break;
				case HeaderType::NCHANNEL :	idx += snprintf(&local_buffer[idx], dlbs - idx, descformat.c_str(), channelstr.c_str()); break;
				case HeaderType::NFILE :	idx += snprintf(&local_buffer[idx], dlbs - idx, descformat.c_str(), filestr.c_str()); break;
				case HeaderType::NFUNC :	idx += snprintf(&local_buffer[idx], dlbs - idx, descformat.c_str(), funcstr.c_str()); break;
				case HeaderType::NLINE :	idx += snprintf(&local_buffer[idx], dlbs - idx, descformat.c_str(), linestr.c_str()); break;
				case HeaderType::NTHREAD :	idx += snprintf(&local_buffer[idx], dlbs - idx, descformat.c_str(), threadstr.c_str()); break;
				case HeaderType::NTIME :	idx += snprintf(&local_buffer[idx], dlbs - idx, descformat.c_str(), timestr); break;
				default: break;
			}
		}
		output += local_buffer;
		output += " ";
	}


	void Logger::print(const Logger::Channel& channel, const char* filename, unsigned int line, const char* function, const char* format, ...){
		std::lock_guard<std::mutex> lock(mutex);
		
		//find output target
		const auto it = channel_int_to_target.find(channel.number);
		if (it == channel_int_to_target.end()) return;
		if (!it->second.second.active) return;
		
		//create command
		commands.emplace_back(Command(it->second.second.out_file, it->second.second.out_ostream));
		
		//header
		internalPrintHeader(channel.getHeaderString(), filename, line, function, commands.back().content);
		
		//contents
		va_list args;
		va_start(args, format);
	#ifdef _MSC_VER
		vsnprintf_s(local_buffer, DEFAULT_LOCAL_BUFFER_SIZE, format, args);
	#else
		vsnprintf(local_buffer, DEFAULT_LOCAL_BUFFER_SIZE, format, args);
	#endif
		va_end(args);
		commands.back().content += local_buffer;
	}
	void Logger::print(int channel, const char* filename, unsigned int line, const char* function, const char* format, ...){
		std::lock_guard<std::mutex> lock(mutex);

		//find output target
		const auto it = channel_int_to_target.find(channel);
		if (it == channel_int_to_target.end()) return;
		if (!it->second.second.active) return;

		//create command
		commands.emplace_back(Command(it->second.second.out_file, it->second.second.out_ostream));

		//header
		std::string channel_name = it->second.first;
		if (channel_name != CHANNEL_NONE_STR) channel_name += "|";
		channel_name += std::to_string(channel);
		internalPrintHeader(channel_name, filename, line, function, commands.back().content);

		//contents
		va_list args;
		va_start(args, format);
#ifdef _MSC_VER
		vsnprintf_s(local_buffer, DEFAULT_LOCAL_BUFFER_SIZE, format, args);
#else
		vsnprintf(local_buffer, DEFAULT_LOCAL_BUFFER_SIZE, format, args);
#endif
		va_end(args);
		commands.back().content += local_buffer;
	}
	void Logger::print(const char* channel, const char* filename, unsigned int line, const char* function, const char* format, ...){
		std::lock_guard<std::mutex> lock(mutex);

		//find output target
		const auto it = channel_string_to_target.find(channel);
		if (it == channel_string_to_target.end()) return;
		if (!it->second.second.active) return;

		//create command
		commands.emplace_back(Command(it->second.second.out_file, it->second.second.out_ostream));

		//header
		std::string headerstring = std::string(channel);
		if (it->second.first != CHANNEL_NONE_INT) headerstring += "|" + std::to_string(it->second.first);
		internalPrintHeader(headerstring, filename, line, function, commands.back().content);

		//contents
		va_list args;
		va_start(args, format);
#ifdef _MSC_VER
		vsnprintf_s(local_buffer, DEFAULT_LOCAL_BUFFER_SIZE, format, args);
#else
		vsnprintf(local_buffer, DEFAULT_LOCAL_BUFFER_SIZE, format, args);
#endif
		va_end(args);
		commands.back().content += local_buffer;
	}
	void Logger::print(const std::string& channel, const char* filename, unsigned int line, const char* function, const char* format, ...){
		std::lock_guard<std::mutex> lock(mutex);

		//find output target
		const auto it = channel_string_to_target.find(channel);
		if (it == channel_string_to_target.end()) return;
		if (!it->second.second.active) return;

		//create command
		commands.emplace_back(Command(it->second.second.out_file, it->second.second.out_ostream));

		//header
		std::string headerstring = channel;
		if (it->second.first != CHANNEL_NONE_INT) headerstring += "|" + std::to_string(it->second.first);
		internalPrintHeader(headerstring, filename, line, function, commands.back().content);

		//contents
		va_list args;
		va_start(args, format);
#ifdef _MSC_VER
		vsnprintf_s(local_buffer, DEFAULT_LOCAL_BUFFER_SIZE, format, args);
#else
		vsnprintf(local_buffer, DEFAULT_LOCAL_BUFFER_SIZE, format, args);
#endif
		va_end(args);
		commands.back().content += local_buffer;
	}




	void Logger::printStream(const Logger::Channel& channel, const char* filename, unsigned int line, const char* function, const char* contents){
		std::lock_guard<std::mutex> lock(mutex);
		//find output 
		const auto it = channel_int_to_target.find(channel.number);
		if (it == channel_int_to_target.end()) return;
		if (!it->second.second.active) return;
				
		//create command
		commands.emplace_back(Command(it->second.second.out_file, it->second.second.out_ostream));

		//header
		internalPrintHeader(channel.getHeaderString(), filename, line, function, commands.back().content);
		
		//content
		commands.back().content += contents;

	}
	void Logger::printStream(int channel, const char* filename, unsigned int line, const char* function, const char* contents){
		std::lock_guard<std::mutex> lock(mutex);
		//find output 
		const auto it = channel_int_to_target.find(channel);
		if (it == channel_int_to_target.end()) return;
		if (!it->second.second.active) return;

		//create command
		commands.emplace_back(Command(it->second.second.out_file, it->second.second.out_ostream));

		//header
		std::string channel_name = it->second.first;
		if (channel_name != CHANNEL_NONE_STR) channel_name += "|";
		channel_name += std::to_string(channel);
		internalPrintHeader(channel_name, filename, line, function, commands.back().content);

		//content
		commands.back().content += contents;
	}
	void Logger::printStream(const char* channel, const char* filename, unsigned int line, const char* function, const char* contents){
		std::lock_guard<std::mutex> lock(mutex);
		//find output 
		const auto it = channel_string_to_target.find(channel);
		if (it == channel_string_to_target.end()) return;
		if (!it->second.second.active) return;

		//create command
		commands.emplace_back(Command(it->second.second.out_file, it->second.second.out_ostream));

		//header
		std::string headerstring = std::string(channel);
		if (it->second.first != CHANNEL_NONE_INT) headerstring += "|" + std::to_string(it->second.first);
		internalPrintHeader(headerstring, filename, line, function, commands.back().content);

		//content
		commands.back().content += contents;
	}
	void Logger::printStream(const std::string& channel, const char* filename, unsigned int line, const char* function, const char* contents){
		std::lock_guard<std::mutex> lock(mutex);
		//find output 
		const auto it = channel_string_to_target.find(channel);
		if (it == channel_string_to_target.end()) return;
		if (!it->second.second.active) return;

		//create command
		commands.emplace_back(Command(it->second.second.out_file, it->second.second.out_ostream));

		//header
		std::string headerstring = std::string(channel);
		if (it->second.first != CHANNEL_NONE_INT) headerstring += "|" + std::to_string(it->second.first);
		internalPrintHeader(headerstring, filename, line, function, commands.back().content);

		//content
		commands.back().content += contents;
	}

	///workaround for deadlock in VS CRT 2013 STILL not solved as of today.
	#if defined(_MSC_VER) && (_MSC_VER<1900)
		#pragma warning(push)
		#pragma warning(disable:4073)
		#pragma init_seg(lib)
		namespace detail{
			struct VS2013ThreadingFix
			{
				VS2013ThreadingFix()
				{
					_Cnd_do_broadcast_at_thread_exit();
				}
			} VS2013_threading_fix;
		}
		#pragma warning(pop)
	#endif

	namespace detail {
		LoggerStreamCollector::LoggerStreamCollector(const Logger::Channel& ch, const char* f, unsigned int ln, const char* fn) : channel_i(ch.number), type(TYPE_INT), filename(f), line(ln), function(fn) {}
		LoggerStreamCollector::LoggerStreamCollector(const char *ch, const char* f, unsigned int ln, const char* fn) : channel_c(ch), type(TYPE_CHARP), filename(f), line(ln), function(fn) {}
		LoggerStreamCollector::LoggerStreamCollector(unsigned int ch, const char* f, unsigned int ln, const char* fn) : channel_i(ch), type(TYPE_INT), filename(f), line(ln), function(fn) {}
		LoggerStreamCollector::LoggerStreamCollector(int ch, const char* f, unsigned int ln, const char* fn) : channel_i(ch), type(TYPE_INT), filename(f), line(ln), function(fn) {}
		LoggerStreamCollector::~LoggerStreamCollector() {
			if (type == TYPE_INT) Logger::getInstance().printStream(channel_i, filename, line, function, this->str().c_str());
			else Logger::getInstance().printStream(channel_c, filename, line, function, this->str().c_str());
		}
	}
}