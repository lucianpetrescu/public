# README #

This is a gpu timer, written with OpenGL 3.3 commands. It is useful for fine timing GPU-commands. The timer uses a time query pool to quickly issue time measurment gpu commands (OpenGL time queries). Because all GPUs work with command buffers the timer queries are just time commands added between the commands in the command buffer. As all GPU measurement based tools, the GPUtimer inserts a small number of query commands which should be read (timer. getXXXX()) at the end of the frame/computation when all commands are most likely to be finished. Otherwise the get commands will WAIT until the query commands (and all the previous GPU commands) are processed. If the user is performing complicated back and forth CPU-GPU streaming operations without proper usage of synchronization (glFinish/glFlush/barriers) then the timings might propagate because of automated GPU transfer synchronization. 

version: 1.00

author: Lucian Petrescu( lucian (dot) petrescu (dot) 24 (at) gmail (dot) com )


### SCREENSHOTS ###

Usage example:

 ![functions are bundled into groups](img/example.png) 

###A simplified view of timer queries on the GPU###

A generic GPU command buffer:

	| cmd cmd cmd cmd cmd cmd ............ cmd cmd cmd cmd |
A generic GPU command buffer with queries inserted, each query has a number which correponds to its index in the query pool

	| cmd cmd cmd q1 cmd q5 ............ cmd q2 cmd cmd q14|
The query commands are inserted between the original commands, and time is measured between the two queries, e.g. q1->q5.

###Properties###

- c++11, no exceptions
- OpenGL 3.3
- the OpenGL functions must be provided by the user, see line 90. This could have been done with extern functions but some OpenGL extension loader libraries are based on macros. Furthermore macros are used by many hook-based OpenGL debug libraries, therefore the current system was preferred.

###Compiling and linking###

Visual studio 2015 project&solution files can be found in the **vstudio** folder. Makefiles for gnu/gcc ( **makefile** ) and for visual studio ( **makefilevs.bat** ) are provided in the root folder. All methods will generate objs, pdbs, exes, etc in **bin**.

- Windows:
	- no special setup for visual studio projects
	- -pthread -lgdi32 otherwise
	- an example visual studio project+solution is provide in the vstudio folder
	- the makefilevs.bat from the root folder can also be used
- Linux:
	- -ldl -lGL -lX11 -lXxf86vm -lXrandr -lXi -lXinerama -lXcursor -lrt -lm
	- the wayland and mir backends are not used by lapwic (but they can easily be added in the future when they mature, right now they are experimental in glfw)
	- an example makefile is provided in the root folder
	- to install the project dependencies get:
		- sudo apt-get install xorg-dev
		- sudo apt-get install libgl-dev
	- also compiles in a VMWARE virtual machine, but needs a special mesa driver http://mesa3d.org/vmware-guest.html. It will crash as the mesa driver does not support OpenGL 4.5.
- OSX
	- -framework Cocoa -framework OpenGL -framework IOKit -framework CoreVideo -framework CoreFoundation -framework Carbon
	- an example makefile is provided in the root folder\n
	

###Usage###

- basic usage

		... include function loader header ...
		#include "lap_gpu_timer.hpp"
		... initialize window with OpenGL context ...
		lap::gpu::GPUTimer gputimer(3);
		gputimer.insertQuery(2);		// insert query number 2 at the beginning
		... do some GPU work here ...
		gputimer.insertQuery(1);		// insert query number 1 here
		... do some GPU work here ...
		gputimer.insertQuery(0);		// insert query number 0 at the end
	
		gputimer.waitQueries();			//wait for queries to finish (= commands before them to finish)
		std::cout << "The gputimer query pool has " << gputimer.getPoolSize() << " entries." << std::endl;
		std::cout << " It took " << gputimer.getTimeInMilliseconds(2, 1) << "ms between 2 and 1." << std::endl;
		std::cout << " It took " << gputimer.getTimeInMilliseconds(1, 0) << "ms between 1 and 0." << std::endl;

###TODO LIST###

nothing

### DOCUMENTATION ###

a Doxyfile is provided in **doc**, which can be used with Doxygen to generate  the entire documentation in HTML format. 
Note: doxygen has problems with arguments of std::function type function arguments (used for callbacks) and will issue a large number of warnings.


### COMPATIBILITY ###

tested with visual studio 2015, MinGW x64 4.0, Ubuntu 15 + gcc 4.8.4, MacOS El Capitan + clang 3.7.1.



