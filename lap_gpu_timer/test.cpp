///---------------------------------------------------------------------------------------------------------------------
/// The MIT License (MIT)
/// 
/// Copyright (c) 2016 Lucian Petrescu, lucian (dot) petrescu (dot) 24 (at) gmail (dot) com
/// 
/// Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation 
/// files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, 
/// modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software 
/// is furnished to do so, subject to the following conditions:
/// 
/// The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
/// 
/// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
/// OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE 
/// LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR 
/// IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
///---------------------------------------------------------------------------------------------------------------------


#include "dep/lap_wic.hpp"
#include "lap_gpu_timer.hpp"
#include <iostream>

//-------------------------------------------------------------------------------------------------
void test() {
	//create a window manager and a window with OpenGL support
	lap::wic::WICSystem wicsystem(nullptr);	//no logging
	auto wp = lap::wic::WindowProperties();
	wp.visible = false;
	lap::wic::Window window = lap::wic::Window(wp, lap::wic::FramebufferProperties(), lap::wic::ContextProperties(), lap::wic::InputProperties());

	//define work
	auto workfunc = [&](int iterations) {
		GLuint buf;
		glCreateBuffers(1, &buf);
		std::vector<unsigned char> data; data.resize(1000000);	//1mb
		for (int i = 0; i < iterations; i++) glNamedBufferData(buf, data.size(), data.data(), GL_STATIC_DRAW);	//do some gpu work
		glMemoryBarrier(GL_ALL_BARRIER_BITS);
		glDeleteBuffers(1, &buf);
	};

	//create a gpu timer with a pool of 5 query entries
	lap::gpu::GPUTimer gputimer(5);

	gputimer.insertQuery(3);		// insert query number 3 at the beginning
	workfunc(1);					// do some work
	gputimer.insertQuery(4);		// insert query number 4 here
	
	workfunc(50);					// do some work
	gputimer.insertQuery(1);		// insert query number 1 here
	
	workfunc(250);					// do some work
	gputimer.insertQuery(0);		// insert query number 0 here

	workfunc(1000);					// do some work
	gputimer.insertQuery(2);		// insert query number 2 at the end

	//wait for the queries
	std::cout << "The gputimer query pool has " << gputimer.getPoolSize() << " entries." << std::endl;
	std::cout << " It took " << gputimer.getTimeInMillisecondsAfterWaitingQueries(3, 4) << " ms between 3 and 4." << std::endl;
	std::cout << " It took " << gputimer.getTimeInMillisecondsAfterWaitingQueries(4, 1) << " ms between 4 and 1." << std::endl;
	std::cout << " It took " << gputimer.getTimeInMillisecondsAfterWaitingQueries(1, 0) << " ms between 1 and 0." << std::endl;
	std::cout << " It took " << gputimer.getTimeInMillisecondsAfterWaitingQueries(0, 2) << " ms between 0 and 2." << std::endl;

	//window manager and window go out of scope and self destroy
}

//-------------------------------------------------------------------------------------------------
int main(int argc, char* argv[]){

	test();

	std::cout << "Finished" << std::endl;
	std::cin.get();
	return 0;
}
