﻿///---------------------------------------------------------------------------------------------------------------------
/// The MIT License (MIT)
/// 
/// Copyright (c) 2016 Lucian Petrescu, lucian (dot) petrescu (dot) 24 (at) gmail (dot) com
/// 
/// Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation 
/// files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, 
/// modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software 
/// is furnished to do so, subject to the following conditions:
/// 
/// The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
/// 
/// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
/// OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE 
/// LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR 
/// IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
///---------------------------------------------------------------------------------------------------------------------

#include "lap_gpu_timer.hpp"
#include <cassert>

namespace lap{
	namespace gpu {

		//----------------------------------------------------------------------
		GPUTimer::GPUTimer(unsigned int count) {
			queries.resize(count);
			glGenQueries(count, &queries[0]);
		}
		GPUTimer::~GPUTimer() {
			glDeleteQueries((GLsizei)queries.size(), &queries[0]);
		}

		///------------------------------------------------------------------------------------
		void GPUTimer::insertQuery(unsigned int index) {
			assert(index < queries.size());
			glQueryCounter(queries[index], GL_TIMESTAMP);
		}

		///------------------------------------------------------------------------------------
		unsigned int GPUTimer::getPoolSize() {
			return (unsigned int)queries.size();
		}
		bool GPUTimer::isQueryFinished(unsigned int index) {
			assert(index < queries.size());
			GLint done = GL_FALSE;
			glGetQueryObjectiv(queries[index], GL_QUERY_RESULT_AVAILABLE, &done);
			return done != GL_FALSE;
		}
		double GPUTimer::getTimeInMillisecondsAfterWaitingQuery(unsigned int index) {
			assert(index < queries.size());
			GLint done = GL_FALSE;
			while (done != GL_FALSE) glGetQueryObjectiv(queries[index], GL_QUERY_RESULT_AVAILABLE, &done);
			GLuint64 value;
			glGetQueryObjectui64v(queries[index], GL_QUERY_RESULT, &value);
			return value / 1000000.0;  //nano to milli
		}
		double GPUTimer::getTimeInMillisecondsAfterWaitingQueries(unsigned int index1, unsigned int index2) {
			double value = getTimeInMillisecondsAfterWaitingQuery(index1) - getTimeInMillisecondsAfterWaitingQuery(index2);
			if (value > 0) return value;
			else return -value;
		}
	}
}
