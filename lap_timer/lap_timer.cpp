///---------------------------------------------------------------------------------------------------------------------
/// The MIT License (MIT)
/// 
/// Copyright (c) 2016 Lucian Petrescu, lucian (dot) petrescu (dot) 24 (at) gmail (dot) com
/// 
/// Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation 
/// files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, 
/// modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software 
/// is furnished to do so, subject to the following conditions:
/// 
/// The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
/// 
/// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
/// OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE 
/// LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR 
/// IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
///---------------------------------------------------------------------------------------------------------------------

#include "lap_timer.hpp"

namespace lap {

	Timer::Timer() {
		origin = std::chrono::high_resolution_clock::now();
		lastcall = origin;
	}
	double Timer::frequency() {
		return (double)std::chrono::high_resolution_clock::period::den / std::chrono::high_resolution_clock::period::num;
	}
	double Timer::period() {
		return (double)std::chrono::high_resolution_clock::period::num / std::chrono::high_resolution_clock::period::den;
	}

	void Timer::reset() {
		origin = std::chrono::high_resolution_clock::now();
		lastcall = origin;
	}

	void Timer::waitNanoseconds(double duration) const {
		if (duration < 0) return;
		std::this_thread::sleep_for(std::chrono::nanoseconds((int64_t)(duration)));
	}
	void Timer::waitMicroseconds(double duration) const {
		if (duration < 0) return;
		std::this_thread::sleep_for(std::chrono::nanoseconds((int64_t)(duration*1000.0)));
	}
	void Timer::waitMilliseconds(double duration) const {
		if (duration < 0) return;
		std::this_thread::sleep_for(std::chrono::microseconds((int64_t)(duration*1000.0)));
	}
	void Timer::waitSeconds(double duration) const {
		if (duration < 0) return;
		std::this_thread::sleep_for(std::chrono::milliseconds((int64_t)(duration*1000.0)));
	}
	void Timer::waitMinutes(double duration) const {
		if (duration < 0) return;
		std::this_thread::sleep_for(std::chrono::milliseconds((int64_t)(duration*60.0*1000.0)));
	}
	void Timer::waitHours(double duration) const {
		if (duration < 0) return;
		std::this_thread::sleep_for(std::chrono::milliseconds((int64_t)(duration*3600.0*1000.0)));
	}


	double Timer::elapsedNanoseconds() const {
		lastcall = std::chrono::high_resolution_clock::now();
		return std::chrono::duration<double>(lastcall - origin).count() * 1000 * 1000 * 1000;
	}
	double Timer::elapsedMicroseconds() const {
		lastcall = std::chrono::high_resolution_clock::now();
		return std::chrono::duration<double>(lastcall - origin).count() * 1000 * 1000;
	}
	double Timer::elapsedMilliseconds() const {
		lastcall = std::chrono::high_resolution_clock::now();
		return std::chrono::duration<double>(lastcall - origin).count() * 1000;
	}
	double Timer::elapsedSeconds() const {
		lastcall = std::chrono::high_resolution_clock::now();
		return std::chrono::duration<double>(lastcall - origin).count();
	}
	double Timer::elapsedMinutes() const {
		lastcall = std::chrono::high_resolution_clock::now();
		return std::chrono::duration<double>(lastcall - origin).count() / 60.0;
	}
	double Timer::elapsedHours() const {
		lastcall = std::chrono::high_resolution_clock::now();
		return std::chrono::duration<double>(lastcall - origin).count() / 3600.0;
	}

	double Timer::elapsedLastCallNanoseconds() const {
		std::chrono::time_point<std::chrono::high_resolution_clock> newlastcall = std::chrono::high_resolution_clock::now();
		double result = std::chrono::duration<double>(newlastcall - lastcall).count() * 1000 * 1000 * 1000;
		lastcall = newlastcall;
		return result;
	}
	double Timer::elapsedLastCallMicroseconds() const {
		std::chrono::time_point<std::chrono::high_resolution_clock> newlastcall = std::chrono::high_resolution_clock::now();
		double result = std::chrono::duration<double>(newlastcall - lastcall).count() * 1000 * 1000;
		lastcall = newlastcall;
		return result;
	}
	double Timer::elapsedLastCallMilliseconds() const {
		std::chrono::time_point<std::chrono::high_resolution_clock> newlastcall = std::chrono::high_resolution_clock::now();
		double result = std::chrono::duration<double>(newlastcall - lastcall).count() * 1000;
		lastcall = newlastcall;
		return result;
	}
	double Timer::elapsedLastCallSeconds() const {
		std::chrono::time_point<std::chrono::high_resolution_clock> newlastcall = std::chrono::high_resolution_clock::now();
		double result = std::chrono::duration<double>(newlastcall - lastcall).count();
		lastcall = newlastcall;
		return result;
	}
	double Timer::elapsedLastCallMinutes() const {
		std::chrono::time_point<std::chrono::high_resolution_clock> newlastcall = std::chrono::high_resolution_clock::now();
		double result = std::chrono::duration<double>(newlastcall - lastcall).count() / 60.0;
		lastcall = newlastcall;
		return result;
	}
	double Timer::elapsedLastCallHours() const {
		std::chrono::time_point<std::chrono::high_resolution_clock> newlastcall = std::chrono::high_resolution_clock::now();
		double result = std::chrono::duration<double>(newlastcall - lastcall).count() / 3600.0;
		lastcall = newlastcall;
		return result;
	}
}