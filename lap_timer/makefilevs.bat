@echo off
rem -----------------------------------------------------------------------------------------
rem this pseudo-makefile uses Microsfot Visual Studio 2015. If this IDE is unavailable then the
rem MinGW-enabled makefile can be used. This requires a working 64bit, cpp11 enabled MinGW.
rem -----------------------------------------------------------------------------------------
setlocal

rem set the path to include devenv
if not defined DevEnvDir (
    call "C:\Program Files (x86)\Microsoft Visual Studio 14.0\VC\vcvarsall.bat" amd64
)

rem determine the name of the project
for %%F in ("%cd%\vstudio\*.sln") do (
 set PROJECT_NAME=%%~nF
 goto Start
)
:Start

rem targets
if "%~1" == "debug" goto DebugTarget
if "%~1" == "release" goto ReleaseTarget
if "%~1" == "all" goto AllTarget
if "%~1" == "clean" goto CleanTarget
goto AllTarget

:DebugTarget
	echo -------------- Building %PROJECT_NAME% in Debug mode ----------------
	devenv "vstudio/%PROJECT_NAME%.sln" /Build Debug /nologo
goto End
	
:ReleaseTarget
	echo -------------- Building %PROJECT_NAME% in Release mode --------------
	devenv "vstudio/%PROJECT_NAME%.sln" /Build Release /nologo
goto End

:AllTarget
	echo -------------- Building %PROJECT_NAME% in Debug mode ----------------
	devenv "vstudio/%PROJECT_NAME%.sln" /Build Debug /nologo
	echo -------------- Building %PROJECT_NAME% in Release mode --------------
	devenv "vstudio/%PROJECT_NAME%.sln" /Build Release /nologo
goto End

:CleanTarget
	echo -------------- Cleaning %PROJECT_NAME% ------------------------------
	devenv "vstudio/%PROJECT_NAME%.sln" /Clean Debug /nologo
	devenv "vstudio/%PROJECT_NAME%.sln" /Clean Release /nologo
	if exist bin\Debug ( rd /s /q bin\Debug & echo rd /s /q bin\Debug )
	if exist bin\Release ( rd /s /q bin\Release & echo rd /s /q bin\Release )
	if exist vstudio\.vs ( rd /s /q vstudio\.vs & echo rd /s /q vstudio\.vs )
	if exist bin\*.iobj ( del bin\*.iobj & echo del bin\*.iobj )
	if exist bin\*.ipdb ( del bin\*.ipdb & echo del bin\*.ipdb )
	if exist bin\*.pdb ( del bin\*.pdb & echo del bin\*.pdb )
	if exist vstudio\*.sdf ( del vstudio\*.sdf & echo del vstudio\*.sdf )
	if exist vstudio\*.suo ( del vstudio\*.suo & echo del vstudio\*.suo )
	if exist vstudio\*.db ( del vstudio\*.db & echo del vstudio\*.db )
	if exist vstudio\*.opendb ( del vstudio\*.opendb & echo del vstudio\*.opendb )
goto End

:End
endlocal