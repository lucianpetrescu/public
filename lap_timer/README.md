# README #

LAP Timer is a simple timer utility class, for quick usage instead of employing the (very) verbose std::chrono
it also acts as a personal standard model for folder organization, documentation and portability.

version 1.00
author Lucian Petrescu( lucian (dot) petrescu (dot) 24 (at) gmail (dot) com )


### SCREENSHOTS ###

Basic timer usage

 ![functions are bundled into groups](img/timer.png) 


###PROPERTIES###
- c++11


### COMPILING ###

visual studio 2015 project&solution files can be found in the **vstudio** folder (will generate objs, pdbs, executables, etc in **bin**)
a makefile is provided (will generate objs and executables in **bin**)

### EXAMPLES ###

- basic usage:

		#include "lap_timer.hpp"
		...
		lap::Timer timer;
		timer.waitSeconds(0.58);
		std::cout << "Waited 0.58s -> milli " << timer.elapsedMilliseconds() << std::endl;
		std::cout << "Waited 0.58s -> hours " << timer.elapsedHours() << std::endl;
		for(int i=0;i<100;i++){
			timer.waitSeconds(0.5);
			std::cout<<" Since last call "<< timer.elapsedLastCallMicroseconds()<<std::endl;
		}

### DOCUMENTATION ###

a Doxyfile is provided in **doc**, which can be used with Doxygen to generate documentation in HTML format. 

### COMPATIBILITY ###

tested with visual studio 2015, MinGW x64 4.0, Ubuntu 15 + gcc 4.8.4, MacOS El Capitan + clang 3.7.1.