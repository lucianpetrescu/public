///---------------------------------------------------------------------------------------------------------------------
/// The MIT License (MIT)
/// 
/// Copyright (c) 2016 Lucian Petrescu, lucian (dot) petrescu (dot) 24 (at) gmail (dot) com
/// 
/// Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation 
/// files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, 
/// modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software 
/// is furnished to do so, subject to the following conditions:
/// 
/// The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
/// 
/// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
/// OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE 
/// LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR 
/// IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
///---------------------------------------------------------------------------------------------------------------------


#include "lap_timer.hpp"
#include <iostream>


int main(int argc, char* argv[]){
	
	lap::Timer timer;

	std::cout << "Using timer with:\nfrequency=" << timer.frequency() << "\nperiod=" << timer.period() << std::endl;

	timer.waitSeconds(0.058);
	std::cout << "Waited 0.58s -> nano  " << timer.elapsedNanoseconds() << std::endl;
	std::cout << "Waited 0.58s -> micro " << timer.elapsedMicroseconds() << std::endl;
	std::cout << "Waited 0.58s -> milli " << timer.elapsedMilliseconds() << std::endl;
	std::cout << "Waited 0.58s -> sec   " << timer.elapsedSeconds() << std::endl;
	std::cout << "Waited 0.58s -> mins  " << timer.elapsedMinutes() << std::endl;
	std::cout << "Waited 0.58s -> hours " << timer.elapsedHours() << std::endl;

	timer.reset();
	timer.waitSeconds(6.12);
	std::cout << "Waited 6.12s -> nano  " << timer.elapsedNanoseconds() << std::endl;
	std::cout << "Waited 6.12s -> micro " << timer.elapsedMicroseconds() << std::endl;
	std::cout << "Waited 6.12s -> milli " << timer.elapsedMilliseconds() << std::endl;
	std::cout << "Waited 6.12s -> sec   " << timer.elapsedSeconds() << std::endl;
	std::cout << "Waited 6.12s -> mins  " << timer.elapsedMinutes() << std::endl;
	std::cout << "Waited 6.12s -> hours " << timer.elapsedHours() << std::endl;


	std::cout << "Reset" << std::endl;
	timer.reset();
	timer.waitNanoseconds(1000000000);
	std::cout << " 10^9 nano " << timer.elapsedSeconds() << " " << timer.elapsedLastCallSeconds() << " " << std::endl;
	timer.waitMicroseconds(1000000);
	std::cout << " 10^6 micro " << timer.elapsedSeconds() << " " << timer.elapsedLastCallSeconds() << " " << std::endl;
	timer.waitMilliseconds(1000);
	std::cout << " 10^3 milli " << timer.elapsedSeconds() << " " << timer.elapsedLastCallSeconds() << " " << std::endl;
	timer.waitSeconds(1);
	std::cout << " 1 seconds " << timer.elapsedSeconds() << " " << timer.elapsedLastCallSeconds() << " " << std::endl;
	timer.waitMinutes(1 / 60.0);
	std::cout << " 1/60 minutes " << timer.elapsedSeconds() << " " << timer.elapsedLastCallSeconds() << " " << std::endl;
	timer.waitHours(1 / 3600.0);
	std::cout << " 1/3600 hours " << timer.elapsedSeconds() << " " << timer.elapsedLastCallSeconds() << " " << std::endl;

	std::cout << "Finished" << std::endl;
	std::cin.get();
	return 0;
}
