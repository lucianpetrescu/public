///---------------------------------------------------------------------------------------------------------------------
/// The MIT License (MIT)
/// 
/// Copyright (c) 2016 Lucian Petrescu, lucian (dot) petrescu (dot) 24 (at) gmail (dot) com
/// 
/// Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation 
/// files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, 
/// modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software 
/// is furnished to do so, subject to the following conditions:
/// 
/// The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
/// 
/// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
/// OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE 
/// LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR 
/// IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
///---------------------------------------------------------------------------------------------------------------------

///------------------------------------------------------------------------------------------------
/// @file
/// @mainpage LAP Timer
/// LAP Timer is a simple timer utility class
///	@version 1.00
///	@par Properties:
///		- c++11
///		
///	@author Lucian Petrescu( lucian (dot) petrescu (dot) 24 (at) gmail (dot) com )
///
///	@par Usage:
///		@code
///		#include "lap_timer.hpp"
///		...
///		lap::Timer timer;
///		timer.waitSeconds(0.58);
///		std::cout << "Waited 0.58s -> milli " << timer.elapsedMilliseconds() << std::endl;
///		std::cout << "Waited 0.58s -> hours " << timer.elapsedHours() << std::endl;
///		for(int i=0;i<100;i++){
///			timer.waitSeconds(0.5);
///			std::cout<<" Since last call "<< timer.elapsedLastCallMicroseconds()<<std::endl;
///		}
///		@endcode
///------------------------------------------------------------------------------------------------

#ifndef LAP_TIMER_H_
#define LAP_TIMER_H_

#include <chrono>
#include <thread>

namespace lap {

	///-----------------------------------------------------------------------------------------
	/// timer class, elapsed always measures from construction or last reset, elapsedLastCall always measures from last "elapsed" call
	class Timer
	{
	public:
		/// creates a timer and starts it
		Timer();
		Timer(const Timer&) = default;
		Timer(Timer&&) = default;
		Timer& operator=(const Timer&) = default;
		Timer& operator=(Timer&&) = default;
		~Timer() = default;

		/// returns the frequency of the clock, in Hz
		/// @return the frequency of the clock, in Hz
		double frequency();
		/// returns the period of the clock, in s
		/// @return the period of the clock, in s
		double period();

		/// resets the timer
		void reset();

		/// caller thread waits for a timed duration
		/// providing a negative duration returns imediately
		/// NOTE: the context switching required to put a thread to sleep (wait) greatly exceeds a nanosecond.
		/// @param duration the waiting duration, in nanoseconds
		void waitNanoseconds(double duration) const;
		/// caller thread waits for a timed duration
		/// providing a negative duration returns imediately
		/// @param duration the waiting duration, in microseconds
		void waitMicroseconds(double duration) const;
		/// caller thread waits for a timed duration
		/// providing a negative duration returns imediately
		/// @param duration the waiting duration, in milliseconds
		void waitMilliseconds(double duration) const;
		/// caller thread waits for a timed duration
		/// providing a negative duration returns imediately
		/// @param duration the waiting duration, in seconds
		void waitSeconds(double duration) const;
		/// caller thread waits for a timed duration
		/// providing a negative duration returns imediately
		/// @param duration the waiting duration, in minutes
		void waitMinutes(double duration) const;
		/// caller thread waits for a timed duration
		/// providing a negative duration returns imediately
		/// @param duration the waiting duration, in hours
		void waitHours(double duration) const;

		/// returns the elapsed time since timer was last reset (or constructed if not reset)
		/// @return the elapsed time since timer was last reset (or constructed if not reset), in nanoseconds
		double elapsedNanoseconds() const;
		/// returns the elapsed time since timer was last reset (or constructed if not reset)
		/// @return the elapsed time since timer was last reset (or constructed if not reset), in microseconds
		double elapsedMicroseconds() const;
		/// returns the elapsed time since timer was last reset (or constructed if not reset)
		/// @return the elapsed time since timer was last reset (or constructed if not reset), in milliseconds
		double elapsedMilliseconds() const;
		/// returns the elapsed time since timer was last reset (or constructed if not reset)
		/// @return the elapsed time since timer was last reset (or constructed if not reset), in seconds
		double elapsedSeconds() const;
		/// returns the elapsed time since timer was last reset (or constructed if not reset)
		/// @return the elapsed time since timer was last reset (or constructed if not reset), in minutes
		double elapsedMinutes() const;
		/// returns the elapsed time since timer was last reset (or constructed if not reset)
		/// @return the elapsed time since timer was last reset (or constructed if not reset), in hours
		double elapsedHours() const;

		/// returns the elapsed time since last elased call (elapsed*).  
		/// @return the elapsed time since last elased call (elapsed*), in nanoseconds
		double elapsedLastCallNanoseconds() const;
		/// returns the elapsed time since last elased call (elapsed*).  
		/// @return the elapsed time since last elased call (elapsed*), in microseconds
		double elapsedLastCallMicroseconds() const;
		/// returns the elapsed time since last elased call (elapsed*).  
		/// @return the elapsed time since last elased call (elapsed*), in milliseconds
		double elapsedLastCallMilliseconds() const;
		/// returns the elapsed time since last elased call (elapsed*).  
		/// @return the elapsed time since last elased call (elapsed*), in seconds
		double elapsedLastCallSeconds() const;
		/// returns the elapsed time since last elased call (elapsed*).  
		/// @return the elapsed time since last elased call (elapsed*), in minutes
		double elapsedLastCallMinutes() const;
		/// returns the elapsed time since last elased call (elapsed*).  
		/// @return the elapsed time since last elased call (elapsed*), in hours
		double elapsedLastCallHours() const;

	private:
		mutable std::chrono::time_point<std::chrono::high_resolution_clock> lastcall;
		std::chrono::time_point<std::chrono::high_resolution_clock> origin;
	};
}

//include guard
#endif