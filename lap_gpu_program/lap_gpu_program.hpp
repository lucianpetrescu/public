﻿///---------------------------------------------------------------------------------------------------------------------
/// The MIT License (MIT)
/// 
/// Copyright (c) 2016 Lucian Petrescu, lucian (dot) petrescu (dot) 24 (at) gmail (dot) com
/// 
/// Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation 
/// files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, 
/// modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software 
/// is furnished to do so, subject to the following conditions:
/// 
/// The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
/// 
/// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
/// OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE 
/// LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR 
/// IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
///---------------------------------------------------------------------------------------------------------------------

///---------------------------------------------------------------------------------------------------------------------
/// @file
/// @mainpage LAP GPUProgram
/// LAP GPUProgram encompasses OpenGL program and shader functionality, enabling quick gpu program creation and usage.
/// It provides api for loading shaders both from memory and from files, accepting any possible origin of sources.
/// LAP GPUProgram does not expose the pipe-based GPU program binding system (explicit Shader Storage Buffer binding, 
/// explicit Uniform Buffer binding, explicit attribute binding, explicit output etc), as these should be controlled 
/// through the layout(location) binding mechanic.
///
/// @version 1.01
/// @par Properties:
///	- c++11, no exceptions
///	- OpenGL 4.5
///	- works on Windows, *nix and MACOS
///	- the OpenGL functions must be provided by the user, see line 136. In this case it uses the lap_wic library, set it to the desired OpenGL 
///   extension libray. This could have been done with extern functions but some OpenGL extension loader libraries are based on macros. Furthermore 
///	  macros are used by many hook-based OpenGL debug libraries, therefore the current system was preferred.
///
/// @par Compiling and linking:
///	- Windows:
///		- no special setup for visual studio projects
///		- -pthread -lgdi32 otherwise
///		- an example visual studio project+solution is provide in the vstudio folder
///	- Linux:
///		- -ldl -lGL -lX11 -lXxf86vm -lXrandr -lXi -lXinerama -lXcursor -lrt -lm
///		- the wayland and mir backends are not used by lapwic (but they can easily be added in the future when they mature, right now they are experimental in glfw)
///		- an example makefile is provided in the root folder
///		- to install the project dependencies get:
///			- sudo apt-get install xorg-dev
///			- sudo apt-get install libgl-dev
///		- also works in a VMWARE virtual machine, but needs a special mesa driver http://mesa3d.org/vmware-guest.html.
///	- OSX
///		- -framework Cocoa -framework OpenGL -framework IOKit -framework CoreVideo -framework CoreFoundation -framework Carbon
///		- an example makefile is provided in the root folder
/// @author Lucian Petrescu( lucian (dot) petrescu (dot) 24 (at) gmail (dot) com )
///
/// @par Usage:
///	- basic usage
///		@code
///		#include "lap_gpu_program.hpp"
///
///		... create OpenGL context ...
///
///		std::string cs = "#version 450\n"
///			"layout(local_size_x = 32) in;\n"
///			"layout(std430, binding = 0) buffer BlockName0 {\n"
///			"	float data[];\n"
///			"};\n"
///			"void main(){\n"
///			"	uint index = gl_GlobalInvocationID.x;\n"
///			"	data[index] = index;\n"
///			"}\n";
///		//create gpu program from memory, logging on std::cout
///		lap::gpu::GPUProgram program(cs, false, &std::cout);
///		//create gpu program from files (vertexfile, fragmentfile), logging on std::cout
///		lap::gpu::GPUProgram program2(vertexfile, true, fragmentfile, true, &std::cout);
///		... 
///		program.bind();
///		... do processing with compute program ...
///		program.unbind();	//= glUseProgram(0)
///		... 
///		program2.bind();
///		... do rendering with vertex + fragment program ...
///		program2.unbind();	//= glUseProgram(0)
///
///		... make sure the GPUProgram objects are destroyed before OpenGL context destruction ...
///		... OpenGL context destruction ...
///		@endcode
///
///	- uniforms, reloading, other
///		@code
///		#include "lap_gpu_program.hpp"
///
///		... create OpenGL context ...
///
///		std::string cs = "#version 450\n"
///			"layout(local_size_x = 32) in;\n"
///			"layout(std430, binding = 0) buffer BlockName0 {\n"
///			"	float data[];\n"
///			"};\n"
///			"uniform uvec3 thevec;\n"
///			"uniform mat2 themat;\n"
///			"void main(){\n"
///			"	uint index = gl_GlobalInvocationID.x;\n"
///			"	data[index] = thevec.x + thevec.y + thevec.z + uint(themat[0][0]);\n"
///			"}\n";
///		lap::gpu::GPUProgram program(cs, false, &std::cout);
///
///		//reload
///		program.reload();
///		// set logger to none
///		program.setLogger(nullptr);
///		// set logger to 
///		program.setLogger(&std::cout);		
///		std::cout << "    ID : " << program.getGLObject() << std::endl;
///		std::cout << "    Linked : " << (program.isLinked() ? "yes" : "no") << std::endl;
///		std::cout << "    Valid : " << (program.isValid() ? "yes" : "no") << std::endl;
///
///		program.setUniform3ui("thevec", 1, 2, 3);
///		float themat[] = { 1.0f,1.0f,1.0f,1.0f};
///		program.setUniformMatrix4fv("themat", 1, false, themat);
///
///		program.setUniform3ui("bogusuniform", 1, 2, 3);			//will issue a warning (sending data to unknown location)
///		program.setUniform3ui("bogusuniform", 1, 2, 3, false);	//will ignore the warning (by default it is on)
///
///		program.bind();
///		... do work here ...
///		program.unbind();	//= glUseProgram(0)
///
///		@endcode
///
///------------------------------------------------------------------------------------------------

#ifndef LAP_GPU_PROGRAM_H_
#define LAP_GPU_PROGRAM_H_

///------------------------------------------------------------------------------------------------
/// IMPORTANT: modify this line to include the desired OpenGL function header
/// NOTE: not using extern functions as some extension loaders use macros
#include "dep/lap_wic.hpp"
///------------------------------------------------------------------------------------------------


#include <string>
#include <vector>
#include <ostream>
#include <map>

namespace lap {
	namespace gpu {
		
		/// GPUProgram
		class GPUProgram {
		public:
			/// shader types
			enum class SHADERTYPE {
				COMPUTE = GL_COMPUTE_SHADER,							///< compute shader type
				GEOMETRY = GL_GEOMETRY_SHADER,							///< geometry shader type
				FRAGMENT = GL_FRAGMENT_SHADER,							///< fragment shader type
				TESS_CONTROL = GL_TESS_CONTROL_SHADER,					///< tessellation control shader type
				TESS_EVALUATION = GL_TESS_EVALUATION_SHADER,			///< tessellation evaluation shader type
				VERTEX = GL_VERTEX_SHADER								///< vertex shader type
			};

			// construct an empty gpu program
			GPUProgram();
			// construct a gpu program from binary data stored in a file.
			// NOTE: the data has to be in driver specific binary format, which can be queried with glGet(GL_PROGRAM_BINARY_FORMATS..), therefore
			// the binary format is machine/driver specific and can't be passed reliably from one machine to another, this is intended only for local use.
			// @param filename		the file from which to read the data
			// @param binaryformat	the OpenGL binary format
			// @param logger		an optional logger
			explicit GPUProgram(std::string filename, GLenum binaryformat, std::ostream* logger = nullptr);
			// construct a gpu program from binary data from memory.
			// NOTE: the data has to be in driver specific binary format, which can be queried with glGet(GL_PROGRAM_BINARY_FORMATS..), therefore
			// the binary format is machine/driver specific and can't be passed reliably from one machine to another, this is intended only for local use.
			// @param binarydata	the binary data
			// @param binaryformat	the OpenGL binary format
			// @param logger		an optional logger
			explicit GPUProgram(const std::vector<char>& binarydata, GLenum binaryformat, std::ostream* logger = nullptr);
			// construct a gpu program from a list of shaders, provided as files or as strings
			// @param src			the shader filenames or strings (depending on asfiles)
			// @param type			the shader types (e.g. GL_COMPUTE_SHADER, SHADERTYPE::FRAGMENT)
			// @param isfile		how to interpret each source (file or string?)
			// @param logger		optional logger
			explicit GPUProgram(const std::vector<std::string>& src, const std::vector<SHADERTYPE>& type, const std::vector<bool>& isfile, std::ostream* logger = nullptr);
			// construct a compute shader gpu program from either a file or a string
			// @param src_cs		the shader filename or string (depending on isfile)
			// @param isfile_cs		is the src_cs a file or a string
			// @param logger		optional logger
			explicit GPUProgram(const std::string& src_cs, bool isfile_cs, std::ostream* logger = nullptr);
			// construct a gpu program (vertex + fragment shader) from either files or strings
			// @param src_vs		the vertex shader filename or string (depending on isfile_vs)
			// @param isfile_vs		is the src_vs a file or a string
			// @param src_fs		the fragment shader filename or string (depending on isfile_fs)
			// @param isfile_fs		is the src_fs a file or a string
			// @param logger		optional logger
			explicit GPUProgram(const std::string& src_vs, bool isfile_vs, const std::string& src_fs, bool isfile_fs, std::ostream* logger = nullptr);
			// construct a gpu program (vertex + geometry + fragment shader) from either files or strings
			// @param src_vs		the vertex shader filename or string (depending on isfile_vs)
			// @param isfile_vs		is the src_vs a file or a string
			// @param src_gs		the geometry shader filename or string (depending on isfile_gs)
			// @param isfile_gs		is the src_gs a file or a string
			// @param src_fs		the fragment shader filename or string (depending on isfile_fs)
			// @param isfile_fs		is the src_fs a file or a string
			// @param logger		optional logger
			explicit GPUProgram(const std::string& src_vs, bool isfile_vs, const std::string& src_gs, bool isfile_gs, const std::string& src_fs, bool isfile_fs, std::ostream* logger = nullptr);
			// construct a gpu program (vertex + tess control + tess evaluation + fragment shader) from either files or strings
			// @param src_vs		the vertex shader filename or string (depending on isfile_vs)
			// @param isfile_vs		is the src_vs a file or a string
			// @param src_tcs		the tess control shader filename or string (depending on isfile_tcs)
			// @param isfile_tcs	is the src_tcs a file or a string
			// @param src_tes		the tess evaluation shader filename or string (depending on isfile_tes)
			// @param isfile_tes	is the src_tes a file or a string
			// @param src_fs		the fragment shader filename or string (depending on isfile_fs)
			// @param isfile_fs		is the src_fs a file or a string
			// @param logger		optional logger
			explicit GPUProgram(const std::string& src_vs, bool asfile_vs, const std::string& src_tcs, bool asfile_tcs, const std::string& src_tes, bool asfile_tes, const std::string& src_fs, bool asfile_fs, std::ostream* logger = nullptr);
			// construct a gpu program (vertex + tess control + tess evaluation + geometry + fragment shader) from either files or strings
			// @param src_vs		the vertex shader filename or string (depending on isfile_vs)
			// @param isfile_vs		is the src_vs a file or a string
			// @param src_tcs		the tess control shader filename or string (depending on isfile_tcs)
			// @param isfile_tcs	is the src_tcs a file or a string
			// @param src_tes		the tess evaluation shader filename or string (depending on isfile_tes)
			// @param isfile_tes	is the src_tes a file or a string
			// @param src_gs		the geometry shader filename or string (depending on isfile_gs)
			// @param isfile_gs		is the src_gs a file or a string
			// @param src_fs		the fragment shader filename or string (depending on isfile_fs)
			// @param isfile_fs		is the src_fs a file or a string
			// @param logger		optional logger
			explicit GPUProgram(const std::string& src_vs, bool isfile_vs, const std::string& src_tcs, bool isfile_tcs, const std::string& src_tes, bool isfile_tes, const std::string& src_gs, bool isfile_gs, const std::string& src_fs, bool isfile_fs, std::ostream* logger = nullptr);
			// forbid explicit GPU object copies, use explicit clone
			GPUProgram(const GPUProgram&) = delete;
			GPUProgram& operator=(const GPUProgram&) = delete;
			/// destructor
			~GPUProgram();
			// allow move semantics
			GPUProgram(GPUProgram&&);
			GPUProgram& operator=(GPUProgram&&);

			// clone the current object. A new GPU object is created and populated, thus this operation is expensive.
			// @return the new GPU program
			GPUProgram clone() const;
			// swap with another image
			// @param other the other image
			void swap(GPUProgram& other);

			// load a gpu program from binary data stored in a file.
			// if a previous program was loaded it is destroyed
			// NOTE: the data has to be in driver specific binary format, which can be queried with glGet(GL_PROGRAM_BINARY_FORMATS..), therefore
			// the binary format is machine/driver specific and can't be passed reliably from one machine to another, this is intended only for local use.
			// @param filename		the file from which to read the data
			// @param binaryformat	the OpenGL binary format
			void load(std::string filename, GLenum binaryformat);
			// load a gpu program from binary data from memory.
			// if a previous program was loaded it is destroyed
			// NOTE: the data has to be in driver specific binary format, which can be queried with glGet(GL_PROGRAM_BINARY_FORMATS..), therefore
			// the binary format is machine/driver specific and can't be passed reliably from one machine to another, this is intended only for local use.
			// @param binarydata	the binary data
			// @param binaryformat	the OpenGL binary format
			void load(const std::vector<char>& binarydata, GLenum binaryformat);
			// load a gpu program from a list of shaders, provided as files or as strings
			// if a previous program was loaded it is destroyed
			// @param src			the shader filenames or strings (depending on asfiles)
			// @param type			the shader types (e.g. GL_COMPUTE_SHADER, SHADERTYPE::FRAGMENT)
			// @param isfile		how to interpret each source (file or string?)
			void load(const std::vector<std::string>& src, const std::vector<SHADERTYPE>& type, const std::vector<bool>& isfile);
			// load a compute shader gpu program from either a file or a string
			// if a previous program was loaded it is destroyed
			// @param src_cs		the shader filename or string (depending on isfile)
			// @param isfile_cs		is the src_cs a file or a string
			void load(const std::string& src_cs, bool isfile_cs);
			// load a gpu program (vertex + fragment shader) from either files or strings
			// if a previous program was loaded it is destroyed
			// @param src_vs		the vertex shader filename or string (depending on isfile_vs)
			// @param isfile_vs		is the src_vs a file or a string
			// @param src_fs		the fragment shader filename or string (depending on isfile_fs)
			// @param isfile_fs		is the src_fs a file or a string
			void load(const std::string& src_vs, bool isfile_vs, const std::string& src_fs, bool isfile_fs);
			// load a gpu program (vertex + geometry + fragment shader) from either files or strings
			// if a previous program was loaded it is destroyed
			// @param src_vs		the vertex shader filename or string (depending on isfile_vs)
			// @param isfile_vs		is the src_vs a file or a string
			// @param src_gs		the geometry shader filename or string (depending on isfile_gs)
			// @param isfile_gs		is the src_gs a file or a string
			// @param src_fs		the fragment shader filename or string (depending on isfile_fs)
			// @param isfile_fs		is the src_fs a file or a string
			void load(const std::string& src_vs, bool isfile_vs, const std::string& src_gs, bool isfile_gs, const std::string& src_fs, bool isfile_fs);
			// load a gpu program (vertex + tess control + tess evaluation + fragment shader) from either files or strings
			// if a previous program was loaded it is destroyed
			// @param src_vs		the vertex shader filename or string (depending on isfile_vs)
			// @param isfile_vs		is the src_vs a file or a string
			// @param src_tcs		the tess control shader filename or string (depending on isfile_tcs)
			// @param isfile_tcs	is the src_tcs a file or a string
			// @param src_tes		the tess evaluation shader filename or string (depending on isfile_tes)
			// @param isfile_tes	is the src_tes a file or a string
			// @param src_fs		the fragment shader filename or string (depending on isfile_fs)
			// @param isfile_fs		is the src_fs a file or a string
			void load(const std::string& src_vs, bool asfile_vs, const std::string& src_tcs, bool asfile_tcs, const std::string& src_tes, bool asfile_tes, const std::string& src_fs, bool asfile_fs);
			// load gpu program (vertex + tess control + tess evaluation + geometry + fragment shader) from either files or strings
			// if a previous program was loaded it is destroyed
			// @param src_vs		the vertex shader filename or string (depending on isfile_vs)
			// @param isfile_vs		is the src_vs a file or a string
			// @param src_tcs		the tess control shader filename or string (depending on isfile_tcs)
			// @param isfile_tcs	is the src_tcs a file or a string
			// @param src_tes		the tess evaluation shader filename or string (depending on isfile_tes)
			// @param isfile_tes	is the src_tes a file or a string
			// @param src_gs		the geometry shader filename or string (depending on isfile_gs)
			// @param isfile_gs		is the src_gs a file or a string
			// @param src_fs		the fragment shader filename or string (depending on isfile_fs)
			// @param isfile_fs		is the src_fs a file or a string
			void load(const std::string& src_vs, bool isfile_vs, const std::string& src_tcs, bool isfile_tcs, const std::string& src_tes, bool isfile_tes, const std::string& src_gs, bool isfile_gs, const std::string& src_fs, bool isfile_fs);
			

			/// reload all the the shaders supplied as files and the recompile and link the entire program
			/// makes sense only for programs created from files
			void reload();

			/// tries to save the program in a binary file. This might not be possible on some platforms.
			/// NOTE: this operation is Machine/driver/platform specific, it is recommended to use this only on the same machine.
			/// @param filename		the filename in which the program will be saved in a binary format
			/// @param binaryformat the binary format in which the data was written. 
			/// @return the success of the operation
			bool saveBinary(const std::string& filename, GLenum& binaryformat);
			/// tries to save the program in a binary form. This might not be possible on some platforms.
			/// NOTE: this operation is Machine/driver/platform specific, it is recommended to use this only on the same machine.
			/// @param binarydata	the binary data
			/// @param binaryformat the binary format in which the data was written. 
			/// @return the success of the operation
			bool saveBinary(std::vector<char>& binarydata, GLenum& binaryformat);

			/// binds the current program, calls glUseProgram(this program)
			void bind();
			/// unbinds the current program, calls glUseProgram(0)
			void unbind();
			
			/// checks to see if the program is valid. A valid OpenGL program is executable in the current OpenGL state.
			/// @return is the program valid for the current OpenGL state?
			bool isValid();
			
			/// returns the link state of this program (true = linked, false = failed to link)
			/// @return the link state
			bool isLinked();

			/// return the Opengl program object
			/// @return the OpenGL program object
			GLuint getGLObject();

			/// sets the current logging stream
			/// @param logger the new logging stream
			void setLogger(std::ostream* logger);
			/// @return the current logging stream
			std::ostream* getLogger();


			/// set a uniform value, uses cached locations
			/// @param name	the in-gpu program name of the uniform variable
			/// @param v0	the value
			/// @param silentwarning if the uniform is not found should the warning be suppressed?
			void setUniform1f(const std::string& name, float v0, bool silentwarning = false);
			/// set a uniform value, uses cached locations
			/// @param name	the in-gpu program name of the uniform variable
			/// @param v0	the 1st value
			/// @param v1	the 2nd value
			/// @param silentwarning if the uniform is not found should the warning be suppressed?
			void setUniform2f(const std::string& name, float v0, float v1, bool silentwarning = false);
			/// set a uniform value, uses cached locations
			/// @param name	the in-gpu program name of the uniform variable
			/// @param v0	the 1st value
			/// @param v1	the 2nd value
			/// @param v2	the 3rd value
			/// @param silentwarning if the uniform is not found should the warning be suppressed?
			void setUniform3f(const std::string& name, float v0, float v1, float v2, bool silentwarning = false);
			/// set a uniform value, uses cached locations
			/// @param name	the in-gpu program name of the uniform variable
			/// @param v0	the 1st value
			/// @param v1	the 2nd value
			/// @param v2	the 3rd value
			/// @param v3	the 4th value
			/// @param silentwarning if the uniform is not found should the warning be suppressed?
			void setUniform4f(const std::string& name, float v0, float v1, float v2, float v3, bool silentwarning = false);
			/// set a uniform value, uses cached locations
			/// @param name	the in-gpu program name of the uniform variable
			/// @param count the number of elements (1 sized) to be set
			/// @param v	memory
			/// @param silentwarning if the uniform is not found should the warning be suppressed?
			void setUniform1fv(const std::string& name, unsigned int count, const float *v, bool silentwarning = false);
			/// set a uniform value, uses cached locations
			/// @param name	the in-gpu program name of the uniform variable
			/// @param count the number of elements (2 sized) to be set
			/// @param v	memory
			/// @param silentwarning if the uniform is not found should the warning be suppressed?
			void setUniform2fv(const std::string& name, unsigned int count, const float *v, bool silentwarning = false);
			/// set a uniform value, uses cached locations
			/// @param name	the in-gpu program name of the uniform variable
			/// @param count the number of elements (3 sized) to be set
			/// @param v	memory
			/// @param silentwarning if the uniform is not found should the warning be suppressed?
			void setUniform3fv(const std::string& name, unsigned int count, const float *v, bool silentwarning = false);
			/// set a uniform value, uses cached locations
			/// @param name	the in-gpu program name of the uniform variable
			/// @param count the number of elements (4 sized) to be set
			/// @param v	memory
			/// @param silentwarning if the uniform is not found should the warning be suppressed?
			void setUniform4fv(const std::string& name, unsigned int count, const float *v, bool silentwarning = false);

			/// set a uniform value, uses cached locations
			/// @param name	the in-gpu program name of the uniform variable
			/// @param v0	the value
			/// @param silentwarning if the uniform is not found should the warning be suppressed?
			void setUniform1d(const std::string& name, double v0, bool silentwarning = false);
			/// set a uniform value, uses cached locations
			/// @param name	the in-gpu program name of the uniform variable
			/// @param v0	the 1st value
			/// @param v1	the 2nd value
			/// @param silentwarning if the uniform is not found should the warning be suppressed?
			void setUniform2d(const std::string& name, double v0, double v1, bool silentwarning = false);
			/// set a uniform value, uses cached locations
			/// @param name	the in-gpu program name of the uniform variable
			/// @param v0	the 1st value
			/// @param v1	the 2nd value
			/// @param v2	the 3rd value
			/// @param silentwarning if the uniform is not found should the warning be suppressed?
			void setUniform3d(const std::string& name, double v0, double v1, double v2, bool silentwarning = false);
			/// set a uniform value, uses cached locations
			/// @param name	the in-gpu program name of the uniform variable
			/// @param v0	the 1st value
			/// @param v1	the 2nd value
			/// @param v2	the 3rd value
			/// @param v3	the 4th value
			/// @param silentwarning if the uniform is not found should the warning be suppressed?
			void setUniform4d(const std::string& name, double v0, double v1, double v2, double v3, bool silentwarning = false);
			/// set a uniform value, uses cached locations
			/// @param name	the in-gpu program name of the uniform variable
			/// @param count the number of elements (1 sized) to be set
			/// @param v	memory
			/// @param silentwarning if the uniform is not found should the warning be suppressed?
			void setUniform1dv(const std::string& name, unsigned int count, const double *v, bool silentwarning = false);
			/// set a uniform value, uses cached locations
			/// @param name	the in-gpu program name of the uniform variable
			/// @param count the number of elements (2 sized) to be set
			/// @param v	memory
			/// @param silentwarning if the uniform is not found should the warning be suppressed?
			void setUniform2dv(const std::string& name, unsigned int count, const double *v, bool silentwarning = false);
			/// set a uniform value, uses cached locations
			/// @param name	the in-gpu program name of the uniform variable
			/// @param count the number of elements (3 sized) to be set
			/// @param v	memory
			/// @param silentwarning if the uniform is not found should the warning be suppressed?
			void setUniform3dv(const std::string& name, unsigned int count, const double *v, bool silentwarning = false);
			/// set a uniform value, uses cached locations
			/// @param name	the in-gpu program name of the uniform variable
			/// @param count the number of elements (4 sized) to be set
			/// @param v	memory
			/// @param silentwarning if the uniform is not found should the warning be suppressed?
			void setUniform4dv(const std::string& name, unsigned int count, const double *v, bool silentwarning = false);

			/// set a uniform value, uses cached locations
			/// @param name	the in-gpu program name of the uniform variable
			/// @param v0	the value
			/// @param silentwarning if the uniform is not found should the warning be suppressed?
			void setUniform1i(const std::string& name, int v0, bool silentwarning = false);
			/// set a uniform value, uses cached locations
			/// @param name	the in-gpu program name of the uniform variable
			/// @param v0	the 1st value
			/// @param v1	the 2nd value
			/// @param silentwarning if the uniform is not found should the warning be suppressed?
			void setUniform2i(const std::string& name, int v0, int v1, bool silentwarning = false);
			/// set a uniform value, uses cached locations
			/// @param name	the in-gpu program name of the uniform variable
			/// @param v0	the 1st value
			/// @param v1	the 2nd value
			/// @param v2	the 3rd value
			/// @param silentwarning if the uniform is not found should the warning be suppressed?
			void setUniform3i(const std::string& name, int v0, int v1, int v2, bool silentwarning = false);
			/// set a uniform value, uses cached locations
			/// @param name	the in-gpu program name of the uniform variable
			/// @param v0	the 1st value
			/// @param v1	the 2nd value
			/// @param v2	the 3rd value
			/// @param v3	the 4th value
			/// @param silentwarning if the uniform is not found should the warning be suppressed?
			void setUniform4i(const std::string& name, int v0, int v1, int v2, int v3, bool silentwarning = false);
			/// set a uniform value, uses cached locations
			/// @param name	the in-gpu program name of the uniform variable
			/// @param count the number of elements (1 sized) to be set
			/// @param v	memory
			/// @param silentwarning if the uniform is not found should the warning be suppressed?
			void setUniform1iv(const std::string& name, unsigned int count, const int *v, bool silentwarning = false);
			/// set a uniform value, uses cached locations
			/// @param name	the in-gpu program name of the uniform variable
			/// @param count the number of elements (2 sized) to be set
			/// @param v	memory
			/// @param silentwarning if the uniform is not found should the warning be suppressed?
			void setUniform2iv(const std::string& name, unsigned int count, const int *v, bool silentwarning = false);
			/// set a uniform value, uses cached locations
			/// @param name	the in-gpu program name of the uniform variable
			/// @param count the number of elements (3 sized) to be set
			/// @param v	memory
			/// @param silentwarning if the uniform is not found should the warning be suppressed?
			void setUniform3iv(const std::string& name, unsigned int count, const int *v, bool silentwarning = false);
			/// set a uniform value, uses cached locations
			/// @param name	the in-gpu program name of the uniform variable
			/// @param count the number of elements (4 sized) to be set
			/// @param v	memory
			/// @param silentwarning if the uniform is not found should the warning be suppressed?
			void setUniform4iv(const std::string& name, unsigned int count, const int *v, bool silentwarning = false);

			/// set a uniform value, uses cached locations
			/// @param name	the in-gpu program name of the uniform variable
			/// @param v0	the value
			/// @param silentwarning if the uniform is not found should the warning be suppressed?
			void setUniform1ui(const std::string& name, unsigned int v0, bool silentwarning = false);
			/// set a uniform value, uses cached locations
			/// @param name	the in-gpu program name of the uniform variable
			/// @param v0	the 1st value
			/// @param v1	the 2nd value
			/// @param silentwarning if the uniform is not found should the warning be suppressed?
			void setUniform2ui(const std::string& name, unsigned int v0, unsigned int v1, bool silentwarning = false);
			/// set a uniform value, uses cached locations
			/// @param name	the in-gpu program name of the uniform variable
			/// @param v0	the 1st value
			/// @param v1	the 2nd value
			/// @param v2	the 3rd value
			/// @param silentwarning if the uniform is not found should the warning be suppressed?
			void setUniform3ui(const std::string& name, unsigned int v0, unsigned int v1, unsigned int v2, bool silentwarning = false);
			/// set a uniform value, uses cached locations
			/// @param name	the in-gpu program name of the uniform variable
			/// @param v0	the 1st value
			/// @param v1	the 2nd value
			/// @param v2	the 3rd value
			/// @param v3	the 4th value
			/// @param silentwarning if the uniform is not found should the warning be suppressed?
			void setUniform4ui(const std::string& name, unsigned int v0, unsigned int v1, unsigned int v2, unsigned int v3, bool silentwarning = false);
			/// set a uniform value, uses cached locations
			/// @param name	the in-gpu program name of the uniform variable
			/// @param count the number of elements (1 sized) to be set
			/// @param v	memory
			/// @param silentwarning if the uniform is not found should the warning be suppressed?
			void setUniform1uiv(const std::string& name, unsigned int count, const unsigned int *v, bool silentwarning = false);
			/// set a uniform value, uses cached locations
			/// @param name	the in-gpu program name of the uniform variable
			/// @param count the number of elements (2 sized) to be set
			/// @param v	memory
			/// @param silentwarning if the uniform is not found should the warning be suppressed?
			void setUniform2uiv(const std::string& name, unsigned int count, const unsigned int *v, bool silentwarning = false);
			/// set a uniform value, uses cached locations
			/// @param name	the in-gpu program name of the uniform variable
			/// @param count the number of elements (3 sized) to be set
			/// @param v	memory
			/// @param silentwarning if the uniform is not found should the warning be suppressed?
			void setUniform3uiv(const std::string& name, unsigned int count, const unsigned int *v, bool silentwarning = false);
			/// set a uniform value, uses cached locations
			/// @param name	the in-gpu program name of the uniform variable
			/// @param count the number of elements (4 sized) to be set
			/// @param v	memory
			/// @param silentwarning if the uniform is not found should the warning be suppressed?
			void setUniform4uiv(const std::string& name, unsigned int count, const unsigned int *v, bool silentwarning = false);


			/// set a uniform matrix value, uses cached locations
			/// @param name		the in-gpu program name of the uniform matrix variable
			/// @param count	the number of matrieces (2x2 sized) to be set
			/// @param transpose are the matrices transpose
			/// @param v		memory
			/// @param silentwarning if the uniform is not found should the warning be suppressed?
			void setUniformMatrix2dv(const std::string& name, unsigned int count, bool transpose, const double* v, bool silentwarning = false);
			/// set a uniform matrix value, uses cached locations
			/// @param name		the in-gpu program name of the uniform matrix variable
			/// @param count	the number of matrieces (2x2 sized) to be set
			/// @param transpose are the matrices transpose
			/// @param v		memory
			/// @param silentwarning if the uniform is not found should the warning be suppressed?
			void setUniformMatrix2fv(const std::string& name, unsigned int count, bool transpose, const float* v, bool silentwarning = false);
			/// set a uniform matrix value, uses cached locations
			/// @param name		the in-gpu program name of the uniform matrix variable
			/// @param count	the number of matrieces (2x3 sized) to be set
			/// @param transpose are the matrices transpose
			/// @param v		memory
			/// @param silentwarning if the uniform is not found should the warning be suppressed?
			void setUniformMatrix2x3dv(const std::string& name, unsigned int count, bool transpose, const double* v, bool silentwarning = false);
			/// set a uniform matrix value, uses cached locations
			/// @param name		the in-gpu program name of the uniform matrix variable
			/// @param count	the number of matrieces (2x3 sized) to be set
			/// @param transpose are the matrices transpose
			/// @param v		memory
			/// @param silentwarning if the uniform is not found should the warning be suppressed?
			void setUniformMatrix2x3fv(const std::string& name, unsigned int count, bool transpose, const float* v, bool silentwarning = false);
			/// set a uniform matrix value, uses cached locations
			/// @param name		the in-gpu program name of the uniform matrix variable
			/// @param count	the number of matrieces (2x4 sized) to be set
			/// @param transpose are the matrices transpose
			/// @param v		memory
			/// @param silentwarning if the uniform is not found should the warning be suppressed?
			void setUniformMatrix2x4dv(const std::string& name, unsigned int count, bool transpose, const double* v, bool silentwarning = false);
			/// set a uniform matrix value, uses cached locations
			/// @param name		the in-gpu program name of the uniform matrix variable
			/// @param count	the number of matrieces (2x4 sized) to be set
			/// @param transpose are the matrices transpose
			/// @param v		memory
			/// @param silentwarning if the uniform is not found should the warning be suppressed?
			void setUniformMatrix2x4fv(const std::string& name, unsigned int count, bool transpose, const float* v, bool silentwarning = false);
			/// set a uniform matrix value, uses cached locations
			/// @param name		the in-gpu program name of the uniform matrix variable
			/// @param count	the number of matrieces (3x3 sized) to be set
			/// @param transpose are the matrices transpose
			/// @param v		memory
			/// @param silentwarning if the uniform is not found should the warning be suppressed?
			void setUniformMatrix3dv(const std::string& name, unsigned int count, bool transpose, const double* v, bool silentwarning = false);
			/// set a uniform matrix value, uses cached locations
			/// @param name		the in-gpu program name of the uniform matrix variable
			/// @param count	the number of matrieces (3x3 sized) to be set
			/// @param transpose are the matrices transpose
			/// @param v		memory
			/// @param silentwarning if the uniform is not found should the warning be suppressed?
			void setUniformMatrix3fv(const std::string& name, unsigned int count, bool transpose, const float* v, bool silentwarning = false);
			/// set a uniform matrix value, uses cached locations
			/// @param name		the in-gpu program name of the uniform matrix variable
			/// @param count	the number of matrieces (3x2 sized) to be set
			/// @param transpose are the matrices transpose
			/// @param v		memory
			/// @param silentwarning if the uniform is not found should the warning be suppressed?
			void setUniformMatrix3x2dv(const std::string& name, unsigned int count, bool transpose, const double* v, bool silentwarning = false);
			/// set a uniform matrix value, uses cached locations
			/// @param name		the in-gpu program name of the uniform matrix variable
			/// @param count	the number of matrieces (3x2 sized) to be set
			/// @param transpose are the matrices transpose
			/// @param v		memory
			/// @param silentwarning if the uniform is not found should the warning be suppressed?
			void setUniformMatrix3x2fv(const std::string& name, unsigned int count, bool transpose, const float* v, bool silentwarning = false);
			/// set a uniform matrix value, uses cached locations
			/// @param name		the in-gpu program name of the uniform matrix variable
			/// @param count	the number of matrieces (3x4 sized) to be set
			/// @param transpose are the matrices transpose
			/// @param v		memory
			/// @param silentwarning if the uniform is not found should the warning be suppressed?
			void setUniformMatrix3x4dv(const std::string& name, unsigned int count, bool transpose, const double* v, bool silentwarning = false);
			/// set a uniform matrix value, uses cached locations
			/// @param name		the in-gpu program name of the uniform matrix variable
			/// @param count	the number of matrieces (3x4 sized) to be set
			/// @param transpose are the matrices transpose
			/// @param v		memory
			/// @param silentwarning if the uniform is not found should the warning be suppressed?
			void setUniformMatrix3x4fv(const std::string& name, unsigned int count, bool transpose, const float* v, bool silentwarning = false);
			/// set a uniform matrix value, uses cached locations
			/// @param name		the in-gpu program name of the uniform matrix variable
			/// @param count	the number of matrieces (4x4 sized) to be set
			/// @param transpose are the matrices transpose
			/// @param v		memory
			/// @param silentwarning if the uniform is not found should the warning be suppressed?
			void setUniformMatrix4dv(const std::string& name, unsigned int count, bool transpose, const double* v, bool silentwarning = false);
			/// set a uniform matrix value, uses cached locations
			/// @param name		the in-gpu program name of the uniform matrix variable
			/// @param count	the number of matrieces (4x4 sized) to be set
			/// @param transpose are the matrices transpose
			/// @param v		memory
			/// @param silentwarning if the uniform is not found should the warning be suppressed?
			void setUniformMatrix4fv(const std::string& name, unsigned int count, bool transpose, const float* v, bool silentwarning = false);
			/// set a uniform matrix value, uses cached locations
			/// @param name		the in-gpu program name of the uniform matrix variable
			/// @param count	the number of matrieces (4x2 sized) to be set
			/// @param transpose are the matrices transpose
			/// @param v		memory
			/// @param silentwarning if the uniform is not found should the warning be suppressed?
			void setUniformMatrix4x2dv(const std::string& name, unsigned int count, bool transpose, const double* v, bool silentwarning = false);
			/// set a uniform matrix value, uses cached locations
			/// @param name		the in-gpu program name of the uniform matrix variable
			/// @param count	the number of matrieces (4x2 sized) to be set
			/// @param transpose are the matrices transpose
			/// @param v		memory
			/// @param silentwarning if the uniform is not found should the warning be suppressed?
			void setUniformMatrix4x2fv(const std::string& name, unsigned int count, bool transpose, const float* v, bool silentwarning = false);
			/// set a uniform matrix value, uses cached locations
			/// @param name		the in-gpu program name of the uniform matrix variable
			/// @param count	the number of matrieces (4x3 sized) to be set
			/// @param transpose are the matrices transpose
			/// @param v		memory
			/// @param silentwarning if the uniform is not found should the warning be suppressed?
			void setUniformMatrix4x3dv(const std::string& name, unsigned int count, bool transpose, const double* v, bool silentwarning = false);
			/// set a uniform matrix value, uses cached locations
			/// @param name		the in-gpu program name of the uniform matrix variable
			/// @param count	the number of matrieces (4x3 sized) to be set
			/// @param transpose are the matrices transpose
			/// @param v		memory
			/// @param silentwarning if the uniform is not found should the warning be suppressed?
			void setUniformMatrix4x3fv(const std::string& name, unsigned int count, bool transpose, const float* v, bool silentwarning = false);

			/// activate a subroutine
			/// NOTE: subroutines must be set after EACH usage/bind of the shader. They are not actual uniforms.
			/// @param name			the name of the subroutine that is being activated
			/// @param shader_type	the type of the shader in which the subroutine resides
			void setSubroutine(const std::string& name, SHADERTYPE shader_type);

		private:
			int internalGetAndSetUniformLocation(const std::string& name, bool silentwarning);
			void internalClean();
			void internalLoad(const std::vector<std::string>& src, const std::vector<SHADERTYPE>& type, const std::vector<bool>& isfile);
			void internalLoad(const std::vector<char>& binarydata, GLenum binaryformat);
			void internalLoad(const std::string& filename, GLenum binaryformat);
			bool internalSaveBinary(std::vector<char>& data, GLenum& binaryformat);


			GLuint glprogram =0;				// gpu program
			std::ostream* logger =nullptr;		// logger
			
			bool islinked=false;				// is the program linked
			std::string log;					// what is the log for the last loaded program

			struct ShaderDesc {
				bool isfile = false;			// is src a file ?
				std::string src;				// filename or shader source
				SHADERTYPE shadertype=SHADERTYPE::COMPUTE;	// type of shader
			};
			std::vector<ShaderDesc> shaderdesc;	// shader descriptors

			GLenum binaryformat = GL_NONE;		//the format of the binary program			
			std::string binaryfilename;			// the name of the file from which the program was loaded, empty means no binary program
			
			std::map<std::string, int> uniform_locations;	//uniform locations map
		};

	}
}


#endif