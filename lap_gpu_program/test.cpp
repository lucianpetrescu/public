///---------------------------------------------------------------------------------------------------------------------
/// The MIT License (MIT)
/// 
/// Copyright (c) 2016 Lucian Petrescu, lucian (dot) petrescu (dot) 24 (at) gmail (dot) com
/// 
/// Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation 
/// files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, 
/// modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software 
/// is furnished to do so, subject to the following conditions:
/// 
/// The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
/// 
/// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
/// OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE 
/// LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR 
/// IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
///---------------------------------------------------------------------------------------------------------------------


#include "dep/lap_wic.hpp"
#include "lap_gpu_program.hpp"
#include <iostream>
#include <cstring>
#include <cassert>
#include <fstream>

//-------------------------------------------------------------------------------------------------
//helper -> creates a vao and a vbo for testing purposes.
class Helper {
public:
	Helper() {
		//vao
		glCreateVertexArrays(1, &vao);
		glBindVertexArray(vao);

		//vbo
		glCreateBuffers(1, &vbo);
		std::vector<float> data = { 0.25f, 0.25f,  0.75f, 0.25f,  0.75f, 0.75f,  0.75f, 0.75f,  0.25f, 0.75f,  0.25f, 0.25f };
		glBindBuffer(GL_ARRAY_BUFFER, vbo);
		glBufferData(GL_ARRAY_BUFFER, sizeof(float) * data.size(), data.data(), GL_STATIC_DRAW);

		//attribs
		glEnableVertexAttribArray(0);
		glVertexAttribPointer(0, 2, GL_FLOAT, GL_FALSE, 2 * sizeof(float), 0);	//read pairs of floats from the vbo (bound to ARRAY_BUFFER) and send them to pipe 0)
	}
	~Helper() {
		glBindBuffer(GL_ARRAY_BUFFER, 0);
		glBindVertexArray(0);
		glDeleteBuffers(1, &vbo);
		glDeleteVertexArrays(1, &vao);
	}
	GLuint vao;
	GLuint vbo;
};
void saveToFile(const std::string& filename, const std::string& contents) {
	std::ofstream file(filename.c_str());
	file.write(contents.c_str(), contents.size());
	file.close();
}


//-------------------------------------------------------------------------------------------------
void testBasics() {
	std::cout << " ------------------------------ Test Basics ---------------------------------------------------------" << std::endl;
	lap::wic::WICSystem wicsystem(nullptr, false);		//no logging, singlethreaded, needed to created windows
	lap::wic::WindowProperties wp; wp.visible = false;
	lap::wic::Window window(wp, lap::wic::FramebufferProperties{}, lap::wic::ContextProperties{}, lap::wic::InputProperties{});	//single window, hidden, needed for OpenGL context
	
	//create a vao and a vbo, take care of state, minimize code for this function
	Helper helper;
	glBindBufferBase(GL_SHADER_STORAGE_BUFFER, 0, helper.vbo);	//use the vbo as a ssbo

	//source
	std::string cs = "#version 450\n"
		"layout(local_size_x = 32) in;\n"
		"layout(std430, binding = 0) buffer BlockName0 {\n"
		"	float data[];\n"
		"};\n"
		"void main(){\n"
		"	uint index = gl_GlobalInvocationID.x;\n"
		"	if(index < 12) data[index] = index;\n"
		"}\n";

	//create program
	lap::gpu::GPUProgram program(cs, false, &std::cout);

	//show some functions
	program.reload();
	program.setLogger(nullptr);			// class logging disabled 
	program.setLogger(&std::cout);		// class logging on std::cout
	program.bind();						// use program
	program.unbind();					// use no program (program 0 for OpenGL)
	std::cout << " Program :" << std::endl;
	std::cout << "         ID : " << program.getGLObject() << std::endl;
	std::cout << "         Linked : " << (program.isLinked() ? "yes" : "no") << std::endl;
	std::cout << "         Valid : " << (program.isValid() ? "yes" : "no") << std::endl;
	
	//run 100 iterations
	program.bind();												//use this program = glUseProgram(OpenGL program id)
	for (int i = 0; i < 100; i++) glDispatchCompute(256, 1, 1);	//256 groups @ 32x1x1
	program.unbind();											//don't use a program = glUseProgram(0)
	
	
	assert(glGetError() == GL_NO_ERROR);
	//window, wicsystem, helper go out of scope
}
void testErrors() {
	std::cout << " ------------------------------ Test (for) Errors ---------------------------------------------------------" << std::endl;
	lap::wic::WICSystem wicsystem(nullptr, false);		//no logging, singlethreaded, needed to created windows
	lap::wic::WindowProperties wp; wp.visible = false;
	lap::wic::Window window(wp, lap::wic::FramebufferProperties{}, lap::wic::ContextProperties{}, lap::wic::InputProperties{});	//single window, hidden, needed for OpenGL context
	std::string cs = "#version 450\n"
		"layout(local_size_x = 32) in;\n"
		"layout(std430, binding = 0) buffer BlockName0 {\n"
		"	float data[];\n"
		"};\n"
		"void main(){\n"
		"	uint index = gl_GlobalInvocationID.x;\n"
		"	if(index < 12) data[index] = index;\n"
		"}\n";
	lap::gpu::GPUProgram program("this does not exist", false, &std::cout);	//error file not found
	program.load(cs + "will not compile nor link", false);					//error doesn't compile and doesn't link
	GLenum badformat = 99999999;
	program.load("this file does not exist.bin", badformat);				//file doesn't exist for binary
	program.load("testbinary.bin", badformat);								//format not supported
	program.saveBinary("will not be written to .bin", badformat);			//program not linked
	program.load(cs, false);
	program.saveBinary("will not be written to .bin", badformat);			//format not supported
	program.setUniform1f("bogus", 1.0f);									//uniform does not exist
	std::cout << " These errors are part of the test, they were expected "<<std::endl;
	assert(glGetError() == GL_NO_ERROR);
}
void testUniforms() {
	std::cout << " ------------------------------ Test Uniforms -------------------------------------------------------" << std::endl;
	lap::wic::WICSystem wicsystem(nullptr, false);		//no logging, singlethreaded, needed to created windows
	lap::wic::WindowProperties wp; wp.visible = false;
	lap::wic::Window window(wp, lap::wic::FramebufferProperties{}, lap::wic::ContextProperties{}, lap::wic::InputProperties{});	//single window, hidden, needed for OpenGL context

	//all possible types, shader, memorie, lots of reloads.
	//source
	std::string cs = "#version 450\n"
		"layout(local_size_x = 32) in;\n"
		"layout(std430, binding = 0) buffer BlockName0 {\n"
		"	double data[];\n"
		"};\n"
		"uniform float f1;"
		"uniform vec2 f2;"
		"uniform vec3 f3;"
		"uniform vec4 f4;"
		"uniform double d1;"
		"uniform dvec2 d2;"
		"uniform dvec3 d3;"
		"uniform dvec4 d4;"
		"uniform int i1;"
		"uniform ivec2 i2;"
		"uniform ivec3 i3;"
		"uniform ivec4 i4;"
		"uniform uint ui1;"
		"uniform uvec2 ui2;"
		"uniform uvec3 ui3;"
		"uniform uvec4 ui4;"
		"uniform mat2 f22;"
		"uniform dmat2 d22;"
		"uniform mat2x3 f23;"
		"uniform dmat2x3 d23;"
		"uniform mat2x4 f24;"
		"uniform dmat2x4 d24;"
		"uniform mat3 f33;"
		"uniform dmat3 d33;"
		"uniform mat3x2 f32;"
		"uniform dmat3x2 d32;"
		"uniform mat3x4 f34;"
		"uniform dmat3x4 d34;"
		"uniform mat4 f44;"
		"uniform dmat4 d44;"
		"uniform mat4x2 f42;"
		"uniform dmat4x2 d42;"
		"uniform mat4x3 f43;"
		"uniform dmat4x3 d43;"
		"void main(){\n"
		"	uint index = gl_GlobalInvocationID.x;\n"
		"	double r = d1*d2.x*d3.x*d4.x*f1*f2.x*f3.x*f4.x*i1*i2.x*i3.x*i4.x*ui1*ui2.x*ui3.x*ui4.x;\n"
		"	r *= d22[0][0]*f22[0][0]*f23[0][0]*d23[0][0]*f24[0][0]*d24[0][0]*f33[0][0]*d33[0][0]*f32[0][0]*d32[0][0]*f34[0][0]*d34[0][0]*f44[0][0]*d44[0][0]*f42[0][0]*d42[0][0]*f43[0][0]*d43[0][0];\n"
		"	if(index < 12) data[index] = r;\n"
		"}\n";
	//create program
	lap::gpu::GPUProgram program(cs, false, &std::cout);
	//test all possibilities .. 
	double d[4] = { 1.0, 2.0, 3.0, 4.0 };
	float f[4] = { 1.0f, 2.0f, 3.0f, 4.0f };
	int i[4] = { 1, 2, -3, -4 };
	unsigned int ui[4] = { 1, 2, 3, 4 };
	float fm[] = { 1.0f,1.0f,1.0f,1.0f,1.0f,1.0f,1.0f,1.0f,1.0f,1.0f,1.0f,1.0f,1.0f,1.0f,1.0f,1.0f };
	double dm[] = { 1.0,1.0,1.0,1.0,1.0,1.0,1.0,1.0,1.0,1.0,1.0,1.0,1.0,1.0,1.0,1.0 };
	program.setUniform1d("d1", d[0]);
	program.setUniform1dv("d1", 1, d);
	program.setUniform2d("d2", d[0], d[1]);
	program.setUniform2dv("d2", 1, d);
	program.setUniform3d("d3", d[0], d[1], d[2]);
	program.setUniform3dv("d3", 1, d);
	program.setUniform4d("d4", d[0], d[1], d[2], d[3]);
	program.setUniform4dv("d4", 1, d);

	program.setUniform1f("f1", f[0]);
	program.setUniform1fv("f1", 1, f);
	program.setUniform2f("f2", f[0], f[1]);
	program.setUniform2fv("f2", 1, f);
	program.setUniform3f("f3", f[0], f[1], f[2]);
	program.setUniform3fv("f3", 1, f);
	program.setUniform4f("f4", f[0], f[1], f[2], f[3]);
	program.setUniform4fv("f4", 1, f);

	program.setUniform1i("i1", i[0]);
	program.setUniform1iv("i1", 1, i);
	program.setUniform2i("i2", i[0], i[1]);
	program.setUniform2iv("i2", 1, i);
	program.setUniform3i("i3", i[0], i[1], i[2]);
	program.setUniform3iv("i3", 1, i);
	program.setUniform4i("i4", i[0], i[1], i[2], i[3]);
	program.setUniform4iv("i4", 1, i);

	program.setUniform1ui("ui1", ui[0]);
	program.setUniform1uiv("ui1", 1, ui);
	program.setUniform2ui("ui2", ui[0], ui[1]);
	program.setUniform2uiv("ui2", 1, ui);
	program.setUniform3ui("ui3", ui[0], ui[1], ui[2]);
	program.setUniform3uiv("ui3", 1, ui);
	program.setUniform4ui("ui4", ui[0], ui[1], ui[2], ui[3]);
	program.setUniform4uiv("ui4", 1, ui);

	program.setUniformMatrix2dv("d22", 1, true, dm);
	program.setUniformMatrix2fv("f22", 1, true, fm);
	program.setUniformMatrix2x3dv("d23", 1, true, dm);
	program.setUniformMatrix2x3fv("f23", 1, true, fm);
	program.setUniformMatrix2x4dv("d24", 1, true, dm);
	program.setUniformMatrix2x4fv("f24", 1, true, fm);
	program.setUniformMatrix3dv("d33", 1, true, dm);
	program.setUniformMatrix3fv("f33", 1, true, fm);
	program.setUniformMatrix3x2dv("d32", 1, true, dm);
	program.setUniformMatrix3x2fv("f32", 1, true, fm);
	program.setUniformMatrix3x4dv("d34", 1, true, dm);
	program.setUniformMatrix3x4fv("f34", 1, true, fm);
	program.setUniformMatrix4dv("d44", 1, true, dm);
	program.setUniformMatrix4fv("f44", 1, true, fm);
	program.setUniformMatrix4x2dv("d42", 1, true, dm);
	program.setUniformMatrix4x2fv("f42", 1, true, fm);
	program.setUniformMatrix4x3dv("d43", 1, true, dm);
	program.setUniformMatrix4x3fv("f43", 1, true, fm);

	assert(glGetError() == GL_NO_ERROR);
}
void testBinary() {
	std::cout << " ------------------------------ Test Binary ---------------------------------------------------------" << std::endl;
	lap::wic::WICSystem wicsystem(nullptr, false);	//no logging, singlethreaded
	lap::wic::Window window(lap::wic::WindowProperties{}, lap::wic::FramebufferProperties{}, lap::wic::ContextProperties{}, lap::wic::InputProperties{});	//single window

	//create a vao and a vbo, take care of state, minimize code for this function
	Helper helper;
	glBindBufferBase(GL_SHADER_STORAGE_BUFFER, 0, helper.vbo);	//use the vbo as a ssbo

	//source
	std::string cs = "#version 450\n"
		"layout(local_size_x = 32) in;\n"
		"layout(std430, binding = 0) buffer BlockName0 {\n"
		"	float data[];\n"
		"};\n"
		"void main(){\n"
		"	uint index = gl_GlobalInvocationID.x;\n"
		"	if(index < 12) data[index] = index;\n"
		"}\n";
	//create program
	lap::gpu::GPUProgram program(cs, false, &std::cout);

	//save program to memory and load it back from memory
	GLenum binaryformat;
	std::vector<char> binarydata;
	bool success = program.saveBinary(binarydata, binaryformat);
	std::cout << "Saved program to memory " << (success ? "successfully" : "unsuccessfully") << " with binary format " << binaryformat << " using "<<binarydata.size()<<" bytes." << std::endl;
	program.load(binarydata, binaryformat);
	std::cout << "Loaded program from memory " << (program.isLinked() ? "successfully" : "unsuccessfully") << std::endl;

	//save program to file and load it back from file
	success = program.saveBinary("testbinary.bin", binaryformat);
	std::cout << "Saved program to testbinary.bin " << (success ? "successfully" : "unsuccessfully") << " with binary format " << binaryformat << std::endl;
	program.load("testbinary.bin", binaryformat);
	std::cout << "Loaded program from testbinary.bin " << (program.isLinked() ? "successfully" : "unsuccessfully") << std::endl;
	assert(glGetError() == GL_NO_ERROR);
}
void testC() {
	std::cout << " ------------------------------ Test Compute Shader -------------------------------------------------" << std::endl;
	lap::wic::WICSystem wicsystem(nullptr, false);		//no logging, singlethreaded, needed to created windows
	lap::wic::WindowProperties wp; wp.visible = false;
	lap::wic::Window window(wp, lap::wic::FramebufferProperties{}, lap::wic::ContextProperties{}, lap::wic::InputProperties{});	//single window, hidden, needed for OpenGL context

	//create a vao and a vbo, take care of state, minimize code for this function
	Helper helper;
	glBindBufferBase(GL_SHADER_STORAGE_BUFFER, 0, helper.vbo);	//use the vbo as a ssbo

	//source and program (test program creation from both source and file)
	std::string cs = "#version 450\n"
		"layout(local_size_x = 32) in;\n"
		"layout(std430, binding = 0) buffer BlockName0 {\n"
		"	float data[];\n"
		"};\n"
		"void main(){\n"
		"	uint index = gl_GlobalInvocationID.x;\n"
		"	if(index < 12) data[index] = index;\n"
		"}\n";
	lap::gpu::GPUProgram program(cs, false, &std::cout); 	//create program from source string
	saveToFile("testc.cs", cs);								//make a glsl file with this source
	program.load("testc.cs", true);							//create program from file

	//run 100 iterations
	program.bind();												//use this program = glUseProgram(OpenGL program id)
	for (int i = 0; i < 100; i++) glDispatchCompute(256, 1, 1);	//256 groups @ 32x1x1
	program.unbind();											//don't use a program = glUseProgram(0)
	
	assert(glGetError() == GL_NO_ERROR);
}
void testVF() {
	std::cout << " ------------------------------ Test Vertex + Fragment Shader ---------------------------------------" << std::endl;
	lap::wic::WICSystem wicsystem(nullptr, false);		//no logging, singlethreaded, needed to created windows
	lap::wic::Window window(lap::wic::WindowProperties{}, lap::wic::FramebufferProperties{}, lap::wic::ContextProperties{}, lap::wic::InputProperties{});	//single window, needed for OpenGL context

	//create a vao and a vbo, take care of state, minimize code for this function
	Helper helper;

	//sources and program (test program creation from both sources and files)
	std::string vs = "#version 450\n"
		"layout(location = 0) in vec2 pos;\n"
		"void main(){\n"
		"	gl_Position = vec4(pos, 0, 1);\n"
		"}\n";
	std::string fs = "#version 450\n"
		"layout(location = 0) out vec3 colorout;\n"
		"void main(){\n"
		"	colorout = vec3(1,0,0);\n"
		"}\n";
	lap::gpu::GPUProgram program(vs, false, fs, false, &std::cout);	//create program from source string
	saveToFile("testvf.vs", vs);									//make glsl files with the sources
	saveToFile("testvf.fs", fs);
	program.load("testvf.vs", true, "testvf.fs", true);				//create program from files

	//draw on screen (quad in ndc)
	bool running = true;
	while (window.isOpened() && running) {
		glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
		program.bind();
		glDrawArrays(GL_TRIANGLES, 0, 6);
		program.unbind();

		window.swapBuffers();
		wicsystem.processProgramEvents();	//process ALL windowing system events
		while (auto* e = window.processEvent()) if (e->type == lap::wic::Event::Type::KeyPress && e->KeyPress.key == lap::wic::Key::ESCAPE) running = false;
		
	}
	assert(glGetError() == GL_NO_ERROR);
	// program goes out of scope
	// helper goes out of scope
	// window and wicsystem go out of scope
}
void testVGF() {
	std::cout << " ------------------------------ Test Vertex + Geoemtry + Fragment Shader ----------------------------" << std::endl;
	lap::wic::WICSystem wicsystem(nullptr, false);		//no logging, singlethreaded, needed to created windows
	lap::wic::Window window(lap::wic::WindowProperties{}, lap::wic::FramebufferProperties{}, lap::wic::ContextProperties{}, lap::wic::InputProperties{});	//single window, needed for OpenGL context

	//create a vao and a vbo, take care of state, minimize code for this function
	Helper helper;

	//sources and program (test program creation from both sources and files)
	std::string vs = "#version 450\n"
		"layout(location = 0) in vec2 pos;\n"
		"void main(){\n"
		"	gl_Position = vec4(pos, 0, 1);\n"
		"}\n";
	std::string gs = "#version 450\n"
		"layout(triangles) in;\n"
		"layout(triangle_strip, max_vertices = 3) out;\n"
		"void main(){\n"
		"	gl_Position = gl_in[0].gl_Position;	EmitVertex();\n"
		"	gl_Position = gl_in[1].gl_Position;	EmitVertex();\n"
		"	gl_Position = gl_in[2].gl_Position;	EmitVertex();\n"
		"}\n";
	std::string fs = "#version 450\n"
		"layout(location = 0) out vec3 colorout;\n"
		"void main(){\n"
		"	colorout = vec3(0,1,0);\n"
		"}\n";
	lap::gpu::GPUProgram program(vs, false, gs, false, fs, false, &std::cout);	//create program from source string
	saveToFile("testvgf.vs", vs);												//make glsl files with the sources
	saveToFile("testvgf.gs", gs);
	saveToFile("testvgf.fs", fs);
	program.load("testvgf.vs", true, "testvgf.gs", true, "testvgf.fs", true);	//create program from files

	//draw on screen (quad in ndc)
	bool running = true;
	while (window.isOpened() && running) {
		glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
		program.bind();
		glDrawArrays(GL_TRIANGLES, 0, 6);
		program.unbind();

		window.swapBuffers();
		wicsystem.processProgramEvents();	//process ALL windowing system events
		while (auto* e = window.processEvent()) if (e->type == lap::wic::Event::Type::KeyPress && e->KeyPress.key == lap::wic::Key::ESCAPE) running = false;

	}
	assert(glGetError() == GL_NO_ERROR);
	// program goes out of scope
	// helper goes out of scope
	// window and wicsystem go out of scope
}
void testVTTF() {
	std::cout << " ------------------------------ Test Vertex + TessControl + TessEval + Fragment Shader --------------" << std::endl;
	lap::wic::WICSystem wicsystem(nullptr, false);		//no logging, singlethreaded, needed to created windows
	lap::wic::Window window(lap::wic::WindowProperties{}, lap::wic::FramebufferProperties{}, lap::wic::ContextProperties{}, lap::wic::InputProperties{});	//single window, needed for OpenGL context

	//create a vao and a vbo, take care of state, minimize code for this function
	Helper helper;
	glPatchParameteri(GL_PATCH_VERTICES, 3);

	//sources and program (test program creation from both sources and files)
	std::string vs = "#version 450\n"
		"layout(location = 0) in vec2 pos;\n"
		"void main(){\n"
		"	gl_Position = vec4(pos, 0, 1);\n"
		"}\n";
	std::string tcs = "#version 450\n"
		"layout(vertices = 3) out;\n"
		"void main(){\n"
		"	if (gl_InvocationID == 0) {\n"
		"		gl_TessLevelInner[0] = 4;\n"
		"		gl_TessLevelOuter[0] = 3;\n"
		"		gl_TessLevelOuter[1] = 2;\n"
		"		gl_TessLevelOuter[2] = 2;\n"
		"	}\n"
		"	gl_out[gl_InvocationID].gl_Position = gl_in[gl_InvocationID].gl_Position;\n"
		"}\n";
	std::string tes = "#version 450\n"
		"layout(triangles, equal_spacing, ccw) in;\n"
		"void main(){\n"
		"	gl_Position = gl_TessCoord.x * gl_in[0].gl_Position + gl_TessCoord.y * gl_in[1].gl_Position + gl_TessCoord.z * gl_in[2].gl_Position;\n"
		"}\n";
	std::string fs = "#version 450\n"
		"layout(location = 0) out vec3 colorout;\n"
		"void main(){\n"
		"	colorout = vec3(0,0,1);\n"
		"}\n";
	lap::gpu::GPUProgram program(vs, false, tcs, false, tes, false, fs, false, &std::cout);				//create program from source string
	saveToFile("testvttf.vs", vs);																		//make glsl files with the sources
	saveToFile("testvttf.tcs", tcs);
	saveToFile("testvttf.tes", tes);
	saveToFile("testvttf.fs", fs);
	program.load("testvttf.vs", true, "testvttf.tcs", true, "testvttf.tes", true, "testvttf.fs", true);	//create program from files

	//draw on screen (quad in ndc)
	glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);
	bool running = true;
	while (window.isOpened() && running) {
		glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
		program.bind();
		glDrawArrays(GL_PATCHES, 0, 6);
		program.unbind();

		window.swapBuffers();
		wicsystem.processProgramEvents();	//process ALL windowing system events
		while (auto* e = window.processEvent()) if (e->type == lap::wic::Event::Type::KeyPress && e->KeyPress.key == lap::wic::Key::ESCAPE) running = false;

	}
	assert(glGetError() == GL_NO_ERROR);
	// program goes out of scope
	// helper goes out of scope
	// window and wicsystem go out of scope
}
void testVTTGF() {
	std::cout << " ------------------------------ Test Vertex + TessControl + TessEval + Geometry + Fragment Shader ---" << std::endl;
	lap::wic::WICSystem wicsystem(nullptr, false);					//no logging, singlethreaded, needed to created windows
	lap::wic::Window window(lap::wic::WindowProperties{}, lap::wic::FramebufferProperties{}, lap::wic::ContextProperties{}, lap::wic::InputProperties{});	//single window, needed for OpenGL context

	//create a vao and a vbo, take care of state, minimize code for this function
	Helper helper;
	glPatchParameteri(GL_PATCH_VERTICES, 3);

	//sources and program (test program creation from both sources and files)
	std::string vs = "#version 450\n"
		"layout(location = 0) in vec2 pos;\n"
		"void main(){\n"
		"	gl_Position = vec4(pos, 0, 1);\n"
		"}\n";
	std::string tcs = "#version 450\n"
		"layout(vertices = 3) out;\n"
		"void main(){\n"
		"	if (gl_InvocationID == 0) {\n"
		"		gl_TessLevelInner[0] = 4;\n"
		"		gl_TessLevelOuter[0] = 3;\n"
		"		gl_TessLevelOuter[1] = 2;\n"
		"		gl_TessLevelOuter[2] = 2;\n"
		"	}\n"
		"	gl_out[gl_InvocationID].gl_Position = gl_in[gl_InvocationID].gl_Position;\n"
		"}\n";
	std::string tes = "#version 450\n"
		"layout(triangles, equal_spacing, ccw) in;\n"
		"void main(){\n"
		"	gl_Position = gl_TessCoord.x * gl_in[0].gl_Position + gl_TessCoord.y * gl_in[1].gl_Position + gl_TessCoord.z * gl_in[2].gl_Position;\n"
		"}\n";
	std::string gs = "#version 450\n"
		"layout(triangles) in;\n"
		"layout(triangle_strip, max_vertices = 3) out;\n"
		"void main(){\n"
		"	gl_Position = gl_in[0].gl_Position;	EmitVertex();\n"
		"	gl_Position = gl_in[1].gl_Position;	EmitVertex();\n"
		"	gl_Position = gl_in[2].gl_Position;	EmitVertex();\n"
		"}\n";
	std::string fs = "#version 450\n"
		"layout(location = 0) out vec3 colorout;\n"
		"void main(){\n"
		"	colorout = vec3(1,1,1);\n"
		"}\n";
	lap::gpu::GPUProgram program(vs, false, tcs, false, tes, false, gs, false, fs, false, &std::cout);	//create program from source string
	saveToFile("testvttgf.vs", vs);																		//make glsl files with the sources
	saveToFile("testvttgf.tcs", tcs);
	saveToFile("testvttgf.tes", tes);
	saveToFile("testvttgf.gs", gs);
	saveToFile("testvttgf.fs", fs);
	program.load("testvttgf.vs", true, "testvttgf.tcs", true, "testvttgf.tes", true, "testvttgf.gs", true, "testvttgf.fs", true);	//create program from files


	auto pp = std::move(program);
	auto ppp(std::move(pp));
	program = std::move(ppp);
	auto pppp = program.clone();
	pp = std::move(pppp);
	program = std::move(pp);


	//draw on screen (quad in ndc)
	glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);
	bool running = true;
	while (window.isOpened() && running) {
		glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
		program.bind();
		glDrawArrays(GL_PATCHES, 0, 6);
		program.unbind();

		window.swapBuffers();
		wicsystem.processProgramEvents();	//process ALL windowing system events
		while (auto* e = window.processEvent()) if (e->type == lap::wic::Event::Type::KeyPress && e->KeyPress.key == lap::wic::Key::ESCAPE) running = false;

	}
	assert(glGetError() == GL_NO_ERROR);
	// program goes out of scope
	// helper goes out of scope
	// window and wicsystem go out of scope
}

//-------------------------------------------------------------------------------------------------
int main(int argc, char* argv[]){

	//basic introduction test
	testBasics();
	//simple example tests
	testC();
	testVF();
	testVGF();
	testVTTF();
	testVTTGF();
	testBinary();
	testUniforms();
	//error tests
	testErrors();
	

	std::cout << "Finished" << std::endl;
	std::cin.get();
	return 0;
}
