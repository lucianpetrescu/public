# LAP GPU Program #

LAP GPUProgram encompasses OpenGL program and shader functionality, enabling quick gpu program creation and usage. It provides api for loading shaders both from memory and from files, accepting any possible origin of sources. LAP GPUProgram does not expose the pipe-based GPU program binding system (explicit Shader Storage Buffer binding, explicit Uniform Buffer binding, explicit attribute binding, explicit output etc), as these should be controlled through the layout(location) binding mechanic.

version 1.01


### SCREENSHOTS ###

Usage example:

 ![functions are bundled into groups](img/example.png) 

###PROPERTIES###
- c++11, no exceptions
- OpenGL 4.5
- works on Windows, *nix and MACOS
- the OpenGL functions must be provided by the user, see line 136. In this case it uses the lap_wic library, set it to the desired OpenGL extension libray. This could have been done with extern functions but some OpenGL extension loader libraries are based on macros. Furthermore macros are used by many hook-based OpenGL debug libraries, therefore the current system was preferred.

###COMPILING AND LINKING###

Visual studio 2015 project&solution files can be found in the **vstudio** folder. Makefiles for gnu/gcc ( **makefile** ) and for visual studio ( **makefilevs.bat** ) are provided in the root folder. All methods will generate objs, pdbs, exes, etc in **bin**.

- Windows:
	- no special setup for visual studio projects
	- -pthread -lgdi32 otherwise
	- an example visual studio project+solution is provide in the vstudio folder
	- the makefilevs.bat from the root folder can also be used
- Linux:
	- -ldl -lGL -lX11 -lXxf86vm -lXrandr -lXi -lXinerama -lXcursor -lrt -lm
	- the wayland and mir backends are not used by lapwic (but they can easily be added in the future when they mature, right now they are experimental in glfw)
	- an example makefile is provided in the root folder
	- to install the project dependencies get:
		- sudo apt-get install xorg-dev
		- sudo apt-get install libgl-dev
	- also compiles in a VMWARE virtual machine, but needs a special mesa driver http://mesa3d.org/vmware-guest.html. It will crash as the mesa driver does not support OpenGL 4.5.
- OSX
	- -framework Cocoa -framework OpenGL -framework IOKit -framework CoreVideo -framework CoreFoundation -framework Carbon
	- an example makefile is provided in the root folder\n

### EXAMPLE - BASIC USAGE###

		#include "lap_gpu_program.hpp"

		... create OpenGL context ...

		std::string cs = "#version 450\n"
			"layout(local_size_x = 32) in;\n"
			"layout(std430, binding = 0) buffer BlockName0 {\n"
			"	float data[];\n"
			"};\n"
			"void main(){\n"
			"	uint index = gl_GlobalInvocationID.x;\n"
			"	data[index] = index;\n"
			"}\n";
		//create gpu program from memory, logging on std::cout
		lap::gpu::GPUProgram program(cs, false, &std::cout);
		//create gpu program from files (vertexfile, fragmentfile), logging on std::cout
		lap::gpu::GPUProgram program2(vertexfile, true, fragmentfile, true, &std::cout);
		... 
		program.bind();
		... do processing with compute program ...
		program.unbind();	//= glUseProgram(0)
		... 
		program2.bind();
		... do rendering with vertex + fragment program ...
		program2.unbind();	//= glUseProgram(0)

		... make sure the GPUProgram objects are destroyed before OpenGL context destruction ...
		... OpenGL context destruction ...

### EXAMPLE - UNIFORMS, RELOADING, OTHER###

		#include "lap_gpu_program.hpp"

		... create OpenGL context ...

		std::string cs = "#version 450\n"
			"layout(local_size_x = 32) in;\n"
			"layout(std430, binding = 0) buffer BlockName0 {\n"
			"	float data[];\n"
			"};\n"
			"uniform uvec3 thevec;\n"
			"uniform mat2 themat;\n"
			"void main(){\n"
			"	uint index = gl_GlobalInvocationID.x;\n"
			"	data[index] = thevec.x + thevec.y + thevec.z + uint(themat[0][0]);\n"
			"}\n";
		lap::gpu::GPUProgram program(cs, false, &std::cout);

		//reload
		program.reload();
		// set logger to none
		program.setLogger(nullptr);
		// set logger to 
		program.setLogger(&std::cout);		
		std::cout << "    ID : " << program.getGLObject() << std::endl;
		std::cout << "    Linked : " << (program.isLinked() ? "yes" : "no") << std::endl;
		std::cout << "    Valid : " << (program.isValid() ? "yes" : "no") << std::endl;

		program.setUniform3ui("thevec", 1, 2, 3);
		float themat[] = { 1.0f,1.0f,1.0f,1.0f};
		program.setUniformMatrix4fv("themat", 1, false, themat);

		program.setUniform3ui("bogusuniform", 1, 2, 3);			//will issue a warning (sending data to unknown location)
		program.setUniform3ui("bogusuniform", 1, 2, 3, false);	//will ignore the warning (by default it is on)

		program.bind();
		... do work here ...
		program.unbind();	//= glUseProgram(0)

### DOCUMENTATION ###

a Doxyfile is provided in **doc**, which can be used with Doxygen to generate  the entire documentation in HTML format. 
Note: doxygen has problems with arguments of std::function type function arguments (used for callbacks) and will issue a large number of warnings.


### COMPATIBILITY ###

tested with visual studio 2015, MinGW x64 4.0, Ubuntu 15 + gcc 4.8.4, MacOS El Capitan + clang 3.7.1.

###TODO LIST###

more testing on osx.