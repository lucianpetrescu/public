///---------------------------------------------------------------------------------------------------------------------
/// The MIT License (MIT)
///
/// Copyright (c) 2016 Lucian Petrescu, lucian (dot) petrescu (dot) 24 (at) gmail (dot) com
/// 
/// Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation 
/// files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, 
/// modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software 
/// is furnished to do so, subject to the following conditions:
/// 
/// The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
/// 
/// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
/// OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE 
/// LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR 
/// IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
///---------------------------------------------------------------------------------------------------------------------

#include "lap_wic.hpp"
#include <iostream>
#include <chrono>
#include <cstring>

//-------------------------------------------------------------------------------------------------
void testNoWindow() {
	std::cout << "------------------------------------------------------------\nTest No Window" << std::endl;
	using namespace lap::wic;

	//create a WIC system, without logging and without thread safety (running in singlethreaded mode)
	WICSystem wicsystem(&std::cout, false);

	std::cout << "Using " << wicsystem.getWindowsOpened() << " windows" << std::endl;
	std::cout << "Using " << wicsystem.getMonitors().size() << " monitors" << std::endl;
	std::cout << "Using " << wicsystem.getMonitorPrimary()->name << " as primary monitor" << std::endl;
#ifdef LAP_WIC_INCLUDE_VULKAN
	std::cout << "Has vulkan support? "<<std::boolalpha << wicsystem.vulkanIsSupported() << std::endl;
#endif
	//window and OpenGL context destroyed by going out of scope
	//system destroyed by going out of scope
}

//-------------------------------------------------------------------------------------------------
void testBasicEvents() {
	std::cout << "------------------------------------------------------------\nTest Basic Events" << std::endl;
	using namespace lap::wic;

	//create a WIC system, without logging and without thread safety (running in singlethreaded mode)
	WICSystem wicsystem(nullptr, false);
	//set the WICSystem to log on std::cout
	wicsystem.setLogger(&std::cout);

	//creates a window with default properties
	WindowProperties wp; FramebufferProperties fp; ContextProperties cp; InputProperties ip;
	Window window = Window(wp, fp, cp, ip);
	std::cout << " Created window with unique id = " << window.getId() << std::endl;
	// the window OpenGL context is now binded on this thread

	//this works exactly like opengl, using adaptive vsync here
	window.setSwapInterval(-1);

	//as long as the window is opened and running
	while (window.isOpened() ) {

		//DO WORK HERE

		//swap buffers
		window.swapBuffers();

		//process events
		while (const Event* e = window.processEvent()) {
			// if ALT+F4 or the close decorator button lead to window close signal the window will be flagged as closed (the close event is not treated here to show this behavior) 
			if (e->KeyPress.key == Key::ESCAPE) window.close();
			if (e->type == Event::Type::MouseMove) std::cout << "Mouse move at " << e->MouseMove.position_x << " " << e->MouseMove.position_y <<", press the ESC/OS key shortuct (e.g. ALT+F4)/close button to destroy window" << std::endl;
		}

		//process all application events (has to be constantly called from the MAIN thread)
		wicsystem.processProgramEvents();
	};

	//window and OpenGL context destroyed by going out of scope
	//system destroyed by going out of scope
}


//-------------------------------------------------------------------------------------------------
void testBasicFullscreen() {
	std::cout << "------------------------------------------------------------\nTest Basic Fullscreen" << std::endl;
	using namespace lap::wic;
	
	//create a WIC system, logging on std::cout and without thread safety (running in singlethreaded mode)
	WICSystem wicsystem(&std::cout, false);

	//creates a window with default properties
	WindowProperties wp; FramebufferProperties fp; ContextProperties cp; InputProperties ip;
	Window window = Window(wp, fp, cp, ip);
	// the window OpenGL context is now binded on this thread

	//as long as the window is opened
	while (window.isOpened()) {

		//DO WORK HERE

		//swap buffers
		window.swapBuffers();

		//process events
		while (const Event* e = window.processEvent()) {
			if (e->type == Event::Type::KeyPress) {
				if (e->KeyPress.key == Key::ESCAPE) window.close();
				if (e->KeyPress.key == Key::F1) window.setFullscreen();
				if (e->KeyPress.key == Key::F2) window.setWindowed(400,400,100,100);
			}
		}

		//process all application events (has to be constantly called from the MAIN thread)
		wicsystem.processProgramEvents();
	};

	//window and OpenGL context destroyed by going out of scope
	//system destroyed by going out of scope
}



//-------------------------------------------------------------------------------------------------
void testBasicCallbacksKeyPress(lap::wic::Window& caller, lap::wic::Key key, bool alt, bool control, bool shift, bool system, const lap::wic::InputState& state, uint64_t timestamp) {
	std::cout << "[c-style/free function callback] Key Pressed : key = " << (int)key << " alt=" << alt << " control=" << control << " shift=" << shift << " system=" << system << std::endl;
}
class TestBasicCallbacksMouseMoveClass{
public:
	void mouseMove(lap::wic::Window& caller, unsigned int posx, unsigned int posy, const lap::wic::InputState&, uint64_t timestamp) {
		std::cout << "[Member Function/ Delegate Callback] Mouse move at " << posx << " " << posy << std::endl;
	}
};
struct TestBasicCallbacksMouseDragFunctor {
	void operator()(lap::wic::Window& caller, lap::wic::MouseButton button, unsigned int posx, unsigned int posy, bool alt, bool control, bool shift, bool system, const lap::wic::InputState&, uint64_t timestamp) {
		std::cout << "[Functor Callback] MouseDrag : button = " << (int)button << " posx=" << posx << " posy=" << posy << " alt=" << alt << " control=" << control << " shift=" << shift << " system=" << system << std::endl;
	}
};
void testBasicCallbacks() {
	std::cout << "------------------------------------------------------------\nTest Basic Callbacks" << std::endl;
	using namespace lap::wic;

	//create a WIC system, logging on std::cout and without thread safety (running in singlethreaded mode)
	WICSystem wicsystem(&std::cout, false);

	//creates a window with default properties
	WindowProperties wp; FramebufferProperties fp; ContextProperties cp; InputProperties ip;
	Window window = Window(wp, fp, cp, ip);
	// the window OpenGL context is now binded on this thread

	//set a callback to a global/free function (c-style)
	window.setCallbackKeyPress(testBasicCallbacksKeyPress);

	//set a callback to a member function (delegate)
	TestBasicCallbacksMouseMoveClass mousemoveclass;
	window.setCallbackMouseMove(std::bind(&TestBasicCallbacksMouseMoveClass::mouseMove, &mousemoveclass, std::placeholders::_1, std::placeholders::_2, std::placeholders::_3, std::placeholders::_4, std::placeholders::_5));

	//set a callback to a functor
	window.setCallbackMouseDrag(TestBasicCallbacksMouseDragFunctor());

	//set a callback to a lambda
	window.setCallbackMouseScroll([](Window& caller, float scrollx, float scrolly, unsigned int posx, unsigned int posy, bool alt, bool control, bool shift, bool system, const InputState&, uint64_t timestamp) {
		std::cout << "[Lambda Callback] Window " << caller.getWindowProperties().title << " Mouse scroll "<<scrolly << std::endl; 
	});
	
	//as long as the window is opened
	while (window.isOpened()) {

		//DO WORK HERE

		//swap buffers
		window.swapBuffers();

		//process events with callbacks
		window.processEvents();

		//process all application events (has to be constantly called from the MAIN thread)
		wicsystem.processProgramEvents();
	};

	//window destroyed by going out of scope
	//system destroyed by going out of scope
}



//-------------------------------------------------------------------------------------------------
void testRemapper() {
	std::cout << "------------------------------------------------------------\nTest Input Remapper" << std::endl;
	using namespace lap::wic;

	//create a WIC system, logging on std::cout and without thread safety (running in singlethreaded mode)
	WICSystem wicsystem(&std::cout, false);

	//creates a window with default properties
	WindowProperties wp; FramebufferProperties fp; ContextProperties cp; InputProperties ip;
	Window window = Window(wp, fp, cp, ip);
	// the window OpenGL context is now binded on this thread
	
	//create remapper
	InputRemapper remapper;
	remapper.add(Key::A, Key::B);											// map A->B
	remapper.add(MouseButton::LEFT, MouseButton::RIGHT);					// map LMB->RMB
	remapper.add(ControllerButton::BUTTON_1, ControllerButton::BUTTON_10);	// map Button1 -> Button10
	remapper.swap(Key::D, Key::E);											// swap D and E
	remapper.swap(MouseButton::EXTRA4, MouseButton::EXTRA5);				// swap EXTRA4 and EXTRA5
	remapper.swap(ControllerButton::BUTTON_3, ControllerButton::BUTTON_4);	// swap button 3 and button 4
	
	// attach remapper to window
	window.setInputRemapper(remapper);
	//const InputRemapper remapper2 = window.getInputRemapper();
	
	//set a callbacks to remappables
	window.setCallbackKeyPress([](Window& wnd, Key key, bool alt, bool control, bool shift, bool system, const InputState& state, uint64_t timestamp) {
		std::cout << "[" << timestamp << "ms] Window " << wnd.getWindowProperties().title << " Key Pressed : key = " << (int)key << " alt=" << alt << " control=" << control << " shift=" << shift << " system=" << system << std::endl;
	});
	window.setCallbackKeyRelease([](Window& wnd, Key key, bool alt, bool control, bool shift, bool system, const InputState& state, uint64_t timestamp) {
		std::cout << "[" << timestamp << "ms] Window " << wnd.getWindowProperties().title << " Key Released : key = " << (int)key << " alt=" << alt << " control=" << control << " shift=" << shift << " system=" << system << std::endl;
	});
	window.setCallbackKeyRepeat([](Window& wnd, Key key, bool alt, bool control, bool shift, bool system, const InputState& state, uint64_t timestamp) {
		std::cout << "[" << timestamp << "ms] Window " << wnd.getWindowProperties().title << " Key Repeat : key = " << (int)key << " alt=" << alt << " control=" << control << " shift=" << shift << " system=" << system << std::endl;
	});
	window.setCallbackMousePress([](Window& wnd, MouseButton button, unsigned int posx, unsigned int posy, bool alt, bool control, bool shift, bool system, const InputState& state, uint64_t timestamp) {
		std::cout << "[" << timestamp << "ms] Window " << wnd.getWindowProperties().title << " MousePress : button = " << (int)button << " posx=" << posx << " posy=" << posy << " alt=" << alt << " control=" << control << " shift=" << shift << " system=" << system << std::endl;
	});
	window.setCallbackMouseRelease([](Window& wnd, MouseButton button, unsigned int posx, unsigned int posy, bool alt, bool control, bool shift, bool system, const InputState& state, uint64_t timestamp) {
		std::cout << "[" << timestamp << "ms] Window " << wnd.getWindowProperties().title << " MouseRelease : button = " << (int)button << " posx=" << posx << " posy=" << posy << " alt=" << alt << " control=" << control << " shift=" << shift << " system=" << system << std::endl;
	});
	window.setCallbackMouseDrag([](Window& wnd, MouseButton button, unsigned int posx, unsigned int posy, bool alt, bool control, bool shift, bool system, const InputState& state, uint64_t timestamp) {
		std::cout << "[" << timestamp << "ms] Window " << wnd.getWindowProperties().title << " MouseDrag : button = " << (int)button << " posx=" << posx << " posy=" << posy << " alt=" << alt << " control=" << control << " shift=" << shift << " system=" << system << std::endl;
	});
	window.setCallbackControllerButtonPress([](unsigned int id, ControllerButton button, uint64_t timestamp) {
		std::cout << "[" << timestamp << "ms] Controller : " << id << " button press " << (int)button << std::endl;
	});
	window.setCallbackControllerButtonRelease([](unsigned int id, ControllerButton button, uint64_t timestamp) {
		std::cout << "[" << timestamp << "ms] Controller : " << id << " button release " << (int)button << std::endl;
	});
	window.setCallbackControllerAxisMove([](unsigned int id, float(&axes)[6], uint64_t timestamp) {
		std::cout << "[" << timestamp << "ms] Controller : " << id << " axis move 0:" << axes[0] << " 1:" << axes[1] << " 2:" << axes[2] << " 3:" << axes[3] << " 4:" << axes[4] << " 5:" << axes[5] << std::endl;
	});
	
	//as long as the window is opened
	while (window.isOpened()) {

		//JUST PRESS keys/mouse buttons/controller buttons

		//swap buffers
		window.swapBuffers();

		//process events with callbacks
		window.processEvents();

		//process all application events (has to be constantly called from the MAIN thread)
		wicsystem.processProgramEvents();
	};

	//window destroyed by going out of scope
	//system destroyed by going out of scope
}


//-------------------------------------------------------------------------------------------------
void testBasicMultipleWindows() {
	std::cout << "------------------------------------------------------------\nTest Multiple Windows" << std::endl;
	using namespace lap::wic;

	//create a WIC system, logging on std::cout and without thread safety (running in singlethreaded mode)
	WICSystem wicsystem(&std::cout, false);

	WindowProperties wp1, wp2;
	wp1.position_x = 100; wp1.position_y = 100;	wp1.width = 500; wp1.height = 400; wp1.title = "1st";
	wp2.position_x = 900; wp2.position_y = 100;	wp2.width = 500; wp2.height = 400; wp2.title = "2nd";
	FramebufferProperties fp; ContextProperties cp; InputProperties ip;
	Window window1 = Window(wp1, fp, cp, ip);
	// the window1 OpenGL context is now binded on this thread
	Window window2 = Window(wp2, fp, cp, ip);
	// the window2 OpenGL context is now binded on this thread
	auto mousemove = [](Window& wnd, unsigned int, unsigned int, const InputState&, uint64_t) {std::cout << "Window " << wnd.getWindowProperties().title << " mouse move" << std::endl; };
	window1.setCallbackMouseMove(mousemove);
	auto keypress = [](Window& wnd, Key key, bool alt, bool control, bool shift, bool system, const InputState& state, uint64_t timestamp) { if (key == Key::ESCAPE) wnd.close(); };
	window1.setCallbackKeyPress(keypress);

	//as long as any window is opened
	while (wicsystem.isAnyWindowOpened()) {
		if (window1.isOpened()) {
			//bind the context of window1 on this thread (only necessary when using more windows or using a window in multiple threads)
			window1.bindContext();

			//DO WORK HERE

			//swap buffers
			window1.swapBuffers();

			//process events with callbacks
			window1.processEvents();
		}
		if (window2.isOpened()) {
			//bind the context of window2 on this thread (only necessary when using more windows or using a window in multiple threads)
			window2.bindContext();

			//DO WORK HERE

			//swap buffers
			window2.swapBuffers();

			//process events with callbacks
			while (const Event* e = window2.processEvent()) {
				if (e->type == Event::Type::KeyPress) if (e->KeyPress.key == Key::ESCAPE) window2.close();
			}
		}
		
		//process all application events (has to be constantly called from the MAIN thread)
		wicsystem.processProgramEvents();
	};

	//window destroyed by going out of scope
	//system destroyed by going out of scope
}

//-------------------------------------------------------------------------------------------------
void testBasicOpenGL() {
#ifdef LAP_WIC_SUPPRESS_GL_EXTENSIONS
	std::cout << "------------------------------------------------------------\nTest Basic OpenGL : SKIPPED , OpenGL function loading disabled" << std::endl;
#else
	std::cout << "------------------------------------------------------------\nTest Basic OpenGL" << std::endl;
	using namespace lap::wic;

	//create a WIC system, logging on std::cout and without thread safety (running in singlethreaded mode)
	WICSystem wicsystem(&std::cout, false);

	//creates a window with default properties and a modern opengl context
	ContextProperties cp;
	cp.api = ContextProperties::ApiType::OPENGL;	//using OpenGL
	cp.debug_context = true;
	cp.profile_core = true;
	cp.release_behavior_flush = true;				//flush pipeline on context release?
	cp.robustness = ContextProperties::RobustnessType::LOSE_CONTEXT_ON_RESET;	//lose context on reset
	cp.swap_interval = -1;		//adaptive vsync
	cp.version_major = 4;		
	cp.version_minor = 5;
	WindowProperties wp; FramebufferProperties fp; InputProperties ip;
	Window window = Window(wp, fp, cp, ip);
	// the window OpenGL context is now binded on this thread 
	// extensions are also loaded, based on requested version, e.g. glDispatchCompute isn't loaded for OpenGL 4.1

	//a big quad in screen space
	std::vector<float> vertices = { -0.5,-0.5, -0.5, 0.5, 0.5, 0.5,  0.5, 0.5,  0.5, -0.5, -0.5, -0.5 };
	GLuint vbo, vao;
	glGenVertexArrays(1, &vao);
	glBindVertexArray(vao);
	glGenBuffers(1, &vbo);
	glBindBuffer(GL_ARRAY_BUFFER, vbo);
	glBufferData(GL_ARRAY_BUFFER, sizeof(float)*vertices.size(), vertices.data(), GL_STATIC_DRAW);
	glEnableVertexAttribArray(0);
	glVertexAttribPointer(0, 2, GL_FLOAT, GL_FALSE, sizeof(float) * 2, (void*)0);
	//shader
	GLuint shader, shader_vertex, shader_fragment;
	shader = shader_vertex = shader_fragment = 0;
	const char* shader_vertex_source = "#version 450 \n"
		"layout (location=0) in vec2 pos;\n"
		"void main(){\n"
		"gl_Position = vec4(pos.xy, 0, 1); \n"
		"} \n";
	GLint shader_source_length = (GLint)strlen(shader_vertex_source);
	shader_vertex = glCreateShader(GL_VERTEX_SHADER);
	glShaderSource(shader_vertex, 1, &shader_vertex_source, &shader_source_length);
	glCompileShader(shader_vertex);
	const char* shader_fragment_source = "#version 450 \n"
		"layout (location=0) out vec4 color;\n"
		"void main(){\n"
		"color = vec4(gl_FragCoord.x/800,gl_FragCoord.y/600,0,0);\n"
		"} \n";
	shader_source_length = (GLint)strlen(shader_fragment_source);
	shader_fragment = glCreateShader(GL_FRAGMENT_SHADER);
	glShaderSource(shader_fragment, 1, &shader_fragment_source, &shader_source_length);
	glCompileShader(shader_fragment);
	shader = glCreateProgram();
	glAttachShader(shader, shader_vertex);
	glAttachShader(shader, shader_fragment);
	glLinkProgram(shader);
	glDeleteShader(shader_vertex);		//not needed anymore
	glDeleteShader(shader_fragment);
	glUseProgram(shader);

	//as long as the window is opened
	while (window.isOpened()) {
		//clear screen to blue
		glClearColor(0, 0, 1, 0);
		glClear(GL_COLOR_BUFFER_BIT);

		//6 vertices (2 triangles)
		glDrawArrays(GL_TRIANGLES, 0, 6);

		//swap buffers
		window.swapBuffers();

		//process events
		while (const Event* e = window.processEvent()) {
			if (e->type == Event::Type::KeyPress) if (e->KeyPress.key == Key::ESCAPE) window.close();
		}

		//process all application events (has to be constantly called from the MAIN thread)
		wicsystem.processProgramEvents();
	};

	//the OpenGL context exists as long as the window exists, clean up can be safely performed here
	glDeleteProgram(shader);
	glDeleteBuffers(1, &vbo);
	glDeleteVertexArrays(1, &vao);

	//window and OpenGL destroyed by going out of scope 
	//system destroyed by going out of scope
#endif
}

//-------------------------------------------------------------------------------------------------
void testOpenGLextensions() {
#ifdef LAP_WIC_SUPPRESS_GL_EXTENSIONS
	std::cout << "------------------------------------------------------------\nTest OpenGL extesions : SKIPPED , OpenGL function loading disabled" << std::endl;
#else
	std::cout << "------------------------------------------------------------\nTest OpenGL extensions" << std::endl;
	using namespace lap::wic;

	//create a WIC system, logging on std::cout and without thread safety (running in singlethreaded mode)
	WICSystem wicsystem(&std::cout, false);

	//creates a window with default properties and a modern opengl context
	ContextProperties cp;
	cp.api = ContextProperties::ApiType::OPENGL;	//using OpenGL
	cp.debug_context = true;
	cp.profile_core = true;
	cp.release_behavior_flush = true;				//flush pipeline on context release?
	cp.robustness = ContextProperties::RobustnessType::LOSE_CONTEXT_ON_RESET;	//lose context on reset
	cp.swap_interval = -1;		//adaptive vsync
	cp.version_major = 4;
	cp.version_minor = 5;
	WindowProperties wp; FramebufferProperties fp; InputProperties ip;
	Window window = Window(wp, fp, cp, ip);
	// the window OpenGL context is now binded on this thread 
	// extensions are also loaded, based on requested version, e.g. glDispatchCompute isn't loaded for OpenGL 4.1

	//ubiquitous extensions (EXT_texture_filter_anisotropic, EXT_texture_compression_s3tc, EXT_texture_sRGB) are already loaded

	// simple token based extensions
	bool supported = window.isOpenGLExtensionSupported("GL_NVX_gpu_memory_info");
	std::cout << " GL_NVX_gpu_memory_info support = " << std::boolalpha << supported << std::endl;
	if (supported) {
		GLint GL_GPU_MEM_INFO_TOTAL_AVAILABLE_MEM_NVX = 0x9048;
		GLint GL_GPU_MEM_INFO_CURRENT_AVAILABLE_MEM_NVX = 0x9049;
		GLint data[2];
		glGetIntegerv(GL_GPU_MEM_INFO_TOTAL_AVAILABLE_MEM_NVX,&data[0]);
		glGetIntegerv(GL_GPU_MEM_INFO_CURRENT_AVAILABLE_MEM_NVX, &data[1]);
		std::cout << "\t\ttotal memory = " << data[0] << " available=" << data[1] << std::endl;
	}
	supported = window.isOpenGLExtensionSupported("GL_ATI_meminfo");
	std::cout << " GL_ATI_meminfo support = " << std::boolalpha << supported << std::endl;
	if (supported) {
		GLint GL_VBO_FREE_MEMORY_ATI = 0x87FB;
		GLint GL_TEXTURE_FREE_MEMORY_ATI = 0x87FC;
		GLint data[2];
		glGetIntegerv(GL_VBO_FREE_MEMORY_ATI, &data[0]);
		glGetIntegerv(GL_TEXTURE_FREE_MEMORY_ATI, &data[1]);
		std::cout << "\t\tfree vbo memory = " << data[0] << " free texture memory =" << data[1] << std::endl;
	}

	// extension with functions
	supported = window.isOpenGLExtensionSupported("GL_ARB_sparse_buffer");
	std::cout << " GL_arb_sparse_buffer support = " << std::boolalpha << supported << std::endl;
	if (supported) {
		typedef void (APIENTRYP PFNGLBUFFERPAGECOMMITMENTARBPROC)(GLenum target, GLintptr offset, GLsizeiptr size, GLboolean commit);
		PFNGLBUFFERPAGECOMMITMENTARBPROC my_glBufferPageCommitmentARB = (PFNGLBUFFERPAGECOMMITMENTARBPROC)window.loadOpenGLExtensionFunction("glBufferPageCommitmentARB");
		std::cout<<"my_glBufferPageCommitmentARB address is  "<< &my_glBufferPageCommitmentARB <<std::endl;
		// do work here but don't forget this function pointer is unmanaged and might be (usually isn't) valid only for this context
	}
	supported = window.isOpenGLExtensionSupported("GL_ARB_sparse_texture");
	std::cout << " GL_arb_sparse_texture support = " << std::boolalpha << supported << std::endl;
	if (supported) {
		typedef void (APIENTRYP PFNGLTEXPAGECOMMITMENTARBPROC)(GLenum target, GLint level, GLint xoffset, GLint yoffset, GLint zoffset, GLsizei width, GLsizei height, GLsizei depth, GLboolean commit);
		PFNGLTEXPAGECOMMITMENTARBPROC my_glBufferPageCommitmentARB = (PFNGLTEXPAGECOMMITMENTARBPROC)window.loadOpenGLExtensionFunction("glTexPageCommitmentARB");
		std::cout<<"my_glBufferPageCommitmentARB address is  "<< &my_glBufferPageCommitmentARB <<std::endl;
		// do work here but don't forget this function pointer is unmanaged and might be (usually isn't) valid only for this context
	}

	//window and OpenGL context destroyed by going out of scope
	//system destroyed by going out of scope
#endif
}

//-------------------------------------------------------------------------------------------------
void testBasicOpenGLProfileFlags() {
#ifdef LAP_WIC_SUPPRESS_GL_EXTENSIONS
	std::cout << "------------------------------------------------------------\nTest Basic OpenGL Profile Flags : SKIPPED , OpenGL function loading disabled" << std::endl;
#else
	std::cout << "------------------------------------------------------------\nTest Basic OpenGL Profile Flags" << std::endl;
	using namespace lap::wic;

	//create a WIC system, logging on std::cout and without thread safety (running in singlethreaded mode)
	WICSystem wicsystem(&std::cout, false);

	//creates a window with default properties and a modern opengl context
	ContextProperties cp;
	cp.api = ContextProperties::ApiType::OPENGL;	//using OpenGL
	cp.debug_context = true;						//debug?
	cp.profile_core = true;							//core profile without compatibility?
	cp.release_behavior_flush = true;				//flush pipeline on context release?
	cp.robustness = ContextProperties::RobustnessType::LOSE_CONTEXT_ON_RESET;	//lose context on reset
	cp.swap_interval = -1;		//adaptive vsync
	cp.version_major = 3;		
	cp.version_minor = 3;	
	WindowProperties wp; FramebufferProperties fp; InputProperties ip;
	Window window = Window(wp, fp, cp, ip);
	// the window OpenGL context is now binded on this thread 
	// extensions are also loaded, based on requested version, e.g. glDispatchCompute isn't loaded for OpenGL 4.1


	//no drawing, just testing context flags
	std::cout << "---------------------------------------------------------------------" << std::endl;
	int vmin, vmax, flags, profile;
	glGetIntegerv(GL_CONTEXT_FLAGS, &flags);
	glGetIntegerv(GL_MAJOR_VERSION, &vmax);															
	glGetIntegerv(GL_MINOR_VERSION, &vmin);
	std::cout << "Version = " << vmax << "." << vmin << std::endl;
	if (flags & GL_CONTEXT_FLAG_DEBUG_BIT) std::cout << "debug" << std::endl;
	if (flags & GL_CONTEXT_FLAG_FORWARD_COMPATIBLE_BIT) std::cout << "forward compatible" << std::endl;	
	if (flags & GL_CONTEXT_FLAG_ROBUST_ACCESS_BIT) std::cout << "robust" << std::endl;
	glGetIntegerv(GL_CONTEXT_PROFILE_MASK, &profile);
	if (profile & GL_CONTEXT_CORE_PROFILE_BIT) std::cout << "Core" << std::endl;
	else std::cout << "Compatibility" << std::endl;
	std::cout << "---------------------------------------------------------------------" << std::endl;

	//window and OpenGL context destroyed by going out of scope
	//system destroyed by going out of scope
#endif
}


//-------------------------------------------------------------------------------------------------
void testBasicOpenGLSharedContext() {
#ifdef LAP_WIC_SUPPRESS_GL_EXTENSIONS
	std::cout << "------------------------------------------------------------\nTest Basic OpenGL Shared Context : SKIPPED , OpenGL function loading disabled" << std::endl;
#else
	std::cout << "------------------------------------------------------------\nTest Basic OpenGL shared context" << std::endl;
	using namespace lap::wic;

	//create a WIC system, logging on std::cout and without thread safety (running in singlethreaded mode)
	WICSystem wicsystem(&std::cout, false);


	//creates a window with default properties and a modern opengl context
	ContextProperties cp;
	cp.api = ContextProperties::ApiType::OPENGL;	//using OpenGL
	cp.debug_context = true;
	cp.profile_core = true;
	cp.release_behavior_flush = true;				//flush pipeline on context release?
	cp.robustness = ContextProperties::RobustnessType::LOSE_CONTEXT_ON_RESET;	//lose context on reset
	cp.swap_interval = -1;		//adaptive vsync
	cp.version_major = 4;		//ask for OpenGL 4.5
	cp.version_minor = 5;
	WindowProperties wp; FramebufferProperties fp; InputProperties ip;
	Window window = Window(wp, fp, cp, ip);
	window.hide();
	//creates a window who shares the context lists (vbos, textures, etc) with the context from window.
	Window window2 = Window(wp, fp, cp, ip, nullptr, &window);

	//create geometry on the first context
	window.bindContext();
	std::vector<float> vertices = { -0.5,-0.5, -0.5, 0.5, 0.5, 0.5,  0.5, 0.5,  0.5, -0.5, -0.5, -0.5 };
	GLuint vbo;
	glGenBuffers(1, &vbo);
	glBindBuffer(GL_ARRAY_BUFFER, vbo);
	glBufferData(GL_ARRAY_BUFFER, sizeof(float)*vertices.size(), vertices.data(), GL_STATIC_DRAW);

	//create vao and shader and render on the second context
	window2.bindContext();
	//vao state
	GLuint vao;
	glGenVertexArrays(1, &vao);
	glBindVertexArray(vao);
	glBindBuffer(GL_ARRAY_BUFFER, vbo);
	glEnableVertexAttribArray(0);
	glVertexAttribPointer(0, 2, GL_FLOAT, GL_FALSE, sizeof(float) * 2, (void*)0);
		//shader
	GLuint shader, shader_vertex, shader_fragment;
	shader = shader_vertex = shader_fragment = 0;
	const char* shader_vertex_source = "#version 450 \n"
		"layout (location=0) in vec2 pos;\n"
		"void main(){\n"
		"gl_Position = vec4(pos.xy, 0, 1); \n"
		"} \n";
	GLint shader_source_length = (GLint)strlen(shader_vertex_source);
	shader_vertex = glCreateShader(GL_VERTEX_SHADER);
	glShaderSource(shader_vertex, 1, &shader_vertex_source, &shader_source_length);
	glCompileShader(shader_vertex);
	const char* shader_fragment_source = "#version 450 \n"
		"layout (location=0) out vec4 color;\n"
		"void main(){\n"
		"color = vec4(gl_FragCoord.x/800,gl_FragCoord.y/600,0,0);\n"
		"} \n";
	shader_source_length = (GLint)strlen(shader_fragment_source);
	shader_fragment = glCreateShader(GL_FRAGMENT_SHADER);
	glShaderSource(shader_fragment, 1, &shader_fragment_source, &shader_source_length);
	glCompileShader(shader_fragment);
	shader = glCreateProgram();
	glAttachShader(shader, shader_vertex);
	glAttachShader(shader, shader_fragment);
	glLinkProgram(shader);
	glDeleteShader(shader_vertex);		//not needed anymore
	glDeleteShader(shader_fragment);
	glUseProgram(shader);

	//as long as the window is opened
	while (window2.isOpened()) {
		//clear screen to blue
		glClearColor(0, 0, 1, 0);
		glClear(GL_COLOR_BUFFER_BIT);

		//6 vertices (2 triangles)
		glDrawArrays(GL_TRIANGLES, 0, 6);

		//swap buffers
		window2.swapBuffers();

		//process events
		while (const Event* e = window2.processEvent()) {
			if (e->type == Event::Type::KeyPress) if (e->KeyPress.key == Key::ESCAPE) window2.close();
		}

		//process all application events (has to be constantly called from the MAIN thread)
		wicsystem.processProgramEvents();
	};

	//cleanup OpenGL on context from window 2 (while the window is closed the context is still valid and active) 
	glDeleteProgram(shader);
	glDeleteBuffers(1, &vbo);
	glDeleteVertexArrays(1, &vao);

	//window 1 (the hidden one) and 2 and their OpenGL contexts destroyed by going out of scope
	//system destroyed by going out of scope
#endif
}


//-------------------------------------------------------------------------------------------------
void testWindowProperties() {
	std::cout << "------------------------------------------------------------\nTest Window Properties" << std::endl;
	using namespace lap::wic;

	//create a WIC system, logging on std::cout and without thread safety (running in singlethreaded mode)
	WICSystem wicsystem(&std::cout, false);
		
	//window properties
	WindowProperties wp;
	wp.auto_restore_mode_on_minimize = false;
	wp.decorated = true;
	wp.floating = false;
	wp.focused = false;
	wp.fullscreen = false;
	wp.resizable = true;
	wp.title = "WIC - Single Window Test";
	wp.visible = true;
	wp.width = 400;
	wp.height = 400;
	wp.position_x = 500;
	wp.position_y = 300;
	std::vector<unsigned char> v; v.resize(32 * 32 * 4); for (int i = 0; i < 32 * 32; i++) {
		v[i * 4] = 255;			//yellow icon
		v[i * 4 + 1] = 255;
		v[i * 4 + 2] = 0;
		v[i * 4 + 3] = 0;
	}
	wp.icon_pixels = &v[0];
	
	//framebuffer properties
	FramebufferProperties fb;
	fb.accum_alpha_bits = 8;
	fb.accum_blue_bits = 8;
	fb.accum_green_bits = 8;
	fb.accum_red_bits = 8;
	fb.alpha_bits = 8;
	fb.red_bits = 8;
	fb.green_bits = 8;
	fb.blue_bits = 8;
	fb.aux_buffers = 1;
	fb.doublebuffer = true;
	fb.samples_per_pixel = 4;
	//fb.stereo = true;	//needs HW support..
	fb.srgb = true;

	//context properties
	ContextProperties cp;
	cp.api = ContextProperties::ApiType::OPENGL;
	cp.debug_context = false;
	cp.profile_core = false;
	cp.release_behavior_flush = false;
	cp.robustness = ContextProperties::RobustnessType::NO_RESET_NOTIFICATION;
	cp.version_major = 1;
	cp.version_minor = 1;
	cp.swap_interval = -1;//adaptive

	//input properties
	InputProperties ip;
	ip.cursor_type = InputProperties::CursorType::CROSSHAIR;
	ip.cursor_enabled = true;
	ip.cursor_visible = true;
	ip.sticky_keys = true;
	ip.sticky_keys = true;
	
	//create a windows with all these properties 
	//warnings are issued when the requested properties are incompatible but solvable
	//this function will crash if some hardware dependent requested properties are not present, e.g. stereo
	//each WindowProperties, FramebufferProperties, ContextProperties and InputProperties filed is properly documented.
	Window window = Window(wp, fb, cp, ip);
	// the window OpenGL context is now binded on this thread
	window.setCallbackWindowClose([](const Window& caller, uint64_t timestamp) {std::cout << "Close Callback" << std::endl; });

	while (window.isOpened()) {
		while (const Event* eptr = window.processEvent()) {
			if (eptr->type == Event::Type::KeyPress) if (eptr->KeyPress.key == Key::ESCAPE) window.close();
		}
		//process all application events (has to be constantly called from the MAIN thread)
		wicsystem.processProgramEvents();
	};

	//window and OpenGL context destroyed by going out of scope
	//system destroyed by going out of scope
}


//-------------------------------------------------------------------------------------------------
void testAllEvents() {
	std::cout << "------------------------------------------------------------\nTest All Events" << std::endl;
	using namespace lap::wic;

	//create a WIC system, logging on std::cout and without thread safety (running in singlethreaded mode)
	WICSystem wicsystem(&std::cout, false);


	//create window
	WindowProperties wp; FramebufferProperties fp; ContextProperties cp; InputProperties ip;
	Window window = Window(wp, fp, cp, ip);
	// the window OpenGL context is now binded on this thread

	
	auto timestart = std::chrono::high_resolution_clock::now();
	unsigned int frames = 0;
	//test all events
	while (window.isOpened()) {
		if (window.getContextProperties().api == ContextProperties::ApiType::OPENGL) window.swapBuffers();
		while (const Event* eptr = window.processEvent()) {
			const Event& e = (*eptr);
			switch (e.type) {
			case Event::Type::ControllerConnect:
				std::cout << "[" << e.timestamp << "ms] ControllerConnect : " << e.ControllerConnect.id << std::endl;
				break;
			case Event::Type::ControllerDisconnect:
				std::cout << "[" << e.timestamp << "ms] ControllerDisconnect : " << e.ControllerDisconnect.id << std::endl;
				break;
			case Event::Type::ControllerButtonPress:
				std::cout << "[" << e.timestamp << "ms] Controller : " << e.ControllerButtonPress.id << " button " << (int)e.ControllerButtonPress.button << " pressed." << std::endl;
				break;
			case Event::Type::ControllerButtonRelease:
				std::cout << "[" << e.timestamp << "ms] Controller : " << e.ControllerButtonRelease.id << " button " << (int)e.ControllerButtonRelease.button << " released." << std::endl;
				break;
			case Event::Type::ControllerAxisMove:
				std::cout << "[" << e.timestamp << "ms] Controller : " << e.ControllerAxisMove.id << " axis move 0:" << e.ControllerAxisMove.axes[0] << " 1:" << e.ControllerAxisMove.axes[1] << " 2:" << e.ControllerAxisMove.axes[2] << " 3:" << e.ControllerAxisMove.axes[3] << " 4:" << e.ControllerAxisMove.axes[4] << " 5:" << e.ControllerAxisMove.axes[5] << std::endl;
				break;
			case Event::Type::FilesDrop:
				std::cout << "[" << e.timestamp << "ms] FilesDrop : "; for (auto& p : (*e.FilesDrop.paths)) std::cout << p << std::endl;
				break;
			case Event::Type::FramebufferResize:
				std::cout << "[" << e.timestamp << "ms] FramebufferResize : width=" << e.FramebufferResize.width << " height=" << e.FramebufferResize.height << std::endl;
				break;
			case Event::Type::KeyPress:
				std::cout << "[" << e.timestamp << "ms] Key Pressed : key = " << (int)e.KeyPress.key << " alt=" << e.KeyPress.alt << " control=" << e.KeyPress.control << " shift=" << e.KeyPress.shift << " system=" << e.KeyPress.system << std::endl;
				break;
			case Event::Type::KeyRelease:
				std::cout << "[" << e.timestamp << "ms] Key Released : key = " << (int)e.KeyRelease.key << " alt=" << e.KeyRelease.alt << " control=" << e.KeyRelease.control << " shift=" << e.KeyRelease.shift << " system=" << e.KeyRelease.system << std::endl;
				break;
			case Event::Type::KeyRepeat:
				std::cout << "[" << e.timestamp << "ms] Key Repeat : key = " << (int)e.KeyRepeat.key << " alt=" << e.KeyRepeat.alt << " control=" << e.KeyRepeat.control << " shift=" << e.KeyRepeat.shift << " system=" << e.KeyRepeat.system << std::endl;
				break;
			case Event::Type::MonitorConnect:
				std::cout << "[" << e.timestamp << "ms] MonitorConnect : " << e.MonitorConnect.monitor->handle << std::endl;
				break;
			case Event::Type::MonitorDisconnect:
				std::cout << "[" << e.timestamp << "ms] MonitorDisconnect : " << e.MonitorDisconnect.monitor->handle << std::endl;
				break;
			case Event::Type::MousePress:
				std::cout << "[" << e.timestamp << "ms] MousePress : button = " << (int)e.MousePress.button << " posx=" << e.MousePress.position_x << " posy=" << e.MousePress.position_y << " alt=" << e.MousePress.alt << " control=" << e.MousePress.control << " shift=" << e.MousePress.shift << " system=" << e.MousePress.system << std::endl;
				break;
			case Event::Type::MouseRelease:
				std::cout << "[" << e.timestamp << "ms] MouseRelease : button = " << (int)e.MouseRelease.button << " posx=" << e.MouseRelease.position_x << " posy=" << e.MouseRelease.position_y << " alt=" << e.MouseRelease.alt << " control=" << e.MouseRelease.control << " shift=" << e.MouseRelease.shift << " system=" << e.MouseRelease.system << std::endl;
				break;
			case Event::Type::MouseDrag:
				std::cout << "[" << e.timestamp << "ms] MouseDrag : button = " << (int)e.MouseDrag.button << " posx=" << e.MouseDrag.position_x << " posy=" << e.MouseDrag.position_y << " alt=" << e.MouseDrag.alt << " control=" << e.MouseDrag.control << " shift=" << e.MouseDrag.shift << " system=" << e.MouseDrag.system << std::endl;
				break;
			case Event::Type::MouseEnter:
				std::cout << "[" << e.timestamp << "ms] MouseEnter " << std::endl;
				break;
			case Event::Type::MouseLeave:
				std::cout << "[" << e.timestamp << "ms] MouseLeave " << std::endl;
				break;
			case Event::Type::MouseMove:
				std::cout << "[" << e.timestamp << "ms] MouseMove posx=" << e.MouseMove.position_x << " posy=" << e.MouseMove.position_y << std::endl;
				break;
			case Event::Type::MouseScroll:
				std::cout << "[" << e.timestamp << "ms] MouseScroll : scrollx= " << e.MouseScroll.scroll_x << " scrolly=" << e.MouseScroll.scroll_y << " posx=" << e.MouseScroll.position_x << " posy=" << e.MouseScroll.position_y << " alt=" << e.MouseScroll.alt << " control=" << e.MouseScroll.control << " shift=" << e.MouseScroll.shift << " system=" << e.MouseScroll.system << std::endl;
				break;
			case Event::Type::UnicodeChar:
				std::cout << "[" << e.timestamp << "ms] UnicodeChar " << e.UnicodeChar.codepoint << std::endl;
				break;
			case Event::Type::UnicodeCharMods:
				std::cout << "[" << e.timestamp << "ms] UnicodeCharMods : codepoint = " << e.UnicodeCharMods.codepoint << " alt=" << e.UnicodeCharMods.alt << " control=" << e.UnicodeCharMods.control << " shift=" << e.UnicodeCharMods.shift << " system=" << e.UnicodeCharMods.system << std::endl;
				break;
			case Event::Type::WindowClose:
				std::cout << "[" << e.timestamp << "ms] WindowClose " << std::endl;
				break;
			case Event::Type::WindowFocusGain:
				std::cout << "[" << e.timestamp << "ms] WindowFocusGain " << std::endl;
				break;
			case Event::Type::WindowFocusLose:
				std::cout << "[" << e.timestamp << "ms] WindowFocusLose " << std::endl;
				break;
			case Event::Type::WindowMinimize:
				std::cout << "[" << e.timestamp << "ms] WindowMinimize " << std::endl;
				break;
			case Event::Type::WindowMaximize:
				std::cout << "[" << e.timestamp << "ms] WindowMaximize " << std::endl;
				break;
			case Event::Type::WindowMove:
				std::cout << "[" << e.timestamp << "ms] WindowMove posx=" << e.WindowMove.position_x << " posy=" << e.WindowMove.position_y << std::endl;
				break;
			case Event::Type::WindowRefresh:
				std::cout << "[" << e.timestamp << "ms] WindowRefresh " << std::endl;
				break;
			case Event::Type::WindowResize:
				std::cout << "[" << e.timestamp << "ms] WindowResize width=" << e.WindowResize.width << " height=" << e.WindowResize.height << std::endl;
				break;
			case Event::Type::WindowRestore:
				std::cout << "[" << e.timestamp << "ms] WindowRestore " << std::endl;
				break;
			}
		}

		//process all application events (has to be constantly called from the MAIN thread)
		wicsystem.processProgramEvents();

		//count frames
		frames++;
	}

	auto avg_duration = std::chrono::duration_cast<std::chrono::milliseconds>(std::chrono::high_resolution_clock::now() - timestart).count() / (float)frames;
	std::cout << "Ran for " << frames << " frames, with avg frame duration= " << avg_duration << "ms"<< std::endl;

	//window destroyed by going out of scope
	//system destroyed by going out of scope
}


//-------------------------------------------------------------------------------------------------
void testAllCallbacks() {
	std::cout << "------------------------------------------------------------\nTest All Callbacks" << std::endl;
	using namespace lap::wic;

	//create a WIC system, logging on std::cout and without thread safety (running in singlethreaded mode)
	WICSystem wicsystem(&std::cout, false);

	//create windows
	WindowProperties wp1, wp2; 
	wp1.position_x = 100; wp1.position_y = 100;	wp1.width = 500; wp1.height = 400; wp1.title = "1st";
	wp2.position_x = 900; wp2.position_y = 100;	wp2.width = 500; wp2.height = 400; wp2.title = "2nd";
	FramebufferProperties fp; ContextProperties cp; InputProperties ip;
	Window window1 = Window(wp1, fp, cp, ip);
	// the window1 OpenGL context is now binded on this thread
	Window window2 = Window(wp2, fp, cp, ip);
	// the window2 OpenGL context is now binded on this thread

	//define callbacks and link them to windows, any type of std::function<TYPE> is accepted so member functions, global functions, lambdas, binds, etc can be used.
	auto callbackControllerConnect = [](unsigned int id, uint64_t timestamp) {
		std::cout << "[" << timestamp << "ms] ControllerConnect : " << id << std::endl;
	};
	window1.setCallbackControllerConnect(callbackControllerConnect);
	window2.setCallbackControllerConnect(callbackControllerConnect);
	auto callbackControllerDisconnect = [](unsigned int id, uint64_t timestamp) {
		std::cout << "[" << timestamp << "ms] ControllerDisconnect : " << id << std::endl;
	};
	window1.setCallbackControllerDisconnect(callbackControllerDisconnect);
	window2.setCallbackControllerDisconnect(callbackControllerDisconnect);
	auto callbackControllerButtonPress = [](unsigned int id, ControllerButton button, uint64_t timestamp) {
		std::cout << "[" << timestamp << "ms] Controller : " << id << " button press " << (int)button << std::endl;
	};
	window1.setCallbackControllerButtonPress(callbackControllerButtonPress);
	window2.setCallbackControllerButtonPress(callbackControllerButtonPress);
	auto callbackControllerButtonRelease = [](unsigned int id, ControllerButton button, uint64_t timestamp) {
		std::cout << "[" << timestamp << "ms] Controller : " << id << " button release " << (int)button << std::endl;
	};
	window1.setCallbackControllerButtonRelease(callbackControllerButtonRelease);
	window2.setCallbackControllerButtonRelease(callbackControllerButtonRelease);
	auto callbackControllerAxisMove = [](unsigned int id, float (&axes)[6], uint64_t timestamp) {
		std::cout << "[" << timestamp << "ms] Controller : " << id << " axis move 0:" << axes[0]<<" 1:"<<axes[1]<<" 2:"<<axes[2]<<" 3:"<<axes[3]<<" 4:"<<axes[4]<<" 5:"<<axes[5] << std::endl;
	};
	window1.setCallbackControllerAxisMove(callbackControllerAxisMove);
	window2.setCallbackControllerAxisMove(callbackControllerAxisMove);
	auto callbackFilesDrop = [](Window& wnd, const std::vector<std::string>& paths, uint64_t timestamp) {
		std::cout << "[" << timestamp << "ms] Window "<<wnd.getWindowProperties().title <<" FilesDrop : "; for (auto& p : paths) std::cout << p << std::endl;
	};
	window1.setCallbackFilesDrop(callbackFilesDrop);
	window2.setCallbackFilesDrop(callbackFilesDrop);
	auto callbackFramebufferResize = [](Window& wnd, unsigned int width, unsigned int height, uint64_t timestamp) {
		std::cout << "[" << timestamp << "ms] Window "<<wnd.getWindowProperties().title <<" FramebufferResize : width=" << width << " height=" << height << std::endl;
	};
	window1.setCallbackFramebufferResize(callbackFramebufferResize);
	window2.setCallbackFramebufferResize(callbackFramebufferResize);
	auto callbackKeyPress = [](Window& wnd, Key key, bool alt, bool control, bool shift, bool system, const InputState& state, uint64_t timestamp) {
		std::cout << "[" << timestamp << "ms] Window "<<wnd.getWindowProperties().title <<" Key Pressed : key = " << (int)key << " alt=" << alt << " control=" << control << " shift=" << shift << " system=" << system << std::endl;
	};
	window1.setCallbackKeyPress(callbackKeyPress);
	window2.setCallbackKeyPress(callbackKeyPress);
	auto callbackKeyRelease = []( Window& wnd, Key key, bool alt, bool control, bool shift, bool system, const InputState& state, uint64_t timestamp) {
		std::cout << "[" << timestamp << "ms] Window "<<wnd.getWindowProperties().title <<" Key Released : key = " << (int)key << " alt=" << alt << " control=" << control << " shift=" << shift << " system=" << system << std::endl;
	};
	window1.setCallbackKeyRelease(callbackKeyRelease);
	window2.setCallbackKeyRelease(callbackKeyRelease);
	auto callbackKeyRepeat = []( Window& wnd, Key key, bool alt, bool control, bool shift, bool system, const InputState& state, uint64_t timestamp) {
		std::cout << "[" << timestamp << "ms] Window "<<wnd.getWindowProperties().title <<" Key Repeat : key = " << (int)key << " alt=" << alt << " control=" << control << " shift=" << shift << " system=" << system << std::endl;
	};
	window1.setCallbackKeyRepeat(callbackKeyRepeat);
	window2.setCallbackKeyRepeat(callbackKeyRepeat);
	auto callbackMonitorConnect = [](const Monitor& monitor, uint64_t timestamp) {
		std::cout << "[" << timestamp << "ms] MonitorConnect : " << monitor.handle << std::endl;
	};
	window1.setCallbackMonitorConnect(callbackMonitorConnect);
	window2.setCallbackMonitorConnect(callbackMonitorConnect);
	auto callbackMonitorDisconnect = [](const Monitor& monitor, uint64_t timestamp) {
		std::cout << "[" << timestamp << "ms] MonitorDisconnect : " << monitor.handle << std::endl;
	};
	window1.setCallbackMonitorDisconnect(callbackMonitorDisconnect);
	window2.setCallbackMonitorDisconnect(callbackMonitorDisconnect);
	auto callbackMousePress = []( Window& wnd, MouseButton button, unsigned int posx, unsigned int posy, bool alt, bool control, bool shift, bool system, const InputState& state, uint64_t timestamp) {
		std::cout << "[" << timestamp << "ms] Window "<<wnd.getWindowProperties().title <<" MousePress : button = " << (int)button << " posx=" << posx << " posy=" << posy << " alt=" << alt << " control=" << control << " shift=" << shift << " system=" << system << std::endl;
	};
	window1.setCallbackMousePress(callbackMousePress);
	window2.setCallbackMousePress(callbackMousePress);
	auto callbackMouseRelease = []( Window& wnd, MouseButton button, unsigned int posx, unsigned int posy, bool alt, bool control, bool shift, bool system, const InputState& state, uint64_t timestamp) {
		std::cout << "[" << timestamp << "ms] Window "<<wnd.getWindowProperties().title <<" MouseRelease : button = " << (int)button << " posx=" << posx << " posy=" << posy << " alt=" << alt << " control=" << control << " shift=" << shift << " system=" << system << std::endl;
	};
	window1.setCallbackMouseRelease(callbackMouseRelease);
	window2.setCallbackMouseRelease(callbackMouseRelease);
	auto callbackMouseDrag = []( Window& wnd, MouseButton button, unsigned int posx, unsigned int posy, bool alt, bool control, bool shift, bool system, const InputState& state, uint64_t timestamp) {
		std::cout << "[" << timestamp << "ms] Window "<<wnd.getWindowProperties().title <<" MouseDrag : button = " << (int)button << " posx=" << posx << " posy=" << posy << " alt=" << alt << " control=" << control << " shift=" << shift << " system=" << system << std::endl;
	};
	window1.setCallbackMouseDrag(callbackMouseDrag);
	window2.setCallbackMouseDrag(callbackMouseDrag);
	auto callbackMouseEnter = []( Window& wnd, uint64_t timestamp) {
		std::cout << "[" << timestamp << "ms] Window "<<wnd.getWindowProperties().title <<" MouseEnter " << std::endl;
	};
	window1.setCallbackMouseEnter(callbackMouseEnter);
	window2.setCallbackMouseEnter(callbackMouseEnter);
	auto callbackMouseLeave = []( Window& wnd, uint64_t timestamp) {
		std::cout << "[" << timestamp << "ms] Window "<<wnd.getWindowProperties().title <<" MouseLeave " << std::endl;
	};
	window1.setCallbackMouseLeave(callbackMouseLeave);
	window2.setCallbackMouseLeave(callbackMouseLeave);
	auto callbackMouseMove = []( Window& wnd, unsigned int posx, unsigned int posy, const InputState& state, uint64_t timestamp) {
		std::cout << "[" << timestamp << "ms] Window "<<wnd.getWindowProperties().title <<" MouseMove posx=" << posx << " posy=" << posy << std::endl;
	};
	window1.setCallbackMouseMove(callbackMouseMove);
	window2.setCallbackMouseMove(callbackMouseMove);
	auto callbackMouseScroll = []( Window& wnd, float scrollx, float scrolly, unsigned int posx, unsigned int posy, bool alt, bool control, bool shift, bool system, const InputState& state, uint64_t timestamp) {
		std::cout << "[" << timestamp << "ms] Window "<<wnd.getWindowProperties().title <<" MouseScroll : scrollx= " << scrollx << " scrolly=" << scrolly << " posx=" << posx << " posy=" << posy << " alt=" << alt << " control=" << control << " shift=" << shift << " system=" << system << std::endl;
	};
	window1.setCallbackMouseScroll(callbackMouseScroll);
	window2.setCallbackMouseScroll(callbackMouseScroll);
	auto callbackUnicodeChar = []( Window& wnd, unsigned int codepoint, const InputState& state, uint64_t timestamp) {
		std::cout << "[" << timestamp << "ms] Window "<<wnd.getWindowProperties().title <<" UnicodeChar " << codepoint << std::endl;
	};
	window1.setCallbackUnicodeChar(callbackUnicodeChar);
	window2.setCallbackUnicodeChar(callbackUnicodeChar);
	auto callbackUnicodeCharMods = []( Window& wnd, unsigned int codepoint, bool alt, bool control, bool shift, bool system, const InputState& state, uint64_t timestamp) {
		std::cout << "[" << timestamp << "ms] Window "<<wnd.getWindowProperties().title <<" UnicodeCharMods : codepoint = " << codepoint << " alt=" << alt << " control=" << control << " shift=" << shift << " system=" << system << std::endl;
	};
	window1.setCallbackUnicodeCharMods(callbackUnicodeCharMods);
	window2.setCallbackUnicodeCharMods(callbackUnicodeCharMods);
	auto callbackWindowClose = []( Window& wnd, uint64_t timestamp) {
		std::cout << "[" << timestamp << "ms] Window "<<wnd.getWindowProperties().title <<" WindowClose " << std::endl;
	};
	window1.setCallbackWindowClose(callbackWindowClose);
	window2.setCallbackWindowClose(callbackWindowClose);
	auto callbackWindowFocusGain = []( Window& wnd, uint64_t timestamp) {
		std::cout << "[" << timestamp << "ms] Window "<<wnd.getWindowProperties().title <<" WindowFocusGain " << std::endl;
	};
	window1.setCallbackWindowFocusGain(callbackWindowFocusGain);
	window2.setCallbackWindowFocusGain(callbackWindowFocusGain);
	auto callbackWindowFocusLose = []( Window& wnd, uint64_t timestamp) {
		std::cout << "[" << timestamp << "ms] Window "<<wnd.getWindowProperties().title <<" WindowFocusLose " << std::endl;
	};
	window1.setCallbackWindowFocusLose(callbackWindowFocusLose);
	window2.setCallbackWindowFocusLose(callbackWindowFocusLose);
	auto callbackWindowMinimize = []( Window& wnd, uint64_t timestamp) {
		std::cout << "[" << timestamp << "ms] Window "<<wnd.getWindowProperties().title <<" WindowMinimize " << std::endl;
	};
	window1.setCallbackWindowMinimize(callbackWindowMinimize);
	window2.setCallbackWindowMinimize(callbackWindowMinimize);
	auto callbackWindowMaximize = []( Window& wnd, uint64_t timestamp) {
		std::cout << "[" << timestamp << "ms] Window " << wnd.getWindowProperties().title << " WindowMaximize " << std::endl;
	};
	window1.setCallbackWindowMaximize(callbackWindowMaximize);
	window2.setCallbackWindowMaximize(callbackWindowMaximize);
	auto callbackWindowMove = []( Window& wnd, unsigned int posx, unsigned int posy, uint64_t timestamp) {
		std::cout << "[" << timestamp << "ms] Window "<<wnd.getWindowProperties().title <<" WindowMove posx=" << posx << " posy=" << posy << std::endl;
	};
	window1.setCallbackWindowMove(callbackWindowMove);
	window2.setCallbackWindowMove(callbackWindowMove);
	auto callbackWindowRefresh = []( Window& wnd, uint64_t timestamp) {
		std::cout << "[" << timestamp << "ms] Window "<<wnd.getWindowProperties().title <<" WindowRefresh " << std::endl;
	};
	window1.setCallbackWindowRefresh(callbackWindowRefresh);
	window2.setCallbackWindowRefresh(callbackWindowRefresh);
	auto callbackWindowResize = []( Window& wnd, unsigned int width, unsigned int height, uint64_t timestamp) {
		std::cout << "[" << timestamp << "ms] Window "<<wnd.getWindowProperties().title <<" WindowResize width=" << width << " height=" << height << std::endl;
	};
	window1.setCallbackWindowResize(callbackWindowResize);
	window2.setCallbackWindowResize(callbackWindowResize);
	auto callbackWindowRestore = []( Window& wnd, uint64_t timestamp) {
		std::cout << "[" << timestamp << "ms] Window "<<wnd.getWindowProperties().title <<" WindowRestore " << std::endl;
	};
	window1.setCallbackWindowRestore(callbackWindowRestore);
	window2.setCallbackWindowRestore(callbackWindowRestore);


	//run both windows
	while (wicsystem.isAnyWindowOpened()) {
		if (window1.isOpened()) {
			//bind the context of window1 on this thread (only necessary when using more windows or using a window in multiple threads)
			window1.bindContext();

			//DO WORK

			//swap buffers
			window1.swapBuffers();
			//process events
			window1.processEvents();
		}
		if (window2.isOpened()) {
			//bind the context of window2 on this thread (only necessary when using more windows or using a window in multiple threads)
			window2.bindContext();

			//DO WORK

			//swap buffers
			window2.swapBuffers();
			//process events
			window2.processEvents();
		}
		//process all application events (has to be constantly called from the MAIN thread)
		wicsystem.processProgramEvents();
	}

	//window 2 destroyed by going out of scope
	//window 1 destroyed by going out of scope
	//system destroyed by going out of scope
}


//-------------------------------------------------------------------------------------------------
void testNoOpenGL(){
	std::cout << "------------------------------------------------------------\nTest No OpenGL" << std::endl;
	using namespace lap::wic;

	//create a WIC system, logging on std::cout and without thread safety (running in singlethreaded mode)
	WICSystem wicsystem(&std::cout, false);

	ContextProperties cp;
	cp.api = ContextProperties::ApiType::NONE;	//by default this is opengl 
	WindowProperties wp; FramebufferProperties fp; InputProperties ip;
	Window window = Window(wp, fp, cp, ip);
	//the output warnings are expected because the default context properties ask for opengl specific. the fields are set automatically to correct values.
	//there is no OpenGL context binded now, as there was none requested.

	//can't set the swap interval, we requested no context
	//window.setSwapInterval(-1);


	//as long as the window is opened
	while (window.isOpened()) {
		//can't bind the window context on this thread, we requested no context => there is no context
		//window.bindContext();		//this will assert, as there is no context


		//DO WORK HERE


		//can't swap buffes on a non-OpenGL window!
		//window.swapBuffers();		//this will assert, as there is no context (context operation)

		//process events
		while (const Event* e = window.processEvent()) {
			if (e->type == Event::Type::KeyPress) if (e->KeyPress.key == Key::ESCAPE) window.close();
			if (e->type == Event::Type::MouseMove) std::cout << "Mouse move at " << e->MouseMove.position_x << " " << e->MouseMove.position_y << std::endl;
		}

		//process all application events (has to be constantly called from the MAIN thread)
		wicsystem.processProgramEvents();
	};

	//window destroyed by going out of scope
	//system destroyed by going out of scope
}


//-------------------------------------------------------------------------------------------------
void testSharedWindowsAndMonitor() {
	std::cout << "------------------------------------------------------------\nTest Shared Windows And Monitor" << std::endl;
	using namespace lap::wic;

	//create a WIC system, logging on std::cout and without thread safety (running in singlethreaded mode)
	WICSystem wicsystem(&std::cout, false);

	//monitor data
	const Monitor* monitor= wicsystem.getMonitorPrimary();
	std::cout << "Monitor name = " << monitor->name << std::endl;
	std::cout << "Monitor physical size in mm = " << monitor->physical_sizex << " x " << monitor->physical_sizey << std::endl;
	std::cout << "Monitor position in virtual screen coords = " << monitor->position_x << " , " << monitor->position_y << std::endl;
	std::cout << "Monitor supported modes: " << std::endl;
	for (auto &mode : monitor->modes) {
		std::cout << mode.width << "x" << mode.height << "@" << mode.refreshrate << "hz R" << mode.redbits << " G" << mode.redbits << " B" << mode.bluebits << std::endl;
	}
	const MonitorMode& currentmode = monitor->mode_current;
	std::cout << "Monitor running in current mode = " << currentmode.width << "x" << currentmode.height << "@" << currentmode.refreshrate << "hz R" << currentmode.redbits << " G" << currentmode.redbits << " B" << currentmode.bluebits << std::endl;
	std::cout << "Monitor gamma ramp" << std::endl;
	const MonitorGammaRamp& gammaramp = monitor->gamma_ramp;
	for (unsigned int i = 0; i < gammaramp.size; i++) {
		std::cout << "(" << gammaramp.red[i] << "," << gammaramp.green[i] << "," << gammaramp.blue[i]<<") ";
	}
	std::cout << std::endl;

	//create two windows
	WindowProperties wp1, wp2;
	wp1.position_x = 100; wp1.position_y = 100;	wp1.width = 500; wp1.height = 400; wp1.title = "1st";
	wp2.position_x = 900; wp2.position_y = 100;	wp2.width = 500; wp2.height = 400; wp2.title = "2nd";
	FramebufferProperties fp; ContextProperties cp; InputProperties ip;
	Window window1 = Window(wp1, fp, cp, ip);
	// the window1 OpenGL context is now binded on this thread
	// create window2 by explicitly requesting window creation on "monitor" (by default it is the primary monitor) 
	// and to share OpenGL lists with the context on window 1 (useful for streaming from different threads for example)
	Window window2 = Window(wp2, fp, cp, ip, monitor, &window1);
	// the window2 OpenGL context is now binded on this thread
	auto keypress = []( Window& wnd, Key key, bool, bool, bool, bool, const InputState&, uint64_t) {
		std::cout << "Window " << wnd.getWindowProperties().title << " key pressed = " << (int)key << std::endl; 
	};
	window1.setCallbackKeyPress(keypress);
	window2.setCallbackKeyPress(keypress);

	//as long as any window is opened
	while (wicsystem.isAnyWindowOpened()) {
		if (window1.isOpened()) {
			//bind the context of window1 on this thread (only necessary when using more windows or using a window in multiple threads)
			window1.bindContext();

			//DO WORK HERE

			//swap buffers
			window1.swapBuffers();

			//process events with callbacks
			window1.processEvents();
		}
		if (window2.isOpened()) {
			//bind the context of window2 on this thread (only necessary when using more windows or using a window in multiple threads)
			window2.bindContext();

			//DO WORK HERE

			//swap buffers
			window2.swapBuffers();

			//process events with callbacks
			window2.processEvents();
		}

		//process all application events (has to be constantly called from the MAIN thread)
		wicsystem.processProgramEvents();
	};

	//window 2 and its OpenGL context destroyed by going out of scope
	//window 1 and its OpenGL context destroyed by going out of scope
	//system destroyed by going out of scope
}

//-------------------------------------------------------------------------------------------------
void testMultithreadedWindows() {
	std::cout << "------------------------------------------------------------\nTest Multithreaded Windows" << std::endl;
	using namespace lap::wic;

	//create a WIC system, logging on std::cout and with thread safety (running in multithreaded mode)
	WICSystem wicsystem(&std::cout, true);

	auto keypress = []( Window& wnd, Key key, bool, bool, bool, bool, const InputState&, uint64_t) {
		std::cout << "Window " << wnd.getWindowProperties().title << " key pressed = " << (int)key << std::endl;
	};
	//flag for the main thread loop, to make it run till at least a window is created
	bool waiting_for_window_creation = true;
	auto threadfunc = [&](int id) {
		WindowProperties wp;
		wp.title = std::to_string(id);
		FramebufferProperties fp; ContextProperties cp; InputProperties ip;
		Window window = Window(wp, fp, cp, ip);
		waiting_for_window_creation =false; //already guaranteed entrance in main loop
		//context is now binded

		window.setCallbackKeyPress(keypress);
		while (window.isOpened()) {
			
			//DO WORK HERE

			//swap buffers
			window.swapBuffers();

			//process events
			window.processEvents();
		}

		//window destroyed by going out of scope
	};
	std::vector<std::thread> threads;
	for (int i = 0; i < 10; i++) threads.emplace_back(std::thread(threadfunc, i));


	// Here the while condition has to guarantee that windows created by threadfunc are created
	// otherwise the while loop in main would finish before threadfunc reaches window creation and then
	// threadfunc would block at window creation because the main thread would exit the while loop
	// and stop processing program events (used in window creation). Unfortunately program event 
	// processing can be done only on the main thread (forced by platform windowing design), thus 
	// it is critical that the main thread always processes program events and is closed only when 
	// all window related execution has finished.
	// In this case one window is sufficient to keep the main loop running.
	// NOTE: compared to other windowing toolkits lapwic does not completely hijack the main thread 
	// like glut and permits window creation and handling from any thread, the constant event processing
	// from the main thread was the least instrusive compromise towards reaching these goals.
	while (wicsystem.isAnyWindowOpened() || waiting_for_window_creation) {

		//main thread work goes here

		//process all application events (has to be constantly called from the MAIN thread)
		wicsystem.processProgramEvents();
	};

	for (auto& t : threads)t.join();

	//windows and their OpenGL contexts destroyed by going out of scope
	//system destroyed by going out of scope
}


//-------------------------------------------------------------------------------------------------
void testMultithreadedWindowsStressTest() {
	std::cout << "------------------------------------------------------------\nTest Multithreaded Windows Stress Test" << std::endl;
	using namespace lap::wic;

	//create a WIC system, logging on std::cout and with thread safety (running in multithreaded mode)
	WICSystem wicsystem(&std::cout, true);

	//callbacks
	auto callbackControllerConnect = [](unsigned int id, uint64_t timestamp) {
		std::cout << "[" << timestamp << "ms] ControllerConnect : " << id << std::endl;
	};
	auto callbackControllerDisconnect = [](unsigned int id, uint64_t timestamp) {
		std::cout << "[" << timestamp << "ms] ControllerDisconnect : " << id << std::endl;
	};
	auto callbackFilesDrop = []( Window& wnd, const std::vector<std::string>& paths, uint64_t timestamp) {
		std::cout << "[" << timestamp << "ms] Window " << wnd.getWindowProperties().title << " FilesDrop : "; for (auto& p : paths) std::cout << p << std::endl;
	};
	auto callbackFramebufferResize = []( Window& wnd, unsigned int width, unsigned int height, uint64_t timestamp) {
		std::cout << "[" << timestamp << "ms] Window " << wnd.getWindowProperties().title << " FramebufferResize : width=" << width << " height=" << height << std::endl;
	};
	auto callbackKeyPress = []( Window& wnd, Key key, bool alt, bool control, bool shift, bool system, const InputState& state, uint64_t timestamp) {
		std::cout << "[" << timestamp << "ms] Window " << wnd.getWindowProperties().title << " Key Pressed : key = " << (int)key << " alt=" << alt << " control=" << control << " shift=" << shift << " system=" << system << std::endl;
	};
	auto callbackKeyRelease = []( Window& wnd, Key key, bool alt, bool control, bool shift, bool system, const InputState& state, uint64_t timestamp) {
		std::cout << "[" << timestamp << "ms] Window " << wnd.getWindowProperties().title << " Key Released : key = " << (int)key << " alt=" << alt << " control=" << control << " shift=" << shift << " system=" << system << std::endl;
	};
	auto callbackKeyRepeat = []( Window& wnd, Key key, bool alt, bool control, bool shift, bool system, const InputState& state, uint64_t timestamp) {
		std::cout << "[" << timestamp << "ms] Window " << wnd.getWindowProperties().title << " Key Repeat : key = " << (int)key << " alt=" << alt << " control=" << control << " shift=" << shift << " system=" << system << std::endl;
	};
	auto callbackMonitorConnect = [](const Monitor& monitor, uint64_t timestamp) {
		std::cout << "[" << timestamp << "ms] MonitorConnect : " << monitor.handle << std::endl;
	};
	auto callbackMonitorDisconnect = [](const Monitor& monitor, uint64_t timestamp) {
		std::cout << "[" << timestamp << "ms] MonitorDisconnect : " << monitor.handle << std::endl;
	};
	auto callbackMousePress = []( Window& wnd, MouseButton button, unsigned int posx, unsigned int posy, bool alt, bool control, bool shift, bool system, const InputState& state, uint64_t timestamp) {
		std::cout << "[" << timestamp << "ms] Window " << wnd.getWindowProperties().title << " MousePress : button = " << (int)button << " posx=" << posx << " posy=" << posy << " alt=" << alt << " control=" << control << " shift=" << shift << " system=" << system << std::endl;
	};
	auto callbackMouseRelease = []( Window& wnd, MouseButton button, unsigned int posx, unsigned int posy, bool alt, bool control, bool shift, bool system, const InputState& state, uint64_t timestamp) {
		std::cout << "[" << timestamp << "ms] Window " << wnd.getWindowProperties().title << " MouseRelease : button = " << (int)button << " posx=" << posx << " posy=" << posy << " alt=" << alt << " control=" << control << " shift=" << shift << " system=" << system << std::endl;
	};
	auto callbackMouseDrag = []( Window& wnd, MouseButton button, unsigned int posx, unsigned int posy, bool alt, bool control, bool shift, bool system, const InputState& state, uint64_t timestamp) {
		std::cout << "[" << timestamp << "ms] Window " << wnd.getWindowProperties().title << " MouseDrag : button = " << (int)button << " posx=" << posx << " posy=" << posy << " alt=" << alt << " control=" << control << " shift=" << shift << " system=" << system << std::endl;
	};
	auto callbackMouseEnter = []( Window& wnd, uint64_t timestamp) {
		std::cout << "[" << timestamp << "ms] Window " << wnd.getWindowProperties().title << " MouseEnter " << std::endl;
	};
	auto callbackMouseLeave = []( Window& wnd, uint64_t timestamp) {
		std::cout << "[" << timestamp << "ms] Window " << wnd.getWindowProperties().title << " MouseLeave " << std::endl;
	};
	auto callbackMouseMove = []( Window& wnd, unsigned int posx, unsigned int posy, const InputState& state, uint64_t timestamp) {
		std::cout << "[" << timestamp << "ms] Window " << wnd.getWindowProperties().title << " MouseMove posx=" << posx << " posy=" << posy << std::endl;
	};
	auto callbackMouseScroll = []( Window& wnd, float scrollx, float scrolly, unsigned int posx, unsigned int posy, bool alt, bool control, bool shift, bool system, const InputState& state, uint64_t timestamp) {
		std::cout << "[" << timestamp << "ms] Window " << wnd.getWindowProperties().title << " MouseScroll : scrollx= " << scrollx << " scrolly=" << scrolly << " posx=" << posx << " posy=" << posy << " alt=" << alt << " control=" << control << " shift=" << shift << " system=" << system << std::endl;
	};
	auto callbackUnicodeChar = []( Window& wnd, unsigned int codepoint, const InputState& state, uint64_t timestamp) {
		std::cout << "[" << timestamp << "ms] Window " << wnd.getWindowProperties().title << " UnicodeChar " << codepoint << std::endl;
	};
	auto callbackUnicodeCharMods = []( Window& wnd, unsigned int codepoint, bool alt, bool control, bool shift, bool system, const InputState& state, uint64_t timestamp) {
		std::cout << "[" << timestamp << "ms] Window " << wnd.getWindowProperties().title << " UnicodeCharMods : codepoint = " << codepoint << " alt=" << alt << " control=" << control << " shift=" << shift << " system=" << system << std::endl;
	};
	auto callbackWindowClose = []( Window& wnd, uint64_t timestamp) {
		std::cout << "[" << timestamp << "ms] Window " << wnd.getWindowProperties().title << " WindowClose " << std::endl;
	};
	auto callbackWindowFocusGain = []( Window& wnd, uint64_t timestamp) {
		std::cout << "[" << timestamp << "ms] Window " << wnd.getWindowProperties().title << " WindowFocusGain " << std::endl;
	};
	auto callbackWindowFocusLose = []( Window& wnd, uint64_t timestamp) {
		std::cout << "[" << timestamp << "ms] Window " << wnd.getWindowProperties().title << " WindowFocusLose " << std::endl;
	};
	auto callbackWindowMinimize = []( Window& wnd, uint64_t timestamp) {
		std::cout << "[" << timestamp << "ms] Window " << wnd.getWindowProperties().title << " WindowMinimize " << std::endl;
	};
	auto callbackWindowMaximize = []( Window& wnd, uint64_t timestamp) {
		std::cout << "[" << timestamp << "ms] Window " << wnd.getWindowProperties().title << " WindowMaximize " << std::endl;
	};
	auto callbackWindowMove = []( Window& wnd, unsigned int posx, unsigned int posy, uint64_t timestamp) {
		std::cout << "[" << timestamp << "ms] Window " << wnd.getWindowProperties().title << " WindowMove posx=" << posx << " posy=" << posy << std::endl;
	};
	auto callbackWindowRefresh = []( Window& wnd, uint64_t timestamp) {
		std::cout << "[" << timestamp << "ms] Window " << wnd.getWindowProperties().title << " WindowRefresh " << std::endl;
	};
	auto callbackWindowResize = []( Window& wnd, unsigned int width, unsigned int height, uint64_t timestamp) {
		std::cout << "[" << timestamp << "ms] Window " << wnd.getWindowProperties().title << " WindowResize width=" << width << " height=" << height << std::endl;
	};
	auto callbackWindowRestore = []( Window& wnd, uint64_t timestamp) {
		std::cout << "[" << timestamp << "ms] Window " << wnd.getWindowProperties().title << " WindowRestore " << std::endl;
	};

	//flag for the main thread loop, to make it run till at least a window is created
	bool waiting_for_window_creation = true;
	auto threadfunc = [&](int id) {
		//need this otherwise main will exit
		WindowProperties wphidden; wphidden.visible = false;
		FramebufferProperties fp; ContextProperties cp; InputProperties ip;
		Window windowhidden = Window(wphidden, fp, cp, ip);

		WindowProperties wp;
		wp.title = std::to_string(id);
		for (int i = 0; i < 5; i++) {
			auto time_start = std::chrono::high_resolution_clock::now();
			Window* window = new Window(wp, fp, cp, ip);
			waiting_for_window_creation = false; //already guaranteed entrance in main loop
										//context is now binded

			window->setCallbackControllerConnect(callbackControllerConnect);
			window->setCallbackControllerDisconnect(callbackControllerDisconnect);
			window->setCallbackFilesDrop(callbackFilesDrop);
			window->setCallbackFramebufferResize(callbackFramebufferResize);
			window->setCallbackKeyPress(callbackKeyPress);
			window->setCallbackKeyRelease(callbackKeyRelease);
			window->setCallbackKeyRepeat(callbackKeyRepeat);
			window->setCallbackMonitorConnect(callbackMonitorConnect);
			window->setCallbackMonitorDisconnect(callbackMonitorDisconnect);
			window->setCallbackMousePress(callbackMousePress);
			window->setCallbackMouseRelease(callbackMouseRelease);
			window->setCallbackMouseDrag(callbackMouseDrag);
			window->setCallbackMouseEnter(callbackMouseEnter);
			window->setCallbackMouseLeave(callbackMouseLeave);
			window->setCallbackMouseMove(callbackMouseMove);
			window->setCallbackMouseScroll(callbackMouseScroll);
			window->setCallbackUnicodeChar(callbackUnicodeChar);
			window->setCallbackUnicodeCharMods(callbackUnicodeCharMods);
			window->setCallbackWindowClose(callbackWindowClose);
			window->setCallbackWindowFocusGain(callbackWindowFocusGain);
			window->setCallbackWindowFocusLose(callbackWindowFocusLose);
			window->setCallbackWindowMinimize(callbackWindowMinimize);
			window->setCallbackWindowMaximize(callbackWindowMaximize);
			window->setCallbackWindowMove(callbackWindowMove);
			window->setCallbackWindowRefresh(callbackWindowRefresh);
			window->setCallbackWindowResize(callbackWindowResize);
			window->setCallbackWindowRestore(callbackWindowRestore);
			while (window->isOpened()) {
				int64_t elapsed_seconds = std::chrono::duration_cast<std::chrono::seconds>(std::chrono::high_resolution_clock::now() - time_start).count();
				if (elapsed_seconds > 1) break;

				//swap buffers
				window->swapBuffers();

				//process events
				window->processEvents();
			}
			delete window;
		}

		//wphidden destroyed by going out of scope
	};
	std::vector<std::thread> threads;
	for (int i = 0; i < 30; i++) threads.emplace_back(std::thread(threadfunc, i));


	// Here the while condition has to guarantee that windows created by threadfunc are created
	// otherwise the while loop in main would finish before threadfunc reaches window creation and then
	// threadfunc would block at window creation because the main thread would exit the while loop
	// and stop processing program events (used in window creation). Unfortunately program event 
	// processing can be done only on the main thread (forced by depedencies), thus it is critical 
	// that the main thread always processes program events and is closed only when all window 
	// related execution has finished.
	// In this case one window is sufficient to keep the main loop running.
	// NOTE: compared to other windowing toolkits lapwic does not hijack the main thread like glut
	// and permits window creation and handling from any thread, the constant event processing from
	// the main thread was the least instrusive compromise towards reaching these goals.
	while (wicsystem.isAnyWindowOpened() || waiting_for_window_creation) {

		//main thread work goes here

		//process all application events (has to be constantly called from the MAIN thread)
		wicsystem.processProgramEvents();
	};

	for (auto& t : threads)t.join();

	//wicsystem destroyed by going out of scope
}

//-------------------------------------------------------------------------------------------------
void testSingleThreadedManyContexts() {
#ifdef LAP_WIC_SUPPRESS_GL_EXTENSIONS
	std::cout << "------------------------------------------------------------\nTest Single Threaded Many Contexts : SKIPPED , OpenGL function loading disabled" << std::endl;
#else
	std::cout << "------------------------------------------------------------\nTest Single Threaded Many Contexts" << std::endl;
	using namespace lap::wic;

	//create a WIC system, logging on std::cout and without thread safety (running in singlethreaded mode)
	WICSystem wicsystem(&std::cout, false);

	ContextProperties cp;
	cp.version_major = 4; cp.version_minor = 5;
	std::vector<Window*> window; window.resize(5);
	std::vector<GLuint> shader;  shader.resize(window.size());
	std::vector<GLuint> vao;	 vao.resize(window.size());
	std::vector<GLuint> vbo;	 vbo.resize(window.size());

	for (unsigned int i = 0; i < window.size(); i++) {
		WindowProperties wp; FramebufferProperties fp; InputProperties ip;
		window[i] = new Window(wp, fp, cp, ip);
		//context is now binded

		//a big quad in screen space
		std::vector<float> vertices = { -0.5,-0.5, -0.5, 0.5, 0.5, 0.5,  0.5, 0.5,  0.5, -0.5, -0.5, -0.5 };
		glGenVertexArrays(1, &vao[i]);
		glBindVertexArray(vao[i]);
		glGenBuffers(1, &vbo[i]);
		glBindBuffer(GL_ARRAY_BUFFER, vbo[i]);
		glBufferData(GL_ARRAY_BUFFER, sizeof(float)*vertices.size(), vertices.data(), GL_STATIC_DRAW);
		glEnableVertexAttribArray(0);
		glVertexAttribPointer(0, 2, GL_FLOAT, GL_FALSE, sizeof(float) * 2, (void*)0);

		//shader
		GLuint shader_vertex, shader_fragment;
		shader_vertex = shader_fragment = 0;
		const char* shader_vertex_source = "#version 450 \n"
			"layout (location=0) in vec2 pos;\n"
			"void main(){\n"
			"gl_Position = vec4(pos.xy, 0, 1); \n"
			"} \n";
		GLint shader_source_length = (GLint)strlen(shader_vertex_source);
		shader_vertex = glCreateShader(GL_VERTEX_SHADER);
		glShaderSource(shader_vertex, 1, &shader_vertex_source, &shader_source_length);
		glCompileShader(shader_vertex);
		const char* shader_fragment_source = "#version 450 \n"
			"layout (location=0) out vec4 color;\n"
			"uniform int id;"
			"void main(){\n"
			"color = vec4(id/5.0,0,0,0);\n"
			"} \n";
		shader_source_length = (GLint)strlen(shader_fragment_source);
		shader_fragment = glCreateShader(GL_FRAGMENT_SHADER);
		glShaderSource(shader_fragment, 1, &shader_fragment_source, &shader_source_length);
		glCompileShader(shader_fragment);
		shader[i] = glCreateProgram();
		glAttachShader(shader[i], shader_vertex);
		glAttachShader(shader[i], shader_fragment);
		glLinkProgram(shader[i]);
		glDeleteShader(shader_vertex);
		glDeleteShader(shader_fragment);
	}

	//as long as the window is opened
	bool running = true;
	while (wicsystem.isAnyWindowOpened() && running) {
		for (unsigned int i = 0; i < window.size(); i++) {
			if (window[i]->isOpened()) {
				//bind the context
				window[i]->bindContext();

				//clear screen to blue
				glClearColor(0, 0, 1, 0);
				glClear(GL_COLOR_BUFFER_BIT);

				//6 vertices (2 triangles)
				glUseProgram(shader[i]);
				glUniform1i(glGetUniformLocation(shader[i], "id"), i);
				glBindVertexArray(vao[i]);
				glDrawArrays(GL_TRIANGLES, 0, 6);

				//swap buffers
				window[i]->swapBuffers();

				//process events
				while (const Event* e = window[i]->processEvent()) {
					if (e->type == Event::Type::KeyPress) if (e->KeyPress.key == Key::ESCAPE) {
						//example of cleanup performed on event
						glDeleteProgram(shader[i]);
						glDeleteBuffers(1, &vbo[i]);
						glDeleteVertexArrays(1, &vao[i]);
						//exit entirely
						running = false;
					}
				}
			}
		}

		//process all application events (has to be constantly called from the MAIN thread)
		wicsystem.processProgramEvents();
	};

	//windows and their OpenGL contexts destroyed by going out of scope
	//wicsystem destroyed by going out of scope
#endif
}



//-------------------------------------------------------------------------------------------------
//multiple windows with multiple context rendering concurrently
//
// The multithreading model that used by OpenGL has the following properties: 
// 1. the same OpenGL context cannot be current within multiple threads simultaneously. While you can have multiple OpenGL 
// contexts which are current in multiple threads, you cannot manipulate a single context simultaneously from two threads. 
// 2. OpenGL commands cannot be emitted concurrently from different threads without some synchronization mechanism 
// (= OpenGL is NOT threadsafe). If the user issues commands to the OpenGL driver from different threads then the 
// either the driver performs an inefficient synchronization or undefined behavior takes place!
// see https://www.opengl.org/wiki/Memory_Model
void testMultithreadedManyContexts() {
#ifdef LAP_WIC_SUPPRESS_GL_EXTENSIONS
	std::cout << "------------------------------------------------------------\nTest Multithreaded many contexts : SKIPPED , OpenGL function loading disabled" << std::endl;
#else
	std::cout << "------------------------------------------------------------\nTest Multithreaded many contexts" << std::endl;
	using namespace lap::wic;

	//create a WIC system, logging on std::cout and with thread safety (running in multithreaded mode)
	WICSystem wicsystem(&std::cout, true);

	//flag for the main thread loop, to make it run till at least a window is created
	bool waiting_for_window_creation = true;

	//OpenGL lock
	std::mutex lock_gl;
	
	auto threadfunc = [&](int id) {
		WindowProperties wp;
		wp.title = std::to_string(id);
		ContextProperties cp;
		cp.version_major = 4; cp.version_minor = 5;
		FramebufferProperties fp; InputProperties ip;
		Window window = Window(wp, fp, cp, ip);
		waiting_for_window_creation = false;

		//a big quad in screen space
		std::vector<float> vertices = { -0.5,-0.5, -0.5, 0.5, 0.5, 0.5,  0.5, 0.5,  0.5, -0.5, -0.5, -0.5 };
		GLuint vbo, vao;
		glGenVertexArrays(1, &vao);
		glBindVertexArray(vao);
		glGenBuffers(1, &vbo);
		glBindBuffer(GL_ARRAY_BUFFER, vbo);
		glBufferData(GL_ARRAY_BUFFER, sizeof(float)*vertices.size(), vertices.data(), GL_STATIC_DRAW);
		glEnableVertexAttribArray(0);
		glVertexAttribPointer(0, 2, GL_FLOAT, GL_FALSE, sizeof(float) * 2, (void*)0);
		//shader
		GLuint shader, shader_vertex, shader_fragment;
		shader = shader_vertex = shader_fragment = 0;
		const char* shader_vertex_source = "#version 450 \n"
			"layout (location=0) in vec2 pos;\n"
			"void main(){\n"
			"gl_Position = vec4(pos.xy, 0, 1); \n"
			"} \n";
		GLint shader_source_length = (GLint)strlen(shader_vertex_source);
		shader_vertex = glCreateShader(GL_VERTEX_SHADER);
		glShaderSource(shader_vertex, 1, &shader_vertex_source, &shader_source_length);
		glCompileShader(shader_vertex);
		const char* shader_fragment_source = "#version 450 \n"
			"layout (location=0) out vec4 color;\n"
			"uniform int id;"
			"uniform int time;"
			"void main(){\n"
			"if (id > 3) color = vec4(gl_FragCoord.x/800,gl_FragCoord.y/600,0,0);\n"
			"else color = vec4(fract(sin(time*0.0002) + id/4.0),0,0,0);\n"
			"} \n";
		shader_source_length = (GLint)strlen(shader_fragment_source);
		shader_fragment = glCreateShader(GL_FRAGMENT_SHADER);
		glShaderSource(shader_fragment, 1, &shader_fragment_source, &shader_source_length);
		glCompileShader(shader_fragment);
		shader = glCreateProgram();
		glAttachShader(shader, shader_vertex);
		glAttachShader(shader, shader_fragment);
		glLinkProgram(shader);
		glUseProgram(shader);
		glDeleteShader(shader_vertex);
		glDeleteShader(shader_fragment);
		glUniform1i(glGetUniformLocation(shader, "id"), id);

		//as long as the window is opened
		while (window.isOpened()){

			//acquire OpenGL 
			lock_gl.lock();

			//clear screen to blue
			glClearColor(0, 0, 1, 0);
			glClear(GL_COLOR_BUFFER_BIT);

			//update uniform
			glUseProgram(shader);
			int time = (int)(std::chrono::steady_clock::now().time_since_epoch().count() / 1000000 % 1000);
			glUniform1i(glGetUniformLocation(shader, "time"), time);

			//6 vertices (2 triangles)
			glDrawArrays(GL_TRIANGLES, 0, 6);

			//swap buffers
			window.swapBuffers();

			//release OpenGL 
			lock_gl.unlock();

			//process events
			while (const Event* e = window.processEvent()) {
				if (e->type == Event::Type::KeyPress) if (e->KeyPress.key == Key::ESCAPE) {
					//example of cleanup performed on event
					glDeleteProgram(shader);
					glDeleteBuffers(1, &vbo);
					glDeleteVertexArrays(1, &vao);
					//exit
					window.close();
				}
			}
		}
	};
	
	std::vector<std::thread> threads;
	for (int i = 0; i < 5; i++) threads.emplace_back(std::thread(threadfunc,i));
	while (wicsystem.isAnyWindowOpened() || waiting_for_window_creation) {
		//process all application events (has to be constantly called from the MAIN thread)
		wicsystem.processProgramEvents();
	};
	for (auto& t : threads)t.join();

	//windows and their OpenGL contexts destroyed by going out of scope
	//wicsystem is destroyed by going out of scope
#endif
}

//-------------------------------------------------------------------------------------------------
//multiple threads using the same window
//KEY CONCEPT:
// this is an academic example, DON'T DO THIS.
void testMultithreadedSameContext() {
#ifdef LAP_WIC_SUPPRESS_GL_EXTENSIONS
	std::cout << "------------------------------------------------------------\nTest Multithreaded Same Context : SKIPPED , OpenGL function loading disabled" << std::endl;
#else
	std::cout << "------------------------------------------------------------\nTest Multithreaded Same Context" << std::endl;
	using namespace lap::wic;

	//create a WIC system, logging on std::cout and with thread safety (running in multithreaded mode)
	WICSystem wicsystem(&std::cout, true);

	ContextProperties cp;
	cp.version_major = 4; cp.version_minor = 5;
	WindowProperties wp; FramebufferProperties fp; InputProperties ip;
	Window window = Window(wp, fp, cp, ip);

	GLuint vbo, vao;
	GLint shader;

	auto func_create = [&]() {
		//bind context
		window.bindContext();
		//a big quad in screen space
		std::vector<float> vbodata = { -0.5,-0.5, -0.5, 0.5, 0.5, 0.5,  0.5, 0.5,  0.5, -0.5, -0.5, -0.5 };
		glGenVertexArrays(1, &vao);
		glBindVertexArray(vao);
		glGenBuffers(1, &vbo);
		glBindBuffer(GL_ARRAY_BUFFER, vbo);
		glBufferData(GL_ARRAY_BUFFER, sizeof(float)*vbodata.size(), vbodata.data(), GL_STATIC_DRAW);
		glEnableVertexAttribArray(0);
		glVertexAttribPointer(0, 2, GL_FLOAT, GL_FALSE, sizeof(float) * 2, (void*)0);
		//unbind context
		window.unbindContext();
	};
	//unbind context
	window.unbindContext();
	//create and wait for creation thread to finish (thus no sync is needed)
	std::thread thread_create(func_create);
	thread_create.join();

	auto func_render = [&]() {
		//bind context
		window.bindContext();
		//shader
		GLuint shader_vertex, shader_fragment;
		shader_vertex = shader_fragment = 0;
		const char* shader_vertex_source = "#version 450 \n"
			"layout (location=0) in vec2 pos;\n"
			"void main(){\n"
			"gl_Position = vec4(pos.xy, 0, 1); \n"
			"} \n";
		GLint shader_source_length = (GLint)strlen(shader_vertex_source);
		shader_vertex = glCreateShader(GL_VERTEX_SHADER);
		glShaderSource(shader_vertex, 1, &shader_vertex_source, &shader_source_length);
		glCompileShader(shader_vertex);
		const char* shader_fragment_source = "#version 450 \n"
			"layout (location=0) out vec4 color;\n"
			"uniform int time;"
			"void main(){\n"
			"color = vec4(fract(sin(time*0.0002)),0,0,0);\n"
			"} \n";
		shader_source_length = (GLint)strlen(shader_fragment_source);
		shader_fragment = glCreateShader(GL_FRAGMENT_SHADER);
		glShaderSource(shader_fragment, 1, &shader_fragment_source, &shader_source_length);
		glCompileShader(shader_fragment);
		shader = glCreateProgram();
		glAttachShader(shader, shader_vertex);
		glAttachShader(shader, shader_fragment);
		glLinkProgram(shader);
		glUseProgram(shader);

		//as long as the window is opened
		while (window.isOpened()) {

			//clear screen to blue
			glClearColor(0, 0, 1, 0);
			glClear(GL_COLOR_BUFFER_BIT);

			//update uniform
			glUseProgram(shader);
			int time = (int)(std::chrono::steady_clock::now().time_since_epoch().count() / 1000000 % 1000);
			glUniform1i(glGetUniformLocation(shader, "time"), time);

			//6 vertices (2 triangles)
			glDrawArrays(GL_TRIANGLES, 0, 6);

			//swap buffers
			window.swapBuffers();

			//process events
			while (const Event* e = window.processEvent()) {
				if (e->type == Event::Type::KeyPress && e->KeyPress.key == Key::ESCAPE) {
					//example of on-event cleanup
					glDeleteProgram(shader);
					glDeleteBuffers(1, &vbo);
					glDeleteVertexArrays(1, &vao);
					//close window
					window.close();
				}
			}
		}
	};


	std::thread thread_render(func_render);
	while (wicsystem.isAnyWindowOpened()) {
		//process all application events (has to be constantly called from the MAIN thread)
		wicsystem.processProgramEvents();
	};
	thread_render.join();

	//windows and their OpenGL contexts destroyed by going out of scope
	//wicsystem is destroyed by going out of scope
#endif
}

int main(int argc, char* argv[]){
	testNoWindow();
	testBasicEvents();
	testBasicFullscreen();
	testBasicCallbacks();
	testRemapper();
	testBasicMultipleWindows();
	testBasicOpenGL();
	testOpenGLextensions();
	testBasicOpenGLProfileFlags();
	testBasicOpenGLSharedContext();
	testWindowProperties();
	testAllCallbacks();
	testAllEvents();
	testNoOpenGL();
	testSharedWindowsAndMonitor();
	testMultithreadedWindows();
	testMultithreadedWindowsStressTest();
	testSingleThreadedManyContexts();
	testMultithreadedManyContexts();
	testMultithreadedSameContext();
	
	std::cout << "Finished" << std::endl;
	std::cin.get();
	return 0;
}
