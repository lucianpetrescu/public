#-------------------------------------------------------------------------------------------------------------------------------
# The MIT License (MIT)
#
# Copyright (c) 2016 Lucian Petrescu, lucian (dot) petrescu (dot) 24 (at) gmail (dot) com
# 
# Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation
# files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy,
# modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software
# is furnished to do so, subject to the following conditions:
# 
# The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
# OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
# LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR
# IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
#-------------------------------------------------------------------------------------------------------------------------------

#-------------------------------------------------------------------------------------------------------------------------------
# a generator for version 320 of GLFW
# 
# This function parses the glfw and glad folders and creates a single file, used by lap_wic. It behaves like a C compiler.
# By using this technique, lap_wic can be deployed with only 3 files : lap_wic.hpp, lap_wic_dep.hpp, and lap_wic.cpp, no libs,
# no dlls, no branching/recurring dependencies, etc. Plug and play.
# The resuling file contains the slightly modified GLAD (https://github.com/Dav1dde/glad) and GLFW (http://www.glfw.org/) libraries.
# (GLFW is written for a C compiler, thus a CPP compiler will throw errors on things like char* letters = calloc(100))
#
# Note: this generator works with GLAD OpenGL 4.5 compatibility and GLFW 3.2.
# Note: the packed result exposes only the GLAD interface (GL functions which the user wants), all the GLFW code stays only
# in the implementation, which will never be exposed by the library, thus lap_wic enables cleaner coding and fast compilation times.
#
# Usage: python generator.py
#-------------------------------------------------------------------------------------------------------------------------------

import os.path

gl_as_functions = True
#gl_as_functions = False
    
#create targets
target = open('../lap_wic_dep.hpp','w+')

#----------------------------------------------------------------------------------------------------
#                                             HEADER
#----------------------------------------------------------------------------------------------------
target.write("""///---------------------------------------------------------------------------------------------------------------------
/// The MIT License (MIT)
///
/// Copyright (c) 2016 Lucian Petrescu, lucian (dot) petrescu (dot) 24 (at) gmail (dot) com
/// 
/// Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation 
/// files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, 
/// modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software 
/// is furnished to do so, subject to the following conditions:
/// 
/// The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
/// 
/// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
/// OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE 
/// LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR 
/// IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
///---------------------------------------------------------------------------------------------------------------------

///---------------------------------------------------------------------------------------------------------------------
/// This file contains the modified GLAD (https://github.com/Dav1dde/glad) and GLFW 3.2 (http://www.glfw.org/) libraries.
/// This file is automatically generated.
///---------------------------------------------------------------------------------------------------------------------
""")

#----------------------------------------------------------------------------------------------------
#                                             GLFW
#----------------------------------------------------------------------------------------------------
target.write("#ifdef LAP_WIC_DEP_IMPLEMENTATION \n")

#detect platform and set GLFW options
target.write("""
#if defined(_WIN32) || defined(_WIN64) || defined(__CYGWIN__) || defined(__MINGW32__) || defined(__MINGW64__)
    #define GLFW_WINDOWS
#elif defined(__unix__) || defined(__unix) || defined(__linux__) || defined(__linux)
    #define GLFW_NIX
#elif defined(__APPLE__)
	#include "TargetConditionals.h"
    #if TARGET_IPHONE_SIMULATOR
         #error "Iphone simulator not supported"
    #elif TARGET_OS_IPHONE
        #error "Iphone not supported"
    #elif TARGET_OS_MAC
        #define GLFW_APPLE
    #else
    #   error "Unknown Apple platform"
    #endif
#else
    #error "Unknown platform"
#endif

#ifdef GLFW_WINDOWS
	#define _GLFW_WIN32 1
	#define _GLFW_WGL 1
	//creates the lib by exporting the symbols, and also seems to not work..
	//#define _GLFW_USE_HYBRID_HPG 1
#endif

#ifdef GLFW_NIX
	#define _GLFW_X11 1
	#define _GLFW_GLX 1
	#define _GLFW_HAS_XINPUT 1
	#define _GLFW_HAS_XF86VM 1
#endif

#ifdef GLFW_APPLE
	#define _GLFW_COCOA 1
	#define _GLFW_NSGL 1
	#define _GLFW_USE_CHDIR 1
	// #define _GLFW_USE_MENUBAR 1
	#define _GLFW_USE_RETINA 1
#endif

#ifdef _MSC_VER
#ifndef _CRT_SECURE_NO_WARNINGS
#define _CRT_SECURE_NO_WARNINGS
#endif
#endif
""")

#parse with a dictionary, like a C compiler, furthermore the GLFW parts will only reach the lap_wic.cpp translation unit
files_loaded ={}
files_exceptions = {"posix_tls.h", "posix_tls.c"}

def ParseAndWrite(file):
    if file not in files_loaded or file in files_exceptions:
        files_loaded[file]=1
        file = open("glfw/3.2.0/"+file,'r')
        for line in file:
            if not '#include "' in line:
                target.write(line)
            else:
                #eliminate wayland and mir support (they are experimental in GLFW ..)
                if "wl_" not in line and "mir_" not in line:
                    ParseAndWrite(line.split()[1].replace('"',""))
    

#glfw headers
ParseAndWrite('internal.h')

#core
ParseAndWrite('context.c')
ParseAndWrite('init.c')
ParseAndWrite('input.c')
ParseAndWrite('monitor.c')
ParseAndWrite('vulkan.c')
ParseAndWrite('window.c')


#windows
target.write("#ifdef GLFW_WINDOWS \n")
ParseAndWrite('win32_platform.h')
ParseAndWrite('win32_joystick.h')
ParseAndWrite('wgl_context.h')
ParseAndWrite('win32_init.c')
ParseAndWrite('win32_joystick.c')
ParseAndWrite('win32_monitor.c')
ParseAndWrite('win32_time.c')
ParseAndWrite('win32_tls.c')
ParseAndWrite('win32_window.c')
ParseAndWrite('wgl_context.c')
target.write("""
#endif
#ifdef _MSC_VER
#undef _CRT_SECURE_NO_WARNINGS
#endif
""")

#nix
target.write("#ifdef GLFW_NIX \n")
ParseAndWrite('x11_platform.h')
ParseAndWrite('xkb_unicode.h')
ParseAndWrite('linux_joystick.h')
ParseAndWrite('posix_time.h')
ParseAndWrite('glx_context.h')
ParseAndWrite('posix_tls.h')
ParseAndWrite('x11_init.c')
ParseAndWrite('x11_monitor.c')
ParseAndWrite('x11_window.c')
ParseAndWrite('xkb_unicode.c')
ParseAndWrite('linux_joystick.c')
ParseAndWrite('posix_time.c')
ParseAndWrite('posix_tls.c')
ParseAndWrite('glx_context.c')
target.write("#endif \n")

#apple
target.write("#ifdef GLFW_APPLE \n")
ParseAndWrite('cocoa_platform.h')
ParseAndWrite('cocoa_joystick.h')
ParseAndWrite('posix_tls.h')
ParseAndWrite('nsgl_context.h')
ParseAndWrite('cocoa_init.m')
ParseAndWrite('cocoa_joystick.m')
ParseAndWrite('cocoa_monitor.m')
ParseAndWrite('cocoa_window.m')
ParseAndWrite('cocoa_time.c')
ParseAndWrite('posix_tls.c')
ParseAndWrite('nsgl_context.m')
target.write("#endif \n")



#cleanup after GLFW
target.write("""
//cleanup after  xlib macros
#undef KeyPress
#undef KeyRelease

//cleanup GLFW
#undef GL_VERSION
#undef GL_NONE
#undef GL_COLOR_BUFFER_BIT	
#undef GL_EXTENSIONS 
#undef GL_NUM_EXTENSIONS 
#undef GL_CONTEXT_FLAGS 
#undef GL_CONTEXT_FLAG_FORWARD_COMPATIBLE_BIT 
#undef GL_CONTEXT_FLAG_DEBUG_BIT 
#undef GL_CONTEXT_PROFILE_MASK 
#undef GL_CONTEXT_COMPATIBILITY_PROFILE_BIT 
#undef GL_CONTEXT_CORE_PROFILE_BIT 
#undef GL_RESET_NOTIFICATION_STRATEGY_ARB 
#undef GL_LOSE_CONTEXT_ON_RESET_ARB 
#undef GL_NO_RESET_NOTIFICATION_ARB 
#undef GL_CONTEXT_RELEASE_BEHAVIOR 
#undef GL_CONTEXT_RELEASE_BEHAVIOR_FLUSH 
#undef GL_CONTEXT_FLAG_NO_ERROR_BIT_KHR
""")

target.write("#endif   //LAP_WIC_DEP_IMPLEMENTATION \n")



#----------------------------------------------------------------------------------------------------
#                                             GLAD
#----------------------------------------------------------------------------------------------------

target.write("#ifndef LAP_WIC_SUPPRESS_GL_EXTENSIONS\n")
#expose the interface to a different file (cleaner inclusion design)
file = open('glad/glad_gl45comp.h','r')
for line in file:

    #if generating functions don't generate macros
    if gl_as_functions:
        if "#define gl" in line or "GLAD_GL_VERSION" in line:
            continue
    if '#include "khrplatform.h"' in line:
        target.write(open('glad/khrplatform.h','r').read())
    else:
        #if generating functions name them directly
        if gl_as_functions:
            target.write(line.replace("GLAD_GL","GL").replace("glad_gl","gl"))
        else:
            target.write(line)

#glad implementation not in interface
target.write("#ifdef LAP_WIC_DEP_IMPLEMENTATION\n")
file = open('glad/glad_gl45comp.c','r')
for line in file:
    if not '#include "glad_gl45comp.h"' in line:
        #if generating functions name them directly, also get rid of all statics
        if gl_as_functions:
            target.write(line.replace("glad_gl","gl").replace("static",""))
        else:
            target.write(line)
target.write("#endif   //LAP_WIC_DEP_IMPLEMENTATION \n")

target.write("#endif   //LAP_WIC_SUPPRESS_GL_EXTENSIONS \n")



#done
target.close()
