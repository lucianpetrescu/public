//----------------------------------------------------------------
// Usage
// - include this header to include entire glfw
// - define GLFW_IMPLEMENTATION in a SINGLE file (to generate the implementation there)
// - discarded Mir, wayland, use X11
//----------------------------------------------------------------


#ifndef GLFW_H_
#define GLFW_H_

//----------------------------------------------------------------
// GLFW platform detection
//----------------------------------------------------------------
#if defined(_WIN32) || defined(_WIN64) || defined(__CYGWIN__) || defined(__MINGW32__) || defined(__MINGW64__)
	#define GLFW_WINDOWS
#elif defined(__unix__) || defined(__unix) || defined(__linux__) || defined(__linux)
	#define GLFW_NIX
#elif defined(__APPLE__)
	#include "TargetConditionals.h"
    #if TARGET_IPHONE_SIMULATOR
         #error "Iphone simulator not supported"
    #elif TARGET_OS_IPHONE
        #error "Iphone not supported"
    #elif TARGET_OS_MAC
        #define GLFW_APPLE
    #else
    #   error "Unknown Apple platform"
    #endif
#else
    #error "Unknown platform"
#endif


//----------------------------------------------------------------
// Use platform detection de select the defines for GLFW
//----------------------------------------------------------------
#ifdef GLFW_WINDOWS
	#define _GLFW_WIN32 1
	#define _GLFW_WGL 1
	//creates the lib by exporting the symbols, and also seems to not work..
	//#define _GLFW_USE_HYBRID_HPG 1
#endif

#ifdef GLFW_NIX
	#define _GLFW_X11 1
	#define _GLFW_GLX 1
	#define _GLFW_HAS_XINPUT 1
	#define _GLFW_HAS_XF86VM 1
#endif

#ifdef GLFW_APPLE
	#define _GLFW_COCOA 1
	#define _GLFW_NSGL 1
	#define _GLFW_USE_CHDIR 1
	// #define _GLFW_USE_MENUBAR 1
	#define _GLFW_USE_RETINA 1
#endif

//----------------------------------------------------------------
// Headers and implementation
//----------------------------------------------------------------

#ifdef _MSC_VER
#ifndef _CRT_SECURE_NO_WARNINGS
#define _CRT_SECURE_NO_WARNINGS
#endif
#endif

//core
#include "internal.h"	//automatically includes glfw3.h
#ifdef GLFW_IMPLEMENTATION
	#include "context.c"
	#include "init.c"
	#include "input.c"
	#include "monitor.c"
	#include "vulkan.c"
	#include "window.c"
#endif

//windows
#ifdef GLFW_WINDOWS
	#include "win32_platform.h"
	#include "win32_joystick.h"
	#include "wgl_context.h"
	#ifdef GLFW_IMPLEMENTATION
		#include "win32_init.c"
		#include "win32_joystick.c"
		#include "win32_monitor.c"
		#include "win32_time.c"
		#include "win32_tls.c"
		#include "win32_window.c"
		#include "wgl_context.c"
	#endif
#endif

#ifdef _MSC_VER
#undef _CRT_SECURE_NO_WARNINGS
#endif

//nix
#ifdef GLFW_NIX
	#include "x11_platform.h"
	#include "xkb_unicode.h"
	#include "linux_joystick.h"
	#include "posix_time.h"
	#include "posix_tls.h"
	#include "glx_context.h"
	#ifdef GLFW_IMPLEMENTATION
		#include "x11_init.c"
		#include "x11_monitor.c"
		#include "x11_window.c"
		#include "xkb_unicode.c"
		#include "linux_joystick.c"
		#include "posix_time.c"
		#include "posix_tls.c"
		#include "glx_context.c"
	#endif
#endif

//apple
#ifdef GLFW_APPLE
	#include "cocoa_platform.h"
	#include "cocoa_joystick.h"
	#include "posix_tls.h"
	#include "nsgl_context.h"
	#ifdef GLFW_IMPLEMENTATION
		#include "cocoa_init.m"
		#include "cocoa_joystick.m"
		#include "cocoa_monitor.m"
		#include "cocoa_window.m"
		#include "cocoa_time.c"
		#include "posix_tls.c"
		#include "nsgl_context.m"
	#endif
#endif


#endif