﻿///---------------------------------------------------------------------------------------------------------------------
/// The MIT License (MIT)
/// 
/// Copyright (c) 2016 Lucian Petrescu, lucian (dot) petrescu (dot) 24 (at) gmail (dot) com
/// 
/// Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation 
/// files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, 
/// modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software 
/// is furnished to do so, subject to the following conditions:
/// 
/// The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
/// 
/// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
/// OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE 
/// LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR 
/// IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
///---------------------------------------------------------------------------------------------------------------------

#ifndef LAP_WIC_CONFIG_H_
#define LAP_WIC_CONFIG_H_

//   EXTENSIONS
//define the next line to include OpenGL tokens and core function extension loading and management (comment next line if you want to use another extension loading library)
#define LAP_WIC_INCLUDE_GL_EXTENSIONS


//   NIX SPECIFIc
//define the next line to force Wayland usage on linux, default is X11
//#define LAP_WIC_FORCE_NIX_WAYLAND
//define the next line to force Mir usage on linux, default is X11
//#define LAP_WIC_FORCE_NIX_MIR


//   VULKAN
//define the next line to include Vulkan functions and tokens, otherwise you'll need to do it manually, e.g. #include <path/to/vulkan.h>
//NOTE: this is equivalent with #include <vulkan/vulkan.h>, and requires the inclusion of the Vulkan SDK into the project
//#define LAP_WIC_INCLUDE_VULKAN
//define the next line to use Vulkan loader linked statically into application (only works if LAP_WIC_INCLUDE_VULKAN is defined)
//#define LAP_WIC_VULKAN_STATIC

#endif