###########################################################################################################################################
#													CONTENTS
LIBRARIES = lap_debug_tools\
			lap_font_tools\
			lap_gl_debug\
			lap_gpu_program\
			lap_gpu_timer\
			lap_image\
			lap_logger\
			lap_timer\
			lap_wic\

#make can barely handle spaces, and only in wildcards, so we use substitution wildcards to get around this. 
#Note the "\" before each space in the original LIBRARIES list.
#Note2: this is not needed with the current nomenclature
spaceto? = $(subst \ ,?,$1)
?tospace = $(subst ?,\ ,$1)			
LIBRARIES := $(call spaceto?,$(LIBRARIES))
			
define \n


endef


###########################################################################################################################################
#													BUILD TARGETS
#make might confuse "all" "clean" "distclean" etc targets for actual filenames, PHONY is used to prevent this confusion.
.PHONY: all build debug release clean help install distclean

all: build

debug:
	@echo ------------------------------------------------------------------------------------
	@echo ------------------ Building all projects : Debug only ------------------------------
	@echo ------------------------------------------------------------------------------------
	$(foreach  x,$(LIBRARIES), @echo $(\n) @$(MAKE) debug --no-print-directory -C $(call ?tospace,$(x)) ${\n})
	@echo ------------------------------------------------------------------------------------
	@echo ------------------------------------------------------------------------------------
	@echo ------------------------------------------------------------------------------------
release:
	@echo ------------------------------------------------------------------------------------
	@echo ------------------ Building all projects : Release only ----------------------------
	@echo ------------------------------------------------------------------------------------
	$(foreach  x,$(LIBRARIES), @echo $(\n) @$(MAKE) release --no-print-directory -C $(call ?tospace,$(x)) ${\n})
	@echo ------------------------------------------------------------------------------------
	@echo ------------------------------------------------------------------------------------
	@echo ------------------------------------------------------------------------------------
build: 
	@echo ------------------------------------------------------------------------------------
	@echo ------------------ Building all projects : Debug and Release -----------------------
	@echo ------------------------------------------------------------------------------------
	$(foreach  x,$(LIBRARIES), @echo $(\n) @$(MAKE) all --no-print-directory -C $(call ?tospace,$(x)) ${\n})
	@echo ------------------------------------------------------------------------------------
	@echo ------------------------------------------------------------------------------------
	@echo ------------------------------------------------------------------------------------
clean:
	@echo ------------------------------------------------------------------------------------
	@echo --------------------------------Cleaning all projects ------------------------------
	@echo ------------------------------------------------------------------------------------
	$(foreach  x,$(LIBRARIES), @echo $(\n) @$(MAKE) clean --no-print-directory -C $(call ?tospace,$(x)) ${\n})
	@echo ------------------------------------------------------------------------------------
	@echo ------------------------------------------------------------------------------------
	@echo ------------------------------------------------------------------------------------
distclean: clean


###########################################################################################################################################
# 														HELP and INSTALL
help:
	@echo $(\n)
	@echo ---This makefile calls the makefiles of all libraries in this repository  ---
	@echo $(\n)
	@echo use -make build- or -make all- to build both debug and release executables
	@echo use -make debug- to build only in debug mode
	@echo use -make release- to build only in release mode
	@echo use -make clean- to clean all gnu/gcc related binaries
install:
	@echo nothing to install
list:
	@echo $(\n)
	@echo --- The list of libraries :  ---
	@echo $(\n)
	$(foreach  x,$(LIBRARIES), @echo $(call ?tospace,$(x)) ${\n})
