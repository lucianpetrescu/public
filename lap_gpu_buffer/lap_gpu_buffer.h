///------------------------------------------------------------------------------------------------------------------------------
/// The MIT License (MIT)
/// 
/// Copyright (c) 2016 Lucian Petrescu, lucian (dot) petrescu (dot) 24 (at) gmail (dot) com
/// 
/// Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation 
/// files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, 
/// modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software 
/// is furnished to do so, subject to the following conditions:
/// 
/// The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
/// 
/// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
/// OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE 
/// LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR 
/// IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
///------------------------------------------------------------------------------------------------------------------------------



///---------------------------------------------------------------------------------------------------------------------
/// @file
/// @mainpage LAP GPUBuffer
/// LAP GPUBuffer uses OpenGL to offer simple access to gpu data with OpenGL. This small library can perform various buffer operations
/// and exactly specify buffer storage.
///
/// @version 1.01
///
/// @par Properties:
///	- c++11, no exceptions
///	- OpenGL 4.5
///	- works on Windows, *nix and MACOS
///	- the OpenGL functions must be provided by the user, see line 84. In this case it uses the lap_wic library, set it to the desired OpenGL 
///   extension libray. This could have been done with extern functions but some OpenGL extension loader libraries are based on macros. Furthermore 
///	  macros are used by many hook-based OpenGL debug libraries, therefore the current system was preferred.
///
/// @par Compiling and linking:
///	- Windows:
///		- no special setup for visual studio projects
///		- -pthread -lgdi32 otherwise
///		- an example visual studio project+solution is provide in the vstudio folder
///		- an example makefile (vs version) is provided in root folder
///	- Linux:
///		- -ldl -lGL -lX11 -lXxf86vm -lXrandr -lXi -lXinerama -lXcursor -lrt -lm
///		- the wayland and mir backends are not used by lapwic (but they can easily be added in the future when they mature, right now they are experimental in glfw)
///		- an example makefile is provided in the root folder
///		- to install the project dependencies get:
///			- sudo apt-get install xorg-dev
///			- sudo apt-get install libgl-dev
///	- OSX
///		- -framework Cocoa -framework OpenGL -framework IOKit -framework CoreVideo -framework CoreFoundation -framework Carbon
///		- an example makefile is provided in the root folder
/// @author Lucian Petrescu( lucian (dot) petrescu (dot) 24 (at) gmail (dot) com )
///
/// @par Usage:
///	- basic usage
///		@code
///
///		#include "lap_gpu_buffer.h"
///
///		... create OpenGL context ...
///
///		//create an empty buffer with specific storage flags
///		lap::GPUBuffer gpubuffer{ lap::GPUBuffer::STORAGE_READ | lap::GPUBuffer::STORAGE_WRITE };
///		// resize the buffer
///		gpubuffer.resize(5000);
///		//update the buffer 
///		gpubuffer.update(dataptr, 0, dataptr_size_less_than_5000);
///		//copy to cpu
///		gpubuffer.copy(dataptr, offset_on_gpu, size);
///
///		//map and unmap
///		int* ptr = reinterpret_cast<int*>(gpubuffer.map(read?, write?, size);
///		gpubuffer.unmap();
///
///		@endcode
///
///------------------------------------------------------------------------------------------------



#pragma once
// module needs OpenGL and wic provides it
#include "dep/lap_wic.hpp"

namespace lap {
	namespace gpu {

		///gpu buffer
		class GPUBuffer {
		public:
			static const GLenum STORAGE_DYNAMIC = GL_DYNAMIC_STORAGE_BIT;						///< the buffer can be changed from the cpu
			static const GLenum STORAGE_READ = GL_MAP_READ_BIT;									///< the buffer can accessed from the cpu for reading
			static const GLenum STORAGE_WRITE = GL_MAP_WRITE_BIT | GL_DYNAMIC_STORAGE_BIT;		///< the buffer can accesesd from the cpu for writing
			static const GLenum STORAGE_PERSISTENT_READ = GL_MAP_PERSISTENT_BIT | GL_MAP_READ_BIT;	///< the buffer can be mapped on the cpu, while performing rendering operations with the buffer, with reading access
			static const GLenum STORAGE_PERSISTENT_WRITE = GL_MAP_PERSISTENT_BIT | GL_MAP_WRITE_BIT;///< the buffer can be mapped on the cpu, while performing rendering operations with the buffer, with writing access
			static const GLenum STORAGE_PERSISTENT_READ_WRITE = GL_MAP_PERSISTENT_BIT | GL_MAP_WRITE_BIT | GL_MAP_READ_BIT; ///<the buffer can be mapped on the cpu, while performing rendering operations with the buffer, with reading and writing accesss
			static const GLenum STORAGE_COHERENT = GL_MAP_COHERENT_BIT | GL_MAP_PERSISTENT_BIT;	///< the buffer is mapped coherently (all changes are visible to all mappings)
			static const GLenum STORAGE_CPU = GL_CLIENT_STORAGE_BIT;							///< use storage from the cpu? This is a HINT, driver decides.

			/// create a GPU buffer object, with specified storage flags. The buffer is empty and has size 1 (minimum accepted by driver).
			/// @param storage_flags the requested storage flags. A bitwise combination of the in class-provided storage flags (STORAGE_DYNAMIC, STORAGE_READ, etc).
			///                      compatible with OpenGL storage flags (GL_DYNAMIC_STORAGE_BIT, GL_MAP_READ_BIT, etc). If using OpenGL flags be sure to respect
			///						 their rules, the provided flags already do that.
			GPUBuffer(GLenum storage_flags);
			/// create a GPU buffer object, with specified storage flags. The buffer is empty and has the specified size.
			/// @param size			 the size of the buffer, in bytes.
			/// @param storage_flags the requested storage flags. A bitwise combination of the in class-provided storage flags (STORAGE_DYNAMIC, STORAGE_READ, etc).
			///                      compatible with OpenGL storage flags (GL_DYNAMIC_STORAGE_BIT, GL_MAP_READ_BIT, etc)
			GPUBuffer(size_t size, GLenum storage_flags);
			/// create a GPU buffer object, with specified storage flags. 
			/// The buffer is filled with the specified data (from data+offset -> data+offset+size) and has the specified size.
			/// @param data			 pointer to cpu data buffer
			/// @param offset		 offset relative to *data
			/// @param size			 the size of the data, in bytes.
			/// @param storage_flags the requested storage flags. A bitwise combination of the in class-provided storage flags (STORAGE_DYNAMIC, STORAGE_READ, etc).
			///                      compatible with OpenGL storage flags (GL_DYNAMIC_STORAGE_BIT, GL_MAP_READ_BIT, etc)
			GPUBuffer(void* data, size_t offset, size_t size, GLenum storage_flags);
			/// disable implicit copies, use clone
			GPUBuffer(const GPUBuffer&) = delete;
			GPUBuffer& operator=(const GPUBuffer&) = delete;
			/// permit moves
			GPUBuffer(GPUBuffer&&);
			GPUBuffer& operator=(GPUBuffer&&);
			~GPUBuffer();

			/// clones this GPUBuffer (creates new GPU object)
			/// @return the new GPUbuffer
			GPUBuffer clone() const;
			/// swaps this GPUBuffer
			/// @param other the other buffer
			void swap(GPUBuffer& other);

			/// resizes the GPU buffer to a new size, all existing data is lost
			/// @param size			the new size of the buffer
			void resize(size_t size);
			/// copies data from the GPU buffer to the cpu
			/// @param dst			destination of copy operation
			/// @param gpuoffset	offset into the gpu buffer, from which data will be read
			/// @param datasize		the size of the read data
			void copy(void* dst, size_t gpuoffset, size_t datasize) const;
			/// updates the contents of the GPU buffer, does NOT change size.
			/// @param data			pointer to cpu data buffer
			/// @param gpuoffset	offset into the gpu buffer, to which data will be written
			/// @param datasize		the size of the written data
			void update(void* data, size_t gpuoffset, size_t datasize);
			/// updates the contents of the GPU buffer, does NOT change size. 
			/// equivalent with update(data, 0, existing gpu buffer size)
			void update(void* data);

			/// bind the buffer to an OpenGL target (e.g. GL_ARRAY_BUFFER, etc)
			/// @param target		the target buffer
			void bind(GLenum target);
			/// bind the buffer to an indexed OpenGL target (e.g. GL_ATOMIC_COUNTER_BUFFER, etc)
			/// @param target		the target buffer
			/// @param index		the index of the target
			void bindIndexed(GLenum target, unsigned int index);
			/// bind a range of the buffer to an indexed OpenGL target (e.g. GL_ATOMIC_COUNTER_BUFFER, etc)
			/// @param target		the target buffer
			/// @param index		the index of the target
			/// @param offset		the offset into the gpu buffer
			/// @param size			the size of the gpu buffer
			void bindIndexed(GLenum target, unsigned int index, size_t offset, size_t size);

			/// map the buffer to the cpu. 
			///	Notes:
			///	1. this operation will crash if the requested access rights are incompatible with the specified storage.
			/// 2. you can't map an already mapped buffer.
			/// @param read			read rights?
			/// @param write		write rights?
			/// @param offset		map the gpu buffer from this offset
			void* map(bool read, bool write, size_t offset = 0);
			/// unmap the buffer
			void unmap();

			/// return the OpenGL object
			/// @return the OpenGL object
			GLuint getGLobject() const;
			/// return the size of the buffer
			// /@return the size of the buffer
			size_t getSize() const;
			/// return the storage flags
			/// @return the storage flags
			GLenum getStorageFlags() const;
			/// return whether the buffer is mapped or not
			/// @return whether the buffer is mapped or not
			bool getIsMapped() const;
		private:
			GLuint globject = 0;
			size_t size = 0;
			GLenum storage_flags = 0;
			bool is_mapped = false;
		};
	}
}
