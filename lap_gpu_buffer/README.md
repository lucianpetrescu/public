# LAP GPU Buffer #

LAP GPUBuffer uses OpenGL to offer simple access to gpu data with OpenGL. This small library can perform various buffer operations and exactly specify buffer storage.

version 1.01


###PROPERTIES###
- c++11, no exceptions
- OpenGL 4.5
- works on Windows, *nix and MACOS
- the OpenGL functions must be provided by the user, see line 84. In this case it uses the lap_wic library, set it to the desired OpenGL extension libray. This could have been done with extern functions but some OpenGL extension loader libraries are based on macros. Furthermore macros are used by many hook-based OpenGL debug libraries, therefore the current system was preferred.

###COMPILING AND LINKING###

Visual studio 2015 project&solution files can be found in the **vstudio** folder. Makefiles for gnu/gcc ( **makefile** ) and for visual studio ( **makefilevs.bat** ) are provided in the root folder. All methods will generate objs, pdbs, exes, etc in **bin**.

- Windows:
	- no special setup for visual studio projects
	- -pthread -lgdi32 otherwise
	- an example visual studio project+solution is provide in the vstudio folder
	- the makefilevs.bat from the root folder can also be used
- Linux:
	- -ldl -lGL -lX11 -lXxf86vm -lXrandr -lXi -lXinerama -lXcursor -lrt -lm
	- the wayland and mir backends are not used by lapwic (but they can easily be added in the future when they mature, right now they are experimental in glfw)
	- an example makefile is provided in the root folder
	- to install the project dependencies get:
		- sudo apt-get install xorg-dev
		- sudo apt-get install libgl-dev
	- also compiles in a VMWARE virtual machine, but needs a special mesa driver http://mesa3d.org/vmware-guest.html. It will crash as the mesa driver does not support OpenGL 4.5.
- OSX
	- -framework Cocoa -framework OpenGL -framework IOKit -framework CoreVideo -framework CoreFoundation -framework Carbon
	- an example makefile is provided in the root folder\n

### EXAMPLE - BASIC USAGE###

		#include "lap_gpu_buffer.h"

		... create OpenGL context ...

		//create an empty buffer with specific storage flags
		lap::GPUBuffer gpubuffer{ lap::GPUBuffer::STORAGE_READ | lap::GPUBuffer::STORAGE_WRITE };
		// resize the buffer
		gpubuffer.resize(5000);
		//update the buffer 
		gpubuffer.update(dataptr, 0, dataptr_size_less_than_5000);
		//copy to cpu
		gpubuffer.copy(dataptr, offset_on_gpu, size);

		//map and unmap
		int* ptr = reinterpret_cast<int*>(gpubuffer.map(read?, write?, size);
		... do work here ...
		gpubuffer.unmap();
		
### DOCUMENTATION ###

a Doxyfile is provided in **doc**, which can be used with Doxygen to generate  the entire documentation in HTML format. 
Note: doxygen has problems with arguments of std::function type function arguments (used for callbacks) and will issue a large number of warnings.


### COMPATIBILITY ###

tested with visual studio 2015, MinGW x64 4.0, Ubuntu 15 + gcc 4.8.4, MacOS El Capitan + clang 3.7.1.

###TODO LIST###

more testing on osx.