///------------------------------------------------------------------------------------------------------------------------------
/// The MIT License (MIT)
/// 
/// Copyright (c) 2016 Lucian Petrescu, lucian (dot) petrescu (dot) 24 (at) gmail (dot) com
/// 
/// Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation 
/// files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, 
/// modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software 
/// is furnished to do so, subject to the following conditions:
/// 
/// The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
/// 
/// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
/// OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE 
/// LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR 
/// IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
///------------------------------------------------------------------------------------------------------------------------------


#include "lap_gpu_buffer.h"
#include <cassert>
#include <cstring>

namespace lap {
	namespace gpu {

		//ctor
		GPUBuffer::GPUBuffer(GLenum in_storage_flags) {
			storage_flags = in_storage_flags;
			resize(1);
		}
		GPUBuffer::GPUBuffer(size_t in_size, GLenum in_storage_flags) {
			storage_flags = in_storage_flags;
			resize(in_size);
		}
		GPUBuffer::GPUBuffer(void* data, size_t in_offset, size_t in_size, GLenum in_storage_flags) {
			storage_flags = in_storage_flags;
			resize(in_size);
			update(data, in_offset, in_size);
		}
		GPUBuffer::~GPUBuffer() {
			if (globject != 0) glDeleteBuffers(1, &globject);
		}

		GPUBuffer::GPUBuffer(GPUBuffer&& rhs) {
			assert(this != &rhs);	//most definately a user bug as this leads to undefined behavior
			globject = rhs.globject;
			size = rhs.size;
			storage_flags = rhs.storage_flags;
			is_mapped = rhs.is_mapped;
			rhs.globject = 0;
			rhs.size = 0;
			rhs.storage_flags = 0;
			rhs.is_mapped = false;
		}
		GPUBuffer& GPUBuffer::operator=(GPUBuffer&& rhs) {
			assert(this != &rhs);	//most definately a user bug as this leads to undefined behavior
			if (globject != 0) {
				glDeleteBuffers(1, &globject);
				globject = 0;
			}
			globject = rhs.globject;
			size = rhs.size;
			storage_flags = rhs.storage_flags;
			is_mapped = rhs.is_mapped;
			rhs.globject = 0;
			rhs.size = 0;
			rhs.storage_flags = 0;
			rhs.is_mapped = false;
			return (*this);
		}

		GPUBuffer GPUBuffer::clone() const {
			assert(globject != 0);
			GPUBuffer result{ storage_flags };
			result.resize(size);
			glCopyNamedBufferSubData(globject, result.getGLobject(), 0, 0, size);
			return result; //nrvo move
		}
		void GPUBuffer::swap(GPUBuffer& other) {
			std::swap(globject, other.globject);
			std::swap(size, other.size);
			std::swap(storage_flags, other.storage_flags);
			std::swap(is_mapped, other.is_mapped);
		}


		//to and from gpu
		void GPUBuffer::resize(size_t in_size) {
			//the memory contract still stands, no resize needed
			if (in_size == size) return;

			if (globject != 0) glDeleteBuffers(1, &globject);
			glCreateBuffers(1, &globject);

			size = in_size;
			glNamedBufferStorage(globject, size, nullptr, storage_flags);
		}
		void GPUBuffer::copy(void* dst, size_t in_gpuoffset, size_t in_datasize) const {
			GLvoid* ptr = glMapNamedBufferRange(globject, 0, size, GL_MAP_READ_BIT);
			std::memcpy(dst, reinterpret_cast<unsigned char*>(ptr) + in_gpuoffset, in_datasize);
			glUnmapNamedBuffer(globject);
		}
		void GPUBuffer::update(void* data, size_t in_gpuoffset, size_t in_datasize) {
			glNamedBufferSubData(globject, in_gpuoffset, in_datasize, reinterpret_cast<unsigned char*>(data));
		}
		void GPUBuffer::update(void* data) {
			glNamedBufferSubData(globject, 0, size, reinterpret_cast<unsigned char*>(data));
		}


		//binds
		void GPUBuffer::bind(GLenum target) {
			glBindBuffer(target, globject);
		}
		void GPUBuffer::bindIndexed(GLenum target, unsigned int index) {
			bindIndexed(target, index, 0, size);
		}
		void GPUBuffer::bindIndexed(GLenum target, unsigned int index, size_t offset, size_t in_size) {
			if (target == GL_ATOMIC_COUNTER_BUFFER || target == GL_TRANSFORM_FEEDBACK_BUFFER || target == GL_UNIFORM_BUFFER || target == GL_SHADER_STORAGE_BUFFER) {
				glBindBufferRange(target, index, globject, (GLintptr)offset, (GLsizeiptr)in_size);
			}
		}

		//maps
		void* GPUBuffer::map(bool read, bool write, size_t offset) {
			//can't map to an already mapped buffer
			assert(!is_mapped);

			GLenum access = 0;
			if (read && write) {
				access = GL_READ_WRITE;
				assert((storage_flags & GL_MAP_WRITE_BIT) && (storage_flags & GL_MAP_READ_BIT));
			}
			else if (read) {
				access = GL_READ_ONLY;
				assert(storage_flags & GL_MAP_READ_BIT);
			}
			else {
				access = GL_WRITE_ONLY;
				assert(storage_flags & GL_MAP_WRITE_BIT);
			}
			GLvoid* ptr = glMapNamedBuffer(globject, access);
			is_mapped = true;
			return (void*)((unsigned char*)ptr + offset);
		}

		void GPUBuffer::unmap() {
			//can't unmap an unmapped buffer
			assert(is_mapped);
			glUnmapNamedBuffer(globject);
			is_mapped = false;
		}

		//raw acess
		GLuint GPUBuffer::getGLobject() const {
			return globject;
		}
		size_t GPUBuffer::getSize() const {
			return size;
		}
		GLenum GPUBuffer::getStorageFlags() const {
			return storage_flags;
		}
		bool GPUBuffer::getIsMapped() const {
			return is_mapped;
		}
	}
}
