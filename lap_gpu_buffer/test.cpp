///---------------------------------------------------------------------------------------------------------------------
/// The MIT License (MIT)
/// 
/// Copyright (c) 2016 Lucian Petrescu, lucian (dot) petrescu (dot) 24 (at) gmail (dot) com
/// 
/// Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation 
/// files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, 
/// modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software 
/// is furnished to do so, subject to the following conditions:
/// 
/// The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
/// 
/// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
/// OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE 
/// LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR 
/// IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
///---------------------------------------------------------------------------------------------------------------------


#include "dep/lap_wic.hpp"
#include "lap_gpu_buffer.h"
#include <iostream>
#include <vector>
#include <cassert>

//-------------------------------------------------------------------------------------------------
int main(int argc, char* argv[]){
	//we need a window for the OpenGL context
	lap::wic::WICSystem wicsystem(&std::cout, false);
	lap::wic::WindowProperties wp; lap::wic::FramebufferProperties fp; lap::wic::ContextProperties cp; lap::wic::InputProperties ip;
	wp.visible = false;			//not interested in visualization
	lap::wic::Window window = lap::wic::Window(wp, fp, cp, ip);
	
	//some test data
	std::vector<int> data; data.resize(1000);
	std::vector<int> data2; data2.resize(1000);
	for (int i = 0; i < 1000; i++) data[i] = i;

	//operations
	using namespace lap::gpu;
	GPUBuffer gpubuffer{ GPUBuffer::STORAGE_READ | GPUBuffer::STORAGE_WRITE };
	std::cout << " The buffer has the OpenGL id = " << gpubuffer.getGLobject() << std::endl;
	if (gpubuffer.getStorageFlags() & GPUBuffer::STORAGE_READ) std::cout << " The buffer can be mapped for reading" << std::endl;
	if (gpubuffer.getStorageFlags() & GPUBuffer::STORAGE_WRITE) std::cout << " The buffer can be mapped for writing" << std::endl;
	if (gpubuffer.getStorageFlags() & GPUBuffer::STORAGE_DYNAMIC) std::cout << " The buffer is dynamic (implictly by making it writeable)" << std::endl;
	if (!(gpubuffer.getStorageFlags() & GPUBuffer::STORAGE_COHERENT)) std::cout << " The buffer is NOT coherent" << std::endl;
	gpubuffer.resize(data.size()*sizeof(int));
	std::cout << " The buffer has size = " << gpubuffer.getSize() << std::endl;
	gpubuffer.update(data.data(), 0, data.size() * sizeof(int));
	
	gpubuffer.bind(GL_ARRAY_BUFFER);
	gpubuffer.bindIndexed(GL_SHADER_STORAGE_BUFFER, 2);
	
	gpubuffer.copy(data2.data(), 100 * sizeof(int), 300 * sizeof(int));
	for (int i = 0; i < 300; i++) assert(data2[i] == data[i + 100]);

	int* ptr = reinterpret_cast<int*>(gpubuffer.map(true, true, sizeof(int) * 500));
	for (int i = 0; i < 500; i++) assert(ptr[i] == data[i + 500]);
	std::cout << " The buffer is mapped = " << std::boolalpha << gpubuffer.getIsMapped() << std::endl;
	gpubuffer.unmap();
	std::cout << " The buffer is mapped = " << std::boolalpha << gpubuffer.getIsMapped() << std::endl;

	//OpenGL errors?
	assert(glGetError() == GL_NO_ERROR);

	std::cout << "Finished" << std::endl;
	std::cin.get();
	return 0;
}
