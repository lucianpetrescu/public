﻿///---------------------------------------------------------------------------------------------------------------------
/// The MIT License (MIT)
/// 
/// Copyright (c) 2016 Lucian Petrescu, lucian (dot) petrescu (dot) 24 (at) gmail (dot) com
/// 
/// Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation 
/// files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, 
/// modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software 
/// is furnished to do so, subject to the following conditions:
/// 
/// The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
/// 
/// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
/// OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE 
/// LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR 
/// IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
///---------------------------------------------------------------------------------------------------------------------


#include "lap_font_tools.hpp"
#include <iostream>
#include <fstream>
#include <cassert>
#include <iomanip>
#include <cstring>

void test_basic() {
	using namespace lap::font;
	std::cout << "Test basic " << std::endl;
	FontAtlas fontatlas(&std::cout);	//create a font atlas object
	fontatlas.import("../assets/DejaVuSansMono.ttf", 15);		//import a font into the font atlas object
	bool success = fontatlas.pack(1024, 1024, 2, 1, true, 4);	//pack the fonts inside the font atlas object into a 1024x1024 image and compute the SDF
	if (!success) {
		std::cout << "Failed packing" << std::endl;
		return;
	}
	
	fontatlas.exportImages("../assets/output_basic_img.png", "../assets/output_basic_imgsdf.png");	//export to debug view
}
void test_multiple_fonts() {
	using namespace lap::font;
	std::cout << "Test multiple fonts in a single atlas " << std::endl;
	FontAtlas fontatlas(&std::cout);	//create a font atlas object
	fontatlas.import("../assets/DejaVuSerif.ttf", 16);	//import a font into the font atlas object
	fontatlas.import("../assets/DejaVuSansMono.ttf", 12);	//import another font into the font atlas object, with a different glyph size
	bool success = fontatlas.pack(2048, 2048, 4, 1, true, 4);	//pack the fonts inside the font atlas object into a 1024x1024 image and compute the SDF
	if (!success) {
		std::cout << "Failed packing" << std::endl;
		return;
	}

	fontatlas.exportImages("../assets/output_multiplefonts_img.png", "../assets/output_multiplefonts_imgsdf.png");	//export to debug view
	//output some detail
	const std::vector<Font>& fonts = fontatlas.getFonts();
	std::cout << "Fonts in atlas:" << std::endl;
	for (unsigned int i = 0; i < fonts.size(); i++) std::cout << i << ": name=" << fonts[i].getName() << "\t raster size=" << fonts[i].getGlyphSize() << "\t glyphs=" << fonts[i].getNumGlyphs() << "\t kerning pairs= " << fonts[i].getNumKerningPairs() << std::endl;
}

void test_ranges() {
	using namespace lap::font;
	std::cout << "Test ranges " << std::endl;
	FontAtlas fontatlas(&std::cout);	//create a font atlas object
	fontatlas.import("../assets/DejaVuSansMono.ttf", 25, { CODEPOINTS_RANGE_BASIC_LATIN, CODEPOINTS_RANGE_ARABIC, CODEPOINTS_RANGE_CYRILLIC, CODEPOINTS_RANGE_HIRAGANA });
	bool success = fontatlas.pack(1024, 1024, 5, 1, true, 10);

	if (!success) {
		std::cout << "Failed packing" << std::endl;
		return;
	}
	
	const Font& f = fontatlas.getFonts().at(0);
	//occupancy tests
	std::cout << "Font has " << f.getCodepointOccupancy(CODEPOINTS_RANGE_ARABIC) << " occupancy over arabic glyphs" << std::endl;
	std::cout << "Font has " << f.getCodepointOccupancy({ CODEPOINTS_RANGE_CYRILLIC, CODEPOINTS_RANGE_HIRAGANA }) << " occupancy over cyrillic and hiragana glyphs" << std::endl;

	//glyph and kerning pair access
	const std::map<int, GlyphData>& codepoint_to_glyphs = f.getCodepointToGlyphMap();
	const GlyphData& glyph_a = f.getCodepointToGlyph(67);
	const GlyphData& glyph_unknown = f.getUndefinedGlyph();
	const std::map<KerningPair, float>& kernmap = f.getKerningMap();

	//various
	const std::string& name = f.getName();
	unsigned int num_glyphs = f.getNumGlyphs();
	unsigned int num_kerning_pairs = f.getNumKerningPairs();
	unsigned int font_glyph_size = f.getGlyphSize();
	bool exists_undefined = f.hasCodepoint(9443423);
	bool exists_a = f.hasCodepoint(67);
	bool exists_latin = f.hasCodepoints({ 67,68,69,70,71,72,73,74 });

	//font general properties
	unsigned int line_height = f.getLineHeight();
	unsigned int baseline = f.getBaseline();
	unsigned int padding = f.getPadding();
	unsigned int oversampling = f.getOversampling();
	unsigned int sdfdistance = f.getSDFdistance();
	bool sdfcomputed = f.getSDFcomputed();
}

void test_export() {
	using namespace lap::font;
	std::cout << "Test export " << std::endl;
	FontAtlas fontatlas(&std::cout);	//create a font atlas object
	fontatlas.import("../assets/DejaVuSerif.ttf", 25, { CODEPOINTS_RANGE_BASIC_LATIN});
	fontatlas.import("../assets/DejaVuSansMono.ttf", 20, { CODEPOINTS_RANGE_BASIC_LATIN });
	bool success = fontatlas.pack(512, 512, 5, 1, true, 10);
	if (!success) {
		std::cout << "Failed packing" << std::endl;
		return;
	}

	//export to BMFONT txt format
	fontatlas.exportBMFONT("../assets/output_export_bmfonttxt_fnt.fnt", "../assets/output_export_bmfonttxt_img.png", "../assets/output_export_bmfonttxt_sdf.png");

	//export to CPP format (directly includable in cpp projects)
	fontatlas.exportCPP("../assets/output_export_cpp.hpp");

	//export to packed format (directly includable in cpp projects)
	fontatlas.exportPacked("../assets/output_export_packed.pak");


	//check binary format
	std::fstream file("../assets/output_export_packed.pak", std::fstream::in | std::fstream::binary);

	unsigned int w, h; bool sdf_computed;
	file.read((char*)&w, sizeof(unsigned int));
	file.read((char*)&h, sizeof(unsigned int));
	file.read((char*)&sdf_computed, sizeof(bool));
	assert(w == fontatlas.getWidth() && h == fontatlas.getHeight() && sdf_computed == fontatlas.getSDFcomputed());

	std::vector<unsigned char> pixels(w*h, 0), pixels_sdf(w*h, 0);
	file.read((char*)pixels.data(), w*h*sizeof(unsigned char));
	auto& checkpixels = fontatlas.getPixels();
	for (unsigned int i = 0; i < w*h; i++) assert(pixels[i] == checkpixels[i]);
	if (sdf_computed) {
		auto& checkpixelssdf = fontatlas.getPixelsSDF();
		file.read((char*)pixels_sdf.data(), w*h*sizeof(unsigned char));
		for (unsigned int i = 0; i < w*h; i++) assert(pixels_sdf[i] == checkpixelssdf[i]);
	}

	unsigned int num_fonts;
	file.read((char*)&num_fonts, sizeof(unsigned int));
	assert(num_fonts == fontatlas.getFonts().size());
	std::vector<Font> fonts(num_fonts);
	for (auto& checkfont : fontatlas.getFonts()) {
		unsigned int name_count;
		file.read((char*)&name_count, sizeof(unsigned int));
		char buffer[256];
		file.read(buffer, name_count*sizeof(char));
		buffer[name_count] = '\0'; 
		std::string name = buffer;
		assert(name == checkfont.getName());

		unsigned int glyph_size, oversampling, padding, line_height, image_width, image_height, baseline;
		file.read((char*)&glyph_size, sizeof(unsigned int));
		file.read((char*)&oversampling, sizeof(unsigned int));
		file.read((char*)&padding, sizeof(unsigned int));
		file.read((char*)&line_height, sizeof(unsigned int));
		file.read((char*)&baseline, sizeof(unsigned int));
		file.read((char*)&image_width, sizeof(unsigned int));
		file.read((char*)&image_height, sizeof(unsigned int));
		assert(glyph_size == checkfont.getGlyphSize() && oversampling == checkfont.getOversampling() && padding == checkfont.getPadding() && line_height == checkfont.getLineHeight());
		assert(image_width == checkfont.getGlyphImageWidth() && image_height == checkfont.getGlyphImageHeight() && baseline == checkfont.getBaseline());

		unsigned int num_codepoints;
		file.read((char*)&num_codepoints, sizeof(unsigned int));
		assert(num_codepoints == checkfont.getCodepointToGlyphMap().size());
		
		//codepoints
		unsigned int codepoint; float x, y, width, height, xoffset, yoffset, xadvance;
		for (const auto& checkpair : checkfont.getCodepointToGlyphMap()) {
			file.read((char*)&codepoint, sizeof(unsigned int));
			file.read((char*)&x, sizeof(float));
			file.read((char*)&y, sizeof(float));
			file.read((char*)&width, sizeof(float));
			file.read((char*)&height, sizeof(float));
			file.read((char*)&xoffset, sizeof(float));
			file.read((char*)&yoffset, sizeof(float));
			file.read((char*)&xadvance, sizeof(float));
			assert(codepoint == checkpair.first);
			assert(x == checkpair.second.x && y == checkpair.second.y && width == checkpair.second.width && height == checkpair.second.height && xoffset == checkpair.second.xoffset && yoffset == checkpair.second.yoffset && xadvance == checkpair.second.xadvance);
		}

		//undefined codepoint glyph
		file.read((char*)&codepoint, sizeof(unsigned int));
		file.read((char*)&x, sizeof(float));
		file.read((char*)&y, sizeof(float));
		file.read((char*)&width, sizeof(float));
		file.read((char*)&height, sizeof(float));
		file.read((char*)&xoffset, sizeof(float));
		file.read((char*)&yoffset, sizeof(float));
		file.read((char*)&xadvance, sizeof(float));
		const GlyphData& checkglyph = checkfont.getUndefinedGlyph();
		assert(codepoint == lap::font::CODEPOINT_UNDEFINED && x == checkglyph.x && y == checkglyph.y && width == checkglyph.width && height == checkglyph.height && xoffset == checkglyph.xoffset && yoffset == checkglyph.yoffset && xadvance == checkglyph.xadvance);

		//kernings
		unsigned int num_kernings;
		file.read((char*)&num_kernings, sizeof(unsigned int));
		assert(num_kernings == checkfont.getKerningMap().size());
		for (const auto& checkpair : checkfont.getKerningMap()) {
			unsigned int codepoint1, codepoint2; float advance;
			file.read((char*)&codepoint1, sizeof(unsigned int));
			file.read((char*)&codepoint2, sizeof(unsigned int));
			file.read((char*)&advance, sizeof(float));
			assert(codepoint1 == checkpair.first.codepoint1 && codepoint2 == checkpair.first.codepoint2 && advance == checkpair.second);
		}
	}
	file.close();
}


//use this test to quickly determine which fonts satisfy your needs
void test_languages() {
	using namespace lap::font;
	std::cout << "Test languages " << std::endl;
	FontAtlas fontatlas(&std::cout);
	fontatlas.import("../assets/DejaVuSansMono.ttf", 20);	//change to your font
	bool success = fontatlas.pack(1024, 1024, 1, 1);
	if (!success) {
		std::cout << "Failed packing" << std::endl;
		return;
	}

	//get the font
	const Font& font = fontatlas.getFonts().at(0);

#ifdef _MSC_VER
#pragma warning(push)
#pragma warning(disable : 4566)
#endif


	std::cout << std::strlen(u8"Ах чудна българска земьо, полюшвай цъфтящи жита") << std::endl;
	std::cout << std::strlen("Ах чудна българска земьо, полюшвай цъфтящи жита") << std::endl;
	std::cout << std::string(u8"Ах чудна българска земьо, полюшвай цъфтящи жита").size() << std::endl;
	std::cout << std::string("Ах чудна българска земьо, полюшвай цъфтящи жита").size() << std::endl;
	std::cout << std::strlen(std::string(u8"Ах чудна българска земьо, полюшвай цъфтящи жита").c_str()) << std::endl;
	std::cout << std::strlen(std::string("Ах чудна българска земьо, полюшвай цъфтящи жита").c_str()) << std::endl;

	//NOTE: it is very probable that your IDE/text editor does not recognize all unicode characters from this list of texts.
	//these panagrams are taken from http://clagnut.com/blog/2380/ (originally on wikipedia)
	//u8 is required to store the data in UTF-8 format, otherwise it might be (depending on IDE and OS) stored in a local codepage (e.g. Windows / Visual Studio / Latin )
	std::vector<std::pair<std::string, std::string>> languages = {
		{ "Arabic",u8"صِف خَلقَ خَودِ كَمِثلِ الشَمسِ إِذ بَزَغَت — يَحظى الضَجيعُ بِها نَجلاءَ مِعطارِ" },
		{ "Azeri",u8"Zəfər, jaketini də papağını da götür, bu axşam hava çox soyuq olacaq." },
		{ "Bulgarian",u8"Ах чудна българска земьо, полюшвай цъфтящи жита" },
		{ "Chinese (Traditional)", u8"視野無限廣，窗外有藍天"},
		{ "Chinese (Simplified)", u8"Innovation in China 中国智造，慧及全球 0123456789"},
		{ "Croatian",u8"Gojazni đačić s biciklom drži hmelj i finu vatu u džepu nošnje" },
		{ "Czech",u8"Nechť již hříšné saxofony ďáblů rozezvučí síň úděsnými tóny waltzu, tanga a quickstepu" },
		{ "Danish",u8"Quizdeltagerne spiste jordbær med fløde, mens cirkusklovnen Walther spillede på xylofon" },
		{ "English",u8"The quick brown fox jumps over the lazy dog" },
		{ "Esperanto",u8"Eble ĉiu kvazaŭ-deca fuŝĥoraĵo ĝojigos homtipon" },
		{ "Estonian",u8"See väike mölder jõuab rongile hüpata" },
		{ "Finnish",u8"Albert osti fagotin ja töräytti puhkuvan melodian" },
		{ "French",u8"Buvez de ce whisky que le patron juge fameux" },
		{ "West Frisian",u8"Alve bazige froulju wachtsje op dyn komst" },
		{ "German",u8"Franz jagt im komplett verwahrlosten Taxi quer durch Bayern" },
		{ "Greek",u8"Ταχίστη αλώπηξ βαφής ψημένη γη, δρασκελίζει υπέρ νωθρού κυνός Takhístè alôpèx vaphês psèménè gè, draskelízei ypér nòthroý kynós" },
		{ "Hebrew",u8"עטלף אבק נס דרך מזגן שהתפוצץ כי חם" },
		{ "Hindi", u8"ऋषियों को सताने वाले दुष्ट राक्षसों के राजा रावण का सर्वनाश करने वाले विष्णुवतार भगवान श्रीराम, अयोध्या के महाराज दशरथ के बड़े सपुत्र थे।" },
		{ "Hungarian", u8"Árvíztűrő tükörfúrógép"},
		{ "Icelandic", u8"Kæmi ný öxi hér, ykist þjófum nú bæði víl og ádrepa" },
		{ "Igbo", u8"Nne, nna, wepụ he’l’ụjọ dum n’ime ọzụzụ ụmụ, vufesi obi nye Chukwu, ṅụrịanụ, gbakọọnụ kpaa, kwee ya ka o guzoshie ike; ọ ghaghị ito, nwapụta ezi agwa" },
		{ "Indonesian",u8"Muharjo seorang xenofobia universal yang takut pada warga jazirah, contohnya Qatar" },
		{ "Irish",u8"D’ḟuascail Íosa Úrṁac na hÓiġe Beannaiṫe pór Éaḃa agus Áḋaiṁ" },
		{ "Italian",u8"Ma la volpe, col suo balzo, ha raggiunto il quieto Fido." },
		{ "Japanese", u8"いろはにほへと ちりぬるを わかよたれそ つねならむ うゐのおくやま けふこえて あさきゆめみし ゑひもせす（ん）"},
		{ "Javanese", u8"꧋ ꦲꦤꦕꦫꦏ꧈ ꦢꦠꦱꦮꦭ꧈ ꦥꦝꦗꦪꦚ꧈ ꦩꦒꦧꦛꦔ꧉"},
		{ "Korean", u8"키스의 고유조건은 입술끼리 만나야 하고 특별한 기술은 필요치 않다."},
		{ "Latin", u8"Sic fugiens, dux, zelotypos, quam Karus haberis"},
		{ "Latvian", u8"Muļķa hipiji mēģina brīvi nogaršot celofāna žņaudzējčūsku."},
		{ "Lithuanian",u8"Įlinkdama fechtuotojo špaga sublykčiojusi pragręžė apvalų arbūzą "},
		{ "Macedonian", u8"Ѕидарски пејзаж: шугав билмез со чудење џвака ќофте и кељ на туѓ цех. "},
		{ "Malayalam", u8"അജവും ആനയും ഐരാവതവും ഗരുഡനും കഠോര സ്വരം പൊഴിക്കെ ഹാരവും ഒഢ്യാണവും ഫാലത്തില്‍ മഞ്ഞളും ഈറന്‍ കേശത്തില്‍ ഔഷധ എണ്ണയുമായി ഋതുമതിയും അനഘയും ഭൂനാഥയുമായ ഉമ ദുഃഖഛവിയോടെ ഇടതു പാദം ഏന്തി ങ്യേയാദൃശം നിര്‍ഝരിയിലെ ചിറ്റലകളെ ഓമനിക്കുമ്പോള്‍ ബാ‍ലയുടെ കണ്‍കളില്‍ നീര്‍ ഊര്‍ന്നു വിങ്ങി."},
		{ "Mongolian", u8"Щётканы фермд пийшин цувъя. Бөгж зогсч хэльюү."},
		{ "Myanmar", u8"သီဟိုဠ်မှ ဉာဏ်ကြီးရှင်သည် အာယုဝဍ္ဎနဆေးညွှန်းစာကို ဇလွန်ဈေးဘေးဗာဒံပင်ထက် အဓိဋ္ဌာန်လျက် ဂဃနဏဖတ်ခဲ့သည်။ "},
		{ "Norwegian", u8"Vår sære Zulu fra badeøya spilte jo whist og quickstep i min taxi. "},
		{ "Polish", u8"Jeżu klątw, spłódź Finom część gry hańb!"},
		{ "Portuguese", u8"Luís argüia à Júlia que «brações, fé, chá, óxido, pôr, zângão» eram palavras do português."},
		{ "Portuguese (Brazilian)", u8"A ligeira raposa marrom ataca o cão preguiçoso."},
		{ "Romanian", u8"Ex-sportivul își fumează jucăuș țigara bând whisky cu tequila. "},
		{ "Russian", u8"ирокая электрификация южных губерний даст мощный толчок подъёму сельского хозяйства. "},
		{ "Sanskrit", u8"कः खगौघाङचिच्छौजा झाञ्ज्ञोऽटौठीडडण्ढणः। तथोदधीन् पफर्बाभीर्मयोऽरिल्वाशिषां सहः।। "},
		{ "Scottish-Gaelic", u8"Mus d’fhàg Cèit-Ùna ròp Ì le ob."},
		{ "Serbian (Cyrillic)", u8"Чешће цeђење мрeжастим џаком побољшава фертилизацију генских хибрида." },
		{ "Serbian (Latin)", u8"Češće ceđenje mrežastim džakom poboljšava fertilizaciju genskih hibrida."},
		{ "Slovak", u8"Kŕdeľ ďatľov učí koňa žrať kôru"},
		{ "Slovenian", u8"Hišničin bratec vzgaja polže pod fikusom. "},
		{ "Spanish", u8"Benjamín pidió una bebida de kiwi y fresa; Noé, sin vergüenza, la más exquisita champaña del menú. "},
		{ "Swedish", u8"Flygande bäckasiner söka hwila på mjuka tuvor"},
		{ "Tagalog", u8"Ang bawat rehistradong kalahok sa patimpalak ay umaasang magantimpalaan ng ñino"},
		{ "Thai", u8"นายสังฆภัณฑ์ เฮงพิทักษ์ฝั่ง ผู้เฒ่าซึ่งมีอาชีพเป็นฅนขายฃวด ถูกตำรวจปฏิบัติการจับฟ้องศาล ฐานลักนาฬิกาคุณหญิงฉัตรชฎา ฌานสมาธิ"},
		{ "Tibetan",u8"༈ དཀར་མཛེས་ཨ་ཡིག་ལས་འཁྲུངས་ཡེ་ཤེས་གཏེར། །ཕས་རྒོལ་ཝ་སྐྱེས་ཟིལ་གནོན་གདོང་ལྔ་བཞིན། །ཆགས་ཐོགས་ཀུན་བྲལ་མཚུངས་མེད་འཇམ་བྱངས་མཐུས། །མ་ཧཱ་མཁས་པའི་གཙོ་བོ་ཉིད་གྱུར་ཅིག།"},
		{ "Turkish", u8"Pijamalı hasta yağız şoföre çabucak güvendi."},
		{ "Ukrainian", u8"Чуєш їх, доцю, га? Кумедна ж ти, прощайся без ґольфів! "},
		{ "Urdu", u8"ژالہ باری میں ر‌ضائی کو غلط اوڑھے بیٹھی قرأة العین اور عظمٰی کے پاس گھر کے ذخیرے سے آناً فاناً ڈش میں ثابت جو، صراحی میں چائے اور پلیٹ میں زرده آیا۔"},
		{ "Uyghur", u8"ئاۋۇ بىر جۈپ خوراز فرانسىيەنىڭ پارىژ شەھرىگە يېقىن تاغقا كۆچەلمىدى."},
		{ "Yoruba", u8"Ìwò̩fà ń yò̩ séji tó gbojúmó̩, ó hàn pákànpò̩ gan-an nis̩é̩ rè̩ bó dò̩la"},
		{ "Welsh", u8"Parciais fy jac codi baw hud llawn dŵr ger tŷ Mabon"}
	};
#ifdef _MSC_VER
#pragma warning(pop)
#endif

	// we need to decode the strings (codepoints in variable length encoding) into integer codepoints
	// the decoder is written over http://bjoern.hoehrmann.de/utf-8/decoder/dfa/
	struct UTF8decoder {
		uint32_t decode(uint32_t* state, uint32_t* codep, uint32_t byte) {
			static const uint8_t utf8d[] = {
				0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0, // 00..1f
				0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0, // 20..3f
				0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0, // 40..5f
				0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0, // 60..7f
				1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,9,9,9,9,9,9,9,9,9,9,9,9,9,9,9,9, // 80..9f
				7,7,7,7,7,7,7,7,7,7,7,7,7,7,7,7,7,7,7,7,7,7,7,7,7,7,7,7,7,7,7,7, // a0..bf
				8,8,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2, // c0..df
				0xa,0x3,0x3,0x3,0x3,0x3,0x3,0x3,0x3,0x3,0x3,0x3,0x3,0x4,0x3,0x3, // e0..ef
				0xb,0x6,0x6,0x6,0x5,0x8,0x8,0x8,0x8,0x8,0x8,0x8,0x8,0x8,0x8,0x8, // f0..ff
				0x0,0x1,0x2,0x3,0x5,0x8,0x7,0x1,0x1,0x1,0x4,0x6,0x1,0x1,0x1,0x1, // s0..s0
				1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,0,1,1,1,1,1,0,1,0,1,1,1,1,1,1, // s1..s2
				1,2,1,1,1,1,1,2,1,2,1,1,1,1,1,1,1,1,1,1,1,1,1,2,1,1,1,1,1,1,1,1, // s3..s4
				1,2,1,1,1,1,1,1,1,2,1,1,1,1,1,1,1,1,1,1,1,1,1,3,1,3,1,1,1,1,1,1, // s5..s6
				1,3,1,1,1,1,1,3,1,3,1,1,1,1,1,1,1,3,1,1,1,1,1,1,1,1,1,1,1,1,1,1 // s7..s8
			};
			uint32_t type = utf8d[byte];

			*codep = (*state != 0) ?
				(byte & 0x3fu) | (*codep << 6) :
				(0xff >> type) & (byte);

			*state = utf8d[256 + *state * 16 + type];
			return *state;
		}
		const std::vector<int> decode(const std::string& text) {
			std::vector<int> result;
			const uint8_t *s = (const uint8_t*)text.c_str();
			uint32_t codepoint;
			uint32_t state = 0;

			for (; *s; ++s)	if (!decode(&state, &codepoint, *s)) result.push_back(codepoint);
			if (state != 0) std::cout << "The string is not well-formed" << std::endl;

			//nrvo
			return result;
		}
	};

	//for each language text decode to codepoints and test codepoint availability in the loaded font (stored in fontatlas)
	UTF8decoder decoder;
	for (auto& e : languages) {
		std::vector<int> codepoints = decoder.decode(e.second);
		std::cout << "Occupancy " << font.getCodepointOccupancy(codepoints) * 100 << "%\t in " << e.first << std::endl;
	}
}



//use this test to see codepoint occupancy
void test_codepoints(){
	using namespace lap::font;
	std::cout << "Test codepoints " << std::endl;
	FontAtlas fontatlas(&std::cout);
	fontatlas.import("../assets/DejaVuSansMono.ttf", 20);	//change to your font
	bool success = fontatlas.pack(1024, 1024, 2, 1, true, 2);
	if (!success) {
		std::cout << "Failed packing" << std::endl;
		return;
	}

	//get the font
	const Font& font = fontatlas.getFonts().at(0);

	// a list of all used non-private Unicode codepoint ranges
	std::vector<std::pair<std::string, std::pair<int, int>>> codepoint_ranges = {
		{ "Aegean Numbers", CODEPOINTS_RANGE_AEGEAN_NUMBERS},
		{ "Alphabetic Presentation Forms", CODEPOINTS_RANGE_ALPHABETIC_PRESENTATION_FORMS},
		{ "Arabic", CODEPOINTS_RANGE_ARABIC},
		{ "Arabic Presentation Forms A", CODEPOINTS_RANGE_ARABIC_PRESENTATION_FORMS_A },
		{ "Arabic Presentation Forms B", CODEPOINTS_RANGE_ARABIC_PRESENTATION_FORMS_B },
		{ "Armenian", CODEPOINTS_RANGE_ARMENIAN },
		{ "Arrows", CODEPOINTS_RANGE_ARROWS },
		{ "Basic Latin", CODEPOINTS_RANGE_BASIC_LATIN },
		{ "Bengali Assamese", CODEPOINTS_RANGE_BENGALI_ASSAMESE },
		{ "Block Elements", CODEPOINTS_RANGE_BLOCK_ELEMENTS },
		{ "Bopomofo", CODEPOINTS_RANGE_BOPOMOFO },
		{ "Bopomofo Extended", CODEPOINTS_RANGE_BOPOMOFO_EXTENDED },
		{ "Box Drawing", CODEPOINTS_RANGE_BOX_DRAWING },
		{ "Braille Patterns", CODEPOINTS_RANGE_BRAILLE_PATTERNS },
		{ "Buhid", CODEPOINTS_RANGE_BUHID },
		{ "Byzantine Musical Symbols", CODEPOINTS_RANGE_BYZANTINE_MUSICAL_SYMBOLS },
		{ "C1 controls and Latin 1 supplement", CODEPOINTS_RANGE_C1_CONTROLS_AND_LATIN_1_SUPPLEMENT },
		{ "Cherokee", CODEPOINTS_RANGE_CHEROKEE },
		{ "CJK Compatibility", CODEPOINTS_RANGE_CJK_COMPATIBILITY },
		{ "CJK Compatibility Forms", CODEPOINTS_RANGE_CJK_COMPATIBILITY_FORMS },
		{ "CJK Compatibility Ideographs", CODEPOINTS_RANGE_CJK_COMPATIBILITY_IDEOGRAPHS },
		{ "CJK Compatibility Ideographs Supplement", CODEPOINTS_RANGE_CJK_COMPATIBILITY_IDEOGRAPHS_SUPPLEMENT },
		{ "CJK Radicals Supplement", CODEPOINTS_RANGE_CJK_RADICALS_SUPPLEMENT },
		{ "CJK Symbols and Punctuation", CODEPOINTS_RANGE_CJK_SYMBOLS_AND_PUNCTUATION },
		{ "CJK Unified Ideographs", CODEPOINTS_RANGE_CJK_UNIFIED_IDEOGRAPHS },
		{ "CJK Unified Ideographs Extension A", CODEPOINTS_RANGE_CJK_UNIFIED_IDEOGRAPHS_EXTENSION_A },
		{ "CJK Unified Ideographs Extension B", CODEPOINTS_RANGE_CJK_UNIFIED_IDEOGRAPHS_EXTENSION_B },
		{ "Combining Diacritical Marks", CODEPOINTS_RANGE_COMBINING_DIACRITICAL_MARKS },
		{ "Combining Diacritical Marks For Symbols", CODEPOINTS_RANGE_COMBINING_DIACRITICAL_MARKS_FOR_SYMBOLS },
		{ "Combining Half Marks", CODEPOINTS_RANGE_COMBINING_HALF_MARKS },
		{ "Control Pictures", CODEPOINTS_RANGE_CONTROL_PICTURES },
		{ "Currency Symbols", CODEPOINTS_RANGE_CURRENCY_SYMBOLS },
		{ "Cypriot Syllabary", CODEPOINTS_RANGE_CYPRIOT_SYLLABARY },
		{ "Cyrillic", CODEPOINTS_RANGE_CYRILLIC },
		{ "Cyrillic Supplement", CODEPOINTS_RANGE_CYRILLIC_SUPPLEMENT },
		{ "Deseret", CODEPOINTS_RANGE_DESERET },
		{ "Devanagari", CODEPOINTS_RANGE_DEVANAGARI },
		{ "Dingbats", CODEPOINTS_RANGE_DINGBATS },
		{ "Enclosed Alphanumerics", CODEPOINTS_RANGE_ENCLOSED_ALPHANUMERICS },
		{ "Enclosed CJK Letters and Months", CODEPOINTS_RANGE_ENCLOSED_CJK_LETTERS_AND_MONTHS },
		{ "Ethiopic", CODEPOINTS_RANGE_ETHIOPIC },
		{ "General Punctuation", CODEPOINTS_RANGE_GENERAL_PUNCTUATION },
		{ "Geometric Shapes", CODEPOINTS_RANGE_GEOMETRIC_SHAPES },
		{ "Georgian", CODEPOINTS_RANGE_GEORGIAN},
		{ "Gothic", CODEPOINTS_RANGE_GOTHIC },
		{ "Greek Coptic", CODEPOINTS_RANGE_GREEK_COPTIC },
		{ "Greek Extended", CODEPOINTS_RANGE_GREEK_EXTENDED },
		{ "Gujarati", CODEPOINTS_RANGE_GUJARATI },
		{ "Gurmukhi", CODEPOINTS_RANGE_GURMUKHI },
		{ "Halfwidth and fullwidth forms", CODEPOINTS_RANGE_HALFWIDTH_AND_FULLWIDTH_FORMS },
		{ "Hangul Compatibility Jamo", CODEPOINTS_RANGE_HANGUL_COMPATIBILITY_JAMO },
		{ "Hangul Jamo", CODEPOINTS_RANGE_HANGUL_JAMO },
		{ "Hangul Syllabes", CODEPOINTS_RANGE_HANGUL_SYLLABLES },
		{ "Hanunoo", CODEPOINTS_RANGE_HANUNOO },
		{ "Hebrew", CODEPOINTS_RANGE_HEBREW },
		{ "High Surrogate Area", CODEPOINTS_RANGE_HIGH_SURROGATE_AREA },
		{ "Hiragana", CODEPOINTS_RANGE_HIRAGANA },
		{ "Ideographic Description Characters", CODEPOINTS_RANGE_IDEOGRAPHIC_DESCRIPTION_CHARACTERS },
		{ "IPA Extensions", CODEPOINTS_RANGE_IPA_EXTENSIONS },
		{ "Kanbun Kunten", CODEPOINTS_RANGE_KANBUN_KUNTEN },
		{ "Kangxi Radicals", CODEPOINTS_RANGE_KANGXI_RADICALS },
		{ "Kannada", CODEPOINTS_RANGE_KANNADA },
		{ "Katakana", CODEPOINTS_RANGE_KATAKANA },
		{ "Katakana Phoenetic Extensions", CODEPOINTS_RANGE_KATAKANA_PHONETIC_EXTENSIONS },
		{ "Khmer", CODEPOINTS_RANGE_KHMER },
		{ "Khmer Symbols", CODEPOINTS_RANGE_KHMER_SYMBOLS },
		{ "Lao", CODEPOINTS_RANGE_LAO },
		{ "Latin Basic", CODEPOINTS_RANGE_BASIC_LATIN },
		{ "Latin 1 supplement and C1 controls", CODEPOINTS_RANGE_C1_CONTROLS_AND_LATIN_1_SUPPLEMENT },
		{ "Latin Extended - A", CODEPOINTS_RANGE_LATIN_EXTENDED_A },
		{ "Latin Extended Additional", CODEPOINTS_RANGE_LATIN_EXTENDED_ADDITIONAL },
		{ "Latin Extended - B", CODEPOINTS_RANGE_LATIN_EXTENDED_B },
		{ "Letterlike Symbols", CODEPOINTS_RANGE_LETTERLIKE_SYMBOLS },
		{ "Limbu", CODEPOINTS_RANGE_LIMBU },
		{ "Linear B Ideograms", CODEPOINTS_RANGE_LINEAR_B_IDEOGRAMS },
		{ "Linear B Syllabary", CODEPOINTS_RANGE_LINEAR_B_SYLLABARY },
		{ "Low Surrogate Area", CODEPOINTS_RANGE_LOW_SURROGATE_AREA },
		{ "Malayalam", CODEPOINTS_RANGE_MALAYALAM },
		{ "Mathmatical Alphanumeric Symbols", CODEPOINTS_RANGE_MATHEMATICAL_ALPHANUMERIC_SYMBOLS },
		{ "Mathmatical Operators", CODEPOINTS_RANGE_MATHEMATICAL_OPERATORS },
		{ "Miscellaneous Mathematical Symbols A", CODEPOINTS_RANGE_MISCELLANEOUS_MATHEMATICAL_SYMBOLS_A },
		{ "Miscellaneous Mathematical Symbols B", CODEPOINTS_RANGE_MISCELLANEOUS_MATHEMATICAL_SYMBOLS_B },
		{ "Miscellaneous Symbols", CODEPOINTS_RANGE_MISCELLANEOUS_SYMBOLS },
		{ "Miscellaneous Symbols and Arrows", CODEPOINTS_RANGE_MISCELLANEOUS_SYMBOLS_AND_ARROWS },
		{ "Miscellaneous Technical", CODEPOINTS_RANGE_MISCELLANEOUS_TECHNICAL },
		{ "Mongolian", CODEPOINTS_RANGE_MONGOLIAN },
		{ "Musical Symbols", CODEPOINTS_RANGE_MUSICAL_SYMBOLS },
		{ "Myanmar", CODEPOINTS_RANGE_MYANMAR },
		{ "Number Forms", CODEPOINTS_RANGE_NUMBER_FORMS },
		{ "Ogham", CODEPOINTS_RANGE_OGHAM },
		{ "Old Italic", CODEPOINTS_RANGE_OLD_ITALIC },
		{ "Optical Character Recognition", CODEPOINTS_RANGE_OPTICAL_CHARACTER_RECOGNITION },
		{ "Oriya", CODEPOINTS_RANGE_ORIYA },
		{ "Osmanya", CODEPOINTS_RANGE_OSMANYA },
		{ "Phoenetic Extensions", CODEPOINTS_RANGE_PHONETIC_EXTENSIONS },
		{ "Runic", CODEPOINTS_RANGE_RUNIC },
		{ "Shavian", CODEPOINTS_RANGE_SHAVIAN },
		{ "Sinhala", CODEPOINTS_RANGE_SINHALA },
		{ "Small Form Variants", CODEPOINTS_RANGE_SMALL_FORM_VARIANTS },
		{ "Spacing Modifier Letters", CODEPOINTS_RANGE_SPACING_MODIFIER_LETTERS },
		{ "Specials", CODEPOINTS_RANGE_SPECIALS },
		{ "Sperscripts and Subscripts", CODEPOINTS_RANGE_SUPERSCRIPTS_AND_SUBSCRIPTS },
		{ "Supplemental Arrows A", CODEPOINTS_RANGE_SUPPLEMENTAL_ARROWS_A },
		{ "Supplemental Arrows B", CODEPOINTS_RANGE_SUPPLEMENTAL_ARROWS_B },
		{ "Supplemental Mathematical Operators", CODEPOINTS_RANGE_SUPPLEMENTAL_MATHEMATICAL_OPERATORS },
		{ "Syriac", CODEPOINTS_RANGE_SYRIAC },
		{ "Tagalog", CODEPOINTS_RANGE_TAGALOG },
		{ "Tagbanwa", CODEPOINTS_RANGE_TAGBANWA },
		{ "Tags", CODEPOINTS_RANGE_TAGS },
		{ "Tai Le", CODEPOINTS_RANGE_TAI_LE },
		{ "Tai Xuan Jing Symbols", CODEPOINTS_RANGE_TAI_XUAN_JING_SYMBOLS },
		{ "Tamil", CODEPOINTS_RANGE_TAMIL },
		{ "Telegu", CODEPOINTS_RANGE_TELUGU },
		{ "Thaana", CODEPOINTS_RANGE_THAANA },
		{ "Thai", CODEPOINTS_RANGE_THAI },
		{ "Tibetan", CODEPOINTS_RANGE_TIBETAN },
		{ "Ugaritic", CODEPOINTS_RANGE_UGARITIC },
		{ "Unified Canadian Aboriginal Syllabics", CODEPOINTS_RANGE_UNIFIED_CANADIAN_ABORIGINAL_SYLLABICS },
		{ "Variation Selectors", CODEPOINTS_RANGE_VARIATION_SELECTORS },
		{ "Variation Selectors Supplement", CODEPOINTS_RANGE_VARIATION_SELECTORS_SUPPLEMENT },
		{ "Yijing Hexagram Symbols", CODEPOINTS_RANGE_YIJING_HEXAGRAM_SYMBOLS},
		{ "Yi Radicals", CODEPOINTS_RANGE_YI_RADICALS },
		{ "Yi Syllables", CODEPOINTS_RANGE_YI_SYLLABLES }
	};

	std::cout << std::fixed;
	for (auto& e : codepoint_ranges) std::cout << "Occupancy " << font.getCodepointOccupancy(e.second)*100 << "%\t in group " << e.first << std::endl;

}


//while the purpose of this library is to inspect, pack, compute SDFs and export fonts this example is provided to show the basic principles of rendering characters.
//this example does not include the rendering process (e.g. shaders + textures + state) and is not written for speed (e.g. unorderd map over codepoints)
void test_renderable_quads() {
	using namespace lap::font;
	std::cout << "Test renderable quads " << std::endl;
	FontAtlas fontatlas(&std::cout);	
	fontatlas.import("../assets/DejaVuSerif.ttf", 20);
	bool success = fontatlas.pack(1024, 1024, 2, 1, true, 2);
	if (!success) {
		std::cout << "Failed packing" << std::endl;
		return;
	}
	const Font& font = fontatlas.getFonts().at(0);

	// we need to decode the strings (codepoints in variable length encoding) into integer codepoints
	// the decoder is written over http://bjoern.hoehrmann.de/utf-8/decoder/dfa/
	struct UTF8decoder {
		uint32_t decode(uint32_t* state, uint32_t* codep, uint32_t byte) {
			static const uint8_t utf8d[] = {
				0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0, // 00..1f
				0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0, // 20..3f
				0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0, // 40..5f
				0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0, // 60..7f
				1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,9,9,9,9,9,9,9,9,9,9,9,9,9,9,9,9, // 80..9f
				7,7,7,7,7,7,7,7,7,7,7,7,7,7,7,7,7,7,7,7,7,7,7,7,7,7,7,7,7,7,7,7, // a0..bf
				8,8,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2, // c0..df
				0xa,0x3,0x3,0x3,0x3,0x3,0x3,0x3,0x3,0x3,0x3,0x3,0x3,0x4,0x3,0x3, // e0..ef
				0xb,0x6,0x6,0x6,0x5,0x8,0x8,0x8,0x8,0x8,0x8,0x8,0x8,0x8,0x8,0x8, // f0..ff
				0x0,0x1,0x2,0x3,0x5,0x8,0x7,0x1,0x1,0x1,0x4,0x6,0x1,0x1,0x1,0x1, // s0..s0
				1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,0,1,1,1,1,1,0,1,0,1,1,1,1,1,1, // s1..s2
				1,2,1,1,1,1,1,2,1,2,1,1,1,1,1,1,1,1,1,1,1,1,1,2,1,1,1,1,1,1,1,1, // s3..s4
				1,2,1,1,1,1,1,1,1,2,1,1,1,1,1,1,1,1,1,1,1,1,1,3,1,3,1,1,1,1,1,1, // s5..s6
				1,3,1,1,1,1,1,3,1,3,1,1,1,1,1,1,1,3,1,1,1,1,1,1,1,1,1,1,1,1,1,1 // s7..s8
			};
			uint32_t type = utf8d[byte];

			*codep = (*state != 0) ?
				(byte & 0x3fu) | (*codep << 6) :
				(0xff >> type) & (byte);

			*state = utf8d[256 + *state * 16 + type];
			return *state;
		}
		const std::vector<int> decode(const std::string& text) {
			std::vector<int> result;
			const uint8_t *s = (const uint8_t*)text.c_str();
			uint32_t codepoint;
			uint32_t state = 0;

			for (; *s; ++s)	if (!decode(&state, &codepoint, *s)) result.push_back(codepoint);
			if (state != 0) std::cout << "The string is not well-formed" << std::endl;

			//nrvo
			return result;
		}
	};

	//characters != codepoints, always decode.
	const std::vector<int> codepoints = UTF8decoder().decode("The quick brown fox jumps over the lazy dog.");

	//textured quads
	struct TexturedQuad {
		//texture coordinates
		float umin, umax, vmin, vmax;
		//(screen) spatial coordinates in pixels
		float xmin, xmax, ymin, ymax;
	};

	//for each point compute textured quad
	const int write_glyph_size = 30;
	float scale = (float)write_glyph_size / font.getGlyphSize();
	const unsigned int glyph_image_width = font.getGlyphImageWidth();
	const unsigned int glyph_image_height = font.getGlyphImageHeight();
	float write_xpos = 100, write_ypos = 100;

	std::vector<TexturedQuad> quads;
	for (int i = 0; i < codepoints.size();i++) {
		//get codepoints
		int codepoint = codepoints[i];

		//do kerning (some codepoint glyphs have special distance rules)
		if (i > 0) {
			auto kernmap = font.getKerningMap();
			int codepoint_prev = codepoints[i - 1];
			auto kernquery = kernmap.find({ codepoint, codepoint_prev });
			//if there is a kerning pair between this codepoint and the previous codepoint then perform the kern xadvance adjustment
			if (kernquery != kernmap.end()) write_xpos = write_xpos + scale * (*kernquery).second;
		}
		
		//add a blank quad
		quads.push_back({});
		TexturedQuad& quad = quads.back();

		//get glyph, if codepoint is unknown the glyph will by the undefined glyph.
		const GlyphData& gd = font.getCodepointToGlyph(codepoint);

		//texture coordinates
		quad.umin = gd.x / glyph_image_width;
		quad.vmin = gd.y / glyph_image_height;
		quad.umax = (gd.x + gd.width) / glyph_image_width;
		quad.vmax = (gd.y + gd.height) / glyph_image_height;

		//quad coordinates on the screen (in pixels, not in normalized)
		quad.xmin = write_xpos + scale * gd.xoffset;
		quad.xmax = write_xpos + scale * (gd.xoffset + gd.width);
		quad.ymin = write_ypos + scale * gd.yoffset;
		quad.ymax = write_ypos + scale * (gd.yoffset + gd.height);
		
		//advance the write position (no word wrapping)
		write_xpos = write_xpos + scale * gd.xadvance;
	}

	//advance to next line
	write_ypos = write_ypos + scale * font.getLineHeight();

	std::cout << std::fixed << std::setprecision(2);
	for (auto& q : quads) std::cout << "position = [" << q.xmin << " , " << q.ymin << "]->[" << q.xmax << " , " << q.ymax << "] \t, uv= [" << q.umin << ", " << q.vmin << "]->[" << q.umax << ", " << q.vmax << "]"<< std::endl;

}

//this text exports a cpp file which is used by lap_outputui
void test_export_for_outputui() {
	using namespace lap::font;
	std::cout << "Test export for outputui" << std::endl;
	FontAtlas fontatlas(&std::cout);	//create a font atlas object
	fontatlas.import("../assets/DejaVuSansMono.ttf", 26, { CODEPOINTS_RANGE_BASIC_LATIN }); //monospace -> no kerning -> faster text rendering
	bool success = fontatlas.pack(256, 256, 5, 1, true, 10);
	if (!success) {
		std::cout << "Failed packing" << std::endl;
		return;
	}
	//export to CPP
	fontatlas.exportImages("../assets/output_export_outputui_img.png", "../assets/output_export_outputui_sdf.png");
	fontatlas.exportCPP("../assets/output_export_outputui_cpp.hpp");
}


int main(int argc, char* argv[]){
	
	test_basic();
	//test_multiple_fonts();
	//test_ranges();
	//test_export();
	//test_languages();
	//test_codepoints();
	//test_renderable_quads();
	//test_export_for_outputui();

	std::cout << "Finished" << std::endl;
	std::cin.get();
	return 0;
}
