
# README #

Lap Font Tools is a small library for font-related tasks. It can be used to import fonts from ttf files, pack them together
into a single image atlas, export fonts to various formats(BMFONT, .hpp include file, binary), analyze font files, generate 
kerning pairs and codepoint maps. LAP Font Tools can also compute the Signed Distance Function (SDF) of packed fonts. SDFs 
are used in state of the art text rendering methods to increase the quality of fonts magnified beyond their normal resolution.
The library is intended to be used as an offline tool and it uses code from 
	- stb_truetype( https://github.com/nothings/stb/blob/master/stb_truetype.h), 
	- stb_rectpack (https://github.com/nothings/stb/blob/master/stb_rect_pack.h)
 	- economical utf8 (http://bjoern.hoehrmann.de/utf-8/decoder/dfa/).
These dependencies are packed in the lap_font_tools_dep.hpp file, thus LAP FontTools is deployable with only 3 files. 
It is important to note that the fonts and the font atlas are valid ONLY AFTER successfully finishing the packing operations 
(see the examples section or the tests in test.cpp).

version 1.00
author Lucian Petrescu( lucian (dot) petrescu (dot) 24 (at) gmail (dot) com )

### SCREENSHOTS ###

Fonts bundled into a single image

 ![functions are bundled into groups](img/output_basic_img.png) 

The SDF (Signed Distance Function) of the previously packed fonts

 ![functions are bundled into groups](img/output_basic_imgsdf.png) 

Large font

 ![functions are bundled into groups](img/largesdf_img.png) 

Large font SDF

 ![functions are bundled into groups](img/largesdf_sdf.png) 

Export to .FNT BMFONT format [http://www.angelcode.com/products/bmfont/doc/file_format.html] : glyphs

 ![functions are bundled into groups](img/fntformat.png)


Export to .FNT BMFONT format [http://www.angelcode.com/products/bmfont/doc/file_format.html] : kerning pairs

 ![functions are bundled into groups](img/fntformat_kerns.png)

###UNICODE###
This section discusses the basics of Unicode and is largely based on the stb_truetype documentation.

- Codepoint:
	Characters are defined by unicode codepoints, e.g. 65 is uppercase A, 231 is lowercase c with a cedilla, 
	0x7e30 is the hiragana for "ma".
- Codepoint Range:
	Codepoints are usually organized by the Unicode Consortium into ranges, e.g. latin glyph range, arabic
	glyphs range, etc. These ranges are included in LAP Font Tools, e.g. CODEPOINTS_RANGE_CYRILLIC.
- Glyph:
	A visual character shape (every codepoint is rendered as some glyph). If no glyph is present in a font
	then an "undefined codepoint" glyph is normally used.
- Baseline:
	Glyph shapes are defined relative to a baseline, which is the bottom of uppercase characters. Characters 
	extend both above and below the baseline.
- Font Height:
	The number of pixels between two consecutive lines of text with the font.
- Font Atlas:
	An image in which glyphs from one or multiple fonts are packed together. 
- Current Point:
	As you draw text to the screen, you keep track of a "current point" which is the origin of each character. 
	The current point's vertical position is the baseline. Even "baked fonts" use this model.
- Kerning pair:
	Sometimes fonts define variable distance between glyphs, for example the AV glyphs overlap to produce a 
	better font aspect. This inter-glyph distance adjustment process is called kerning and it is represented 
	with kerning pairs.
- Monospace:
	Monospaces fonts have constant spacing between glyphs, thus they have no kerning pairs.

		
###EXPORT FORMAT###
LAP Font Tools can export to the following formats:
	- BMFONT format (http://www.angelcode.com/products/bmfont/doc/file_format.html)
	- a CPP(header) format which can be included directly by any cpp file. 
	- a packed binary format (see test example)
		
		
###PROPERTIES###
- c++11
- object oriented
- plug and play, just include the 3 files (lap\_font\_tools.cpp, lap\_font\_tools.hpp, lap\_font\_tools\_dep.hpp) to your build system
- works with TrueType 1.0 fonts
- can pack multiple fonts into a single image atlas
- can compute the Signed Distance Function (SDF) of the image atlas
- can query codepoint support with ranges
- packs library dependencies into lap\_font\_tools_dep.hppwith the /dep\_gen/generator.py script (does not need to be included in the build system)
- exposes codepoint ranges through the /dep\_gen/generator\_unicode\_ranges.py script (does not need to be included in the build system)
- exports kerning pairs as a kerningpair->advance map
- exports supported codepoints as a codepoint->glyph map


### COMPILING ###

Visual studio 2015 project&solution files can be found in the **vstudio** folder. Makefiles for gnu/gcc ( **makefile** ) and for visual studio ( **makefilevs.bat** ) are provided in the root folder. All methods will generate objs, pdbs, exes, etc in **bin**.

### EXAMPLES ###

- basic usage:

		#include "lap_font_tools.hpp"
		...
		lap::font::FontAtlas fontatlas(&std::cout);					//create a font atlas object logging on std::cout
		fontatlas.import("../assets/DejaVuSansMono.ttf", 25);		//import a font into the font atlas object, glyphs will be rasterized to size 25 pixels
		bool success = fontatlas.pack(1024, 1024, 1, 1);			//pack the fonts inside the font atlas object into a 1024x1024 image
		if(success){
			const lap::font::Font& f= fontatlas.getFonts().at(0);	//access font
			... DO WORK HERE ...
		}
		
- pack multiple fonts and compute SDF
		
		#include "lap_font_tools.hpp"
		...
		lap::font::FontAtlas fontatlas(&std::cout);					//create a font atlas object logging on std::cout
		fontatlas.import("../assets/DejaVuSansMono.ttf", 18);		//import a font into the font atlas object, glyphs will be rasterized to size 18 pixels
		fontatlas.import("../assets/DejaVuSans.ttf",12);			//import a font into the font atlas object, glyphs will be rasterized to size 12 pixels
		fontatlas.import("../assets/DejaVuMathTeXGyre.ttf", 19);	//import a font into the font atlas object, glyphs will be rasterized to size 19 pixels
		fontatlas.import("../assets/DejaVuSansCondensed.ttf", 14);	//import a font into the font atlas object, glyphs will be rasterized to size 14 pixels
		bool success = fontatlas.pack(2048, 2048, 5, 1, true, 10);	//pack the fonts inside the font atlas object into a 2048x2048 image and compute the SDF
		if(success){
			const lap::font::Font& = fontatlas.getFonts().at(0);	//access font
			... DO WORK HERE ...
		}

- export
		
		#include "lap_font_tools.hpp"
		...
		lap::font::FontAtlas fontatlas(&std::cout);					//create a font atlas object logging on std::cout
		fontatlas.import("../assets/DejaVuSansMono.ttf", 18);		//import a font into the font atlas object, glyphs will be rasterized to size 18 pixels
		fontatlas.import("../assets/DejaVuSerif.ttf",20);			//import a font into the font atlas object, glyphs will be rasterized to size 20 pixels
		bool success = fontatlas.pack(2048, 2048, 5, 1, true, 10);	//pack the fonts inside the font atlas object into a 2048x2048 image and compute the SDF
		if(success){
			fontatlas.exportBMFONT("../assets/output_export_bmfonttxt_fnt.fnt", "../assets/output_export_bmfonttxt_img.png", "../assets/output_export_bmfonttxt_sdf.png");
			fontatlas.exportCPP("../assets/output_export_cpp.hpp");
			fontatlas.exportPacked("../assets/output_export_packed.pak");
		}

- various 

		#include "lap_font_tools.hpp"
		...
		lap::font::FontAtlas fontatlas(&std::cout);					//create a font atlas object logging on std::cout
		//import only some codepoint ranges from a ttf font
		fontatlas.import("../assets/DejaVuSansMono.ttf", 25, { CODEPOINTS_RANGE_BASIC_LATIN, CODEPOINTS_RANGE_ARABIC, CODEPOINTS_RANGE_CYRILLIC, CODEPOINTS_RANGE_HIRAGANA });
		bool success = fontatlas.pack(2048, 2048, 5, 1, true, 10);	//pack the fonts inside the font atlas object into a 2048x2048 image and compute the SDF
		if(success){
			const lap::font::Font& f= fontatlas.getFonts().at(0);	//access font

			//test the occupancy of glyphs from the arabic codepoint range
			std::cout << "Font has " << f.getCodepointOccupancy(CODEPOINTS_RANGE_ARABIC) << " occupancy over arabic glyphs" << std::endl;
			//test the occupancy of glyphs from the cyrillic and hiragana codepoint ranges
			std::cout << "Font has " << f.getCodepointOccupancy({ CODEPOINTS_RANGE_CYRILLIC, CODEPOINTS_RANGE_HIRAGANA }) << " occupancy over cyrillic and hiragana glyphs" << std::endl;

			const std::string& name = f.getName();
			unsigned int num_glyphs = f.getNumGlyphs();
			unsigned int num_kerning_pairs = f.getNumKerningPairs();
			unsigned int font_glyph_size = f.getGlyphSize();
			bool exists_undefined = f.hasCodepoint(9443423);
			bool exists_a = f.hasCodepoint(67);
			bool exists_group = f.hasCodepoints({ 67,68,69,70,71,72,73,74 });
			unsigned int line_height = f.getLineHeight();
			unsigned int baseline = f.getBaseline();
			unsigned int padding = f.getPadding();
			unsigned int oversampling = f.getOversampling();
			unsigned int sdfdistance = f.getSDFdistance();
			bool sdfcomputed = f.getSDFcomputed();
		}

- further examples can be found in the testing file (test.cpp)

### DOCUMENTATION ###

a Doxyfile is provided in **doc**, which can be used with Doxygen to generate documentation in HTML format. 

### COMPATIBILITY ###

tested with visual studio 2015, MinGW x64 4.0, Ubuntu 15 + gcc 4.8.4, MacOS El Capitan + clang 3.7.1.