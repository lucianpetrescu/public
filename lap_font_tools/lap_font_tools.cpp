///---------------------------------------------------------------------------------------------------------------------
/// The MIT License (MIT)
/// 
/// Copyright (c) 2016 Lucian Petrescu, lucian (dot) petrescu (dot) 24 (at) gmail (dot) com
/// 
/// Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation 
/// files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, 
/// modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software 
/// is furnished to do so, subject to the following conditions:
/// 
/// The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
/// 
/// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
/// OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE 
/// LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR 
/// IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
///---------------------------------------------------------------------------------------------------------------------

#include "lap_font_tools.hpp"
#include "lap_font_tools_dep.hpp"
#include <fstream>
#include <iostream>
#include <algorithm>
#include <locale>
#include <sstream>

namespace lap {
	namespace font {

		///-------------------------------------------------------------------------------------------------------------
		//codepoints
		bool Font::hasCodepoint(unsigned int codepoint) const {
			if (codepoint_to_glyphdata_map.find(codepoint) == codepoint_to_glyphdata_map.end()) return false;
			return true;
		}
		bool Font::hasCodepoints(const std::vector<int>& codepoints) const {
			for (auto& c : codepoints) if (!hasCodepoint(c)) return false;
			return true;
		}
		float Font::getCodepointOccupancy(const std::pair<int, int>& range) const {
			assert(range.second > range.first);
			int count = 0;
			for (int codepoint = range.first; codepoint < range.second; codepoint++) if (hasCodepoint(codepoint)) count++;
			return (float)count / (range.second - range.first);
		}
		float Font::getCodepointOccupancy(const std::vector<std::pair<int, int>>& ranges) const {
			int count = 0;
			int domain = 0;
			for (auto& range : ranges) {
				assert(range.second > range.first);
				domain += (range.second - range.first);
				for (int codepoint = range.first; codepoint < range.second; codepoint++) if (hasCodepoint(codepoint)) count++;
			}
			return (float)count / domain;
		}
		float Font::getCodepointOccupancy(const std::vector<int>& codepoints) const {
			if (codepoints.size() == 0) return 0;
			int count = 0;
			for (auto& codepoint : codepoints) if (hasCodepoint(codepoint)) count++;
			return (float)count / codepoints.size();
		}

		const GlyphData& Font::getCodepointToGlyph(unsigned int codepoint) const {
			const auto& result = codepoint_to_glyphdata_map.find(codepoint);
			if (result == codepoint_to_glyphdata_map.end()) return glyph_undefined;
			return (*result).second;
		}
		const GlyphData Font::getUndefinedGlyph() const {
			return glyph_undefined;
		}
		const std::map<int, GlyphData>& Font::getCodepointToGlyphMap() const {
			return codepoint_to_glyphdata_map;
		}
		const std::map<KerningPair, float> Font::getKerningMap() const {
			return kerning_map;
		}
		const std::string& Font::getName() const {
			return name;
		}
		unsigned int Font::getGlyphSize() const {
			return glyph_size;
		}
		unsigned int Font::getNumGlyphs() const {
			return (unsigned int) codepoint_to_glyphdata_map.size();
		}
		unsigned int Font::getNumKerningPairs() const {
			return (unsigned int) kerning_map.size();
		}
		unsigned int Font::getLineHeight() const {
			return line_gap;
		}

		unsigned int Font::getPadding() const {
			return padding;
		}
		unsigned int Font::getOversampling() const {
			return oversampling;
		}
		unsigned int Font::getSDFdistance() const {
			return sdf_distance;
		}
		bool Font::getSDFcomputed() const {
			return sdf_computed;
		}
		unsigned int Font::getBaseline() const {
			return baseline;
		}
		unsigned int Font::getGlyphImageWidth() const {
			return image_width;
		}
		unsigned int Font::getGlyphImageHeight() const {
			return image_height;
		}



		///-------------------------------------------------------------------------------------------------------------
		struct InternalFontDescriptor {
			unsigned char* filebuffer = nullptr;
			unsigned int offset;
			unsigned int index;
			unsigned int size;
			std::vector<int> codepoints;
			std::string name;
			stbtt_fontinfo info;
		};

		struct InternalFontAtlasState {
			~InternalFontAtlasState() {
				for (auto& fb : filebuffers) delete[] fb;
			}
			std::vector<unsigned char*> filebuffers;
			std::vector<InternalFontDescriptor> fontdescs;
		};

		///-------------------------------------------------------------------------------------------------------------
		FontAtlas::FontAtlas(std::ostream* in_logger) {
			logger = in_logger;
			state = new InternalFontAtlasState();
		}
		FontAtlas::~FontAtlas() {
			//destroys all internal state
			delete reinterpret_cast<InternalFontAtlasState*>(state);
		}

		const std::vector<Font>& FontAtlas::getFonts() const {
			return fonts;
		}

		void FontAtlas::import(const std::string& filename, unsigned int size) {
			std::vector<std::pair<int, int>> pairs;
			import(filename, size, pairs);
		}
		void FontAtlas::import(const std::string& filename, unsigned int size, const std::vector<std::pair<int, int>>& ranges) {
			//check extension
			auto ext_pctidx = filename.find_last_of('.');
			auto ext_diridx = filename.find_last_of("/\\");
			std::string ext;
			if (ext_pctidx != std::string::npos && ext_diridx != std::string::npos && ext_pctidx > ext_diridx) ext = filename.substr(filename.find_last_of('.') + 1);
			else ext = "no";
			std::transform(ext.begin(), ext.end(), ext.begin(), ::tolower);
			if (ext != "ttf") {
				if (logger) (*logger) << "[FONT TOOLS] this utility supports only TTF files, " << filename << " has " << ext << " extension." << std::endl;
				assert(false);
			}
			//get font name
			std::string name = filename.substr(ext_diridx + 1, ext_pctidx-ext_diridx-1);

			//try to open the ttf file
			std::ifstream file(filename.c_str(), std::ifstream::binary);
			if (!file.is_open()) {
				if (logger) (*logger) << "[FONT TOOLS] failed to open file " << filename << std::endl;
				assert(false);
			}
			if (logger) (*logger) << "[FONT TOOLS] loaded font "<<name<<" from " << filename << std::endl;

			//read file contents
			file.seekg(0, file.end);
			unsigned int filesize = (unsigned int)file.tellg();
			file.seekg(0, file.beg);
			unsigned char *filebuffer = new unsigned char[filesize];
			file.read((char*)filebuffer, filesize);
			file.close();

			//add the filebuffer to the state (will be used in packing)
			reinterpret_cast<InternalFontAtlasState*>(state)->filebuffers.push_back(filebuffer);

			//load all fonts in file
			int offset = 0;
			int index = -1;
			while (true) {
				//find next font in file (ttfs can contain multiple fonts)
				offset = stbtt_GetFontOffsetForIndex(filebuffer, ++index);
				if (offset == -1) break;

				//load current font
				stbtt_fontinfo fi;
				int result = stbtt_InitFont(&fi, filebuffer, offset);
				if (result == 0) {
					if (logger) (*logger) << "[FONT TOOLS] ttf file " << filename << " does not have a proper TTF structure" << std::endl;
					assert(false);
				}

				//find all codepoints from this font which reside in the requested codepoint ranges
				//IMPROVE : use an interval tree.
				std::vector<int> codepoints; codepoints.reserve(10000);
				for (int codepoint = 0; codepoint < 0xE01EF; codepoint++) {
					if (stbtt_FindGlyphIndex(&fi, codepoint) > 0) {
						//load only range-specified
						if (ranges.size()>0) {
							for (auto& p : ranges) {
								if (p.first <= codepoint && p.second >= codepoint) {
									codepoints.push_back(codepoint);
								}
							}
						}
						//or load all when no range-specified data is given
						else {
							codepoints.push_back(codepoint);
						}
					}
				}

				//guarantee the existence of the "unrepresented" glyph, this is defined as unused so there will be no collision with another codepoint glyph
				codepoints.push_back(CODEPOINT_UNDEFINED);

				//add font information to state, will be used in the packing stage
				reinterpret_cast<InternalFontAtlasState*>(state)->fontdescs.emplace_back(InternalFontDescriptor());
				InternalFontDescriptor& fd = reinterpret_cast<InternalFontAtlasState*>(state)->fontdescs.back();
				fd.codepoints = std::move(codepoints);
				fd.filebuffer = filebuffer;
				fd.name = name;
				fd.offset = offset;
				fd.index = index;
				fd.info = fi;
				fd.size = size;

				if (logger) (*logger) << "             Added font #" << fd.index << " from offset= " << fd.offset << " with " << fd.codepoints.size() << " codepoints and size = "<<size <<"." << std::endl;
			}
		}


		bool FontAtlas::pack(unsigned int in_width, unsigned int in_height, unsigned int in_padding, unsigned int in_oversampling, bool in_sdf_compute, unsigned int in_sdf_distance) {
			//reality check
			assert(in_width > 0 && in_height > 0);

			//clean up internal state, but keep internal fonts and file buffers
			fonts.clear();
			InternalFontAtlasState* istate = reinterpret_cast<InternalFontAtlasState*>(state);
			width = in_width;
			height = in_height;
			padding = in_padding;
			oversampling = in_oversampling;
			sdf_compute = in_sdf_compute;
			sdf_distance = in_sdf_distance;
			pixels.clear(); 
			pixels.resize(width*height, 0);
			pixels_sdf.clear();	 
			pixels_sdf.resize(width*height, 0);
			

			//create a packing context
			stbtt_pack_context pack_context;
			stbtt_PackSetOversampling(&pack_context, oversampling, oversampling);
			int result = stbtt_PackBegin(&pack_context, pixels.data() , width, height, 0, padding * 2, nullptr);
			assert(result);

			//for each font descriptor pack the defined font
			for (auto& fd : istate->fontdescs) {
				//create packing range
				std::vector<stbtt_packedchar> packedchars(fd.codepoints.size());
				stbtt_pack_range range;
				range.array_of_unicode_codepoints = fd.codepoints.data();
				range.chardata_for_range = packedchars.data();
				range.num_chars = (int)fd.codepoints.size();
				range.font_size = (float)fd.size;
				
				//pack range
				result = stbtt_PackFontRanges(&pack_context, fd.filebuffer, fd.index, &range, 1);
				if (result == 0) {
					if (logger) (*logger) << "[FONT TOOLS] PACKING FAILED : could not pack fonts inside an " << width << "x" << height << " image." << std::endl;
					return false;
				}

				//create a new font object
				fonts.push_back(Font());
				Font& font = fonts.back();

				//basic properties
				font.name = fd.name;
				font.glyph_size = fd.size;
				font.padding = padding;
				font.oversampling = oversampling;
				font.sdf_distance = sdf_compute?sdf_distance:0;
				font.sdf_computed = sdf_compute;
				font.image_width = width;
				font.image_height = height;

				//store the codepoints
				for (int i = 0; i < fd.codepoints.size(); i++) {
					stbtt_packedchar& pc = packedchars[i];
					GlyphData gd;
					gd.x = pc.x0;
					gd.y = pc.y0;
					gd.width = (float)(pc.x1 - pc.x0);
					gd.height = (float)(pc.y1 - pc.y0);
					gd.xoffset = pc.xoff;
					gd.yoffset = pc.yoff;
					gd.xadvance = pc.xadvance;
					int codepoint = fd.codepoints[i];

					//undefined codepoint is special and is not added to the map
					if (codepoint == CODEPOINT_UNDEFINED) font.glyph_undefined = gd;
					else font.codepoint_to_glyphdata_map[codepoint] = gd;
				}

				//compute scaling factor
				float scale = stbtt_ScaleForPixelHeight(&fd.info, (float)fd.size);

				//compute the kerning pairs, O(n^2) .. slow code but no other solution because stb_truetype does not expose the kerning table.
				for (int i = 0; i < fd.codepoints.size() - 1; i++) {
					for (int j = 1; j < fd.codepoints.size(); j++) {
						KerningPair kp;
						kp.codepoint1 = fd.codepoints[i];
						kp.codepoint2 = fd.codepoints[j];
						int advance = stbtt_GetCodepointKernAdvance(&fd.info, kp.codepoint1, kp.codepoint2);
						//add to kerning map only if the advance is different than 0
						if (advance != 0) font.kerning_map[kp] = advance * scale;
					}
				}

				//compute line height
				int ascent, descent, linegap;
				stbtt_GetFontVMetrics(&fd.info, &ascent, &descent, &linegap);
				font.line_gap = (int)((ascent - descent + linegap) *scale);
				font.baseline = (int)(ascent * scale);
			}

			//end pack
			stbtt_PackEnd(&pack_context);

			//log
			if (logger) (*logger) << "[FONT TOOLS] packed fonts inside an " << width << "x" << height << " image." << std::endl;

			
			if (sdf_compute) {
				//notify padding 
				if(padding < sdf_distance) if (logger) (*logger) << "[FONT TOOLS] WARNING: sdf distance (" << sdf_distance << ") is larger than the the glyph padding(" << padding << "). Artifacts may occur in sdf map." << std::endl;

				//compute sdf , basically decouple energy transport from source (glyph pixels) to vicinty, solved linearly in pixels (not quadratically), cheaper than a linear blur.
				int delta = (int) (255.0f / (sdf_distance + 1));
				int transport = 0;

				//vertical transport for each column
				for (unsigned int i = 0; i < width; i++) {
					for (int j = 0; j < (int)height;j++){
						int index = j*width + i;
						if (pixels[index] > 25) transport = 255;			// transport only glyph energy to guarantee kernel size (take care of aa)
						if (transport > pixels_sdf[index]) pixels_sdf[index] = transport;
						transport -= delta;
					}
					for (int j = (int)height-1; j >=0; j--) {
						int index = j*width + i;
						if (pixels[index] > 25) transport = 255;			// transport only glyph energy to guarantee kernel size (take care of aa)
						if (transport > pixels_sdf[index]) pixels_sdf[index] = transport;
						transport -= delta;
					}
				}

				//horizontal transport for each row
				transport = 0;
				for (unsigned int j = 0; j < height; j++) {
					for (int i = 0; i < (int)width; i++) {
						int index = j*width + i;
						if (transport > pixels_sdf[index]) pixels_sdf[index] = transport;
						transport = pixels_sdf[index] - delta;				// transport any energy
					}
					for (int i = (int)width - 1; i >= 0; i--) {
						int index = j*width + i;
						if (transport > pixels_sdf[index]) pixels_sdf[index] = transport;
						transport = pixels_sdf[index] - delta;				// transport any energy
					}
				}

				if (logger) (*logger) << "[FONT TOOLS] sdf computed." << std::endl;
			}

			//finished with success
			return true;
		}


		unsigned int FontAtlas::getWidth() const {
			return width;
		}
		unsigned int FontAtlas::getHeight() const {
			return height;
		}
		unsigned int FontAtlas::getPadding() const {
			return padding;
		}
		unsigned int FontAtlas::getOversampling() const {
			return oversampling;
		}
		bool FontAtlas::getSDFcomputed() const {
			return sdf_compute;
		}
		unsigned int FontAtlas::getSDFdistance() const {
			return sdf_distance;
		}
		const std::vector<unsigned char>& FontAtlas::getPixels() const {
			return pixels;
		}
		const std::vector<unsigned char>& FontAtlas::getPixelsSDF() const {
			return pixels_sdf;
		}


		void FontAtlas::exportImages(const std::string& img_filename, const std::string& sdf_filename) {
			stbi_write_png(img_filename.c_str(), width, height, 1, pixels.data(), 0);
			if(sdf_filename !="") stbi_write_png(sdf_filename.c_str(), width, height, 1, pixels_sdf.data(), 0);

			if (logger) (*logger) << "[FONT TOOLS] exported atlas pixels to "<<img_filename<<" and atlas SDF pixels to "<<sdf_filename<<"." << std::endl;
		}


		void FontAtlas::exportBMFONT(const std::string& fnt_filename, const std::string& img_filename, const std::string& sdf_filename) {
			//write the fnt file
			std::fstream file(fnt_filename.c_str(), std::fstream::out);
			file << "<?xml version=\"1.0\"?>" << std::endl;

			//for each font
			for (auto& f : fonts) {
				file << "<font>" << std::endl;

				//info
				file << "\t<info face=\"" << f.name << "\" size=\"" << f.glyph_size << "\" bold=\"0\" italic=\"0\" charset=\"\" unicode=\"1\" stretchH=\"100\" ";
				if (f.oversampling > 0) file << "smooth=\"1\" aa=\""<<f.oversampling<<"\" ";
				else file << "smooth=\"0\" aa=\"1\" ";
				file << "padding=\"" << f.padding << "," << f.padding << "," << f.padding << "," << f.padding << "\" spacing=\"" << f.padding << "," << f.padding << "\" outline=\"0\" />" << std::endl;

				//common
				file << "\t<common lineHeight=\"" << f.line_gap << "\" base=\"" << f.baseline << "\" scaleW=\"" << f.image_width << "\" scaleH=\"" << f.image_height << "\" pages=\"1\" packed=\"0\"";
				file << " alphaChnl=\"3\" redChnl=\"1\" greenChnl=\"3\" blueChnl=\"3\" />" << std::endl;

				//pages
				auto imgfile = img_filename.substr(img_filename.find_last_of("/\\") + 1);
				file << "\t<pages>\n\t\t<page id=\"0\" file=\"" << imgfile;
				if (sdf_filename != "") {
					auto sdffile = sdf_filename.substr(sdf_filename.find_last_of("/\\") + 1);
					file << "\" sdffile=\"" << sdffile << "\"";
				}else file << "\" sdffile=\"\"";
				file <<	" />\n\t</pages>" << std::endl;
				
				//chars /codepoints
				file << "\t<chars count=\"" << f.codepoint_to_glyphdata_map.size() << "\">" << std::endl;
				for (auto& e : f.codepoint_to_glyphdata_map) {
					file << "\t\t<char id=\"" << (unsigned int)e.first;
					file << "\" x=\"" << (unsigned int)std::round(e.second.x);
					file << "\" y=\"" << (unsigned int)std::round(e.second.y);
					file << "\" width=\"" << (unsigned int)std::round(e.second.width);
					file << "\" height=\"" << (unsigned int)std::round(e.second.height);
					file << "\" xoffset=\"" << (int)std::round(e.second.xoffset);
					file << "\" yoffset=\"" << (int)std::round(e.second.yoffset);
					file << "\" xadvance=\"" << (int)std::round(e.second.xadvance);
					file << "\" page=\"0\" chnl=\"4\"/>" << std::endl;
				}
				//unknown glyph
				file << "\t\t<char id=\"" << (unsigned int)CODEPOINT_UNDEFINED;
				file << "\" x=\"" << (unsigned int)f.glyph_undefined.x;
				file << "\" y=\"" << (unsigned int)f.glyph_undefined.y;
				file << "\" width=\"" << (unsigned int)f.glyph_undefined.width;
				file << "\" height=\"" << (unsigned int)f.glyph_undefined.height;
				file << "\" xoffset=\"" << (int)f.glyph_undefined.xoffset;
				file << "\" yoffset=\"" << (int)f.glyph_undefined.yoffset;
				file << "\" xadvance=\"" << (int)f.glyph_undefined.xadvance;
				file << "\" page=\"0\" chnl=\"4\"/>" << std::endl;
				//end chars
				file << "\t</chars>" << std::endl;

				//kernings
				//due to rounding from float to int (format) some kerning pairs will stop being relevant (rounded to zero)
				std::stringstream kerning_buffer;
				unsigned int kerning_pairs = 0;
				for (auto& e : f.kerning_map) {
					int advance = (int)std::round(e.second);
					if (advance != 0) {
						kerning_buffer << "\t\t<kerning first=\"" << (unsigned int)e.first.codepoint1 << "\" second=\"" << (unsigned int)e.first.codepoint2 << "\" amount=\"" << advance << "\" />" << std::endl;
						kerning_pairs++;
					}
				}

				//warn of kerning pair lost
				if (f.kerning_map.size() != kerning_pairs) {
					if (logger) (*logger) << "[FONT TOOLS] WARNING, exporting to BMFONT format in which kerning advance is integer, "<< f.kerning_map.size()-kerning_pairs <<" floating point kerning advances were rounded to zero and thus the pairs were not exported." << std::endl;
				}

				file << "\t<kernings count=\"" << kerning_pairs << "\">" << std::endl;
				file << kerning_buffer.str();
				file << "\t</kernings>" << std::endl;
				file << "</font>" << std::endl;
			}
			file.close();

			if (logger) (*logger) << "[FONT TOOLS] exported to BMFONT format in file "<<fnt_filename<<"." << std::endl;

			//write the images
			exportImages(img_filename, sdf_filename);
		}


		void FontAtlas::exportCPP(const std::string& filename) {
			std::fstream file(filename.c_str(), std::fstream::out);

			//header
			file<<"#include <string>\n#include <map>\nnamespace lap{\n\tnamespace font{" << std::endl;
			
			//write pixels of atlas
			int w = width;
			int h = height;
			file << "\t\t//this array contains the pixels of the font atlas, used by all the font structs in this file\n";
			file << "\t\tconst unsigned char pixels [] = {";
			for (int j = 0; j < h; j++) {
				for (int i = 0; i < w; i++) {		//some IDEs have problems with very long lines.
					if (i != 0 || j != 0) file << ",";
					file << (int)pixels[j*w + i];
				}
				file << std::endl;
			}
			file << "};" << std::endl;
			file << "\t\t//this array contains the SDF pixels of the font atlas, used by all the font structs in this file\n";
			file << "\t\tconst unsigned char pixels_sdf [] = {";
			for (int j = 0; j < h; j++) {
				for (int i = 0; i < w; i++) {		//some IDEs have problems with very long lines.
					if (i != 0 || j != 0) file << ",";
					file << (int)pixels_sdf[j*w + i];
				}
				file << std::endl;
			}
			file << "};" << std::endl;

			//write glyph data
			file << "\n\t\t/// Kerning pair - used for special advance rule between codepoints" << std::endl;
			file << "\t\tstruct KerningPair{" << std::endl;
			file << "\t\t\tint codepoint1;	 ///< first codepoint" << std::endl;
			file << "\t\t\tint codepoint2;	 ///< second codepoint" << std::endl;
			file << "\t\t\tbool operator<(const KerningPair& other) const	///< needed to order pairs in a map " << std::endl;
			file << "\t\t\t{ " << std::endl;
			file << "\t\t\t\tif (codepoint1 < other.codepoint1) return true; " << std::endl;
			file << "\t\t\t\telse if (codepoint1 == other.codepoint1) return codepoint2 < other.codepoint2; " << std::endl;
			file << "\t\t\t\treturn false; " << std::endl;
			file << "\t\t\t} " << std::endl;
			file << "\t\t}; " << std::endl;

			file << "\t\t/// Glyph Data - information about glyphs " << std::endl;
			file << "\t\tstruct GlyphData { " << std::endl;
			file << "\t\t\tfloat x;				///< lower left corner x position (in pixels image space, x grows to the right) " << std::endl;
			file << "\t\t\tfloat y;				///< lower left corner y position (in pixels image space, y grows downwards) " << std::endl;
			file << "\t\t\tfloat width;			///< width (in pixels image space) " << std::endl;
			file << "\t\t\tfloat height;			///< height (in pixels image space) " << std::endl;
			file << "\t\t\tfloat xoffset;			///< x offset from the baseline for the starting position (in pixels image space, x grows to the right) " << std::endl;
			file << "\t\t\tfloat yoffset;			///< y offset from the baseline for the starting position (in pixels image space, y grows downwards) " << std::endl;
			file << "\t\t\tfloat xadvance;			///< x advance after writing glyph (in pixels image space, x grows to the right) " << std::endl;
			file << "\t\t}; " << std::endl;

			//for each font
			for (auto& f : fonts) {
				file << "\n\n\t\tconst struct{" << std::endl;
				//general info
				file << "\t\t\tconst std::string name = \"" << f.name << "\";" << std::endl;
				file << "\t\t\tconst unsigned int baseline = " << f.baseline << ";" << std::endl;
				file << "\t\t\tconst unsigned int line_height = " << f.line_gap << ";" << std::endl;
				file << "\t\t\tconst unsigned int glyph_size = " << f.glyph_size << ";" << std::endl;
				file << "\t\t\tconst unsigned int oversampling = " << f.oversampling << ";" << std::endl;
				file << "\t\t\tconst unsigned int padding = " << f.padding << ";" << std::endl;
				file << "\t\t\tconst bool sdf_computed = " << ((f.sdf_computed) ? "true" : "false") << ";" << std::endl;
				file << "\t\t\tconst unsigned int sdf_distance = " << f.sdf_distance << ";" << std::endl;
				file << "\t\t\tconst unsigned int image_width = " << f.image_width << ";" << std::endl;
				file << "\t\t\tconst unsigned int image_height = " << f.image_height << ";" << std::endl;
				file << "\t\t\tconst unsigned char* image_pixels = pixels;" << std::endl;
				file << "\t\t\tconst unsigned char* image_pixels_sdf = " << (f.sdf_computed ? "pixels_sdf;" : "nullptr;") << std::endl;
				
				//undefined glyph
				file << "\t\t\tconst GlyphData glyph_undefined = {" << f.glyph_undefined.x << "," << f.glyph_undefined.y << "," << f.glyph_undefined.width << "," << f.glyph_undefined.height;
				file << "," << f.glyph_undefined.xoffset << "," << f.glyph_undefined.yoffset << "," << f.glyph_undefined.xadvance << "f};" << std::endl;

				//codepoint map
				int codepoint_count = 0;
				file << "\t\t\tconst std::map<int, GlyphData> codepoint_to_glyphdata_map = {";
				for (auto& e : f.codepoint_to_glyphdata_map) {
					if (codepoint_count !=0) file << ",";
					codepoint_count++;
					file << "{" << e.first << ",{"<<e.second.x<<","<<e.second.y<<","<<e.second.width<<","<<e.second.height<<","<<e.second.xoffset<<","<<e.second.yoffset<<","<<e.second.xadvance<< "f}}";
				}
				file << "};" << std::endl;

				//kerning map
				int kerning_count = 0;
				file << "\t\t\tconst std::map<KerningPair, float> kerning_map = {";
				for (auto& e : f.kerning_map) {
					if (kerning_count != 0) file << ",";
					kerning_count++;
					file << "{{" << e.first.codepoint1 << "," << e.first.codepoint2 << "}," << e.second << "f}";
				}
				file << "};" << std::endl;

				file << "\t\t}Font" << f.name << ";" << std::endl;
			}
			file << "\t}\n}" << std::endl;
			file.close();

			if (logger) (*logger) << "[FONT TOOLS] exported to CPP format in "<< filename <<"."<< std::endl;
		}

		void FontAtlas::exportPacked(const std::string& filename) {
			//write the fnt file
			std::fstream file(filename.c_str(), std::fstream::out | std::fstream::binary);

			//write the width of the atlas
			file.write((const char*)&width, sizeof(unsigned int));
			file.write((const char*)&height, sizeof(unsigned int));
			file.write((const char*)&sdf_compute, sizeof(bool));
			file.write((const char*)pixels.data(), width*height*sizeof(unsigned char));
			if(sdf_compute) file.write((const char*)pixels_sdf.data(), width*height*sizeof(unsigned char));

			//for each font
			unsigned int num_fonts = (unsigned int)fonts.size();
			file.write((const char*)&num_fonts, sizeof(unsigned int));
			for (auto& f : fonts) {
				//general
				unsigned int name_size = (unsigned int)f.name.size();
				file.write((const char*)&name_size, sizeof(unsigned int));
				file.write((const char*)f.name.c_str(), sizeof(char)*name_size);
				file.write((const char*)&f.glyph_size, sizeof(unsigned int));
				file.write((const char*)&f.oversampling, sizeof(unsigned int));
				file.write((const char*)&f.padding, sizeof(unsigned int));
				file.write((const char*)&f.line_gap, sizeof(unsigned int));
				file.write((const char*)&f.baseline, sizeof(unsigned int));
				file.write((const char*)&f.image_width, sizeof(unsigned int));
				file.write((const char*)&f.image_height, sizeof(unsigned int));
							
				//codepoints
				unsigned int num_codepoints = (unsigned int)f.codepoint_to_glyphdata_map.size();
				file.write((const char*)&num_codepoints, sizeof(unsigned int));

				for (auto& e : f.codepoint_to_glyphdata_map) {
					file.write((const char*)&e.first, sizeof(unsigned int));
					file.write((const char*)&e.second.x, sizeof(float));
					file.write((const char*)&e.second.y, sizeof(float));
					file.write((const char*)&e.second.width, sizeof(float));
					file.write((const char*)&e.second.height, sizeof(float));
					file.write((const char*)&e.second.xoffset, sizeof(float));
					file.write((const char*)&e.second.yoffset, sizeof(float));
					file.write((const char*)&e.second.xadvance, sizeof(float));
				}

				//undefined codepoint glyph
				unsigned int undefined = (unsigned int)CODEPOINT_UNDEFINED;
				file.write((const char*)&undefined, sizeof(unsigned int));
				file.write((const char*)&f.glyph_undefined.x, sizeof(float));
				file.write((const char*)&f.glyph_undefined.y, sizeof(float));
				file.write((const char*)&f.glyph_undefined.width, sizeof(float));
				file.write((const char*)&f.glyph_undefined.height, sizeof(float));
				file.write((const char*)&f.glyph_undefined.xoffset, sizeof(float));
				file.write((const char*)&f.glyph_undefined.yoffset, sizeof(float));
				file.write((const char*)&f.glyph_undefined.xadvance, sizeof(float));

				//kernings
				unsigned int num_kernings = (unsigned int)f.kerning_map.size();
				file.write((const char*)&num_kernings, sizeof(unsigned int));
				for (auto& e : f.kerning_map) {
					file.write((const char*)&e.first.codepoint1, sizeof(unsigned int));
					file.write((const char*)&e.first.codepoint2, sizeof(unsigned int));
					file.write((const char*)&e.second, sizeof(float));
				}
			}
			file.close();
			if (logger) (*logger) << "[FONT TOOLS] exported to packed format in "<<filename<<"." << std::endl;
		}


	}
}