///---------------------------------------------------------------------------------------------------------------------
/// The MIT License (MIT)
/// 
/// Copyright (c) 2016 Lucian Petrescu, lucian (dot) petrescu (dot) 24 (at) gmail (dot) com
/// 
/// Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation 
/// files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, 
/// modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software 
/// is furnished to do so, subject to the following conditions:
/// 
/// The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
/// 
/// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
/// OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE 
/// LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR 
/// IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
///---------------------------------------------------------------------------------------------------------------------

///---------------------------------------------------------------------------------------------------------------------
/// @file
/// @mainpage LAP FontTools
/// 
/// Lap Font Tools is a small library for font-related tasks. It can be used to import fonts from ttf files, pack them together
/// into a single image atlas, export fonts to various formats(BMFONT, .hpp include file, binary), analyze font files, generate 
/// kerning pairs and codepoint maps. LAP Font Tools can also compute the Signed Distance Function (SDF) of packed fonts. SDFs 
/// are used in state of the art text rendering methods to increase the quality of fonts magnified beyond their normal resolution.
/// The library is intended to be used as an offline tool and it uses code from 
/// 	- stb_truetype( https://github.com/nothings/stb/blob/master/stb_truetype.h), 
/// 	- stb_rectpack (https://github.com/nothings/stb/blob/master/stb_rect_pack.h)
/// 	- economical utf8 (http://bjoern.hoehrmann.de/utf-8/decoder/dfa/).
///
/// These dependencies are packed in the lap_font_tools_dep.hpp file, thus LAP FontTools is deployable with only 3 files. 
/// It is important to note that the fonts and the font atlas are valid ONLY AFTER successfully finishing the packing operations 
/// (see the examples section or the tests in test.cpp).
///
/// @author Lucian Petrescu( lucian (dot) petrescu (dot) 24 (at) gmail (dot) com )
///
/// @par Unicode:
/// This section discusses the basics of Unicode and is largely based on the stb_truetype documentation.
/// 	- Codepoint:
///		Characters are defined by unicode codepoints, e.g. 65 is uppercase A, 231 is lowercase c with a cedilla, 
///		0x7e30 is the hiragana for "ma".
/// 	- Codepoint Range:
///		Codepoints are usually organized by the Unicode Consortium into ranges, e.g. latin glyph range, arabic
///		glyphs range, etc. These ranges are included in LAP Font Tools, e.g. CODEPOINTS_RANGE_CYRILLIC.
/// 	- Glyph:
///		A visual character shape (every codepoint is rendered as some glyph). If no glyph is present in a font
///		then an "undefined codepoint" glyph is normally used.
/// 	- Baseline:
///		Glyph shapes are defined relative to a baseline, which is the bottom of uppercase characters. Characters 
///		extend both above and below the baseline.
/// 	- Font Height:
///		The number of pixels between two consecutive lines of text with the font.
/// 	- Font Atlas:
///		An image in which glyphs from one or multiple fonts are packed together. 
/// 	- Current Point:
///		As you draw text to the screen, you keep track of a "current point" which is the origin of each character. 
///		The current point's vertical position is the baseline. Even "baked fonts" use this model.
/// 	- Kerning pair:
///		Sometimes fonts define variable distance between glyphs, for example the AV glyphs overlap to produce a 
///		better font aspect. This inter-glyph distance adjustment process is called kerning and it is represented 
///		with kerning pairs.
/// 	- Monospace:
///		Monospaces fonts have constant spacing between glyphs, thus they have no kerning pairs.
///
/// @par Export Formats:
/// LAP Font Tools can export to the following formats:
/// - BMFONT format (http://www.angelcode.com/products/bmfont/doc/file_format.html)
/// - a CPP(header) format which can be included directly by any cpp file. 
/// - a packed binary format (see test example)
///
/// @version 1.00
/// @par Properties:
/// 	- c++11
///	- object oriented
///	- plug and play, just include the 3 files (lap_font_tools.cpp, lap_font_tools.hpp, lap_font_tools_dep.hpp) to your build system
///	- works with TrueType 1.0 fonts
///	- can pack multiple fonts into a single image atlas
///	- can compute the Signed Distance Function (SDF) of the image atlas
///	- can query codepoint support with ranges
///	- packs library dependencies into lap_font_tools_dep.hppwith the /dep_gen/generator.py script (does not need to be included in the build system)
///	- exposes codepoint ranges through the /dep_gen/generator_unicode_ranges.py script (does not need to be included in the build system)
///	- exports kerning pairs as a kerningpair->advance map
///	- exports supported codepoints as a codepoint->glyph map
///		
/// @par Usage:
///	- basic usage
///	@code
///	#include "lap_font_tools.hpp"
///	...
///	lap::font::FontAtlas fontatlas(&std::cout);					//create a font atlas object logging on std::cout
///	fontatlas.import("../assets/DejaVuSansMono.ttf", 25);		//import a font into the font atlas object, glyphs will be rasterized to size 25 pixels
///	bool success = fontatlas.pack(1024, 1024, 1, 1);			//pack the fonts inside the font atlas object into a 1024x1024 image
///	if(success){
///		const lap::font::Font& f= fontatlas.getFonts().at(0);	//access font
///		... DO WORK HERE ...
///	}
///	@endcode
///
///	- pack multiple fonts and compute SDF
///	@code
///	#include "lap_font_tools.hpp"
///	...
///	lap::font::FontAtlas fontatlas(&std::cout);					//create a font atlas object logging on std::cout
///	fontatlas.import("../assets/DejaVuSansMono.ttf", 18);		//import a font into the font atlas object, glyphs will be rasterized to size 18 pixels
///	fontatlas.import("../assets/DejaVuSans.ttf",12);			//import a font into the font atlas object, glyphs will be rasterized to size 12 pixels
///	fontatlas.import("../assets/DejaVuMathTeXGyre.ttf", 19);	//import a font into the font atlas object, glyphs will be rasterized to size 19 pixels
///	fontatlas.import("../assets/DejaVuSansCondensed.ttf", 14);	//import a font into the font atlas object, glyphs will be rasterized to size 14 pixels
///	bool success = fontatlas.pack(2048, 2048, 5, 1, true, 10);	//pack the fonts inside the font atlas object into a 2048x2048 image and compute the SDF
///	if(success){
///		const lap::font::Font& = fontatlas.getFonts().at(0);	//access font
///		... DO WORK HERE ...
///	}
///	@endcode
///
///	- export
///	@code
///	#include "lap_font_tools.hpp"
///	...
///	lap::font::FontAtlas fontatlas(&std::cout);					//create a font atlas object logging on std::cout
///	fontatlas.import("../assets/DejaVuSansMono.ttf", 18);		//import a font into the font atlas object, glyphs will be rasterized to size 18 pixels
///	fontatlas.import("../assets/DejaVuSerif.ttf",20);			//import a font into the font atlas object, glyphs will be rasterized to size 20 pixels
///	bool success = fontatlas.pack(2048, 2048, 5, 1, true, 10);	//pack the fonts inside the font atlas object into a 2048x2048 image and compute the SDF
///	if(success){
///		fontatlas.exportBMFONT("../assets/output_export_bmfonttxt_fnt.fnt", "../assets/output_export_bmfonttxt_img.png", "../assets/output_export_bmfonttxt_sdf.png");
///		fontatlas.exportCPP("../assets/output_export_cpp.hpp");
///		fontatlas.exportPacked("../assets/output_export_packed.pak");
///	}
///	@endcode
///
///	- various 
///	@code
///	#include "lap_font_tools.hpp"
///	...
///	lap::font::FontAtlas fontatlas(&std::cout);					//create a font atlas object logging on std::cout
///	//import only some codepoint ranges from a ttf font
///	fontatlas.import("../assets/DejaVuSansMono.ttf", 25, { CODEPOINTS_RANGE_BASIC_LATIN, CODEPOINTS_RANGE_ARABIC, CODEPOINTS_RANGE_CYRILLIC, CODEPOINTS_RANGE_HIRAGANA });
///	bool success = fontatlas.pack(2048, 2048, 5, 1, true, 10);	//pack the fonts inside the font atlas object into a 2048x2048 image and compute the SDF
///	if(success){
///		const lap::font::Font& f= fontatlas.getFonts().at(0);	//access font
///
///		//test the occupancy of glyphs from the arabic codepoint range
///		std::cout << "Font has " << f.getCodepointOccupancy(CODEPOINTS_RANGE_ARABIC) << " occupancy over arabic glyphs" << std::endl;
///		//test the occupancy of glyphs from the cyrillic and hiragana codepoint ranges
///		std::cout << "Font has " << f.getCodepointOccupancy({ CODEPOINTS_RANGE_CYRILLIC, CODEPOINTS_RANGE_HIRAGANA }) << " occupancy over cyrillic and hiragana glyphs" << std::endl;
///
///		const std::string& name = f.getName();
///		unsigned int num_glyphs = f.getNumGlyphs();
///		unsigned int num_kerning_pairs = f.getNumKerningPairs();
///		unsigned int font_glyph_size = f.getGlyphSize();
///		bool exists_undefined = f.hasCodepoint(9443423);
///		bool exists_a = f.hasCodepoint(67);
///		bool exists_group = f.hasCodepoints({ 67,68,69,70,71,72,73,74 });
///		unsigned int line_height = f.getLineHeight();
///		unsigned int baseline = f.getBaseline();
///		unsigned int padding = f.getPadding();
///		unsigned int oversampling = f.getOversampling();
///		unsigned int sdfdistance = f.getSDFdistance();
///		bool sdfcomputed = f.getSDFcomputed();
///	}
///	@endcode
///
///	- further examples can be found in the testing file (test.cpp)
///
///---------------------------------------------------------------------------------------------------------------------

#ifndef LAP_FONT_TOOLS_H_
#define LAP_FONT_TOOLS_H_

#include <string>
#include <map>
#include <vector>
#include <utility>

namespace lap {
	namespace font {
		///unicode codepoint ranges, from http://billposer.org/Linguistics/Computation/UnicodeRanges.html
		const std::pair<int, int> CODEPOINTS_RANGE_BASIC_LATIN = { 0x0000 , 0x007F };
		const std::pair<int, int> CODEPOINTS_RANGE_C1_CONTROLS_AND_LATIN_1_SUPPLEMENT = { 0x0080 , 0x00FF };
		const std::pair<int, int> CODEPOINTS_RANGE_LATIN_EXTENDED_A = { 0x0100 , 0x017F };
		const std::pair<int, int> CODEPOINTS_RANGE_LATIN_EXTENDED_B = { 0x0180 , 0x024F };
		const std::pair<int, int> CODEPOINTS_RANGE_IPA_EXTENSIONS = { 0x0250 , 0x02AF };
		const std::pair<int, int> CODEPOINTS_RANGE_SPACING_MODIFIER_LETTERS = { 0x02B0 , 0x02FF };
		const std::pair<int, int> CODEPOINTS_RANGE_COMBINING_DIACRITICAL_MARKS = { 0x0300 , 0x036F };
		const std::pair<int, int> CODEPOINTS_RANGE_GREEK_COPTIC = { 0x0370 , 0x03FF };
		const std::pair<int, int> CODEPOINTS_RANGE_CYRILLIC = { 0x0400 , 0x04FF };
		const std::pair<int, int> CODEPOINTS_RANGE_CYRILLIC_SUPPLEMENT = { 0x0500 , 0x052F };
		const std::pair<int, int> CODEPOINTS_RANGE_ARMENIAN = { 0x0530 , 0x058F };
		const std::pair<int, int> CODEPOINTS_RANGE_HEBREW = { 0x0590 , 0x05FF };
		const std::pair<int, int> CODEPOINTS_RANGE_ARABIC = { 0x0600 , 0x06FF };
		const std::pair<int, int> CODEPOINTS_RANGE_SYRIAC = { 0x0700 , 0x074F };
		const std::pair<int, int> CODEPOINTS_RANGE_THAANA = { 0x0780 , 0x07BF };
		const std::pair<int, int> CODEPOINTS_RANGE_DEVANAGARI = { 0x0900 , 0x097F };
		const std::pair<int, int> CODEPOINTS_RANGE_BENGALI_ASSAMESE = { 0x0980 , 0x09FF };
		const std::pair<int, int> CODEPOINTS_RANGE_GURMUKHI = { 0x0A00 , 0x0A7F };
		const std::pair<int, int> CODEPOINTS_RANGE_GUJARATI = { 0x0A80 , 0x0AFF };
		const std::pair<int, int> CODEPOINTS_RANGE_ORIYA = { 0x0B00 , 0x0B7F };
		const std::pair<int, int> CODEPOINTS_RANGE_TAMIL = { 0x0B80 , 0x0BFF };
		const std::pair<int, int> CODEPOINTS_RANGE_TELUGU = { 0x0C00 , 0x0C7F };
		const std::pair<int, int> CODEPOINTS_RANGE_KANNADA = { 0x0C80 , 0x0CFF };
		const std::pair<int, int> CODEPOINTS_RANGE_MALAYALAM = { 0x0D00 , 0x0DFF };
		const std::pair<int, int> CODEPOINTS_RANGE_SINHALA = { 0x0D80 , 0x0DFF };
		const std::pair<int, int> CODEPOINTS_RANGE_THAI = { 0x0E00 , 0x0E7F };
		const std::pair<int, int> CODEPOINTS_RANGE_LAO = { 0x0E80 , 0x0EFF };
		const std::pair<int, int> CODEPOINTS_RANGE_TIBETAN = { 0x0F00 , 0x0FFF };
		const std::pair<int, int> CODEPOINTS_RANGE_MYANMAR = { 0x1000 , 0x109F };
		const std::pair<int, int> CODEPOINTS_RANGE_GEORGIAN = { 0x10A0 , 0x10FF };
		const std::pair<int, int> CODEPOINTS_RANGE_HANGUL_JAMO = { 0x1100 , 0x11FF };
		const std::pair<int, int> CODEPOINTS_RANGE_ETHIOPIC = { 0x1200 , 0x137F };
		const std::pair<int, int> CODEPOINTS_RANGE_CHEROKEE = { 0x13A0 , 0x13FF };
		const std::pair<int, int> CODEPOINTS_RANGE_UNIFIED_CANADIAN_ABORIGINAL_SYLLABICS = { 0x1400 , 0x167F };
		const std::pair<int, int> CODEPOINTS_RANGE_OGHAM = { 0x1680 , 0x169F };
		const std::pair<int, int> CODEPOINTS_RANGE_RUNIC = { 0x16A0 , 0x16FF };
		const std::pair<int, int> CODEPOINTS_RANGE_TAGALOG = { 0x1700 , 0x171F };
		const std::pair<int, int> CODEPOINTS_RANGE_HANUNOO = { 0x1720 , 0x173F };
		const std::pair<int, int> CODEPOINTS_RANGE_BUHID = { 0x1740 , 0x175F };
		const std::pair<int, int> CODEPOINTS_RANGE_TAGBANWA = { 0x1760 , 0x177F };
		const std::pair<int, int> CODEPOINTS_RANGE_KHMER = { 0x1780 , 0x17FF };
		const std::pair<int, int> CODEPOINTS_RANGE_MONGOLIAN = { 0x1800 , 0x18AF };
		const std::pair<int, int> CODEPOINTS_RANGE_LIMBU = { 0x1900 , 0x194F };
		const std::pair<int, int> CODEPOINTS_RANGE_TAI_LE = { 0x1950 , 0x197F };
		const std::pair<int, int> CODEPOINTS_RANGE_KHMER_SYMBOLS = { 0x19E0 , 0x19FF };
		const std::pair<int, int> CODEPOINTS_RANGE_PHONETIC_EXTENSIONS = { 0x1D00 , 0x1D7F };
		const std::pair<int, int> CODEPOINTS_RANGE_LATIN_EXTENDED_ADDITIONAL = { 0x1E00 , 0x1EFF };
		const std::pair<int, int> CODEPOINTS_RANGE_GREEK_EXTENDED = { 0x1F00 , 0x1FFF };
		const std::pair<int, int> CODEPOINTS_RANGE_GENERAL_PUNCTUATION = { 0x2000 , 0x206F };
		const std::pair<int, int> CODEPOINTS_RANGE_SUPERSCRIPTS_AND_SUBSCRIPTS = { 0x2070 , 0x209F };
		const std::pair<int, int> CODEPOINTS_RANGE_CURRENCY_SYMBOLS = { 0x20A0 , 0x20CF };
		const std::pair<int, int> CODEPOINTS_RANGE_COMBINING_DIACRITICAL_MARKS_FOR_SYMBOLS = { 0x20D0 , 0x20FF };
		const std::pair<int, int> CODEPOINTS_RANGE_LETTERLIKE_SYMBOLS = { 0x2100 , 0x214F };
		const std::pair<int, int> CODEPOINTS_RANGE_NUMBER_FORMS = { 0x2150 , 0x218F };
		const std::pair<int, int> CODEPOINTS_RANGE_ARROWS = { 0x2190 , 0x21FF };
		const std::pair<int, int> CODEPOINTS_RANGE_MATHEMATICAL_OPERATORS = { 0x2200 , 0x22FF };
		const std::pair<int, int> CODEPOINTS_RANGE_MISCELLANEOUS_TECHNICAL = { 0x2300 , 0x23FF };
		const std::pair<int, int> CODEPOINTS_RANGE_CONTROL_PICTURES = { 0x2400 , 0x243F };
		const std::pair<int, int> CODEPOINTS_RANGE_OPTICAL_CHARACTER_RECOGNITION = { 0x2440 , 0x245F };
		const std::pair<int, int> CODEPOINTS_RANGE_ENCLOSED_ALPHANUMERICS = { 0x2460 , 0x24FF };
		const std::pair<int, int> CODEPOINTS_RANGE_BOX_DRAWING = { 0x2500 , 0x257F };
		const std::pair<int, int> CODEPOINTS_RANGE_BLOCK_ELEMENTS = { 0x2580 , 0x259F };
		const std::pair<int, int> CODEPOINTS_RANGE_GEOMETRIC_SHAPES = { 0x25A0 , 0x25FF };
		const std::pair<int, int> CODEPOINTS_RANGE_MISCELLANEOUS_SYMBOLS = { 0x2600 , 0x26FF };
		const std::pair<int, int> CODEPOINTS_RANGE_DINGBATS = { 0x2700 , 0x27BF };
		const std::pair<int, int> CODEPOINTS_RANGE_MISCELLANEOUS_MATHEMATICAL_SYMBOLS_A = { 0x27C0 , 0x27EF };
		const std::pair<int, int> CODEPOINTS_RANGE_SUPPLEMENTAL_ARROWS_A = { 0x27F0 , 0x27FF };
		const std::pair<int, int> CODEPOINTS_RANGE_BRAILLE_PATTERNS = { 0x2800 , 0x28FF };
		const std::pair<int, int> CODEPOINTS_RANGE_SUPPLEMENTAL_ARROWS_B = { 0x2900 , 0x297F };
		const std::pair<int, int> CODEPOINTS_RANGE_MISCELLANEOUS_MATHEMATICAL_SYMBOLS_B = { 0x2980 , 0x29FF };
		const std::pair<int, int> CODEPOINTS_RANGE_SUPPLEMENTAL_MATHEMATICAL_OPERATORS = { 0x2A00 , 0x2AFF };
		const std::pair<int, int> CODEPOINTS_RANGE_MISCELLANEOUS_SYMBOLS_AND_ARROWS = { 0x2B00 , 0x2BFF };
		const std::pair<int, int> CODEPOINTS_RANGE_CJK_RADICALS_SUPPLEMENT = { 0x2E80 , 0x2EFF };
		const std::pair<int, int> CODEPOINTS_RANGE_KANGXI_RADICALS = { 0x2F00 , 0x2FDF };
		const std::pair<int, int> CODEPOINTS_RANGE_IDEOGRAPHIC_DESCRIPTION_CHARACTERS = { 0x2FF0 , 0x2FFF };
		const std::pair<int, int> CODEPOINTS_RANGE_CJK_SYMBOLS_AND_PUNCTUATION = { 0x3000 , 0x303F };
		const std::pair<int, int> CODEPOINTS_RANGE_HIRAGANA = { 0x3040 , 0x309F };
		const std::pair<int, int> CODEPOINTS_RANGE_KATAKANA = { 0x30A0 , 0x30FF };
		const std::pair<int, int> CODEPOINTS_RANGE_BOPOMOFO = { 0x3100 , 0x312F };
		const std::pair<int, int> CODEPOINTS_RANGE_HANGUL_COMPATIBILITY_JAMO = { 0x3130 , 0x318F };
		const std::pair<int, int> CODEPOINTS_RANGE_KANBUN_KUNTEN = { 0x3190 , 0x319F };
		const std::pair<int, int> CODEPOINTS_RANGE_BOPOMOFO_EXTENDED = { 0x31A0 , 0x31BF };
		const std::pair<int, int> CODEPOINTS_RANGE_KATAKANA_PHONETIC_EXTENSIONS = { 0x31F0 , 0x31FF };
		const std::pair<int, int> CODEPOINTS_RANGE_ENCLOSED_CJK_LETTERS_AND_MONTHS = { 0x3200 , 0x32FF };
		const std::pair<int, int> CODEPOINTS_RANGE_CJK_COMPATIBILITY = { 0x3300 , 0x33FF };
		const std::pair<int, int> CODEPOINTS_RANGE_CJK_UNIFIED_IDEOGRAPHS_EXTENSION_A = { 0x3400 , 0x4DBF };
		const std::pair<int, int> CODEPOINTS_RANGE_YIJING_HEXAGRAM_SYMBOLS = { 0x4DC0 , 0x4DFF };
		const std::pair<int, int> CODEPOINTS_RANGE_CJK_UNIFIED_IDEOGRAPHS = { 0x4E00 , 0x9FAF };
		const std::pair<int, int> CODEPOINTS_RANGE_YI_SYLLABLES = { 0xA000 , 0xA48F };
		const std::pair<int, int> CODEPOINTS_RANGE_YI_RADICALS = { 0xA490 , 0xA4CF };
		const std::pair<int, int> CODEPOINTS_RANGE_HANGUL_SYLLABLES = { 0xAC00 , 0xD7AF };
		const std::pair<int, int> CODEPOINTS_RANGE_HIGH_SURROGATE_AREA = { 0xD800 , 0xDBFF };
		const std::pair<int, int> CODEPOINTS_RANGE_LOW_SURROGATE_AREA = { 0xDC00 , 0xDFFF };
		const std::pair<int, int> CODEPOINTS_RANGE_CJK_COMPATIBILITY_IDEOGRAPHS = { 0xF900 , 0xFAFF };
		const std::pair<int, int> CODEPOINTS_RANGE_ALPHABETIC_PRESENTATION_FORMS = { 0xFB00 , 0xFB4F };
		const std::pair<int, int> CODEPOINTS_RANGE_ARABIC_PRESENTATION_FORMS_A = { 0xFB50 , 0xFDFF };
		const std::pair<int, int> CODEPOINTS_RANGE_VARIATION_SELECTORS = { 0xFE00 , 0xFE0F };
		const std::pair<int, int> CODEPOINTS_RANGE_COMBINING_HALF_MARKS = { 0xFE20 , 0xFE2F };
		const std::pair<int, int> CODEPOINTS_RANGE_CJK_COMPATIBILITY_FORMS = { 0xFE30 , 0xFE4F };
		const std::pair<int, int> CODEPOINTS_RANGE_SMALL_FORM_VARIANTS = { 0xFE50 , 0xFE6F };
		const std::pair<int, int> CODEPOINTS_RANGE_ARABIC_PRESENTATION_FORMS_B = { 0xFE70 , 0xFEFF };
		const std::pair<int, int> CODEPOINTS_RANGE_HALFWIDTH_AND_FULLWIDTH_FORMS = { 0xFF00 , 0xFFEF };
		const std::pair<int, int> CODEPOINTS_RANGE_SPECIALS = { 0xFFF0 , 0xFFFF };
		const std::pair<int, int> CODEPOINTS_RANGE_LINEAR_B_SYLLABARY = { 0x10000 , 0x1007F };
		const std::pair<int, int> CODEPOINTS_RANGE_LINEAR_B_IDEOGRAMS = { 0x10080 , 0x100FF };
		const std::pair<int, int> CODEPOINTS_RANGE_AEGEAN_NUMBERS = { 0x10100 , 0x1013F };
		const std::pair<int, int> CODEPOINTS_RANGE_OLD_ITALIC = { 0x10300 , 0x1032F };
		const std::pair<int, int> CODEPOINTS_RANGE_GOTHIC = { 0x10330 , 0x1034F };
		const std::pair<int, int> CODEPOINTS_RANGE_UGARITIC = { 0x10380 , 0x1039F };
		const std::pair<int, int> CODEPOINTS_RANGE_DESERET = { 0x10400 , 0x1044F };
		const std::pair<int, int> CODEPOINTS_RANGE_SHAVIAN = { 0x10450 , 0x1047F };
		const std::pair<int, int> CODEPOINTS_RANGE_OSMANYA = { 0x10480 , 0x104AF };
		const std::pair<int, int> CODEPOINTS_RANGE_CYPRIOT_SYLLABARY = { 0x10800 , 0x1083F };
		const std::pair<int, int> CODEPOINTS_RANGE_BYZANTINE_MUSICAL_SYMBOLS = { 0x1D000 , 0x1D0FF };
		const std::pair<int, int> CODEPOINTS_RANGE_MUSICAL_SYMBOLS = { 0x1D100 , 0x1D1FF };
		const std::pair<int, int> CODEPOINTS_RANGE_TAI_XUAN_JING_SYMBOLS = { 0x1D300 , 0x1D35F };
		const std::pair<int, int> CODEPOINTS_RANGE_MATHEMATICAL_ALPHANUMERIC_SYMBOLS = { 0x1D400 , 0x1D7FF };
		const std::pair<int, int> CODEPOINTS_RANGE_CJK_UNIFIED_IDEOGRAPHS_EXTENSION_B = { 0x20000 , 0x2A6DF };
		const std::pair<int, int> CODEPOINTS_RANGE_CJK_COMPATIBILITY_IDEOGRAPHS_SUPPLEMENT = { 0x2F800 , 0x2FA1F };
		const std::pair<int, int> CODEPOINTS_RANGE_TAGS = { 0xE0000 , 0xE007F };
		const std::pair<int, int> CODEPOINTS_RANGE_VARIATION_SELECTORS_SUPPLEMENT = { 0xE0100 , 0xE01EF };

		//used for undefined codepoint, from http://billposer.org/Linguistics/Computation/UnicodeRanges.html
		const int CODEPOINT_UNDEFINED = 0xFFFFE;

		/// Kerning pair - used for special advance rule between codepoints
		struct KerningPair {
			int codepoint1;			///< first codepoint
			int codepoint2;			///< second codepoint
			bool operator<(const KerningPair& other) const	///< needed to order pairs in a map
			{
				if (codepoint1 < other.codepoint1) return true;
				else if (codepoint1 == other.codepoint1) return codepoint2 < other.codepoint2;
				return false;
			}
		};

		/// Glyph Data - information about glyphs
		struct GlyphData {
			float x;				///< lower left corner x position (in pixels image space, x grows to the right)
			float y;				///< lower left corner y position (in pixels image space, y grows downwards)
			float width;			///< width (in pixels image space)
			float height;			///< height (in pixels image space)
			float xoffset;			///< x offset from the baseline for the starting position (in pixels image space, x grows to the right)
			float yoffset;			///< y offset from the baseline for the starting position (in pixels image space, y grows downwards)
			float xadvance;			///< x advance after writing glyph (in pixels image space, x grows to the right)
		};

		class FontAtlas;
		/// Font - contains the codepoint-glyph relationships for a truetype font.
		class Font {
			friend class FontAtlas;
		public:
			Font() = default;
			Font(const Font&) = default;
			Font& operator=(const Font&) = default;
			Font(Font&&) = default;
			Font& operator=(Font&&) = default;
			~Font() = default;

			/// returns true if the codepoint has a glyph in this font, false otherwise
			/// @param codepoint the queried codepoint
			/// @return true if the codepoint has a glyph in this font, false otherwise
			bool hasCodepoint(unsigned int codepoint) const;
			/// returns true if ALL the codepoints have glyphs in this font, false otherwise
			/// @param codepoints a list of queried codepoints
			/// @return true if ALL the codepoints have glyphs in this font, false otherwise
			bool hasCodepoints(const std::vector<int>& codepoints) const;
			/// returns the percentage of codepoints which have glyphs in the provided range
			/// @param range the queried range
			/// @return the percentage of codepoints which have glyphs in the provided range
			float getCodepointOccupancy(const std::pair<int, int>& range) const;
			/// returns the percentage of codepoints which have glyphs in the provided list
			/// @param codepoints the queried codepoints.
			/// @return the percentage of codepoints which have glyphs in the provided list. returns 0 if no codepoints are provided.
			float getCodepointOccupancy(const std::vector<int>& codepoints) const;
			/// returns the percentage of codepoints which have glyphs in the provided ranges
			/// @param ranges a list of the queried ranges
			/// @return the percentage of codepoints which have glyphs in the provided ranges
			float getCodepointOccupancy(const std::vector<std::pair<int, int>>& ranges) const;

			//detailed access
			//todo if the codepoint is not supported return "undefined glyph"

			/// returns the glyph for the specified codepoint. If the codepoint is not supported (has no glyph) the undefined glyph is returned
			/// @param codepoint the queried codepoint
			/// @return the glyph for the specified codepoint. If the codepoint is not supported (has no glyph) the undefined glyph is returned
			const GlyphData& getCodepointToGlyph(unsigned int codepoint) const;
			/// returns the codepoint to glyph map 
			/// @return the codepoint to glyph map 
			const std::map<int, GlyphData>& getCodepointToGlyphMap() const;
			/// returns the glyph for undefined codepoints
			/// @return the glyph for undefined codepoints
			const GlyphData getUndefinedGlyph() const;
			/// returns the map of kerning pairs
			/// @return the map of kerning pairs
			const std::map<KerningPair, float> getKerningMap() const;
			/// returns the name of the font
			/// @return the name of the font
			const std::string& getName() const;
			/// returns the size (in pixels) at which the glyphs of the font were rasterized
			/// NOTE: this size is important because it limits the quality of glyphs rendered above this size. There are methods (signed distance functions) 
			/// through which this limitation is ameliorated 
			/// @return the size (in pixels) at which the glyphs of the font were rasterized
			unsigned int getGlyphSize() const;
			/// returns the number of glyphs in the font
			/// @return the number of glyphs in the font
			unsigned int getNumGlyphs() const;
			/// returns the number of kerning pairs in the font
			/// @return the number of kerning pairs in the font
			unsigned int getNumKerningPairs() const;

			/// returns the line height (in pixels). This describes the jump in pixels between two consecutive lines rendered with this font.
			/// @return the line height.
			unsigned int getLineHeight() const;
			/// returns the baseline
			/// @return the baseline
			unsigned int getBaseline() const;
			// returns the glyph image (the font atlas) width
			// @return the glyph image (the font atlas) width
			unsigned int getGlyphImageWidth() const;
			// returns the glyph image (the font atlas) height
			// @return the glyph image (the font atlas) height
			unsigned int getGlyphImageHeight() const;

			/// returns the padding distance (in pixels) used for each glyph
			/// EXAMPLE: padding=2
			///					 padd
			///					 padd
			///       padd padd   G   padd padd
			///					 padd
			///					 padd
			/// @return the padding distance used for each glyph
			unsigned int getPadding() const;
			/// returns the oversampling distance (in pixels) used when the glyphs were created. The oversampling is used to anti-alias the glyphs. 
			/// @returns the oversampling distance used on glyph creation
			unsigned int getOversampling() const;
			/// returns the signed distance function distance used when the sdf was computed. returns 0 if sdf is not computed
			/// NOTE: similar to the padding distance, the sdf distance around each glyph pixel defines the vicinity up to which the SDF is computed.
			/// @return the signed distance function distance used when the sdf was computed
			unsigned int getSDFdistance() const;
			/// is the SDF computed?
			/// @return is the SDF computed?
			bool getSDFcomputed() const;

		private:
			GlyphData glyph_undefined;
			std::map<int, GlyphData> codepoint_to_glyphdata_map;
			std::map<KerningPair, float> kerning_map;
			unsigned int line_gap;
			unsigned int baseline;
			unsigned int glyph_size;
			unsigned int padding;
			unsigned int oversampling;
			unsigned int sdf_distance;
			unsigned int image_width;
			unsigned int image_height;
			bool sdf_computed;
			std::string name;
		};


		/// Font Atlas - a collection of multiple fonts packed in a single atlas
		class FontAtlas {
		public:
			/// constructs a font atlas
			/// NOTE: the fontatlas is usable only after the imported fonts have been successfully packed
			/// @param logger the logging stream
			FontAtlas(std::ostream* logger = nullptr);
			FontAtlas(const FontAtlas&) = default;
			FontAtlas& operator=(const FontAtlas&) = default;
			FontAtlas(FontAtlas&&) = default;
			FontAtlas& operator=(FontAtlas&&) = default;
			~FontAtlas();

			/// returns the fonts in this font atlas
			/// @return the fonts in this font atlas
			const std::vector<Font>& getFonts() const;

			/// imports a .ttf truetype font from a filename and stores it internally with a specific rasterization size target
			/// once all the desired fonts have been imported they have to be packed in order for the font glyphs to be generated.
			/// NOTE: works with multiple fonts per .ttf file (such files are rare)
			/// @param filename the .ttf file
			/// @param size the size (in pixels) of the glyphs which will be created from this font
			void import(const std::string& filename, unsigned int size);
			/// imports a .ttf truetype font from a filename and stores it internally with a specific rasterization size target
			/// once all the desired fonts have been imported they have to be packed in order for the font glyphs to be generated.
			/// Only loads the codepoints specificed in the codepoint_ranges vector
			/// NOTE: works with multiple fonts per .ttf file (such files are rare)
			/// @param filename the .ttf file
			/// @param size the size (in pixels) of the glyphs which will be created from this font
			/// @param codepoint_ranges only these requested codepoint ranges will be extracted from the font
			void import(const std::string& filename, unsigned int size, const std::vector<std::pair<int, int>>& codepoint_ranges);

			/// generates the glyphs for all the imported fonts, and packs the glyphs in a SINGLE image atlas file. 
			/// also generates the kerning pair map of the font. can compute and generate the Signed Distance Function (SDF) of the glyph image.
			/// NOTE: the result should ALWAYS be queried as the glyphs of the imported fonts might not fit into the specified image space!
			/// NOTE: this function is computationally heavy, especially for large fonts and large image atlases.
			/// @param width the width of the image atlas
			/// @param height the height of the image atlas
			/// @param padding the padding distance (up, down, left, right) to be used for each glyph. Should be >= oversampling and >= sdf_distance/2 (depends). 
			///		   1 is enough for bilinearly filtered antialiased textures
			/// @param oversampling the space around the glyph which will be sampled to antialias the glyph. Should be <= than padding
			/// @param sdf_compute compute the Signed Distance Function (SDF) of the image atlas ? 
			///		   SDF text tutorial https://www.mapbox.com/blog/text-signed-distance-fields/
			///		   SDF uses the free interpolation in GPU texturing to increase the sampling resolution, minimizing rendering artifacts generated by small glyph fonts.
			/// @param sdf_distance the distance from the glyph which will be populated as a SDF.
			/// @return has the function succeed in packing all the imported fonts into the atlas?
			bool pack(unsigned int width, unsigned int height, unsigned int padding = 1, unsigned int oversampling = 1, bool sdf_compute = false, unsigned int sdf_distance = 4);

			/// returns the image atlas width
			/// @return the image atlas width
			unsigned int getWidth() const;
			/// returns the image atlas height
			/// @return the image atlas height
			unsigned int getHeight() const;
			/// returns the image atlas glyph padding
			/// @return the image atlas glyph padding
			unsigned int getPadding() const;
			/// returns the image atlas glyph oversampling
			/// @return the image atlas glyph oversampling
			unsigned int getOversampling() const;
			/// is the sdf computed?
			/// @return wether the sdf was computed
			bool getSDFcomputed() const;
			/// returns the sdf vicinity distance
			/// @return the sdf vicinity distance
			unsigned int getSDFdistance() const;
			/// returns the pixels of the glyph image atlas
			/// @return the pixels of the glyph image atlas
			const std::vector<unsigned char>& getPixels() const;
			/// returns the pixels of sdf of the glyph image atlas
			/// @return the pixels of sdf of the glyph image atlas
			const std::vector<unsigned char>& getPixelsSDF() const;

			/// exports the glyph image and the sdf image to png images. This function has only debug value.
			/// @param img_filename the file to which the glyph image will be exported
			/// @param sdf_filename the file to which the sdf of the glyph image will be exported
			void exportImages(const std::string& img_filename, const std::string& sdf_filename = "");
			/// exports the packed fonts to the BMFONT file format : http://www.angelcode.com/products/bmfont/doc/file_format.html
			/// NOTE: the BMFONT format uses integer kerning advances, thus floating-point kerning advances will be rounded to the
			///		  nearest integer. If the rounding is done to zero the kerning pair is not exported and a warning message will
			///		  be printed to the logger (if one was provided)
			/// @param fnt_filename the font information will be exported to this file
			/// @param img_filename the glyph image will be exported to this file
			/// @param sdf_filename the SDF of the glyph image will be exported to this file
			void exportBMFONT(const std::string& fnt_filename, const std::string& img_filename, const std::string& sdf_filename = "");
			/// exports the packed fonts to a CPP (header) file format. The header file will contain ALL (pixels, sdf pixels, kerning
			/// pairs, codepoints, etc) font related information.
			/// @param filename the name of the CPP(header) file, e.g. : myfont.hpp
			void exportCPP(const std::string& filename);
			/// exports the packed fonts to a single binary format file
			/// test_export() from test.cpp contains a parsing example
			void exportPacked(const std::string& filename);

		private:
			std::vector<Font> fonts;
			std::ostream* logger;
			bool logger_verbose;
			unsigned int width, height, padding, oversampling, sdf_distance;
			bool sdf_compute;
			std::vector<unsigned char> pixels;
			std::vector<unsigned char> pixels_sdf;
			//pimpl state
			void* state;
		};
	}
}

//include guard
#endif