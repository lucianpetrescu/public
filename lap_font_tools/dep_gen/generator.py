#-------------------------------------------------------------------------------------------------------------------------------
# The MIT License (MIT)
#
# Copyright (c) 2016 Lucian Petrescu, lucian (dot) petrescu (dot) 24 (at) gmail (dot) com
# 
# Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation
# files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy,
# modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software
# is furnished to do so, subject to the following conditions:
# 
# The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
# OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
# LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR
# IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
#-------------------------------------------------------------------------------------------------------------------------------

#-------------------------------------------------------------------------------------------------------------------------------
# This function parses the stb folder and creates a single dependency file, used by lap_font_tool. It behaves like a C compiler.
# By using this technique, lap_font_tools can be deployed with only 3 files : lap_font_tools.hpp, lap_font_tools_dep.hpp, and
# lap_font_tools.cpp, no libs, no dlls, no branching/recurring dependencies, etc. Plug and play.
# The resuling file contains these STB libraries:
# - truetype (https://github.com/nothings/stb/blob/master/stb_truetype.h)
# - rectpack (https://github.com/nothings/stb/blob/master/stb_rect_pack.h)
# - imagewrite (https://github.com/nothings/stb/blob/master/stb_image_write.h)
#
# Usage: python generator.py
#-------------------------------------------------------------------------------------------------------------------------------

import os.path

#no define protection as this file should only be included by lap_font_tools.cpp
    
#create target
current_directory = os.path.dirname(__file__)
print(current_directory)
target = open('../lap_font_tools_dep.hpp','w+')

#header 
target.write("""
//-------------------------------------------------------------------------------------------------------------------------------
// The MIT License (MIT)
//
// Copyright (c) 2016 Lucian Petrescu, lucian (dot) petrescu (dot) 24 (at) gmail (dot) com
// 
// Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation
// files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy,
// modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software
// is furnished to do so, subject to the following conditions:
// 
// The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
// OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
// LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR
// IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//-------------------------------------------------------------------------------------------------------------------------------

//implementation defines
#define STB_IMAGE_WRITE_IMPLEMENTATION
#define STB_TRUETYPE_IMPLEMENTATION
#define STB_RECT_PACK_IMPLEMENTATION
""")

#suppress warnings
target.write("""
#if defined(_MSC_VER)
#pragma warning ( push )
#pragma warning ( disable: 4996 )
#endif
""")

# image writer
target.write(open('stb/stb_image_write.h').read())
# rectangle packer
target.write(open('stb/stb_rect_pack.h').read())
# true type
target.write(open('stb/stb_truetype.h').read())

#suppress warnings
target.write("""
#pragma warning ( pop )
""")

#done
target.close()
