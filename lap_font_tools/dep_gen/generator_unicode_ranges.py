#-------------------------------------------------------------------------------------------------------------------------------
# The MIT License (MIT)
#
# Copyright (c) 2016 Lucian Petrescu, lucian (dot) petrescu (dot) 24 (at) gmail (dot) com
# 
# Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation
# files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy,
# modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software
# is furnished to do so, subject to the following conditions:
# 
# The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
# OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
# LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR
# IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
#-------------------------------------------------------------------------------------------------------------------------------

#-------------------------------------------------------------------------------------------------------------------------------
# This function parses the local file Unicode Character Ranges.htm (from http://billposer.org/Linguistics/Computation/UnicodeRanges.html)
# and generates a set of std::pair objects inside the lap_font_tools.hpp file. This script first erases the existing std::pair objects
# and replaces them with the ones found in the htm file. Ranges labeled as undefined or private are not taken into consideration.
#
# Note: While the generator could have read the .htm page directly from its online origin, the small extra size ensures functionality
# in connectivity deprived development environments.
#
# Usage: python generator.py
#-------------------------------------------------------------------------------------------------------------------------------

from html.parser import HTMLParser
import os


# html parser for unicode ranges
class UnicodeTableParser(HTMLParser):
    def __init__(self):
        HTMLParser.__init__(self)
        self.data = []
        self.begin = ""
        self.end = ""
        self.name = ""
        self.number = 0

    def handle_starttag(self, tag, attributes):
        if tag=="tr":
            self.number = 0
    def handle_endtag(self, tag):
        #reset table entry state on end of table entry
        if tag == "tr":
            self.data.append([self.begin,self.end,self.name])
            self.begin = ""
            self.end = ""
            self.name = ""
            self.number = 0
    def handle_data(self,data):
        if self.number == 0:
            self.begin = data
        elif self.number == 1:
            self.end = data
        elif self.number == 2:
            self.name = data
        self.number +=1
        return
        
    
#create target
target = open('../lap_font_tools.hpp','r+')
content = target.readlines()

#write to target
target.seek(0)
for line in content:
    #strip previous codepoint ranges from the original file but keep the rest of the content        
    if not "const std::pair<int, int> CODEPOINTS_RANGE_" in line and not "///unicode codepoint ranges"in line:
        target.write(line)
    #parse the unicode ranges htm file and inject them as std pairs in the lap_font_tools.hpp file after the namespace scope
    if "namespace font" in line:
        parser = UnicodeTableParser()
        parser.feed(open("Unicode Character Ranges.htm").read())
        target.write("""\t\t///unicode codepoint ranges, from http://billposer.org/Linguistics/Computation/UnicodeRanges.html\n""")
        for e in parser.data:
            name = e[2].replace(" ","_").replace("/","_").replace("-","_").replace("(","").replace(")","").upper()
            # leave out undefined/private/unused ranges
            if not "UNDEFINED" in name and not "PRIVATE" in name and not "UNUSED" in name:
                text = "\t\tconst std::pair<int, int> CODEPOINTS_RANGE_"+name+" = { 0x"+ e[0]+ " , 0x"+ e[1] + " };\n"
                target.write(text)
        parser.close()

#done
target.close()

