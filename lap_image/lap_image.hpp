///---------------------------------------------------------------------------------------------------------------------
/// The MIT License (MIT)
/// 
/// Copyright (c) 2016 Lucian Petrescu, lucian (dot) petrescu (dot) 24 (at) gmail (dot) com
/// 
/// Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation 
/// files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, 
/// modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software 
/// is furnished to do so, subject to the following conditions:
/// 
/// The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
/// 
/// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
/// OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE 
/// LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR 
/// IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
///---------------------------------------------------------------------------------------------------------------------

///---------------------------------------------------------------------------------------------------------------------
/// @file
/// @mainpage LAP Image
/// LAP Image (LAPimg) is a tiny image loading library, written over stb_image image loading library
/// (https://github.com/nothings/stb revision 2.10). It is written in a terse OOP style,
/// in order to minimize any integration effort and it is primarily useful for industries where 
/// problematic image formats can be avoided (education, game development, embedded).\n
/// LAPimg distinguishes itself from other tiny image libraries by supporting multi frame image operations,
/// saving and loading of animated gifs (no transparency), per-channel operations and an extensive doxygen 
/// based documentation. \n
///	@version 1.03
///	@par Properties:
///		- c++11
///		- tested with vs2015 and gcc
///		- plug and play, just add lap_image.hpp, lap_image.cpp and lap_image_dep.hpp to your build system
///		- supported input formats
///			- JPEG baseline & progressive (12 bpc / arithmetic not supported, same as stock IJG lib)
///			- PNG 1 / 2 / 4 / 8 - bit - per - channel (16 bpc not supported)
///			- TGA
///			- BMP non - 1bpp, non - RLE
///			- PSD (composited view only, no extra channels, 8 / 16 bit - per - channel)
///			- GIF 
///			- animated GIF 
///			- HDR (radiance rgbE format), the images are automatically converted to LDR with a gamma of 
///			2.2 and a scale factor of 1.0. These can be changed per instance through the Image interface
///			- PIC (Softimage PIC)
///			- PNM (PPM and PGM binary only)
///		- supported output formats (when not specified done through stb)
///			- BMP
///			- GIF
///			- PNG
///			- animated GIF, alpha not supported (through http://www.jonolick.com/home/gif-writer, slightly modified)
///		- supports channel level operations (invert, set, switch, RGB->GBR, etc)
///		- support channel control on creation
///		- SRGB
///			- support, on by default (as most color images are already SRGB)
///			- can be specified through a ColorSpace parameter
///			- SRGB (default) is non-linear and it is standard in photos, cameras and monitors, and it is used for color/albedo/diffuse/emission maps in rendering, https://en.wikipedia.org/wiki/SRGB
///			- RGB is linear, and it is used for normal/bump/specular/glow/dudv/height/ao/noise maps , https://en.wikipedia.org/wiki/RGB_color_space
///		- comes with access operators
///			- array style access with operator(unsigned int)
///			- matrix/image style access with operator()(unsigned int x, unsigned int y, unsigned int channel)
///			- multi-frame matrix/image style access with operator()(unsigned int x, unsigned int y, unsigned int channel, unsigned int frame)
///			- access functions are also provided
///		- supported correct image resizing for both SRGB and RGB (through stb)
///		- no cpp exceptions (while more elegant, exceptions are not available/desired in/on some environments/platforms)
///		
///	@author Lucian Petrescu, lucian (dot) petrescu (dot) 24 (at) gmail (dot) com
///
///	@par Usage:
///		@code
///		Basic usage:
///		#include "lap_image.hpp"
///		using namespace lap;
///		lap::Image img(std::string("myfilename.ext")); 
///		img.transformGrayscale();
///		img.save(std::string("mygrayscale.ext"));
///
///		Channel ops basics:
///		lap::Image img2(std::string("myfilename.ext"), Image::Channel::BLUE, Image::Channel::RED, image::Channel::GREEN);
///		img2.save(std::string("mybrgfile.ext"));
///		img2.transformRGBtoGRB();
///		img2.save(std::string("myrbgfile.ext"));
///		img2.channelSwitch(Image::Channel::GREEN, Image::Channel::BLUE);
///		img2.save(std::string("myfilenamecopy.ext"));
///		img2.saveChannel(std::string("myfilenameredchannel.ext"), Image::Channel::RED);
///
///		Multiframe image basics:
///		lap::Image img3(std::string("myanimated.gif"));
///		img3.reverseFrames();
///		img3.invert(Image::Channel::ALLNOA);
///		img3.save(std::string("myreversedcolorinvertedanimated.gif"));
///		img3.saveFrameChannel(std::string("myreversedcolorinvertedanimated_channelred_frame22.ext"), Image::Channel::RED, 22);
///		@endcode
///---------------------------------------------------------------------------------------------------------------------

#ifndef LAP_IMAGE_H_
#define LAP_IMAGE_H_
#include <cassert>
#include <string>
#include <vector>
#include <algorithm>
#include <cstring>


namespace lap{

	///--------------------------------------------------------------------------------------------
	class Image{
		public:
			/// Image Channels are used by the Image class to specify channel redirection operations
			enum class Channel : int{
				RED = 0,	///< use the red channel
				GREEN = 1,	///< use the green channel
				BLUE = 2,	///< use the blue channel
				ALPHA = 3,	///< use the alpha channel
				ALL = 4,	///< use all channels (useful for transformXXXXXX operations)
				ALLNOA =5,	///< use all channels but not alpha (useful for transformXXXXX operations)
			};

			/// Color Spaces are used by the Image class to differentiate between linear and non-linear data
			enum class ColorSpace : int{
				SRGB = 0,	///< RGB (non-linear, standard in photos, cameras and monitors), used for color/albedo/diffuse/emission maps in rendering, https://en.wikipedia.org/wiki/SRGB
				RGB = 1,	///< RGB (linear), used for normal/bump/specular/glow/dudv/height/ao/noise maps , https://en.wikipedia.org/wiki/RGB_color_space
			};
			
			///------------------------------------------------------------------------------------
			Image(const Image& img) = default;
			Image(Image&& img) = default;
			Image& operator=(const Image& img) = default;
			Image& operator=(Image&& img) = default;


			///------------------------------------------------------------------------------------
			///	Creates an empty Image with no channels and no data, in the SRGB color space
			Image();
			///------------------------------------------------------------------------------------
			///	Creates an empty Image with no channels and no data
			/// @param width	the width of the image
			/// @param height	the height of the image
			/// @param num_channels the number of channels
			/// @param num_frames	the number of frames
			/// @param color_space	the #ColorSpace of the image (if not sure leave this parameter to SRGB)
			Image(unsigned int width, unsigned int height, unsigned int num_channels=3, unsigned int num_frames=1, const ColorSpace& color_space = ColorSpace::SRGB);
			///------------------------------------------------------------------------------------
			///	Creates an Image from file filename \n
			/// the number of channels depend on the contents of filename
			/// @param	filename	the name of the file that will be opened
			/// @param	color_space	the #ColorSpace of the image read from filename (if not sure leave this parameter to SRGB)
			/// @param	ret			true on constructor success, false on image corrupt/invalid/not found
			Image(const std::string& filename, const ColorSpace& color_space = ColorSpace::SRGB, bool *ret = nullptr);
			///------------------------------------------------------------------------------------
			///	Creates a single channel Image from file filename \n
			/// valid channels from #Channel are:\n
			/// RED - will use the red channel\n
			/// GREEN - will use the green channel or zero if the loaded image doesn't have one\n
			/// BLUE - will use the blue channel or zero if the loaded image doesn't have one\n
			/// ALPHA - will use the alpha channel or zero if the loaded image doesn't have one
			/// @param	filename		the name of the file that will be opened
			/// @param	to_r_channel	the #Channel from the loaded image that will be written to the R channel of this image
			/// @param	color_space		the #ColorSpace of the image read from filename (if not sure leave this parameter to SRGB)
			/// @param	ret				true on constructor success, false on image corrupt/invalid/not found
			Image(const std::string& filename, const Channel& to_r_channel, const ColorSpace& color_space = ColorSpace::SRGB, bool *ret = nullptr);
			///------------------------------------------------------------------------------------
			///	Creates a two channel Image from file filename \n
			/// valid channels from #Channel are:\n
			/// RED - will use the red channel\n
			/// GREEN - will use the green channel or zero if the loaded image doesn't have one\n
			/// BLUE - will use the blue channel or zero if the loaded image doesn't have one\n
			/// ALPHA - will use the alpha channel or zero if the loaded image doesn't have one
			/// @param	filename		the name of the file that will be opened
			/// @param	to_r_channel	the #Channel from the loaded image that will be written to the R channel of this image
			/// @param	to_g_channel	the #Channel from the loaded image that will be written to the G channel of this image
			/// @param	color_space		the #ColorSpace of the image read from filename (if not sure leave this parameter to SRGB)
			/// @param	ret				true on constructor success, false on image corrupt/invalid/not found
			Image(const std::string& filename, const Channel& to_r_channel, const Channel &to_g_channel, const ColorSpace& color_space = ColorSpace::SRGB, bool *ret = nullptr);
			///------------------------------------------------------------------------------------
			///	Creates a three channel Image from file filename \n
			/// valid channels from #Channel are:\n
			/// RED - will use the red channel\n
			/// GREEN - will use the green channel or zero if the loaded image doesn't have one\n
			/// BLUE - will use the blue channel or zero if the loaded image doesn't have one\n
			/// ALPHA - will use the alpha channel or zero if the loaded image doesn't have one
			/// @param	filename		the name of the file that will be opened
			/// @param	to_r_channel	the #Channel from the loaded image that will be written to the R channel of this image
			/// @param	to_g_channel	the #Channel from the loaded image that will be written to the G channel of this image
			/// @param	to_b_channel	the #Channel from the loaded image that will be written to the B channel of this image
			/// @param	color_space		the #ColorSpace of the image read from filename (if not sure leave this parameter to SRGB)
			/// @param	ret				true on constructor success, false on image corrupt/invalid/not found
			Image(const std::string& filename, const Channel& to_r_channel, const Channel& to_g_channel, const Channel& to_b_channel, const ColorSpace& color_space = ColorSpace::SRGB, bool *ret = nullptr);
			///------------------------------------------------------------------------------------
			///	Creates a four channel Image from file from filename \n
			/// valid channels from #Channel are:\n
			/// RED - will use the red channel\n
			/// GREEN - will use the green channel or zero if the loaded image doesn't have one\n
			/// BLUE - will use the blue channel or zero if the loaded image doesn't have one\n
			/// ALPHA - will use the alpha channel or zero if the loaded image doesn't have one
			/// @param	filename		the name of the file that will be opened
			/// @param	to_r_channel	the #Channel from the loaded image that will be written to the R channel of this image
			/// @param	to_g_channel	the #Channel from the loaded image that will be written to the G channel of this image
			/// @param	to_b_channel	the #Channel from the loaded image that will be written to the B channel of this image
			/// @param	to_a_channel	the #Channel from the loaded image that will be written to the A channel of this image
			/// @param	color_space		the #ColorSpace of the image read from filename (if not sure leave this parameter to SRGB)
			/// @param	ret				true on constructor success, false on image corrupt/invalid/not found
			Image(const std::string& filename, const Channel& to_r_channel, const Channel& to_g_channel, const Channel& to_b_channel, const Channel& to_a_channel, const ColorSpace& color_space = ColorSpace::SRGB, bool *ret = nullptr);
		

			///------------------------------------------------------------------------------------
			///	Creates a single channel Image from an already loaded image \n
			/// valid channels from #Channel are:\n
			/// RED - will use the red channel\n
			/// GREEN - will use the green channel or zero if img doesn't have one\n
			/// BLUE - will use the blue channel or zero if img doesn't have one\n
			/// ALPHA - will use the alpha channel or zero if img doesn't have one
			/// @param	img an already loaded Image
			/// @param	to_r_channel the #Channel from img that will be written to the R channel of this image
			Image(const Image& img, const Channel& to_r_channel);
			///------------------------------------------------------------------------------------
			///	Creates a two channel Image from an already loaded image \n
			/// valid channels from #Channel are:\n
			/// RED - will use the red channel\n
			/// GREEN - will use the green channel or zero if img doesn't have one\n
			/// BLUE - will use the blue channel or zero if img doesn't have one\n
			/// ALPHA - will use the alpha channel or zero if img doesn't have one
			/// @param	img an already loaded Image
			/// @param	to_r_channel the #Channel from img that will be written to the R channel of this image
			/// @param	to_g_channel the #Channel from img that will be written to the G channel of this image
			Image(const Image& img, const Channel& to_r_channel, const Channel& to_g_channel);
			///------------------------------------------------------------------------------------
			///	Creates a three channel Image from an already loaded image \n
			/// valid channels from #Channel are:\n
			/// RED - will use the red channel\n
			/// GREEN - will use the green channel or zero if img doesn't have one\n
			/// BLUE - will use the blue channel or zero if img doesn't have one\n
			/// ALPHA - will use the alpha channel or zero if img doesn't have one
			/// @param	img an already loaded Image
			/// @param	to_r_channel the #Channel from img that will be written to the R channel of this image
			/// @param	to_g_channel the #Channel from img that will be written to the G channel of this image
			/// @param	to_b_channel the #Channel from img that will be written to the B channel of this image
			Image(const Image& img, const Channel& to_r_channel, const Channel& to_g_channel, const Channel& to_b_channel);
			///------------------------------------------------------------------------------------
			///	Creates a four channel Image from an already loaded image \n
			/// valid channels from #Channel are:\n
			/// RED - will use the red channel\n
			/// GREEN - will use the green channel or zero if img doesn't have one\n
			/// BLUE - will use the blue channel or zero if img doesn't have one\n
			/// ALPHA - will use the alpha channel or zero if img doesn't have one
			/// @param	img an already loaded Image
			/// @param	to_r_channel the #Channel from img that will be written to the R channel of this image
			/// @param	to_g_channel the #Channel from img that will be written to the G channel of this image
			/// @param	to_b_channel the #Channel from img that will be written to the B channel of this image
			/// @param	to_a_channel the #Channel from img that will be written to the A channel of this image
			Image(const Image& img, const Channel& to_r_channel, const Channel& to_g_channel, const Channel& to_b_channel, const Channel& to_a_channel);
			
			///------------------------------------------------------------------------------------
			///	Creates an Image from memory
			/// @param	data	pointer to data containing image
			/// @param	width	the width of the image
			/// @param	height  the height of the image
			/// @param	num_channels	the number of channels of the image
			/// @param	color_space		the #ColorSpace of the image read from data (if not sure leave this parameter to SRGB)
			Image(const unsigned char* data, unsigned int width, unsigned int height, unsigned int num_channels, const ColorSpace& color_space = ColorSpace::SRGB);
			///------------------------------------------------------------------------------------
			///	Creates a single channel Image from a many channel image in memory \n
			/// valid channels from #Channel are:\n
			/// RED - will use the red channel\n
			/// GREEN - will use the green channel or zero if the image from memory doesn't have one\n
			/// BLUE - will use the blue channel or zero if the image from memory doesn't have one\n
			/// ALPHA - will use the alpha channel or zero if the image from memory doesn't have one
			/// @param	data	pointer to data
			/// @param	width	the width of the data
			/// @param	height  the height of the data
			/// @param	num_channels	the number of channels of the data
			/// @param	to_r_channel	the #Channel from data that will be written to the R channel of this image
			/// @param	color_space		the #ColorSpace of the image read from data (if not sure leave this parameter to SRGB)
			Image(const unsigned char* data, unsigned int width, unsigned int height, unsigned int num_channels, const Channel& to_r_channel, const ColorSpace& color_space = ColorSpace::SRGB);
			///------------------------------------------------------------------------------------
			///	Creates a two channel Image from a many channel image in memory \n
			/// valid channels from #Channel are:\n
			/// RED - will use the red channel\n
			/// GREEN - will use the green channel or zero if the image from memory doesn't have one\n
			/// BLUE - will use the blue channel or zero if the image from memory doesn't have one\n
			/// ALPHA - will use the alpha channel or zero if the image from memory doesn't have one
			/// @param	data	pointer to data
			/// @param	width	the width of the data
			/// @param	height  the height of the data
			/// @param	num_channels	the number of channels of the data
			/// @param	to_r_channel	the #Channel from data that will be written to the R channel of this image
			/// @param	to_g_channel	the #Channel from data that will be written to the G channel of this image
			/// @param	color_space		the #ColorSpace of the image read from data (if not sure leave this parameter to SRGB)
			Image(const unsigned char* data, unsigned int width, unsigned int height, unsigned int num_channels, const Channel& to_r_channel, const Channel& to_g_channel, const ColorSpace& color_space = ColorSpace::SRGB);
			///------------------------------------------------------------------------------------
			///	Creates a three channel Image from a many channel image in memory \n
			/// valid channels from #Channel are:\n
			/// RED - will use the red channel\n
			/// GREEN - will use the green channel or zero if the image from memory doesn't have one\n
			/// BLUE - will use the blue channel or zero if the image from memory doesn't have one\n
			/// ALPHA - will use the alpha channel or zero if the image from memory doesn't have one
			/// @param	data	pointer to data
			/// @param	width	the width of the data
			/// @param	height  the height of the data
			/// @param	num_channels	the number of channels of the data
			/// @param	to_r_channel	the #Channel from data that will be written to the R channel of this image
			/// @param	to_g_channel	the #Channel from data that will be written to the G channel of this image
			/// @param	to_b_channel	the #Channel from data that will be written to the B channel of this image
			/// @param	color_space		the #ColorSpace of the image read from data (if not sure leave this parameter to SRGB)
			Image(const unsigned char* data, unsigned int width, unsigned int height, unsigned int num_channels, const Channel& to_r_channel, const Channel& to_g_channel, const Channel& to_b_channel, const ColorSpace& color_space = ColorSpace::SRGB);
			///------------------------------------------------------------------------------------
			///	Creates a four channel Image from a many channel image in memory \n
			/// valid channels from #Channel are:\n
			/// RED - will use the red channel\n
			/// GREEN - will use the green channel or zero if the image from memory doesn't have one\n
			/// BLUE - will use the blue channel or zero if the image from memory doesn't have one\n
			/// ALPHA - will use the alpha channel or zero if the image from memory doesn't have one
			/// @param	data	pointer to data
			/// @param	width	the width of the data
			/// @param	height  the height of the data
			/// @param	num_channels	the number of channels of the data
			/// @param	to_r_channel	the #Channel from data that will be written to the R channel of this image
			/// @param	to_g_channel	the #Channel from data that will be written to the G channel of this image
			/// @param	to_b_channel	the #Channel from data that will be written to the B channel of this image
			/// @param	to_a_channel	the #Channel from data that will be written to the A channel of this image
			/// @param	color_space		the #ColorSpace of the image read from data (if not sure leave this parameter to SRGB)
			Image(const unsigned char* data, unsigned int width, unsigned int height, unsigned int num_channels, const Channel& to_r_channel, const Channel& to_g_channel, const Channel& to_b_channel, const Channel& to_a_channel, const ColorSpace& color_space = ColorSpace::SRGB);

			///------------------------------------------------------------------------------------
			///	Creates an Image from memory
			/// @param	data	pointer to data containing image
			/// @param	width	the width of the image
			/// @param	height  the height of the image
			/// @param	num_channels	the number of channels of the image
			/// @param	color_space		the #ColorSpace of the image read from data (if not sure leave this parameter to SRGB)
			Image(const std::vector<unsigned char>& data, unsigned int width, unsigned int height, unsigned int num_channels, const ColorSpace& color_space = ColorSpace::SRGB);
			///------------------------------------------------------------------------------------
			///	Creates a single channel Image from a many channel image in memory \n
			/// valid channels from #Channel are:\n
			/// RED - will use the red channel\n
			/// GREEN - will use the green channel or zero if the image from memory doesn't have one\n
			/// BLUE - will use the blue channel or zero if the image from memory doesn't have one\n
			/// ALPHA - will use the alpha channel or zero if the image from memory doesn't have one
			/// @param	data	vector with data
			/// @param	width	the width of the data
			/// @param	height  the height of the data
			/// @param	num_channels	the number of channels of the data
			/// @param	to_r_channel	the #Channel from data that will be written to the R channel of this image
			/// @param	color_space		the #ColorSpace of the image read from data (if not sure leave this parameter to SRGB)
			Image(const std::vector<unsigned char>& data, unsigned int width, unsigned int height, unsigned int num_channels, const Channel& to_r_channel, const ColorSpace& color_space = ColorSpace::SRGB);
			///------------------------------------------------------------------------------------
			///	Creates a two channel Image from a many channel image in memory \n
			/// valid channels from #Channel are:\n
			/// RED - will use the red channel\n
			/// GREEN - will use the green channel or zero if the image from memory doesn't have one\n
			/// BLUE - will use the blue channel or zero if the image from memory doesn't have one\n
			/// ALPHA - will use the alpha channel or zero if the image from memory doesn't have one
			/// @param	data	vector with data
			/// @param	width	the width of the data
			/// @param	height  the height of the data
			/// @param	num_channels	the number of channels of the data
			/// @param	to_r_channel	the #Channel from data that will be written to the R channel of this image
			/// @param	to_g_channel	the #Channel from data that will be written to the G channel of this image
			/// @param	color_space		the #ColorSpace of the image read from data (if not sure leave this parameter to SRGB)
			Image(const std::vector<unsigned char>& data, unsigned int width, unsigned int height, unsigned int num_channels, const Channel& to_r_channel, const Channel& to_g_channel, const ColorSpace& color_space = ColorSpace::SRGB);
			///------------------------------------------------------------------------------------
			///	Creates a three channel Image from a many channel image in memory \n
			/// valid channels from #Channel are:\n
			/// RED - will use the red channel\n
			/// GREEN - will use the green channel or zero if the image from memory doesn't have one\n
			/// BLUE - will use the blue channel or zero if the image from memory doesn't have one\n
			/// ALPHA - will use the alpha channel or zero if the image from memory doesn't have one
			/// @param	data	vector with data
			/// @param	width	the width of the data
			/// @param	height  the height of the data
			/// @param	num_channels	the number of channels of the data
			/// @param	to_r_channel	the #Channel from data that will be written to the R channel of this image
			/// @param	to_g_channel	the #Channel from data that will be written to the G channel of this image
			/// @param	to_b_channel	the #Channel from data that will be written to the B channel of this image
			/// @param	color_space		the #ColorSpace of the image read from data (if not sure leave this parameter to SRGB)
			Image(const std::vector<unsigned char>& data, unsigned int width, unsigned int height, unsigned int num_channels, const Channel& to_r_channel, const Channel& to_g_channel, const Channel& to_b_channel, const ColorSpace& color_space = ColorSpace::SRGB);
			///------------------------------------------------------------------------------------
			///	Creates a four channel Image from a many channel image in memory \n
			/// valid channels from #Channel are:\n
			/// RED - will use the red channel\n
			/// GREEN - will use the green channel or zero if the image from memory doesn't have one\n
			/// BLUE - will use the blue channel or zero if the image from memory doesn't have one\n
			/// ALPHA - will use the alpha channel or zero if the image from memory doesn't have one
			/// @param	data	vector with data
			/// @param	width	the width of the data
			/// @param	height  the height of the data
			/// @param	num_channels	the number of channels of the data
			/// @param	to_r_channel	the #Channel from data that will be written to the R channel of this image
			/// @param	to_g_channel	the #Channel from data that will be written to the G channel of this image
			/// @param	to_b_channel	the #Channel from data that will be written to the B channel of this image
			/// @param	to_a_channel	the #Channel from data that will be written to the A channel of this image
			/// @param	color_space		the #ColorSpace of the image read from data (if not sure leave this parameter to SRGB)
			Image(const std::vector<unsigned char>& data, unsigned int width, unsigned int height, unsigned int num_channels, const Channel& to_r_channel, const Channel& to_g_channel, const Channel& to_b_channel, const Channel& to_a_channel, const ColorSpace& color_space = ColorSpace::SRGB);
			
			///------------------------------------------------------------------------------------
			///	Creates a multi-frame Image from memory
			/// @param	data	pointer to pointers containing image frames (num_frames frames)
			/// @param	delay	pointer to delays between frames (num_frames-1 delays)
			/// @param	width	the width of the image
			/// @param	height  the height of the image
			/// @param	num_channels	the number of channels of the image
			/// @param  num_frames		the number of frames of the image
			/// @param	color_space		the #ColorSpace of the image read from data (if not sure leave this parameter to SRGB)
			Image(const unsigned char **data, const unsigned int *delay, unsigned int width, unsigned int height, unsigned int num_channels, unsigned int num_frames, const ColorSpace& color_space = ColorSpace::SRGB);
			///------------------------------------------------------------------------------------
			///	Creates a single channel multi-frame Image from a many channel multi-frame image in memory \n
			/// valid channels from #Channel are:\n
			/// RED - will use the red channel\n
			/// GREEN - will use the green channel or zero if the image from memory doesn't have one\n
			/// BLUE - will use the blue channel or zero if the image from memory doesn't have one\n
			/// ALPHA - will use the alpha channel or zero if the image from memory doesn't have one
			/// @param	data	pointer to pointers containing image frames (num_frames frames)
			/// @param	delay	pointer to delays between frames (num_frames-1 delays)
			/// @param	width	the width of the data
			/// @param	height  the height of the data
			/// @param	num_channels	the number of channels of the data
			/// @param	num_frames		the number of frames of the image
			/// @param	to_r_channel	the #Channel from data that will be written to the R channel of this image
			/// @param	color_space		the #ColorSpace of the image read from data (if not sure leave this parameter to SRGB)
			Image(const unsigned char** data, const unsigned int *delay, unsigned int width, unsigned int height, unsigned int num_channels, unsigned int num_frames, const Channel& to_r_channel, const ColorSpace& color_space = ColorSpace::SRGB);
			///------------------------------------------------------------------------------------
			///	Creates a two channel multi-frame Image from a many channel multi-frame image in memory \n
			/// valid channels from #Channel are:\n
			/// RED - will use the red channel\n
			/// GREEN - will use the green channel or zero if the image from memory doesn't have one\n
			/// BLUE - will use the blue channel or zero if the image from memory doesn't have one\n
			/// ALPHA - will use the alpha channel or zero if the image from memory doesn't have one
			/// @param	data	pointer to pointers containing image frames (num_frames frames)
			/// @param	delay	pointer to delays between frames (num_frames-1 delays)
			/// @param	width	the width of the data
			/// @param	height  the height of the data
			/// @param	num_channels	the number of channels of the data
			///	@param	num_frames		the number of frames of the image
			/// @param	to_r_channel	the #Channel from data that will be written to the R channel of this image
			/// @param	to_g_channel	the #Channel from data that will be written to the G channel of this image
			/// @param	color_space		the #ColorSpace of the image read from data (if not sure leave this parameter to SRGB)
			Image(const unsigned char** data, const unsigned int *delay, unsigned int width, unsigned int height, unsigned int num_channels, unsigned int num_frames, const Channel& to_r_channel, const Channel& to_g_channel, const ColorSpace& color_space = ColorSpace::SRGB);
			///------------------------------------------------------------------------------------
			///	Creates a three channel multi-frame Image from a many channel multi-frame image in memory \n
			/// valid channels from #Channel are:\n
			/// RED - will use the red channel\n
			/// GREEN - will use the green channel or zero if the image from memory doesn't have one\n
			/// BLUE - will use the blue channel or zero if the image from memory doesn't have one\n
			/// ALPHA - will use the alpha channel or zero if the image from memory doesn't have one
			/// @param	data	pointer to pointers containing image frames (num_frames frames)
			/// @param	delay	pointer to delays between frames (num_frames-1 delays)
			/// @param	width	the width of the data
			/// @param	height  the height of the data
			/// @param	num_channels	the number of channels of the data
			///	@param	num_frames		the number of frames of the image
			/// @param	to_r_channel	the #Channel from data that will be written to the R channel of this image
			/// @param	to_g_channel	the #Channel from data that will be written to the G channel of this image
			/// @param	to_b_channel	the #Channel from data that will be written to the B channel of this image
			/// @param	color_space		the #ColorSpace of the image read from data (if not sure leave this parameter to SRGB)
			Image(const unsigned char** data, const unsigned int *delay, unsigned int width, unsigned int height, unsigned int num_channels, unsigned int num_frames, const Channel& to_r_channel, const Channel& to_g_channel, const Channel& to_b_channel, const ColorSpace& color_space = ColorSpace::SRGB);
			///------------------------------------------------------------------------------------
			///	Creates a four channel multi-frame Image from a many channel multi-frame image in memory \n
			/// valid channels from #Channel are:\n
			/// RED - will use the red channel\n
			/// GREEN - will use the green channel or zero if the image from memory doesn't have one\n
			/// BLUE - will use the blue channel or zero if the image from memory doesn't have one\n
			/// ALPHA - will use the alpha channel or zero if the image from memory doesn't have one
			/// @param	data	pointer to pointers containing image frames (num_frames frames)
			/// @param	delay	pointer to delays between frames (num_frames-1 delays)
			/// @param	width	the width of the data
			/// @param	height  the height of the data
			/// @param	num_channels	the number of channels of the data
			///	@param	num_frames		the number of frames of the image
			/// @param	to_r_channel	the #Channel from data that will be written to the R channel of this image
			/// @param	to_g_channel	the #Channel from data that will be written to the G channel of this image
			/// @param	to_b_channel	the #Channel from data that will be written to the B channel of this image
			/// @param	to_a_channel	the #Channel from data that will be written to the A channel of this image
			/// @param	color_space		the #ColorSpace of the image read from data (if not sure leave this parameter to SRGB)
			Image(const unsigned char** data, const unsigned int *delay, unsigned int width, unsigned int height, unsigned int num_channels, unsigned int num_frames, const Channel& to_r_channel, const Channel& to_g_channel, const Channel& to_b_channel, const Channel& to_a_channel, const ColorSpace& color_space = ColorSpace::SRGB);
			

			///------------------------------------------------------------------------------------
			///	Creates a multi-frame Image from memory
			/// @param	data	vector to vector containing image frames (num_frames frames)
			/// @param	delay	vector to delays between frames (num_frames-1 delays)
			/// @param	width	the width of the image
			/// @param	height  the height of the image
			/// @param	num_channels	the number of channels of the image
			/// @param  num_frames		the number of frames of the image
			/// @param	color_space		the #ColorSpace of the image read from data (if not sure leave this parameter to SRGB)
			Image(const std::vector<std::vector<unsigned char>>& data, const std::vector<unsigned int>& delay, unsigned int width, unsigned int height, unsigned int num_channels, unsigned int num_frames, const ColorSpace& color_space = ColorSpace::SRGB);
			///------------------------------------------------------------------------------------
			///	Creates a single channel multi-frame Image from a many channel multi-frame image in memory \n
			/// valid channels from #Channel are:\n
			/// RED - will use the red channel\n
			/// GREEN - will use the green channel or zero if the image from memory doesn't have one\n
			/// BLUE - will use the blue channel or zero if the image from memory doesn't have one\n
			/// ALPHA - will use the alpha channel or zero if the image from memory doesn't have one
			/// @param	data	vector to vector containing image frames (num_frames frames)
			/// @param	delay	vector to delays between frames (num_frames-1 delays)
			/// @param	width	the width of the data
			/// @param	height  the height of the data
			/// @param	num_channels	the number of channels of the data
			/// @param	num_frames		the number of frames of the image
			/// @param	to_r_channel	the #Channel from data that will be written to the R channel of this image
			/// @param	color_space		the #ColorSpace of the image read from data (if not sure leave this parameter to SRGB)
			Image(const std::vector<std::vector<unsigned char>>& data, const std::vector<unsigned int>& delay, unsigned int width, unsigned int height, unsigned int num_channels, unsigned int num_frames, const Channel& to_r_channel, const ColorSpace& color_space = ColorSpace::SRGB);
			///------------------------------------------------------------------------------------
			///	Creates a two channel multi-frame Image from a many channel multi-frame image in memory \n
			/// valid channels from #Channel are:\n
			/// RED - will use the red channel\n
			/// GREEN - will use the green channel or zero if the image from memory doesn't have one\n
			/// BLUE - will use the blue channel or zero if the image from memory doesn't have one\n
			/// ALPHA - will use the alpha channel or zero if the image from memory doesn't have one
			/// @param	data	vector to vector containing image frames (num_frames frames)
			/// @param	delay	vector to delays between frames (num_frames-1 delays)
			/// @param	width	the width of the data
			/// @param	height  the height of the data
			/// @param	num_channels	the number of channels of the data
			///	@param	num_frames		the number of frames of the image
			/// @param	to_r_channel	the #Channel from data that will be written to the R channel of this image
			/// @param	to_g_channel	the #Channel from data that will be written to the G channel of this image
			/// @param	color_space		the #ColorSpace of the image read from data (if not sure leave this parameter to SRGB)
			Image(const std::vector<std::vector<unsigned char>>& data, const std::vector<unsigned int>& delay, unsigned int width, unsigned int height, unsigned int num_channels, unsigned int num_frames, const Channel& to_r_channel, const Channel& to_g_channel, const ColorSpace& color_space = ColorSpace::SRGB);
			///------------------------------------------------------------------------------------
			///	Creates a three channel multi-frame Image from a many channel multi-frame image in memory \n
			/// valid channels from #Channel are:\n
			/// RED - will use the red channel\n
			/// GREEN - will use the green channel or zero if the image from memory doesn't have one\n
			/// BLUE - will use the blue channel or zero if the image from memory doesn't have one\n
			/// ALPHA - will use the alpha channel or zero if the image from memory doesn't have one
			/// @param	data	vector to vector containing image frames (num_frames frames)
			/// @param	delay	vector to delays between frames (num_frames-1 delays)
			/// @param	width	the width of the data
			/// @param	height  the height of the data
			/// @param	num_channels	the number of channels of the data
			///	@param	num_frames		the number of frames of the image
			/// @param	to_r_channel	the #Channel from data that will be written to the R channel of this image
			/// @param	to_g_channel	the #Channel from data that will be written to the G channel of this image
			/// @param	to_b_channel	the #Channel from data that will be written to the B channel of this image
			/// @param	color_space		the #ColorSpace of the image read from data (if not sure leave this parameter to SRGB)
			Image(const std::vector<std::vector<unsigned char>>& data, const std::vector<unsigned int>& delay, unsigned int width, unsigned int height, unsigned int num_channels, unsigned int num_frames, const Channel& to_r_channel, const Channel& to_g_channel, const Channel& to_b_channel, const ColorSpace& color_space = ColorSpace::SRGB);
			///------------------------------------------------------------------------------------
			///	Creates a four channel multi-frame Image from a many channel multi-frame image in memory \n
			/// valid channels from #Channel are:\n
			/// RED - will use the red channel\n
			/// GREEN - will use the green channel or zero if the image from memory doesn't have one\n
			/// BLUE - will use the blue channel or zero if the image from memory doesn't have one\n
			/// ALPHA - will use the alpha channel or zero if the image from memory doesn't have one
			/// @param	data	vector to vector containing image frames (num_frames frames)
			/// @param	delay	vector to delays between frames (num_frames-1 delays)
			/// @param	width	the width of the data
			/// @param	height  the height of the data
			/// @param	num_channels	the number of channels of the data
			///	@param	num_frames		the number of frames of the image
			/// @param	to_r_channel	the #Channel from data that will be written to the R channel of this image
			/// @param	to_g_channel	the #Channel from data that will be written to the G channel of this image
			/// @param	to_b_channel	the #Channel from data that will be written to the B channel of this image
			/// @param	to_a_channel	the #Channel from data that will be written to the A channel of this image
			/// @param	color_space		the #ColorSpace of the image read from data (if not sure leave this parameter to SRGB)
			Image(const std::vector<std::vector<unsigned char>>& data, const std::vector<unsigned int>& delay, unsigned int width, unsigned int height, unsigned int num_channels, unsigned int num_frames, const Channel& to_r_channel, const Channel& to_g_channel, const Channel& to_b_channel, const Channel& to_a_channel, const ColorSpace& color_space = ColorSpace::SRGB);
			
			





			///------------------------------------------------------------------------------------
			///	Loads an Image from file filename \n
			/// the number of channels depend on the contents of filename
			/// @param	filename	the name of the file that will be opened
			/// @param	color_space	the #ColorSpace of the image read from filename (if not sure leave this parameter to SRGB)
			/// @param	ret			true on constructor success, false on image corrupt/invalid/not found
			void load(const std::string& filename, const ColorSpace& color_space = ColorSpace::SRGB, bool *ret = nullptr);
			///------------------------------------------------------------------------------------
			///	Loads a single channel Image from file filename \n
			/// valid channels from #Channel are:\n
			/// RED - will use the red channel\n
			/// GREEN - will use the green channel or zero if the loaded image doesn't have one\n
			/// BLUE - will use the blue channel or zero if the loaded image doesn't have one\n
			/// ALPHA - will use the alpha channel or zero if the loaded image doesn't have one
			/// @param	filename		the name of the file that will be opened
			/// @param	to_r_channel	the #Channel from the loaded image that will be written to the R channel of this image
			/// @param	color_space		the #ColorSpace of the image read from filename (if not sure leave this parameter to SRGB)
			/// @param	ret				true on constructor success, false on image corrupt/invalid/not found
			void load(const std::string& filename, const Channel& to_r_channel, const ColorSpace& color_space = ColorSpace::SRGB, bool *ret = nullptr);
			///------------------------------------------------------------------------------------
			///	Loads a two channel Image from file filename \n
			/// valid channels from #Channel are:\n
			/// RED - will use the red channel\n
			/// GREEN - will use the green channel or zero if the loaded image doesn't have one\n
			/// BLUE - will use the blue channel or zero if the loaded image doesn't have one\n
			/// ALPHA - will use the alpha channel or zero if the loaded image doesn't have one
			/// @param	filename		the name of the file that will be opened
			/// @param	to_r_channel	the #Channel from the loaded image that will be written to the R channel of this image
			/// @param	to_g_channel	the #Channel from the loaded image that will be written to the G channel of this image
			/// @param	color_space		the #ColorSpace of the image read from filename (if not sure leave this parameter to SRGB)
			/// @param	ret				true on constructor success, false on image corrupt/invalid/not found
			void load(const std::string& filename, const Channel& to_r_channel, const Channel &to_g_channel, const ColorSpace& color_space = ColorSpace::SRGB, bool *ret = nullptr);
			///------------------------------------------------------------------------------------
			///	Loads a three channel Image from file filename \n
			/// valid channels from #Channel are:\n
			/// RED - will use the red channel\n
			/// GREEN - will use the green channel or zero if the loaded image doesn't have one\n
			/// BLUE - will use the blue channel or zero if the loaded image doesn't have one\n
			/// ALPHA - will use the alpha channel or zero if the loaded image doesn't have one
			/// @param	filename		the name of the file that will be opened
			/// @param	to_r_channel	the #Channel from the loaded image that will be written to the R channel of this image
			/// @param	to_g_channel	the #Channel from the loaded image that will be written to the G channel of this image
			/// @param	to_b_channel	the #Channel from the loaded image that will be written to the B channel of this image
			/// @param	color_space		the #ColorSpace of the image read from filename (if not sure leave this parameter to SRGB)
			/// @param	ret				true on constructor success, false on image corrupt/invalid/not found
			void load(const std::string& filename, const Channel& to_r_channel, const Channel& to_g_channel, const Channel& to_b_channel, const ColorSpace& color_space = ColorSpace::SRGB, bool *ret = nullptr);
			///------------------------------------------------------------------------------------
			///	Loads a four channel Image from file from filename \n
			/// valid channels from #Channel are:\n
			/// RED - will use the red channel\n
			/// GREEN - will use the green channel or zero if the loaded image doesn't have one\n
			/// BLUE - will use the blue channel or zero if the loaded image doesn't have one\n
			/// ALPHA - will use the alpha channel or zero if the loaded image doesn't have one
			/// @param	filename		the name of the file that will be opened
			/// @param	to_r_channel	the #Channel from the loaded image that will be written to the R channel of this image
			/// @param	to_g_channel	the #Channel from the loaded image that will be written to the G channel of this image
			/// @param	to_b_channel	the #Channel from the loaded image that will be written to the B channel of this image
			/// @param	to_a_channel	the #Channel from the loaded image that will be written to the A channel of this image
			/// @param	color_space		the #ColorSpace of the image read from filename (if not sure leave this parameter to SRGB)
			/// @param	ret				true on constructor success, false on image corrupt/invalid/not found
			void load(const std::string& filename, const Channel& to_r_channel, const Channel& to_g_channel, const Channel& to_b_channel, const Channel& to_a_channel, const ColorSpace& color_space = ColorSpace::SRGB, bool *ret = nullptr);
			
			///------------------------------------------------------------------------------------
			///	Loads a single channel Image from an already loaded image \n
			/// valid channels from #Channel are:\n
			/// RED - will use the red channel\n
			/// GREEN - will use the green channel or zero if img doesn't have one\n
			/// BLUE - will use the blue channel or zero if img doesn't have one\n
			/// ALPHA - will use the alpha channel or zero if img doesn't have one
			/// @param	img an already loaded Image
			/// @param	to_r_channel the #Channel from img that will be written to the R channel of this image
			void load(const Image& img, const Channel& to_r_channel);
			///------------------------------------------------------------------------------------
			///	Loads a two channel Image from an already loaded image \n
			/// valid channels from #Channel are:\n
			/// RED - will use the red channel\n
			/// GREEN - will use the green channel or zero if img doesn't have one\n
			/// BLUE - will use the blue channel or zero if img doesn't have one\n
			/// ALPHA - will use the alpha channel or zero if img doesn't have one
			/// @param	img an already loaded Image
			/// @param	to_r_channel the #Channel from img that will be written to the R channel of this image
			/// @param	to_g_channel the #Channel from img that will be written to the G channel of this image
			void load(const Image& img, const Channel& to_r_channel, const Channel& to_g_channel);
			///------------------------------------------------------------------------------------
			///	Loads a three channel Image from an already loaded image \n
			/// valid channels from #Channel are:\n
			/// RED - will use the red channel\n
			/// GREEN - will use the green channel or zero if img doesn't have one\n
			/// BLUE - will use the blue channel or zero if img doesn't have one\n
			/// ALPHA - will use the alpha channel or zero if img doesn't have one
			/// @param	img an already loaded Image
			/// @param	to_r_channel the #Channel from img that will be written to the R channel of this image
			/// @param	to_g_channel the #Channel from img that will be written to the G channel of this image
			/// @param	to_b_channel the #Channel from img that will be written to the B channel of this image
			void load(const Image& img, const Channel& to_r_channel, const Channel& to_g_channel, const Channel& to_b_channel);
			///------------------------------------------------------------------------------------
			///	Loads a four channel Image from an already loaded image \n
			/// valid channels from #Channel are:\n
			/// RED - will use the red channel\n
			/// GREEN - will use the green channel or zero if img doesn't have one\n
			/// BLUE - will use the blue channel or zero if img doesn't have one\n
			/// ALPHA - will use the alpha channel or zero if img doesn't have one
			/// @param	img an already loaded Image
			/// @param	to_r_channel the #Channel from img that will be written to the R channel of this image
			/// @param	to_g_channel the #Channel from img that will be written to the G channel of this image
			/// @param	to_b_channel the #Channel from img that will be written to the B channel of this image
			/// @param	to_a_channel the #Channel from img that will be written to the A channel of this image
			void load(const Image& img, const Channel& to_r_channel, const Channel& to_g_channel, const Channel& to_b_channel, const Channel& to_a_channel);

			///------------------------------------------------------------------------------------
			///	Loads an Image from memory
			/// @param	data	pointer to data containing image
			/// @param	width	the width of the image
			/// @param	height  the height of the image
			/// @param	num_channels	the number of channels of the image
			/// @param	color_space		the #ColorSpace of the image read from data (if not sure leave this parameter to SRGB)
			void load(const unsigned char* data, unsigned int width, unsigned int height, unsigned int num_channels, const ColorSpace& color_space = ColorSpace::SRGB);
			///------------------------------------------------------------------------------------
			///	Loads a single channel Image from a many channel image in memory \n
			/// valid channels from #Channel are:\n
			/// RED - will use the red channel\n
			/// GREEN - will use the green channel or zero if the image from memory doesn't have one\n
			/// BLUE - will use the blue channel or zero if the image from memory doesn't have one\n
			/// ALPHA - will use the alpha channel or zero if the image from memory doesn't have one
			/// @param	data	pointer to data
			/// @param	width	the width of the data
			/// @param	height  the height of the data
			/// @param	num_channels	the number of channels of the data
			/// @param	to_r_channel	the #Channel from data that will be written to the R channel of this image
			/// @param	color_space		the #ColorSpace of the image read from data (if not sure leave this parameter to SRGB)
			void load(const unsigned char* data, unsigned int width, unsigned int height, unsigned int num_channels, const Channel& to_r_channel, const ColorSpace& color_space = ColorSpace::SRGB);
			///------------------------------------------------------------------------------------
			///	Loads a two channel Image from a many channel image in memory \n
			/// valid channels from #Channel are:\n
			/// RED - will use the red channel\n
			/// GREEN - will use the green channel or zero if the image from memory doesn't have one\n
			/// BLUE - will use the blue channel or zero if the image from memory doesn't have one\n
			/// ALPHA - will use the alpha channel or zero if the image from memory doesn't have one
			/// @param	data	pointer to data
			/// @param	width	the width of the data
			/// @param	height  the height of the data
			/// @param	num_channels	the number of channels of the data
			/// @param	to_r_channel	the #Channel from data that will be written to the R channel of this image
			/// @param	to_g_channel	the #Channel from data that will be written to the G channel of this image
			/// @param	color_space		the #ColorSpace of the image read from data (if not sure leave this parameter to SRGB)
			void load(const unsigned char* data, unsigned int width, unsigned int height, unsigned int num_channels, const Channel& to_r_channel, const Channel& to_g_channel, const ColorSpace& color_space = ColorSpace::SRGB);
			///------------------------------------------------------------------------------------
			///	Loads a three channel Image from a many channel image in memory \n
			/// valid channels from #Channel are:\n
			/// RED - will use the red channel\n
			/// GREEN - will use the green channel or zero if the image from memory doesn't have one\n
			/// BLUE - will use the blue channel or zero if the image from memory doesn't have one\n
			/// ALPHA - will use the alpha channel or zero if the image from memory doesn't have one
			/// @param	data	pointer to data
			/// @param	width	the width of the data
			/// @param	height  the height of the data
			/// @param	num_channels	the number of channels of the data
			/// @param	to_r_channel	the #Channel from data that will be written to the R channel of this image
			/// @param	to_g_channel	the #Channel from data that will be written to the G channel of this image
			/// @param	to_b_channel	the #Channel from data that will be written to the B channel of this image
			/// @param	color_space		the #ColorSpace of the image read from data (if not sure leave this parameter to SRGB)
			void load(const unsigned char* data, unsigned int width, unsigned int height, unsigned int num_channels, const Channel& to_r_channel, const Channel& to_g_channel, const Channel& to_b_channel, const ColorSpace& color_space = ColorSpace::SRGB);
			///------------------------------------------------------------------------------------
			///	Loads a four channel Image from a many channel image in memory \n
			/// valid channels from #Channel are:\n
			/// RED - will use the red channel\n
			/// GREEN - will use the green channel or zero if the image from memory doesn't have one\n
			/// BLUE - will use the blue channel or zero if the image from memory doesn't have one\n
			/// ALPHA - will use the alpha channel or zero if the image from memory doesn't have one
			/// @param	data	pointer to data
			/// @param	width	the width of the data
			/// @param	height  the height of the data
			/// @param	num_channels	the number of channels of the data
			/// @param	to_r_channel	the #Channel from data that will be written to the R channel of this image
			/// @param	to_g_channel	the #Channel from data that will be written to the G channel of this image
			/// @param	to_b_channel	the #Channel from data that will be written to the B channel of this image
			/// @param	to_a_channel	the #Channel from data that will be written to the A channel of this image
			/// @param	color_space		the #ColorSpace of the image read from data (if not sure leave this parameter to SRGB)
			void load(const unsigned char* data, unsigned int width, unsigned int height, unsigned int num_channels, const Channel& to_r_channel, const Channel& to_g_channel, const Channel& to_b_channel, const Channel& to_a_channel, const ColorSpace& color_space = ColorSpace::SRGB);

			///------------------------------------------------------------------------------------
			///	Loads an Image from memory
			/// @param	data	pointer to data containing image
			/// @param	width	the width of the image
			/// @param	height  the height of the image
			/// @param	num_channels	the number of channels of the image
			/// @param	color_space		the #ColorSpace of the image read from data (if not sure leave this parameter to SRGB)
			void load(const std::vector<unsigned char>& data, unsigned int width, unsigned int height, unsigned int num_channels, const ColorSpace& color_space = ColorSpace::SRGB);
			///------------------------------------------------------------------------------------
			///	Loads a single channel Image from a many channel image in memory \n
			/// valid channels from #Channel are:\n
			/// RED - will use the red channel\n
			/// GREEN - will use the green channel or zero if the image from memory doesn't have one\n
			/// BLUE - will use the blue channel or zero if the image from memory doesn't have one\n
			/// ALPHA - will use the alpha channel or zero if the image from memory doesn't have one
			/// @param	data	vector with data
			/// @param	width	the width of the data
			/// @param	height  the height of the data
			/// @param	num_channels	the number of channels of the data
			/// @param	to_r_channel	the #Channel from data that will be written to the R channel of this image
			/// @param	color_space		the #ColorSpace of the image read from data (if not sure leave this parameter to SRGB)
			void load(const std::vector<unsigned char>& data, unsigned int width, unsigned int height, unsigned int num_channels, const Channel& to_r_channel, const ColorSpace& color_space = ColorSpace::SRGB);
			///------------------------------------------------------------------------------------
			///	Loads a two channel Image from a many channel image in memory \n
			/// valid channels from #Channel are:\n
			/// RED - will use the red channel\n
			/// GREEN - will use the green channel or zero if the image from memory doesn't have one\n
			/// BLUE - will use the blue channel or zero if the image from memory doesn't have one\n
			/// ALPHA - will use the alpha channel or zero if the image from memory doesn't have one
			/// @param	data	vector with data
			/// @param	width	the width of the data
			/// @param	height  the height of the data
			/// @param	num_channels	the number of channels of the data
			/// @param	to_r_channel	the #Channel from data that will be written to the R channel of this image
			/// @param	to_g_channel	the #Channel from data that will be written to the G channel of this image
			/// @param	color_space		the #ColorSpace of the image read from data (if not sure leave this parameter to SRGB)
			void load(const std::vector<unsigned char>& data, unsigned int width, unsigned int height, unsigned int num_channels, const Channel& to_r_channel, const Channel& to_g_channel, const ColorSpace& color_space = ColorSpace::SRGB);
			///------------------------------------------------------------------------------------
			///	Loads a three channel Image from a many channel image in memory \n
			/// valid channels from #Channel are:\n
			/// RED - will use the red channel\n
			/// GREEN - will use the green channel or zero if the image from memory doesn't have one\n
			/// BLUE - will use the blue channel or zero if the image from memory doesn't have one\n
			/// ALPHA - will use the alpha channel or zero if the image from memory doesn't have one
			/// @param	data	vector with data
			/// @param	width	the width of the data
			/// @param	height  the height of the data
			/// @param	num_channels	the number of channels of the data
			/// @param	to_r_channel	the #Channel from data that will be written to the R channel of this image
			/// @param	to_g_channel	the #Channel from data that will be written to the G channel of this image
			/// @param	to_b_channel	the #Channel from data that will be written to the B channel of this image
			/// @param	color_space		the #ColorSpace of the image read from data (if not sure leave this parameter to SRGB)
			void load(const std::vector<unsigned char>& data, unsigned int width, unsigned int height, unsigned int num_channels, const Channel& to_r_channel, const Channel& to_g_channel, const Channel& to_b_channel, const ColorSpace& color_space = ColorSpace::SRGB);
			///------------------------------------------------------------------------------------
			///	Loads a four channel Image from a many channel image in memory \n
			/// valid channels from #Channel are:\n
			/// RED - will use the red channel\n
			/// GREEN - will use the green channel or zero if the image from memory doesn't have one\n
			/// BLUE - will use the blue channel or zero if the image from memory doesn't have one\n
			/// ALPHA - will use the alpha channel or zero if the image from memory doesn't have one
			/// @param	data	vector with data
			/// @param	width	the width of the data
			/// @param	height  the height of the data
			/// @param	num_channels	the number of channels of the data
			/// @param	to_r_channel	the #Channel from data that will be written to the R channel of this image
			/// @param	to_g_channel	the #Channel from data that will be written to the G channel of this image
			/// @param	to_b_channel	the #Channel from data that will be written to the B channel of this image
			/// @param	to_a_channel	the #Channel from data that will be written to the A channel of this image
			/// @param	color_space		the #ColorSpace of the image read from data (if not sure leave this parameter to SRGB)
			void load(const std::vector<unsigned char>& data, unsigned int width, unsigned int height, unsigned int num_channels, const Channel& to_r_channel, const Channel& to_g_channel, const Channel& to_b_channel, const Channel& to_a_channel, const ColorSpace& color_space = ColorSpace::SRGB);

			///------------------------------------------------------------------------------------
			///	Loads a multi-frame Image from memory
			/// @param	data	pointer to pointers containing image frames (num_frames frames)
			/// @param	delay	pointer to delays between frames (num_frames-1 delays)
			/// @param	width	the width of the image
			/// @param	height  the height of the image
			/// @param	num_channels	the number of channels of the image
			/// @param  num_frames		the number of frames of the image
			/// @param	color_space		the #ColorSpace of the image read from data (if not sure leave this parameter to SRGB)
			void load(const unsigned char **data, const unsigned int *delay, unsigned int width, unsigned int height, unsigned int num_channels, unsigned int num_frames, const ColorSpace& color_space = ColorSpace::SRGB);
			///------------------------------------------------------------------------------------
			///	Loads a single channel multi-frame Image from a many channel multi-frame image in memory \n
			/// valid channels from #Channel are:\n
			/// RED - will use the red channel\n
			/// GREEN - will use the green channel or zero if the image from memory doesn't have one\n
			/// BLUE - will use the blue channel or zero if the image from memory doesn't have one\n
			/// ALPHA - will use the alpha channel or zero if the image from memory doesn't have one
			/// @param	data	pointer to pointers containing image frames (num_frames frames)
			/// @param	delay	pointer to delays between frames (num_frames-1 delays)
			/// @param	width	the width of the data
			/// @param	height  the height of the data
			/// @param	num_channels	the number of channels of the data
			/// @param	num_frames		the number of frames of the image
			/// @param	to_r_channel	the #Channel from data that will be written to the R channel of this image
			/// @param	color_space		the #ColorSpace of the image read from data (if not sure leave this parameter to SRGB)
			void load(const unsigned char** data, const unsigned int *delay, unsigned int width, unsigned int height, unsigned int num_channels, unsigned int num_frames, const Channel& to_r_channel, const ColorSpace& color_space = ColorSpace::SRGB);
			///------------------------------------------------------------------------------------
			///	Loads a two channel multi-frame Image from a many channel multi-frame image in memory \n
			/// valid channels from #Channel are:\n
			/// RED - will use the red channel\n
			/// GREEN - will use the green channel or zero if the image from memory doesn't have one\n
			/// BLUE - will use the blue channel or zero if the image from memory doesn't have one\n
			/// ALPHA - will use the alpha channel or zero if the image from memory doesn't have one
			/// @param	data	pointer to pointers containing image frames (num_frames frames)
			/// @param	delay	pointer to delays between frames (num_frames-1 delays)
			/// @param	width	the width of the data
			/// @param	height  the height of the data
			/// @param	num_channels	the number of channels of the data
			///	@param	num_frames		the number of frames of the image
			/// @param	to_r_channel	the #Channel from data that will be written to the R channel of this image
			/// @param	to_g_channel	the #Channel from data that will be written to the G channel of this image
			/// @param	color_space		the #ColorSpace of the image read from data (if not sure leave this parameter to SRGB)
			void load(const unsigned char** data, const unsigned int *delay, unsigned int width, unsigned int height, unsigned int num_channels, unsigned int num_frames, const Channel& to_r_channel, const Channel& to_g_channel, const ColorSpace& color_space = ColorSpace::SRGB);
			///------------------------------------------------------------------------------------
			///	Loads a three channel multi-frame Image from a many channel multi-frame image in memory \n
			/// valid channels from #Channel are:\n
			/// RED - will use the red channel\n
			/// GREEN - will use the green channel or zero if the image from memory doesn't have one\n
			/// BLUE - will use the blue channel or zero if the image from memory doesn't have one\n
			/// ALPHA - will use the alpha channel or zero if the image from memory doesn't have one
			/// @param	data	pointer to pointers containing image frames (num_frames frames)
			/// @param	delay	pointer to delays between frames (num_frames-1 delays)
			/// @param	width	the width of the data
			/// @param	height  the height of the data
			/// @param	num_channels	the number of channels of the data
			///	@param	num_frames		the number of frames of the image
			/// @param	to_r_channel	the #Channel from data that will be written to the R channel of this image
			/// @param	to_g_channel	the #Channel from data that will be written to the G channel of this image
			/// @param	to_b_channel	the #Channel from data that will be written to the B channel of this image
			/// @param	color_space		the #ColorSpace of the image read from data (if not sure leave this parameter to SRGB)
			void load(const unsigned char** data, const unsigned int *delay, unsigned int width, unsigned int height, unsigned int num_channels, unsigned int num_frames, const Channel& to_r_channel, const Channel& to_g_channel, const Channel& to_b_channel, const ColorSpace& color_space = ColorSpace::SRGB);
			///------------------------------------------------------------------------------------
			///	Loads a four channel multi-frame Image from a many channel multi-frame image in memory \n
			/// valid channels from #Channel are:\n
			/// RED - will use the red channel\n
			/// GREEN - will use the green channel or zero if the image from memory doesn't have one\n
			/// BLUE - will use the blue channel or zero if the image from memory doesn't have one\n
			/// ALPHA - will use the alpha channel or zero if the image from memory doesn't have one
			/// @param	data	pointer to pointers containing image frames (num_frames frames)
			/// @param	delay	pointer to delays between frames (num_frames-1 delays)
			/// @param	width	the width of the data
			/// @param	height  the height of the data
			/// @param	num_channels	the number of channels of the data
			///	@param	num_frames		the number of frames of the image
			/// @param	to_r_channel	the #Channel from data that will be written to the R channel of this image
			/// @param	to_g_channel	the #Channel from data that will be written to the G channel of this image
			/// @param	to_b_channel	the #Channel from data that will be written to the B channel of this image
			/// @param	to_a_channel	the #Channel from data that will be written to the A channel of this image
			/// @param	color_space		the #ColorSpace of the image read from data (if not sure leave this parameter to SRGB)
			void load(const unsigned char** data, const unsigned int *delay, unsigned int width, unsigned int height, unsigned int num_channels, unsigned int num_frames, const Channel& to_r_channel, const Channel& to_g_channel, const Channel& to_b_channel, const Channel& to_a_channel, const ColorSpace& color_space = ColorSpace::SRGB);


			///------------------------------------------------------------------------------------
			///	Loads a multi-frame Image from memory
			/// @param	data	vector to vector containing image frames (num_frames frames)
			/// @param	delay	vector to delays between frames (num_frames-1 delays)
			/// @param	width	the width of the image
			/// @param	height  the height of the image
			/// @param	num_channels	the number of channels of the image
			/// @param  num_frames		the number of frames of the image
			/// @param	color_space		the #ColorSpace of the image read from data (if not sure leave this parameter to SRGB)
			void load(const std::vector<std::vector<unsigned char>>& data, const std::vector<unsigned int>& delay, unsigned int width, unsigned int height, unsigned int num_channels, unsigned int num_frames, const ColorSpace& color_space = ColorSpace::SRGB);
			///------------------------------------------------------------------------------------
			///	Loads a single channel multi-frame Image from a many channel multi-frame image in memory \n
			/// valid channels from #Channel are:\n
			/// RED - will use the red channel\n
			/// GREEN - will use the green channel or zero if the image from memory doesn't have one\n
			/// BLUE - will use the blue channel or zero if the image from memory doesn't have one\n
			/// ALPHA - will use the alpha channel or zero if the image from memory doesn't have one
			/// @param	data	vector to vector containing image frames (num_frames frames)
			/// @param	delay	vector to delays between frames (num_frames-1 delays)
			/// @param	width	the width of the data
			/// @param	height  the height of the data
			/// @param	num_channels	the number of channels of the data
			/// @param	num_frames		the number of frames of the image
			/// @param	to_r_channel	the #Channel from data that will be written to the R channel of this image
			/// @param	color_space		the #ColorSpace of the image read from data (if not sure leave this parameter to SRGB)
			void load(const std::vector<std::vector<unsigned char>>& data, const std::vector<unsigned int>& delay, unsigned int width, unsigned int height, unsigned int num_channels, unsigned int num_frames, const Channel& to_r_channel, const ColorSpace& color_space = ColorSpace::SRGB);
			///------------------------------------------------------------------------------------
			///	Loads a two channel multi-frame Image from a many channel multi-frame image in memory \n
			/// valid channels from #Channel are:\n
			/// RED - will use the red channel\n
			/// GREEN - will use the green channel or zero if the image from memory doesn't have one\n
			/// BLUE - will use the blue channel or zero if the image from memory doesn't have one\n
			/// ALPHA - will use the alpha channel or zero if the image from memory doesn't have one
			/// @param	data	vector to vector containing image frames (num_frames frames)
			/// @param	delay	vector to delays between frames (num_frames-1 delays)
			/// @param	width	the width of the data
			/// @param	height  the height of the data
			/// @param	num_channels	the number of channels of the data
			///	@param	num_frames		the number of frames of the image
			/// @param	to_r_channel	the #Channel from data that will be written to the R channel of this image
			/// @param	to_g_channel	the #Channel from data that will be written to the G channel of this image
			/// @param	color_space		the #ColorSpace of the image read from data (if not sure leave this parameter to SRGB)
			void load(const std::vector<std::vector<unsigned char>>& data, const std::vector<unsigned int>& delay, unsigned int width, unsigned int height, unsigned int num_channels, unsigned int num_frames, const Channel& to_r_channel, const Channel& to_g_channel, const ColorSpace& color_space = ColorSpace::SRGB);
			///------------------------------------------------------------------------------------
			///	Loads a three channel multi-frame Image from a many channel multi-frame image in memory \n
			/// valid channels from #Channel are:\n
			/// RED - will use the red channel\n
			/// GREEN - will use the green channel or zero if the image from memory doesn't have one\n
			/// BLUE - will use the blue channel or zero if the image from memory doesn't have one\n
			/// ALPHA - will use the alpha channel or zero if the image from memory doesn't have one
			/// @param	data	vector to vector containing image frames (num_frames frames)
			/// @param	delay	vector to delays between frames (num_frames-1 delays)
			/// @param	width	the width of the data
			/// @param	height  the height of the data
			/// @param	num_channels	the number of channels of the data
			///	@param	num_frames		the number of frames of the image
			/// @param	to_r_channel	the #Channel from data that will be written to the R channel of this image
			/// @param	to_g_channel	the #Channel from data that will be written to the G channel of this image
			/// @param	to_b_channel	the #Channel from data that will be written to the B channel of this image
			/// @param	color_space		the #ColorSpace of the image read from data (if not sure leave this parameter to SRGB)
			void load(const std::vector<std::vector<unsigned char>>& data, const std::vector<unsigned int>& delay, unsigned int width, unsigned int height, unsigned int num_channels, unsigned int num_frames, const Channel& to_r_channel, const Channel& to_g_channel, const Channel& to_b_channel, const ColorSpace& color_space = ColorSpace::SRGB);
			///------------------------------------------------------------------------------------
			///	Loads a four channel multi-frame Image from a many channel multi-frame image in memory \n
			/// valid channels from #Channel are:\n
			/// RED - will use the red channel\n
			/// GREEN - will use the green channel or zero if the image from memory doesn't have one\n
			/// BLUE - will use the blue channel or zero if the image from memory doesn't have one\n
			/// ALPHA - will use the alpha channel or zero if the image from memory doesn't have one
			/// @param	data	vector to vector containing image frames (num_frames frames)
			/// @param	delay	vector to delays between frames (num_frames-1 delays)
			/// @param	width	the width of the data
			/// @param	height  the height of the data
			/// @param	num_channels	the number of channels of the data
			///	@param	num_frames		the number of frames of the image
			/// @param	to_r_channel	the #Channel from data that will be written to the R channel of this image
			/// @param	to_g_channel	the #Channel from data that will be written to the G channel of this image
			/// @param	to_b_channel	the #Channel from data that will be written to the B channel of this image
			/// @param	to_a_channel	the #Channel from data that will be written to the A channel of this image
			/// @param	color_space		the #ColorSpace of the image read from data (if not sure leave this parameter to SRGB)
			void load(const std::vector<std::vector<unsigned char>>& data, const std::vector<unsigned int>& delay, unsigned int width, unsigned int height, unsigned int num_channels, unsigned int num_frames, const Channel& to_r_channel, const Channel& to_g_channel, const Channel& to_b_channel, const Channel& to_a_channel, const ColorSpace& color_space = ColorSpace::SRGB);
















			
			
			
			
			
			~Image();
			
			///------------------------------------------------------------------------------------
			/// Computes the difference between this image and another image and returns the result as another image\n
			/// NOTE: if the images have different number of channels or different sizes the operations is performed on the minimum width/height/number of channels
			/// NOTE: the results are clamped in [0,255]
			/// @param other the other image
			/// @return the result image
			Image difference(const Image& other) const;
			///------------------------------------------------------------------------------------
			/// Computes the sum between this image and another image and returns the result as another image\n
			/// NOTE: if the images have different number of channels or different sizes the operations is performed on the minimum width/height/number of channels
			/// NOTE: the results are clamped in [0,255]
			/// @param other the other image
			/// @return the result image
			Image sum(const Image& other) const;
	

			///------------------------------------------------------------------------------------
			/// Resizes the image\n
			/// If there are multiple frames in the image, all are resized
			/// @param width the width of the resized image
			/// @param height the height of the resized image
			void resize(unsigned int width, unsigned int height);
			///------------------------------------------------------------------------------------
			/// Deletes all existing data (zeroes out) and creates an empty image with structure
			/// @param width the width of the reset image
			/// @param height the height of the reset image
			/// @param num_channels the number of channels of the reset image
			/// @param num_frames the number of frames of the reset image
			void reset(unsigned int width, unsigned int height, unsigned int num_channels = 3, unsigned int num_frames = 1);
			///------------------------------------------------------------------------------------
			/// Deletes all existing data but leaves the structure intact, zeroing out data but \n
			/// keeping width, height,number of channels, number of frames and allocated memory
			void clearData();
			///------------------------------------------------------------------------------------
			/// Deletes all existing data and the data structures, making the image identical to an empty constructed Image object \n
			void clear();
			///------------------------------------------------------------------------------------
			/// Swap the content of this image with the content form another image \n
			/// @param other the other image
			void swap(Image& other);

			///------------------------------------------------------------------------------------
			/// Adds a frame to an image. The frame is added in the last position \n
			/// the width, height and number of channels MUST match those in this image
			/// @param data the data of the new frame
			/// @param delay the delay of the new frame
			void frameAdd(const unsigned char* data, unsigned int delay);
			///------------------------------------------------------------------------------------
			/// Adds a frame to an image. The frame is added in the last position \n
			/// the width, height and number of channels MUST match those in this image
			/// @param data the data of the new frame
			/// @param delay the delay of the new frame
			void frameAdd(const std::vector<unsigned char>& data, unsigned int delay);
			///------------------------------------------------------------------------------------
			/// Adds a frame to an image. The frame is added to the specified position \n
			/// the width, height and number of channels MUST match those in this image
			/// @param data the data of the new frame
			/// @param delay the delay of the new frame
			/// @param frame the position of the new frame
			void frameAdd(const unsigned char* data, unsigned int delay, unsigned int frame);
			///------------------------------------------------------------------------------------
			/// Adds a frame to an image. The frame is added to the specified position \n
			/// the width, height and number of channels MUST match those in this image
			/// @param data the data of the new frame
			/// @param delay the delay of the new frame
			/// @param frame the position of the new frame
			void frameAdd(const std::vector<unsigned char>& data, unsigned int delay, unsigned int frame);
			///------------------------------------------------------------------------------------
			/// Removes a frame from the image \n
			/// @param frame the position of the removed frame
			void frameRemove(unsigned int frame);
			///------------------------------------------------------------------------------------
			/// Removes all frame from the image, leaving structure such as width/height/num_channels \n
			/// untouched, but removing all frame information
			void framesRemove();
			///------------------------------------------------------------------------------------
			/// Reverses the order of the frames
			void framesReverse();
			///------------------------------------------------------------------------------------
			/// Accelerates or deccelerates the speed (delay) of the frames
			/// @param factor the speed modifier percent:\n
			///	1 = animate at 1% of original speed (speed up)\n
			///	125 = animate at 125% of original speed (slowed down)\n
			///	100 = animate at 100% of original speed (no change)\n
			/// NOTE: if the factor is a negative number then the frames are also reversed.
			void framesSetSpeed(float factor);

			///------------------------------------------------------------------------------------
			///	Switches two channels \n
			/// valid channels from #Channel are:\n
			/// RED - will set the red channel\n
			/// GREEN - will set the green channel \n
			/// BLUE - will set the blue channel\n
			/// ALPHA - will set the alpha channel
			/// @param	channel1	the first #Channel that will be switched
			/// @param	channel2	the second #Channel that will be switched
			void channelSwitch(const Channel& channel1, const Channel& channel2);
			///------------------------------------------------------------------------------------
			/// inserts a new channel, in the specified channel position \n
			/// (R = first, G= second, B= third, A = fourth). The rest of the channels are shifted
			/// to the right.\n 
			/// E.g.: inserting in the red channel on a RGB image transforms the current
			/// image into a RGBA image where R is emtpy, G=oldR, B =oldG, A = oldB
			/// NOTE: can't insert into a RGBA image, as all channels already exist.
			/// NOTE: can't insert beyond the last empty channel (e.g. can insert B into a RG image but can't insert A into a RG image)
			/// NOTE: accepts only single channel tokens (RED, GREEN, BLUE, ALPHA)
			/// @param channel	where to insert the new empty channel
			void channelInsert(const Channel& channel);
			///------------------------------------------------------------------------------------
			/// removes an existing channel, from the specified channel position \n
			/// (R = first, G= second, B= third, A = fourth). The rest of the channels are shifted
			/// to the left.\n 
			/// E.g.: removing the red channel from a RGBA image transforms the current
			/// image into a RGB image where R = oldG, G=oldB, B =oldA
			/// NOTE: can't remove from an empty image. 
			/// NOTE: accepts only single channel tokens (RED, GREEN, BLUE, ALPHA)
			/// @param channel	which channel will be removed?
			void channelRemove(const Channel& channel);
			///------------------------------------------------------------------------------------
			/// clones the contents of a channel into another channel
			/// @param channel_src	the source channel
			/// @param channel_dst	the destination channel
			/// NOTE: accepts only single channel tokens (RED, GREEN, BLUE, ALPHA)
			void channelClone(const Channel& channel_src, const Channel& channel_dst);
			///------------------------------------------------------------------------------------
			///	Sets a channel to specified color \n
			/// valid channels from #Channel are:\n
			/// ALL - will set all channels\n
			/// ALLNOA - will set all channels except the alpha channel\n
			/// RED - will set the red channel\n
			/// GREEN - will set the green channel \n
			/// BLUE - will set the blue channel\n
			/// ALPHA - will set the alpha channel
			/// @param	channel	the #Channel from that will be set
			/// @param	value	the 0-255 value for this channel (e.g. 255 on the RED channel will make the image red if the other channels are 0, 0)
			void channelColor(const Channel& channel, unsigned char value);
			///------------------------------------------------------------------------------------
			///	Sets a channel to white \n
			/// valid channels from #Channel are:\n
			/// ALL - will set all channels\n
			/// ALLNOA - will set all channels except the alpha channel\n
			/// RED - will set the red channel\n
			/// GREEN - will set the green channel \n
			/// BLUE - will set the blue channel\n
			/// ALPHA - will set the alpha channel
			/// @param	channel	the #Channel from that will be set
			void channelWhite(const Channel& channel);
			///------------------------------------------------------------------------------------
			///	Sets a channel to black \n
			/// valid channels from #Channel are:\n
			/// ALL - will set all channels\n
			/// ALLNOA - will set all channels except the alpha channel\n
			/// RED - will set the red channel\n
			/// GREEN - will set the green channel \n
			/// BLUE - will set the blue channel\n
			/// ALPHA - will set the alpha channel
			/// @param	channel	the #Channel from that will be set
			void channelBlack(const Channel& channel);
			///------------------------------------------------------------------------------------
			///	Sets a channel to the minimum value of all channels \n
			/// valid channels from #Channel are:\n
			/// ALL - will set all channels\n
			/// ALLNOA - will set all channels except the alpha channel\n
			/// RED - will set the red channel\n
			/// GREEN - will set the green channel \n
			/// BLUE - will set the blue channel\n
			/// ALPHA - will set the alpha channel
			/// @param	channel	the #Channel from that will be set
			void channelMin(const Channel& channel);
			///------------------------------------------------------------------------------------
			///	Sets a channel to the maximum value of all channels \n
			/// valid channels from #Channel are:\n
			/// ALL - will set all channels\n
			/// ALLNOA - will set all channels except the alpha channel\n
			/// RED - will set the red channel\n
			/// GREEN - will set the green channel \n
			/// BLUE - will set the blue channel\n
			/// ALPHA - will set the alpha channel
			/// @param	channel	the #Channel from that will be set
			void channelMax(const Channel& channel);
			///------------------------------------------------------------------------------------
			///	Inverts a channel \n
			/// valid channels from #Channel are:\n
			/// ALL - will set all channels\n
			/// ALLNOA - will set all channels except the alpha channel\n
			/// RED - will set the red channel\n
			/// GREEN - will set the green channel \n
			/// BLUE - will set the blue channel\n
			/// ALPHA - will set the alpha channel
			/// @param	channel	the #Channel from that will be set
			void channelInvert(const Channel& channel);


			///------------------------------------------------------------------------------------
			///	Flips the image on the X axis 
			void transformFlipX();
			///------------------------------------------------------------------------------------
			///	Flips the image on the Y axis 
			void transformFlipY();
			///------------------------------------------------------------------------------------
			///	Flips the image on the X and Y axes 
			void transformFlipXY();
			///------------------------------------------------------------------------------------
			///	Transforms the current image into a grayscale counterpart. \n
			/// If there is an alpha channel it is not considered.\n
			/// The applied transformations (in linear space) are:\n
			/// if(only r)	-> gray = r\n
			/// if(r,g)		-> gray = r*0.2126 + g*0.7152\n
			/// if(r,g,b)	-> gray = r*0.2126 + g*0.6152 + b*0.0722\n
			/// NOTE: the channels are preserved and ALL channels contain the same grayscale value
			void transformGrayscale();
			///------------------------------------------------------------------------------------
			///	Switches the channels from RGB to RBG \n
			/// This operation requires at least 3 channels. If an alpha channel exists, it is left unchanged
			void transformRGBtoRBG();
			///------------------------------------------------------------------------------------
			///	Switches the channels from RGB to BGR \n
			/// This operation requires at least 3 channels. If an alpha channel exists, it is left unchanged
			void transformRGBtoBGR();
			///------------------------------------------------------------------------------------
			///	Switches the channels from RGB to BRG \n
			/// This operation requires at least 3 channels. If an alpha channel exists, it is left unchanged
			void transformRGBtoBRG();
			///------------------------------------------------------------------------------------
			///	Switches the channels from RGB to GRB \n
			/// This operation requires at least 3 channels. If an alpha channel exists, it is left unchanged
			void transformRGBtoGRB();
			///------------------------------------------------------------------------------------
			///	Switches the channels from RGB to GBR \n
			/// This operation requires at least 3 channels. If an alpha channel exists, it is left unchanged
			void transformRGBtoGBR();
			///------------------------------------------------------------------------------------
			/// transforms the image to the SRGB color space\n
			/// The SRGB (https://en.wikipedia.org/wiki/SRGB) space is non-linear, standard in photos,
			///	cameras and monitors and it is used for color/albedo/diffuse/emission maps in rendering\n
			/// This operation requires at least 3 channels. If an alpha channel exists, it is left unchanged
			void transformToSRGB();
			///------------------------------------------------------------------------------------
			/// transforms the image to the RGB color space \n
			/// The RGB (https://en.wikipedia.org/wiki/RGB_color_space) space is linear, and it is used
			/// for normal/bump/specular/glow/dudv/height/ao/noise maps\n
			/// This operation requires at least 3 channels. If an alpha channel exists, it is left unchanged
			void transformToRGB();


			///------------------------------------------------------------------------------------
			///	Saves the image\n
			/// If the filename extension is gif and if the image contains multiple frames, it is
			///	saved into to an animated .gif file, otherwise it is saved the filename extension,
			/// provided that this extension is one of the following: GIF, PNG, TGA, BMP. \n
			/// If a format requires channels not contained by the image these will be zeroed out.
			/// Some formats (TGA, PNG) contain grayscale saving, saving single-channel images as
			/// grayscale data.
			/// @param filename name of the file in which the image will be saved
			/// @return true if successfull, false if filename was impossible to access
			bool save(const std::string& filename) const;
			///------------------------------------------------------------------------------------
			///	Saves a single frame of the image\n
			/// The image is saved with the filename extension, provided that this extension is one
			///	of the following: GIF, PNG, TGA, BMP.\n
			/// If a format requires channels not contained by the image these will be zeroed out.
			/// Some formats (TGA, PNG) contain grayscale saving, saving single-channel images as
			/// grayscale data.
			/// @param filename name of the file in which the image will be saved
			/// @param frame the number of the requested frame
			/// @return true if successfull, false if filename was impossible to access
			bool saveFrame(const std::string& filename, unsigned int frame) const;
			///------------------------------------------------------------------------------------
			///	Saves a single channel of this image \n
			/// This function is especially useful for quick visualizations \n
			/// If the filename extension is gif and if the image contains multiple frames, it is\n
			///	saved into to an animated .gif file, otherwise it is saved with the filename extension,
			/// provided that this extension is one of the following: GIF, PNG, TGA, BMP\n
			/// GIF saves at 8bpp as a red channel image \n
			/// PNG saves at 8bpp as a grayscale image\n
			/// TGA saves at 8bpp as a grayscale image\n
			/// BMP saves at 24bpp as a color image\n
			/// If channel ALL is provided this function becomes identical to save\n
			/// If the alpha channel si not extracted then alpha is set to 255\n
			/// valid channels from #Channel are:\n
			/// RED - will save the red channel\n
			/// GREEN - will save the green channel or zero if the image from memory doesn't have one\n
			/// BLUE - will save the blue channel or zero if the image from memory doesn't have one\n
			/// ALPHA - will save the alpha channel or zero if the image from memory doesn't have one\n
			/// ALL - will save all channels (identical to save)\n
			/// ALLNOA - will save all channels except the alpha channel
			/// @param filename name of the file in which the image will be saved
			/// @param channel the channel that will be saved
			/// @return true if successfull, false if filename was impossible to access
			bool saveChannel(const std::string& filename, const Channel& channel) const;
			///------------------------------------------------------------------------------------
			///	Saves a single channel of a single frame of this image \n
			/// The result is saved with the filename extension, provided that this extension is one\n
			///	of the following: GIF, PNG, TGA, BMP, otherwise it is saved with the filename extension,
			/// provided that this extension is one of the following: GIF, PNG, TGA, BMP\n
			/// GIF saves at 8bpp as a red channel image \n
			/// PNG saves at 8bpp as a grayscale image\n
			/// TGA saves at 8bpp as a grayscale image\n
			/// BMP saves at 24bpp as a color image\n
			/// If channel ALL is provided this function becomes identical to saveFrame\n
			/// If the alpha channel si not extracted then alpha is set to 255\n
			/// valid channels from #Channel are:\n
			/// RED - will save the red channel\n
			/// GREEN - will save the green channel or zero if the image from memory doesn't have one\n
			/// BLUE - will save the blue channel or zero if the image from memory doesn't have one\n
			/// ALPHA - will save the alpha channel or zero if the image from memory doesn't have one\n
			/// ALL - will save all channels (identical to save)\n
			/// ALLNOA - will save all channels except the alpha channel
			/// @param filename name of the file in which the image will be saved
			/// @param channel the channel that will be saved
			/// @param frame the number of the requested frame
			/// @return true if successfull, false if filename was impossible to access
			bool saveFrameChannel(const std::string& filename, const Channel& channel, unsigned int frame) const;


			///------------------------------------------------------------------------------------
			/// Sets the gamma used for implicit LDR conversion of HDR files (.hdr)\n
			/// the default value is 2.2
			/// @param gamma the new gamma
			void setHDRgamma(float gamma);
			///------------------------------------------------------------------------------------
			/// Sets the scale factor used for implicit LDR conversion of HDR files (.hdr)\n
			/// the default value is 1.0
			/// @param scale the new scale
			void setHDRscale(float scale);
			///------------------------------------------------------------------------------------
			/// returns the gamma used for implicit LDR conversion of HDR files (.hdr)
			/// @return the gamma used for implicit LDR conversion of HDR files (.hdr)
			float getHDRgamma() const;
			///------------------------------------------------------------------------------------
			/// returns the scale factor used for implicit LDR conversion of HDR files (.hdr)
			/// @return the scale factor used for implicit LDR conversion of HDR files (.hdr)
			float getHDRscale() const;


			///------------------------------------------------------------------------------------
			///	returns the width of this image
			/// @return the width of this image
			unsigned int getWidth() const;
			///------------------------------------------------------------------------------------
			///	returns the height of this image
			/// @return the height of this image
			unsigned int getHeight() const;
			///------------------------------------------------------------------------------------
			///	returns the number of channels of this image
			/// @return the number of channels of this image
			unsigned int getNumChannels() const;
			///------------------------------------------------------------------------------------
			///	returns a pointer to the raw data, does NOT give ownership\n
			/// if called on multi-frame image formats(animated GIF) this function will return
			/// a pointer to the first frame
			/// @return a pointer to the raw data 
			unsigned char* getData();
			///------------------------------------------------------------------------------------
			///	returns a reference to the image data\n
			/// if called on multi-frame image formats(animated GIF) this function will return
			/// a reference to the first frame
			/// @return a vector reference of to the raw data 
			std::vector<unsigned char>& getDataVec();
			///------------------------------------------------------------------------------------
			///	returns a const pointer to the raw data\n
			/// if called on multi-frame image formats(animated GIF) this function will return
			/// a pointer to the first frame
			/// @return a const pointer to the raw data 
			const unsigned char* getData() const;
			///------------------------------------------------------------------------------------
			///	returns a const reference to the image data\n
			/// if called on multi-frame image formats(animated GIF) this function will return
			/// a const reference to the first frame
			/// @return a const vector reference of to the raw data 
			const std::vector<unsigned char>& getDataVec() const;
			///------------------------------------------------------------------------------------
			///	returns a pointer to the raw data of a specific frame of this image\n
			/// this function should be used for multi-frame image formats (animated GIF),
			/// but it can be used for single-frame images with frame_num=0
			/// @param frame_num the number of the requested frame
			/// @return a pointer to the raw data for frame frame_num. The pointer is owned by this Image.
			unsigned char* getFrameData(unsigned int frame_num = 0);
			///------------------------------------------------------------------------------------
			///	returns a vector reference to the data of a specific frame of this image\n
			/// this function should be used for multi-frame image formats (animated GIF),
			/// but it can be used for single-frame images with frame_num=0
			/// @param frame_num the number of the requested frame
			/// @return a vector reference to the raw data for frame frame_num.
			std::vector<unsigned char>& getFrameDataVec(unsigned int frame_num = 0);
			///------------------------------------------------------------------------------------
			///	returns a const pointer to the raw data of a specific frame of this image\n
			/// this function should be used for multi-frame image formats (animated GIF),
			/// but it can be used for single-frame images with frame_num=0
			/// @param frame_num the number of the requested frame
			/// @return a const pointer to the raw data for frame frame_num
			const unsigned char* getFrameData(unsigned int frame_num = 0) const;
			///------------------------------------------------------------------------------------
			///	returns a const vector reference to the raw data of a specific frame of this image\n
			/// this function should be used for multi-frame image formats (animated GIF),
			/// but it can be used for single-frame images with frame_num=0
			/// @param frame_num the number of the requested frame
			/// @return a const vector reference to the raw data for frame frame_num
			const std::vector<unsigned char>& getFrameDataVec(unsigned int frame_num = 0) const;
			///------------------------------------------------------------------------------------
			///	returns a pointer to the delays of the frames (num_frames-1 in size). Does NOT give OWNERSHIP\n
			/// this function should be used for multi-frame image formats (animated GIF),
			/// but it can be used for single-frame returning a pointer to a single int
			/// @return a pointer to the delays for all frames
			unsigned int* getDelays();
			///------------------------------------------------------------------------------------
			///	returns a const pointer to the delays of the frames (num_frames-1 in size)\n
			/// this function should be used for multi-frame image formats (animated GIF),
			/// but it can be used for single-frame returning a pointer to a single int
			/// @return a pointer to the delays for all frames
			const unsigned int* getDelays() const;
			///------------------------------------------------------------------------------------
			///	returns a reference vector to the delays of the frames (num_frames-1 in size)\n
			/// this function should be used for multi-frame image formats (animated GIF),
			/// but it can be used for single-frame returning a reference to a single int vector
			/// @return a vector reference to the delays for all frames
			std::vector<unsigned int>& getDelaysVec();
			///------------------------------------------------------------------------------------
			///	returns a const reference vector to the delays of the frames (num_frames-1 in size)\n
			/// this function should be used for multi-frame image formats (animated GIF),
			/// but it can be used for single-frame returning a const reference to a single int vector
			/// @return a const vector reference to the delays for all frames
			const std::vector<unsigned int>& getDelaysVec() const;
			///------------------------------------------------------------------------------------
			///	returns the delay for a specified frame\n
			/// this function should be used for multi-frame image formats (animated GIF), 
			/// but it can be used for single-frame images with frame_num=0
			/// @param frame_num the number of the requested frame
			/// @return the delay for the specified frame
			unsigned int getFrameDelay(unsigned int frame_num = 0) const;
			///------------------------------------------------------------------------------------
			///	sets the delay for a specified frame\n
			/// this function should be used for multi-frame image formats (animated GIF),
			/// but it can be used for single-frame images with frame_num=0
			/// @param frame_num the number of the specified frame
			/// @param delay the delay for the specified frame
			void setFrameDelay(unsigned int frame_num, unsigned int delay);
			///------------------------------------------------------------------------------------
			///	returns the total delay for all the frames of the image\n
			/// this function should be used for multi-frame image formats (animated GIF),
			/// but it can be used for single-frame images, returning 0
			/// @return the total delay for all the frames of the image
			unsigned int getTotalFrameDelay() const;
			///------------------------------------------------------------------------------------
			///	returns the total size of the data of the image, identical to getMemory()
			/// @return the total size of the data of the image
			unsigned int getDataSize() const;
			///------------------------------------------------------------------------------------
			///	returns the total size of the data of the image, identical to getDataSize()
			/// @return the total size of the data of the image
			unsigned int getMemory() const;
			///------------------------------------------------------------------------------------
			///	returns number of frames of the image\n
			/// this function should be used for multi-frame image formats (animated GIF),
			/// but it can be used for single-frame images, returning 1
			/// @return number of frames of the image
			unsigned int getNumFrames() const;
			///------------------------------------------------------------------------------------
			///	returns the color space used in the image
			/// @return the color space used in the image
			ColorSpace getColorSpace() const;


			///------------------------------------------------------------------------------------
			///	access data from this image, operator, array-style\n
			/// in case this is a multiframe image the first frame will be accessed
			/// @param idx the location at which data is accessed 
			/// @return a reference to the accessed location
			unsigned char& operator()(unsigned int idx);
			///------------------------------------------------------------------------------------
			///	access data from this image, operator, array-style\n
			/// in case this is a multiframe image the first frame will be accessed
			/// @param idx the location at which data is accessed 
			/// @return a const reference to the accessed location
			const unsigned char& operator()(unsigned int idx) const;
			///------------------------------------------------------------------------------------
			///	access data from this image, operator, matrix-style/image-style\n
			/// in case this is a multiframe image the first frame will be accessed
			/// image coordinates are top-to-bottom & left-to-right, like this:\n
			/// |--------> x\n
			/// |\n
			/// |\n
			/// v\n
			/// y
			/// @param x the coordinate on the x axis 
			/// @param y the coordinate on the y axis
			/// @param c the channel (0=r, 1=g, 2=b, 3=a)
			/// @return a reference to the accessed location
			unsigned char& operator()(unsigned int x, unsigned int y, unsigned int c);
			///------------------------------------------------------------------------------------
			///	access data from this image, operator, matrix-style/image-style\n
			/// in case this is a multiframe image the first frame will be accessed
			/// image coordinates are top-to-bottom & left-to-right, like this:\n
			/// |--------> x\n
			/// |\n
			/// |\n
			/// v\n
			/// y
			/// @param x the coordinate on the x axis 
			/// @param y the coordinate on the y axis 
			/// @param c the channel (0=r, 1=g, 2=b, 3=a)
			/// @return a const reference to the accessed location
			const unsigned char& operator()(unsigned int x, unsigned int y, unsigned int c) const;
			///------------------------------------------------------------------------------------
			///	access data from this image, operator, matrix-style/image-style\n
			/// image coordinates are top-to-bottom & left-to-right, like this:\n
			/// |--------> x\n
			/// |\n
			/// |\n
			/// v\n
			/// y
			/// @param x the coordinate on the x axis 
			/// @param y the coordinate on the y axis 
			/// @param c the channel (0=r, 1=g, 2=b, 3=a)
			/// @param f the requested frame
			/// @return a reference to the accessed location
			unsigned char& operator()(unsigned int x, unsigned int y, unsigned int c, unsigned int f);
			///------------------------------------------------------------------------------------
			///	access data from this image, operator, matrix-style/image-style\n
			/// image coordinates are top-to-bottom & left-to-right, like this:\n
			/// |--------> x\n
			/// |\n
			/// |\n
			/// v\n
			/// y
			/// @param x the coordinate on the x axis 
			/// @param y the coordinate on the y axis 
			/// @param c the channel (0=r, 1=g, 2=b, 3=a)
			/// @param f the requested frame
			/// @return a const reference to the accessed location
			const unsigned char& operator()(unsigned int x, unsigned int y,unsigned int c, unsigned int f) const;
		private:
			float internalRGBtoSRGB(unsigned char ch);
			float internalSRGBtoRGB(unsigned char ch);
			/// internal usage
			bool internalSave(const std::string& filename, int frame) const;
			bool internalSaveChannel(const std::string& filename, const Channel& channel, int frame ) const;
			bool internalLoadFile(const std::string& filename, int ndst, int r, int g, int b, int a);
			void internalLoadImg(const Image& img, int ndst, int r, int g, int b, int a);
			void internalLoadMemory(const unsigned char *memory, int w, int h, int nsrc, int ndst, int r, int g, int b, int a);
			void internalLoadMemory(const unsigned char **memory, const unsigned int *times, int w, int h, int nsrc, int frames, int ndst, int r, int g, int b, int a);
			void internalLoadMemory(const std::vector<std::vector<unsigned char>>& memory, const std::vector<unsigned int>& times, int w, int h, int nsrc, int frames, int ndst, int r, int g, int b, int a);
			void internalCopy(unsigned char* dst, const unsigned char* src, int w, int h, int ndst, int nsrc, int r, int g, int b, int a);
		private:
			std::vector<std::vector<unsigned char>> data;
			std::vector<unsigned int> delay;
			unsigned int width=0, height=0, num_channels=0, num_frames=0, total_delay=0;
			float hdr_gamma=0, hdr_scale=0;
			ColorSpace color_space;
	};
}

//include guard
#endif