///---------------------------------------------------------------------------------------------------------------------
/// The MIT License (MIT)
/// 
/// Copyright (c) 2016 Lucian Petrescu, lucian (dot) petrescu (dot) 24 (at) gmail (dot) com
/// 
/// Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation 
/// files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, 
/// modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software 
/// is furnished to do so, subject to the following conditions:
/// 
/// The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
/// 
/// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
/// OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE 
/// LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR 
/// IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
///---------------------------------------------------------------------------------------------------------------------

#include <iostream>
#include <vector>

#include "lap_image.hpp"


void test_basic(){
	using namespace lap;
	{Image img(std::string("../asset/animated.gif")); std::cout << "ANIMATED GIF(3+1) " << img.getNumChannels() << std::endl; img.save("../asset/out_basic_animated.gif"); }
	//{Image img(std::string("../asset/animated_interlace.gif")); std::cout << "ANIMATED GIF I(4) " << img.getNumChannels() << std::endl; img.save("../asset/out_basic_animated_interlaced.gif"); }
	{Image img(std::string("../asset/img.bmp")); std::cout << "BMP(3) " << img.getNumChannels() << std::endl; img.save("../asset/out_basic_img.bmp"); }
	{Image img(std::string("../asset/img.gif")); std::cout << "GIF(3+1) " << img.getNumChannels() << std::endl; img.save("../asset/out_basic_img.gif"); }
	//{Image img(std::string("../asset/img.hdr")); std::cout << "HDR(3) " << img.getNumChannels() << std::endl; img.save("../asset/out_basic_hdr.png"); }	//needs asset!
	{Image img(std::string("../asset/img.jpg")); std::cout << "JPG(3) " << img.getNumChannels() << std::endl; img.save("../asset/out_basic_jpg.png"); }
	{Image img(std::string("../asset/img.pgm")); std::cout << "PGM(1) " << img.getNumChannels() << std::endl; img.save("../asset/out_basic_pgm.png"); }
	{Image img(std::string("../asset/img.png")); std::cout << "PNG(4) " << img.getNumChannels() << std::endl; img.save("../asset/out_basic_png.png"); }
	{Image img(std::string("../asset/img.ppm")); std::cout << "PPM(3) " << img.getNumChannels() << std::endl; img.save("../asset/out_basic_ppm.png"); }
	{Image img(std::string("../asset/img.tga")); std::cout << "TGA(3) " << img.getNumChannels() << std::endl; img.save("../asset/out_basic_tga.tga"); }
	{Image img(std::string("../asset/img2.tga")); std::cout << "TGA(4) " << img.getNumChannels() << std::endl; img.save("../asset/out_basic_tga2.tga"); }

	//gifs
	//for (int i = 0; i < 21; i++){
	//	Image img(std::string("../asset/gif/gif") + std::to_string(i)+ ".gif");
	//	img.save(std::string("../asset/out_gif_") + std::to_string(i) + ".gif");
	//}
}
void test_ctor_save(){
	using namespace lap;
	bool ret;
	
	Image img(std::string("../asset/thisdoesnotexist.pgm"), Image::ColorSpace::SRGB, &ret);
	assert(ret == false);
	Image img2(std::string("../asset/img.pgm"), Image::ColorSpace::SRGB, &ret);
	img2.save("../asset/out_ctor_pgm.gif");
	img2.save("../asset/out_ctor_pgm.png");
	img2.save("../asset/out_ctor_pgm.tga");
	img2.save("../asset/out_ctor_pgm.bmp");
	Image img3(std::string("../asset/animated.gif"), Image::ColorSpace::SRGB, &ret);
	img3.save("../asset/out_ctor_gifanimated.gif");
	img3.save("../asset/out_ctor_gifanimated.png");
	img3.save("../asset/out_ctor_gifanimated.tga");
	img3.save("../asset/out_ctor_gifanimated.bmp");
	Image img4(std::string("../asset/img.gif"), Image::ColorSpace::SRGB, &ret);
	img4.save("../asset/out_ctor_gif.gif");
	img4.save("../asset/out_ctor_gif.png");
	img4.save("../asset/out_ctor_gif.tga");
	img4.save("../asset/out_ctor_gif.bmp");
	
	//Image img5(std::string("../asset/img.hdr"), Image::ColorSpace::SRGB, &ret);
	//img5.save("../asset/out_ctor_hdr.gif");
	//img5.save("../asset/out_ctor_hdr.png");
	//img5.save("../asset/out_ctor_hdr.tga");
	//img5.save("../asset/out_ctor_hdr.bmp");

	Image img6(std::string("../asset/img.ppm"), Image::ColorSpace::SRGB, &ret);
	img6.save("../asset/out_ctor_ppm.gif");
	img6.save("../asset/out_ctor_ppm.png");
	img6.save("../asset/out_ctor_ppm.tga");
	img6.save("../asset/out_ctor_ppm.bmp");
	
	Image img7(std::string("../asset/img.png"), Image::ColorSpace::SRGB, &ret);
	//extensions for channel saves and single channels
	img7.saveChannel("../asset/out_ctor_channel_r.bmp", Image::Channel::RED);
	img7.saveChannel("../asset/out_ctor_channel_r.gif", Image::Channel::RED);
	img7.saveChannel("../asset/out_ctor_channel_r.png", Image::Channel::RED);
	img7.saveChannel("../asset/out_ctor_channel_r.tga", Image::Channel::RED);
	img7.saveChannel("../asset/out_ctor_channel_g.png", Image::Channel::GREEN);
	img7.saveChannel("../asset/out_ctor_channel_b.png", Image::Channel::BLUE);
	img7.saveChannel("../asset/out_ctor_channel_a.png", Image::Channel::ALPHA);
	img7.saveChannel("../asset/out_ctor_channel_all.png", Image::Channel::ALL);
	
}
void test_ctor_save_animated(){
	using namespace lap;
	Image img3(std::string("../asset/animated.gif"));
	img3.saveChannel("../asset/out_ctor_animated_r.gif", Image::Channel::RED);
	img3.saveChannel("../asset/out_ctor_animated_g.gif", Image::Channel::GREEN);
	img3.saveChannel("../asset/out_ctor_animated_b.gif", Image::Channel::BLUE);
	img3.saveChannel("../asset/out_ctor_animated_a.gif", Image::Channel::ALPHA);
}
void test_ctor_single_channel(){
	using namespace lap;
	Image img1(std::string("../asset/img.png"), Image::Channel::RED); 
	img1.save("../asset/out_ctor_single_r.png");
	Image img2(std::string("../asset/img.png"), Image::Channel::GREEN); 
	img2.save("../asset/out_ctor_single_g.png");
	Image img3(std::string("../asset/img.png"), Image::Channel::BLUE);
	img3.save("../asset/out_ctor_single_b.png");
	Image img4(std::string("../asset/img.png"), Image::Channel::ALPHA); 
	img4.save("../asset/out_ctor_single_a.png");
}
void test_ctor_multi_channel(){
	using namespace lap;
	Image img1(std::string("../asset/img.png"), Image::Channel::RED, Image::Channel::GREEN);
	img1.saveChannel("../asset/out_ctor_multi_rg_r.tga", Image::Channel::RED);
	img1.saveChannel("../asset/out_ctor_multi_rg_g.gif", Image::Channel::GREEN);

	Image img2(std::string("../asset/img.png"), Image::Channel::GREEN, Image::Channel::BLUE);
	img2.saveChannel("../asset/out_ctor_multi_gb_r.png", Image::Channel::RED);
	img2.saveChannel("../asset/out_ctor_multi_gb_g.bmp", Image::Channel::GREEN);

	Image img3(std::string("../asset/img.png"), Image::Channel::ALPHA, Image::Channel::RED);
	img3.saveChannel("../asset/out_ctor_multi_ar_r.gif", Image::Channel::RED);
	img3.saveChannel("../asset/out_ctor_multi_ar_g.png", Image::Channel::GREEN);


	Image img4(std::string("../asset/img.png"), Image::Channel::ALPHA, Image::Channel::RED, Image::Channel::GREEN);
	img4.saveChannel("../asset/out_ctor_multi_arg_r.gif", Image::Channel::RED);
	img4.saveChannel("../asset/out_ctor_multi_arg_g.bmp", Image::Channel::GREEN);
	img4.saveChannel("../asset/out_ctor_multi_arg_b.tga", Image::Channel::BLUE);

	Image img5(std::string("../asset/img.png"), Image::Channel::BLUE, Image::Channel::RED, Image::Channel::ALPHA);
	img5.saveChannel("../asset/out_ctor_multi_bra_r.gif", Image::Channel::RED);
	img5.saveChannel("../asset/out_ctor_multi_bra_g.png", Image::Channel::GREEN);
	img5.saveChannel("../asset/out_ctor_multi_bra_b.bmp", Image::Channel::BLUE);


	Image img6(std::string("../asset/img.png"), Image::Channel::GREEN, Image::Channel::ALPHA, Image::Channel::RED, Image::Channel::GREEN);
	img6.saveChannel("../asset/out_ctor_multi_garg_r.gif", Image::Channel::RED);
	img6.saveChannel("../asset/out_ctor_multi_garg_g.bmp", Image::Channel::GREEN);
	img6.saveChannel("../asset/out_ctor_multi_garg_b.png", Image::Channel::BLUE);
	img6.saveChannel("../asset/out_ctor_multi_garg_a.tga", Image::Channel::ALPHA);

	Image img7(std::string("../asset/img.png"), Image::Channel::ALPHA, Image::Channel::ALPHA, Image::Channel::BLUE, Image::Channel::RED);
	img7.saveChannel("../asset/out_ctor_multi_aabr_r.gif", Image::Channel::RED);
	img7.saveChannel("../asset/out_ctor_multi_aabr_g.bmp", Image::Channel::GREEN);
	img7.saveChannel("../asset/out_ctor_multi_aabr_b.tga", Image::Channel::BLUE);
	img7.saveChannel("../asset/out_ctor_multi_aabr_a.png", Image::Channel::ALPHA);
}
void test_copyctor_multi_channel(){
	using namespace lap;
	const Image img(std::string("../asset/img.png"));
	Image imgclone(img);
	bool res = imgclone.save("../asset/out_multi_clone.png");
	std::cout << std::boolalpha << res << std::endl;

	Image img1(img, Image::Channel::RED, Image::Channel::GREEN);
	img1.saveChannel("../asset/out_copyctormulti_rg_r.tga", Image::Channel::RED);
	img1.saveChannel("../asset/out_copyctormulti_rg_g.gif", Image::Channel::GREEN);

	Image img2(img, Image::Channel::GREEN, Image::Channel::BLUE);
	img2.saveChannel("../asset/out_copyctormulti_gb_r.png", Image::Channel::RED);
	img2.saveChannel("../asset/out_copyctormulti_gb_g.bmp", Image::Channel::GREEN);

	Image img3(img, Image::Channel::ALPHA, Image::Channel::RED);
	img3.saveChannel("../asset/out_copyctormulti_ar_r.gif", Image::Channel::RED);
	img3.saveChannel("../asset/out_copyctormulti_ar_g.png", Image::Channel::GREEN);


	Image img4(img, Image::Channel::ALPHA, Image::Channel::RED, Image::Channel::GREEN);
	img4.saveChannel("../asset/out_copyctormulti_arg_r.gif", Image::Channel::RED);
	img4.saveChannel("../asset/out_copyctormulti_arg_g.bmp", Image::Channel::GREEN);
	img4.saveChannel("../asset/out_copyctormulti_arg_b.tga", Image::Channel::BLUE);

	Image img5(img, Image::Channel::BLUE, Image::Channel::RED, Image::Channel::ALPHA);
	img5.saveChannel("../asset/out_copyctormulti_bra_r.gif", Image::Channel::RED);
	img5.saveChannel("../asset/out_copyctormulti_bra_g.png", Image::Channel::GREEN);
	img5.saveChannel("../asset/out_copyctormulti_bra_b.bmp", Image::Channel::BLUE);


	Image img6(img, Image::Channel::GREEN, Image::Channel::ALPHA, Image::Channel::RED, Image::Channel::GREEN);
	img6.saveChannel("../asset/out_copyctormulti_garg_r.gif", Image::Channel::RED);
	img6.saveChannel("../asset/out_copyctormulti_garg_g.bmp", Image::Channel::GREEN);
	img6.saveChannel("../asset/out_copyctormulti_garg_b.png", Image::Channel::BLUE);
	img6.saveChannel("../asset/out_copyctormulti_garg_a.tga", Image::Channel::ALPHA);

	Image img7(img, Image::Channel::ALPHA, Image::Channel::ALPHA, Image::Channel::BLUE, Image::Channel::RED);
	img7.saveChannel("../asset/out_copyctormulti_aabr_r.gif", Image::Channel::RED);
	img7.saveChannel("../asset/out_copyctormulti_aabr_g.bmp", Image::Channel::GREEN);
	img7.saveChannel("../asset/out_copyctormulti_aabr_b.tga", Image::Channel::BLUE);
	img7.saveChannel("../asset/out_copyctormulti_aabr_a.png", Image::Channel::ALPHA);
}
void test_memory_multi_channel(){
	using namespace lap;
	Image img(std::string("../asset/img.png"));
	unsigned char* data = img.getData();
	unsigned int w = img.getWidth();
	unsigned int h = img.getHeight();
	unsigned int n = img.getNumChannels();
	unsigned int f = img.getNumFrames();
	unsigned int datasize = img.getDataSize();
	unsigned char* data2 = img.getFrameData(0);
	unsigned int delay = img.getFrameDelay(0);
	float hdrgamma = img.getHDRgamma();
	float hdrscale = img.getHDRscale();
	unsigned int totaldelay = img.getTotalFrameDelay();
	std::cout << data << " " << w << " " << h << " " << n << " " << f << " " << datasize << " " << data2 << " " << delay << " " << hdrgamma << " " << hdrscale << " " << totaldelay << std::endl;

	Image img1(data, w, h, n, Image::Channel::RED, Image::Channel::GREEN);
	img1.saveChannel("../asset/out_multi_rg_r.tga", Image::Channel::RED);
	img1.saveChannel("../asset/out_multi_rg_g.gif", Image::Channel::GREEN);

	Image img2(data, w, h, n, Image::Channel::GREEN, Image::Channel::BLUE);
	img2.saveChannel("../asset/out_multi_gb_r.png", Image::Channel::RED);
	img2.saveChannel("../asset/out_multi_gb_g.bmp", Image::Channel::GREEN);

	Image img3(data, w, h, n, Image::Channel::ALPHA, Image::Channel::RED);
	img3.saveChannel("../asset/out_multi_ar_r.gif", Image::Channel::RED);
	img3.saveChannel("../asset/out_multi_ar_g.png", Image::Channel::GREEN);


	Image img4(data, w, h, n, Image::Channel::ALPHA, Image::Channel::RED, Image::Channel::GREEN);
	img4.saveChannel("../asset/out_multi_arg_r.gif", Image::Channel::RED);
	img4.saveChannel("../asset/out_multi_arg_g.bmp", Image::Channel::GREEN);
	img4.saveChannel("../asset/out_multi_arg_b.tga", Image::Channel::BLUE);

	Image img5(data, w, h, n, Image::Channel::BLUE, Image::Channel::RED, Image::Channel::ALPHA);
	img5.saveChannel("../asset/out_multi_bra_r.gif", Image::Channel::RED);
	img5.saveChannel("../asset/out_multi_bra_g.png", Image::Channel::GREEN);
	img5.saveChannel("../asset/out_multi_bra_b.bmp", Image::Channel::BLUE);


	Image img6(data, w, h, n, Image::Channel::GREEN, Image::Channel::ALPHA, Image::Channel::RED, Image::Channel::GREEN);
	img6.saveChannel("../asset/out_multi_garg_r.gif", Image::Channel::RED);
	img6.saveChannel("../asset/out_multi_garg_g.bmp", Image::Channel::GREEN);
	img6.saveChannel("../asset/out_multi_garg_b.png", Image::Channel::BLUE);
	img6.saveChannel("../asset/out_multi_garg_a.tga", Image::Channel::ALPHA);

	Image img7(data, w, h, n, Image::Channel::ALPHA, Image::Channel::ALPHA, Image::Channel::BLUE, Image::Channel::RED);
	img7.saveChannel("../asset/out_multi_aabr_r.gif", Image::Channel::RED);
	img7.saveChannel("../asset/out_multi_aabr_g.bmp", Image::Channel::GREEN);
	img7.saveChannel("../asset/out_multi_aabr_b.tga", Image::Channel::BLUE);
	img7.saveChannel("../asset/out_multi_aabr_a.png", Image::Channel::ALPHA);
}
void test_memory_multi_channel_multi_frame(){
	using namespace lap;
	const Image img(std::string("../asset/animated.gif"));
	//const Image img(std::string("../asset/animated_transparent.gif"));
	const unsigned char* data = img.getData();
	unsigned int w = img.getWidth();
	unsigned int h = img.getHeight();
	unsigned int n = img.getNumChannels();
	unsigned int f = img.getNumFrames();
	unsigned int datasize = img.getDataSize();
	const unsigned char* data2 = img.getFrameData(0);
	unsigned int delay0 = img.getFrameDelay(0);
	const unsigned int* delays_src = img.getDelays();
	float hdrgamma = img.getHDRgamma();
	float hdrscale = img.getHDRscale();
	unsigned int totaldelay = img.getTotalFrameDelay();
	std::cout<<w<<" "<<h<<" "<<n<<" "<<f<<" "<<datasize<<" "<<data2<<" "<<data<<" "<<delay0<<" "<<delays_src<<" "<<hdrgamma<<" "<<hdrscale<<" "<<totaldelay<<std::endl;

	unsigned char **memory = new unsigned char*[f];
	for (unsigned int i = 0; i < f; i++){
		const unsigned char* src = img.getFrameData(i);
		memory[i] = new unsigned char[w*h*n];
		std::memcpy(memory[i], src, w*h*n);
	}
	unsigned int *times = new unsigned int[f - 1];
	std::memcpy(times, delays_src, sizeof(unsigned int)*(f - 1));
	const unsigned char** mem = const_cast<const unsigned char**>(memory);

	Image img0(mem, times, w, h, n, f, Image::Channel::ALPHA);
	img0.saveChannel("../asset/out_multi_a_r.gif", Image::Channel::RED);
	std::cout << "1/8" << std::endl;

	Image img1(mem, times, w, h, n, f, Image::Channel::RED, Image::Channel::GREEN);
	img1.saveChannel("../asset/out_multi_rg_r.tga", Image::Channel::RED);
	img1.saveChannel("../asset/out_multi_rg_g.gif", Image::Channel::GREEN);
	std::cout << "2/8" << std::endl;
	
	Image img2(mem, times, w, h, n, f, Image::Channel::GREEN, Image::Channel::BLUE);
	img2.saveChannel("../asset/out_multi_gb_r.gif", Image::Channel::RED);
	img2.saveChannel("../asset/out_multi_gb_g.bmp", Image::Channel::GREEN);
	std::cout << "3/8" << std::endl;

	Image img3(mem, times, w, h, n, f, Image::Channel::ALPHA, Image::Channel::RED);
	img3.saveChannel("../asset/out_multi_ar_r.gif", Image::Channel::RED);
	img3.saveChannel("../asset/out_multi_ar_g.png", Image::Channel::GREEN);
	std::cout << "4/8" << std::endl;

	Image img4(mem, times, w, h, n, f, Image::Channel::ALPHA, Image::Channel::RED, Image::Channel::GREEN);
	img4.saveChannel("../asset/out_multi_arg_r.gif", Image::Channel::RED);
	img4.saveChannel("../asset/out_multi_arg_g.bmp", Image::Channel::GREEN);
	img4.saveChannel("../asset/out_multi_arg_b.tga", Image::Channel::BLUE);
	std::cout << "5/8" << std::endl;

	Image img5(mem, times, w, h, n, f, Image::Channel::BLUE, Image::Channel::RED, Image::Channel::ALPHA);
	img5.saveChannel("../asset/out_multi_bra_r.gif", Image::Channel::RED);
	img5.saveChannel("../asset/out_multi_bra_g.png", Image::Channel::GREEN);
	img5.saveChannel("../asset/out_multi_bra_b.bmp", Image::Channel::BLUE);
	std::cout << "6/8" << std::endl;

	Image img6(mem, times, w, h, n, f, Image::Channel::GREEN, Image::Channel::ALPHA, Image::Channel::RED, Image::Channel::GREEN);
	img6.saveChannel("../asset/out_multi_garg_r.gif", Image::Channel::RED);
	img6.saveChannel("../asset/out_multi_garg_g.bmp", Image::Channel::GREEN);
	img6.saveChannel("../asset/out_multi_garg_b.png", Image::Channel::BLUE);
	img6.saveChannel("../asset/out_multi_garg_a.tga", Image::Channel::ALPHA);
	std::cout << "7/8" << std::endl;

	Image img7(mem, times, w, h, n, f, Image::Channel::ALPHA, Image::Channel::ALPHA, Image::Channel::BLUE, Image::Channel::RED);
	img7.saveChannel("../asset/out_multi_aabr_r.gif", Image::Channel::RED);
	img7.saveChannel("../asset/out_multi_aabr_g.bmp", Image::Channel::GREEN);
	img7.saveChannel("../asset/out_multi_aabr_b.tga", Image::Channel::BLUE);
	img7.saveChannel("../asset/out_multi_aabr_a.png", Image::Channel::ALPHA);
	std::cout << "8/8" << std::endl;
	
	for (unsigned int i = 0; i < f; i++) delete[] memory[i];
	delete[] memory;
	delete[] times;
}

void test_memory_multi_channel_multi_frame_vec(){
	using namespace lap;
	const Image img(std::string("../asset/animated.gif"));
	
	const std::vector<unsigned char>& data = img.getDataVec();
	unsigned int w = img.getWidth();
	unsigned int h = img.getHeight();
	unsigned int n = img.getNumChannels();
	unsigned int f = img.getNumFrames();
	unsigned int datasize = img.getDataSize();
	const std::vector<unsigned char>& data2 = img.getFrameDataVec(0);
	unsigned int delay0 = img.getFrameDelay(0);
	const std::vector<unsigned int>& delays_src = img.getDelaysVec();
	float hdrgamma = img.getHDRgamma();
	float hdrscale = img.getHDRscale();
	unsigned int totaldelay = img.getTotalFrameDelay();
	std::cout << w << " " << h << " " << n << " " << f << " " << datasize << " " << data2[0] << " " << data[0] << " " << delay0 << " " << delays_src[0] << " " << hdrgamma << " " << hdrscale << " " << totaldelay << std::endl;

	std::vector<std::vector<unsigned char>> memory(f);
	for (unsigned int i = 0; i < f;i++){
		const std::vector<unsigned char>& src = img.getFrameDataVec(i);
		memory[i] = src;
	}
	std::vector<unsigned int> times(f - 1);
	times = delays_src;
	const std::vector<std::vector<unsigned char>>& mem = memory;

	Image img0(mem, times, w, h, n, f, Image::Channel::ALPHA);
	img0.saveChannel("../asset/out_multi_a_r.gif", Image::Channel::RED);
	std::cout << "1/8" << std::endl;

	Image img1(mem, times, w, h, n, f, Image::Channel::RED, Image::Channel::GREEN);
	img1.saveChannel("../asset/out_multi_rg_r.tga", Image::Channel::RED);
	img1.saveChannel("../asset/out_multi_rg_g.gif", Image::Channel::GREEN);
	std::cout << "2/8" << std::endl;

	Image img2(mem, times, w, h, n, f, Image::Channel::GREEN, Image::Channel::BLUE);
	img2.saveChannel("../asset/out_multi_gb_r.gif", Image::Channel::RED);
	img2.saveChannel("../asset/out_multi_gb_g.bmp", Image::Channel::GREEN);
	std::cout << "3/8" << std::endl;

	Image img3(mem, times, w, h, n, f, Image::Channel::ALPHA, Image::Channel::RED);
	img3.saveChannel("../asset/out_multi_ar_r.gif", Image::Channel::RED);
	img3.saveChannel("../asset/out_multi_ar_g.png", Image::Channel::GREEN);
	std::cout << "4/8" << std::endl;

	Image img4(mem, times, w, h, n, f, Image::Channel::ALPHA, Image::Channel::RED, Image::Channel::GREEN);
	img4.saveChannel("../asset/out_multi_arg_r.gif", Image::Channel::RED);
	img4.saveChannel("../asset/out_multi_arg_g.bmp", Image::Channel::GREEN);
	img4.saveChannel("../asset/out_multi_arg_b.tga", Image::Channel::BLUE);
	std::cout << "5/8" << std::endl;

	Image img5(mem, times, w, h, n, f, Image::Channel::BLUE, Image::Channel::RED, Image::Channel::ALPHA);
	img5.saveChannel("../asset/out_multi_bra_r.gif", Image::Channel::RED);
	img5.saveChannel("../asset/out_multi_bra_g.png", Image::Channel::GREEN);
	img5.saveChannel("../asset/out_multi_bra_b.bmp", Image::Channel::BLUE);
	std::cout << "6/8" << std::endl;

	Image img6(mem, times, w, h, n, f, Image::Channel::GREEN, Image::Channel::ALPHA, Image::Channel::RED, Image::Channel::GREEN);
	img6.saveChannel("../asset/out_multi_garg_r.gif", Image::Channel::RED);
	img6.saveChannel("../asset/out_multi_garg_g.bmp", Image::Channel::GREEN);
	img6.saveChannel("../asset/out_multi_garg_b.png", Image::Channel::BLUE);
	img6.saveChannel("../asset/out_multi_garg_a.tga", Image::Channel::ALPHA);
	std::cout << "7/8" << std::endl;

	Image img7(mem, times, w, h, n, f, Image::Channel::ALPHA, Image::Channel::ALPHA, Image::Channel::BLUE, Image::Channel::RED);
	img7.saveChannel("../asset/out_multi_aabr_r.gif", Image::Channel::RED);
	img7.saveChannel("../asset/out_multi_aabr_g.bmp", Image::Channel::GREEN);
	img7.saveChannel("../asset/out_multi_aabr_b.tga", Image::Channel::BLUE);
	img7.saveChannel("../asset/out_multi_aabr_a.png", Image::Channel::ALPHA);
	std::cout << "8/8" << std::endl;



}

void test_assert(){
	//various types of assert crashes
	using namespace lap;
	//Image img1(std::string("../asset/img.pgm"), Image::Channel::RED, Image::Channel::GREEN, Image::Channel::ALPHA, Image::Channel::BLUE);
	//Image img2(std::string("../asset/img.pgm"), Image::Channel::RED, Image::Channel::GREEN, Image::Channel::ALPHA);
	//Image img3(std::string("../asset/img.pgm"), Image::Channel::RED, Image::Channel::ALPHA);
	//Image img4(std::string("../asset/img.pgm"), Image::Channel::ALPHA);
}

void test_resize(){
	using namespace lap;
	{	
		Image img(std::string("../asset/img.bmp"));
		img.resize(1200, 700);
		img.save("../asset/out_resize_big.png");
		img.resize(200, 100);
		img.save("../asset/out_resize_small.png");
		Image img2(std::string("../asset/animated.gif"));
		img2.resize(1200, 700);
		img2.save("../asset/out_resize_animated_big.gif");
		img2.resize(200, 100);
		img2.save("../asset/out_resize_animated_small.gif");
	}
	{
		lap::Image img(std::string("../asset/img.bmp"));
		lap::Image img2(std::string("../asset/animated.gif"));
		{ lap::Image i(img); i.transformGrayscale(); i.save("../asset/out_resize_grayscale.png"); }
		{ lap::Image i(img2); i.transformGrayscale(); i.save("../asset/out_resize_grayscaleanim.gif"); }
		{ lap::Image i(img); i.resize(420, 300); i.save("../asset/out_resize_small_3.png"); }
		{ lap::Image i(img); i.resize(1200, 800); i.save("../asset/out_resize_large_3.png"); }
		{ lap::Image i(img2); i.resize(420, 300); i.save("../asset/out_resizeanim_small_3.gif"); }
		{ lap::Image i(img2); i.resize(1200, 800); i.save("../asset/out_resizeanim_large_3.gif"); }
		lap::Image img4(std::string("../asset/img.png"));
		lap::Image img42(std::string("../asset/animated_transparent.gif"));
		{ lap::Image i(img4); i.resize(420, 300); i.save("../asset/out_resize_small_4.png"); }
		{ lap::Image i(img4); i.resize(1200, 800); i.save("../asset/out_resize_large_4.png"); }
		{ lap::Image i(img42);	i.resize(420, 300); i.save("../asset/out_resizeanim_small_4.gif"); }
		{ lap::Image i(img42); i.resize(1200, 800); i.save("../asset/out_resizeanim_large_4.gif"); }
	}
}

void test_access(){
	lap::Image img(std::string("../asset/img.bmp"));
	const lap::Image cimg(std::string("../asset/img.bmp"));
	int w = img.getWidth();
	int h = img.getHeight();
	int n = img.getNumChannels();
	img.clearData();
	img.save("../asset/out_acces_empty.png");
	for (int i = 0; i < w*h; i++) for (int c = 0; c < n; c++) if (c < 3) img(i*n + c) = 255 - cimg(i*n + c); else img(i*n + c) = cimg(i*n + c);
	img.save("../asset/out_access_inverted_c.png");

	for (int j = 0; j < h; j++) for (int i = 0; i<w; i++) for (int c = 0; c < n; c++) img(i, j, c) = cimg(i, h - 1 - j, c); 
	img.save("../asset/out_access_inverted_y.png");
	for (int j = 0; j < h; j++) for (int i = 0; i<w; i++) for (int c = 0; c < n; c++) img(i, j, c, 0) = cimg(w-1-i, h - 1 - j, c, 0);
	img.save("../asset/out_access_inverted_xy.png");

	lap::Image imgg(std::string("../asset/animated.gif"));
	const lap::Image cimgg(std::string("../asset/animated.gif"));
	w = imgg.getWidth();
	h = imgg.getHeight();
	n = imgg.getNumChannels();
	imgg.clearData();
	int frames = imgg.getNumFrames();
	for (int f = 0; f < frames; f++) for (int j = 0; j < h; j++) for (int i = 0; i < w; i++) for (int c = 0; c < n; c++){
		imgg(i, j, c, f) = cimgg(w - 1 - i, j, c, f);
	}
	for (int f = 0; f < frames; f++) imgg.setFrameDelay(f, cimgg.getFrameDelay(f));
	imgg.save("../asset/out_access_animated_inv_x.gif");
}
void test_frames(){
	const lap::Image img(std::string("../asset/animated.gif"));
	lap::Image img2(img);
	img2.framesRemove();
	
	for (unsigned int i = 0; i < img.getNumFrames(); i++) img2.frameAdd(img.getFrameData(i), img.getFrameDelay(i));
	img2.save("../asset/out_frames_adds.gif");
	for (unsigned int i = 0; i < img.getNumFrames(); i++) img2.frameRemove(0);
	img2.save("../asset/out_frames_zero1.gif");	//ok

	//effective revers
	img2.frameAdd(img.getFrameData(0), img.getFrameDelay(0));
	for (unsigned int i = 1; i < img.getNumFrames(); i++) img2.frameAdd(img.getFrameData(i), img.getFrameDelay(i), 0);
	img2.save("../asset/out_frames_reversebyinsert.gif");
	img2.framesRemove();
	img2.save("../asset/out_frames_reversereverse.gif");
	img2.framesRemove();
	img2.save("../asset/out_frames_zero1.gif");

	lap::Image img1frame(std::string("../asset/img.bmp"));
	img1frame.framesReverse();
	img1frame.save("../asset/out_frames_single.png");
}
void test_switch_channel(){
	lap::Image img(std::string("../asset/animated.gif"));
	img.channelSwitch(lap::Image::Channel::RED, lap::Image::Channel::BLUE);
	img.save("../asset/out_switch_channels.gif");

	lap::Image img2(std::string("../asset/img.jpg"));
	img2.channelSwitch(lap::Image::Channel::GREEN, lap::Image::Channel::BLUE);
	img2.save("../asset/out_switch_channels.tga");
}
void test_save_advanced(){
	{
		lap::Image img(std::string("../asset/img.png"));
		lap::Image imgR(img, lap::Image::Channel::RED);
		imgR.saveChannel("../asset/out_save_allnoa_r.png", lap::Image::Channel::ALLNOA);
		imgR.saveChannel("../asset/out_save_allnoa_r.gif", lap::Image::Channel::ALLNOA);
		imgR.saveChannel("../asset/out_save_allnoa_r.tga", lap::Image::Channel::ALLNOA);
		imgR.saveChannel("../asset/out_save_allnoa_r.bmp", lap::Image::Channel::ALLNOA);
		imgR.save("../asset/out_save_allnoa_cr.bmp");
		imgR.save("../asset/out_save_allnoa_cr.gif");
		imgR.save("../asset/out_save_allnoa_cr.tga");
		imgR.save("../asset/out_save_allnoa_cr.png");
		lap::Image imgRG(img, lap::Image::Channel::RED, lap::Image::Channel::GREEN);
		imgRG.saveChannel("../asset/out_save_allnoa_rg.tga", lap::Image::Channel::ALLNOA);
		imgRG.saveChannel("../asset/out_save_allnoa_rg.gif", lap::Image::Channel::ALLNOA);
		imgRG.saveChannel("../asset/out_save_allnoa_rg.bmp", lap::Image::Channel::ALLNOA);
		imgRG.saveChannel("../asset/out_save_allnoa_rg.png", lap::Image::Channel::ALLNOA);
		imgRG.save("../asset/out_save_allnoa_crg.bmp");
		imgRG.save("../asset/out_save_allnoa_crg.gif");
		imgRG.save("../asset/out_save_allnoa_crg.tga");
		imgRG.save("../asset/out_save_allnoa_crg.png");
		lap::Image imgRGB(img, lap::Image::Channel::RED, lap::Image::Channel::GREEN, lap::Image::Channel::BLUE);
		imgRGB.saveChannel("../asset/out_save_allnoa_rgb.png", lap::Image::Channel::ALLNOA);
		imgRGB.saveChannel("../asset/out_save_allnoa_rgb.gif", lap::Image::Channel::ALLNOA);
		imgRGB.saveChannel("../asset/out_save_allnoa_rgb.tga", lap::Image::Channel::ALLNOA);
		imgRGB.saveChannel("../asset/out_save_allnoa_rgb.bmp", lap::Image::Channel::ALLNOA);
		imgRGB.save("../asset/out_save_allnoa_crgb.bmp");
		imgRGB.save("../asset/out_save_allnoa_crgb.gif");
		imgRGB.save("../asset/out_save_allnoa_crgb.tga");
		imgRGB.save("../asset/out_save_allnoa_crgb.png");
		lap::Image imgRGBA(img, lap::Image::Channel::RED, lap::Image::Channel::GREEN, lap::Image::Channel::BLUE, lap::Image::Channel::ALPHA);
		imgRGBA.saveChannel("../asset/out_save_allnoa_rgba.png", lap::Image::Channel::ALLNOA);
		imgRGBA.saveChannel("../asset/out_save_allnoa_rgba.gif", lap::Image::Channel::ALLNOA);
		imgRGBA.saveChannel("../asset/out_save_allnoa_rgba.tga", lap::Image::Channel::ALLNOA);
		imgRGBA.saveChannel("../asset/out_save_allnoa_rgba.bmp", lap::Image::Channel::ALLNOA);
		imgRGBA.save("../asset/out_save_allnoa_crgba.bmp");
		imgRGBA.save("../asset/out_save_allnoa_crgba.gif");
		imgRGBA.save("../asset/out_save_allnoa_crgba.tga");
		imgRGBA.save("../asset/out_save_allnoa_crgba.png");
	}
	{
		lap::Image img(std::string("../asset/animated.gif"));
		lap::Image imgR(img, lap::Image::Channel::RED);
		imgR.saveFrameChannel("../asset/out_fsave_allnoa_r.png", lap::Image::Channel::ALLNOA, 0);
		imgR.saveFrameChannel("../asset/out_fsave_allnoa_r.gif", lap::Image::Channel::ALLNOA, 0);
		imgR.saveFrameChannel("../asset/out_fsave_allnoa_r.tga", lap::Image::Channel::ALLNOA, 0);
		imgR.saveFrameChannel("../asset/out_fsave_allnoa_r.bmp", lap::Image::Channel::ALLNOA, 0);
		imgR.saveFrame("../asset/out_fsave_allnoa_cr.bmp", 0);
		imgR.saveFrame("../asset/out_fsave_allnoa_cr.gif", 0);
		imgR.saveFrame("../asset/out_fsave_allnoa_cr.tga", 0);
		imgR.saveFrame("../asset/out_fsave_allnoa_cr.png", 0);
		lap::Image imgRG(img, lap::Image::Channel::RED, lap::Image::Channel::GREEN);
		imgRG.saveFrameChannel("../asset/out_fsave_allnoa_rg.tga", lap::Image::Channel::ALLNOA, 0);
		imgRG.saveFrameChannel("../asset/out_fsave_allnoa_rg.gif", lap::Image::Channel::ALLNOA, 0);
		imgRG.saveFrameChannel("../asset/out_fsave_allnoa_rg.bmp", lap::Image::Channel::ALLNOA, 0);
		imgRG.saveFrameChannel("../asset/out_fsave_allnoa_rg.png", lap::Image::Channel::ALLNOA, 0);
		imgRG.saveFrame("../asset/out_fsave_allnoa_crg.bmp", 0);
		imgRG.saveFrame("../asset/out_fsave_allnoa_crg.gif", 0);
		imgRG.saveFrame("../asset/out_fsave_allnoa_crg.tga", 0);
		imgRG.saveFrame("../asset/out_fsave_allnoa_crg.png", 0);
		lap::Image imgRGB(img, lap::Image::Channel::RED, lap::Image::Channel::GREEN, lap::Image::Channel::BLUE);
		imgRGB.saveFrameChannel("../asset/out_fsave_allnoa_rgb.png", lap::Image::Channel::ALLNOA, 0);
		imgRGB.saveFrameChannel("../asset/out_fsave_allnoa_rgb.gif", lap::Image::Channel::ALLNOA, 0);
		imgRGB.saveFrameChannel("../asset/out_fsave_allnoa_rgb.tga", lap::Image::Channel::ALLNOA, 0);
		imgRGB.saveFrameChannel("../asset/out_fsave_allnoa_rgb.bmp", lap::Image::Channel::ALLNOA, 0);
		imgRGB.saveFrame("../asset/out_fsave_allnoa_crgb.bmp", 0);
		imgRGB.saveFrame("../asset/out_fsave_allnoa_crgb.gif", 0);
		imgRGB.saveFrame("../asset/out_fsave_allnoa_crgb.tga", 0);
		imgRGB.saveFrame("../asset/out_fsave_allnoa_crgb.png", 0);
		lap::Image imgRGBA(img, lap::Image::Channel::RED, lap::Image::Channel::GREEN, lap::Image::Channel::BLUE, lap::Image::Channel::ALPHA);
		imgRGBA.saveFrameChannel("../asset/out_fsave_allnoa_rgba.png", lap::Image::Channel::ALLNOA, 0);
		imgRGBA.saveFrameChannel("../asset/out_fsave_allnoa_rgba.gif", lap::Image::Channel::ALLNOA, 0);
		imgRGBA.saveFrameChannel("../asset/out_fsave_allnoa_rgba.tga", lap::Image::Channel::ALLNOA, 0);
		imgRGBA.saveFrameChannel("../asset/out_fsave_allnoa_rgba.bmp", lap::Image::Channel::ALLNOA, 0);
		imgRGBA.saveFrame("../asset/out_fsave_allnoa_crgba.bmp", 0);
		imgRGBA.saveFrame("../asset/out_fsave_allnoa_crgba.gif", 0);
		imgRGBA.saveFrame("../asset/out_fsave_allnoa_crgba.tga", 0);
		imgRGBA.saveFrame("../asset/out_fsave_allnoa_crgba.png", 0);
	}
	{
		lap::Image img(std::string("../asset/animated.gif"));
		for (unsigned int i = 0; i < img.getNumFrames(); i++){
			img.saveFrame(std::string("../asset/out_zsave_") + std::to_string(i) + std::string(".png"), i);
			img.saveFrameChannel(std::string("../asset/out_zsave_r_") + std::to_string(i) + std::string(".png"), lap::Image::Channel::RED, i);
			img.saveFrameChannel(std::string("../asset/out_zsave_g_") + std::to_string(i) + std::string(".png"), lap::Image::Channel::GREEN, i);
			img.saveFrameChannel(std::string("../asset/out_zsave_b_") + std::to_string(i) + std::string(".png"), lap::Image::Channel::BLUE, i);
			img.saveFrameChannel(std::string("../asset/out_zsave_a_") + std::to_string(i) + std::string(".png"), lap::Image::Channel::ALPHA, i);
		}
	}
}
void test_transform(){
	{
		lap::Image img(std::string("../asset/img.png"));
		lap::Image img2(img), imgg(img);

		imgg.transformFlipX();
		imgg.save("../asset/out_transform_flipx.png");
		imgg.transformFlipX();
		imgg.transformFlipY();
		imgg.save("../asset/out_transform_flipy.png");
		imgg.transformFlipY();
		imgg.transformFlipXY();
		imgg.save("../asset/out_transform_flipxy.png");
		imgg.transformFlipXY();
		imgg.transformGrayscale();
		imgg.save("../asset/out_transform_grayscale_rgba.png");
		lap::Image imgr(img, lap::Image::Channel::RED); imgr.save("../asset/out_transform_grayscale_r.png");
		lap::Image imgrg(img, lap::Image::Channel::RED, lap::Image::Channel::GREEN); imgrg.save("../asset/out_transform_grayscale_rg.png");
		lap::Image imgrgb(img, lap::Image::Channel::RED, lap::Image::Channel::GREEN, lap::Image::Channel::BLUE); imgrgb.save("../asset/out_transform_grayscale_rgb.png");

		img2.channelInvert(lap::Image::Channel::RED);
		img2.save("../asset/out_transform_invert_r.png");
		img2.channelInvert(lap::Image::Channel::RED);
		img2.channelInvert(lap::Image::Channel::GREEN);
		img2.save("../asset/out_transform_invert_g.png");
		img2.channelInvert(lap::Image::Channel::GREEN);
		img2.channelInvert(lap::Image::Channel::BLUE);
		img2.save("../asset/out_transform_invert_b.png");
		img2.channelInvert(lap::Image::Channel::BLUE);
		img2.channelInvert(lap::Image::Channel::ALPHA);
		img2.save("../asset/out_transform_invert_a.png");
		img2.channelInvert(lap::Image::Channel::ALPHA);
		img2.save("../asset/out_transform_invert_normal.png");
		img2.channelInvert(lap::Image::Channel::ALL);
		img2.save("../asset/out_transform_invert_all.png");
		img2.channelInvert(lap::Image::Channel::ALL);
		img2.channelInvert(lap::Image::Channel::ALLNOA);
		img2.save("../asset/out_transform_invert_allnoa.png");

		{
			lap::Image img3(img); img3.channelWhite(lap::Image::Channel::RED);		img3.save("../asset/out_transform_white_r.png");
			lap::Image img4(img); img4.channelWhite(lap::Image::Channel::GREEN);	img4.save("../asset/out_transform_white_g.png");
			lap::Image img5(img); img5.channelWhite(lap::Image::Channel::BLUE);		img5.save("../asset/out_transform_white_b.png");
			lap::Image img6(img); img6.channelWhite(lap::Image::Channel::ALPHA);	img6.save("../asset/out_transform_white_a.png");
			lap::Image img7(img); img7.channelWhite(lap::Image::Channel::ALL);		img7.save("../asset/out_transform_white_all.png");
			lap::Image img8(img); img8.channelWhite(lap::Image::Channel::ALLNOA);	img8.save("../asset/out_transform_white_allnoa.png");
		}

		{
			lap::Image img7(img); img7.channelBlack(lap::Image::Channel::RED);		img7.save("../asset/out_transform_black_r.png");
			lap::Image img8(img); img8.channelBlack(lap::Image::Channel::GREEN);	img8.save("../asset/out_transform_black_g.png");
			lap::Image img9(img); img9.channelBlack(lap::Image::Channel::BLUE);		img9.save("../asset/out_transform_black_b.png");
			lap::Image img10(img); img10.channelBlack(lap::Image::Channel::ALPHA);	img10.save("../asset/out_transform_black_a.png");
			lap::Image img11(img); img11.channelBlack(lap::Image::Channel::ALL);	img11.save("../asset/out_transform_black_all.png");
			lap::Image img12(img); img12.channelBlack(lap::Image::Channel::ALLNOA);	img12.save("../asset/out_transform_black_allnoa.png");
		}
		{
			lap::Image img11(img); img11.channelMin(lap::Image::Channel::RED);		img11.save("../asset/out_transform_min_r.png");
			lap::Image img12(img); img12.channelMin(lap::Image::Channel::GREEN);	img12.save("../asset/out_transform_min_g.png");
			lap::Image img13(img); img13.channelMin(lap::Image::Channel::BLUE);		img13.save("../asset/out_transform_min_b.png");
			lap::Image img14(img); img14.channelMin(lap::Image::Channel::ALPHA);	img14.save("../asset/out_transform_min_a.png");
			lap::Image img15(img); img15.channelMin(lap::Image::Channel::ALL);		img15.save("../asset/out_transform_min_all.png");
			lap::Image img16(img); img16.channelMin(lap::Image::Channel::ALLNOA);	img16.save("../asset/out_transform_min_allnoa.png");
		}
		{
			lap::Image img15(img); img15.channelMax(lap::Image::Channel::RED);		img15.save("../asset/out_transform_max_r.png");
			lap::Image img16(img); img16.channelMax(lap::Image::Channel::GREEN);	img16.save("../asset/out_transform_max_g.png");
			lap::Image img17(img); img17.channelMax(lap::Image::Channel::BLUE);		img17.save("../asset/out_transform_max_b.png");
			lap::Image img18(img); img18.channelMax(lap::Image::Channel::ALPHA);	img18.save("../asset/out_transform_max_a.png");
			lap::Image img19(img); img19.channelMax(lap::Image::Channel::ALL);		img19.save("../asset/out_transform_max_all.png");
			lap::Image img20(img); img20.channelMax(lap::Image::Channel::ALLNOA);	img20.save("../asset/out_transform_max_allnoa.png");
		}
	}

	//animated
	lap::Image aimg(std::string("../asset/animated.gif"));
	lap::Image aimg2(aimg), aimgg(aimg);

	aimgg.transformFlipX();
	aimgg.save("../asset/out_transforma_flipx.gif");
	aimgg.transformFlipX();
	aimgg.transformFlipY();
	aimgg.save("../asset/out_transforma_flipy.gif");
	aimgg.transformFlipY();
	aimgg.transformFlipXY();
	aimgg.save("../asset/out_transforma_flipxy.gif");
	aimgg.transformFlipXY();
	aimgg.transformGrayscale();
	aimgg.save("../asset/out_transforma_grayscale_rgba.gif");
	lap::Image aimgr(aimg, lap::Image::Channel::RED); aimgr.save("../asset/out_transforma_grayscale_r.gif");
	lap::Image aimgrg(aimg, lap::Image::Channel::RED, lap::Image::Channel::GREEN); aimgrg.save("../asset/out_transforma_grayscale_rg.gif");
	lap::Image aimgrgb(aimg, lap::Image::Channel::RED, lap::Image::Channel::GREEN, lap::Image::Channel::BLUE); aimgrgb.save("../asset/out_transforma_grayscale_rgb.gif");

	aimg2.channelInvert(lap::Image::Channel::RED);
	aimg2.save("../asset/out_transforma_invert_r.gif");
	aimg2.channelInvert(lap::Image::Channel::RED);
	aimg2.channelInvert(lap::Image::Channel::GREEN);
	aimg2.save("../asset/out_transforma_invert_g.gif");
	aimg2.channelInvert(lap::Image::Channel::GREEN);
	aimg2.channelInvert(lap::Image::Channel::BLUE);
	aimg2.save("../asset/out_transforma_invert_b.gif");
	aimg2.channelInvert(lap::Image::Channel::BLUE);
	aimg2.channelInvert(lap::Image::Channel::ALPHA);
	aimg2.save("../asset/out_transforma_invert_a.gif");
	aimg2.channelInvert(lap::Image::Channel::ALPHA);
	aimg2.save("../asset/out_transforma_invert_normal.gif");
	aimg2.channelInvert(lap::Image::Channel::ALL);
	aimg2.save("../asset/out_transforma_invert_all.gif");
	aimg2.channelInvert(lap::Image::Channel::ALL);
	aimg2.channelInvert(lap::Image::Channel::ALLNOA);
	aimg2.save("../asset/out_transforma_invert_allnoa.gif");

	{
		lap::Image aimg3(aimg); aimg3.channelWhite(lap::Image::Channel::RED);		aimg3.save("../asset/out_transforma_white_r.gif");
		lap::Image aimg4(aimg); aimg4.channelWhite(lap::Image::Channel::GREEN);		aimg4.save("../asset/out_transforma_white_g.gif");
		lap::Image aimg5(aimg); aimg5.channelWhite(lap::Image::Channel::BLUE);		aimg5.save("../asset/out_transforma_white_b.gif");
		lap::Image aimg6(aimg); aimg6.channelWhite(lap::Image::Channel::ALPHA);		aimg6.save("../asset/out_transforma_white_a.gif");
		lap::Image aimg7(aimg); aimg7.channelWhite(lap::Image::Channel::ALL);		aimg7.save("../asset/out_transforma_white_all.gif");
		lap::Image aimg8(aimg); aimg8.channelWhite(lap::Image::Channel::ALLNOA);	aimg8.save("../asset/out_transforma_white_allnoa.gif");
	}

	{
		lap::Image aimg7(aimg); aimg7.channelBlack(lap::Image::Channel::RED);		aimg7.save("../asset/out_transforma_black_r.gif");
		lap::Image aimg8(aimg); aimg8.channelBlack(lap::Image::Channel::GREEN);		aimg8.save("../asset/out_transforma_black_g.gif");
		lap::Image aimg9(aimg); aimg9.channelBlack(lap::Image::Channel::BLUE);		aimg9.save("../asset/out_transforma_black_b.gif");
		lap::Image aimg10(aimg); aimg10.channelBlack(lap::Image::Channel::ALPHA);	aimg10.save("../asset/out_transforma_black_a.gif");
		lap::Image aimg11(aimg); aimg11.channelBlack(lap::Image::Channel::ALL);		aimg11.save("../asset/out_transforma_black_all.gif");
		lap::Image aimg12(aimg); aimg12.channelBlack(lap::Image::Channel::ALLNOA);	aimg12.save("../asset/out_transforma_black_allnoa.gif");
	}
	{
		lap::Image aimg11(aimg); aimg11.channelMin(lap::Image::Channel::RED);		aimg11.save("../asset/out_transforma_min_r.gif");
		lap::Image aimg12(aimg); aimg12.channelMin(lap::Image::Channel::GREEN);		aimg12.save("../asset/out_transforma_min_g.gif");
		lap::Image aimg13(aimg); aimg13.channelMin(lap::Image::Channel::BLUE);		aimg13.save("../asset/out_transforma_min_b.gif");
		lap::Image aimg14(aimg); aimg14.channelMin(lap::Image::Channel::ALPHA);		aimg14.save("../asset/out_transforma_min_a.gif");
		lap::Image aimg15(aimg); aimg15.channelMin(lap::Image::Channel::ALL);		aimg15.save("../asset/out_transforma_min_all.gif");
		lap::Image aimg16(aimg); aimg16.channelMin(lap::Image::Channel::ALLNOA);	aimg16.save("../asset/out_transforma_min_allnoa.gif");
	}
	{
		lap::Image aimg15(aimg); aimg15.channelMax(lap::Image::Channel::RED);		aimg15.save("../asset/out_transforma_max_r.gif");
		lap::Image aimg16(aimg); aimg16.channelMax(lap::Image::Channel::GREEN);		aimg16.save("../asset/out_transforma_max_g.gif");
		lap::Image aimg17(aimg); aimg17.channelMax(lap::Image::Channel::BLUE);		aimg17.save("../asset/out_transforma_max_b.gif");
		lap::Image aimg18(aimg); aimg18.channelMax(lap::Image::Channel::ALPHA);		aimg18.save("../asset/out_transforma_max_a.gif");
		lap::Image aimg19(aimg); aimg19.channelMax(lap::Image::Channel::ALL);		aimg19.save("../asset/out_transforma_max_all.gif");
		lap::Image aimg20(aimg); aimg20.channelMax(lap::Image::Channel::ALLNOA);	aimg20.save("../asset/out_transforma_max_allnoa.gif");
	}

	{
		bool ret;
		lap::Image bimg("../asset/img2.tga", lap::Image::ColorSpace::SRGB, &ret); assert(ret);
		lap::Image bimgai("../asset/animated.gif", lap::Image::ColorSpace::SRGB, &ret); assert(ret);
		{lap::Image i(bimg); i.transformRGBtoBGR(); i.save("../asset/out_transform2_rgb_to_bgr.png"); }
		{lap::Image i(bimg); i.transformRGBtoBRG(); i.save("../asset/out_transform2_rgb_to_brg.png"); }
		{lap::Image i(bimg); i.transformRGBtoGBR(); i.save("../asset/out_transform2_rgb_to_gbr.png"); }
		{lap::Image i(bimg); i.transformRGBtoGRB(); i.save("../asset/out_transform2_rgb_to_grb.png"); }
		{lap::Image i(bimg); i.transformRGBtoRBG(); i.save("../asset/out_transform2_rgb_to_rbg.png"); }
		{lap::Image i(bimgai); i.transformRGBtoBGR(); i.save("../asset/out_transform2ai_rgb_to_bgr.png"); }
		{lap::Image i(bimgai); i.transformRGBtoBRG(); i.save("../asset/out_transform2ai_rgb_to_brg.png"); }
		{lap::Image i(bimgai); i.transformRGBtoGBR(); i.save("../asset/out_transform2ai_rgb_to_gbr.png"); }
		{lap::Image i(bimgai); i.transformRGBtoGRB(); i.save("../asset/out_transform2ai_rgb_to_grb.png"); }
		{lap::Image i(bimgai); i.transformRGBtoRBG(); i.save("../asset/out_transform2ai_rgb_to_rbg.png"); }
	}
}

void test_color(){
	lap::Image img(std::string("../asset/img.bmp"));
	lap::Image img2(std::string("../asset/animated.gif"));

	{ lap::Image i(img); i.transformToRGB(); i.transformGrayscale(); i.save("../asset/out_color_grayscale_nosrgb.png"); }
	{ lap::Image i(img); i.transformGrayscale(); i.save("../asset/out_color_grayscale_srgb.png"); }
	{ lap::Image i(img2); i.transformToRGB(); i.transformGrayscale(); i.save("../asset/out_color_grayscale_nosrgb.gif"); }
	{ lap::Image i(img2); i.transformGrayscale(); i.save("../asset/out_color_grayscale_srgb.gif"); }
	{ lap::Image i(img); i.transformToRGB(); i.save("../asset/out_color_rgb.png"); }
	{ lap::Image i(img); i.transformToRGB(); i.transformToSRGB();  i.save("../asset/out_color_normal.png"); }
	{ lap::Image i(img); i.transformToRGB(); lap::Image i2(i);  i2.transformToSRGB();  i2.save("../asset/out_color_normal2.png"); }
	{ lap::Image i(img); i.transformToRGB(); i.transformGrayscale();  i.save("../asset/out_color_grayscale_rgb.png"); }
	{ lap::Image i(img); i.transformToRGB(); i.transformToSRGB(); i.transformGrayscale(); i.save("../asset/out_color_grayscale_normal.png"); }

	lap::Image img3(img2);				img3.save("../asset/out_color_ops_cctor.gif");
	lap::Image img4 = img2;				img4.save("../asset/out_color_ops_assigned.gif");
	lap::Image img5 = std::move(img2);	img5.save("../asset/out_color_ops_assigned_moved.gif");
	lap::Image img6(std::move(img4));	img6.save("../asset/out_color_ops_mctor.gif");

}

void test_v1p1(){
	lap::Image imgg(std::string("../asset/img.bmp"));
	//unsigned int size = imgg.getMemory();

	lap::Image gif1(std::string("../asset/animated.gif"));
	gif1.framesReverse();
	gif1.save("../asset/out_v1p1_reversebasic.gif");
	gif1.framesReverse();
	gif1.framesSetSpeed(200);
	gif1.save("../asset/out_v1p1_speedup_200.gif");
	gif1.framesSetSpeed(50);
	gif1.save("../asset/out_v1p1_speedup_original.gif");
	gif1.framesSetSpeed(50);
	gif1.save("../asset/out_v1p1_speedup_50%.gif");
	gif1.framesSetSpeed(-400);
	gif1.save("../asset/out_v1p1_speedup_200minus.gif");
	gif1.framesSetSpeed(-12);
	gif1.save("../asset/out_v1p1_speedup_25.gif");
	
	//remove
	lap::Image imgg22 = imgg;
	imgg22.channelRemove(lap::Image::Channel::RED);
	imgg22.save("../asset/out_v1p1_remove_r.png");
	lap::Image imgg23 = imgg;
	imgg23.channelRemove(lap::Image::Channel::GREEN);
	imgg23.save("../asset/out_v1p1_remove_g.png");
	lap::Image imgg24 = imgg;
	imgg24.channelRemove(lap::Image::Channel::BLUE);
	imgg24.save("../asset/out_v1p1_remove_b.png");
	lap::Image imgg25 = imgg;
	imgg25.channelRemove(lap::Image::Channel::ALPHA);
	imgg25.save("../asset/out_v1p1_remove_a.png");
	
	//insert to existing
	lap::Image imgg2 = imgg;
	imgg2.channelInsert(lap::Image::Channel::RED);
	imgg2.save("../asset/out_v1p1_insertinexisting_r.png");
	lap::Image imgg3 = imgg;
	imgg3.channelInsert(lap::Image::Channel::GREEN);
	imgg3.save("../asset/out_v1p1_insertinexisting_g.png");
	lap::Image imgg4 = imgg;
	imgg4.channelInsert(lap::Image::Channel::BLUE);
	imgg4.save("../asset/out_v1p1_insertinexisting_b.png");
	lap::Image imgg5 = imgg;
	imgg5.channelInsert(lap::Image::Channel::ALPHA);
	imgg5.save("../asset/out_v1p1_insertinexisting_a.png");

	//insert from zero and channel color
	lap::Image img;
	img.resize(200, 200);
	img.channelInsert(lap::Image::Channel::RED);
	img.save("../asset/out_v1p1_insert_r.png");
	img.channelColor(lap::Image::Channel::RED, 255);
	img.save("../asset/out_v1p1_color_r.png");
	img.channelInsert(lap::Image::Channel::GREEN);
	img.save("../asset/out_v1p1_insert_rg.png");
	img.channelColor(lap::Image::Channel::GREEN, 200);
	img.save("../asset/out_v1p1_color_rg.png");
	img.channelInsert(lap::Image::Channel::BLUE);
	img.save("../asset/out_v1p1_insert_rgb.png");
	img.channelColor(lap::Image::Channel::BLUE, 150);
	img.save("../asset/out_v1p1_color_rgb.png");
	img.channelInsert(lap::Image::Channel::ALPHA);
	img.save("../asset/out_v1p1_insert_rgba.png");
	img.channelColor(lap::Image::Channel::ALPHA, 255);
	img.save("../asset/out_v1p1_color_rgba.png");
	
	//channel switch
	img.channelClone(lap::Image::Channel::RED, lap::Image::Channel::RED);
	img.save("../asset/out_v1p1_clone_rtor.png");
	img.channelClone(lap::Image::Channel::RED, lap::Image::Channel::GREEN);
	img.save("../asset/out_v1p1_clone_rtog.png");
	img.channelClone(lap::Image::Channel::RED, lap::Image::Channel::BLUE);
	img.save("../asset/out_v1p1_clone_rtob.png");
	img.channelClone(lap::Image::Channel::RED, lap::Image::Channel::ALPHA);
	img.save("../asset/out_v1p1_clone_rtoa.png");

	//channel color
	img.channelColor(lap::Image::Channel::ALL, 128);
	img.save("../asset/out_v1p1_color_all_rgba.png");
	img.channelColor(lap::Image::Channel::ALLNOA, 188);
	img.save("../asset/out_v1p1_color_allnoa_rgba.png");
}

void test_operations() {
	lap::Image gif(std::string("../asset/animated.gif"));
	unsigned int width = gif.getWidth();
	unsigned int height = gif.getHeight();
	unsigned int channels = gif.getNumChannels();
	unsigned int frames = gif.getNumFrames();
	lap::Image gif50{ width + 3, height + 222, channels , frames + 100 };
	for (unsigned int f = 0; f < frames; f++) {
		for (unsigned int c = 0; c < channels; c++) {
			for (unsigned int j = 0; j < height; j++) {
				for (unsigned int i = 0; i < width; i++) {
					gif50(i, j, c, f) = 50;
				}
			}
		}
	}

	lap::Image sum = gif.sum(gif50);
	sum.save("../asset/out_ops_sum.gif");
	lap::Image dif = gif.difference(gif50);
	dif.save("../asset/out_ops_dif.gif");
}

void test_all(){
	test_basic();
	test_ctor_save();
	test_ctor_single_channel();
	test_ctor_save_animated();
	//test_assert();
	test_ctor_multi_channel();
	test_copyctor_multi_channel();
	test_memory_multi_channel();
	test_memory_multi_channel_multi_frame();
	test_memory_multi_channel_multi_frame_vec();
	test_resize();
	test_access();
	test_frames();
	test_switch_channel();
	test_save_advanced();
	test_transform();
	test_color();
	test_v1p1();
}

int main(int argc, char* argv[]){
	
	test_all();
	//test_memory_multi_channel_multi_frame_vec();
	//test_basic();
	//test_operations();

	std::cout << "Finished" << std::endl;
	std::cin.get();
	return 0;
}