# LAP GPU Profiler #

GPUProfiler is useful for profiling the dedicated and available memory, the number of primitives generated, the number  of generated transform feedback primitivs, the number of generated fragment samples and gpu and cpu work time.

version 1.00


###PROPERTIES###
- c++11, no exceptions
- OpenGL 4.5
- works on Windows, *nix and MACOS
- the OpenGL functions must be provided by the user, see line 107. In this case it uses the lap_wic library, set it to the desired OpenGL extension libray. This could have been done with extern functions but some OpenGL extension loader libraries are based on macros. Furthermore macros are used by many hook-based OpenGL debug libraries, therefore the current system was preferred.

###COMPILING AND LINKING###

Visual studio 2015 project&solution files can be found in the **vstudio** folder. Makefiles for gnu/gcc ( **makefile** ) and for visual studio ( **makefilevs.bat** ) are provided in the root folder. All methods will generate objs, pdbs, exes, etc in **bin**.

- Windows:
	- no special setup for visual studio projects
	- -pthread -lgdi32 otherwise
	- an example visual studio project+solution is provide in the vstudio folder
	- the makefilevs.bat from the root folder can also be used
- Linux:
	- -ldl -lGL -lX11 -lXxf86vm -lXrandr -lXi -lXinerama -lXcursor -lrt -lm
	- the wayland and mir backends are not used by lapwic (but they can easily be added in the future when they mature, right now they are experimental in glfw)
	- an example makefile is provided in the root folder
	- to install the project dependencies get:
		- sudo apt-get install xorg-dev
		- sudo apt-get install libgl-dev
	- also compiles in a VMWARE virtual machine, but needs a special mesa driver http://mesa3d.org/vmware-guest.html. It will crash as the mesa driver does not support OpenGL 4.5.
- OSX
	- -framework Cocoa -framework OpenGL -framework IOKit -framework CoreVideo -framework CoreFoundation -framework Carbon
	- an example makefile is provided in the root folder\n

### EXAMPLE - BASIC USAGE###

		#include "lap_gpu_profiler"
		...
		//create window context but don't create any gpu objects (this is only necessary on AMD cards, on NVIDIA you can create the profiler anywhere)
		...
		lap::GPUProfiler gpuprofiler;
		...
		//do some work here
		...
		//update
		gpuprofiler.update();
		//output stats
		std::cout << "GPU time since creation = " << gpuprofiler.getGPUtimeSinceResetMS() << " ms"<< std::endl;
		std::cout << "CPU time since reset = " << gpuprofiler.getCPUtimeSinceResetMS() << " ms" << std::endl;
		std::cout << "GPU memory available = " << gpuprofiler.getMemoryAvailableInexactInKB() << " kb" << std::endl;
		std::cout << "GPU memory dedicated = " << gpuprofiler.getMemoryDedicatedInKB() << " kb" << std::endl;
		std::cout << "GPU num_updates = " << gpuprofiler.getNumUpdates() << std::endl;
		std::cout << "GPU primitives generated = " << gpuprofiler.getPrimitivesGeneratedSinceReset() << std::endl;
		std::cout << "GPU fragment sample count = " << gpuprofiler.getSampleCountSinceReset() << std::endl;
		std::cout << "GPU transform freedback primitives = " << gpuprofiler.getTransformFeedbackPrimitivesWrittenSinceReset() << std::endl;
		
### EXAMPLE - PER FRAME USAGE ###
		
		#include "lap_gpu_profiler"
		...
		//create window context but don't create any gpu objects (this is only necessary on AMD cards, on NVIDIA you can create the profiler anywhere)
		...
		lap::GPUProfiler gpuprofiler;
		...
		while(app runnning){
			//reset profiler
			gpuprofiler.reset();
			...
			// do some work here
			...
			//update
			gpuprofiler.update();
			//output per-frame stats
			std::cout << "GPU time per update = " << gpuprofiler.getGPUtimeSinceResetMS() / (float)gpuprofiler.getNumUpdates() << " ms" << std::endl;
			std::cout << "CPU time per update = " << gpuprofiler.getCPUtimeSinceResetMS() / (float)gpuprofiler.getNumUpdates() << " ms" << std::endl;
			std::cout << "GPU primitives generated per update = " << gpuprofiler.getPrimitivesGeneratedSinceReset() / (float)gpuprofiler.getNumUpdates() << std::endl;
			std::cout << "GPU fragment sample count per update = " << gpuprofiler.getSampleCountSinceReset() / (float)gpuprofiler.getNumUpdates() << std::endl;
		}


### DOCUMENTATION ###

a Doxyfile is provided in **doc**, which can be used with Doxygen to generate  the entire documentation in HTML format. 
Note: doxygen has problems with arguments of std::function type function arguments (used for callbacks) and will issue a large number of warnings.


### COMPATIBILITY ###

tested with visual studio 2015, MinGW x64 4.0, Ubuntu 15 + gcc 4.8.4, MacOS El Capitan + clang 3.7.1.

does **NOT** work with Intel integrated gpu cards as they have no standard querying api.

###TODO LIST###

more testing on osx.