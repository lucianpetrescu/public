///---------------------------------------------------------------------------------------------------------------------
/// The MIT License (MIT)
/// 
/// Copyright (c) 2016 Lucian Petrescu, lucian (dot) petrescu (dot) 24 (at) gmail (dot) com
/// 
/// Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation 
/// files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, 
/// modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software 
/// is furnished to do so, subject to the following conditions:
/// 
/// The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
/// 
/// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
/// OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE 
/// LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR 
/// IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
///---------------------------------------------------------------------------------------------------------------------


#include "dep/lap_wic.hpp"
#include "lap_gpu_profiler.h"
#include "dep/lap_gpu_program.hpp"
#include <iostream>
#include <vector>
#include <cassert>



//-------------------------------------------------------------------------------------------------
//helper -> creates a vao and a vbo for testing purposes.
class Helper {
public:
	Helper() {
		//vao
		glCreateVertexArrays(1, &vao);
		glBindVertexArray(vao);

		//vbo
		glCreateBuffers(1, &vbo);
		std::vector<float> data = { 0.25f, 0.25f,  0.75f, 0.25f,  0.75f, 0.75f,  0.75f, 0.75f,  0.25f, 0.75f,  0.25f, 0.25f };
		glBindBuffer(GL_ARRAY_BUFFER, vbo);
		glBufferData(GL_ARRAY_BUFFER, sizeof(float) * data.size(), data.data(), GL_STATIC_DRAW);

		//attribs
		glEnableVertexAttribArray(0);
		glVertexAttribPointer(0, 2, GL_FLOAT, GL_FALSE, 2 * sizeof(float), 0);	//read pairs of floats from the vbo (bound to ARRAY_BUFFER) and send them to pipe 0)
	}
	~Helper() {
		glBindBuffer(GL_ARRAY_BUFFER, 0);
		glBindVertexArray(0);
		glDeleteBuffers(1, &vbo);
		glDeleteVertexArrays(1, &vao);
	}
	GLuint vao;
	GLuint vbo;
};



//-------------------------------------------------------------------------------------------------
int main(int argc, char* argv[]){
	//we need a window for the OpenGL context
	lap::wic::WICSystem wicsystem(nullptr, false);																											//no logging, singlethreaded, needed to created windows
	lap::wic::Window window(lap::wic::WindowProperties{}, lap::wic::FramebufferProperties{}, lap::wic::ContextProperties{}, lap::wic::InputProperties{});	//single window, needed for OpenGL context
	
	//create a vao and a vbo, take care of state, minimize code for this function
	Helper helper;
	glPatchParameteri(GL_PATCH_VERTICES, 3);

	//sources and program (test program creation from both sources and files)
	std::string vs = "#version 450\n"
		"layout(location = 0) in vec2 pos;\n"
		"void main(){\n"
		"	gl_Position = vec4(pos, 0, 1);\n"
		"}\n";
	std::string tcs = "#version 450\n"
		"layout(vertices = 3) out;\n"
		"void main(){\n"
		"	if (gl_InvocationID == 0) {\n"
		"		gl_TessLevelInner[0] = 1;\n"
		"		gl_TessLevelOuter[0] = 1;\n"
		"		gl_TessLevelOuter[1] = 1;\n"
		"		gl_TessLevelOuter[2] = 1;\n"
		"	}\n"
		"	gl_out[gl_InvocationID].gl_Position = gl_in[gl_InvocationID].gl_Position;\n"
		"}\n";
	std::string tes = "#version 450\n"
		"layout(triangles, equal_spacing, ccw) in;\n"
		"void main(){\n"
		"	gl_Position = gl_TessCoord.x * gl_in[0].gl_Position + gl_TessCoord.y * gl_in[1].gl_Position + gl_TessCoord.z * gl_in[2].gl_Position;\n"
		"}\n";
	std::string fs = "#version 450\n"
		"layout(location = 0) out vec3 colorout;\n"
		"void main(){\n"
		"	colorout = vec3(0,0,1);\n"
		"}\n";
	lap::gpu::GPUProgram program(vs, false, tcs, false, tes, false, fs, false, &std::cout);				//create program from source string
	
	//texture id
	std::vector<GLuint> texids; texids.resize(100, 0);

	//create the profiler
	lap::GPUProfiler gpuprofiler;
	std::cout << "Created profiler on " << std::endl;
	std::cout << "   vendor   : " << gpuprofiler.getVendor() << std::endl;
	std::cout << "   renderer : " << gpuprofiler.getRenderer() << std::endl;
	std::cout << "   version  : " << gpuprofiler.getVersionMajor() << "." << gpuprofiler.getVersionMinor() << std::endl;
	
	//draw on screen (quad in ndc)
	glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);
	while (window.isOpened()) {
		//frame id
		static int frame = 0;
		frame = (frame + 1) % 1000000;

		//do some drawing work
		glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
		program.bind();
		glDrawArrays(GL_PATCHES, 0, 6);
		program.unbind();

		//do some texture work
		GLuint& texid = texids[(frame + 79 + 19* (frame%17)) % 100];
		if (texid != 0)glDeleteTextures(1, &texid);
		glCreateTextures(GL_TEXTURE_2D, 1, &texid);
		glTextureStorage2D(texid, 1, GL_R8, frame % 1000 + 200, frame % 300 + frame % 500 + 300);

		//update stats
		gpuprofiler.update();

		//print every 360 screen updates (vsynced) -> 6 or 3 seconds depending on hz.
		if (frame % 360 == 0) {
			std::cout << "------------------------ frame " << gpuprofiler.getNumUpdates() << " ------------------------" << std::endl;
			std::cout << "GPU time since reset = " << gpuprofiler.getGPUtimeSinceResetMS() << " ms"<< std::endl;
			std::cout << "GPU time per update = " << gpuprofiler.getGPUtimeSinceResetMS() / (float)gpuprofiler.getNumUpdates() << " ms" << std::endl;
			std::cout << "CPU time since reset = " << gpuprofiler.getCPUtimeSinceResetMS() << " ms" << std::endl;
			std::cout << "CPU time per update = " << gpuprofiler.getCPUtimeSinceResetMS() / (float)gpuprofiler.getNumUpdates() << " ms" << std::endl;
			std::cout << "GPU memory available = " << gpuprofiler.getMemoryAvailableInexactInKB() << " kb" << std::endl;
			std::cout << "GPU memory dedicated = " << gpuprofiler.getMemoryDedicatedInKB() << " kb" << std::endl;
			std::cout << "GPU num_updates = " << gpuprofiler.getNumUpdates() << std::endl;
			std::cout << "GPU primitives generated = " << gpuprofiler.getPrimitivesGeneratedSinceReset() << std::endl;
			std::cout << "GPU primitives generated per update = " << gpuprofiler.getPrimitivesGeneratedSinceReset() / (float)gpuprofiler.getNumUpdates() << std::endl;
			std::cout << "GPU fragment sample count = " << gpuprofiler.getSampleCountSinceReset() << std::endl;
			std::cout << "GPU fragment sample count per update = " << gpuprofiler.getSampleCountSinceReset() / (float)gpuprofiler.getNumUpdates() << std::endl;
			std::cout << "GPU transform freedback primitives = " << gpuprofiler.getTransformFeedbackPrimitivesWrittenSinceReset() << std::endl;
		}

		window.swapBuffers();
		wicsystem.processProgramEvents();	//process ALL windowing system events
		while (auto* e = window.processEvent()) if (e->type == lap::wic::Event::Type::KeyPress && e->KeyPress.key == lap::wic::Key::ESCAPE) window.close();

	}

	//clean up textures
	for (auto& texid : texids) if (texid != 0) glDeleteTextures(1, &texid);

	//reality check
	assert(glGetError() == GL_NO_ERROR);

	std::cout << "Finished" << std::endl;
	std::cin.get();

	// program goes out of scope
	// helper goes out of scope
	// window and wicsystem go out of scope
	return 0;
}
