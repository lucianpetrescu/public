﻿///---------------------------------------------------------------------------------------------------------------------
/// The MIT License (MIT)
/// 
/// Copyright (c) 2016 Lucian Petrescu, lucian (dot) petrescu (dot) 24 (at) gmail (dot) com
/// 
/// Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation 
/// files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, 
/// modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software 
/// is furnished to do so, subject to the following conditions:
/// 
/// The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
/// 
/// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
/// OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE 
/// LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR 
/// IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
///---------------------------------------------------------------------------------------------------------------------

#include "lap_gpu_program.hpp"
#include <cassert>
#include <fstream>
#include <sstream>

namespace lap{
	namespace gpu {

		///---------------------------------------------------------------------------------------------------------------------
		GPUProgram::GPUProgram(std::string filename, GLenum in_binaryformat, std::ostream* in_logger){
			logger = in_logger;
			load(filename, in_binaryformat);
		}
		GPUProgram::GPUProgram(const std::vector<char>& binarydata, GLenum in_binaryformat, std::ostream* in_logger) {
			logger = in_logger;
			load(binarydata, in_binaryformat);
		}
		
		GPUProgram::GPUProgram(const std::vector<std::string>& srcs, const std::vector<SHADERTYPE>& types, const std::vector<bool>& isfile, std::ostream* in_logger) {
			logger = in_logger;
			load(srcs, types, isfile);
		}
		GPUProgram::GPUProgram(const std::string& src_cs, bool isfile_cs, std::ostream* in_logger)
			: GPUProgram({ src_cs }, { SHADERTYPE::COMPUTE }, { isfile_cs }, in_logger) {
		}
		GPUProgram::GPUProgram(const std::string& src_vs, bool isfile_vs, const std::string& src_fs, bool isfile_fs, std::ostream* in_logger )
			: GPUProgram({ src_vs, src_fs }, { SHADERTYPE::VERTEX, SHADERTYPE::FRAGMENT }, { isfile_vs, isfile_fs }, in_logger) {
		}
		GPUProgram::GPUProgram(const std::string& src_vs, bool isfile_vs, const std::string& src_gs, bool isfile_gs, const std::string& src_fs, bool isfile_fs, std::ostream* in_logger )
			: GPUProgram({ src_vs, src_gs, src_fs }, { SHADERTYPE::VERTEX, SHADERTYPE::GEOMETRY, SHADERTYPE::FRAGMENT }, { isfile_vs, isfile_gs, isfile_fs }, in_logger) {
		}
		GPUProgram::GPUProgram(const std::string& src_vs, bool isfile_vs, const std::string& src_tcs, bool isfile_tcs, const std::string& src_tes, bool isfile_tes, const std::string& src_fs, bool isfile_fs, std::ostream* in_logger )
			: GPUProgram({ src_vs, src_tcs, src_tes, src_fs }, { SHADERTYPE::VERTEX, SHADERTYPE::TESS_CONTROL, SHADERTYPE::TESS_EVALUATION, SHADERTYPE::FRAGMENT }, {isfile_vs, isfile_tcs, isfile_tes, isfile_fs}, in_logger) {
		}
		GPUProgram::GPUProgram(const std::string& src_vs, bool isfile_vs, const std::string& src_tcs, bool isfile_tcs, const std::string& src_tes, bool isfile_tes, const std::string& src_gs, bool isfile_gs, const std::string& src_fs, bool isfile_fs, std::ostream* in_logger)
			: GPUProgram({ src_vs, src_tcs, src_tes, src_gs, src_fs }, { SHADERTYPE::VERTEX, SHADERTYPE::TESS_CONTROL, SHADERTYPE::TESS_EVALUATION, SHADERTYPE::GEOMETRY, SHADERTYPE::FRAGMENT }, { isfile_vs, isfile_tcs, isfile_tes, isfile_gs, isfile_fs }, in_logger) {
		}
		GPUProgram::~GPUProgram() {
			if (logger) (*logger) << "[LAP_SHADER] Destroyed program " << glprogram << std::endl;
			glDeleteProgram(glprogram);
		}


		///---------------------------------------------------------------------------------------------------------------------
		void GPUProgram::load(std::string filename, GLenum in_binaryformat) {
			log = "[LAP_SHADER] Loading binary program from " + filename + ", using binary format " + std::to_string(in_binaryformat) + "\n";
			internalLoad(filename, in_binaryformat);
			if (logger) (*logger) << log;
		}
		void GPUProgram::load(const std::vector<char>& binarydata, GLenum in_binaryformat) {
			log = "[LAP_SHADER] Loading binary program from memory, using binary format " + std::to_string(in_binaryformat) + "\n";
			internalLoad(binarydata, in_binaryformat);
			if (logger) (*logger) << log;
		}
		void GPUProgram::load(const std::vector<std::string>& src, const std::vector<SHADERTYPE>& type, const std::vector<bool>& isfile) {
			log = "[LAP_SHADER] ";
			internalLoad(src, type, isfile);
			if (logger) (*logger) << log;
		}

		void GPUProgram::load(const std::string& src_cs, bool isfile_cs) {
			load({ src_cs }, { SHADERTYPE::COMPUTE }, { isfile_cs });
		}
		void GPUProgram::load(const std::string& src_vs, bool isfile_vs, const std::string& src_fs, bool isfile_fs) {
			load({ src_vs, src_fs }, { SHADERTYPE::VERTEX, SHADERTYPE::FRAGMENT }, { isfile_vs, isfile_fs });
		}
		void GPUProgram::load(const std::string& src_vs, bool isfile_vs, const std::string& src_gs, bool isfile_gs, const std::string& src_fs, bool isfile_fs) {
			load({ src_vs, src_gs, src_fs }, { SHADERTYPE::VERTEX, SHADERTYPE::GEOMETRY, SHADERTYPE::FRAGMENT }, { isfile_vs, isfile_gs, isfile_fs });
		}
		void GPUProgram::load(const std::string& src_vs, bool isfile_vs, const std::string& src_tcs, bool isfile_tcs, const std::string& src_tes, bool isfile_tes, const std::string& src_fs, bool isfile_fs) {
			load({ src_vs, src_tcs, src_tes, src_fs }, { SHADERTYPE::VERTEX, SHADERTYPE::TESS_CONTROL, SHADERTYPE::TESS_EVALUATION, SHADERTYPE::FRAGMENT }, { isfile_vs, isfile_tcs, isfile_tes, isfile_fs });
		}
		void GPUProgram::load(const std::string& src_vs, bool isfile_vs, const std::string& src_tcs, bool isfile_tcs, const std::string& src_tes, bool isfile_tes, const std::string& src_gs, bool isfile_gs, const std::string& src_fs, bool isfile_fs) {
			load({ src_vs, src_tcs, src_tes, src_gs, src_fs }, { SHADERTYPE::VERTEX, SHADERTYPE::TESS_CONTROL, SHADERTYPE::TESS_EVALUATION, SHADERTYPE::GEOMETRY, SHADERTYPE::FRAGMENT }, { isfile_vs, isfile_tcs, isfile_tes, isfile_gs, isfile_fs });
		}


		void GPUProgram::internalClean() {
			//reset internal state
			if (glprogram != 0) {
				glDeleteProgram(glprogram);
				glprogram = 0;
			}
			//no logger modifications
			islinked = false;
			shaderdesc.clear();
			binaryformat = GL_NONE;
			binaryfilename = "";
			//clear uniform locations
			uniform_locations.clear();
		}
		void GPUProgram::internalLoad(const std::vector<std::string>& src, const std::vector<SHADERTYPE>& type, const std::vector<bool>& isfile) {
			//sanity check
			assert(src.size() == type.size() && type.size() == isfile.size());

			//reset internal state
			internalClean();

			//start log
			std::string fromfiles;
			bool frommemory = false;
			for (unsigned int i = 0; i < src.size(); i++) {
				if (isfile[i]) fromfiles += src[i] + " ";
				else frommemory = true;
			}
			log += "Loading program from ";
			if (fromfiles.size() > 0) log += " (" + fromfiles + ")";
			if (fromfiles.size() > 0 && frommemory) log += " and ";
			if (frommemory) log += "memory";
			log += ".\n";

			//for each shader do (create -> load source -> set source-> compile -> store in glshader)
			std::vector<GLuint> glshader;	glshader.resize(src.size());
			for (unsigned int i = 0; i < src.size(); i++) {

				//save information for potential reloading
				ShaderDesc desc;
				desc.isfile = isfile[i];
				desc.shadertype = type[i];
				desc.src = src[i];
				shaderdesc.push_back(desc);

				//obtain shader code
				std::string code;
				if (isfile[i]) {
					std::ifstream file(src[i].c_str());
					if (!file.good()) {
						log += "             ERROR : Could not find or open " + src[i] + ".\n";
						//don't throw (no exceptions) or exit (bad design), use the (i)fstream model of loading and isLinked/isValid
						//instead set it to the filename, compiler will complain and the user will have a bigger % of understanding the problem
						code = src[i];
					}
					else {
						 while( file.good() ) {
							std::string line;
							std::getline(file, line);
							code.append(line + "\n");
						}
					}
					file.close();
				}

				//create a shader
				glshader[i] = glCreateShader((int)type[i]);
				const char *code_ptr; int code_size;
				if (isfile[i]) {
					code_ptr = code.c_str();
					code_size = (int)code.size();
				}
				else {
					code_ptr = src[i].c_str();
					code_size = (int)src[i].size();
				}
				glShaderSource(glshader[i], 1, &code_ptr, &code_size);
				glCompileShader(glshader[i]);

				//check compile
				GLint compile_result;
				glGetShaderiv(glshader[i], GL_COMPILE_STATUS, &compile_result);
				if (compile_result == GL_FALSE && logger) {
					std::string str_shader_type = "";
					switch ((int)type[i]) {
					case GL_VERTEX_SHADER: str_shader_type = "vertex shader"; break;
					case GL_TESS_CONTROL_SHADER: str_shader_type = "tessellation control shader"; break;
					case GL_TESS_EVALUATION_SHADER: str_shader_type = "tessellation evaluation shader"; break;
					case GL_GEOMETRY_SHADER: str_shader_type = "geometry shader"; break;
					case GL_FRAGMENT_SHADER: str_shader_type = "fragment shader"; break;
					case GL_COMPUTE_SHADER: str_shader_type = "compute shader"; break;
					}

					GLint info_log_length;
					glGetShaderiv(glshader[i], GL_INFO_LOG_LENGTH, &info_log_length);
					std::string shader_log;
					shader_log.resize(info_log_length);
					glGetShaderInfoLog(glshader[i], info_log_length, NULL, &shader_log[0]);

					//header
					log += "             COMPILE ERROR for " + str_shader_type;
					if (isfile[i]) log + " from file " + src[i];
					log += " : \n";

					//compiler log but with proper spacing
					std::stringstream ss; ss.str(shader_log);
					std::string s;
					while (std::getline(ss, s)) {
						log += "                " + s + "\n";
					}
				}
			}

			//program : create -> attach all shaders -> link -> check
			glprogram = glCreateProgram();
			for (auto& s : glshader) glAttachShader(glprogram, s);
			glProgramParameteri(glprogram, GL_PROGRAM_BINARY_RETRIEVABLE_HINT, GL_TRUE);
			glLinkProgram(glprogram);

			//check link
			GLint link_result;
			glGetProgramiv(glprogram, GL_LINK_STATUS, &link_result);
			if (link_result == GL_FALSE) {
				int info_log_length;
				glGetProgramiv(glprogram, GL_INFO_LOG_LENGTH, &info_log_length);
				std::string program_log;
				program_log.resize(info_log_length);
				glGetProgramInfoLog(glprogram, info_log_length, NULL, &program_log[0]);
				log += "             LINK ERROR : \n";

				//program log but with proper spacing
				std::stringstream ss; ss.str(program_log);
				std::string s;
				while (std::getline(ss, s)) {
					log += "                " + s + "\n";
				}

				//no termination, just flag it like fstream.
				islinked = false;
			}
			else {
				//all good
				islinked = true;

				//log success
				log += "             Successfully created program " + std::to_string(glprogram) + ".\n";

			}

			//detach and delete the shader objects, they now only pointlessly occupy space.
			for (auto& s : glshader) {
				glDetachShader(glprogram, s);
				glDeleteShader(s);
			}
		}
		void GPUProgram::internalLoad(const std::string& filename, GLenum in_binaryformat) {
			//load data from file
			std::ifstream file(filename.c_str(), std::ios::binary);
			std::vector<char> binary;
			unsigned int binarysize = 0;
			if (!file.good()) {
				log += "             ERROR : Could not find or open " + filename + ".\n";
				//don't throw (no exceptions) or exit (bad design), use the (i)fstream model of loading and isLinked/isValid
				//instead set it to the nullptr, compiler will complain and the user will have a bigger % of understanding the problem
			}
			else {
				//all good, read data
				file.seekg(0, std::ios::end);
				binarysize = (unsigned int)file.tellg();
				binary.resize(binarysize);
				file.seekg(0, std::ios::beg);
				file.read(binary.data(), binarysize);
			}
			file.close();

			//call internal loading and log
			internalLoad(binary, in_binaryformat);
			binaryfilename = filename;
		}
		void GPUProgram::internalLoad(const std::vector<char>& binarydata, GLenum in_binaryformat) {
			//reset internal state
			internalClean();
			binaryformat = in_binaryformat;
			binaryfilename = "";

			//sanity check
			int num_program_binary_formats;
			glGetIntegerv(GL_NUM_PROGRAM_BINARY_FORMATS, &num_program_binary_formats);
			std::vector<int> program_binary_formats;
			program_binary_formats.resize(num_program_binary_formats);
			glGetIntegerv(GL_PROGRAM_BINARY_FORMATS, program_binary_formats.data());
			bool foundformat = false;
			for (auto& pbf : program_binary_formats) if ((unsigned int)pbf == binaryformat) foundformat = true;
			if (!foundformat) {
				log += "             ERROR : trying to load with " + std::to_string(binaryformat) + " format, but the format is not supported on this machine.\n";
				return;
			}

			//load shader
			glprogram = glCreateProgram();
			glProgramBinary(glprogram, binaryformat, binarydata.data(), (GLsizei)binarydata.size());

			//link status
			GLint link_result;
			glGetProgramiv(glprogram, GL_LINK_STATUS, &link_result);
			if (link_result == GL_FALSE) {
				int info_log_length;
				glGetProgramiv(glprogram, GL_INFO_LOG_LENGTH, &info_log_length);
				std::string program_log;
				program_log.resize(info_log_length);
				glGetProgramInfoLog(glprogram, info_log_length, NULL, &program_log[0]);
				log += "             ERROR: link failed, message: \n" + program_log + ".\n";
				//no termination, just flag it like fstream.
				islinked = false;
			}
			else {
				//all good
				islinked = true;
				//log success
				log += "             Successfully created program " + std::to_string(glprogram) + " from memory.\n";
			}
		}


		///---------------------------------------------------------------------------------------------------------------------
		void GPUProgram::reload() {
			if (logger) (*logger) << "[LAP_SHADER] Reloading program " << glprogram << "." << std::endl;

			if (binaryfilename != "") {
				std::string original_binaryfilename = binaryfilename;
				GLenum original_binaryformat = binaryformat;
				internalLoad(original_binaryfilename, original_binaryformat);
			}
			else if (binaryformat != GL_NONE) {
				if (logger) (*logger) << "             WARNING : reloading only from memory (nothing from files) makes no sense." << std::endl;
			}
			else {
				//reload from files and source
				std::vector<std::string> src;	src.resize(shaderdesc.size());
				std::vector<SHADERTYPE> type;	type.resize(shaderdesc.size());
				std::vector<bool> isfile;		isfile.resize(shaderdesc.size());
				bool warning = true;
				for (unsigned int i = 0; i < shaderdesc.size(); i++) {
					auto& desc = shaderdesc[i];
					src[i] = desc.src;
					type[i] = desc.shadertype;
					isfile[i] = desc.isfile;
					if (isfile[i]) warning = false;
				}
				if (warning && logger) (*logger) << "             WARNING : reloading only from memory (nothing from files) makes no sense." << std::endl;
				internalLoad(src, type, isfile);
			}
			if (logger) (*logger) << "             "<<(islinked?"Successfully":"Unsuccessfuly")<<" loaded program " << glprogram << "." << std::endl;

		}
		

		///---------------------------------------------------------------------------------------------------------------------
		bool GPUProgram::saveBinary(const std::string& filename, GLenum& out_binaryformat) {
			if (logger) (*logger) << "[LAP_SHADER] Saving binarily to" + filename + ".\n";
			log = "";
			std::vector<char> binarydata;
			GLenum binformat;
			bool result = internalSaveBinary(binarydata, binformat);
			out_binaryformat = binformat;
			if (!result) {
				if (logger) (*logger) << log;
				return false;
			}
			
			std::ofstream file(filename.c_str(), std::ios::binary);
			file.write(binarydata.data(), binarydata.size());
			file.close();
			if (logger) (*logger) << log;
			return true;
		}
		bool GPUProgram::saveBinary(std::vector<char>& binarydata, GLenum& out_binaryformat) {
			if (logger) (*logger) << "[LAP_SHADER] Saving binarily to memory.\n";
			log = "";
			GLenum binformat;
			bool result = internalSaveBinary(binarydata, binformat);
			out_binaryformat = binformat;
			if (logger) (*logger) << log;
			return result;
		}
		bool GPUProgram::internalSaveBinary(std::vector<char>& data, GLenum& out_binaryformat) {
			//sanity check
			if (!islinked) {
				log += "             ERROR : can't save a program with a failed build (not linked), it has no binary.\n";
				return false;
			}
			GLint binary_length;
			glGetProgramiv(glprogram, GL_PROGRAM_BINARY_LENGTH, &binary_length);
			if (binary_length == 0) {
				log += "             ERROR : binary has 0 size, saving to binary is most probably unsupported.\n";
				return false;
			}
			
			//save
			data.resize(binary_length);
			GLsizei size;
			GLenum binformat;
			glGetProgramBinary(glprogram, binary_length, &size, &binformat, data.data());
			out_binaryformat = binformat;
			data.resize(size);
			if (binary_length < size) {
				log += "             ERROR : bad driver, saving to binary is most probably unsupported.\n";
				return false;
			}
			log += "             Successfully saved with " + std::to_string(binformat) + " format.\n";
			return true;
		}



		///---------------------------------------------------------------------------------------------------------------------
		void GPUProgram::bind() {
			glUseProgram(glprogram);
		}
		void GPUProgram::unbind() {
			glUseProgram(0);
		}
		bool GPUProgram::isValid() {
			glValidateProgram(glprogram);
			GLint valid;
			glGetProgramiv(glprogram, GL_VALIDATE_STATUS, &valid);
			return (valid == GL_TRUE);
		}
		bool GPUProgram::isLinked() {
			return islinked;
		}
		GLuint GPUProgram::getGLObject() {
			return glprogram;
		}

		void GPUProgram::setLogger(std::ostream* in_logger) {
			logger = in_logger;
		}
		std::ostream* GPUProgram::getLogger() {
			return logger;
		}


		///---------------------------------------------------------------------------------------------------------------------
		int GPUProgram::internalGetAndSetUniformLocation(const std::string& name, bool silentwarning) {
			assert(islinked);
			int location = -1;
			//search cache
			auto it = uniform_locations.find(name);
			if (it != uniform_locations.end()) location = it->second;
			else {
				//search shader and add to cache
				location = glGetUniformLocation(glprogram, name.c_str());
				uniform_locations[name] = location;
			}

			//issue warning if trying to set a inexistent/unsed uniform 
			if (location == -1 && logger && !silentwarning) (*logger) << "[LAP_SHADER] WARNING : uniform " << name << " does not exist (or is not used) in program " << glprogram << std::endl;

			return location;
		}
		
		void GPUProgram::setUniform1f(const std::string& name, float v0, bool silentwarning) {
			glProgramUniform1f(glprogram, internalGetAndSetUniformLocation(name, silentwarning), v0);
		}
		void GPUProgram::setUniform2f(const std::string& name, float v0, float v1, bool silentwarning) {
			glProgramUniform2f(glprogram, internalGetAndSetUniformLocation(name, silentwarning), v0, v1);
		}
		void GPUProgram::setUniform3f(const std::string& name, float v0, float v1, float v2, bool silentwarning) {
			glProgramUniform3f(glprogram, internalGetAndSetUniformLocation(name, silentwarning), v0, v1, v2);
		}
		void GPUProgram::setUniform4f(const std::string& name, float v0, float v1, float v2, float v3, bool silentwarning) {
			glProgramUniform4f(glprogram, internalGetAndSetUniformLocation(name, silentwarning), v0, v1, v2, v3);
		}
		void GPUProgram::setUniform1fv(const std::string& name, unsigned int count, const float *v, bool silentwarning) {
			glProgramUniform1fv(glprogram, internalGetAndSetUniformLocation(name, silentwarning), count, v);
		}
		void GPUProgram::setUniform2fv(const std::string& name, unsigned int count, const float *v, bool silentwarning) {
			glProgramUniform2fv(glprogram, internalGetAndSetUniformLocation(name, silentwarning), count, v);
		}
		void GPUProgram::setUniform3fv(const std::string& name, unsigned int count, const float *v, bool silentwarning) {
			glProgramUniform3fv(glprogram, internalGetAndSetUniformLocation(name, silentwarning), count, v);
		}
		void GPUProgram::setUniform4fv(const std::string& name, unsigned int count, const float *v, bool silentwarning) {
			glProgramUniform4fv(glprogram, internalGetAndSetUniformLocation(name, silentwarning), count, v);
		}
		

		void GPUProgram::setUniform1d(const std::string& name, double v0, bool silentwarning) {
			glProgramUniform1d(glprogram, internalGetAndSetUniformLocation(name, silentwarning), v0);
		}
		void GPUProgram::setUniform2d(const std::string& name, double v0, double v1, bool silentwarning) {
			glProgramUniform2d(glprogram, internalGetAndSetUniformLocation(name, silentwarning), v0, v1);
		}
		void GPUProgram::setUniform3d(const std::string& name, double v0, double v1, double v2, bool silentwarning) {
			glProgramUniform3d(glprogram, internalGetAndSetUniformLocation(name, silentwarning), v0, v1, v2);
		}
		void GPUProgram::setUniform4d(const std::string& name, double v0, double v1, double v2, double v3, bool silentwarning) {
			glProgramUniform4d(glprogram, internalGetAndSetUniformLocation(name, silentwarning), v0, v1, v2, v3);
		}
		void GPUProgram::setUniform1dv(const std::string& name, unsigned int count, const double *v, bool silentwarning) {
			glProgramUniform1dv(glprogram, internalGetAndSetUniformLocation(name, silentwarning), count, v);
		}
		void GPUProgram::setUniform2dv(const std::string& name, unsigned int count, const double *v, bool silentwarning) {
			glProgramUniform2dv(glprogram, internalGetAndSetUniformLocation(name, silentwarning), count, v);
		}
		void GPUProgram::setUniform3dv(const std::string& name, unsigned int count, const double *v, bool silentwarning) {
			glProgramUniform3dv(glprogram, internalGetAndSetUniformLocation(name, silentwarning), count, v);
		}
		void GPUProgram::setUniform4dv(const std::string& name, unsigned int count, const double *v, bool silentwarning) {
			glProgramUniform4dv(glprogram, internalGetAndSetUniformLocation(name, silentwarning), count, v);
		}

		void GPUProgram::setUniform1i(const std::string& name, int v0, bool silentwarning) {
			glProgramUniform1i(glprogram, internalGetAndSetUniformLocation(name, silentwarning), v0);
		}
		void GPUProgram::setUniform2i(const std::string& name, int v0, int v1, bool silentwarning) {
			glProgramUniform2i(glprogram, internalGetAndSetUniformLocation(name, silentwarning), v0, v1);
		}
		void GPUProgram::setUniform3i(const std::string& name, int v0, int v1, int v2, bool silentwarning) {
			glProgramUniform3i(glprogram, internalGetAndSetUniformLocation(name, silentwarning), v0, v1, v2);
		}
		void GPUProgram::setUniform4i(const std::string& name, int v0, int v1, int v2, int v3, bool silentwarning) {
			glProgramUniform4i(glprogram, internalGetAndSetUniformLocation(name, silentwarning), v0, v1, v2, v3);
		}
		void GPUProgram::setUniform1iv(const std::string& name, unsigned int count, const int *v, bool silentwarning) {
			glProgramUniform1iv(glprogram, internalGetAndSetUniformLocation(name, silentwarning), count, v);
		}
		void GPUProgram::setUniform2iv(const std::string& name, unsigned int count, const int *v, bool silentwarning) {
			glProgramUniform2iv(glprogram, internalGetAndSetUniformLocation(name, silentwarning), count, v);
		}
		void GPUProgram::setUniform3iv(const std::string& name, unsigned int count, const int *v, bool silentwarning) {
			glProgramUniform3iv(glprogram, internalGetAndSetUniformLocation(name, silentwarning), count, v);
		}
		void GPUProgram::setUniform4iv(const std::string& name, unsigned int count, const int *v, bool silentwarning) {
			glProgramUniform4iv(glprogram, internalGetAndSetUniformLocation(name, silentwarning), count, v);
		}

		void GPUProgram::setUniform1ui(const std::string& name, unsigned int v0, bool silentwarning) {
			glProgramUniform1ui(glprogram, internalGetAndSetUniformLocation(name, silentwarning), v0);
		}
		void GPUProgram::setUniform2ui(const std::string& name, unsigned int v0, unsigned int v1, bool silentwarning) {
			glProgramUniform2ui(glprogram, internalGetAndSetUniformLocation(name, silentwarning), v0, v1);
		}
		void GPUProgram::setUniform3ui(const std::string& name, unsigned int v0, unsigned int v1, unsigned int v2, bool silentwarning) {
			glProgramUniform3ui(glprogram, internalGetAndSetUniformLocation(name, silentwarning), v0, v1, v2);
		}
		void GPUProgram::setUniform4ui(const std::string& name, unsigned int v0, unsigned int v1, unsigned int v2, unsigned int v3, bool silentwarning) {
			glProgramUniform4ui(glprogram, internalGetAndSetUniformLocation(name, silentwarning), v0, v1, v2, v3);
		}
		void GPUProgram::setUniform1uiv(const std::string& name, unsigned int count, const unsigned int *v, bool silentwarning) {
			glProgramUniform1uiv(glprogram, internalGetAndSetUniformLocation(name, silentwarning), count, v);
		}
		void GPUProgram::setUniform2uiv(const std::string& name, unsigned int count, const unsigned int *v, bool silentwarning) {
			glProgramUniform2uiv(glprogram, internalGetAndSetUniformLocation(name, silentwarning), count, v);
		}
		void GPUProgram::setUniform3uiv(const std::string& name, unsigned int count, const unsigned int *v, bool silentwarning) {
			glProgramUniform3uiv(glprogram, internalGetAndSetUniformLocation(name, silentwarning), count, v);
		}
		void GPUProgram::setUniform4uiv(const std::string& name, unsigned int count, const unsigned int *v, bool silentwarning) {
			glProgramUniform4uiv(glprogram, internalGetAndSetUniformLocation(name, silentwarning), count, v);
		}

		void GPUProgram::setUniformMatrix2dv(const std::string& name, unsigned int count, bool transpose, const double* v, bool silentwarning) {
			glProgramUniformMatrix2dv(glprogram, internalGetAndSetUniformLocation(name, silentwarning), count, transpose ? GL_TRUE : GL_FALSE, v);
		}
		void GPUProgram::setUniformMatrix2fv(const std::string& name, unsigned int count, bool transpose, const float* v, bool silentwarning) {
			glProgramUniformMatrix2fv(glprogram, internalGetAndSetUniformLocation(name, silentwarning), count, transpose ? GL_TRUE : GL_FALSE, v);
		}
		void GPUProgram::setUniformMatrix2x3dv(const std::string& name, unsigned int count, bool transpose, const double* v, bool silentwarning) {
			glProgramUniformMatrix2x3dv(glprogram, internalGetAndSetUniformLocation(name, silentwarning), count, transpose ? GL_TRUE : GL_FALSE, v);
		}
		void GPUProgram::setUniformMatrix2x3fv(const std::string& name, unsigned int count, bool transpose, const float* v, bool silentwarning) {
			glProgramUniformMatrix2x3fv(glprogram, internalGetAndSetUniformLocation(name, silentwarning), count, transpose ? GL_TRUE : GL_FALSE, v);
		}
		void GPUProgram::setUniformMatrix2x4dv(const std::string& name, unsigned int count, bool transpose, const double* v, bool silentwarning) {
			glProgramUniformMatrix2x4dv(glprogram, internalGetAndSetUniformLocation(name, silentwarning), count, transpose ? GL_TRUE : GL_FALSE, v);
		}
		void GPUProgram::setUniformMatrix2x4fv(const std::string& name, unsigned int count, bool transpose, const float* v, bool silentwarning) {
			glProgramUniformMatrix2x4fv(glprogram, internalGetAndSetUniformLocation(name, silentwarning), count, transpose ? GL_TRUE : GL_FALSE, v);
		}
		void GPUProgram::setUniformMatrix3dv(const std::string& name, unsigned int count, bool transpose, const double* v, bool silentwarning) {
			glProgramUniformMatrix3dv(glprogram, internalGetAndSetUniformLocation(name, silentwarning), count, transpose ? GL_TRUE : GL_FALSE, v);
		}
		void GPUProgram::setUniformMatrix3fv(const std::string& name, unsigned int count, bool transpose, const float* v, bool silentwarning) {
			glProgramUniformMatrix3fv(glprogram, internalGetAndSetUniformLocation(name, silentwarning), count, transpose ? GL_TRUE : GL_FALSE, v);
		}
		void GPUProgram::setUniformMatrix3x2dv(const std::string& name, unsigned int count, bool transpose, const double* v, bool silentwarning) {
			glProgramUniformMatrix3x2dv(glprogram, internalGetAndSetUniformLocation(name, silentwarning), count, transpose ? GL_TRUE : GL_FALSE, v);
		}
		void GPUProgram::setUniformMatrix3x2fv(const std::string& name, unsigned int count, bool transpose, const float* v, bool silentwarning) {
			glProgramUniformMatrix3x2fv(glprogram, internalGetAndSetUniformLocation(name, silentwarning), count, transpose ? GL_TRUE : GL_FALSE, v);
		}
		void GPUProgram::setUniformMatrix3x4dv(const std::string& name, unsigned int count, bool transpose, const double* v, bool silentwarning) {
			glProgramUniformMatrix3x4dv(glprogram, internalGetAndSetUniformLocation(name, silentwarning), count, transpose ? GL_TRUE : GL_FALSE, v);
		}
		void GPUProgram::setUniformMatrix3x4fv(const std::string& name, unsigned int count, bool transpose, const float* v, bool silentwarning) {
			glProgramUniformMatrix3x4fv(glprogram, internalGetAndSetUniformLocation(name, silentwarning), count, transpose ? GL_TRUE : GL_FALSE, v);
		}
		void GPUProgram::setUniformMatrix4dv(const std::string& name, unsigned int count, bool transpose, const double* v, bool silentwarning) {
			glProgramUniformMatrix4dv(glprogram, internalGetAndSetUniformLocation(name, silentwarning), count, transpose ? GL_TRUE : GL_FALSE, v);
		}
		void GPUProgram::setUniformMatrix4fv(const std::string& name, unsigned int count, bool transpose, const float* v, bool silentwarning) {
			glProgramUniformMatrix4fv(glprogram, internalGetAndSetUniformLocation(name, silentwarning), count, transpose ? GL_TRUE : GL_FALSE, v);
		}
		void GPUProgram::setUniformMatrix4x2dv(const std::string& name, unsigned int count, bool transpose, const double* v, bool silentwarning) {
			glProgramUniformMatrix4x2dv(glprogram, internalGetAndSetUniformLocation(name, silentwarning), count, transpose ? GL_TRUE : GL_FALSE, v);
		}
		void GPUProgram::setUniformMatrix4x2fv(const std::string& name, unsigned int count, bool transpose, const float* v, bool silentwarning) {
			glProgramUniformMatrix4x2fv(glprogram, internalGetAndSetUniformLocation(name, silentwarning), count, transpose ? GL_TRUE : GL_FALSE, v);
		}
		void GPUProgram::setUniformMatrix4x3dv(const std::string& name, unsigned int count, bool transpose, const double* v, bool silentwarning) {
			glProgramUniformMatrix4x3dv(glprogram, internalGetAndSetUniformLocation(name, silentwarning), count, transpose ? GL_TRUE : GL_FALSE, v);
		}
		void GPUProgram::setUniformMatrix4x3fv(const std::string& name, unsigned int count, bool transpose, const float* v, bool silentwarning) {
			glProgramUniformMatrix4x3fv(glprogram, internalGetAndSetUniformLocation(name, silentwarning), count, transpose ? GL_TRUE : GL_FALSE, v);
		}

	}
}
