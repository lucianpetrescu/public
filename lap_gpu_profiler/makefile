###########################################################################################################################################
# new to makefiles ? -> read https://sites.google.com/site/michaelsafyan/software-engineering/how-to-write-a-makefile  also read this http://aegis.sourceforge.net/auug97.pdf
# NOTE: 
#    this makefile will not find differences in inline headers (headers with implementation code). The headers need to be addedd as a dependency on the object build rules. 
# INLINE_HEADERS += $(wildcard *.h) $(wildcard *.hpp)
###########################################################################################################################################


###########################################################################################################################################
# 															PROJECT FILES
PROJECT_NAME = lap_gpu_profiler
SOURCES := $(wildcard *.cpp) lap_wic.cpp lap_gpu_program.cpp
VPATH := dep

ifeq ($(OS),Windows_NT)
	OUTPUT_NAME := $(addsuffix _mingw, $(PROJECT_NAME))
else
	UNAME_S := $(shell uname -s)
    ifeq ($(UNAME_S),Linux)
        OUTPUT_NAME := $(addsuffix _nix, $(PROJECT_NAME))
    endif
    ifeq ($(UNAME_S),Darwin)
        OUTPUT_NAME := $(addsuffix _osx, $(PROJECT_NAME))
    endif
endif

OUTPUT_DIR := bin
OUTPUT_NAME_DEBUG =  $(OUTPUT_NAME)_d.exe
OUTPUT_NAME_RELEASE = $(OUTPUT_NAME).exe
OUTPUT_DEBUG_DIR := makedebug
OUTPUT_RELEASE_DIR := makerelease
INLINE_HEADERS :=
OBJS_DEBUG := $(addprefix $(OUTPUT_DIR)/$(OUTPUT_DEBUG_DIR)/,   $(addsuffix .o,   $(notdir $(basename $(SOURCES)))))
OBJS_RELEASE := $(addprefix $(OUTPUT_DIR)/$(OUTPUT_RELEASE_DIR)/,   $(addsuffix .o,   $(notdir $(basename $(SOURCES)))))


###########################################################################################################################################
# 													COMPILER AND LINKER FLAGS
#compiler automatically knows who CPP is and uses CXXFLAGS to create objs (note: CPPFLAGS is for C preprocessor.)
#release flags (deliverable) (all warnings, full optimization,  optimize for size, no debugging,  link time optimization, enable sse & sse2, enable sse for math
RELEASE_CPP_FLAGS = $(CXXFLAGS) -m64 -std=c++11 -Wall -O2 -Os -DNDEBUG -msse -msse2 -mfpmath=sse -Wno-unused-variable -Wno-sign-compare -Wno-unused-but-set-variable
RELEASE_LD_FLAGS = $(LDFLAGS) -Wno-unused-variable
#debug flags (debugging), export, variable shadowing (bad practice usually)
# debug linker can add --export-dynamic to export all symbols to the dynamic linker table (permits callstack inspection...), but this is only valid on ELF.
DEBUG_CPP_FLAGS = $(CXXFLAGS) -m64 -std=c++11 -Wall -Og -g -O0 -Wno-unused-variable -Wno-sign-compare
DEBUG_LD_FLAGS = $(LDFLAGS) 
# platform specific
ifeq ($(OS),Windows_NT)
	#mingw and cygwin don't have rdynamic, also have to add gdi32, which is automatically added in Visual Studio through pragma comment.
	DEBUG_LD_FLAGS += -lgdi32 -static-libgcc -static-libstdc++ -static -pthread
	RELEASE_LD_FLAGS += -lgdi32 -static-libgcc -static-libstdc++ -static -pthread
else
	#native make
	DEBUG_LD_FLAGS += -pthread -rdynamic -flto
	RELEASE_LD_FLAGS += -pthread -flto
	#platform dependent
	UNAME_S := $(shell uname -s)
	ifeq ($(UNAME_S),Linux)
		#link with OpenGL, dl, 
		DEBUG_LD_FLAGS += -ldl -lGL -lX11 -lXxf86vm -lXrandr -lXi -lXinerama -lXcursor -lrt -lm
		RELEASE_LD_FLAGS += -ldl -lGL -lX11 -lXxf86vm -lXrandr -lXi -lXinerama -lXcursor -lrt -lm
	endif
	ifeq ($(UNAME_S),Darwin)
		#tell compiler that we are mixing objective c and cpp
		DEBUG_CPP_FLAGS += -x objective-c++ -std=c++11
		RELEASE_CPP_FLAGS += -x objective-c++ -std=c++11
		#link with the required frameworks
		DEBUG_LD_FLAGS += -framework Cocoa -framework OpenGL -framework IOKit -framework CoreVideo -framework CoreFoundation -framework Carbon
		RELEASE_LD_FLAGS += -framework Cocoa -framework OpenGL -framework IOKit -framework CoreVideo -framework CoreFoundation -framework Carbon
	endif
endif


###############################################################################
#										BUILD TARGETS
#make might confuse "all" "clean" "distclean" etc targets for actual filenames, PHONY is used to prevent this confusion.
.PHONY: all pre-build-debug pre-build-release build-debug build-release debug release clean distclean help install

#the build process is the following
# 1. pre-build-(debug|release) is always called before compilation and it creates the folder structure required for building debug|release
# 2. build-(debug|release) builds the program for debug|release, and depends on pre-build-(debug|release)
# 3. debug|release is always called last (depends on build-(debug|release)),
all: debug release

pre-build-debug:
	@echo ------------------------------------ Building $(PROJECT_NAME) in debug mode --------
	@if [ ! -d $(OUTPUT_DIR) ]; then mkdir -p $(OUTPUT_DIR); fi
	@if [ ! -d $(OUTPUT_DIR)/$(OUTPUT_DEBUG_DIR) ]; then mkdir -p $(OUTPUT_DIR)/$(OUTPUT_DEBUG_DIR); fi
pre-build-release:
	@echo ------------------------------------ Building $(PROJECT_NAME) in release mode ------
	@if [ ! -d $(OUTPUT_DIR) ]; then mkdir -p $(OUTPUT_DIR); fi
	@if [ ! -d $(OUTPUT_DIR)/$(OUTPUT_RELEASE_DIR) ]; then mkdir -p $(OUTPUT_DIR)/$(OUTPUT_RELEASE_DIR); fi
	
OBJS_DEBUG := $(addprefix $(OUTPUT_DIR)/$(OUTPUT_DEBUG_DIR)/,   $(addsuffix .o,   $(notdir $(basename $(SOURCES)))))
OBJS_RELEASE := $(addprefix $(OUTPUT_DIR)/$(OUTPUT_RELEASE_DIR)/,   $(addsuffix .o,   $(notdir $(basename $(SOURCES)))))
$(OUTPUT_DIR)/$(OUTPUT_DEBUG_DIR)/%.o : %.cpp $(INLINE_HEADERS)
	$(CXX) $(DEBUG_CPP_FLAGS) -c $< -o $@
$(OUTPUT_DIR)/$(OUTPUT_RELEASE_DIR)/%.o : %.cpp $(INLINE_HEADERS)
	$(CXX) $(RELEASE_CPP_FLAGS) -c $< -o $@

$(OUTPUT_DIR)/$(OUTPUT_NAME_DEBUG): $(OBJS_DEBUG)
	$(CXX) $(OBJS_DEBUG) $(DEBUG_LD_FLAGS) -o $(OUTPUT_DIR)/$(OUTPUT_NAME_DEBUG)
$(OUTPUT_DIR)/$(OUTPUT_NAME_RELEASE): $(OBJS_RELEASE)
	$(CXX) $(OBJS_RELEASE) $(RELEASE_LD_FLAGS) -o $(OUTPUT_DIR)/$(OUTPUT_NAME_RELEASE)
debug: pre-build-debug $(OUTPUT_DIR)/$(OUTPUT_NAME_DEBUG)
	@echo Finished
release: pre-build-release $(OUTPUT_DIR)/$(OUTPUT_NAME_RELEASE)
	@echo Finished
clean:
	@echo ------------------------------------ Cleaning $(PROJECT_NAME) ----------------------
	rm -f $(OUTPUT_DIR)/$(OUTPUT_NAME_DEBUG)
	rm -f $(OUTPUT_DIR)/$(OUTPUT_NAME_RELEASE)
	rm -rf $(OUTPUT_DIR)/$(OUTPUT_DEBUG_DIR)
	rm -rf $(OUTPUT_DIR)/$(OUTPUT_RELEASE_DIR)
	
distclean: clean


###########################################################################################################################################
# 														HELP and INSTALL
help:
	@echo ---This makefile is used to create the test executable for $(PROJECT_NAME)---
	@echo ---Since the library is source only it can be used by just including it in your build system---
	@echo use -make all- to build both debug and release executables
	@echo use -make debug- to build only the debug executable
	@echo use -make release- to build only the release executable
install:
	@echo nothing to install

dbg:
	@echo $(DEBUG_CPP_FLAGS)
	@echo $(RELEASE_CPP_FLAGS)
	@echo $(DEBUG_LD_FLAGS)
	@echo $(RELEASE_LD_FLAGS)
