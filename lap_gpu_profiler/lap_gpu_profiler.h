///------------------------------------------------------------------------------------------------------------------------------
/// The MIT License (MIT)
/// 
/// Copyright (c) 2016 Lucian Petrescu, lucian (dot) petrescu (dot) 24 (at) gmail (dot) com
/// 
/// Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation 
/// files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, 
/// modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software 
/// is furnished to do so, subject to the following conditions:
/// 
/// The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
/// 
/// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
/// OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE 
/// LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR 
/// IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
///------------------------------------------------------------------------------------------------------------------------------


///---------------------------------------------------------------------------------------------------------------------
/// @file
/// @mainpage LAP GPUProfiler
/// GPUProfiler is useful for profiling the dedicated and available memory, the number of primitives generated, the number 
/// of generated transform feedback primitivs, the number of generated fragment samples and gpu and cpu work time.
///
/// @version 1.00
///
/// @par Properties:
///	- c++11, no exceptions
///	- OpenGL 4.5
///	- works on Windows, *nix and MACOS
///	- the OpenGL functions must be provided by the user, see line 107. In this case it uses the lap_wic library, set it to the desired OpenGL 
///   extension libray. This could have been done with extern functions but some OpenGL extension loader libraries are based on macros. Furthermore 
///	  macros are used by many hook-based OpenGL debug libraries, therefore the current system was preferred.
///
/// @par Compiling and linking:
///	- Windows:
///		- no special setup for visual studio projects
///		- -pthread -lgdi32 otherwise
///		- an example visual studio project+solution is provide in the vstudio folder
///		- an example makefile (vs version) is provided in root folder
///	- Linux:
///		- -ldl -lGL -lX11 -lXxf86vm -lXrandr -lXi -lXinerama -lXcursor -lrt -lm
///		- the wayland and mir backends are not used by lapwic (but they can easily be added in the future when they mature, right now they are experimental in glfw)
///		- an example makefile is provided in the root folder
///		- to install the project dependencies get:
///			- sudo apt-get install xorg-dev
///			- sudo apt-get install libgl-dev
///	- OSX
///		- -framework Cocoa -framework OpenGL -framework IOKit -framework CoreVideo -framework CoreFoundation -framework Carbon
///		- an example makefile is provided in the root folder
/// @author Lucian Petrescu( lucian (dot) petrescu (dot) 24 (at) gmail (dot) com )
///
/// @par Usage:
///	- basic usage
///		@code
///		#include "lap_gpu_profiler"
///		...
///		//create window context but don't create any gpu objects (this is only necessary on AMD cards, on NVIDIA you can create the profiler anywhere)
///		...
///		lap::GPUProfiler gpuprofiler;
///		...
///		//do some work here
///		...
///		//update
///		gpuprofiler.update();
///		//output stats
///		std::cout << "GPU time since creation = " << gpuprofiler.getGPUtimeSinceResetMS() << " ms"<< std::endl;
///		std::cout << "CPU time since reset = " << gpuprofiler.getCPUtimeSinceResetMS() << " ms" << std::endl;
///		std::cout << "GPU memory available = " << gpuprofiler.getMemoryAvailableInexactInKB() << " kb" << std::endl;
///		std::cout << "GPU memory dedicated = " << gpuprofiler.getMemoryDedicatedInKB() << " kb" << std::endl;
///		std::cout << "GPU num_updates = " << gpuprofiler.getNumUpdates() << std::endl;
///		std::cout << "GPU primitives generated = " << gpuprofiler.getPrimitivesGeneratedSinceReset() << std::endl;
///		std::cout << "GPU fragment sample count = " << gpuprofiler.getSampleCountSinceReset() << std::endl;
///		std::cout << "GPU transform freedback primitives = " << gpuprofiler.getTransformFeedbackPrimitivesWrittenSinceReset() << std::endl;
///		@endcode
///	- per frame usage
///		@code
///		#include "lap_gpu_profiler"
///		...
///		//create window context but don't create any gpu objects (this is only necessary on AMD cards, on NVIDIA you can create the profiler anywhere)
///		...
///		lap::GPUProfiler gpuprofiler;
///		...
///		while(app runnning){
///			//reset profiler
///			gpuprofiler.reset();
///			...
///			// do some work here
///			...
///			//update
///			gpuprofiler.update();
///			//output per-frame stats
///			std::cout << "GPU time per update = " << gpuprofiler.getGPUtimeSinceResetMS() / (float)gpuprofiler.getNumUpdates() << " ms" << std::endl;
///			std::cout << "CPU time per update = " << gpuprofiler.getCPUtimeSinceResetMS() / (float)gpuprofiler.getNumUpdates() << " ms" << std::endl;
///			std::cout << "GPU primitives generated per update = " << gpuprofiler.getPrimitivesGeneratedSinceReset() / (float)gpuprofiler.getNumUpdates() << std::endl;
///			std::cout << "GPU fragment sample count per update = " << gpuprofiler.getSampleCountSinceReset() / (float)gpuprofiler.getNumUpdates() << std::endl;
///		}
///		@endcode
///
///------------------------------------------------------------------------------------------------



#pragma once
// module needs OpenGL and wic provides it
#include "dep/lap_wic.hpp"
#include <string>
#include <chrono>

namespace lap {
	///gpu buffer
	class GPUProfiler {
	public:
		/// constructor. 
		/// DETAIL: uses the GL_SAMPLES_PASSED, GL_PRIMITIVES_GENERATED, GL_TRANSFORM_FEEDBACK_PRIMITIVES_WRITTEN and GL_TIME_ELAPSED OpenGL counters
		///         if the user does not want usage of these queries, the "noqueires" argument should be true.
		/// @param noqueries	queries only the available memory.
		GPUProfiler(bool noqueries = false);
		GPUProfiler(const GPUProfiler&) = delete;
		GPUProfiler(GPUProfiler&&) = delete;
		GPUProfiler& operator= (const GPUProfiler&) = delete;
		GPUProfiler& operator= (GPUProfiler&&) = delete;
		~GPUProfiler();

		/// resets all the counters
		/// DETAIL: uses the GL_SAMPLES_PASSED, GL_PRIMITIVES_GENERATED, GL_TRANSFORM_FEEDBACK_PRIMITIVES_WRITTEN and GL_TIME_ELAPSED OpenGL counters
		///         if the user does not want usage of these queries, the "noqueires" argument should be true.
		/// @param noqueries	queries only the available memory.
		void reset(bool noqueries = false);
		/// updates all the counters
		/// DETAIL: uses the GL_SAMPLES_PASSED, GL_PRIMITIVES_GENERATED, GL_TRANSFORM_FEEDBACK_PRIMITIVES_WRITTEN and GL_TIME_ELAPSED OpenGL counters
		///         if the user does not want usage of these queries, the "noqueires" argument should be true.
		/// @param noqueries	queries only the available memory.
		void update(bool noqueries = false);

		/// returns the dedicated GPU memory in kb
		/// @return the dedicated GPU memory in kb
		size_t getMemoryDedicatedInKB() const;
		/// returns the available GPU memory in kb
		/// @return the available GPU memory in kb
		size_t getMemoryAvailableInexactInKB() const;
		/// returns the sample count (fragment samples) since the last reset
		/// @return the sample count (fragment samples) since the last reset
		size_t getSampleCountSinceReset() const;
		/// returns the primitive count (points, triangles, lines, NOT vertices), written on the default(0th) geometry shader stream, since the last reset
		/// @return the primitive count (points, triangles, lines, NOT vertices) written on the default(0th) geometry shader stream, since the last reset
		size_t getPrimitivesGeneratedSinceReset() const;
		/// returns the GPU used time since the last reset
		/// @return the GPU used time since the last reset
		size_t getGPUtimeSinceResetMS() const;
		/// returns the CPU time since the last reset
		/// @return the CPU time since the last reset
		size_t getCPUtimeSinceResetMS() const;
		/// returns the transform feedback number of primitives written to the default (0th) stream since the last reset
		/// @return the transform feedback number of primitives written to the default (0th) stream since the last reset
		size_t getTransformFeedbackPrimitivesWrittenSinceReset() const;
		/// returns the number of updates since the last reset
		/// @return the number of updates since the last reset
		size_t getNumUpdates() const;
		/// returns the vendor name (e.g. NVIDIA Corporation, AMD Corporation, Intel Corporation, etc)
		/// @return the vendor name
		const std::string& getVendor() const;
		/// returns the renderer (graphics card engine) name
		/// @return the renderer
		const std::string& getRenderer() const;
		/// returns the major OpenGL version
		/// @return the major OpenGL version
		const unsigned int getVersionMajor() const;
		/// returns the minor OpenGL version
		/// @return the minor OpenGL version
		const unsigned int getVersionMinor() const;
	private:
		std::string string_vendor, string_renderer;
		unsigned int version_major, version_minor;
		GLuint queries[4];
		size_t updates = 0;
		size_t memory_available = 0;
		size_t memory_dedicated = 0;
		size_t sample_count = 0;
		size_t primitives_generated = 0;
		size_t gpu_time = 0;
		size_t cpu_time = 0;
		size_t transform_feedback_primitives_written = 0;
		unsigned int platform = 0;
		std::chrono::time_point<std::chrono::high_resolution_clock> time_reset;
	};
}