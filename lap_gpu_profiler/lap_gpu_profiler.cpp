﻿///------------------------------------------------------------------------------------------------------------------------------
/// The MIT License (MIT)
/// 
/// Copyright (c) 2016 Lucian Petrescu, lucian (dot) petrescu (dot) 24 (at) gmail (dot) com
/// 
/// Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation 
/// files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, 
/// modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software 
/// is furnished to do so, subject to the following conditions:
/// 
/// The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
/// 
/// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
/// OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE 
/// LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR 
/// IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
///------------------------------------------------------------------------------------------------------------------------------

#include "lap_gpu_profiler.h"
#include <algorithm>
#include <cstring>

namespace lap {
	
	//various vendor constants, because a common ARB extension is just too much..
	const GLint GL_GPU_MEMORY_INFO_DEDICATED_VIDMEM_NVX = 0x9047;
	const GLint GL_GPU_MEMORY_INFO_CURRENT_AVAILABLE_VIDMEM_NVX = 0x9049;
	const GLint GL_VBO_FREE_MEMORY_ATI = 0x87FB;
	const GLint GL_TEXTURE_FREE_MEMORY_ATI = 0x87FC;
	const GLint GL_RENDERBUFFER_FREE_MEMORY_ATI = 0x87FD;

	//supported vendor names
	//0 = uninitialized
	const int PLATFORM_AMDATI = 1;
	const int PLATFORM_NVIDIA = 2;
	const int PLATFORM_UNSUPPORTED = 3;

	GPUProfiler::GPUProfiler(bool noqueries) {
		//create queries
		glGenQueries(4, &queries[0]);

		//discover platform
		if (platform == 0) {
			int num_extensions;
			glGetIntegerv(GL_NUM_EXTENSIONS, &num_extensions);
			std::string GL_NVX_gpu_memory_info = "GL_NVX_gpu_memory_info";
			std::string GL_ATI_meminfo = "GL_ATI_meminfo";
			bool nvidia = false, amdati = false;
			for (int i = 0; i < num_extensions; i++) {	
				if (std::strcmp((const char*)glGetStringi(GL_EXTENSIONS, i), "GL_NVX_gpu_memory_info") == 0) nvidia = true;					// http://developer.download.nvidia.com/opengl/specs/GL_NVX_gpu_memory_info.txt	
				else if (std::strcmp((const char*)glGetStringi(GL_EXTENSIONS, i), "GL_ATI_meminfo") == 0) amdati = true;					// https://www.opengl.org/registry/specs/ATI/meminfo.txt
			}
			if (nvidia) platform = PLATFORM_NVIDIA;				//NVIDIA
			else if (amdati) platform = PLATFORM_AMDATI;		//ATI AMD
			else platform = PLATFORM_UNSUPPORTED;				//other, not supported (there has to be an extension..)
		}

		//query dedicated memory
		GLint64 aux[4];
		if (platform == PLATFORM_NVIDIA) {
			//NVIDIA, no need for additional extensions
			glGetInteger64v(GL_GPU_MEMORY_INFO_DEDICATED_VIDMEM_NVX, &aux[0]);
			memory_dedicated = aux[0];
		}
		else if (platform == PLATFORM_AMDATI) {
			//given that this software is written for 4.5 the hardware is guaranteed to be unified, most probably the same pools are used for allocations, 
			//we'll use max, basically select the max pool. 
			glGetInteger64v(GL_VBO_FREE_MEMORY_ATI, aux);
			memory_dedicated = (size_t)aux[0];
			glGetInteger64v(GL_TEXTURE_FREE_MEMORY_ATI, aux);
			memory_dedicated = std::max(memory_dedicated, (size_t)aux[0]);
			glGetInteger64v(GL_RENDERBUFFER_FREE_MEMORY_ATI, aux);
			memory_dedicated = std::max(memory_dedicated, (size_t)aux[0]);
		}
		else {
			//no api for intel
			memory_dedicated = 0;
		}

		//query relevant driver, vendor and version information
		string_vendor = (const char*)glGetString(GL_VENDOR);
		string_renderer = (const char*)glGetString(GL_RENDERER);
		glGetInteger64v(GL_MAJOR_VERSION, aux);
		version_major = (unsigned int)aux[0];
		glGetInteger64v(GL_MINOR_VERSION, aux);
		version_minor = (unsigned int)aux[0];

		//reset
		reset(noqueries);
	}
	GPUProfiler::~GPUProfiler() {
		glDeleteQueries(4, &queries[0]);
	}

	void GPUProfiler::reset(bool noqueries) {
		//reset stats
		updates = 0;
		memory_available = 0;
		sample_count = 0;
		primitives_generated = 0;
		gpu_time = 0;
		transform_feedback_primitives_written = 0;

		if (!noqueries) {
			//inject queries into command queue
			glBeginQuery(GL_TIME_ELAPSED, queries[0]);
			glBeginQuery(GL_SAMPLES_PASSED, queries[1]);
			glBeginQuery(GL_PRIMITIVES_GENERATED, queries[2]);
			glBeginQuery(GL_TRANSFORM_FEEDBACK_PRIMITIVES_WRITTEN, queries[3]);
		}

		//cpu time
		time_reset = std::chrono::high_resolution_clock::now();
	}

	GLuint64 waitForQuery(GLint query) {
		GLint done = GL_FALSE;
		while (done != GL_FALSE) glGetQueryObjectiv(query, GL_QUERY_RESULT_AVAILABLE, &done);
		GLuint64 value;
		glGetQueryObjectui64v(query, GL_QUERY_RESULT, &value);
		return value;
	}

	void GPUProfiler::update(bool noqueries) {
		if (!noqueries) {
			//end queries
			glEndQuery(GL_TIME_ELAPSED);
			glEndQuery(GL_SAMPLES_PASSED);
			glEndQuery(GL_PRIMITIVES_GENERATED);
			glEndQuery(GL_TRANSFORM_FEEDBACK_PRIMITIVES_WRITTEN);
		}

		//increase update count
		updates++;

		//query results
		gpu_time += (size_t)(waitForQuery(queries[0]) / 1000000.0);  //nano to milli
		sample_count += (size_t)waitForQuery(queries[1]);
		primitives_generated += (size_t)waitForQuery(queries[2]);
		transform_feedback_primitives_written += (size_t)waitForQuery(queries[3]);

		if (!noqueries) {
			//re-inject queries
			glBeginQuery(GL_TIME_ELAPSED, queries[0]);
			glBeginQuery(GL_SAMPLES_PASSED, queries[1]);
			glBeginQuery(GL_PRIMITIVES_GENERATED, queries[2]);
			glBeginQuery(GL_TRANSFORM_FEEDBACK_PRIMITIVES_WRITTEN, queries[3]);
		}

		//query available memory
		GLint64 aux[4];
		if (platform == PLATFORM_NVIDIA) {
			//nvidia
			glGetInteger64v(GL_GPU_MEMORY_INFO_CURRENT_AVAILABLE_VIDMEM_NVX, &aux[0]);
			memory_available = aux[0];
		}
		else if (platform == PLATFORM_AMDATI) {
			//ati
			glGetInteger64v(GL_VBO_FREE_MEMORY_ATI, aux);
			memory_available = (size_t)aux[0];
			glGetInteger64v(GL_TEXTURE_FREE_MEMORY_ATI, aux);
			memory_available = std::max(memory_available, (size_t)aux[0]);
			glGetInteger64v(GL_RENDERBUFFER_FREE_MEMORY_ATI, aux);
			memory_available = std::max(memory_available, (size_t)aux[0]);
		}
		else {
			//no api for intel
			memory_available = 0;
		}

		//cpu time
		auto now = std::chrono::high_resolution_clock::now();
		cpu_time = (size_t)(std::chrono::duration<double>(now - time_reset).count() * 1000);
	}

	size_t GPUProfiler::getMemoryDedicatedInKB() const {
		return memory_dedicated;
	}
	size_t GPUProfiler::getMemoryAvailableInexactInKB() const {
		return memory_available;
	}
	size_t GPUProfiler::getSampleCountSinceReset() const {
		return sample_count;
	}
	size_t GPUProfiler::getPrimitivesGeneratedSinceReset() const {
		return primitives_generated;
	}
	size_t GPUProfiler::getGPUtimeSinceResetMS() const {
		return gpu_time;
	}
	size_t GPUProfiler::getCPUtimeSinceResetMS() const {
		return cpu_time;
	}
	size_t GPUProfiler::getTransformFeedbackPrimitivesWrittenSinceReset() const {
		return transform_feedback_primitives_written;
	}
	size_t GPUProfiler::getNumUpdates() const {
		return updates;
	}
	const std::string& GPUProfiler::getVendor() const {
		return string_vendor;
	}
	const std::string& GPUProfiler::getRenderer() const {
		return string_renderer;
	}
	const unsigned int GPUProfiler::getVersionMajor() const {
		return version_major;
	}
	const unsigned int GPUProfiler::getVersionMinor() const {
		return version_minor;
	}
}
